﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using OVTime.Services.Models.Base;
using OVTime.Services.Models.External;

namespace OVTime.Services.DataAccessLayer.Interfaces
{
    public interface IDataRepository<TModel> where TModel : ModelBase
    {
        CrudResponse Create(TModel model);
        TModel Read(long id);
        CrudResponse Update(TModel model);
        CrudResponse Delete(long id);
        IEnumerable<CrudResponse> Create(IEnumerable<TModel> models);
        IEnumerable<TModel> Read(IEnumerable<long> ids);
        IEnumerable<CrudResponse> Update(IEnumerable<TModel> model);
        IEnumerable<CrudResponse> Delete(IEnumerable<long> ids);
    }
}
