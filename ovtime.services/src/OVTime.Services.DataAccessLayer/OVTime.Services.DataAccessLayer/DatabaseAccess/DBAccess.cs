﻿using OVTime.Services.Models.External;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace OVTime.Services.DataAccessLayer.DatabaseAccess
{

        public class DBAccess
        {
            private SqlConnection ConnectDB()
            {
                SqlConnection Con = new SqlConnection();

                try
                {
                    Con.ConnectionString = "Data Source=tcp:edb-omni-prod01,1433;Initial Catalog=OVTIME_ECG;User ID=tm_dev;Password=tm_dev;MultipleActiveResultSets=True;";
                    return Con;
                }

                catch (CrudResponse Exc)
                {
                    IEnumerable<string> ErrorInfos = new List<string> { "Error when connecting to DB" };
                    Exc.HasError = true;
                    Exc.HasInfo = true;
                    Exc.Infos = ErrorInfos;
                    throw Exc;
                }
            }

            public DataSet GetDataSet(string sqlStatement)
            {
                DataSet dsData = new DataSet();

                try
                {
                    SqlConnection Con = new SqlConnection();
                    Con = ConnectDB();

                    SqlCommand Command = new SqlCommand();
                    Command.CommandText = sqlStatement;
                    Command.CommandTimeout = 350;
                    Command.Connection = Con;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter(Command);
                    dataAdapter.Fill(dsData);

                    return dsData;
                }

                catch (CrudResponse Exc)
                {
                    IEnumerable<string> ErrorInfos = new List<string> { "Error when getting dataset" + sqlStatement };
                    Exc.HasError = true;
                    Exc.HasInfo = true;
                    Exc.Infos = ErrorInfos;
                    throw Exc;
                }

            }

            public int ExecuteNonQuery(string sqlStatement)
            {
                int RowsAffected = 0;
                try
                {
                    SqlConnection Con = new SqlConnection();
                    Con = ConnectDB();

                    SqlCommand Command = new SqlCommand();
                    Command.CommandText = sqlStatement;
                    Command.CommandTimeout = 3600;
                    Command.Connection = Con;

                    Con.Open();
                    RowsAffected = Command.ExecuteNonQuery();
                    Con.Close();

                    return RowsAffected;
                }

                catch (CrudResponse Exc)
                {
                    IEnumerable<string> ErrorInfos = new List<string> { "Error when execute query" + sqlStatement };
                    Exc.HasError = true;
                    Exc.HasInfo = true;
                    Exc.Infos = ErrorInfos;
                    throw Exc;
                }
            }
        }
    }
