﻿using OVTime.Services.DataAccessLayer.DatabaseAccess;
using OVTime.Services.DataAccessLayer.Interfaces;
using OVTime.Services.Models.External;
using OVTime.Services.Models.Internal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace OVTime.Services.DataAccessLayer.Implementations
{
    public class EndOfServiceRuleRepository : IDataRepository<EndOfServiceRule>
    {
        public CrudResponse Create(EndOfServiceRule model)
        {
            return null;
        }
        public EndOfServiceRule Read(long id)
        {
            DBAccess db = new DBAccess();
            string query = string.Format("Select * from OMV_EndOfServiceRule where Id = {0} ",id);
            DataTable dt = db.GetDataSet(query).Tables[0];

            EndOfServiceRule Rule = new EndOfServiceRule();
            Rule.Id = int.Parse(dt.Rows[0]["Id"].ToString());
            Rule.CalculationBased = dt.Rows[0]["CalculationBased"].ToString();
            Rule.CompanyName = dt.Rows[0]["CompanyName"].ToString();
            Rule.IncludeThistYearVacations = int.Parse(dt.Rows[0]["IncludeThisYearVacations"].ToString());
            Rule.IncludePreviousYearsVacations = int.Parse(dt.Rows[0]["IncludePreviousYearsVacations"].ToString());

            return Rule;
        }        
        public CrudResponse Update(EndOfServiceRule model)
        {
            return null;
        }
        public CrudResponse Delete(long id)
        {
            return null;
        }
        public IEnumerable<CrudResponse> Create(IEnumerable<EndOfServiceRule> models)
        {
            return null;
        }
        public IEnumerable<EndOfServiceRule> Read(IEnumerable<long> ids)
        {
            return null;
        }
        public IEnumerable<CrudResponse> Update(IEnumerable<EndOfServiceRule> model)
        {
            return null;
        }
        public IEnumerable<CrudResponse> Delete(IEnumerable<long> ids)
        {
            return null;
        }
    }
}
