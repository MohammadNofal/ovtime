﻿using OVTime.Services.DataAccessLayer.Interfaces;
using OVTime.Services.Models.External;
using OVTime.Services.Models.Internal;
using OVTime.Services.DataAccessLayer.DatabaseAccess;
using OVTime.Services.Core.Implementations;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Data;

namespace OVTime.Services.DataAccessLayer.Implementations
{
    public class UserVacationRepository : IDataRepository<VacationHistory>
    {
       

        public CrudResponse Create(VacationHistory Vacation)
        {
            //CrudResponse CrudRes = new CrudResponse();
            //SqlCommand Command = new SqlCommand();
            //try
            //{
            //    DBAccess db = new DBAccess();
            //    string sqlQuery = string.Empty;
            //    sqlQuery = string.Format("Insert into OMV_UserVacation (Id,VacationDate,UserId,SickLeave) values ({0},'{1}','{2}',{3})", Vacation.Id, Vacation.VacationDate, Vacation.UserId, Vacation.SickLeave);
            //    db.ExecuteNonQuery(sqlQuery);

            //    CrudRes.HasError = false;
            //    return CrudRes;
            //}
            //catch (Exception e)
            //{

            //    return CrudRes;
            //}
            return null;
        }

        public IEnumerable<CrudResponse> Create(IEnumerable<VacationHistory> VacationList)
        {            
            return null;
        }

        public CrudResponse Delete(long id)
        {
            CrudResponse CrudRes = new CrudResponse();
            SqlCommand Command = new SqlCommand();
            try
            {
                DBAccess db = new DBAccess();
                string sqlQuery = string.Empty;
                sqlQuery = string.Format("Delete OMV_UserVacation where Id ={0}", id.ToString());
                db.ExecuteNonQuery(sqlQuery);


                //.Add(Vacation);
                CrudRes.HasError = false;
                return CrudRes;
            }
            catch (Exception e)
            {
                CrudRes.HasError = true;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Delete(IEnumerable<long> ids)
        {            
            return null;
        }

        public VacationHistory Read(long id)
        {            
            return null;
        }

        public VacationHistory Read(string UserId)
        {
            DBAccess db = new DBAccess();
            CalculatedVacations ThisYearCalcVacations = new CalculatedVacations();
            CalculatedVacations PreviousYearsCalcVacations = new CalculatedVacations();
            UserRepository UserRepo = new UserRepository();
            VacationRuleRepository RuleRepo = new VacationRuleRepository();
            EmployeeSalaryRepository SalaryRepo = new EmployeeSalaryRepository();
            WeekendRuleRepository WeekendRepo = new WeekendRuleRepository();

            User Employee = UserRepo.Read(UserId);
            EmployeeSalary Salary = SalaryRepo.Read(UserId);
            VacationRule Rule = RuleRepo.Read(Salary.VacationRuleId);
            WeekendRule Weekend = WeekendRepo.Read(Rule.WeekendRuleId);
            VacationHistory History = new VacationHistory();


            VacationCalculations Calc = new VacationCalculations();
            double ThisYearTotalVacations = Calc.CalculateEarnedVacations(Employee, Rule);
            double PreviousYearsTotalVacations = Calc.CalculatePreviousYearsTotalVacations(Employee, Rule);

            DateTime StartDate = Employee.StartDate;
            int StartDay = StartDate.Day;
            int StartMonth = StartDate.Month;
            int StartYear = StartDate.Year;

            int CurrentDay = DateTime.Today.Day;
            int CurrentMonth = DateTime.Today.Month;
            int CurrentYear = DateTime.Today.Year;
            string ThisYearVacationsSQL = string.Empty;


            //gettitng all employee vacations for this year
            if (CurrentMonth >= StartMonth)
            {
                ThisYearVacationsSQL = string.Format("Select * from OVEmployeeVacationView where datepart(year,VacationDate)={0} " +
                    "and (datepart(month,VacationDate)>{1} or (datepart(month,VacationDate)={1} and datepart(day,VacationDate)<={3})) " +
                    "and UserId = '{2}' " +
                    "order by VacationDate desc", CurrentYear, StartMonth, Employee.Id, StartDay);
            }
            else
            {
                ThisYearVacationsSQL = string.Format("Select * from OVEmployeeVacationView where UserId = '{2}' " +
                    "and ((datepart(year,VacationDate) = {0} and (datepart(month,VacationDate)<={4})) " +
                    "or (datepart(year,VacationDate) = {0}-1 and datepart(month,VacationDate)>{1}) " +
                    "or(datepart(year,VacationDate) = {0}-1 and datepart(month,VacationDate)={1} and datepart(day,VacationDate)>={3})) order by VacationDate desc", CurrentYear, StartMonth, Employee.Id, StartDay, CurrentMonth);
            }
            DataTable ThisYearTakenVacations = db.GetDataSet(ThisYearVacationsSQL).Tables[0];

            ThisYearCalcVacations = Calc.CalculateEmployeeVacations(Employee, ThisYearTakenVacations, Rule, ThisYearTotalVacations, Salary, Weekend,false);
            History.ThisYear = ThisYearCalcVacations;


            string PreviousYearsVacationsSQL = string.Empty;
            //getting all employee vacations for previous years
            if (CurrentMonth < StartMonth)
            {
                PreviousYearsVacationsSQL = string.Format("Select * from OVEmployeeVacationView where UserId = '{0}' " +
                    "and VacationDate < DateAdd(year,-1,'{1}/{2}/{3}') " +
                    "order by VacationDate desc", Employee.Id, StartMonth, StartDay, CurrentYear);
            }
            else
                if (CurrentMonth > StartMonth)
            { 
                PreviousYearsVacationsSQL = string.Format("Select * from OVEmployeeVacationView where UserId = '{0}' " +
                    "and VacationDate < '{1}/{2}/{3}' " +
                    "order by VacationDate desc", Employee.Id, StartMonth ,StartDay, CurrentYear);
            }
            else
                if(CurrentMonth==StartMonth)
            {
                if (CurrentDay>StartDay)
                    PreviousYearsVacationsSQL = string.Format("Select * from OVEmployeeVacationView where UserId = '{0}' " +
                    "and VacationDate < '{1}/{2}/{3}' " +
                    "order by VacationDate desc", Employee.Id, StartMonth, StartDay, CurrentYear);
                else
                    PreviousYearsVacationsSQL = string.Format("Select * from OVEmployeeVacationView where UserId = '{0}' " +
                    "and VacationDate < DateAdd(year,-1,'{1}/{2}/{3}') " +
                    "order by VacationDate desc", Employee.Id, StartMonth, StartDay, CurrentYear);
            }

            DataTable PreviousYearsTakenVacations = db.GetDataSet(PreviousYearsVacationsSQL).Tables[0];
            PreviousYearsCalcVacations = Calc.CalculateEmployeeVacations(Employee, PreviousYearsTakenVacations, Rule, PreviousYearsTotalVacations, Salary,Weekend,true);
            History.PreviousYears = PreviousYearsCalcVacations;
            return History;
        }

        public Double Read(string UserId, bool EndOfService = true)
        {            
            return 0;
        }

        public IEnumerable<VacationHistory> Read(IEnumerable<long> ids)
        {
            return null;
        }

        public CrudResponse Update(VacationHistory Vacation)
        {            
            return null;
        }

        public IEnumerable<CrudResponse> Update(IEnumerable<VacationHistory> VacationList)
        {
            return null;
        }
    }
}
