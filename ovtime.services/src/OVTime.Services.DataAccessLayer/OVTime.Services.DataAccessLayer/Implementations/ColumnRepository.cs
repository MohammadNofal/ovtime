﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using OVTime.Services.DataAccessLayer.Interfaces;
using OVTime.Services.Models.External;
using System.Linq;

namespace OVTime.Services.DataAccessLayer.Implementations
{
    public class ColumnRepository : IDataRepository<OVReportColumn>
    {
        List<OVReportColumn> AllColumns = new List<OVReportColumn>();// to be removed
        


        public SqlConnection GetOpenConnection()
        {
            string ConnectioString = Globals.connectiongString;
            SqlConnection Connection = new SqlConnection(ConnectioString);
            Connection.Open();
            return Connection;
        }
        public List<OVReportColumn> getAllColumns()
        {
            SqlConnection Connection = GetOpenConnection();
            string query = "select distinct ColumnName, ColumnDisplayName,ColumnTableName,CAliasTableName from OVReportDetailsView";
            SqlCommand Command = new SqlCommand(query, Connection);
            SqlDataReader reader = Command.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            Connection.Close();
            List <OVReportColumn> AllColumns = new List<OVReportColumn>();
            
            dt.Rows.CopyTo(AllColumns.ToArray(),0);
            return AllColumns;
        }
        

        public CrudResponse Create(OVReportColumn Column)
        {
            CrudResponse CrudRes = new CrudResponse();
            try
            {
                AllColumns.Add(Column);
                return CrudRes;
            }
            catch
            {
                CrudRes.HasError = true;
                CrudRes.HasInfo = true;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Create(IEnumerable<OVReportColumn> ColumnList)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (OVReportColumn Column in ColumnList)
                {
                    AllColumns.Add(Column);
                    CrudResponse CrudRes = new CrudResponse();
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }

        public CrudResponse Delete(long id)
        {
            CrudResponse CrudRes = new CrudResponse();
            OVReportColumn Column = AllColumns.Find(p => p.Id == id);
            if (Column != null)
            {
                try
                {
                    AllColumns.Remove(Column);
                    CrudRes.HasError = false;
                    return CrudRes;
                }
                catch
                {
                    CrudRes.HasError = true;
                    return CrudRes;
                }
            }
            else
            {
                CrudRes.HasError = false;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Delete(IEnumerable<long> ids)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (long id in ids)
                {
                    OVReportColumn Column = new OVReportColumn();
                    Column = AllColumns.Find(p => p.Id == id);
                    AllColumns.Remove(Column);
                    CrudResponse CrudRes = new CrudResponse();
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }

        public OVReportColumn Read(long id)
        {
            CrudResponse CrudRes = new CrudResponse();
            OVReportColumn Join = new OVReportColumn();
            try
            {
                Join = AllColumns.Find(p => p.Id == id);
                CrudRes.HasError = false;
                return Join;
            }
            catch
            {
                CrudRes.HasError = true;
                return null;
            }
        }

        public IEnumerable<OVReportColumn> Read(IEnumerable<long> ids)
        {
            CrudResponse CrudRes = new CrudResponse();
            IList<OVReportColumn> JoinList = new List<OVReportColumn>();
            try
            {
                foreach (long id in ids)
                {
                    OVReportColumn Column = AllColumns.Find(p => p.Id == id);
                    JoinList.Add(Column);
                    CrudRes.HasError = false;
                }
                return JoinList;
            }
            catch
            {
                CrudRes.HasError = true;
                return null;
            }
        }

        public CrudResponse Update(OVReportColumn Column)
        {
            CrudResponse CrudRes = new CrudResponse();
            OVReportColumn TempColumn = new OVReportColumn();
            try
            {
                TempColumn = AllColumns.Find(p => p.Id == Column.Id);
                TempColumn = Column;
                return CrudRes;
            }
            catch
            {
                CrudRes.HasError = true;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Update(IEnumerable<OVReportColumn> ColumnList)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (OVReportColumn Column in ColumnList)
                {
                    CrudResponse CrudRes = new CrudResponse();
                    Update(Column);
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }
    }
}
