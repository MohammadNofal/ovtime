﻿using OVTime.Services.DataAccessLayer.DatabaseAccess;
using OVTime.Services.DataAccessLayer.Interfaces;
using OVTime.Services.Models.External;
using OVTime.Services.Models.Internal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace OVTime.Services.DataAccessLayer.Implementations
{
    public class UserRepository : IDataRepository<User>
    {
        
        List<User> AllUsers = new List<User>();

        public CrudResponse Create(User Usr)// dummy data to be filled from database
        {
            CrudResponse CrudRes = new CrudResponse();
            try
            {
                AllUsers.Add(Usr);
                CrudRes.HasError = false;
                return CrudRes;
            }
            catch
            {
                CrudRes.HasError = true;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Create(IEnumerable<User> UserlList)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (User Usr in UserlList)
                {
                    AllUsers.Add(Usr);
                    CrudResponse CrudRes = new CrudResponse();
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }

        public CrudResponse Delete(long id)
        {
            return null;
        }

        public CrudResponse Delete(string id)
        {
            CrudResponse CrudRes = new CrudResponse();
            User Usr = AllUsers.Find(p => p.Id == id);
            if (Usr != null)
            {
                try
                {
                    AllUsers.Remove(Usr);
                    CrudRes.HasError = false;
                    return CrudRes;
                }
                catch
                {
                    CrudRes.HasError = true;
                    return CrudRes;
                }
            }
            else
            {
                CrudRes.HasError = false;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Delete(IEnumerable<long> ids)
        {
            return null;
        }

        public IEnumerable<CrudResponse> Delete(IEnumerable<string> ids)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (string id in ids)
                {
                    User Usr = new User();
                    Usr = AllUsers.Find(p => p.Id == id);
                    AllUsers.Remove(Usr);
                    CrudResponse CrudRes = new CrudResponse();
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }

        public User Read(long id)
        {
            return null;
        }

        public User Read(string id)
        {
            CrudResponse CrudRes = new CrudResponse();
            SqlCommand Command = new SqlCommand();
            User Employee = new User(); ;
            try
            {
                DBAccess db = new DBAccess();
                string sqlQuery = string.Empty;
                sqlQuery = string.Format("Select * from Employee where EmployeeID like '{0}'", id.ToString());
                DataTable dt = new DataTable();
                dt = db.GetDataSet(sqlQuery).Tables[0];
                Employee.Id = id;
                Employee.StartDate = DateTime.Parse(dt.Rows[0]["EmpDateHired"].ToString());


                return Employee;

            }
            catch (Exception e)
            {
                CrudRes.HasError = true;
                return null;
            }
        }

        public IEnumerable<User> Read(IEnumerable<long> ids)
        {
            return null;
        }

        public IEnumerable<User> Read(IEnumerable<string> ids)
        {
            CrudResponse CrudRes = new CrudResponse();
            IList<User> UserList = new List<User>();
            try
            {
                foreach (string id in ids)
                {
                    User Usr = AllUsers.Find(p => p.Id == id);
                    UserList.Add(Usr);
                    CrudRes.HasError = false;
                }
                return UserList;
            }
            catch
            {
                CrudRes.HasError = true;
                return null;
            }
        }

        public CrudResponse Update(User Usr)
        {
            CrudResponse CrudRes = new CrudResponse();
            User TempUsr = new User();
            try
            {
                TempUsr = AllUsers.Find(p => p.Id == Usr.Id);
                TempUsr = Usr;
                return CrudRes;
            }
            catch
            {
                CrudRes.HasError = true;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Update(IEnumerable<User> UserList)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (User Usr in UserList)
                {
                    CrudResponse CrudRes = new CrudResponse();
                    Update(Usr);
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }
    }
}
