﻿using OVTime.Services.DataAccessLayer.DatabaseAccess;
using OVTime.Services.DataAccessLayer.Interfaces;
using OVTime.Services.Models.External;
using OVTime.Services.Models.Internal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace OVTime.Services.DataAccessLayer.Implementations
{
    public class VacationRuleRepository : IDataRepository<VacationRule>
    {
        List<VacationRule> AllVacationRules = new List<VacationRule>();

        public CrudResponse Create(VacationRule Rule)// dummy data to be filled from database
        {
            CrudResponse CrudRes = new CrudResponse();
            try
            {
                AllVacationRules.Add(Rule);
                CrudRes.HasError = false;
                return CrudRes;
            }
            catch
            {
                CrudRes.HasError = true;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Create(IEnumerable<VacationRule> RuleList)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (VacationRule Rule in RuleList)
                {
                    AllVacationRules.Add(Rule);
                    CrudResponse CrudRes = new CrudResponse();
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }

        public CrudResponse Delete(long id)
        {
            CrudResponse CrudRes = new CrudResponse();
            VacationRule Rule = AllVacationRules.Find(p => p.Id == id);
            if (Rule != null)
            {
                try
                {
                    AllVacationRules.Remove(Rule);
                    CrudRes.HasError = false;
                    return CrudRes;
                }
                catch
                {
                    CrudRes.HasError = true;
                    return CrudRes;
                }
            }
            else
            {
                CrudRes.HasError = false;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Delete(IEnumerable<long> ids)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (long id in ids)
                {
                    VacationRule Rule = new VacationRule();
                    Rule = AllVacationRules.Find(p => p.Id == id);
                    AllVacationRules.Remove(Rule);
                    CrudResponse CrudRes = new CrudResponse();
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }

        public VacationRule Read(long id)
        {
            WeekendRuleRepository WeekendRepo = new WeekendRuleRepository();
            CrudResponse CrudRes = new CrudResponse();
            SqlCommand Command = new SqlCommand();
            VacationRule Rule = new VacationRule();
            try
            {
                DBAccess db = new DBAccess();
                string sqlQuery = string.Empty;
                sqlQuery = string.Format("Select * from OMV_VacationRule where Id ={0}", id.ToString());
                DataTable dt = new DataTable();
                dt = db.GetDataSet(sqlQuery).Tables[0];

                Rule.Id = int.Parse(dt.Rows[0]["Id"].ToString());
                Rule.VacationsPerYear = int.Parse(dt.Rows[0]["VacationsPerYear"].ToString());
                Rule.SickLeavePerYear = int.Parse(dt.Rows[0]["SickLeavePerYear"].ToString());
                Rule.IncludeFirstYear = int.Parse(dt.Rows[0]["IncludeFirstYear"].ToString());
                Rule.VacationType = dt.Rows[0]["VacationType"].ToString();
                Rule.WorkingHours = double.Parse(dt.Rows[0]["WorkingHours"].ToString());
                Rule.WeekendRuleId = int.Parse(dt.Rows[0]["WeekendRuleId"].ToString());
                Rule.CompanyName = dt.Rows[0]["CompanyName"].ToString();
                Rule.Weekend = WeekendRepo.Read(Rule.WeekendRuleId);
                return Rule;

            }
            catch (Exception e)
            {
                CrudRes.HasError = true;
                return null;
            }
        }

        public VacationRule Read(string id)
        {
            WeekendRuleRepository WeekendRepo = new WeekendRuleRepository();
            CrudResponse CrudRes = new CrudResponse();
            SqlCommand Command = new SqlCommand();
            VacationRule Rule = new VacationRule();
            try
            {
                DBAccess db = new DBAccess();
                string sqlQuery = string.Empty;
                sqlQuery = string.Format("Select * from OMV_VacationRule where CompanyName ='{0}'", id.ToString());
                DataTable dt = new DataTable();
                dt = db.GetDataSet(sqlQuery).Tables[0];

                Rule.Id = int.Parse(dt.Rows[0]["Id"].ToString());
                Rule.VacationsPerYear = int.Parse(dt.Rows[0]["VacationsPerYear"].ToString());
                Rule.SickLeavePerYear = int.Parse(dt.Rows[0]["SickLeavePerYear"].ToString());
                Rule.IncludeFirstYear = int.Parse(dt.Rows[0]["IncludeFirstYear"].ToString());
                Rule.VacationType = dt.Rows[0]["VacationType"].ToString();
                Rule.WorkingHours = double.Parse(dt.Rows[0]["WorkingHours"].ToString());
                Rule.WeekendRuleId = int.Parse(dt.Rows[0]["WeekendRuleId"].ToString());
                Rule.CompanyName = dt.Rows[0]["CompanyName"].ToString();
                Rule.Weekend = WeekendRepo.Read(Rule.WeekendRuleId);
                return Rule;

            }
            catch (Exception e)
            {
                CrudRes.HasError = true;
                return null;
            }
        }

        public IEnumerable<VacationRule> Read(IEnumerable<long> ids)
        {
            CrudResponse CrudRes = new CrudResponse();
            IList<VacationRule> RuleList = new List<VacationRule>();
            try
            {
                foreach (long id in ids)
                {
                    VacationRule Rule = AllVacationRules.Find(p => p.Id == id);
                    RuleList.Add(Rule);
                    CrudRes.HasError = false;
                }
                return RuleList;
            }
            catch
            {
                CrudRes.HasError = true;
                return null;
            }
        }

        public CrudResponse Update(VacationRule Rule)
        {
            CrudResponse CrudRes = new CrudResponse();
            VacationRule TempRule = new VacationRule();
            try
            {
                TempRule = AllVacationRules.Find(p => p.Id == Rule.Id);
                TempRule = Rule;
                return CrudRes;
            }
            catch
            {
                CrudRes.HasError = true;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Update(IEnumerable<VacationRule> RuleList)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (VacationRule Rule in RuleList)
                {
                    CrudResponse CrudRes = new CrudResponse();
                    Update(Rule);
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }
    }
}
