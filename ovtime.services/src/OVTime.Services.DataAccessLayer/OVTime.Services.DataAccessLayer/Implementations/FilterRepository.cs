﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using OVTime.Services.DataAccessLayer.Interfaces;
using OVTime.Services.Models.External;

namespace OVTime.Services.DataAccessLayer.Implementations
{
    class FilterRepository : IDataRepository<OVReportFilter>
    {
        public SqlConnection GetOpenConnection()
        {
            string ConnectioString = Globals.connectiongString;
            SqlConnection Connection = new SqlConnection(ConnectioString);
            Connection.Open();
            return Connection;
        }
        List<OVReportFilter> AllFilters = new List<OVReportFilter>();
        public CrudResponse Create(OVReportFilter Filter)
        {
            CrudResponse CrudRes = new CrudResponse();
            try
            {
                AllFilters.Add(Filter);
                return CrudRes;
            }
            catch
            {
                CrudRes.HasError = true;
                CrudRes.HasInfo = true;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Create(IEnumerable<OVReportFilter> FilterList)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (OVReportFilter Filter in FilterList)
                {
                    AllFilters.Add(Filter);
                    CrudResponse CrudRes = new CrudResponse();
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }

        public CrudResponse Delete(long id)
        {
            CrudResponse CrudRes = new CrudResponse();
            OVReportFilter Filter = AllFilters.Find(p => p.Id == id);
            if (Filter != null)
            {
                try
                {
                    AllFilters.Remove(Filter);
                    CrudRes.HasError = false;
                    return CrudRes;
                }
                catch
                {
                    CrudRes.HasError = true;
                    return CrudRes;
                }
            }
            else
            {
                CrudRes.HasError = false;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Delete(IEnumerable<long> ids)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (long id in ids)
                {
                    OVReportFilter Filter = new OVReportFilter();
                    Filter = AllFilters.Find(p => p.Id == id);
                    AllFilters.Remove(Filter);
                    CrudResponse CrudRes = new CrudResponse();
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }

        public OVReportFilter Read(long id)
        {
            CrudResponse CrudRes = new CrudResponse();
            OVReportFilter Join = new OVReportFilter();
            try
            {
                Join = AllFilters.Find(p => p.Id == id);
                CrudRes.HasError = false;
                return Join;
            }
            catch
            {
                CrudRes.HasError = true;
                return null;
            }
        }

        public IEnumerable<OVReportFilter> Read(IEnumerable<long> ids)
        {
            CrudResponse CrudRes = new CrudResponse();
            IList<OVReportFilter> JoinList = new List<OVReportFilter>();
            try
            {
                foreach (long id in ids)
                {
                    OVReportFilter Filter = AllFilters.Find(p => p.Id == id);
                    JoinList.Add(Filter);
                    CrudRes.HasError = false;
                }
                return JoinList;
            }
            catch
            {
                CrudRes.HasError = true;
                return null;
            }
        }

        public CrudResponse Update(OVReportFilter Filter)
        {
            CrudResponse CrudRes = new CrudResponse();
            OVReportFilter TempFilter = new OVReportFilter();
            try
            {
                TempFilter = AllFilters.Find(p => p.Id == Filter.Id);
                TempFilter = Filter;
                return CrudRes;
            }
            catch
            {
                CrudRes.HasError = true;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Update(IEnumerable<OVReportFilter> FilterList)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (OVReportFilter Filter in FilterList)
                {
                    CrudResponse CrudRes = new CrudResponse();
                    Update(Filter);
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }
    }
}
