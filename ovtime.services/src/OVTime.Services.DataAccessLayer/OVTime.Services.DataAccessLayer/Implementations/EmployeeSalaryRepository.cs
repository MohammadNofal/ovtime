﻿using OVTime.Services.DataAccessLayer.DatabaseAccess;
using OVTime.Services.DataAccessLayer.Interfaces;
using OVTime.Services.Models.External;
using OVTime.Services.Models.Internal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace OVTime.Services.DataAccessLayer.Implementations
{
    public class EmployeeSalaryRepository : IDataRepository<EmployeeSalary>
    {
        List<EmployeeSalary> AllEmployeeSalaries = new List<EmployeeSalary>();

        public CrudResponse Create(EmployeeSalary EmpSal)// dummy data to be filled from database
        {
            CrudResponse CrudRes = new CrudResponse();
            try
            {
                AllEmployeeSalaries.Add(EmpSal);
                CrudRes.HasError = false;
                return CrudRes;
            }
            catch
            {
                CrudRes.HasError = true;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Create(IEnumerable<EmployeeSalary> EmpSalList)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (EmployeeSalary EmpSal in EmpSalList)
                {
                    AllEmployeeSalaries.Add(EmpSal);
                    CrudResponse CrudRes = new CrudResponse();
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }

        public CrudResponse Delete(long id)
        {
            CrudResponse CrudRes = new CrudResponse();
            EmployeeSalary EmpSal = AllEmployeeSalaries.Find(p => p.Id == id);
            if (EmpSal != null)
            {
                try
                {
                    AllEmployeeSalaries.Remove(EmpSal);
                    CrudRes.HasError = false;
                    return CrudRes;
                }
                catch
                {
                    CrudRes.HasError = true;
                    return CrudRes;
                }
            }
            else
            {
                CrudRes.HasError = false;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Delete(IEnumerable<long> ids)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (long id in ids)
                {
                    EmployeeSalary EmpSal = new EmployeeSalary();
                    EmpSal = AllEmployeeSalaries.Find(p => p.Id == id);
                    AllEmployeeSalaries.Remove(EmpSal);
                    CrudResponse CrudRes = new CrudResponse();
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }

        public EmployeeSalary Read (long id)
        {
            return null;
        }
        public EmployeeSalary Read(string UserId)
        {
            CrudResponse CrudRes = new CrudResponse();
            SqlCommand Command = new SqlCommand();
            EmployeeSalary Salary = new EmployeeSalary();
            try
            {
                DBAccess db = new DBAccess();
                string sqlQuery = string.Empty;
                sqlQuery = string.Format("Select * from OMV_EmployeeSalary where UserId ='{0}'", UserId.ToString());
                DataTable dt = new DataTable();
                dt = db.GetDataSet(sqlQuery).Tables[0];

                Salary.Id=  int.Parse(dt.Rows[0]["Id"].ToString());
                Salary.UserId= dt.Rows[0]["UserId"].ToString();
                Salary.BasicSalary = double.Parse(dt.Rows[0]["BasicSalary"].ToString());
                Salary.Transportation = double.Parse(dt.Rows[0]["Transportation"].ToString());
                Salary.Housing = double.Parse(dt.Rows[0]["Housing"].ToString());
                Salary.TotalSalary = double.Parse(dt.Rows[0]["TotalSalary"].ToString());
                Salary.VacationRuleId = int.Parse(dt.Rows[0]["VacationRuleId"].ToString());
                Salary.EndOfServiceRuleId = int.Parse(dt.Rows[0]["EndOfServiceRuleId"].ToString());

                return Salary;
            
            }
            catch (Exception e)
            {
                CrudRes.HasError = true;
                return null;
            }
        }

        public IEnumerable<EmployeeSalary> Read(IEnumerable<long> ids)
        {
            CrudResponse CrudRes = new CrudResponse();
            IList<EmployeeSalary> EmpSalList = new List<EmployeeSalary>();
            try
            {
                foreach (long id in ids)
                {
                    EmployeeSalary EmpSal = AllEmployeeSalaries.Find(p => p.Id == id);
                    EmpSalList.Add(EmpSal);
                    CrudRes.HasError = false;
                }
                return EmpSalList;
            }
            catch
            {
                CrudRes.HasError = true;
                return null;
            }
        }

        public CrudResponse Update(EmployeeSalary EmpSal)
        {
            CrudResponse CrudRes = new CrudResponse();
            EmployeeSalary TempEmpSal = new EmployeeSalary();
            try
            {
                TempEmpSal = AllEmployeeSalaries.Find(p => p.Id == EmpSal.Id);
                TempEmpSal = EmpSal;
                return CrudRes;
            }
            catch
            {
                CrudRes.HasError = true;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Update(IEnumerable<EmployeeSalary> EmpSalList)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (EmployeeSalary EmpSal in EmpSalList)
                {
                    CrudResponse CrudRes = new CrudResponse();
                    Update(EmpSal);
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }
    }
}
