﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using OVTime.Services.DataAccessLayer.Interfaces;
using OVTime.Services.Models.External;

namespace OVTime.Services.DataAccessLayer.Implementations
{
    public class JoinsRepository : IDataRepository<OVReportJoin>
    {
        List<OVReportJoin> AllJoins = new List<OVReportJoin>();
        public SqlConnection GetOpenConnection()
        {
            string ConnectioString = Globals.connectiongString;
            SqlConnection Connection = new SqlConnection(ConnectioString);
            Connection.Open();
            return Connection;
        }
        public CrudResponse Create(OVReportJoin Join)
        {
            CrudResponse CrudRes = new CrudResponse();
            try
            {
                AllJoins.Add(Join);
                return CrudRes;
            }
            catch
            {
                CrudRes.HasError = true;
                CrudRes.HasInfo = true;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Create(IEnumerable<OVReportJoin> JoinList)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (OVReportJoin Join in JoinList)
                {
                    AllJoins.Add(Join);
                    CrudResponse CrudRes = new CrudResponse();
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }

        public CrudResponse Delete(long id)
        {
            CrudResponse CrudRes = new CrudResponse();
            OVReportJoin Join = AllJoins.Find(p => p.Id == id);
            if (Join != null)
            {
                try
                {
                    AllJoins.Remove(Join);
                    CrudRes.HasError = false;
                    return CrudRes;
                }
                catch
                {
                    CrudRes.HasError = true;
                    return CrudRes;
                }
            }
            else
            {
                CrudRes.HasError = false;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Delete(IEnumerable<long> ids)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (long id in ids)
                {
                    OVReportJoin Join = new OVReportJoin();
                    Join = AllJoins.Find(p => p.Id == id);
                    AllJoins.Remove(Join);
                    CrudResponse CrudRes = new CrudResponse();
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }

        public OVReportJoin Read(long id)
        {
            CrudResponse CrudRes = new CrudResponse();
            OVReportJoin Join = new OVReportJoin();
            try
            {
                Join = AllJoins.Find(p => p.Id == id);
                CrudRes.HasError = false;
                return Join;
            }
            catch
            {
                CrudRes.HasError = true;
                return null;
            }
        }

        public IEnumerable<OVReportJoin> Read(IEnumerable<long> ids)
        {
            CrudResponse CrudRes = new CrudResponse();
            IList<OVReportJoin> JoinList = new List<OVReportJoin>();
            try
            {
                foreach (long id in ids)
                {
                    OVReportJoin Join = AllJoins.Find(p => p.Id == id);
                    JoinList.Add(Join);
                    CrudRes.HasError = false;
                }
                return JoinList;
            }
            catch
            {
                CrudRes.HasError = true;
                return null;
            }
        }

        public CrudResponse Update(OVReportJoin Join)
        {
            CrudResponse CrudRes = new CrudResponse();
            OVReportJoin TempJoin = new OVReportJoin();
            try
            {
                TempJoin = AllJoins.Find(p => p.Id == Join.Id);
                TempJoin = Join;
                return CrudRes;
            }
            catch
            {
                CrudRes.HasError = true;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Update(IEnumerable<OVReportJoin> JoinList)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (OVReportJoin Join in JoinList)
                {
                    CrudResponse CrudRes = new CrudResponse();
                    Update(Join);
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }
    }
}
