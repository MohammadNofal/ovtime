﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using OVTime.Services.DataAccessLayer.Interfaces;
using OVTime.Services.Models.External;

namespace OVTime.Services.DataAccessLayer.Implementations
{
    public class FilterTypeRepository : IDataRepository<OVFilterType>
    {
        public  SqlConnection GetOpenConnection()
        {
            string ConnectioString = Globals.connectiongString;
            SqlConnection Connection = new SqlConnection(ConnectioString);
            Connection.Open();
            return Connection;
        }
        List<OVFilterType> AllFilterTypes = new List<OVFilterType>();

        public CrudResponse Create(OVFilterType FilterType)// dummy data to be filled from database
        {
            CrudResponse CrudRes = new CrudResponse();
            try
            {
                AllFilterTypes.Add(FilterType);
                CrudRes.HasError = false;
                return CrudRes;
            }
            catch
            {
                CrudRes.HasError = true;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Create(IEnumerable<OVFilterType> FilterTypeList)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (OVFilterType FilterType in FilterTypeList)
                {
                    AllFilterTypes.Add(FilterType);
                    CrudResponse CrudRes = new CrudResponse();
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }

        public CrudResponse Delete(long id)
        {
            CrudResponse CrudRes = new CrudResponse();
            OVFilterType FilterType = AllFilterTypes.Find(p => p.Id == id);
            if (FilterType != null)
            {
                try
                {
                    AllFilterTypes.Remove(FilterType);
                    CrudRes.HasError = false;
                    return CrudRes;
                }
                catch
                {
                    CrudRes.HasError = true;
                    return CrudRes;
                }
            }
            else
            {
                CrudRes.HasError = false;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Delete(IEnumerable<long> ids)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (long id in ids)
                {
                    OVFilterType FilterType = new OVFilterType();
                    FilterType = AllFilterTypes.Find(p => p.Id == id);
                    AllFilterTypes.Remove(FilterType);
                    CrudResponse CrudRes = new CrudResponse();
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }

        public OVFilterType Read(long id)
        {
            CrudResponse CrudRes = new CrudResponse();
            OVFilterType FilterType = new OVFilterType();
            try
            {
                FilterType = AllFilterTypes.Find(p => p.Id == id);
                CrudRes.HasError = false;
                return FilterType;
            }
            catch
            {
                CrudRes.HasError = true;
                return null;
            }
        }

        public IEnumerable<OVFilterType> Read(IEnumerable<long> ids)
        {
            CrudResponse CrudRes = new CrudResponse();
            IList<OVFilterType> FilterTypeList = new List<OVFilterType>();
            try
            {
                foreach (long id in ids)
                {
                    OVFilterType FilterType = AllFilterTypes.Find(p => p.Id == id);
                    FilterTypeList.Add(FilterType);
                    CrudRes.HasError = false;
                }
                return FilterTypeList;
            }
            catch
            {
                CrudRes.HasError = true;
                return null;
            }
        }

        public CrudResponse Update(OVFilterType FilterType)
        {
            CrudResponse CrudRes = new CrudResponse();
            OVFilterType TempFilterType = new OVFilterType();
            try
            {
                TempFilterType = AllFilterTypes.Find(p => p.Id == FilterType.Id);
                TempFilterType = FilterType;                
                return CrudRes;
            }
            catch
            {
                CrudRes.HasError = true;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Update(IEnumerable<OVFilterType> FilterTypeList)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (OVFilterType FilterType in FilterTypeList)
                {
                    CrudResponse CrudRes = new CrudResponse();
                    Update(FilterType);
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }
    }
}
