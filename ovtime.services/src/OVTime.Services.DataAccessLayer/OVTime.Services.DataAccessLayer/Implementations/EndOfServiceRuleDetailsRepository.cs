﻿using OVTime.Services.DataAccessLayer.DatabaseAccess;
using OVTime.Services.DataAccessLayer.Interfaces;
using OVTime.Services.Models.External;
using OVTime.Services.Models.Internal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace OVTime.Services.DataAccessLayer.Implementations
{
    public class EndOfServiceRuleDetailsRepository : IDataRepository<EndOfServiceRuleDetails>
    {
        public CrudResponse Create(EndOfServiceRuleDetails model)
        {
            return null;
        }

        public EndOfServiceRuleDetails Read(long id)
        {
            return null;
        }

        public List<EndOfServiceRuleDetails> Read(int id)
        {
            DBAccess db = new DBAccess();
            string query = string.Format("Select * from OMV_EndOfServiceRuleDetails where EndOfServiceRuleId = {0} ", id);
            DataTable RuleDetailsTable = db.GetDataSet(query).Tables[0];

            List<EndOfServiceRuleDetails> RuleDetailsList = new List<EndOfServiceRuleDetails>();
            foreach (DataRow dr in RuleDetailsTable.Rows)
            {
                EndOfServiceRuleDetails RuleDetails = new EndOfServiceRuleDetails();
                RuleDetails.Id = int.Parse(dr["Id"].ToString());
                RuleDetails.EndOfServiceRuleId = int.Parse(dr["EndOfServiceRuleId"].ToString());
                RuleDetails.RuleFrom = double.Parse(dr["RuleFrom"].ToString());
                RuleDetails.RuleTo = dr["RuleTo"].ToString() == "" ? 0 : double.Parse(dr["RuleTo"].ToString());
                RuleDetails.SalaryPercentagePerYear = double.Parse(dr["SalaryPercentagPerYear"].ToString());
                RuleDetailsList.Add(RuleDetails);
            }

            return RuleDetailsList;
        }
        public CrudResponse Update(EndOfServiceRuleDetails model)
        {
            return null;
        }
        public CrudResponse Delete(long id)
        {
            return null;
        }
        public IEnumerable<CrudResponse> Create(IEnumerable<EndOfServiceRuleDetails> models)
        {
            return null;
        }
        public IEnumerable<EndOfServiceRuleDetails> Read(IEnumerable<long> ids)
        {
            return null;
        }
        public IEnumerable<CrudResponse> Update(IEnumerable<EndOfServiceRuleDetails> model)
        {
            return null;
        }
        public IEnumerable<CrudResponse> Delete(IEnumerable<long> ids)
        {
            return null;
        }
    }
}
