﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using OVTime.Services.DataAccessLayer.Interfaces;
using OVTime.Services.Models.External;

namespace OVTime.Services.DataAccessLayer.Implementations
{
    public class ReportRepository : IDataRepository<OVReport>
    {
        List<OVReport> AllReports = new List<OVReport>();

        public SqlConnection GetOpenConnection()
        {
            string ConnectioString = Globals.connectiongString;
            SqlConnection Connection = new SqlConnection(ConnectioString);
            Connection.Open();
            return Connection;
        }
        public CrudResponse Create(OVReport Report)
        {
            CrudResponse CrudRes = new CrudResponse();
            try
            {
                AllReports.Add(Report);
                return CrudRes;
            }
            catch
            {
                CrudRes.HasError = true;
                CrudRes.HasInfo = true;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Create(IEnumerable<OVReport> ReportList)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (OVReport Report in ReportList)
                {
                    AllReports.Add(Report);
                    CrudResponse CrudRes = new CrudResponse();
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }

        public CrudResponse Delete(long id)
        {
            CrudResponse CrudRes = new CrudResponse();
            OVReport Report = AllReports.Find(p => p.Id == id);
            if (Report != null)
            {
                try
                {
                    AllReports.Remove(Report);
                    CrudRes.HasError = false;
                    return CrudRes;
                }
                catch
                {
                    CrudRes.HasError = true;
                    return CrudRes;
                }
            }
            else
            {
                CrudRes.HasError = false;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Delete(IEnumerable<long> ids)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (long id in ids)
                {
                    OVReport Report = new OVReport();
                    Report = AllReports.Find(p => p.Id == id);
                    AllReports.Remove(Report);
                    CrudResponse CrudRes = new CrudResponse();
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }

        public OVReport Read(long id)
        {
            CrudResponse CrudRes = new CrudResponse();
            OVReport Report = new OVReport();
            try
            {
                Report = AllReports.Find(p => p.Id == id);
                CrudRes.HasError = false;
                return Report;
            }
            catch
            {
                CrudRes.HasError = true;
                return null;
            }
        }

        public IEnumerable<OVReport> Read(IEnumerable<long> ids)
        {
            CrudResponse CrudRes = new CrudResponse();
            IList<OVReport> ReportList = new List<OVReport>();
            try
            {
                foreach (long id in ids)
                {
                    OVReport Report = AllReports.Find(p => p.Id == id);
                    ReportList.Add(Report);
                    CrudRes.HasError = false;
                }
                return ReportList;
            }
            catch
            {
                CrudRes.HasError = true;
                return null;
            }
        }

        public CrudResponse Update(OVReport Report)
        {
            CrudResponse CrudRes = new CrudResponse();
            OVReport TempReport = new OVReport();
            try
            {
                TempReport = AllReports.Find(p => p.Id == Report.Id);
                TempReport = Report;

                return CrudRes;
            }
            catch
            {
                CrudRes.HasError = true;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Update(IEnumerable<OVReport> ReportList)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (OVReport Report in ReportList)
                {
                    CrudResponse CrudRes = new CrudResponse();
                    Update(Report);
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }
    }
}
