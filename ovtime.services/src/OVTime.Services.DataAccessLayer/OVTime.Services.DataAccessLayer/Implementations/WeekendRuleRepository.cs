﻿using OVTime.Services.DataAccessLayer.DatabaseAccess;
using OVTime.Services.DataAccessLayer.Interfaces;
using OVTime.Services.Models.External;
using OVTime.Services.Models.Internal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace OVTime.Services.DataAccessLayer.Implementations
{
    public class WeekendRuleRepository : IDataRepository<WeekendRule>
    {
        public SqlConnection GetOpenConnection()
        {
            string ConnectioString = Globals.connectiongString;
            SqlConnection Connection = new SqlConnection(ConnectioString);
            Connection.Open();
            return Connection;
        }
        List<WeekendRule> AllWeekendRules = new List<WeekendRule>();

        public CrudResponse Create(WeekendRule Rule)// dummy data to be filled from database
        {
            CrudResponse CrudRes = new CrudResponse();
            try
            {
                AllWeekendRules.Add(Rule);
                CrudRes.HasError = false;
                return CrudRes;
            }
            catch
            {
                CrudRes.HasError = true;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Create(IEnumerable<WeekendRule> RuleList)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (WeekendRule Rule in RuleList)
                {
                    AllWeekendRules.Add(Rule);
                    CrudResponse CrudRes = new CrudResponse();
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }

        public CrudResponse Delete(long id)
        {
            CrudResponse CrudRes = new CrudResponse();
            WeekendRule Rule = AllWeekendRules.Find(p => p.Id == id);
            if (Rule != null)
            {
                try
                {
                    AllWeekendRules.Remove(Rule);
                    CrudRes.HasError = false;
                    return CrudRes;
                }
                catch
                {
                    CrudRes.HasError = true;
                    return CrudRes;
                }
            }
            else
            {
                CrudRes.HasError = false;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Delete(IEnumerable<long> ids)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (long id in ids)
                {
                    WeekendRule Rule = new WeekendRule();
                    Rule = AllWeekendRules.Find(p => p.Id == id);
                    AllWeekendRules.Remove(Rule);
                    CrudResponse CrudRes = new CrudResponse();
                    CrudRes.HasError = false;
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }

        public WeekendRule Read(long id)
        {
            CrudResponse CrudRes = new CrudResponse();
            SqlCommand Command = new SqlCommand();
            WeekendRule Rule = new WeekendRule();
            try
            {
                DBAccess db = new DBAccess();
                string sqlQuery = string.Empty;
                sqlQuery = string.Format("Select * from OMV_WeekendRule where Id ={0}", id.ToString());
                DataTable dt = new DataTable();
                dt = db.GetDataSet(sqlQuery).Tables[0];

                Rule.Id = int.Parse(dt.Rows[0]["Id"].ToString());
                Rule.DayOff1 = (dt.Rows[0]["DayOff1"].ToString()=="" ?null: dt.Rows[0]["DayOff1"].ToString());
                Rule.DayOff2 = (dt.Rows[0]["DayOff2"].ToString() == "" ? null : dt.Rows[0]["DayOff2"].ToString());
                Rule.DayOff3 = (dt.Rows[0]["DayOff3"].ToString() == "" ? null : dt.Rows[0]["DayOff3"].ToString());
                Rule.DayOff4 = (dt.Rows[0]["DayOff4"].ToString() == "" ? null : dt.Rows[0]["DayOff4"].ToString());
                Rule.DayOff5 = (dt.Rows[0]["DayOff5"].ToString() == "" ? null : dt.Rows[0]["DayOff5"].ToString());
                Rule.DayOff6 = (dt.Rows[0]["DayOff6"].ToString() == "" ? null : dt.Rows[0]["DayOff6"].ToString());
                Rule.DayOff7 = (dt.Rows[0]["DayOff7"].ToString() == "" ? null : dt.Rows[0]["DayOff7"].ToString());
                Rule.StartDay = dt.Rows[0]["StartDay"].ToString();
                Rule.DaysOffCount =int.Parse(dt.Rows[0]["DaysOffCount"].ToString());

                return Rule;

            }
            catch (Exception e)
            {
                CrudRes.HasError = true;
                return null;
            }
        }

        public IEnumerable<WeekendRule> Read(IEnumerable<long> ids)
        {
            CrudResponse CrudRes = new CrudResponse();
            IList<WeekendRule> RuleList = new List<WeekendRule>();
            try
            {
                foreach (long id in ids)
                {
                    WeekendRule Rule = AllWeekendRules.Find(p => p.Id == id);
                    RuleList.Add(Rule);
                    CrudRes.HasError = false;
                }
                return RuleList;
            }
            catch
            {
                CrudRes.HasError = true;
                return null;
            }
        }

        public CrudResponse Update(WeekendRule Rule)
        {
            CrudResponse CrudRes = new CrudResponse();
            WeekendRule TempRule = new WeekendRule();
            try
            {
                TempRule = AllWeekendRules.Find(p => p.Id == Rule.Id);
                TempRule = Rule;
                return CrudRes;
            }
            catch
            {
                CrudRes.HasError = true;
                return CrudRes;
            }
        }

        public IEnumerable<CrudResponse> Update(IEnumerable<WeekendRule> RuleList)
        {
            IList<CrudResponse> CrudResList = new List<CrudResponse>();
            try
            {
                foreach (WeekendRule Rule in RuleList)
                {
                    CrudResponse CrudRes = new CrudResponse();
                    Update(Rule);
                    CrudResList.Add(CrudRes);
                }
                return CrudResList;
            }
            catch
            {
                CrudResponse CrudRes = new CrudResponse();
                CrudRes.HasError = true;
                CrudResList.Add(CrudRes);
                return CrudResList;
            }
        }
    }
}
