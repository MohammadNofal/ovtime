﻿using OVTime.Services.Core.Implementations;
using OVTime.Services.DataAccessLayer.DatabaseAccess;
using OVTime.Services.DataAccessLayer.Interfaces;
using OVTime.Services.Models.External;
using OVTime.Services.Models.Internal;
using System;
using System.Collections.Generic;
using System.Text;

namespace OVTime.Services.DataAccessLayer.Implementations
{
    public class EndOfServiceRepository : IDataRepository<EndOfService>
    {
        public CrudResponse Create(EndOfService model)
        {
            return null;
        }
        public EndOfService Read(long id)
        {
            return null;
        }
        public EndOfService Read(string UserId)
        {
            DBAccess db = new DBAccess();
            UserRepository UserRepo = new UserRepository();
            EmployeeSalaryRepository SalaryRepo = new EmployeeSalaryRepository();
            EndOfServiceRuleRepository RuleRepo = new EndOfServiceRuleRepository();
            EndOfServiceRuleDetailsRepository RuleDetailsRepo = new EndOfServiceRuleDetailsRepository();

            User Employee = UserRepo.Read(UserId);
            EmployeeSalary Salary = SalaryRepo.Read(UserId);
            EndOfServiceRule Rule = RuleRepo.Read(Salary.EndOfServiceRuleId);
            List<EndOfServiceRuleDetails> RuleDetailsList = RuleDetailsRepo.Read(Salary.EndOfServiceRuleId);

            EndOfServiceCalculations Calc = new EndOfServiceCalculations();
            double CashLiabilityForYears = Calc.CalculateEndOfService(Employee, Salary, Rule, RuleDetailsList);

            double CashLiabilityForUnusedVacations = 0;

            UserVacationRepository VacationRepo = new UserVacationRepository();
            if (Rule.IncludeThistYearVacations == 1 || Rule.IncludePreviousYearsVacations == 1)
            {
                VacationHistory vacations = VacationRepo.Read(UserId);
                if (Rule.IncludeThistYearVacations == 1)
                    CashLiabilityForUnusedVacations += vacations.ThisYear.VacationCashLiability;
                if (Rule.IncludePreviousYearsVacations == 1)
                    CashLiabilityForUnusedVacations += vacations.PreviousYears.VacationCashLiability;
            }

            EndOfService EndOfService = new EndOfService();
            EndOfService.EmployeeId = UserId;
            EndOfService.HireDate = Employee.StartDate.ToString("MM/dd/yyyy");

            EndOfService.CashLiability = Math.Round(CashLiabilityForYears + CashLiabilityForUnusedVacations ,2);

            return EndOfService;
        }
        public CrudResponse Update(EndOfService model)
        {
            return null;
        }
        public CrudResponse Delete(long id)
        {
            return null;
        }
        public IEnumerable<CrudResponse> Create(IEnumerable<EndOfService> models)
        {
            return null;
        }
        public IEnumerable<EndOfService> Read(IEnumerable<long> ids)
        {
            return null;
        }
        public IEnumerable<CrudResponse> Update(IEnumerable<EndOfService> model)
        {
            return null;
        }
        public IEnumerable<CrudResponse> Delete(IEnumerable<long> ids)
        {
            return null;
        }
    }
}
