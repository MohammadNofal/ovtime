﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OVTime.Services.Models.Internal;
using OVTime.Services.DataAccessLayer.Implementations;

namespace OVTime.Services.BackendServiceDispatcher
{
    public class Program
    {
        public static void Main(string[] args)
        {
           // UserVacationRepository VacationRepo = new UserVacationRepository();
           // UserRepository USerRepo = new UserRepository();
           // EmployeeSalaryRepository SalaryRepo = new EmployeeSalaryRepository();
           // VacationRuleRepository RuleRepo = new VacationRuleRepository();

           // int RuleId = 1;
           // VacationRule rule = RuleRepo.Read(RuleId);

           // UserVacation Vacation = new UserVacation();
           // Vacation.Id = 6;

           // User Emp = new User();
           // Emp.Id = "ABBAS.ALSAFFAR";

           // Emp = USerRepo.Read(Emp.Id);

           // CalculatedVacations EmpVacations = new CalculatedVacations();
           // EmpVacations = VacationRepo.Read(Emp.Id);
           //// Emp.Id = "ABBAS.ALCHARIF";
           // EmployeeSalary EmployeeSalary = SalaryRepo.Read(Emp.Id);


           // Vacation.VacationDate = DateTime.Now;
           // Vacation.SickLeave = 0;
           // Vacation.UserId = "ABBAS.ALCHARIF";
           // VacationRepo.Create(Vacation);
           // VacationRepo.Delete(Vacation.Id);
            
            
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();
    }
}
