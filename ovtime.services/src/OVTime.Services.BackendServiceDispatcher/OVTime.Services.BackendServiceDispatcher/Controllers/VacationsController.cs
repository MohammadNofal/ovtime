﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OVTime.Services.DataAccessLayer.Implementations;
using OVTime.Services.Models.Internal;

namespace OVTime.Services.BackendServiceDispatcher.Controllers
{
    [Route("api/Vacations")]
    public class VacationsController : Controller
    {
        // GET: api/Vacations
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Vacations/5
        [HttpGet("{id}")]
        public string Get(string id)


        {
            UserVacationRepository Repo = new UserVacationRepository();
            VacationHistory VacationsObject = Repo.Read(id);
            return JsonConvert.SerializeObject(VacationsObject);
        }
        
        // POST: api/Vacations
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
        
        // PUT: api/Vacations/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
