﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OVTime.Services.DataAccessLayer.Implementations;
using OVTime.Services.Models.Internal;

namespace OVTime.Services.BackendServiceDispatcher.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalaryController : ControllerBase
    {
        // GET: api/Salary
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Salary/5
        [HttpGet("{id}")]
        public string Get(string id)
        {
            EmployeeSalaryRepository Repo = new EmployeeSalaryRepository();
            EmployeeSalary Salary = Repo.Read(id);            
            return JsonConvert.SerializeObject(Salary);
        }

        // POST: api/Salary
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Salary/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
