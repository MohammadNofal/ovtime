﻿using OVTime.Services.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace OVTime.Services.Models.External
{
    public class OVReportJoin : ModelBase
    {
        public long Id { get; set; }
        public long ReportId { get; set; }
        public string Table1 { get; set; }
        public string Table2 { get; set; }
        public string AliasTable1 { get; set; }
        public string AliasTable2 { get; set; }
        public string JoinType { get; set; }
        public string JoinTable1Key { get; set; }
        public string JoinTable2Key { get; set; }
    }
}
