﻿using OVTime.Services.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace OVTime.Services.Models.External
{
    public class OVReportFilter : ModelBase
    {
        public long Id { get; set; }
        public string FilterName { get; set; }
        public string DisplayName { get; set; }
        public long ReportId { get; set; }
        public string TableName{ get; set; }
        public long FilterTypeId { get; set; }
        public string FilterOperation { get; set; }
        public string FilterValue { get; set; }
        public string AliasTableName { get; set; }
    }
}
