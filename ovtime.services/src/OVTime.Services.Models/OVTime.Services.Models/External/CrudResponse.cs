﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OVTime.Services.Models.External
{
    public class CrudResponse : Exception
    {
        public bool HasInfo { get; set; }
        public bool HasWarning { get; set; }
        public bool HasError { get; set; }
        public long ReferenceId { get; set; }
        public IEnumerable<string> Infos { get; set; }
        public IEnumerable<string> Warnings { get; set; }
        public IEnumerable<string> Errors { get; set; }
    }
}
