﻿using OVTime.Services.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace OVTime.Services.Models.External
{
    public class OVFilterType : ModelBase
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
