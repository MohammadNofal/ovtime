﻿using OVTime.Services.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace OVTime.Services.Models.External
{
    public class OVReportColumn : ModelBase
    {
        public long Id { get; set; }
        public long ReportId { get; set; }
        public string ColumnName { get; set; }
        public string DisplayName { get; set; }
        public string TableName { get; set; }
        public string AliasTableName { get; set; }
        public string Formula { get; set; }
    }
}
