﻿using OVTime.Services.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace OVTime.Services.Models.Internal
{
    public class UserVacation : ModelBase
    {
        public int Id { get; set; }
        public DateTime VacationDate { get; set; }
        public string UserId { get; set; }
        public int SickLeave { get; set; }
    }
}
