﻿using OVTime.Services.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace OVTime.Services.Models.Internal
{
    public class VacationRule : ModelBase
    {
        public int Id { get; set; }
        public double VacationsPerYear { get; set; }
        public double SickLeavePerYear { get; set; }
        public int IncludeFirstYear { get; set;} // True if the first year is included in the vacations
        public string VacationType { get; set; } // if vacations calculated as calender days (c) or as working days (w)
        public double WorkingHours { get; set; } // one day vacation means (8 or 9 or..) hours
        public int WeekendRuleId { get; set; }
        public string CompanyName { get; set; }
        public WeekendRule Weekend { get; set; }
        
    }
}
