﻿using OVTime.Services.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace OVTime.Services.Models.Internal
{
    public class WeekendRule: ModelBase
    {
        public int Id { get; set; }
        public string DayOff1 { get; set; }
        public string DayOff2 { get; set; }
        public string DayOff3{ get; set; }
        public string DayOff4 { get; set; }
        public string DayOff5 { get; set; }
        public string DayOff6 { get; set; }
        public string DayOff7 { get; set; }
        public string StartDay { get; set; }
        public int DaysOffCount { get; set; }

    }
}
