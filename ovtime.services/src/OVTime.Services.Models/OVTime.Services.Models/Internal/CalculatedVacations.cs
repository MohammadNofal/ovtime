﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OVTime.Services.Models.Internal
{
    public class CalculatedVacations
    {
        public string UserId { get; set; }
        public string HiredDate { get; set; }
        public double EarnedVacations { get; set;}
        public double UsedVacations { get; set; }
        public double RemainingVacations { get; set; }
        //public double TotalSickLeaves { get; set; }
        public double UsedSickLeaves { get; set; }
        public double RemainingSickLeaves { get; set; }
        public double VacationCashLiability { get; set; }
        public double TotalSalary { get; set; }
        public List<VacationDetails> VacationsDetails { get; set; }
    }
}
