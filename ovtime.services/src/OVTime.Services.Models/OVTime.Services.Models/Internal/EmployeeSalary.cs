﻿using OVTime.Services.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace OVTime.Services.Models.Internal
{
    public class EmployeeSalary : ModelBase
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public double BasicSalary { get; set; }
        public double Transportation { get; set; }
        public double Housing { get; set; }
        public double TotalSalary { get; set; }
        public int VacationRuleId { get; set; }
        public int EndOfServiceRuleId { get; set; }
    }
}
