﻿using OVTime.Services.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace OVTime.Services.Models.Internal
{
    public class VacationHistory :ModelBase
    {
        public CalculatedVacations ThisYear { get; set; }
        public CalculatedVacations PreviousYears { get; set; }
    }
}
