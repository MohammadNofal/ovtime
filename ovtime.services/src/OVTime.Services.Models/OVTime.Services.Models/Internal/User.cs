﻿using OVTime.Services.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace OVTime.Services.Models.Internal
{
    public class User : ModelBase
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public long ManagerId { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public long UserAccessId { get; set; }
        public DateTime StartDate { get; set; }
    }
}
