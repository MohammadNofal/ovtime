﻿using OVTime.Services.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace OVTime.Services.Models.Internal
{
    public class EndOfServiceRuleDetails : ModelBase 
    {
        public int Id { get; set; }
        public int EndOfServiceRuleId { get; set; }
        public double RuleFrom { get; set; }
        public double? RuleTo { get; set; }
        public double SalaryPercentagePerYear { get; set; }
    }
}
