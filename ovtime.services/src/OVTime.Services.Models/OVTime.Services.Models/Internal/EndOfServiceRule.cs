﻿using OVTime.Services.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace OVTime.Services.Models.Internal
{
    public class EndOfServiceRule : ModelBase
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public int IncludeThistYearVacations { get; set; }
        public int IncludePreviousYearsVacations { get; set; }
        public string CalculationBased { get; set; }
    }
}
