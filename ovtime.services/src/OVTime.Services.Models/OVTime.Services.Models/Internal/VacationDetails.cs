﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OVTime.Services.Models.Internal
{
    public class VacationDetails
    {
        public string VacationDate { get; set; }
        public string VacationDay { get; set; }
        public int VacationHours { get; set; }
        public string HolidayBefore { get; set; }
        public string HolidayAfter { get; set; }
        public string VacationType { get; set; }
    }
}
