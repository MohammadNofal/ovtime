﻿using OVTime.Services.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace OVTime.Services.Models.Internal
{
    public class EndOfService : ModelBase
    {
        public string EmployeeId { get; set; }
        public string HireDate { get; set; }
        public double CashLiability { get; set; }
    }
}
