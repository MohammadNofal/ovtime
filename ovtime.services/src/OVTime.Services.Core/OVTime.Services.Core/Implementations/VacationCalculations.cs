﻿using OVTime.Services.Models.Internal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace OVTime.Services.Core.Implementations
{
    public class VacationCalculations
    {
        public double CalculateEarnedVacations(User Employee, VacationRule Rule)
        {
            double WorkingDaysForThisYear = 0;
            double WorkingDaysFromHiring = (DateTime.Today - Employee.StartDate).TotalDays;

            if (WorkingDaysFromHiring < 365)
                if (Rule.IncludeFirstYear == 0)
                {
                    WorkingDaysForThisYear = 0;
                }
                else
                {
                    WorkingDaysForThisYear = WorkingDaysFromHiring;
                }
            else
            {
                CommonCalculations CommonCalc = new CommonCalculations();
                WorkingDaysForThisYear = CommonCalc.GetCountOfDaystOFThisYear(Employee);
            }

            double VacationPerDay = Rule.VacationsPerYear / 365;
            double Vacations = VacationPerDay * WorkingDaysForThisYear;            
            return Vacations;
        }        

        public CalculatedVacations CalculateEmployeeVacations(User Employee, DataTable AllTakenVacations, VacationRule Rule, double TotalVacations, EmployeeSalary Salary, WeekendRule Weekend, bool PreviousYearsFlag)
        {
            double TakenVacationHours = 0;
            double TakenNormalVacations = 0;
            double SickLeaves = 0;
            CommonCalculations CommonCalc = new CommonCalculations();
            int CountOfPreviousYears = CommonCalc.GetCountOfPreviousYears(Employee);

            foreach (DataRow dr in AllTakenVacations.Rows)
            {
                if (dr["SickLeave"].ToString() == "0")
                    if (double.Parse(dr["Hours"].ToString()) == Rule.WorkingHours)
                    {
                        if (CheckIfVacationBeforeIsWeekend(DateTime.Parse(dr["VacationDate"].ToString()), Employee.Id, Weekend, AllTakenVacations) == true )
                            TakenNormalVacations += 3;
                        else
                           if (CheckIfVacationBeforeIsWeekend(DateTime.Parse(dr["VacationDate"].ToString()), Employee.Id, Weekend, AllTakenVacations) == false)
                            TakenNormalVacations++;
                    }
                    else
                    {
                        TakenVacationHours += double.Parse(dr["Hours"].ToString());
                    }
                else
                    SickLeaves++;
            }

            TakenNormalVacations += Math.Round(TakenVacationHours / Rule.WorkingHours, 2);

            CalculatedVacations CalcVacations = new CalculatedVacations();
            CalcVacations.UserId = Employee.Id;
            CalcVacations.HiredDate = Employee.StartDate.ToString("MM/dd/yyyy");
            CalcVacations.EarnedVacations = Math.Round(TotalVacations, 2);
            CalcVacations.UsedVacations = Math.Round(TakenNormalVacations, 2);
            CalcVacations.RemainingVacations = Math.Round(TotalVacations - TakenNormalVacations, 2);
            //CalcVacations.TotalSickLeaves = Math.Round(Rule.SickLeavePerYear, 2);
            CalcVacations.UsedSickLeaves = Math.Round(SickLeaves, 2);
            CalcVacations.RemainingSickLeaves = PreviousYearsFlag == false ? Math.Round(Rule.SickLeavePerYear - SickLeaves, 2) : Math.Round(CountOfPreviousYears * Rule.SickLeavePerYear - SickLeaves, 2);
            CalcVacations.TotalSalary = Math.Round(Salary.TotalSalary, 2);
            CalcVacations.VacationCashLiability = Math.Round((Salary.TotalSalary * 12 / 365) * CalcVacations.RemainingVacations, 2);
            List<VacationDetails> VacationDetailsList = new List<VacationDetails>();
            foreach (DataRow dr in AllTakenVacations.Rows)
            {
                VacationDetails Vacation = new VacationDetails();
                Vacation.VacationDate = DateTime.Parse(dr["VacationDate"].ToString()).ToString("MM/dd/yyyy");
                Vacation.VacationDay = DateTime.Parse(dr["VacationDate"].ToString()).DayOfWeek.ToString();
                Vacation.VacationHours = int.Parse(dr["Hours"].ToString());
                Vacation.HolidayBefore = DateTime.Parse(dr["HolidayBefore"].ToString()).ToString("MM/dd/yyyy");
                Vacation.HolidayAfter = DateTime.Parse(dr["HolidayAfter"].ToString()).ToString("MM/dd/yyyy");
                Vacation.VacationType = dr["SickLeave"].ToString() == "0"?"Vacation":"Sick Leave";
                VacationDetailsList.Add(Vacation);
            }
            CalcVacations.VacationsDetails=VacationDetailsList;
            return CalcVacations;

        }

        public bool CheckIfVacationBeforeIsWeekend (DateTime VacationDate,string UserId,WeekendRule Weekend, DataTable AllTakenVacations)
        {
            bool DayBeforeIsWeekwend = false;
            if (Weekend == null)
                DayBeforeIsWeekwend= false;
            else
            {
                if (VacationDate.DayOfWeek.ToString() == Weekend.StartDay)
                {
                    DateTime DayBeforeVacation = VacationDate.AddDays(-Weekend.DaysOffCount-1);                    
                    foreach (DataRow dr in AllTakenVacations.Rows)
                    {
                        if (DateTime.Parse(dr["VacationDate"].ToString()).ToString("MM/dd/yyy") == DayBeforeVacation.ToString("MM/dd/yyy"))
                        {
                            DayBeforeIsWeekwend = true;
                            break;
                        }
                        else
                            DayBeforeIsWeekwend = false;
                    }
                }
            }
            return DayBeforeIsWeekwend;
        }

        public double CalculatePreviousYearsTotalVacations(User Employee ,VacationRule Rule)
        {
            CommonCalculations CommonCalc = new CommonCalculations();
            int PreviousYears =CommonCalc.GetCountOfPreviousYears(Employee);
            double EarnedPreviousYearsVacations = PreviousYears * Rule.VacationsPerYear;
            return EarnedPreviousYearsVacations;
        }
    }
}
