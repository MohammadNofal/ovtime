﻿using OVTime.Services.Models.Internal;
using System;
using System.Collections.Generic;
using System.Text;

namespace OVTime.Services.Core.Implementations
{
    public class EndOfServiceCalculations
    {
        public double CalculateEndOfService(User Employee, EmployeeSalary Salary,EndOfServiceRule Rule, List<EndOfServiceRuleDetails> RuleDetailsList)
        {
            CommonCalculations CommonCalc = new CommonCalculations();
            int Years = CommonCalc.GetCountOfPreviousYears(Employee);
            double Days = CommonCalc.GetCountOfDaystOFThisYear(Employee);            
            double CashLiability = 0;
            double ServiceDuration = Years + Days / 365;
            string CalculationBased = Rule.CalculationBased;
            foreach(EndOfServiceRuleDetails Detail in RuleDetailsList)
            {
                if (ServiceDuration >= Detail.RuleFrom && ServiceDuration < Detail.RuleTo)
                    CashLiability = ServiceDuration * Detail.SalaryPercentagePerYear * (CalculationBased=="BasicSalary"? Salary.BasicSalary:Salary.TotalSalary);
                else
                    if (ServiceDuration >= Detail.RuleFrom && Detail.RuleTo==null)
                    CashLiability = ServiceDuration * Detail.SalaryPercentagePerYear * (CalculationBased == "BasicSalary" ? Salary.BasicSalary : Salary.TotalSalary);

            }
            return CashLiability;
        }
    }
}
