﻿using OVTime.Services.Models.Internal;
using System;
using System.Collections.Generic;
using System.Text;

namespace OVTime.Services.Core.Implementations
{
    public class CommonCalculations
    {
        public double GetCountOfDaystOFThisYear(User Employee)
        {
            DateTime StartDate = Employee.StartDate;
            int StartDay = StartDate.Day;
            int StartMonth = StartDate.Month;
            int StartYear = StartDate.Year;

            int CurrentDay = DateTime.Today.Day;
            int CurrentMonth = DateTime.Today.Month;
            int CurrentYear = DateTime.Today.Year;

            double TotalDays = 0;


            if (CurrentMonth > StartMonth)
            {
                TotalDays = (DateTime.Today - DateTime.Parse(StartMonth.ToString() + "/" + StartDay.ToString() + "/"+CurrentYear.ToString())).TotalDays;
            }
            else
            if (CurrentMonth < StartMonth)
            {
                TotalDays = (DateTime.Today - DateTime.Parse(StartMonth.ToString() + "/" + StartDay.ToString() + "/" + (CurrentYear-1).ToString())).TotalDays;

            }
            else // if he started at the same month
            {
                if (CurrentDay > StartDay)
                    TotalDays = (DateTime.Today - DateTime.Parse(StartMonth.ToString() + "/" + StartDay.ToString() + "/" + CurrentYear.ToString())).TotalDays;
                else
                TotalDays = (DateTime.Today - DateTime.Parse(StartMonth.ToString() + "/" + StartDay.ToString() + "/" + (CurrentYear-1).ToString())).TotalDays;
            }
            return TotalDays;
        }

        public int GetCountOfPreviousYears(User Employee)
        {
            int StartMonth = Employee.StartDate.Month;
            int StartDay = Employee.StartDate.Day;
            int CurrentMonth = DateTime.Now.Month;
            int CurrentDay = DateTime.Now.Day;

            int PreviousYears = 0;

            if (CurrentMonth > StartMonth)
                PreviousYears = DateTime.Now.Year - Employee.StartDate.Year;
            else
                if (CurrentMonth < StartMonth)
                PreviousYears = DateTime.Now.Year - 1 - Employee.StartDate.Year;
            else
                if (CurrentDay > StartDay)
                PreviousYears = DateTime.Now.Year - Employee.StartDate.Year;
            else
                PreviousYears = DateTime.Now.Year - 1 - Employee.StartDate.Year;

            return PreviousYears;
        }
    }
}
