﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WcfOmniVistaTimeRESTfulJSonService.Data
{
    public class ProjectSummary
    {
        [DataMember]
        public string projectId { get; set; }
        [DataMember]
        public double ProjectHrs { get; set; }
       
    }
}