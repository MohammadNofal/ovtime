﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WcfOmniVistaTimeRESTfulJSonService
{
    public class AllocationSummary
    {
        [DataMember]
        public string MonthNumber { get; set; }
        [DataMember]
        public string MonthText { get; set; }
        [DataMember]
        public string ProjectIdText1 { get; set; }
        [DataMember]
        public string ProjectIdText2 { get; set; }
        [DataMember]
        public string ProjectIdText3 { get; set; }
        [DataMember]
        public string ProjectIdText4 { get; set; }
        [DataMember]
        public string ProjectIdValue1 { get; set; }
        [DataMember]
        public string ProjectIdValue2 { get; set; }
        [DataMember]
        public string ProjectIdValue3 { get; set; }
        [DataMember]
        public string ProjectIdValue4 { get; set; }
    }
}