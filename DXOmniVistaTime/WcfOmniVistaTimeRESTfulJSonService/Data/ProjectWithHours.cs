﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WcfOmniVistaTimeRESTfulJSonService.Data
{
    public class ProjectWithHours
    {
        [DataMember]
        public string Project { get; set; }
        [DataMember]
        public double ProjectHours { get; set; }

    }
}