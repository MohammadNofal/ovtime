﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WcfOmniVistaTimeRESTfulJSonService
{
    public class PtoSummary
    {
        [DataMember]
        public string EmployeeID { get; set; }
        [DataMember]
        public string ColumnText { get; set; }
        [DataMember]
        public string ColumnValue { get; set; }
        
    }
}