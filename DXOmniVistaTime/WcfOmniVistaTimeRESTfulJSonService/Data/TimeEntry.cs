﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WcfOmniVistaTimeRESTfulJSonService
{
    public class TimeEntry
    {
        [DataMember]
        public string EmployeeID { get; set; }
        [DataMember]
        public string ProjectID { get; set; }
        [DataMember]
        public string ProjectName { get; set; }
        [DataMember]
        public string TotalHours { get; set; }
    }
}