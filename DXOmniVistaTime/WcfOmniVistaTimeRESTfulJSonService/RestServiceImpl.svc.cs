﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using WcfOmniVistaTimeRESTfulJSonService.Data;

namespace WcfOmniVistaTimeRESTfulJSonService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RestServiceImpl" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select RestServiceImpl.svc or RestServiceImpl.svc.cs at the Solution Explorer and start debugging.
    [JavascriptCallbackBehavior(UrlParameterName = "callback")]
    public class RestServiceImpl : IRestServiceImpl
    {
        #region IRestService Members
        public string XMLData(string id)
        {
            return "You reqquested product " + id;
        }

        public string JSONData(string id)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            return serializer.Serialize("You reqquested product " + id);
        }

        public string GetData(string id)
        {
            return JsonDataAccess.ConvertDataTabletoString(id);
        }

        public TimeEntry[] GetTimeEntry(string id)
        {
            return JsonDataAccess.GetTimeEntries(id).ToArray();
        }

        public PtoSummary[] GetPtoSummary(string id)
        {
            return JsonDataAccess.GetPtoSummary(id).ToArray();
        }

        public AllocationSummary[] GetAllocationSummary(string id)
        {
            return JsonDataAccess.GetAllocationSummary(id).ToArray();
        }

        public TimeEntry[] GetTimeEntry1()
        {
            return JsonDataAccess.GetTimeEntries("baher.yousef").ToArray();
        }

        public List<ProjectSummary> GetProjectsSummary()
        {
            return JsonDataAccess.GetProjectsSummary();
        }

        public List<ProjectWithHours> GetProjectsWithHours(string id)
        {
            return JsonDataAccess.GetProjectsWithHours(id);
        }

        public List<ProjectWithHours> GetDaysOffWithHours(string id)
        {
            return JsonDataAccess.GetDaysOffWithHours(id);
        }

        #endregion

    }
}
