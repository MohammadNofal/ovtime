﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script;
using System.Configuration;

namespace WcfOmniVistaTimeRESTfulJSonService
{
    public static class JsonDataAccess
    {
        private static SqlConnection _cn = new SqlConnection();



        public static void SetSqlConnection(string connectionString)
        {
            string cs = connectionString;

            _cn = new SqlConnection(cs);

        }

        public static DataTable GetDataTableBySqlSyntax(string sqlSyntax, string additionalParams)
        {

            DataTable dt = new DataTable();
            try
            {
                String strSqlCommand = sqlSyntax;

                SqlDataAdapter da = new SqlDataAdapter(strSqlCommand + " " + additionalParams, _cn);
                switch (_cn.State)
                {
                    case ConnectionState.Closed:

                        break;
                    case ConnectionState.Executing:
                        break;
                    case ConnectionState.Fetching:
                        break;
                    case ConnectionState.Broken:
                        break;
                }

                da.Fill(dt);
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }


        public static int ExecuteSqlStatement(string sqlSyntax, string additionalParams)
        {

            int ireturn = new int();
            try
            {
                switch (_cn.State)
                {
                    case ConnectionState.Closed:
                        _cn.Open();
                        break;
                }
                String strSqlCommand = sqlSyntax;
                SqlCommand cmd = new SqlCommand(sqlSyntax, _cn);
                ireturn = cmd.ExecuteNonQuery();


                switch (_cn.State)
                {
                    case ConnectionState.Open:
                        _cn.Close();
                        break;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ireturn;
        }

        public static string ConvertDataTabletoString(string username)
        {
            DataTable dt = new DataTable();

            SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            dt = GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["EmployeePTOSummary"], " ");


            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);


        }


        public static List<TimeEntry> GetTimeEntries(string username)
        {
            List<TimeEntry> timeEntries = new List<TimeEntry>();
            DataTable dt = new DataTable();

            SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            dt = GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["SummaryByProject"], " and EmployeeID = '" + username + "' ");

            if (dt == null) return null;

            foreach (DataRow dr in dt.Rows)
            {
                TimeEntry timeEntry = new TimeEntry()
                {
                    EmployeeID = dr["EmployeeID"].ToString(),
                    ProjectID = dr["ProjectID"].ToString(),
                    ProjectName = dr["ProjectName"].ToString(),
                    TotalHours = dr["TotalHours"].ToString()
                };
                timeEntries.Add(timeEntry);
            }

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            return timeEntries;

        }


        public static List<PtoSummary> GetPtoSummary(string username)
        {
            List<PtoSummary> ptoSummaries = new List<PtoSummary>();
            DataTable dt = new DataTable();

            SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            dt = GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["EmployeePTOSummary"], " and EmployeeID = '" + username + "' ");

            if (dt == null) return null;

            foreach (DataRow dr in dt.Rows)
            {
                PtoSummary ptoSummary = new PtoSummary()
                {
                    EmployeeID = dr["EmployeeID"].ToString(),
                    ColumnText = dr["ColumnText"].ToString(),
                    ColumnValue = dr["ColumnValue"].ToString()
                };
                ptoSummaries.Add(ptoSummary);
            }

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            return ptoSummaries;

        }
    }
}