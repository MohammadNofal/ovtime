﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script;
using System.Configuration;
using WcfOmniVistaTimeRESTfulJSonService.Data;
using System.Web.Security;
using System.Web.Http;

namespace WcfOmniVistaTimeRESTfulJSonService
{
    public static class JsonDataAccess
    {
        private static SqlConnection _cn = new SqlConnection();

        public static void SetSqlConnection(string connectionString)
        {
            string cs = connectionString;

            _cn = new SqlConnection(cs);

        }

        public static DataTable GetDataTableBySqlSyntax(string sqlSyntax, string additionalParams)
        {

            DataTable dt = new DataTable();
            try
            {
                String strSqlCommand = sqlSyntax;

                SqlDataAdapter da = new SqlDataAdapter(strSqlCommand + " " + additionalParams, _cn);
                switch (_cn.State)
                {
                    case ConnectionState.Closed:

                        break;
                    case ConnectionState.Executing:
                        break;
                    case ConnectionState.Fetching:
                        break;
                    case ConnectionState.Broken:
                        break;
                }

                da.Fill(dt);
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }


        public static int ExecuteSqlStatement(string sqlSyntax, string additionalParams)
        {

            int ireturn = new int();
            try
            {
                switch (_cn.State)
                {
                    case ConnectionState.Closed:
                        _cn.Open();
                        break;
                }
                String strSqlCommand = sqlSyntax;
                SqlCommand cmd = new SqlCommand(sqlSyntax, _cn);
                ireturn = cmd.ExecuteNonQuery();


                switch (_cn.State)
                {
                    case ConnectionState.Open:
                        _cn.Close();
                        break;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ireturn;
        }

        public static string ConvertDataTabletoString(string username)
        {
            DataTable dt = new DataTable();

            SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            dt = GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["EmployeePTOSummary"], " ");


            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);


        }


        public static List<TimeEntry> GetTimeEntries(string username)
        {
            List<TimeEntry> timeEntries = new List<TimeEntry>();
            DataTable dt = new DataTable();

            SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            dt = GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["SummaryByProject"], " and EmployeeID = '" + username + "' ");

            if (dt == null) return null;

            foreach (DataRow dr in dt.Rows)
            {
                TimeEntry timeEntry = new TimeEntry()
                {
                    EmployeeID = dr["EmployeeID"].ToString(),
                    ProjectID = dr["ProjectID"].ToString(),
                    ProjectName = dr["ProjectName"].ToString(),
                    TotalHours = dr["TotalHours"].ToString()
                };
                timeEntries.Add(timeEntry);
            }

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            return timeEntries;

        }

        public static List<PtoSummary> GetPtoSummary(string username)
        {
            List<PtoSummary> ptoSummaries = new List<PtoSummary>();
            DataTable dt = new DataTable();

            SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            dt = GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["EmployeePTOSummary"], " and EmployeeID = '" + username + "' ");

            if (dt == null) return null;

            foreach (DataRow dr in dt.Rows)
            {
                PtoSummary ptoSummary = new PtoSummary()
                {
                    EmployeeID = dr["EmployeeID"].ToString(),
                    ColumnText = dr["ColumnText"].ToString(),
                    ColumnValue = dr["ColumnValue"].ToString()
                };
                ptoSummaries.Add(ptoSummary);
            }

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            return ptoSummaries;

        }

        public static List<AllocationSummary> GetAllocationSummary(string username)
        {
            List<AllocationSummary> allocationSummaries = new List<AllocationSummary>();
            DataTable dt = new DataTable();

            SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            dt = GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["AllocationSummary"].Replace("[EMPLOYEEID]",username), " and EmployeeID = '" + username + "' ");


            if (dt == null) return null;

            foreach (DataRow dr in dt.Rows)
            {
                AllocationSummary allocationSummary = new  AllocationSummary()
                {
                    MonthNumber = dr["MonthNumber"].ToString(),
                    MonthText = dr["MonthText"].ToString(),
                    ProjectIdText1 = dr["ProjectIdText1"].ToString(),
                    ProjectIdText2 = dr["ProjectIdText2"].ToString(),
                    ProjectIdText3 = dr["ProjectIdText3"].ToString(),
                    ProjectIdText4 = dr["ProjectIdText4"].ToString(),
                    ProjectIdValue1 = dr["ProjectIdValue1"].ToString(),
                    ProjectIdValue2 = dr["ProjectIdValue2"].ToString(),
                    ProjectIdValue3 = dr["ProjectIdValue3"].ToString(),
                    ProjectIdValue4 = dr["ProjectIdValue4"].ToString()
                };
                allocationSummaries.Add(allocationSummary);
            }

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            return allocationSummaries;

        }
        public static List<ProjectSummary> GetProjectsSummary()
        {
            DataTable dt = new DataTable();
            List<ProjectSummary> list = new List<ProjectSummary>();

            SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            dt = GetDataTableBySqlSyntax("exec [OVS_GetProjectsDashboard] null,null,null,null,'Design,Supervision,Variation,Other'", " ");
            foreach(DataRow dr in dt.Rows)
            {
                ProjectSummary projSum = new ProjectSummary();
                projSum.projectId = dr["projectId"].ToString();
                projSum.ProjectHrs = double.Parse(dr["ProjectHrs"].ToString());
                list.Add(projSum);
            }

            var list2 = list;

            for (int i = 0; i < list.Count; i++)
            {
                list2.Add(list[i]);
            }

            return list2;
            //System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            //Dictionary<string, object> row;
            //foreach (DataRow dr in dt.Rows)
            //{
            //    row = new Dictionary<string, object>();
            //    foreach (DataColumn col in dt.Columns)
            //    {
            //        if (col.ColumnName != "ProjectHrs" && col.ColumnName != "projectId") continue; 
            //        row.Add(col.ColumnName, dr[col]);
            //    }
            //    rows.Add(row);
            //}
            //return serializer.Serialize(rows);

        }

        public static List<ProjectWithHours> GetProjectsWithHours(string id)
        {
            DataTable dt = new DataTable();
            List<ProjectWithHours> list = new List<ProjectWithHours>();
            ////Mohammad added the following 
            SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            DateTime passedDate = new DateTime();
            passedDate = DateTime.Now;
            int days = 0;
            string ToDate = ConfigurationManager.AppSettings["TimeSheetToDate"];
            switch (ToDate)
            {
                case "Sunday":
                    days = 0;
                    break;
                case "Monday":
                    days = 1;
                    break;
                case "Tuesday":
                    days = 2;
                    break;
                case "Wednesday":
                    days = 3;
                    break;
                case "Thursday":
                    days = 4;
                    break;
                case "Friday":
                    days = 5;
                    break;
                case "Saturday":
                    days = 6;
                    break;
            }
            //If the configuration is Saturday
            switch (passedDate.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    passedDate = passedDate.AddDays(days); break;
                case DayOfWeek.Monday:
                    passedDate = passedDate.AddDays((days - 1) < 0 ? days + 6 : days - 1); break;
                case DayOfWeek.Tuesday:
                    passedDate = passedDate.AddDays((days - 2) < 0 ? days + 5 : days - 2); break;
                case DayOfWeek.Wednesday:
                    passedDate = passedDate.AddDays((days - 3) < 0 ? days + 4 : days - 3); break;
                case DayOfWeek.Thursday:
                    passedDate = passedDate.AddDays((days - 4) < 0 ? days + 3 : days - 4); break;
                case DayOfWeek.Friday:
                    passedDate = passedDate.AddDays((days - 5) < 0 ? days + 2 : days - 5); break;
                case DayOfWeek.Saturday:
                    passedDate = passedDate.AddDays((days - 6) < 0 ? days + 1 : days - 6); break;
                default: break;
            }

            ////...............................
            //Mohammad eddited the following
            //SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            //dt = GetDataTableBySqlSyntax("EXEC OVS_GetWeeklyTimeSheet '" + DateTime.Today.AddDays(-7) + "' , '" + id + "'", "");
            passedDate = passedDate.AddDays(-8);
            dt = GetDataTableBySqlSyntax("EXEC OVS_GetWeeklyTimeSheet '" + passedDate.Date + "' , '" + id + "'", "");
            //.......................................
            foreach (DataRow dr in dt.Rows)
            {
                ProjectWithHours project = new ProjectWithHours();

                project.Project = dr["projectid"].ToString();
                project.ProjectHours = Double.Parse(dr["SAT_HRS"].ToString()) + Double.Parse(dr["SUN_HRS"].ToString()) + Double.Parse(dr["MON_HRS"].ToString()) + Double.Parse(dr["TUE_HRS"].ToString()) + Double.Parse(dr["WED_HRS"].ToString()) + Double.Parse(dr["THU_HRS"].ToString()) + Double.Parse(dr["FRI_HRS"].ToString());
                if (project.Project != "Vacation" && project.Project != "Sick" && project.Project != "Holiday" && project.Project != "Training")
                {
                    list.Add(project);
                }
            }

            return list;
        }

        public static List<ProjectWithHours> GetDaysOffWithHours(string id)
        {
            DataTable dt = new DataTable();
            List<ProjectWithHours> list = new List<ProjectWithHours>();

            ////Mohammad added the following 
            SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            DateTime passedDate = new DateTime();
            passedDate = DateTime.Now;
            int days = 0;
            string ToDate = ConfigurationManager.AppSettings["TimeSheetToDate"];
            switch (ToDate)
            {
                case "Sunday":
                    days = 0;
                    break;
                case "Monday":
                    days = 1;
                    break;
                case "Tuesday":
                    days = 2;
                    break;
                case "Wednesday":
                    days = 3;
                    break;
                case "Thursday":
                    days = 4;
                    break;
                case "Friday":
                    days = 5;
                    break;
                case "Saturday":
                    days = 6;
                    break;
            }
            //If the configuration is Saturday
            switch (passedDate.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    passedDate = passedDate.AddDays(days); break;
                case DayOfWeek.Monday:
                    passedDate = passedDate.AddDays((days - 1) < 0 ? days + 6 : days - 1); break;
                case DayOfWeek.Tuesday:
                    passedDate = passedDate.AddDays((days - 2) < 0 ? days + 5 : days - 2); break;
                case DayOfWeek.Wednesday:
                    passedDate = passedDate.AddDays((days - 3) < 0 ? days + 4 : days - 3); break;
                case DayOfWeek.Thursday:
                    passedDate = passedDate.AddDays((days - 4) < 0 ? days + 3 : days - 4); break;
                case DayOfWeek.Friday:
                    passedDate = passedDate.AddDays((days - 5) < 0 ? days + 2 : days - 5); break;
                case DayOfWeek.Saturday:
                    passedDate = passedDate.AddDays((days - 6) < 0 ? days + 1 : days - 6); break;
                default: break;
            }

            ////...............................
            
            //Mohammad eddited the following
            //SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            //dt = GetDataTableBySqlSyntax("EXEC OVS_GetWeeklyTimeSheet '" + DateTime.Today.AddDays(-7) + "' , '" + id + "'", "");
            passedDate = passedDate.AddDays(-8);
            dt = GetDataTableBySqlSyntax("EXEC OVS_GetWeeklyTimeSheet '" + passedDate.Date + "' , '" + id + "'", "");
            //.......................................


            foreach (DataRow dr in dt.Rows)
            {
                ProjectWithHours project = new ProjectWithHours();

                project.Project = dr["projectid"].ToString();
                project.ProjectHours = (Double.Parse(dr["SAT_HRS"].ToString()) + Double.Parse(dr["SUN_HRS"].ToString()) + Double.Parse(dr["MON_HRS"].ToString()) + Double.Parse(dr["TUE_HRS"].ToString()) + Double.Parse(dr["WED_HRS"].ToString()) + Double.Parse(dr["THU_HRS"].ToString()) + Double.Parse(dr["FRI_HRS"].ToString()))/8;
                if (project.Project == "Vacation" || project.Project == "Sick" || project.Project == "Holiday" || project.Project == "Training")
                {
                    list.Add(project);
                }
            }

            return list;
        }

    
    }

 
}