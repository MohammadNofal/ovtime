﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
//using BQSDK = BillQuickSDK2014;

using System.Text;


namespace DXOmniVistaTimeEngine
{
    public static class DataAccess
    {
        
        private static SqlConnection _cn = new SqlConnection();

        public static void SetSqlConnection(string connectionString)
        {
            string cs = connectionString;

            _cn = new SqlConnection(cs);

        }

        public static DataTable GetDataTableBySqlSyntax(string sqlSyntax, string additionalParams)
        {

            DataTable dt = new DataTable();
            try
            {
                String strSqlCommand = sqlSyntax;

                SqlDataAdapter da = new SqlDataAdapter(strSqlCommand + " " + additionalParams, _cn);
                switch (_cn.State)
                {
                    case ConnectionState.Closed:

                        break;
                    case ConnectionState.Executing:
                        break;
                    case ConnectionState.Fetching:
                        break;
                    case ConnectionState.Broken:
                        break;
                }

                da.Fill(dt);
            }

            catch (Exception ex)
            {
                string exceptionstring = ex.Message;
            }

            return dt;
        }


        public static int ExecuteSqlStatement(string sqlSyntax, string additionalParams)
        {

            int ireturn = new  int();
            try
            {
                switch (_cn.State)
                {
                    case ConnectionState.Closed:
                        _cn.Open();
                        break;
                }
                String strSqlCommand = sqlSyntax;
                SqlCommand cmd = new SqlCommand(sqlSyntax, _cn);
                ireturn = cmd.ExecuteNonQuery();


                switch (_cn.State)
                {
                    case ConnectionState.Open:
                        _cn.Close();
                        break;
                }
            }

            catch (Exception ex)
            {
                string exceptionstring = ex.Message;
            }

            return ireturn;
        }

        public static void InsertTime(DataRow row, DateTime _saturday)
        {
            TimeSheet.TimeProcessManager timeProcessManager = new TimeSheet.TimeProcessManager(new TimeSheet.TimeProcessSql());

            timeProcessManager.InsertTime(row, _saturday);
        }

        public static void UpdateTime(DataRow row, DateTime _saturday)
        {
            TimeSheet.TimeProcessManager timeProcessManager = new TimeSheet.TimeProcessManager(new TimeSheet.TimeProcessSql());

            timeProcessManager.UpdateTime(row, _saturday);
        }

        public static void DeleteTime(DataRow row)
        {

            TimeSheet.TimeProcessManager timeProcessManager = new TimeSheet.TimeProcessManager(new TimeSheet.TimeProcessSql());

            timeProcessManager.DeleteTime(row);
        }

        public static void SubmitTime(DataTable dt)
        {
            //TimeSheet.TimeProcessManager timeProcessManager = new TimeSheet.TimeProcessManager(new TimeSheet.TimeProcessBqe());

            //timeProcessManager.SubmitTime(dt);
            
        }
    }
}