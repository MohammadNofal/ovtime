﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using BQSDK = BillQuickSDK2014;


namespace DXOmniVistaTimeEngine.TimeSheet
{
    public class TimeProcessBqe : iTimeProcess
    {

        private  BQSDK.BQConnect conn;
        private  BQSDK.TimeEntry getTE = new BQSDK.TimeEntry();
        private  BQSDK.TimeEntryDataTable sendTE = new BQSDK.TimeEntryDataTable();


        public void InsertTime(DataRow row, DateTime _friday)
        {

        }

        private string insertTimeSqlStatement(string weekDay, string dayHRS, string projectID, string activityID, string employeeID)
        {
            return null;
        }

        public void UpdateTime(DataRow row, DateTime _friday)
        {

        }

        private string updateTimeSqlStatement(string weekDay, string dayHRS, string projectID, string activityID, string employeeID, string TEID)
        {
            return null;
        }

        public void DeleteTime(DataRow row)
        {

        }

        private string deleteTimeSqlStatement(string TEID)
        {
            return null;
        }

        public void SubmitTime(DataTable dt)
        {
            try
            {
                conn = new BQSDK.BQConnect("Engineering Consultants Group", "ENY1-YYDE-WJ96-9QXV");
                conn.Connect();

                //conn.Connect(cmbDBName.Text.ToString().Trim(), txtServer.Text.ToString().Trim(), txtUName.Text.ToString().Trim(), txtPass.Text.ToString().Trim());

                foreach (DataRow row in dt.Rows)
                {
                    ProcessSubmit(dt, row, "SAT_TEID");
                    ProcessSubmit(dt, row, "SUN_TEID");
                    ProcessSubmit(dt, row, "MON_TEID");
                    ProcessSubmit(dt, row, "TUE_TEID");
                    ProcessSubmit(dt, row, "WED_TEID");
                    ProcessSubmit(dt, row, "THU_TEID");
                    ProcessSubmit(dt, row, "FRI_TEID");






                    //if (row["SAT_TEID"].ToString() != string.Empty)
                    //{
                    //    Guid TEID = new Guid(dt.Rows[0]["SAT_TEID"].ToString());
                    //    string sFilter = "TEID = '" + TEID.ToString().ToUpper() + "'";

                    //    getTE = new BQSDK.TimeEntry();
                    //    int pageNumber = getTE.GetTotalPages(sFilter);

                    //    sendTE = getTE.GetTimeEntry(BQSDK.TimeEntry.Type.None, "", sFilter, 0)[0];
                    //    sendTE.ApprovalStatus = BQSDK.TimeEntryDataTable.approveStatus.Submitted;

                    //    if (getTE.UpdateTimeEntry(sendTE, DateTime.Parse(sendTE.LastUpdated.ToString())) == 1)
                    //    {
                    //        //
                    //    }
                    //    else
                    //    {
                    //        //
                    //    }
                    //}

                }
            }

            catch (BQSDK.BQSDKException ex)
            {
                string exceptionstring = ex.Message;
            }


        }

        private  void ProcessSubmit(DataTable dt, DataRow row, string TEIDdate)
        {
            sendTE = new BQSDK.TimeEntryDataTable();
            if (row[TEIDdate].ToString() != string.Empty)
            {
                Guid TEID = new Guid(row[TEIDdate].ToString());
                string sFilter = "TEID = '" + TEID.ToString().ToUpper() + "'";

                getTE = new BQSDK.TimeEntry();
                int pageNumber = getTE.GetTotalPages(sFilter);

                sendTE = getTE.GetTimeEntry(BQSDK.TimeEntry.Type.None, "", sFilter, 0)[0];
                sendTE.ApprovalStatus = BQSDK.TimeEntryDataTable.approveStatus.Submitted;

                if (getTE.UpdateTimeEntry(sendTE, DateTime.Parse(sendTE.LastUpdated.ToString())) == 1)
                {
                    //
                }
                else
                {
                    //
                }
            }

        }
    }
}
