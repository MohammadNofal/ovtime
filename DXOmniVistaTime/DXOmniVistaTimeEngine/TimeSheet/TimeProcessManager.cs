﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Configuration;
using System.Data;


namespace DXOmniVistaTimeEngine.TimeSheet
{
    public class TimeProcessManager
    {
        iTimeProcess _TimeProcess;

        public TimeProcessManager(iTimeProcess TimeProcess)
        {
            _TimeProcess = TimeProcess;
        }

        public void InsertTime(DataRow row, DateTime _saturday)
        {
            _TimeProcess.InsertTime(row, _saturday);
        }

        public void UpdateTime(DataRow row, DateTime _saturday)
        {
            _TimeProcess.UpdateTime(row, _saturday);
        }

        public void DeleteTime(DataRow row)
        {
            _TimeProcess.DeleteTime(row);
        }

        public void SubmitTime(DataTable dt)
        {
            _TimeProcess.SubmitTime(dt);
        }
        
    }
    
}
