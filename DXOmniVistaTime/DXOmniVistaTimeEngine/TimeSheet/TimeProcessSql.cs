﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace DXOmniVistaTimeEngine.TimeSheet
{
    public class TimeProcessSql : iTimeProcess
    {
        public static int StartTime = 480;
        //public static string ActivityID = "DEFAULT";
        public void InsertTime(DataRow row, DateTime _saturday)
        {
            StringBuilder sqlSyntax = new StringBuilder();

            string TEOT = "0";

            if ((bool)row["TEOT"] == true)
            {
                TEOT = "1";
            }

            string overtime = row["TEOT"].ToString();

            if (double.Parse(row["SUN_HRS"].ToString()) > 0)
            {
                sqlSyntax.Append(insertTimeSqlStatement(_saturday.AddDays(-6).ToShortDateString(), row["SUN_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectID"].ToString(), row["RoleName"].ToString(), row["EmployeeID"].ToString(), row["SUN_MEMO"].ToString(), TEOT));
            }
            if (double.Parse(row["MON_HRS"].ToString()) > 0)
            {
                sqlSyntax.Append(insertTimeSqlStatement(_saturday.AddDays(-5).ToShortDateString(), row["MON_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectID"].ToString(), row["RoleName"].ToString(), row["EmployeeID"].ToString(), row["MON_MEMO"].ToString(), TEOT));
            }
            if (double.Parse(row["TUE_HRS"].ToString()) > 0)
            {
                sqlSyntax.Append(insertTimeSqlStatement(_saturday.AddDays(-4).ToShortDateString(), row["TUE_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectID"].ToString(), row["RoleName"].ToString(), row["EmployeeID"].ToString(), row["TUE_MEMO"].ToString(), TEOT));
            }
            if (double.Parse(row["WED_HRS"].ToString()) > 0)
            {
                sqlSyntax.Append(insertTimeSqlStatement(_saturday.AddDays(-3).ToShortDateString(), row["WED_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectID"].ToString(), row["RoleName"].ToString(), row["EmployeeID"].ToString(), row["WED_MEMO"].ToString(), TEOT));
            }
            if (double.Parse(row["THU_HRS"].ToString()) > 0)
            {
                sqlSyntax.Append(insertTimeSqlStatement(_saturday.AddDays(-2).ToShortDateString(), row["THU_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectID"].ToString(), row["RoleName"].ToString(), row["EmployeeID"].ToString(), row["THU_MEMO"].ToString(), TEOT));
            }
            if (double.Parse(row["FRI_HRS"].ToString()) > 0)
            {
                sqlSyntax.Append(insertTimeSqlStatement(_saturday.AddDays(-1).ToShortDateString(), row["FRI_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectID"].ToString(), row["RoleName"].ToString(), row["EmployeeID"].ToString(), row["FRI_MEMO"].ToString(), TEOT));
            }
            if (double.Parse(row["SAT_HRS"].ToString()) > 0)
            {
                sqlSyntax.Append(insertTimeSqlStatement(_saturday.ToShortDateString(), row["SAT_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectID"].ToString(), row["RoleName"].ToString(), row["EmployeeID"].ToString(), row["SAT_MEMO"].ToString(), TEOT));
            }

            DataAccess.ExecuteSqlStatement(sqlSyntax.ToString(), null);
        }

        private string insertTimeSqlStatement(string weekDay, string dayHRS, string clientID, string projectID, string roleID, string employeeID, string memo, string TEOT)
        {
            StringBuilder sqlSyntax = new StringBuilder();

            Guid generatedID = Guid.NewGuid();
            float EndTime = (float.Parse(dayHRS) * 60) + StartTime;
            sqlSyntax.Append("INSERT INTO TimeEntry (TEID, TEDATE, EmployeeID,  ProjectID,  TEHours, TEOT, ProjectName,TECostRate,aHours,StartTime,StopTime,LastUpdated,ApprovalStatus,CreatedOn, TEMemo)");
            sqlSyntax.Append(" SELECT '" + generatedID + "','" + weekDay + "',e.EmployeeID,p.ProjectID," + dayHRS + "," + TEOT + ", p.ProjectName, e.EmpCostRate," + dayHRS + ","+ StartTime + "," + EndTime + ",GETDATE(),0,GETDATE(),'" + @memo.Replace("'", "''") + "'");
            //sqlSyntax.Append(" FROM	Project p, Activity a, Employee e");
            sqlSyntax.Append(" FROM	Project p, Employee e");
            sqlSyntax.Append(" WHERE	p.ProjectID =" + "'" + projectID + "'");
            //sqlSyntax.Append(" AND      a.ActivityID = '1147-SUPER-MD:'"); //Mohammad commented this out
            sqlSyntax.Append(" AND      e.EmployeeID =" + "'" + employeeID + "';");
            
            //Inserting into TimeEntryDetails 
            sqlSyntax.Append("INSERT INTO TimeEntryDetails (TEID, RoleID, ClientID, ClientName, RoleName) VALUES ('" + generatedID + "','" + roleID + "','" + clientID + "',(SELECT ClientCompany From Client WHERE ClientID ='" + clientID + "'),(SELECT RoleName FROM Role WHERE RoleID = '" + roleID + "'));");

            return sqlSyntax.ToString();
        }

        public void UpdateTime(DataRow row, DateTime _saturday)
        {
            StringBuilder sqlSyntax = new StringBuilder();

            string TEOT = "0";

            if ((bool)row["TEOT"] == true)
            {
                TEOT = "1";
            }

            if (row["SUN_TEID"].ToString() != string.Empty)
            {
                sqlSyntax.Append(updateTimeSqlStatement(_saturday.AddDays(-6).ToShortDateString(), row["SUN_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectNb"].ToString(), row["RoleId"].ToString(), row["EmployeeID"].ToString(), row["SUN_TEID"].ToString(), row["SUN_MEMO"].ToString(), TEOT));
            }
            else if (double.Parse(row["SUN_HRS"].ToString()) > 0)
            {
                sqlSyntax.Append(insertTimeSqlStatement(_saturday.AddDays(-6).ToShortDateString(), row["SUN_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectNb"].ToString(), row["RoleId"].ToString(), row["EmployeeID"].ToString(), row["SUN_MEMO"].ToString(), TEOT));
            }

            if (row["MON_TEID"].ToString() != string.Empty)
            {
                sqlSyntax.Append(updateTimeSqlStatement(_saturday.AddDays(-5).ToShortDateString(), row["MON_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectNb"].ToString(), row["RoleId"].ToString(), row["EmployeeID"].ToString(), row["MON_TEID"].ToString(), row["MON_MEMO"].ToString(), TEOT));
            }
            else if (double.Parse(row["MON_HRS"].ToString()) > 0)
            {
                sqlSyntax.Append(insertTimeSqlStatement(_saturday.AddDays(-5).ToShortDateString(), row["MON_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectNb"].ToString(), row["RoleId"].ToString(), row["EmployeeID"].ToString(), row["MON_MEMO"].ToString(), TEOT));
            }

            if (row["TUE_TEID"].ToString() != string.Empty)
            {
                sqlSyntax.Append(updateTimeSqlStatement(_saturday.AddDays(-4).ToShortDateString(), row["TUE_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectNb"].ToString(), row["RoleId"].ToString(), row["EmployeeID"].ToString(), row["TUE_TEID"].ToString(), row["TUE_MEMO"].ToString(), TEOT));
            }
            else if (double.Parse(row["TUE_HRS"].ToString()) > 0)
            {
                sqlSyntax.Append(insertTimeSqlStatement(_saturday.AddDays(-4).ToShortDateString(), row["TUE_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectNb"].ToString(), row["RoleId"].ToString(), row["EmployeeID"].ToString(), row["TUE_MEMO"].ToString(), TEOT));
            }

            if (row["WED_TEID"].ToString() != string.Empty)
            {
                sqlSyntax.Append(updateTimeSqlStatement(_saturday.AddDays(-3).ToShortDateString(), row["WED_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectNb"].ToString(), row["RoleId"].ToString(), row["EmployeeID"].ToString(), row["WED_TEID"].ToString(), row["WED_MEMO"].ToString(), TEOT));
            }
            else if (double.Parse(row["WED_HRS"].ToString()) > 0)
            {
                sqlSyntax.Append(insertTimeSqlStatement(_saturday.AddDays(-3).ToShortDateString(), row["WED_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectNb"].ToString(), row["RoleId"].ToString(), row["EmployeeID"].ToString(), row["WED_MEMO"].ToString(), TEOT));
            }

            if (row["THU_TEID"].ToString() != string.Empty)
            {
                sqlSyntax.Append(updateTimeSqlStatement(_saturday.AddDays(-2).ToShortDateString(), row["THU_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectNb"].ToString(), row["RoleId"].ToString(), row["EmployeeID"].ToString(), row["THU_TEID"].ToString(), row["THU_MEMO"].ToString(), TEOT));
            }
            else if (double.Parse(row["THU_HRS"].ToString()) > 0)
            {
                sqlSyntax.Append(insertTimeSqlStatement(_saturday.AddDays(-2).ToShortDateString(), row["THU_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectNb"].ToString(), row["RoleId"].ToString(), row["EmployeeID"].ToString(), row["THU_MEMO"].ToString(), TEOT));
            }

            if (row["FRI_TEID"].ToString() != string.Empty)
            {
                sqlSyntax.Append(updateTimeSqlStatement(_saturday.AddDays(-1).ToShortDateString(), row["FRI_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectNb"].ToString(), row["RoleId"].ToString(), row["EmployeeID"].ToString(), row["FRI_TEID"].ToString(), row["FRI_MEMO"].ToString(), TEOT));
            }
            else if (double.Parse(row["FRI_HRS"].ToString()) > 0)
            {
                sqlSyntax.Append(insertTimeSqlStatement(_saturday.AddDays(-1).ToShortDateString(), row["FRI_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectNb"].ToString(), row["RoleId"].ToString(), row["EmployeeID"].ToString(), row["FRI_MEMO"].ToString(), TEOT));
            }
            if (row["SAT_TEID"].ToString() != string.Empty)
            {
                sqlSyntax.Append(updateTimeSqlStatement(_saturday.AddDays(0).ToShortDateString(), row["SAT_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectNb"].ToString(), row["RoleId"].ToString(), row["EmployeeID"].ToString(), row["SAT_TEID"].ToString(), row["SAT_MEMO"].ToString(), TEOT));
            }
            else if (double.Parse(row["SAT_HRS"].ToString()) > 0)
            {
                sqlSyntax.Append(insertTimeSqlStatement(_saturday.AddDays(0).ToShortDateString(), row["SAT_HRS"].ToString(), row["ClientID"].ToString(), row["ProjectNb"].ToString(), row["RoleId"].ToString(), row["EmployeeID"].ToString(), row["SAT_MEMO"].ToString(), TEOT));
            }

            DataAccess.ExecuteSqlStatement(sqlSyntax.ToString(), null);
        }

        private string updateTimeSqlStatement(string weekDay, string dayHRS,string clientID, string projectID, string roleID, string employeeID, string TEID, string memo, string TEOT)
        {
            StringBuilder sqlSyntax = new StringBuilder();

            sqlSyntax.Append("Update t ");
            sqlSyntax.Append(" SET t.TEHours =" + dayHRS + ", aHours = " + dayHRS + ", TEMemo ='" + @memo.Replace("'", "''") + "', TEOT ='" + TEOT + "'");
            sqlSyntax.Append(" FROM	TimeEntry t, Project p, Role r, Employee e, Client c");
            sqlSyntax.Append(" WHERE	CAST(t.TEID as varchar(50)) = '" + TEID + "'");
            sqlSyntax.Append(" AND      p.ProjectID =" + "'" + projectID + "'");
            sqlSyntax.Append(" AND      c.ClientID =" + "'" + clientID + "'");
            sqlSyntax.Append(" AND		r.RoleID =" + "'" + roleID + "'");
            sqlSyntax.Append(" AND      e.EmployeeID =" + "'" + employeeID + "';");

            // Updating TimeEntryDetails
            //sqlSyntax.Append("UPDATE TimeEntryDetails SET RoleID = '" + roleID + "', ClientID = '" + clientID + "', ClientName = (SELECT ClientCompany From Client WHERE ClientID ='" + clientID + "'), RoleName = (SELECT RoleName FROM Role WHERE RoleID = '" + roleID + "') WHERE TEID = '" + TEID + "'");

            return sqlSyntax.ToString();

        }

        public void DeleteTime(DataRow row)
        {

            //return;

            StringBuilder sqlSyntax = new StringBuilder();
            if (row["SUN_TEID"].ToString() != string.Empty)
            {
                sqlSyntax.Append(deleteTimeSqlStatement(row["SUN_TEID"].ToString()));
            }
            if (row["MON_TEID"].ToString() != string.Empty)
            {
                sqlSyntax.Append(deleteTimeSqlStatement(row["MON_TEID"].ToString()));
            }
            if (row["TUE_TEID"].ToString() != string.Empty)
            {
                sqlSyntax.Append(deleteTimeSqlStatement(row["TUE_TEID"].ToString()));
            }
            if (row["WED_TEID"].ToString() != string.Empty)
            {
                sqlSyntax.Append(deleteTimeSqlStatement(row["WED_TEID"].ToString()));
            }
            if (row["THU_TEID"].ToString() != string.Empty)
            {
                sqlSyntax.Append(deleteTimeSqlStatement(row["THU_TEID"].ToString()));
            }
            if (row["FRI_TEID"].ToString() != string.Empty)
            {
                sqlSyntax.Append(deleteTimeSqlStatement(row["FRI_TEID"].ToString()));
            }
            if (row["SAT_TEID"].ToString() != string.Empty)
            {
                sqlSyntax.Append(deleteTimeSqlStatement(row["SAT_TEID"].ToString()));
            }

            DataAccess.ExecuteSqlStatement(sqlSyntax.ToString(), null);

        }

        private string deleteTimeSqlStatement(string TEID)
        {
            StringBuilder sqlSyntax = new StringBuilder();

            //sqlSyntax.Append("DELETE TimeEntry WHERE TEPosted = 0 and TEID = '" + TEID + "';"); //ref Mohammad altered this
            sqlSyntax.Append("DELETE TimeEntry WHERE TEID = '" + TEID + "';");
            //Delete from timeentrydetails table

            sqlSyntax.Append("DELETE From TimeEntryDetails WHERE TEID = '" + TEID + "';");

            return sqlSyntax.ToString();

        }

        public void SubmitTime(DataTable dt)
        {

        }
    }
}
