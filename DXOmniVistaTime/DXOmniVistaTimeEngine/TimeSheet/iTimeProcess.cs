﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Configuration;
using System.Data;


namespace DXOmniVistaTimeEngine.TimeSheet
{
    public interface iTimeProcess
    {
        void SubmitTime(DataTable dt);

        void InsertTime(DataRow row, DateTime _saturday);

        void UpdateTime(DataRow row, DateTime _saturday);
        
        void DeleteTime(DataRow row);
        
    }
}
