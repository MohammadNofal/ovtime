﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DXOmniVistaTimeEngine;
using System.Web.Security;


public partial class Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);


            this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["SummaryByProject"], " and EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' ");
            this.ASPxGridView1.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "TotalHours");

            this.ASPxGridView1.DataBind();

            
            //DevExpress.XtraCharts.Web.WebChartControl myChart = WebChartControl1;
            //myChart.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["SummaryByProject"], " and EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' "); 
            //// Create a series, and add it to the chart. 
            //DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series("My Series", DevExpress.XtraCharts.ViewType.Pie);
            //myChart.Series.Add(series1);

            //// Adjust the series data members. 
            //series1.ArgumentDataMember = "ProjectID";
            //series1.ValueDataMembers.AddRange(new string[] { "TotalHours" });
            //series1.LegendTextPattern = "Project: {A} | Total Hours: {V}";
            //this.WebChartControl1.DataBind();






            // Access the view-type-specific options of the series. 
            //((DevExpress.XtraCharts.BarSeriesView)series1.View).ColorEach = true;
            //((DevExpress.XtraCharts.PieSeriesView)series1.View). = true;
            


            //this.WebChartControl1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["SummaryByProject"], " and EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' ");

            //WebChartControl1.SeriesTemplate.ChangeView(DevExpress.XtraCharts.ViewType.Pie);
            //DevExpress.XtraCharts.Series s = new DevExpress.XtraCharts.Series("Pie", DevExpress.XtraCharts.ViewType.Pie);
            
            //s.DataSource  = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["SummaryByProject"], " and EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' ");
            //s.ArgumentDataMember = "ProjectID";
            //s.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            
            //s.ValueDataMembers.AddRange(new string[] { "TotalHours" });
           
            //WebChartControl1.Series.Add(s);

           

            //WebChartControl1.SeriesDataMember = "TotalHours";
            //WebChartControl1.SeriesTemplate.ArgumentDataMember = "ProjectID";
            //WebChartControl1.SeriesTemplate.ValueDataMembers.AddRange(new string[] { "TotalHours" });
           

            
            

            
            
        }
    }

     protected void ASPxGridView1_CustomColumnSort(object sender, DevExpress.Web.CustomColumnSortEventArgs e)
    {

    }
    protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
    {
        
        
    }
    protected void ASPxGridView1_DataBound(object sender, EventArgs e)
    {
        
        
    }
    protected void ASPxGridView1_BeforeColumnSortingGrouping(object sender, DevExpress.Web.ASPxGridViewBeforeColumnGroupingSortingEventArgs e)
    {
        this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["SummaryByProject"], " and EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' ");
        this.ASPxGridView1.DataBind();
    }


    protected void ASPxGridView1_SummaryDisplayText(object sender, DevExpress.Web.ASPxGridViewSummaryDisplayTextEventArgs e)
    {
        if (e.Item.FieldName == "TotalHours")
            e.Text = string.Format("{0:N2}", Convert.ToDouble(e.Value));
    }

}