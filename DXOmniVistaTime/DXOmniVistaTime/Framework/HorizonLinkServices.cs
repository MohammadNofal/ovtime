using System;
using System.Collections.Generic;
using System.Text;
using com.UniversaLink.HorizonLink.Services;
using com.UniversaLink.HorizonLink;
using com.UniversaLink.ELink.Framework;
using com.UniversaLink.HorizonLink.DataProvider;

namespace OmniLinkUI.Framework
{
    public class HorizonLinkServices
    {
        ServiceFactory factory;

        public HorizonLinkServices(ServiceFactory factory)
        {
            this.factory = factory;
        }

        public StockDataManagementService StockDataManagementService
        {
            get
            {
                return factory.StockDataManagementService;
            }
        }

        public StockDataProvider StockDataProvider
        {
            get
            {
                return factory.StockDataProvider;
            }
        }
    }
}   