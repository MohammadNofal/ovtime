using System;
using System.Collections.Generic;
using System.Text;
using Infragistics.Win.UltraWinDock;
using System.Windows.Forms;
using System.Drawing;
using Infragistics.Win;
using System.Configuration;

namespace com.UniversaLink.OmniLink.UI.Framework
{
    public class LayoutManager
    {
        private UltraDockManager dockManager;       // Manager that will be used for docking
        private List<DockableControlPane> listControls = new List<DockableControlPane>();
        private Form hostForm;
        private UserControl hostControl;

        public LayoutManager(UltraDockManager dockManager, Form hostForm)
        {
            this.dockManager = dockManager;
            this.hostForm = hostForm;
        }
        public LayoutManager(UltraDockManager dockManager, UserControl hostControl)
        {
            this.dockManager = dockManager;
            this.hostControl = hostControl;
        }
        private DockableControlPane GetPaneForControl(String key)
        {
            foreach (DockableControlPane pane in listControls)
            {
                if (pane.Key == key)
                    return pane;
            }
            return null;
        }

        public bool HasControl(Control control)
        {
            if (dockManager.PaneFromControl(control) != null)
            {
                return true;
            }
            return false;
        }

        public DockableControlPane AddControl(String key, Control control)
        {
            if (HasControl(control))
            {
                // remove older control
                DockableControlPane oldControlPane = GetPaneForControl(key);
                listControls.Remove(oldControlPane);
            }
            // Create a new DockableControlPane 
            DockableControlPane controlPane = new DockableControlPane(key, control);
            controlPane.Text = key;
            //controlPane.Settings.AllowFloating = DefaultableBoolean.True;
            controlPane.Settings.CanDisplayAsMdiChild = DefaultableBoolean.True;
            listControls.Add(controlPane);
            return controlPane;
        }

        public void ShowControl(String key, bool show)
        {
            DockablePaneBase controlPane = dockManager.PaneFromKey(key);
            if (controlPane != null)
            {
                if (!show)
                    controlPane.Close(true);
                else
                    controlPane.Show();
            }
        }

        public void ShowControl(Control control, bool show)
        {
            DockablePaneBase controlPane = dockManager.PaneFromControl(control);
            if (controlPane != null)
            {
                if (show)
                {
                    controlPane.Show();
                    ActivateControl(control);
                }
                else
                    controlPane.Close(true);
            }
        }

        public void ActivateControl(Control control)
        {
            DockablePaneBase controlPane = dockManager.PaneFromControl(control);
            if (controlPane != null)
                controlPane.Activate();
        }

        public bool IsVisible(Control control)
        {
            DockablePaneBase controlPane = dockManager.PaneFromControl(control);
            if (controlPane != null)
                return !controlPane.Closed;
            return false;
        }

        public void DoLayout()
        {
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).BeginInit();
            DockableGroupPane groupPane = new DockableGroupPane();
            WindowDockingArea windowDockArea = new WindowDockingArea();
            windowDockArea.Owner = dockManager;
            windowDockArea.BackColor = Color.WhiteSmoke;
            windowDockArea.Dock = DockStyle.Left;

            groupPane.ChildPaneStyle = ChildPaneStyle.TabGroup;
            foreach (DockableControlPane controlPane in listControls)
            {
                groupPane.Panes.Add(controlPane);
                DockableWindow dockableWindow = new DockableWindow();
                dockableWindow.Controls.Add(controlPane.Control);
                dockableWindow.Owner = dockManager;
                windowDockArea.Controls.Add(dockableWindow);
            }
            DockAreaPane areaPane = new DockAreaPane(DockedLocation.DockedLeft);
            areaPane.Panes.Add(groupPane);
            dockManager.DockAreas.Add(areaPane);

            hostForm.Controls.Add(windowDockArea);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).EndInit();

            LoadDefaultLayout();
        }

        public void SaveLayoutAsDefault()
        {
            SaveLayout(ConfigurationManager.AppSettings["DefaultLayoutFile"]);
        }

        public void SaveLayout(String fileName)
        {
            dockManager.SaveAsXML(fileName);
        }

        public void LoadDefaultLayout()
        {
            LoadLayout(ConfigurationManager.AppSettings["DefaultLayoutFile"]);
        }

        public void LoadLayout(String fileName)
        {
            if (fileName != null && System.IO.File.Exists(fileName))
            {
                dockManager.LoadFromXML(fileName);
            }
        }

        public void DoLayoutControl()
        {
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).BeginInit();
            DockableGroupPane groupPane = new DockableGroupPane();
            WindowDockingArea windowDockArea = new WindowDockingArea();
            windowDockArea.Owner = dockManager;
            windowDockArea.Dock = DockStyle.Left;

            groupPane.ChildPaneStyle = ChildPaneStyle.TabGroup;
            foreach (DockableControlPane controlPane in listControls)
            {
                groupPane.Panes.Add(controlPane);
                DockableWindow dockableWindow = new DockableWindow();
                dockableWindow.Controls.Add(controlPane.Control);
                dockableWindow.Owner = dockManager;
                windowDockArea.Controls.Add(dockableWindow);
            }

            DockAreaPane areaPane = new DockAreaPane(DockedLocation.DockedLeft);
            areaPane.Panes.Add(groupPane);
            dockManager.DockAreas.Add(areaPane);

            hostControl.Controls.Add(windowDockArea);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).EndInit();
            //dockManager.LoadFromXML(@"C:\DefaultLayout.xml");
        }
    }
}
