using System;
using System.Collections.Generic;
using System.Text;
using com.UniversaLink.HorizonLink;
//using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;
using System.Collections;
using System.Runtime.Serialization.Formatters;
using com.UniversaLink.OmniLink.AccessControl;
using System.Configuration;
using System.Threading;


namespace OmniLinkUI.Framework
{
    public class HorizonLinkUIApplication
    {
        private static HorizonLinkUIApplication instance = new HorizonLinkUIApplication();
        private HorizonLinkServices services;
        private static bool connected = false;

        public HorizonLinkServices Services
        {
            get { return services; }
            set { services = value; }
        }

        private String url;

        private HorizonLinkUIApplication()
        {
            try
            {
                // Creating a custom formatter for a TcpChannel sink chain.
                BinaryServerFormatterSinkProvider provider = new BinaryServerFormatterSinkProvider();
                provider.TypeFilterLevel = TypeFilterLevel.Full;

                IDictionary props = new Hashtable();
                props["port"] = 0;
                props["name"] = "HorizonLink";

                bool channelRegisteredFlag = false;
                TcpChannel channel = new TcpChannel(props, null, provider);
                try
                {
                    for (int registeredChannelIndex = 0; registeredChannelIndex < ChannelServices.RegisteredChannels.Length; registeredChannelIndex++)
                    {
                        if (ChannelServices.RegisteredChannels[registeredChannelIndex].ChannelName == channel.ChannelName)
                        {
                            channelRegisteredFlag = true;
                            break;
                        }
                    }
                    if (!channelRegisteredFlag)
                    {
                        ChannelServices.RegisterChannel(channel, false);    
                    }
                    
                }
                catch (Exception ex)
                {
                    //NAEL FARSAKH-START
                }

                string strHorizonLinkServiceIP = "0";
                strHorizonLinkServiceIP = ConfigurationManager.AppSettings["HorizonLinkServiceIP"];

                string strHorizonLinkServicePort = "0";
                strHorizonLinkServicePort = ConfigurationManager.AppSettings["HorizonLinkServicePort"];

                url = "tcp://" + strHorizonLinkServiceIP + ":" + strHorizonLinkServicePort + "/HorizonLinkServiceFactory";
                //url = "tcp://127.0.0.1:9000/HorizonLinkServiceFactory";

                while (!connected)
                {
                    services = new HorizonLinkServices(Factory);
                    try
                    {
                        if (services.StockDataManagementService != null)
                        {
                            connected = true;
                        }
                    }
                    catch (System.Net.Sockets.SocketException ex)
                    {
                        //MessageBox.Show("Error Connecting, sleeping for a while");
                        //Thread.Sleep(1000);
                        ChannelServices.UnregisterChannel(channel);
                        return;

                    }
                }

            }
            catch (Exception ex) { 
                //MessageBox.Show(ex.Message); 
            }
        }

        public static HorizonLinkUIApplication Instance
        {
            get
            {
                if (!connected)
                    instance = new HorizonLinkUIApplication();

                return instance;
            }
        }

        private ServiceFactory Factory
        {
            get
            {
                try
                {
                    return (ServiceFactory)RemotingServices.Connect(typeof(ServiceFactory), url);
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);
                }
                return null;
            }
        }

        private UserCredential user;
        public UserCredential User
        {
            get
            {
                return user;
            }
            set
            {
                user = value;
            }
        }
    }
}
