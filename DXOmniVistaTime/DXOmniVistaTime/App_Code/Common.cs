﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Security;

/// <summary>
/// Summary description for Class1
/// </summary>
public class CommonFunctions
{
	public CommonFunctions()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static void TestSend()
    {
        string constr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
        // string activationCode = Guid.NewGuid().ToString();
        //mahmoud.ebeed@omnivistasolutions.com
        //mahmoudebeed1988@gmail.com
        using (MailMessage mm = new MailMessage("support2@omnivistasolutions.com", "mahmoud.ebeed@omnivistasolutions.com"))
        {
            mm.Subject = "Account Activation";
            string link = link = HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath + "Account/Login.aspx?ActivationCode=" + "toekntest";

            //   string link = string.Format(HttpContext.Current.Server.MapPath("~/Account/Login.aspx?ActivationCode={0}"), user.ProviderUserKey);
            string body = createEmailBody(link, "asdfci", "Mahmoud", "Ebeed", "//");
            mm.Body = body;
            mm.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();

            smtp.Host = "mail028-1.exch028.serverdata.net";
            smtp.EnableSsl = true;
            NetworkCredential NetworkCred = new NetworkCredential("support2@omnivistasolutions.com", "Solutions1!");
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            try
            {
                smtp.Send(mm);
            }
            catch (Exception ex)
            {

            }
        }
    }
    
    public static void SendActivationEmail(MembershipUser user, string EmpFName, string EmpLName, string baseUrl)
    {
        string constr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
        // string activationCode = Guid.NewGuid().ToString();
       
        using (MailMessage mm = new MailMessage("support2@omnivistasolutions.com", user.Email))
        {
            mm.Subject = "Account Activation";
            string link = link = HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath + "Account/Login.aspx?ActivationCode=" + user.ProviderUserKey;

            //   string link = string.Format(HttpContext.Current.Server.MapPath("~/Account/Login.aspx?ActivationCode={0}"), user.ProviderUserKey);
            string body = createEmailBody(link, user.UserName, EmpFName, EmpLName, baseUrl);
            mm.Body = body;
            mm.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();

            smtp.Host = "mail028-1.exch028.serverdata.net";
            smtp.EnableSsl = true;
            NetworkCredential NetworkCred = new NetworkCredential("support2@omnivistasolutions.com", "Solutions1!");
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            try
            {
                smtp.Send(mm);
            }catch(Exception ex){

            }
        }
    }

    public static string GetHTMLBody(string url)
    {
        string htmlBody;

        using (WebClient client = new WebClient())
        {

            htmlBody = client.DownloadString(HttpContext.Current.Server.MapPath("~/HtmlTemplate.html"));
        }

        return htmlBody;
       
    }
    public static string createEmailBody(string link, string id, string first_name, string last_name,string base_url)
    {

        string body = string.Empty;
        //using streamreader for reading my htmltemplate   
        body = GetHTMLBody(base_url + "/HtmlTemplate.html");
     //   using (StreamReader reader = new StreamReader("~/HtmlTemplate.html"))
    //    {

        //    body = reader.ReadToEnd();

      //  }

        body = body.Replace("{LINK}", link); //replacing the required things  
        body = body.Replace("[YEAR]", DateTime.Today.ToString("yyyy"));
        body = body.Replace("{first_name}", first_name);
        body = body.Replace("{last_name}", last_name);
        body = body.Replace("{id}", id);

        return body;

    }  
}