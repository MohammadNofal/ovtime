﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AccessData
/// </summary>
public class AccessData
{
	 
		private static SqlConnection _cn = new SqlConnection();

        public static void SetSqlConnection(string connectionString)
        {
            string cs = connectionString;

            _cn = new SqlConnection(cs);

        }

        public static DataTable GetDataTableBySqlSyntax(string sqlSyntax, string additionalParams)
        {

            DataTable dt = new DataTable();
            try
            {
                String strSqlCommand = sqlSyntax;

                SqlDataAdapter da = new SqlDataAdapter(strSqlCommand + " " + additionalParams, _cn);
                switch (_cn.State)
                {
                    case ConnectionState.Closed:

                        break;
                    case ConnectionState.Executing:
                        break;
                    case ConnectionState.Fetching:
                        break;
                    case ConnectionState.Broken:
                        break;
                }

                da.Fill(dt);
            }

            catch (Exception ex)
            {
                string exceptionstring = ex.Message;
            }

            return dt;
        }


        public static int ExecuteSqlStatement(string sqlSyntax, string additionalParams)
        {

            int ireturn = new int();
            try
            {
                switch (_cn.State)
                {
                    case ConnectionState.Closed:
                        _cn.Open();
                        break;
                }
                String strSqlCommand = sqlSyntax;
                SqlCommand cmd = new SqlCommand(sqlSyntax, _cn);
                ireturn = cmd.ExecuteNonQuery();


                switch (_cn.State)
                {
                    case ConnectionState.Open:
                        _cn.Close();
                        break;
                }
            }

            catch (Exception ex)
            {
                string exceptionstring = ex.Message;
            }

            return ireturn;
        }

	
}