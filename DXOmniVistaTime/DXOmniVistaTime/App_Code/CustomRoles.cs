﻿using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Configuration;

/// <summary>
/// Summary description for Roles
/// </summary>
public class CustomRoles
{
    public CustomRoles()
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString);
        //
        // TODO: Add constructor logic here
        //
    }
    public static DataTable GetActiveRoles()
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString);
        string sql = "Select RoleName from aspnet_Roles where Active = 1";
        return DataAccess.GetDataTableBySqlSyntax(sql, "");
    }
    public static DataTable GetAllRoles()
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString);
        string sql = "Select RoleName from aspnet_Roles where Active = 1";
        return DataAccess.GetDataTableBySqlSyntax(sql, "");
    }
}