﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DXOmniVistaTimeEngine;
using System.Web.Security;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI.HtmlControls;
public partial class OVMaster : System.Web.UI.MasterPage
{
    string reportsFolder = "~/Reports2/";
    
    protected void Page_Load(object sender, EventArgs e)
    {
       WebRequest request = WebRequest.Create(String.Format("http://{0}/WCFOVTimeJSon/RestServiceImpl.svc/ptosummary/{1}",System.Web.Configuration.WebConfigurationManager.AppSettings["JsonServer"].ToString(), HttpContext.Current.User.Identity.Name)) as HttpWebRequest;
       //WebResponse response = request.GetResponse();  

       //this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(sql, "");
       DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);



        string sql = "SELECT ALT.AccessLevelTab from AccessLevelTab ALT INNER JOIN Accesslevel AL ON ALT.AccessLevelID = AL.AccessLevelID INNER JOIN EmployeeDetails ED ON AL.AccessLevelID = ED.AccessLevelID INNER JOIN Employee E ON E.EmployeeID = ED.EmployeeID WHERE E.EmployeeID = '" + Membership.GetUser().UserName + "'";
       DataTable tabs = DataAccess.GetDataTableBySqlSyntax(sql, "");
       
       // Load the notifications
       DataTable Notifications = DataAccess.GetDataTableBySqlSyntax("EXEC Notifications '" + Membership.GetUser().UserName + "', 5", "");

       //li1.Visible = false;
       //li2.Visible = false;
       //li3.Visible = false;
       //li4.Visible = false;
       //li5.Visible = false;

       //Setting the names of the employees in the notifications

       if (Notifications.Rows.Count > 0)
       {
           var string1 = Notifications.Rows[0]["EmployeeID"];
           var string2 = Membership.GetUser().UserName;
       
           addNotification(Notifications, 0, Employee1, Subject1, Date1, li1, Calendar1, Message1);

           if (Notifications.Rows.Count > 1)
           {
               addNotification(Notifications, 1, Employee2, Subject2, Date2, li2, Calendar2, Message2);
           }

           if (Notifications.Rows.Count > 2)
           {
               addNotification(Notifications, 2, Employee3, Subject3, Date3, li3, Calendar3, Message3);
           }

           if (Notifications.Rows.Count > 3)
           {
               addNotification(Notifications, 3, Employee4, Subject4, Date4, li4, Calendar4, Message4);
           }

           if (Notifications.Rows.Count > 4)
           {
               addNotification(Notifications, 4, Employee5, Subject5, Date5, li5, Calendar5, Message5);
           }
       }
       else
       {
           NotificationsList.Visible = false;
       }
       //Date1.Attributes["class"] = "hidden";
       //<ul class="media-list dropdown-content-body width-350">

       liHome.Visible = false;
       liTimeSheet.Visible = false;
       liManager.Visible = false;
       liEmployeeWeeklyReports.Visible = false;
       liSetup.Visible = false;
       liReports.Visible = false;
       liMyReports.Visible = false;

       for(int i=0;i<tabs.Rows.Count;i++) {
           switch (tabs.Rows[i][0].ToString())
           {
               case "Home":
                   liHome.Visible = true;
                   break;
               case "TimeSheet":
                   liTimeSheet.Visible = true;
                   break;
               case "Manager":
                   liManager.Visible = true;
                   break;
               case "EmployeeWeeklyReports":
                   liEmployeeWeeklyReports.Visible = true;
                   break;
               case "Setup":
                   liSetup.Visible = true;
                   break; 
               case "Reports":
                   liReports.Visible = true;
                   NavBarDataBind();
                   break;
               case "MyReports":
                   liMyReports.Visible = false;
                   //NavBarDataBindMyReports();
                   break;
           }
       }     
       
       string path = HttpContext.Current.Request.Url.AbsolutePath;
        if (path== "/time/Default.aspx" || path == "" || path=="/time/default.aspx")
        {
            liHome.Attributes["class"] = "active";
        }
        else if (path == "/time/TimeSheet/OVTimeSheet.aspx")
        {
            liTimeSheet.Attributes["class"] = "active";
        }
        else if (path == "/time/TimeManagement/EmployeesWeeklyHours.aspx")
        {
            liEmployeeWeeklyReports.Attributes["class"] = "active";
        }
    }

    private void addNotification(DataTable Notifications, int row, HtmlGenericControl Employee, HtmlGenericControl Subject, HtmlGenericControl Date, HtmlGenericControl li, HtmlAnchor Calendar, HtmlGenericControl Message)
    {
        if (row < Notifications.Rows.Count)
        {
            if (Notifications.Rows[row]["EmployeeID"].ToString().Equals(Membership.GetUser().UserName, StringComparison.InvariantCultureIgnoreCase))
            {
                Employee.InnerHtml = Notifications.Rows[row]["Employee"].ToString();
            }
            else
            {
                HtmlAnchor employeeAnchor = new HtmlAnchor();
                employeeAnchor.InnerHtml = Notifications.Rows[row]["Employee"].ToString();
                employeeAnchor.HRef = "/time/Setup/SetupEmployee.aspx?Name=" + Notifications.Rows[row]["Employee"].ToString();
                Employee.Controls.Add(employeeAnchor);
            }

            Subject.InnerHtml = Notifications.Rows[row]["Subject"].ToString();

            DateTime Date2 = (DateTime)Notifications.Rows[row]["Date"];

            Date.InnerHtml = ((DateTime)Notifications.Rows[row]["Date"]).ToString("MMM dd");

            message(Calendar, Message, (DateTime)Notifications.Rows[row]["Date"]);
        }
        else
        {
            li.Visible = false;
        }
    }

    //Returns the required message depending on the date given
    private void message(HtmlAnchor Calendar,HtmlGenericControl Message, DateTime date)
    {
        if ((date - DateTime.Now).TotalDays < 0)
        {
            Message.InnerHtml = "has expired";
            Calendar.Attributes.Add("class", "btn border-warning text-warning btn-flat btn-rounded btn-icon btn-sm");
        }
        else if ((date - DateTime.Now).TotalDays < 7)
        {
            Message.InnerHtml = "is due in less than a week";
            Calendar.Attributes.Add("class", "btn border-warning-300 text-warning-300 btn-flat btn-rounded btn-icon btn-sm");
        }
        else
        {
            Message.InnerHtml = "is due in less than a month";
            Calendar.Attributes.Add("class", "btn border-warning-400 text-warning-400 btn-flat btn-rounded btn-icon btn-sm");
        }
    }

    private void NavBarDataBind()
    {
        List<string> fileName = new List<string>();
        List<string> filePath = new List<string>();
        string[] files = Directory.GetFiles(Server.MapPath(reportsFolder));
        foreach (string file in files)
        {
            fileName.Add(Path.GetFileNameWithoutExtension(file));
        }
        foreach (string file in files)
        {
            filePath.Add("time/NewReports.aspx?id=" + Path.GetFileName(file));
        }
        var fileNameArray = fileName.ToArray();
        var filePathArray = filePath.ToArray();

        string currentGroup = "Reports";

        for (int i = 0; i < fileNameArray.Length; i++)
        {
            string gr1 = currentGroup;
            HtmlGenericControl li = new HtmlGenericControl("li");
            HtmlGenericControl a = new HtmlGenericControl("a");

            a.Attributes.Add("href", "\\" + filePathArray[i]);
            a.InnerText = fileNameArray[i];
            li.Attributes.Add("id", "li_Reports" + i.ToString());
            li.Controls.Add(a);

            string path = Request.QueryString["id"];
            if ("time/NewReports.aspx?id=" + path == filePathArray[i])
            {
                li.Attributes.Add("class", "active");
            }

            this.ul_reports.Controls.Add(li);
        }
    }

    private void NavBarDataBindMyReports()
    {
        string currentGroup = "MyReports";

        string gr1 = currentGroup;
        HtmlGenericControl li = new HtmlGenericControl("li");
        HtmlGenericControl a = new HtmlGenericControl("a");

        a.Attributes.Add("href", "\\" + "time/NewMyReports.aspx?id=Employee%20Weekly%20Report.rpt");
        a.InnerText = "Employee Weekly Report";
        li.Attributes.Add("id", "li_Reports0");
        li.Controls.Add(a);

        string path = Request.QueryString["id"];
        if ("time/NewMyReports.aspx?id=" + path == "time/NewMyReports.aspx?id=Employee Weekly Report.rpt")
        {
            li.Attributes.Add("class", "active");
        }

        this.ul_myreports.Controls.Add(li);
    }
}
