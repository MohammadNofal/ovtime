﻿using DevExpress.Web;
using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Runtime.Serialization.Json;
using DevExpress.Utils.OAuth.Provider;
using System.IO;
using System.Text;
using Newtonsoft.Json;

public partial class Vacation : System.Web.UI.Page
{
    private bool isManager;
    public DataTable dt_grid = new DataTable();
    public DataTable dt_employees = new DataTable();
    private Dictionary<String,RootObject> hash = new Dictionary<String,RootObject>();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //Works for first time
        if (!IsPostBack)
        {
            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            Session["HashMap"] = null;
            Session["VacationDataGrid"] = null;
            Session["SelectedValues"] = null;
            Session["PrevYears"] = false;
            isManager = checkifmanager();

            if (isManager)
            {
                GetEmployees();
                AddEmployeesToList();
                refreshDataGrid();
            }
            else
            {
                hidedata();
                GetEmployeeDetailsFromServer("" + Membership.GetUser(User.Identity.Name));
                refreshDataGrid();
            }
        }

        //Works after callback
        else
        {
            //AddEmployeesToList();
        }

    }

    //Mohammad added this
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["VacationDataGrid"] != null)
        {
            this.ASPxGridView1.DataSource = (DataTable)Session["VacationDataGrid"];
            this.ASPxGridView1.DataBind();
        }
    }
    //.......................
    //protected void Page_Init(object sender, EventArgs e)
    //{
    //    // initialize SomeDataTable

    //    ASPxGridView1.DataSource = (DataTable)Session["VacationDataGrid"];
    //    ASPxGridView1.DataBind();
    //}

    private bool checkifmanager() 
    {
        DataTable dt = DataAccess.GetDataTableBySqlSyntax("SELECT EmpFName + EmpLName as 'EmployeeName', EmployeeID FROM Employee WHERE ManagerID='" + Membership.GetUser(User.Identity.Name) + "'", "");
        if (dt.Rows.Count == 0)
        {
            return false;
        }
            return true;
    }

    private void GetEmployees()
    {
        dt_employees = DataAccess.GetDataTableBySqlSyntax("SELECT e.EmployeeID, e.EmpFName + ' ' + e.EmpLName as 'EmployeeName' FROM Employee e WHERE e.ManagerID='" + Membership.GetUser(User.Identity.Name) + "'", "");
        DataTable logedinUser = DataAccess.GetDataTableBySqlSyntax("SELECT e.EmployeeID, e.EmpFName + ' ' + e.EmpLName as 'EmployeeName' FROM Employee e WHERE e.EmployeeID='" + Membership.GetUser(User.Identity.Name) + "'", "");
        
        dt_employees.Merge(logedinUser);
        Session["EmployeeNameData"] = dt_employees;
    }

    private void AddEmployeesToList()
    {
        ASPxListBox list = ASPxDropDownEdit1.FindControl("listBox") as ASPxListBox;
        if (Session["EmployeeNameData"] != null)
        {
            foreach (DataRow row in ((DataTable)Session["EmployeeNameData"]).Rows)
            {

                list.Items.Add(row.Field<String>("EmployeeName"), row.Field<String>("EmployeeID"));//.Add("Text", "Value");

            }
            list.DataBind();
        }
    }

    private void hidedata()
    {
        
        ASPxGridView1.Columns["UsedSickLeaves"].Visible = false;
        ASPxGridView1.Columns["RemainingSickLeaves"].Visible = false;
        ASPxGridView1.Columns["VacationCashLiability"].Visible = false;
        ASPxGridView1.Columns["TotalSalary"].Visible = false;
        ASPxDropDownEdit1.Text = Membership.GetUser(User.Identity.Name).ToString();
        ASPxDropDownEdit1.Enabled = false;
       // ASPxDropDownEdit1.Visible = false;
    }

    protected void GridViewPannel_CallBack(object sender, CallbackEventArgsBase e)
    {
        // e.Parameter is a String containing all SelectedValues in the list separated by a ,
        if (e.Parameter.Equals("false") || e.Parameter.Equals("true"))
        {
            if (e.Parameter.Equals("true")) Session["PrevYears"] = true;
            else Session["PrevYears"] = false;
            //hashmap
            if ((DataTable)Session["VacationDataGrid"] != null)//Session is null initially 
            {
                dt_grid = (DataTable)Session["VacationDataGrid"];
            }
            dt_grid.Clear();
            if (Session["HashMap"] != null && Session["SelectedValues"]!=null)
            {
                String[] selectedvalues = (String [])Session["SelectedValues"];
                foreach (KeyValuePair<String, RootObject> v in (Dictionary<String, RootObject>)(Session["HashMap"]))
                {
                   
                    if (selectedvalues.Contains(v.Key))
                    {
                        DataTable t = CreateEmployeeDetails(v.Value);
                        AddEmployeeRowtoDataTable(t);
                    }
                }
            }
            refreshDataGrid();
        }
        else
        {
            String[] SelectedValues = e.Parameter.Split(',');
            Session["SelectedValues"] = SelectedValues;
            if ((DataTable)Session["VacationDataGrid"] != null)//Session is null initially 
            {
                dt_grid = (DataTable)Session["VacationDataGrid"];
            }
            dt_grid.Clear();
            Session["VacationDataGrid"] = dt_grid;

            for (int i = 0; i < SelectedValues.Length; i++)
            {
                GetEmployeeDetailsFromServer(SelectedValues[i]);
            }


            refreshDataGrid();
        }
    }

    //protected void DisplayPrevYearsStateChanged(object sender, CallbackEventArgsBase e)
    //{
    //    if (e.Parameter.Equals("true")) Session["PrevYears"] = true;
    //    else Session["PrevYears"] = false;
    //    //hashmap
    //    if ((DataTable)Session["VacationDataGrid"] != null)//Session is null initially 
    //    {
    //        dt_grid = (DataTable)Session["VacationDataGrid"];
    //    }
    //    dt_grid.Clear();

    //    foreach(KeyValuePair<String,RootObject> v in (Dictionary<String, RootObject>)(Session["HashMap"])) { 
           
    //        DataTable t = CreateEmployeeDetails(v.Value);
    //        AddEmployeeRowtoDataTable(t);
    //    }
    //    refreshDataGrid();
    //}
    
    //protected void PopUpPannel_CallBack(object sender, CallbackEventArgsBase e)
    //{
    //    //e.Parameter = UserId
    //    VacationDaysPopUP.HeaderText = e.Parameter;
    //    if (Session["HashMap"] != null)
    //    {

    //        var s= (Dictionary<String, RootObject>)(Session["HashMap"]);
    //        RootObject JSONobj = (RootObject) s[e.Parameter];
    //        DataTable vacationdetails = GetVacationDetails(JSONobj);
    //        VacationDaysGrid.DataSource = vacationdetails;
    //        VacationDaysGrid.DataBind();
    //    }

    //    //RootObject JSONobj = (RootObject)hash[e.Parameter];

    //}

    protected void VacationGrid_DataSelect(object sender, EventArgs e)
    {
        ASPxGridView detailGrid = (ASPxGridView)sender;
        String userid = (String)detailGrid.GetMasterRowFieldValues("UserId");
        var s = (Dictionary<String, RootObject>)(Session["HashMap"]);//add try and catch
        RootObject JSONobj = (RootObject) s[userid];
        DataTable vacationdetails = GetVacationDetails(JSONobj);
        detailGrid.DataSource = vacationdetails;
        //detailGrid.DataBind();
    }

    private DataTable GetVacationDetails(RootObject JSONobj)
    {
        DataTable vacationdetails = new DataTable();
        vacationdetails.Columns.Add("VacationType", typeof(String));
        vacationdetails.Columns.Add("VacationDate", typeof(String));
        vacationdetails.Columns.Add("VacationDay", typeof(String));
        vacationdetails.Columns.Add("VacationHours", typeof(String));
        vacationdetails.Columns.Add("HolidayBefore", typeof(String));
        vacationdetails.Columns.Add("HolidayAfter", typeof(String));

        for (int i=0; i<JSONobj.Thisyear.VacationsDetails.Count; i++)
        {
            DataRow row = vacationdetails.NewRow();
            row[0] = JSONobj.Thisyear.VacationsDetails[i].VacationType;
            row[1] = JSONobj.Thisyear.VacationsDetails[i].VacationDate;
            row[2] = JSONobj.Thisyear.VacationsDetails[i].VacationDay;
            row[3] = JSONobj.Thisyear.VacationsDetails[i].VacationHours;
            row[4] = JSONobj.Thisyear.VacationsDetails[i].HolidayBefore;
            row[5] = JSONobj.Thisyear.VacationsDetails[i].HolidayAfter;
            vacationdetails.Rows.Add(row);
        }

        if ((bool)Session["PrevYears"])
        {
            for (int i = 0; i < JSONobj.PreviousYears.VacationsDetails.Count; i++)
            {
                DataRow row = vacationdetails.NewRow();
                row[0] = JSONobj.PreviousYears.VacationsDetails[i].VacationType;
                row[1] = JSONobj.PreviousYears.VacationsDetails[i].VacationDate;
                row[2] = JSONobj.PreviousYears.VacationsDetails[i].VacationDay;
                row[3] = JSONobj.PreviousYears.VacationsDetails[i].VacationHours;
                row[4] = JSONobj.PreviousYears.VacationsDetails[i].HolidayBefore;
                row[5] = JSONobj.PreviousYears.VacationsDetails[i].HolidayAfter;
                vacationdetails.Rows.Add(row);
            }
        }

        return vacationdetails;
    }

    private void refreshDataGrid()
    {
        ASPxGridView1.DataSource = (DataTable)Session["VacationDataGrid"];
        ASPxGridView1.DataBind();
        if (ASPxGridView1.VisibleRowCount == 0) ASPxGridView1.Visible = false;
        else ASPxGridView1.Visible = true;
    }

    private void AddEmployeeRowtoDataTable(DataTable row)
    {
        if (Session["DataTable"] != null) { dt_grid = (DataTable)Session["VacationDataGrid"]; }
        dt_grid.Merge(row);
        Session["VacationDataGrid"] = dt_grid;
    }

    private void GetEmployeeDetailsFromServer(String employeeID)
    {
        if (!employeeID.Equals("") && !employeeID.Equals("SelectAll"))
        {
            //employeeID = "ABBAS.ALSAFFAR"; //Hardcoded ID for testing 
            WebRequest req = WebRequest.Create(@ConfigurationManager.AppSettings["Vacations"] + employeeID);
            req.Method = "GET";
            try
            {
                HttpWebResponse resp = req.GetResponse() as HttpWebResponse;
                Object result;
                if (resp.StatusCode == HttpStatusCode.OK)
                {
                    using (Stream respStream = resp.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(respStream, Encoding.UTF8);
                        // Console.WriteLine(reader.ReadToEnd());
                        result = reader.ReadToEnd();
                        String resultS = (String)result;

                       // resultS = resultS.Insert(0, "[");
                       // resultS = resultS + "]";
                        //Make the JSON into a class (RootObject)
                        RootObject JSONobj = JsonConvert.DeserializeObject<RootObject>(resultS);

                        DataTable employeeDetails = CreateEmployeeDetails(JSONobj); //Create a DataTable from the class

                        AddEmployeeRowtoDataTable(employeeDetails);

                        //add the class to the Hashmap (Dictionary)
                        if (Session["HashMap"] != null)
                        {
                            hash = (Dictionary<String, RootObject>)(Session["HashMap"]);  
                        }
                        if (!hash.ContainsKey(employeeID))
                        {
                            hash.Add(employeeID, JSONobj);
                            Session["HashMap"] = hash;
                        }
                        


                        //DataTable employeeDetails = (DataTable)JsonConvert.DeserializeObject(resultS, (typeof(DataTable)));
                        //AddEmployeeRowtoDataTable(employeeDetails);
                    }
                }
                else
                {
                    //Error connecting to database
                    //Console.WriteLine(string.Format("Status Code: {0}, Status Description: {1}", resp.StatusCode, resp.StatusDescription));
                }
            }
            catch(WebException ex)
            {
                //Error connecting to database
            }
            // Console.Read();
        }
    }

    private DataTable CreateEmployeeDetails(RootObject JSONobj)
    {
        DataTable empDetails = new DataTable();
        empDetails.Columns.Add("UserId", typeof(String));
        empDetails.Columns.Add("HiredDate", typeof(String));
        empDetails.Columns.Add("EarnedVacations", typeof(String));
        empDetails.Columns.Add("UsedVacations", typeof(String));
        empDetails.Columns.Add("RemainingVacations", typeof(String));
        empDetails.Columns.Add("UsedSickLeaves", typeof(String));
        empDetails.Columns.Add("RemainingSickLeaves", typeof(String));
        empDetails.Columns.Add("VacationCashLiability", typeof(String));
        empDetails.Columns.Add("TotalSalary", typeof(String));
        DataRow row = empDetails.NewRow();
        if ((bool)Session["PrevYears"])
        {
            row[0] = JSONobj.Thisyear.UserId;
            row[1] = JSONobj.Thisyear.HiredDate;
            row[2] = float.Parse(JSONobj.Thisyear.EarnedVacations) + float.Parse(JSONobj.PreviousYears.EarnedVacations);
            row[3] = float.Parse(JSONobj.Thisyear.UsedVacations) + float.Parse(JSONobj.PreviousYears.UsedVacations);
            row[4] = float.Parse(JSONobj.Thisyear.RemainingVacations) + float.Parse(JSONobj.PreviousYears.RemainingVacations);
            row[5] = float.Parse(JSONobj.Thisyear.UsedSickLeaves) + float.Parse(JSONobj.PreviousYears.UsedSickLeaves);
            row[6] = float.Parse(JSONobj.Thisyear.RemainingSickLeaves) + float.Parse(JSONobj.PreviousYears.RemainingSickLeaves);
            row[7] = float.Parse(JSONobj.Thisyear.VacationCashLiability) + float.Parse(JSONobj.PreviousYears.VacationCashLiability);
            row[8] = JSONobj.Thisyear.TotalSalary;
        }
        else
        {
            row[0] = JSONobj.Thisyear.UserId;
            row[1] = JSONobj.Thisyear.HiredDate;
            row[2] = JSONobj.Thisyear.EarnedVacations;
            row[3] = JSONobj.Thisyear.UsedVacations;
            row[4] = JSONobj.Thisyear.RemainingVacations;
            row[5] = JSONobj.Thisyear.UsedSickLeaves;
            row[6] = JSONobj.Thisyear.RemainingSickLeaves;
            row[7] = JSONobj.Thisyear.VacationCashLiability;
            row[8] = JSONobj.Thisyear.TotalSalary;
        }
        empDetails.Rows.Add(row);

        return empDetails;
      

        
    }

    private class RootObject //JSON root
    {
        public ThisYear Thisyear { get; set; }
        public Previousyears PreviousYears { get; set; }

    }

    private class Vacationdetails
    {
        public String VacationDate { get; set; }
        public String VacationDay { get; set; }
        public String VacationHours { get; set; }
        public String HolidayBefore { get; set; }
        public String HolidayAfter { get; set; }
        public String VacationType { get; set; }
    }

    private class ThisYear
    {
        public String UserId { get; set; }
        public String HiredDate { get; set; }
        public String EarnedVacations { get; set; }
        public String UsedVacations { get; set; }
        public String RemainingVacations { get; set; }
        public String UsedSickLeaves { get; set; }
        public String RemainingSickLeaves { get; set; }
        public String VacationCashLiability { get; set; }
        public String TotalSalary { get; set; }
        
        public List<Vacationdetails> VacationsDetails { get; set; }

    }

    private class Previousyears
    {
        public String UserId { get; set; }
        public String HiredDate { get; set; }
        public String EarnedVacations { get; set; }
        public String UsedVacations { get; set; }
        public String RemainingVacations { get; set; }
        public String UsedSickLeaves { get; set; }
        public String RemainingSickLeaves { get; set; }
        public String VacationCashLiability { get; set; }
        public String TotalSalary { get; set; }

        public List<Vacationdetails> VacationsDetails { get; set; }
    }


}