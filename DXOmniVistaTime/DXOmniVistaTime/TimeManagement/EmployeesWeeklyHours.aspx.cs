﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using System.Data;
using System.Configuration;
using DXOmniVistaTimeEngine;
using System.Web.Security;
using DevExpress.Data.PivotGrid;
using DevExpress.Web.ASPxPivotGrid;
using DevExpress.XtraPivotGrid;

public partial class EmployeesWeeklyHours : System.Web.UI.Page
{
    //private DateTime _friday = new DateTime();
    private DateTime _toDate = new DateTime();
    //Boolean IsDateChanged = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            _toDate = getToDate(DateTime.Now);
            this.ASPXDateEdit.Date = _toDate;

            //_friday = getFridayDate(DateTime.Now);
            //this.ASPXDateEdit.Date = _friday;
            Session["Data"] = null;
            refreshDataGrid();

        }

        //else if (IsDateChanged)
        //{
        //    _friday = getFridayDate(this.ASPXDateEdit.Date);
        //    this.ASPXDateEdit.Date = _friday;
        //    refreshDataGrid(true);
        //    IsDateChanged = false;
        //}
        else if (Request["__CALLBACKID"] != null && Request["__CALLBACKID"].Contains(this.ASPxPivotGrid1.UniqueID))
        {
            string[] parts = Request["__CALLBACKPARAM"].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            parts[0] = parts[0].Substring(parts[0].IndexOf(":") + 1);
            if (parts[1] != "Checked")
            {
                _toDate = getToDate(this.ASPXDateEdit.Date);
                this.ASPXDateEdit.Date = _toDate;
                refreshDataGrid();
            }
        }
        else if (IsCallback)
        {
            _toDate = getToDate(this.ASPXDateEdit.Date);
            this.ASPXDateEdit.Date = _toDate;
            refreshDataGrid();
        }

        formatGridHeaderSheet();
        //refreshDataGrid();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        _toDate = getToDate(DateTime.Now);
        this.ASPXDateEdit.Date = _toDate;
        refreshDataGrid();
    }

    private void refreshDataGrid(bool reload = false)
    {

        if (this.ASPXDateEdit == null || this.ASPXDateEdit.Text == string.Empty || this.ASPXDateEdit.Date.Year.ToString().Equals("1"))
        {
        }
        else
        {
            _toDate = getToDate(this.ASPXDateEdit.Date);
            DataTable dt = new DataTable();
            if (Session["Data"] == null || reload == true)
            {
                //SQL gets all employees without timeapproval................................................................................................................
                //dt = DataAccess.GetDataTableBySqlSyntax("SELECT e.EmployeeID as 'EmployeeID', e.EmpFName + ' ' + e.EmpLName as 'EmployeeName', t.ProjectID as 'ProjectID' , SUM(case when t.TEDate = '" + _friday.AddDays(-6).ToShortDateString() + "' then t.TEHours else 0 end) Sat, SUM(case when t.TEDate = '" + _friday.AddDays(-5).ToShortDateString() + "' then t.TEHours else 0 end) Sun, SUM(case when t.TEDate = '" + _friday.AddDays(-4).ToShortDateString() + "' then t.TEHours else 0 end )Mon, SUM(case when t.TEDate = '" + _friday.AddDays(-3).ToShortDateString() + "' then t.TEHours else 0 end )Tue, SUM(case when t.TEDate = '" + _friday.AddDays(-2).ToShortDateString() + "' then t.TEHours else 0 end )Wed, SUM(case when t.TEDate = '" + _friday.AddDays(-1).ToShortDateString() + "' then t.TEHours else 0 end) Thu, SUM(case when t.TEDate = '" + _friday.AddDays(-0).ToShortDateString() + "' then t.TEHours else 0 end )Fri ,SUM(case when t.TEDate = '" + _friday.AddDays(-6).ToShortDateString() + "' then t.TEHours else 0 end + case when t.TEDate = '" + _friday.AddDays(-5).ToShortDateString() + "' then t.TEHours else 0 end + case when t.TEDate = '" + _friday.AddDays(-4).ToShortDateString() + "' then t.TEHours else 0 end + case when t.TEDate = '" + _friday.AddDays(-3).ToShortDateString() + "' then t.TEHours else 0 end + case when t.TEDate = '" + _friday.AddDays(-2).ToShortDateString() + "' then t.TEHours else 0 end + case when t.TEDate = '" + _friday.AddDays(-1).ToShortDateString() + "' then t.TEHours else 0 end + case when t.TEDate = '" + _friday.AddDays(-0).ToShortDateString() + "' then t.TEHours else 0 end)Total_hrs From TimeEntry t, Employee e WHERE t.EmployeeID = e.EmployeeID AND (t.TEDate ='" + _friday.AddDays(-6).ToShortDateString() + "' OR t.TEDate = '" + _friday.AddDays(-5).ToShortDateString() + "' OR t.TEDate = '" + _friday.AddDays(-4).ToShortDateString() + "' OR t.TEDate = '" + _friday.AddDays(-3).ToShortDateString() + "' OR t.TEDate = '" + _friday.AddDays(-2).ToShortDateString() + "' OR t.TEDate = '" + _friday.AddDays(-1).ToShortDateString() + "' OR t.TEDate = '" + _friday.AddDays(-0).ToShortDateString() + "') Group by e.EmployeeID, e.EmpFName + ' ' + e.EmpLName, t.ProjectID ", "");
                //....................................................................................................................................
                //String text = _toDate.AddDays(-6).ToShortDateString();
                //SQL gets manager's employees and when the timeapproval status is 0 (Undecided)....................................................................................................
                dt = DataAccess.GetDataTableBySqlSyntax("SELECT e.EmployeeID as 'EmployeeID', e.EmpFName + ' ' + e.EmpLName as 'EmployeeName', t.ProjectID as 'ProjectID' , SUM(case when t.TEDate = '" + _toDate.AddDays(-0).ToShortDateString() + "' then t.TEHours else 0 end) Sat, SUM(case when t.TEDate = '" + _toDate.AddDays(-6).ToShortDateString() + "' then t.TEHours else 0 end) Sun, SUM(case when t.TEDate = '" + _toDate.AddDays(-5).ToShortDateString() + "' then t.TEHours else 0 end )Mon, SUM(case when t.TEDate = '" + _toDate.AddDays(-4).ToShortDateString() + "' then t.TEHours else 0 end )Tue, SUM(case when t.TEDate = '" + _toDate.AddDays(-3).ToShortDateString() + "' then t.TEHours else 0 end )Wed, SUM(case when t.TEDate = '" + _toDate.AddDays(-2).ToShortDateString() + "' then t.TEHours else 0 end) Thu, SUM(case when t.TEDate = '" + _toDate.AddDays(-1).ToShortDateString() + "' then t.TEHours else 0 end )Fri ,SUM(case when t.TEDate = '" + _toDate.AddDays(-0).ToShortDateString() + "' then t.TEHours else 0 end + case when t.TEDate = '" + _toDate.AddDays(-6).ToShortDateString() + "' then t.TEHours else 0 end + case when t.TEDate = '" + _toDate.AddDays(-5).ToShortDateString() + "' then t.TEHours else 0 end + case when t.TEDate = '" + _toDate.AddDays(-4).ToShortDateString() + "' then t.TEHours else 0 end + case when t.TEDate = '" + _toDate.AddDays(-3).ToShortDateString() + "' then t.TEHours else 0 end + case when t.TEDate = '" + _toDate.AddDays(-2).ToShortDateString() + "' then t.TEHours else 0 end + case when t.TEDate = '" + _toDate.AddDays(-1).ToShortDateString() + "' then t.TEHours else 0 end)Total_hrs From TimeEntry t, Employee e WHERE t.EmployeeID = e.EmployeeID AND (   t.TEDate ='" + _toDate.AddDays(-0).ToShortDateString() + "' OR t.TEDate = '" + _toDate.AddDays(-6).ToShortDateString() + "' OR t.TEDate = '" + _toDate.AddDays(-5).ToShortDateString() + "' OR t.TEDate = '" + _toDate.AddDays(-4).ToShortDateString() + "' OR t.TEDate = '" + _toDate.AddDays(-3).ToShortDateString() + "' OR t.TEDate = '" + _toDate.AddDays(-2).ToShortDateString() + "' OR t.TEDate = '" + _toDate.AddDays(-1).ToShortDateString() + "') AND e.ManagerID = '" + Membership.GetUser(User.Identity.Name) + "' AND t.ApprovalStatus = 0 Group by e.EmployeeID, e.EmpFName + ' ' + e.EmpLName, t.ProjectID ", "");
                //....................................................................................................................................
                dt.Columns.Add("Check", typeof(bool));
                foreach (DataRow dr in dt.Rows)
                {
                    dr["Check"] = false;
                }
                Session["Data"] = dt;
            }

            this.ASPxPivotGrid1.DataSource = (DataTable)Session["Data"];
          
            this.ASPxPivotGrid1.DataBind();
            //Mohammad eddited the following
            //if (((DataTable)this.ASPxPivotGrid1.DataSource).Rows.Count == 0)
            if (dt.Rows.Count == 0)
            {
                this.ASPxPivotGrid1.Visible = false;
                TimeSheetsLabel.Visible = true;
            }
            else
            {
                this.ASPxPivotGrid1.Visible = true;
                TimeSheetsLabel.Visible = false;
            }
        }
        ASPxPivotGrid1.CellTemplate = new CellTemplate();
    }

    private void formatGridHeaderSheet()
    {
        if (this.ASPXDateEdit != null && this.ASPXDateEdit.Text != string.Empty && !this.ASPXDateEdit.Date.Year.ToString().Equals("1"))
        {
            _toDate = getToDate(this.ASPXDateEdit.Date);
            this.ASPxPivotGrid1.Fields["Sat"].Caption = "Sat " + _toDate.AddDays(-0).Day.ToString();
            this.ASPxPivotGrid1.Fields["Sun"].Caption = "Sun " + _toDate.AddDays(-6).Day.ToString();
            this.ASPxPivotGrid1.Fields["Mon"].Caption = "Mon " + _toDate.AddDays(-5).Day.ToString();
            this.ASPxPivotGrid1.Fields["Tue"].Caption = "Tue " + _toDate.AddDays(-4).Day.ToString();
            this.ASPxPivotGrid1.Fields["Wed"].Caption = "Wed " + _toDate.AddDays(-3).Day.ToString();
            this.ASPxPivotGrid1.Fields["Thu"].Caption = "Thu " + _toDate.AddDays(-2).Day.ToString();
            this.ASPxPivotGrid1.Fields["Fri"].Caption = "Fri " + _toDate.AddDays(-1).Day.ToString();
        }

    }

    private DateTime getToDate(DateTime passedDate)
    {
        int days = 0;

        string ToDate = ConfigurationManager.AppSettings["TimeSheetToDate"];

        switch (ToDate)
        {
            case "Sunday":
                days = 0;
                break;
            case "Monday":
                days = 1;
                break;
            case "Tuesday":
                days = 2;
                break;
            case "Wednesday":
                days = 3;
                break;
            case "Thursday":
                days = 4;
                break;
            case "Friday":
                days = 5;
                break;
            case "Saturday":
                days = 6;
                break;
        }

        //If the configuration is Saturday
        switch (passedDate.DayOfWeek)
        {
            case DayOfWeek.Sunday:
                return passedDate.AddDays(days);
            case DayOfWeek.Monday:
                return passedDate.AddDays((days - 1) < 0 ? days + 6 : days - 1);
            case DayOfWeek.Tuesday:
                return passedDate.AddDays((days - 2) < 0 ? days + 5 : days - 2);
            case DayOfWeek.Wednesday:
                return passedDate.AddDays((days - 3) < 0 ? days + 4 : days - 3);
            case DayOfWeek.Thursday:
                return passedDate.AddDays((days - 4) < 0 ? days + 3 : days - 4);
            case DayOfWeek.Friday:
                return passedDate.AddDays((days - 5) < 0 ? days + 2 : days - 5);
            case DayOfWeek.Saturday:
                return passedDate.AddDays((days - 6) < 0 ? days + 1 : days - 6);
        }

        return passedDate;

    }

    //private DateTime getFridayDate(DateTime passedDate)
    //{
    //    switch (passedDate.DayOfWeek)
    //    {
    //        case DayOfWeek.Saturday:
    //            return passedDate.AddDays(6);
    //        case DayOfWeek.Sunday:
    //            return passedDate.AddDays(5);
    //        case DayOfWeek.Monday:
    //            return passedDate.AddDays(4);
    //        case DayOfWeek.Tuesday:
    //            return passedDate.AddDays(3);
    //        case DayOfWeek.Wednesday:
    //            return passedDate.AddDays(2);
    //        case DayOfWeek.Thursday:
    //            return passedDate.AddDays(1);
    //        case DayOfWeek.Friday:
    //            return passedDate.AddDays(0);
    //    }

    //    return passedDate;

    //}

    protected void ASPXDateEdit_CustomDisabledDate(object sender, CalendarCustomDisabledDateEventArgs e)
    {
        if (e.Date.DayOfWeek == DayOfWeek.Friday ||
            e.Date.DayOfWeek == DayOfWeek.Sunday ||
            e.Date.DayOfWeek == DayOfWeek.Monday ||
            e.Date.DayOfWeek == DayOfWeek.Tuesday ||
            e.Date.DayOfWeek == DayOfWeek.Wednesday ||
            e.Date.DayOfWeek == DayOfWeek.Thursday)
            e.IsDisabled = true;
       
    }

   
    private class CellTemplate : ITemplate
    {
        public void InstantiateIn(Control container)
        {
            PivotGridCellTemplateContainer templateContainer = container as PivotGridCellTemplateContainer;
            DevExpress.Web.ASPxPivotGrid.PivotGridField field = templateContainer.DataField;
            if (field != null && field.FieldName == "Check")
            {

                CheckBox cb = new CheckBox();
                cb.ID = string.Format("cb_{0}|{1}", templateContainer.Item.ColumnIndex, templateContainer.Item.RowIndex);
                cb.Attributes.Add("onchange", "CheckedChanged()");

                cb.Checked = Int32.Parse(templateContainer.Item.Value.ToString()) == 0 ? false : true;

                templateContainer.Controls.Add(cb);
                object v = templateContainer.GetFieldValue(field);

            }
            else
                templateContainer.Controls.Add(new LiteralControl(templateContainer.Text));
        }
    }

    protected void ASPxPivotGrid1_CustomCallback(object sender, PivotGridCustomCallbackEventArgs e)
    {

        if (e.Parameters == "Refresh")
        {
            //_friday = getFridayDate(this.ASPXDateEdit.Date);
            //this.ASPXDateEdit.Date = _friday;
            //refreshDataGrid(true);

            //change the button back to "select all"

        }
        else if (e.Parameters == "SelectAll")
        {
            DataTable CheckTable = (DataTable)Session["Data"];
            this.ASPxPivotGrid1.DataSource = Session["Data"];
            int NumberofRows = this.ASPxPivotGrid1.VisibleRowsOnPage;
            if (NumberofRows > 1)
            {
               // NumberofRows -= 1; //Removes the "Grand Total" row
            }
            // int page = this.ASPxPivotGrid1.OptionsPager.PageIndex;
            string[] fieldname = new string[2];
            string[] fieldValueEmployeeName = new string[NumberofRows];
            string[] fieldValueProjectID = new string[NumberofRows];

            getAllItemsInPage(NumberofRows, fieldValueEmployeeName, fieldValueProjectID, fieldname);
            ChangeSelectionofSelectedItems(CheckTable, NumberofRows, fieldValueEmployeeName, fieldValueProjectID, fieldname, true);
            Session["Data"] = CheckTable;

            //refreshDataGrid();
            // ASPxPivotGrid1.CellTemplate = new CellTemplate();

            //DataTable CheckTable = (DataTable)Session["Data"];
            //int NumberofRows = this.ASPxPivotGrid1.VisibleRowsOnPage;
            //int page = this.ASPxPivotGrid1.OptionsPager.PageIndex;

            //int startindex = 0;
            //int endindex = -1;

            //if (NumberofRows - 1 >= 20)
            //{
            //    startindex = (page) * (NumberofRows - 1);
            //    endindex = startindex + (NumberofRows - 1);
            //    //drs = CheckTable.Select();
            //}
            //int count = 0;
            ////if (b.Text == "Select All")
            ////{
            //    foreach (DataRow dr in CheckTable.Rows)
            //    {
            //        if (endindex == -1)
            //        {
            //            dr["Check"] = true;
            //        }
            //        else if (count == startindex && startindex < endindex)
            //        {
            //            dr["Check"] = true;
            //            startindex++;
            //        }
            //        if (startindex >= endindex && endindex !=-1)
            //        {
            //            break;
            //        }
            //        count++;
            //    }

        }
        else if (e.Parameters == "UnSelectAll")
        {
            DataTable CheckTable = (DataTable)Session["Data"];
            this.ASPxPivotGrid1.DataSource = Session["Data"];
            int NumberofRows = this.ASPxPivotGrid1.VisibleRowsOnPage;
            if (NumberofRows > 1)
            {
               // NumberofRows -= 1; //Removes the "Grand Total" row
            }
            // int page = this.ASPxPivotGrid1.OptionsPager.PageIndex;
            string[] fieldname = new string[2];
            string[] fieldValueEmployeeName = new string[NumberofRows];
            string[] fieldValueProjectID = new string[NumberofRows];

            getAllItemsInPage(NumberofRows, fieldValueEmployeeName, fieldValueProjectID, fieldname);
            ChangeSelectionofSelectedItems(CheckTable, NumberofRows, fieldValueEmployeeName, fieldValueProjectID, fieldname, false);
            Session["Data"] = CheckTable;

        }
        //else if (e.Parameters == "ApproveSelectedClicked")
        //{
        //    DataTable CheckTable = (DataTable)Session["Data"];
        //    string wherestatement = getSelectedItems(CheckTable);
        //    if (wherestatement == string.Empty)
        //    {
        //        //popup message nothing is selected
        //    }
        //    else
        //    {
        //        string UpdateQuery = "UPDATE TimeEntry set ApprovalStatus = 1, ApprovedBy= '" + Membership.GetUser(User.Identity.Name) + "' " + wherestatement;
        //        DataAccess.ExecuteSqlStatement(UpdateQuery, "");
        //        //update query (Approve = 1) 

        //        //refresh
        //        refreshDataGrid(true);
        //        //Mohamamd changed the following
        //        //if (((DataTable)this.ASPxPivotGrid1.DataSource).Rows.Count == 0)
        //        DataTable t = (DataTable)Session["Data"];
        //        if (t.Rows.Count == 0)
        //        {
        //            this.ASPxPivotGrid1.Visible = false;
        //            TimeSheetsLabel.Visible = true;
        //        }
        //        else
        //        {
        //            this.ASPxPivotGrid1.Visible = true;
        //            TimeSheetsLabel.Visible = false;
        //        }
        //    }
        //}
        //else if (e.Parameters == "DisapproveClicked")
        //{
        //    DataTable CheckTable = (DataTable)Session["Data"];
        //    string wherestatement = getSelectedItems(CheckTable);
        //    if (wherestatement == string.Empty)
        //    {
        //        //popup message nothing is selected
        //    }
        //    else
        //    {
        //        string UpdateQuery = "UPDATE TimeEntry set ApprovalStatus = 2, ApprovedBy= '" + Membership.GetUser(User.Identity.Name) + "' " + wherestatement;
        //        DataAccess.ExecuteSqlStatement(UpdateQuery, "");
        //        //update query (DisApprove = 2)

        //        //refresh
        //        refreshDataGrid(true);

        //        if (((DataTable)this.ASPxPivotGrid1.DataSource).Rows.Count == 0)
        //        {
        //            this.ASPxPivotGrid1.Visible = false;
        //            TimeSheetsLabel.Visible = true;
        //        }
        //        else
        //        {
        //            this.ASPxPivotGrid1.Visible = true;
        //            TimeSheetsLabel.Visible = false;
        //        }
        //    }
        //}
        else
        {
            string[] p = e.Parameters.Split("_|".ToCharArray());
            int columnIndex = int.Parse(p[p.Length - 3]);
            int rowIndex = int.Parse(p[p.Length - 2]);
            bool result = bool.Parse(p[p.Length - 1]);

            DataTable CheckTable = (DataTable)Session["Data"];
            //DataTable dt = new DataTable();
            //dt.Columns.Add("FieldName", typeof(string));
            //dt.Columns.Add("Value");

            this.ASPxPivotGrid1.DataSource = Session["Data"];

            string[] fieldname = new string[2];
            string[] fieldvalue = new string[2];
            int i = 0;
            foreach (DevExpress.Web.ASPxPivotGrid.PivotGridField field in ASPxPivotGrid1.GetFieldsByArea(PivotArea.RowArea))
            {
                //object v= ASPxPivotGrid1.GetFieldValue(field, rowIndex);
                //dt.Rows.Add(field.FieldName, v);
                fieldvalue[i] = (string)ASPxPivotGrid1.GetFieldValue(field, rowIndex);
                fieldname[i] = field.FieldName;
                i++;
            }
            if (fieldname[1] == "ProjectID" && fieldvalue[1] == null)//row is collapsed
            {
                DataRow[] drs = CheckTable.Select("EmployeeName = '" + fieldvalue[0] + "'");
                foreach (DataRow selectedRow in drs)
                {
                    selectedRow["Check"] = result;
                }
            }
            else
            {
                DataRow[] drs = CheckTable.Select("EmployeeName = '" + fieldvalue[0] + "' AND ProjectID = '" + fieldvalue[1] + "'");
                foreach (DataRow selectedRow in drs)
                {
                    selectedRow["Check"] = result;
                }
            }
            Session["Data"] = CheckTable;

        }

        ASPxPivotGrid pivot = (ASPxPivotGrid)sender;
        pivot.ReloadData();

        //refreshDataGrid();
        ASPxPivotGrid1.CellTemplate = new CellTemplate();

    }

    protected void ExpandClicked(object sender, EventArgs e)
    {
        refreshDataGrid();
        this.ASPxPivotGrid1.ExpandAll();
    }

    protected void CollapseClicked(object sender, EventArgs e)
    {
        refreshDataGrid();
        this.ASPxPivotGrid1.CollapseAll();
    }

    protected void GridViewPannel_CallBack(object sender, CallbackEventArgsBase e)
    {
        if (e.Parameter == "Refresh")
        {
            //_toDate = getToDate(this.ASPXDateEdit.Date);
            //this.ASPXDateEdit.Date = _toDate;
            //refreshDataGrid(true);
        }
        //Mohamamd added the following
        else if (e.Parameter == "ApproveSelectedClicked")
        {
            DataTable CheckTable = (DataTable)Session["Data"];
            string wherestatement = getSelectedItems(CheckTable);
            if (wherestatement == string.Empty)
            {
                //popup message nothing is selected
            }
            else
            {
                string UpdateQuery = "UPDATE TimeEntry set ApprovalStatus = 1, ApprovedBy= '" + Membership.GetUser(User.Identity.Name) + "' " + wherestatement;
                DataAccess.ExecuteSqlStatement(UpdateQuery, "");
                //update query (Approve = 1) 

                //refresh
                refreshDataGrid(true);
                //Mohamamd changed the following
                //if (((DataTable)this.ASPxPivotGrid1.DataSource).Rows.Count == 0)
                DataTable t = (DataTable)Session["Data"];
                if (t.Rows.Count == 0)
                {
                    this.ASPxPivotGrid1.Visible = false;
                    TimeSheetsLabel.Visible = true;
                }
                else
                {
                    this.ASPxPivotGrid1.Visible = true;
                    TimeSheetsLabel.Visible = false;
                }
            }
        }
        else if (e.Parameter == "DisapproveClicked")
        {
            DataTable CheckTable = (DataTable)Session["Data"];
            string wherestatement = getSelectedItems(CheckTable);
            if (wherestatement == string.Empty)
            {
                //popup message nothing is selected
            }
            else
            {
                string UpdateQuery = "UPDATE TimeEntry set ApprovalStatus = 2, ApprovedBy= '" + Membership.GetUser(User.Identity.Name) + "' " + wherestatement;
                DataAccess.ExecuteSqlStatement(UpdateQuery, "");
                //update query (DisApprove = 2)

                //refresh
                refreshDataGrid(true);

                if (((DataTable)this.ASPxPivotGrid1.DataSource).Rows.Count == 0)
                {
                    this.ASPxPivotGrid1.Visible = false;
                    TimeSheetsLabel.Visible = true;
                }
                else
                {
                    this.ASPxPivotGrid1.Visible = true;
                    TimeSheetsLabel.Visible = false;
                }
            }
        }
        _toDate = getToDate(this.ASPXDateEdit.Date);
        this.ASPXDateEdit.Date = _toDate;
        refreshDataGrid(true);
        //..............................
    }

    protected void ASPxPivotGrid1_CustomCellStyle(object sender, PivotCustomCellStyleEventArgs e)
    {
        if (e.RowIndex % 2 == 0)
        {
            e.CellStyle.BackColor = System.Drawing.Color.White;
        }
        else
        {
            e.CellStyle.BackColor = System.Drawing.Color.FromArgb(237, 237, 235);
        }

    }

    private string getSelectedItems(DataTable CheckTable)
    {
        String whereStatement = "Where ( ";
        bool nothingisSelected = true;
        int i = 0;
        foreach (DataRow dr in CheckTable.Rows)
        {
            bool check = bool.Parse(dr["Check"].ToString());
            if (check == true)
            {
                whereStatement = whereStatement + "( EmployeeID = '" + dr["EmployeeID"] + "' AND ProjectID = '" + dr["ProjectID"] + "')" + " OR ";
                nothingisSelected = false;
            }
            i++;
        }
        if (nothingisSelected)
        {
            return string.Empty;
        }
        else
        {
            whereStatement = whereStatement.Substring(0, whereStatement.Length - 3); //removes the last OR statement
            whereStatement = whereStatement + ") AND ( TEDate = '" + _toDate.AddDays(-6).ToShortDateString() + "' OR TEDate = '" + _toDate.AddDays(-5).ToShortDateString() + "' OR TEDate = '" + _toDate.AddDays(-4).ToShortDateString() + "' OR TEDate = '" + _toDate.AddDays(-3).ToShortDateString() + "' OR TEDate = '" + _toDate.AddDays(-2).ToShortDateString() + "' OR TEDate = '" + _toDate.AddDays(-1).ToShortDateString() + "' OR TEDate = '" + _toDate.AddDays(-0).ToShortDateString() + "')";

            return whereStatement;
        }

    }

    private void getAllItemsInPage(int NumberofRows, string[] fieldValueEmployeeName, string[] fieldValueProjectID, string[] fieldname)
    {
        int i = 0;
        foreach (DevExpress.Web.ASPxPivotGrid.PivotGridField field in ASPxPivotGrid1.GetFieldsByArea(PivotArea.RowArea))
        {

            for (int j = 0; j < NumberofRows; j++)
            {
                if (field.FieldName == "EmployeeName")
                {
                    fieldValueEmployeeName[j] = (string)ASPxPivotGrid1.GetFieldValue(field, j);
                }
                else if (field.FieldName == "ProjectID")
                {
                    fieldValueProjectID[j] = (string)ASPxPivotGrid1.GetFieldValue(field, j);
                }
            }
            fieldname[i] = field.FieldName;
            i++;
        }
    }

    private void ChangeSelectionofSelectedItems(DataTable CheckTable, int NumberofRows, string[] fieldValueEmployeeName, string[] fieldValueProjectID, string[] fieldname, bool result)
    {
        for (int k = 0; k < NumberofRows; k++)
        {
            if (fieldname[1] == "ProjectID" && fieldValueProjectID[k] == null)//row is collapsed
            {
                DataRow[] drs = CheckTable.Select("EmployeeName = '" + fieldValueEmployeeName[k] + "'");
                foreach (DataRow selectedRow in drs)
                {
                    selectedRow["Check"] = result;
                }
            }
            else
            {
                DataRow[] drs = CheckTable.Select("EmployeeName = '" + fieldValueEmployeeName[k] + "' AND ProjectID = '" + fieldValueProjectID[k] + "'");
                foreach (DataRow selectedRow in drs)
                {
                    selectedRow["Check"] = result;
                }
            }
        }
    }



    //protected void ASPxPivotGrid1_CustomCell(object sender, DevExpress.Web.ASPxPivotGrid.PivotCellDisplayTextEventArgs e)
    //{
    //    if (
    //       (e.ColumnValueType == DevExpress.XtraPivotGrid.PivotGridValueType.GrandTotal) &&
    //       (e.RowValueType == DevExpress.XtraPivotGrid.PivotGridValueType.GrandTotal)
    //      )
    //    {

    //        //e.DisplayText = "";
    //        if (e.ColumnIndex == 0)
    //        {
    //          // e.DataField.Visible = false;
    //           // e.RowField.Visible = false;
    //            //e.ColumnField.Visible = false;

    //        }
    //    }

    //}

}