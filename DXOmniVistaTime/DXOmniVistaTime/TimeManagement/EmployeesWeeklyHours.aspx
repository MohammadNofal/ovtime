﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/OVMaster.master" CodeFile="EmployeesWeeklyHours.aspx.cs" Inherits="EmployeesWeeklyHours" %>

<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v15.2, Version=15.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>

<asp:Content ID="header_" ContentPlaceHolderID="Header" runat="server">
<h4><span class="text-semibold">Time Management / Approve Time Sheets</span></h4>
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPage" runat="server">
    
        <style>
             .dxWeb_pPrevDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNext {
             background: url('../content/images/iconmonstr-arrow-63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pPrev {
             background: url('../content/images/iconmonstr-arrow-64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNextDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }
        .Expand_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 20px !important;
            padding: 1px 1px;
            text-decoration: none !important;
        }
        .Expand_icon:hover 
        {
            color: #7373FF !important;
        }
        .Expand_icon:before 
        {
           content: "\f067" !important;
        }
        .Collapse_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #ee784a !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .Collapse_icon:hover 
        {
            color: #FF8080 !important;
        }
        .Collapse_icon:before
        {
            content: "\f068" !important;
        }
        .Unselected_PageNumber {
             border-radius: 25px;
             border-color: #333333;
             border-width: 2px !important;
             border-style: solid;
             color: #333333 !important;
             background-color:white;
             text-decoration: none !important;
        }
        .Unselected_PageNumber:hover {
            color:white !important;
            background-color:#333333;

        }
        .Current_PageNumber {
            color: white !important;
            border-radius: 25px;
            border-color: #333333;
            border-width: 2px !important;
            border-style: solid;
            background-color: #333333;
            text-decoration: none !important;
            content: "{0}" !important;   
        }
        .CurrentPageNumber:after {
                content: "{0}" !important;
        }
        .Current_PageNumber:hover  {
            color: white !important;
            border-radius: 25px;
            border-color: #333333;
            border-width: 5px;
            background-color: #333333;
            }
        .Button {
              background-image: none !important;
              background-color: #4CAF50;
              color: white;
              padding: 6px 10px;
              border: none;
              border-radius: 4px;
              cursor: pointer;
              margin-top: 8px;
         }
         .ComboBox {
              width: 100%;
              padding: 7px 10px;
              border: 1px solid #ccc;
              border-radius: 4px;
              box-sizing: border-box;
              resize: vertical;
              margin-top: 1px;
         }
         .TextBox
         {
              width: 100%;
              padding: 12px;
              border: 1px solid #ccc;
              border-radius: 4px;
              box-sizing: border-box;
              resize: vertical;
         }
           .TextBoxEdit {
            width: 100%;
            padding: 7px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }
             .newFont * 
        {
            font-family: Calibri;
            font-size: 16px;
        }
         label {
          padding: 12px 12px 12px 0;
          display: inline-block;
         }

         .timesheetlabel{
             font-size: 17px;
         }
         .pagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #333333 !important;
            text-decoration: none !important;
            padding-left: 7px !important;
            padding-right: 7px !important;
        }
        .pagenumber:hover{
            background-color : #333333;
            color: #FFFFFF !important;
        }
        .currentpagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #FFFFFF !important;
            background-color: #333333;
            text-decoration: none !important;
        }
         .dxeButtonEditButton{
            background: none !important;
            border: none !important;
        }
        </style>
        <script>
            var usrName = '<%=HttpUtility.JavaScriptStringEncode(HttpContext.Current.User.Identity.Name)%>';

            function onDateChange(s, e) {

                //ASPxPivotGrid1.PerformCallback("Refresh");
                GridViewPannel.PerformCallback("Refresh");

            }
            function CheckedChanged() {
                ASPxPivotGrid1.PerformCallback("Checked" + '|' + event.target.id + '|' + event.target.checked);
            }
            function SelectAllClicked(s, e) {

                ASPxPivotGrid1.PerformCallback("SelectAll");
            }
            function UnSelectAllClicked(s, e) {

                ASPxPivotGrid1.PerformCallback("UnSelectAll");
            }
            function ApproveSelectedClick(s, e) {
                if (window.confirm("Are you sure you want to approve selected?") == true) {
                    //ASPxPivotGrid1.PerformCallback("ApproveSelectedClicked");
                    GridViewPannel.PerformCallback("ApproveSelectedClicked");
                }
            }
            function DisapproveClicked(s, e) {
                if (window.confirm("Are you sure you want to disapprove selected?") == true) {
                    //ASPxPivotGrid1.PerformCallback("DisapproveClicked");
                    GridViewPannel.PerformCallback("DisapproveClicked");
                }
            }

        </script>
     
    <dx:ASPxCallbackPanel ID="DetailPanel" runat="server" ClientInstanceName="detailPanelSmall" Width="100%" CssClass="detailPanelLarge" Collapsible="false" SettingsLoadingPanel-Enabled="false">
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <Styles>
            <ExpandBar Width="100%" CssClass="bar">
            </ExpandBar>
            <ExpandedExpandBar CssClass="expanded">
            </ExpandedExpandBar>
        </Styles>
          <BorderTop BorderWidth="0px"></BorderTop>
        <PanelCollection>
    <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
        <table style="width:100%">
            <tr>
                <td style="width:30%; ">
        <dx:ASPxDateEdit runat="server" ID="ASPXDateEdit" Caption="Period End Date" AutoPostBack="false" CalendarProperties-ShowClearButton="false" Width="100%"
            OnCustomDisabledDate="ASPXDateEdit_CustomDisabledDate" OnCalendarCustomDisabledDate="ASPXDateEdit_CustomDisabledDate"
            CssClass="TextBoxEdit"  >
               <DropDownButton >
                        <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                    </DropDownButton>
            <ValidationSettings ValidationGroup="DateValidationGroup" Display="Static" ErrorDisplayMode="None">
                <RequiredField IsRequired="True" />
            </ValidationSettings>
            <ClientSideEvents ValueChanged="onDateChange" /> <%--Refresh grid only(callBack) --%>
                </dx:ASPxDateEdit>
        </td>
                <td style="width:70%; padding-bottom: 10px; float:right;text-align: right;">
      <dx:ASPxButton ID="ASPxButton" runat="server" Text="Select All" AutoPostBack="false"  ClientSideEvents-Click="SelectAllClicked" CssClass="Button"></dx:ASPxButton >
      <dx:ASPxButton ID="ASPxButton5" runat="server" Text="Deselect All" AutoPostBack="false" ClientSideEvents-Click="UnSelectAllClicked"  CssClass="Button"> </dx:ASPxButton >
<%--      <dx:ASPxButton ID="ASPxButton5" runat="server" Text="Deselect All" AutoPostBack="false" ClientSideEvents-Click="UnSelectAllClicked"  BackColor="#a6d9ac" HoverStyle-BackColor="#c9e8cd" BackgroundImage-ImageUrl="none"> </dx:ASPxButton >--%>
      <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Approve Selected" AutoPostBack="false" ClientSideEvents-Click="ApproveSelectedClick"  CssClass="Button"> </dx:ASPxButton >
      <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Disapprove Selected" AutoPostBack="false" ClientSideEvents-Click="DisapproveClicked"  CssClass="Button"> </dx:ASPxButton >
      <dx:ASPxButton ID="ASPxButton3" runat="server" Text="" 
          Border-BorderColor="Transparent" BackColor="Transparent" BackgroundImage-ImageUrl="Default.aspx"
           AutoPostBack="false" OnClick="ExpandClicked" CssClass="Expand_icon" Visible="false">
          <Paddings Padding="0px"/>
          <FocusRectPaddings Padding="0px" />
      </dx:ASPxButton> 
                    </td>
      <dx:ASPxButton ID="ASPxButton4" runat="server"  AutoPostBack="false" OnClick="CollapseClicked"  
          Border-BorderColor="Transparent" BackColor="Transparent" BackgroundImage-ImageUrl="Default.aspx"
          CssClass="Collapse_icon" Visible="false">
          <Paddings Padding="0px"/>
          <FocusRectPaddings Padding="0px" />
      </dx:ASPxButton>
                </tr>
            </table>
        </dx:PanelContent>

            </PanelCollection>

            <Paddings Padding="8px" />
            </dx:ASPxCallbackPanel>

     <div style="overflow-y: auto;height: calc(100vh - 267px);">
    <dx:ASPxCallbackPanel ID="GridViewPannel" runat="server" ClientInstanceName="GridViewPannel" Collapsible="false"   
        SettingsLoadingPanel-Enabled ="true" OnCallback="GridViewPannel_CallBack"> 
        <PanelCollection>
            <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
               <%--PIVOT GRID HERE--%>
              
                <dx:ASPxPivotGrid ID="ASPxPivotGrid1" ClientInstanceName="ASPxPivotGrid1" Width="100%" runat="server" 
                    oncustomcallback="ASPxPivotGrid1_CustomCallback" 
                    OnCustomCellStyle="ASPxPivotGrid1_CustomCellStyle" 
                    Border-BorderColor="#dbdbdb" OptionsCustomization-AllowExpand="false"
                     CssClass="newFont"
                                         ><%--OnCustomCellDisplayText="ASPxPivotGrid1_CustomCell" OnCustomCellValue="ASPxPivotGrid1_CustomCellValue"--%>
                    <Styles  >
                        <CellStyle Font-Size="16px" Font-Names="Calibri" BackColor="White" Border-BorderColor="#CFCFCF" />
                     
                        <FieldValueStyle BackColor="#dbdbdb"  Border-BorderColor="#ACACAC" Font-Size="16px" Font-Names="Calibri"/> <%--#9F9F9F--%>

                    </Styles>
                    
                    <OptionsPager CurrentPageNumberFormat="{0}"></OptionsPager>
                    <StylesPager>
                        <PageNumber CssClass="pagenumber"></PageNumber>
                        <CurrentPageNumber CssClass="currentpagenumber"></CurrentPageNumber>
                    </StylesPager>
                   
                  
                    <Fields>
            <dx:PivotGridField Area="RowArea" AreaIndex="0" Caption="Employee Name" FieldName="EmployeeName"
                ID="fieldCategoryName" TotalsVisibility="CustomTotals" Options-AllowDrag="False" Options-AllowFilter="False" >
                
                <HeaderStyle BackColor="#f3f3f2" Border-BorderColor="#9F9F9F" Font-Size="16px" Font-Names="Calibri" HoverStyle-BackColor="White" />
            </dx:PivotGridField>

                        <dx:PivotGridField Area="RowArea"  AreaIndex="1" Caption="Project ID" FieldName="ProjectID" ID="fieldProductName" Options-AllowDrag="False" Options-AllowSort="False" 
                            Options-AllowFilter="False" >
                            <HeaderStyle BackColor="#f3f3f2" Border-BorderColor="#dbdbdb" Font-Size="16px" Font-Names="Calibri" HoverStyle-BackColor="White" />
                       <%--#DCDCDC--%> <%--#9F9F9F--%>
                             </dx:PivotGridField>

                        <dx:PivotGridField Area="DataArea" AreaIndex="0" FieldName="Check" Caption="Check"/>
                        <dx:PivotGridField Area="DataArea" AreaIndex="1" Caption="Sun" FieldName="Sun"/>
                        <dx:PivotGridField Area="DataArea" AreaIndex="2" Caption="Mon" FieldName="Mon"/>
                        <dx:PivotGridField Area="DataArea" AreaIndex="3" Caption="Tue" FieldName="Tue"/>
                        <dx:PivotGridField Area="DataArea" AreaIndex="4" Caption="Wed" FieldName="Wed"/>
                        <dx:PivotGridField Area="DataArea" AreaIndex="5" Caption="Thu" FieldName="Thu" />
                        <dx:PivotGridField Area="DataArea" AreaIndex="6" Caption="Fri" FieldName="Fri">
                            <ValueStyle BackColor="#4CAF50" />
                        </dx:PivotGridField>
                        <dx:PivotGridField Area="DataArea" AreaIndex="7" Caption="Sat" FieldName="Sat">
                            <ValueStyle BackColor="#4CAF50"  />
                        </dx:PivotGridField>
                        <dx:PivotGridField Area="DataArea" AreaIndex="8" Caption="Total" FieldName="Total_hrs"/>
                                           
                    </Fields>
                        <OptionsView ShowContextMenus="false"   ShowFilterHeaders="False" HorizontalScrollBarMode="Auto"  
                            ShowColumnHeaders="False" ShowDataHeaders="False" ShowColumnGrandTotalHeader="False" ShowColumnGrandTotals="False" ShowRowGrandTotals="False" ShowRowGrandTotalHeader="False" />
                       <%--EnableFilterControlPopupMenuScrolling="false"--%>
                        <OptionsFilter  NativeCheckBoxes="false" />
                    
                     <OptionsPager RowsPerPage="20"/>
                    <StylesPager CurrentPageNumber-CssClass="Current_PageNumber" PageNumber-CssClass ="Unselected_PageNumber"  />
                    
                </dx:ASPxPivotGrid>
                  
                <table style="width:100%">
                    <tr>
                        <td style="text-align:center">
                             <dx:ASPxLabel runat="server" ID="TimeSheetsLabel" Text="No timesheets for this date need approval" CssClass="timesheetlabel"></dx:ASPxLabel>
                        </td>
                    </tr>
                </table>
               
                 
                </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
     </div>
      

    </asp:Content>