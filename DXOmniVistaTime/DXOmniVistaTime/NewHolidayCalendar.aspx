﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/OVMaster.master" CodeFile="NewHolidayCalendar.aspx.cs" Inherits="NewHolidayCalendar" %>

<asp:Content ID="header_" ContentPlaceHolderID="Header" runat="server">
<h4><span class="text-semibold">Holiday Calendar</span></h4>
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPage" runat="server">
     <style>
         .delete_icon 
        {
            position: relative;
            font-family: "Courier New" !important;
            color: #ee784a !important;
            font-size: 200% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .delete_icon:hover 
        {
            color: #FF0000 !important;
        }
        .delete_icon::before 
        {
            content: "\000D7" !important;
        }
        .new_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 20px !important;
            padding: 1px 1px;
            text-decoration: none !important;
        }

        .new_icon:hover 
        {
            color: #7373FF !important;
        }
        .new_icon:before 
        {
           content: "\f067" !important;
        }
        .update_icon
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        
        .update_icon:hover
        {
            color: #7373FF !important;
             visibility: visible;
        }
        .update_icon:before 
        {
            content: "\f05d " !important;
        }
         .cancel_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #ee784a !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .cancel_icon:hover 
        {
            color: #FF8080 !important;
        }
        .cancel_icon:before
        {
            content: "\f0e2" !important;
        }
     </style>
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" ClientInstanceName="ASPxGridView1" Width="65%" AutoPostBack="true" KeyFieldName="HolidayName;HolidayDate"
        OnRowInserting = "ASPxGridView1_RowInserting"
        OnRowDeleting = "ASPxGridView1_RowDeleting"
        OnRowUpdating = "ASPxGridView1_RowUpdating"
        OnBeforeColumnSortingGrouping="ASPxGridView1_BeforeColumnSortingGrouping" Border-BorderColor="#dbdbdb" >
        <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
        <Paddings Padding="0px" />
        <Styles Row-BackColor="White" >
            <Header Wrap="True" BackColor="#f3f3f2" Border-BorderColor="#dbdbdb" >

            </Header>
            
            <AlternatingRow BackColor="#ededeb" Enabled="true" />
            <Cell Border-BorderColor="#CFCFCF"/>
            
        </Styles>
       
        <Paddings PaddingLeft="10px" />
        <SettingsPager PageSize="50" />
        <Settings ShowFooter="True" />
        <SettingsBehavior ConfirmDelete="true" />
        <SettingsText ConfirmDelete="Do you want delete this row?" />
        <SettingsEditing Mode="Batch" />
        <Columns>
            <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="False" ShowNewButtonInHeader="True" VisibleIndex="0" Width="1%">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="HolidayName" HeaderStyle-Font-Size="Medium" CellStyle-Font-Size="Medium" Width="16%" CellStyle-HorizontalAlign="Left" VisibleIndex="1">
                <HeaderStyle Font-Size="Medium"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" Font-Size="Medium">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="HolidayDate" HeaderStyle-Font-Size="Medium" CellStyle-Font-Size="Medium" Width="16%" CellStyle-HorizontalAlign="Left" VisibleIndex="2">
                <HeaderStyle Font-Size="Medium"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" Font-Size="Medium">
                </CellStyle>
            </dx:GridViewDataDateColumn>
        </Columns>

        <Paddings PaddingTop="10px" />
        <Border BorderWidth="0px" />
        <BorderBottom BorderWidth="1px" />

         <SettingsCommandButton>
             <DeleteButton Text=" ">
                            <Styles Style-CssClass="delete_icon"></Styles>
                        </DeleteButton>
                        <NewButton Text=" ">
                            <Styles Style-CssClass="new_icon"></Styles>
                        </NewButton>
             <CancelButton Text=" ">
                  <Styles Style-CssClass="cancel_icon"></Styles>
             </CancelButton>
             <UpdateButton Text=" ">
                 <Styles Style-CssClass="update_icon">
                     
                 </Styles>
                 
             </UpdateButton>
             </SettingsCommandButton>
        
    </dx:ASPxGridView>



 </asp:content>

