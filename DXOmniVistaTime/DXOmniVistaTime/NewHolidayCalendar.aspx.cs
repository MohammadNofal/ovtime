﻿using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NewHolidayCalendar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        DataTable table = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["HolidayCalendar"], string.Empty);

        DataColumn[] columns = new DataColumn[2];
        columns[0] = table.Columns["HolidayName"];
        columns[1] = table.Columns["HolidayDate"];

        table.PrimaryKey = columns;

        if (Session["GridDataHoliday"] == null)
        {
            Session["GridDataHoliday"] = table;
        }

        this.ASPxGridView1.DataSource = Session["GridDataHoliday"];
        //this.ASPxGridView1.SettingsPager.Mode = DevExpress.Web.GridViewPagerMode.ShowAllRecords; // this disables pagination, was taking too long if second page clicked
        this.ASPxGridView1.DataBind();
    }

    protected void ASPxGridView1_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ((DataTable)Session["GridDataHoliday"]).Rows.Add(new object[] { e.NewValues["HolidayName"], e.NewValues["HolidayDate"] });
        e.Cancel = true;
        ASPxGridView1.CancelEdit();
        ASPxGridView1.DataBind();

        String query = String.Format("INSERT INTO OMV_HolidayCalendar VALUES ('{0}', '{1}')", e.NewValues["HolidayName"], e.NewValues["HolidayDate"]);
        int success = DataAccess.ExecuteSqlStatement(query, null);
        if (success <= 0) throw new Exception("Error. Row(s) have not been inserted to database.");
    }

    protected void ASPxGridView1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        string newName = e.NewValues["HolidayName"].ToString();
        string newDate = e.NewValues["HolidayDate"].ToString();
        string oldName = e.OldValues["HolidayName"].ToString();
        string oldDate = e.OldValues["HolidayDate"].ToString();

        object[] eventArgs = new object[2] { e.Keys["HolidayName"], e.Keys["HolidayDate"] };
        DataRow row = ((DataTable)Session["GridDataHoliday"]).Rows.Find(eventArgs);
        row["HolidayName"] = newName;
        row["HolidayDate"] = newDate;
        e.Cancel = true;
        ASPxGridView1.CancelEdit();
        ASPxGridView1.DataBind();

        String query = String.Format("UPDATE o SET HolidayName = '{0}', HolidayDate = '{1}' FROM OMV_HolidayCalendar o  WHERE HolidayName = '{2}' AND HolidayDate = '{3}'", newName, newDate, oldName, oldDate);
        int success = DataAccess.ExecuteSqlStatement(query, null);
        if (success <= 0) throw new Exception("Error. Row update(s) have not been saved to database.");
    }

    protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        object[] eventArgs = new object[2] { e.Keys["HolidayName"], e.Keys["HolidayDate"] };
        DataRow row = ((DataTable)Session["GridDataHoliday"]).Rows.Find(eventArgs);
        ((DataTable)Session["GridDataHoliday"]).Rows.Remove(row);
        e.Cancel = true;
        ASPxGridView1.DataBind();

        String query = String.Format("DELETE FROM OMV_HolidayCalendar WHERE HolidayName = '{0}' AND HolidayDate = '{1}'", e.Values["HolidayName"], e.Values["HolidayDate"]);
        int success = DataAccess.ExecuteSqlStatement(query, null);
        if (success <= 0) throw new Exception("Error. Row(s) have not been deleted from database.");
    }

    protected void ASPxGridView1_BeforeColumnSortingGrouping(object sender, DevExpress.Web.ASPxGridViewBeforeColumnGroupingSortingEventArgs e)
    {
        this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["HolidayCalendar"], string.Empty);
        this.ASPxGridView1.DataBind();
    }
}