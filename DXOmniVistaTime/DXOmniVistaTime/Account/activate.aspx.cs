﻿using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_activate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString);
        string token = Request.QueryString["ActivationCode"];
        if ( token !=null && token != String.Empty)
        {
            string sql = "select * from aspnet_Users where UserId ='" + token + "'";
            DataTable user = DataAccess.GetDataTableBySqlSyntax(sql, "");
            if (user.Rows.Count > 0 && user.Rows[0]["Activated"] == DBNull.Value)
            {
                string sql2 = "Update   aspnet_Users set Activated = 1 where UserId ='" + token + "' ";
                DataAccess.ExecuteSqlStatement(sql2, "");
                statues.Text = "Employee activated successfully . Please go to login page and type your user name and password .";
            }
            else if (user.Rows.Count > 0 && user.Rows[0]["Activated"] != DBNull.Value)
            {
                statues.Text = "Sorry, Token activated before exist. Please contact your administrator .";
            }
        }
        else
        {
            statues.Text = "Error";
        }
    }
}