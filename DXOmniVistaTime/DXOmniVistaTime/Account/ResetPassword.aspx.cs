﻿using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_ResetPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnChangePassword_Click(object sender, EventArgs e)
    {
        if (tbPassword.Text.ToString().Equals(tbConfirmPassword.Text.ToString()))//checks if 2 passwords are equal 
        {
            if (tbPassword.Text.ToString().Length >= 6)//checks if pass is 6 or more char
            {
                try
                {
                    // Create a new user after getting the id from the database depending on the key
                    String key = Request.QueryString["key"];

                    DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);

                    //Getting employees data from database and binding it to gridview
                    DataTable employeeid = DataAccess.GetDataTableBySqlSyntax("select employeeid from employeedetails where [key] = '" + key + "'", "");
                    if (employeeid != null && employeeid.Rows.Count > 0) //checks if key has an employeeid (user might manually change the url key)
                    {
                        if (Membership.GetUser(employeeid.Rows[0][0].ToString()) != null)
                        {
                            Membership.DeleteUser(employeeid.Rows[0][0].ToString());
                        }

                        Membership.CreateUser(employeeid.Rows[0][0].ToString(), tbPassword.Text);
                        Response.Redirect(Request.QueryString["ReturnUrl"] ?? "~/Account/Login.aspx");
                    }
                    else //If the key is wrong -> alert user
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "alert", string.Format("alert('{1}', '{0}');", "Entered Key is wrong", "Wrong Key"), true);
                    }
                }
                catch (MembershipCreateUserException exc)
                {
                    if (exc.StatusCode == MembershipCreateStatus.InvalidPassword)
                    {
                        tbPassword.ErrorText = exc.Message;
                      
                        tbPassword.IsValid = false;
                    }
                }
            }
            else
            {
                tbPassword.ErrorText = "Length less than 6";
                tbPassword.IsValid = false;
            }
        }
        else
        {
            tbPassword.ErrorText = "Passwords do not match";
            tbPassword.IsValid = false;
        }
    }
}