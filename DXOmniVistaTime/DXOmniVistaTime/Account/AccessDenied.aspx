<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Light.master" CodeFile="AccessDenied.aspx.cs" Inherits="AccessDenied" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="accountHeader">
    <h2 style="padding-left: 10px">
         Access Denied</h2>
    <p style="padding-left: 10px">
         Sorry, your access is denied.  Please ask your system admin to grant you access. 
        <!--<a href="Register.aspx">Register</a> if you don't have an account.--></p>
</div>
    <form class="login-form" runat="server" style="padding-left: 10px">
    <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Back to Home Page" ValidationGroup="LoginUserValidationGroup"  ImagePosition = "Left" Image-Url="~/Content/Images/key-login-icon.png" Image-Height="20px" Image-Width="20px"
    OnClick="btnLogin_Click"></dx:ASPxButton>

        </form>

</asp:Content>