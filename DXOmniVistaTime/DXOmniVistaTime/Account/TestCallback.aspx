﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TestCallback.aspx.cs" Inherits="Account_TestCallback" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/globalize/0.1.1/globalize.min.js"></script>
    <script type="text/javascript" src="http://cdn3.devexpress.com/jslib/14.2.6/js/dx.chartjs.js"></script>
    

    <script type="text/javascript" src="http://cdn3.devexpress.com/jslib/14.2.6/js/dx.phonejs.js"></script> <!--Mobile UI widgets-->
    <script type="text/javascript" src="http://cdn3.devexpress.com/jslib/14.2.6/js/dx.webappjs.js"></script> <!--Desktop UI widgets-->
    <script type="text/javascript" src="http://cdn3.devexpress.com/jslib/14.2.6/js/dx.all.js"></script> <!--All UI widgets-->
    <link rel="stylesheet" type="text/css" href="http://cdn3.devexpress.com/jslib/14.2.6/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="http://cdn3.devexpress.com/jslib/14.2.6/css/dx.light.css" />
    <link rel="stylesheet" type="text/css" href="../Script/styles.css" />

    <script>
        var globalUsrName = "baher.yousef"
        var cpGlobalProjectID = "1852"
    </script>

    <script type="text/javascript" src="../Script/EmployeeProjectLineChart.js"></script>

    <script>
        function onChange(s, e) {
            
            cpGlobalProjectID = s.GetText();
            globalUsrName = ASPxTextBox1.GetText();
            refreshData();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <dx:ASPxLabel  runat="server" Text="Enter User Name: (ie. baher.yousef or lama.farsakh or mun.park), either tab or click refersh to test the update." />
        <dx:ASPxTextBox ID="ASPxTextBox1" Caption="User Name: (ie. baher.yousef or lama.farsakh or mun.park)" runat="server" Width="170px" ClientEnabled="true" ClientInstanceName="ASPxTextBox1">
            <ClientSideEvents TextChanged="onChange" />
        </dx:ASPxTextBox>
        <dx:ASPxButton ID="JohnButton" Text="RefreshGraph" runat="server" ClientEnabled="true" ClientInstanceName="JohnButton" AutoPostBack="false">
            <ClientSideEvents Click="onChange" />
        </dx:ASPxButton>
        

        <dx:ASPxCallbackPanel ID="CallbackPane" runat="server" ClientInstanceName="CallbackPane" Collapsible="false" OnCallback="CallbackPane_Callback" ScrollBars="Auto"   SettingsLoadingPanel-Enabled="false"> 
            <ClientSideEvents />
            
            <PanelCollection>
                <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                        
                    <dx:ASPxTextBox ID ="PanelText" runat="server" ClientEnabled="true"  ClientInstanceName="PanelText" AutoPostBack ="true" />
                    
                    <div id="lineChartContainer" class="graphContainer" style="float:left;width:45%;"></div>
            
                    
                    
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
    </div>
    </form>
</body>
</html>
