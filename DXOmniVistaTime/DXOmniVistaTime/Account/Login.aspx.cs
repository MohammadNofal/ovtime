using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using DXOmniVistaTimeEngine;
using System.Threading.Tasks;
using System.Configuration;

public partial class Login : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
        //Membership.CreateUser("ADMIN.ADMIN", "password");
        Task t = CreateRoles();//Mohammad added this
        t.Wait();//MOhammad added this

        //Loging out the old user if any

        HttpContext.Current.Session.Abandon();
            FormsAuthentication.SignOut();
        }

        protected void btnLogin_Click(object sender, EventArgs e) {
            if (Membership.ValidateUser(tbUserName.Text, tbPassword.Text))
            {
                if (string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
                {
                    FormsAuthentication.SetAuthCookie(tbUserName.Text, false);               
               
                Response.Redirect("~//Default.aspx");
                }
                else
                    FormsAuthentication.RedirectFromLoginPage(tbUserName.Text, false);
            }
            else
            {
                tbUserName.ErrorText = "Invalid user";
                tbUserName.IsValid = false;
            }
        }
    //Mohammad added this
    private async Task CreateRoles()
    {

        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);

        //Mohammad added the following
        //Get roles from dataTable
        DataTable roles = DataAccess.GetDataTableBySqlSyntax("SELECT * FROM AccessLevel", "");
        //Pre_existing roles
        string[] prevRoles = Roles.GetAllRoles();

        //Roles.DeleteRole("Admin");
        //Roles.DeleteRole("Staff");
        //create roles 
        foreach (DataRow row in roles.Rows)
        {
            if (!prevRoles.Contains(row["AccessLevelName"].ToString()))
            {
                Roles.CreateRole(row["AccessLevelName"].ToString()); //creating roles (ex: Admin, Staff)
            }
        }
        //Check if user is admin or not
        DataTable users = DataAccess.GetDataTableBySqlSyntax("SELECT E.EmployeeID, A.AccessLevelName FROM EmployeeDetails E JOIN AccessLevel A ON E.AccessLevelID=A.AccessLevelID", "");
        foreach (DataRow row in users.Rows)
        {
            if (!Roles.GetUsersInRole(row[1].ToString()).Contains(row[0].ToString()))
            {
               Roles.AddUserToRole(row[0].ToString(), row[1].ToString()); //Add user to the correct role
            }
        }
        //..........................................................................
    }

    //.....................
}