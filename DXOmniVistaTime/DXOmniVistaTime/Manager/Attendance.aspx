﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Main.master" CodeFile="Attendance.aspx.cs" Inherits="Attendance" %>

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <table style="margin:12px;border-spacing:6px;border-collapse:separate; ">
        <tr>
            <td>
    <asp:Label ID="L1" Text="Start Date:&nbsp;&nbsp;" runat="server"/>
                </td>
            <td>
    <dx:ASPxDateEdit ID="StartDate" ClientInstanceName="deStart" runat="server" Paddings-Padding="10px" OnDateChanged="txtDateTime_DateChanged" />
                </td>
            </tr>
        <tr>
            <td>
    <asp:Label ID="L2" Text="End Date:" runat="server"/>
                </td>
            <td>
    <dx:ASPxDateEdit ID="EndDate" ClientInstanceName="deEnd" runat="server" Paddings-Padding="10px" OnDateChanged="txtDateTime_DateChanged" AutoPostBack="True"> 
        <DateRangeSettings StartDateEditID="deStart">
        </DateRangeSettings>
    </dx:ASPxDateEdit>
    </td>
            </tr>
        </table>

    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="True" ClientInstanceName="ASPxGridView1"
    Width="65%" AutoPostBack="true"  
        OnDataBinding ="ASPxGridView1_DataBinding" 
        OnDataBound = "ASPxGridView1_DataBound"
        OnBeforeColumnSortingGrouping="ASPxGridView1_BeforeColumnSortingGrouping">
        <SettingsPager PageSize="50" />
        <Paddings Padding="0px" />
        <Paddings PaddingLeft="10px" />
        <Paddings PaddingTop="10px" />
        <Border BorderWidth="0px" />
        <BorderBottom BorderWidth="1px" />
        <Settings ShowFooter="True" />
        <Styles Header-Wrap="True" />
        <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
        <Columns>
            <dx:GridViewDataTextColumn FieldName="EmployeeName" HeaderStyle-Font-Size="Small" CellStyle-Font-Size="Small" Width="16%" CellStyle-HorizontalAlign="Left" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Vacation" HeaderStyle-Font-Size="Small" CellStyle-Font-Size="Small" Width="16%" CellStyle-HorizontalAlign="Left" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Sick" HeaderStyle-Font-Size="Small" CellStyle-Font-Size="Small" Width="16%" CellStyle-HorizontalAlign="Left" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="UsedVacation" HeaderStyle-Font-Size="Small" CellStyle-Font-Size="Small" Width="16%" CellStyle-HorizontalAlign="Left" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="UsedSick" HeaderStyle-Font-Size="Small" CellStyle-Font-Size="Small" Width="16%" CellStyle-HorizontalAlign="Left" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="HoursWorked" HeaderStyle-Font-Size="Small" CellStyle-Font-Size="Small" Width="16%" CellStyle-HorizontalAlign="Left" VisibleIndex="6">
            </dx:GridViewDataTextColumn> 
        </Columns>
        <Styles>
            <AlternatingRow Enabled="true" />
        </Styles>
    </dx:ASPxGridView>

</asp:Content>
