﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/OVMaster.master" CodeFile="ProjectCost.aspx.cs" Inherits="ProjectCost" %>

<%@ Register Assembly="DevExpress.XtraCharts.v15.2.Web, Version=15.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v15.2, Version=15.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Header" runat="server">
    <h4><span class="text-semibold">Manager / Projects Hours Cost</span></h4>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">

    <style>
         .dxWeb_pPrevDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNext {
             background: url('../content/images/iconmonstr-arrow-63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pPrev {
             background: url('../content/images/iconmonstr-arrow-64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNextDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }
        .month1_border {
            border-left-width: 2px !important;
        }

        .head_container {
            background-color: #EEE;
            float: right;
            margin-right: 7.8%;
        }

        .Month1_css {
            border-style: solid;
            border-width: medium;
            float: right;
            background-color: red;
        }

        .Month2_css {
            border-style: solid;
            border-width: medium;
            float: right;
        }

        .delete_icon {
            position: relative;
            font-family: "Courier New" !important;
            color: #ee784a !important;
            font-size: 200% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }

            .delete_icon:hover {
                color: #FF0000 !important;
            }

            .delete_icon::before {
                content: "\000D7" !important;
            }

        .new_icon {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 20px !important;
            padding: 1px 1px;
            text-decoration: none !important;
        }

            .new_icon:hover {
                color: #7373FF !important;
            }

            .new_icon:before {
                content: "\f067" !important;
            }

        .edit_icon {
            position: relative;
            font-family: FontAwesome !important;
            color: #f3cb76 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }

            .edit_icon:hover {
                color: #FF8000 !important;
            }

            .edit_icon:before {
                content: "\f044" !important;
            }

        .save_icon {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
            text-align: center !important;
        }

            .save_icon:before {
                content: "\f0c7" !important;
            }

        .location_icon {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
        }

            .location_icon:before {
                content: "\f041" !important;
            }

        .book_icon {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
        }

            .book_icon:before {
                content: "\f02d" !important;
            }

        .cancel_icon {
            position: relative;
            font-family: FontAwesome !important;
            color: #ee784a !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }

            .cancel_icon:hover {
                color: #FF8080 !important;
            }

            .cancel_icon:before {
                content: "\f0e2" !important;
            }

        .update_icon {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }

            .update_icon:hover {
                color: #7373FF !important;
            }

            .update_icon:before {
                content: "\f05d " !important;
            }

        .header {
            font-size: 1.25em !important;
        }

        .ProjectDescription {
            font-size: 14px !important;
        }

        .newFont * {
            font-family: Calibri;
            font-size: 16px;
        }

        .TextBoxEdit {
            width: 100%;
            padding: 7px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }

        .SubmitButton {
            background-image: none !important;
            background-color: #4CAF50;
            color: white;
            padding: 6px 10px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            margin-top: 0px;
        }

        .inline {
            display: inline-block;
            background-color: lightblue;
        }

        .float {
            margin-left: 5px;
        }
        .GridTitle{
            font-size:19.5px;
        }
        .pagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #333333 !important;
            text-decoration: none !important;
            padding-left: 7px !important;
            padding-right: 7px !important;
        }
        .pagenumber:hover{
            background-color : #333333;
            color: #FFFFFF !important;
        }
        .currentpagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #FFFFFF !important;
            background-color: #333333;
            text-decoration: none !important;
        }
         .dxeButtonEditButton{
            background: none !important;
            border: none !important;
        }
    </style>
    <script type="text/javascript">
        function SubmitClicked(s, e) {
            GridViewPannel.PerformCallback();
        }
</script>

    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { LoadingPanel.Hide(); }" />
    </dx:ASPxCallback>
    <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel"
        Modal="True">
    </dx:ASPxLoadingPanel>
    
    <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" ClientInstanceName="ComboBoxPanel" Collapsible="false" SettingsLoadingPanel-Enabled="true">
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
                <table style="width:99%;  margin-left: 7px; margin-top:27px;">
                    <tr>
                        <td style="width:18.66%;">
                            <dx:ASPxComboBox Caption="Projects" ID="ProjectComboBox" DropDownStyle="DropDown" runat="server" CssClass="TextBoxEdit" CaptionCellStyle-Paddings-PaddingTop="10px"
                              width="100%"
                                ValueField="ProjectID" TextField="ProjectID" ValidationSettings-Display="Static" ValidationSettings-RequiredField-IsRequired="true" ValidationSettings-ErrorDisplayMode="None">
                                    			<DropDownButton >
                        <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                    </DropDownButton>
                            </dx:ASPxComboBox>
                        </td>
                        <td style="padding-left:7px; text-align:left;">
                            <dx:ASPxButton ID="SubmitButton" Text="Submit" runat="server" AutoPostBack="false" CssClass="SubmitButton">
                                <ClientSideEvents Click="SubmitClicked" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
         <div style="overflow-y: auto;height: calc(100vh - 267px);">
    <dx:ASPxCallbackPanel ID="GridViewPannel" runat="server" ClientInstanceName="GridViewPannel" Collapsible="false" OnCallback="GridViewPannel_CallBack" SettingsLoadingPanel-Enabled="true">
        <PanelCollection>
            <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                <div style="float: left; padding-right: 1px; padding-left: 5px; padding-top: 27px; ">
                    <dx:ASPxGridView
                        ID="DisciplineGridView"
                        runat="server"
                        AutoGenerateColumns="False"
                        Width="650px"
                        AutoPostBack="false"
                        KeyFieldName="DisciplineID"
                        Visible="false">
                         <SettingsPager CurrentPageNumberFormat="{0}">
                    </SettingsPager>
                    <StylesPager>
                        <PageNumber CssClass="pagenumber"></PageNumber>
                        <CurrentPageNumber CssClass="currentpagenumber"></CurrentPageNumber>
                    </StylesPager>
                        <Settings ShowTitlePanel="true" />

                        <SettingsEditing EditFormColumnCount="4" Mode="Inline" />

                        <Styles>
                            <AlternatingRow Enabled="true" />
                            <Header HorizontalAlign="Center"></Header>
                        </Styles>

                        <SettingsPopup>
                            <EditForm Width="100%" Modal="false" />
                        </SettingsPopup>

                        <SettingsPager PageSize="50" />
                        <Paddings Padding="0px" />
                        <Border BorderWidth="0px" />
                        <BorderBottom BorderWidth="1px" />
                        <Settings ShowFooter="True" />
                        <Styles Header-Wrap="True" />
                        <TotalSummary>
                            <dx:ASPxSummaryItem FieldName="Cost" SummaryType="Sum" DisplayFormat="Total: {0:n}" />
                        </TotalSummary>
                        <Columns>
                            <dx:GridViewDataColumn FieldName="Discipline" HeaderStyle-Font-Size="Medium" />
                            <dx:GridViewDataColumn FieldName="Cost" Caption="Cost (AED)" HeaderStyle-Font-Size="Medium" />
                        </Columns>

                        <Templates>
                            <TitlePanel>
                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Project Costs Per Discipline" CssClass="GridTitle"></dx:ASPxLabel>
                            </TitlePanel>
                        </Templates>

                    </dx:ASPxGridView>

                    <dx:WebChartControl ID="DisciplineWebChartControl" runat="server" Height="400px" Visible="false"
                        Width="650px"
                        ClientInstanceName="chart"
                        ToolTipEnabled="False" CrosshairEnabled="True" RenderFormat="Png">
                    </dx:WebChartControl>
                </div>

                <div style="float: left; padding-left: 5px; padding-top: 27px; padding-bottom:10px;">
                    <dx:ASPxGridView
                        ID="RoleGridView"
                        runat="server"
                        AutoGenerateColumns="False"
                        Width="650px"
                        AutoPostBack="false"
                        KeyFieldName="RoleID"
                        Visible="false">

                        <Settings ShowTitlePanel="true" />

                        <SettingsEditing EditFormColumnCount="4" Mode="Inline" />

                        <Styles>
                            <AlternatingRow Enabled="true" />
                            <Header HorizontalAlign="Center"></Header>
                        </Styles>

                        <SettingsPopup>
                            <EditForm Width="100%" Modal="false" />
                        </SettingsPopup>

                        <SettingsPager PageSize="50" />
                        <Paddings Padding="0px" />
                        <Border BorderWidth="0px" />
                        <BorderBottom BorderWidth="1px" />
                        <Settings ShowFooter="True" />
                        <Styles Header-Wrap="True" />
                        <TotalSummary>
                            <dx:ASPxSummaryItem FieldName="Cost" SummaryType="Sum" DisplayFormat="Total: {0:n}" />
                        </TotalSummary>
                        <Columns>
                            <dx:GridViewDataColumn FieldName="Role" HeaderStyle-Font-Size="Medium" />
                            <dx:GridViewDataColumn FieldName="Cost" Caption="Cost (AED)" HeaderStyle-Font-Size="Medium" />
                        </Columns>

                        <Templates>
                            <TitlePanel>
                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Project Costs Per Role" CssClass="GridTitle"></dx:ASPxLabel>
                            </TitlePanel>
                        </Templates>

                    </dx:ASPxGridView>

                    <dx:WebChartControl ID="RoleWebChartControl" runat="server" Height="400px" Visible="false"
                        Width="650px"
                        ClientInstanceName="chart"
                        ToolTipEnabled="False" CrosshairEnabled="True" RenderFormat="Png">
                    </dx:WebChartControl>
                </div>


                <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="ASPxGridView1" />
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
         </div>
</asp:Content>


