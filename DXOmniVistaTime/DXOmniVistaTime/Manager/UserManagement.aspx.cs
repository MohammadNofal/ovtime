﻿using DevExpress.Web;
using DXOmniVistaTimeEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_UserManagement : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["EmployeesDetails"].ToString(), "");
        this.ASPxGridView1.DataBind();
        if (!IsPostBack)
        {

        }

    }
    private void refreshGrid()
    {
        this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["Employees"].ToString(), "");
        this.ASPxGridView1.DataBind();
    }
    protected void ASPxGridView1_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
    {
        // this.ASPxGridView1.UpdateEdit();
    }
    protected void ASPxGridView1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {

        string updateCommand = "";
        string[] keys = new string[e.NewValues.Count];

        e.NewValues.Keys.CopyTo(keys, 0);

        for (int count = 0; count < e.NewValues.Count; count++)
        {
            if (e.NewValues[this.ASPxGridView1.KeyFieldName] == e.NewValues[keys[count]])
            {
                continue;
            }
            string entry = string.Format("{0} = '{1}'", keys[count], e.NewValues[keys[count]]);
            if (count + 1 == e.NewValues.Count)
            {
                updateCommand += entry;
            }
            else
            {
                updateCommand += entry + " , ";
            }

        }
        updateCommand += string.Format(" where EmployeeID='{0}'", e.NewValues[this.ASPxGridView1.KeyFieldName]);
        //DataAccess.UpdateEmployee(updateCommand);
        e.Cancel = true;
        this.ASPxGridView1.CancelEdit();
    }

    protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        e.Cancel = true;
        string command = "Delete from Employee where EmployeeID = " + string.Format("'{0}'", e.Keys["EmployeeID"]);
        DataAccess.ExecuteSqlStatement(command, "");
        refreshGrid();

    }
    protected void ASPxGridView1_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {

        string updateCommand = "Insert INTO Employee  ";
        string[] keys = new string[e.NewValues.Count];

        e.NewValues.Keys.CopyTo(keys, 0);
        string into = " ( ";
        string values = "values ( ";
        for (int count = 0; count < e.NewValues.Count; count++)
        {

            string entry = string.Format("{0} = '{1}'", keys[count], e.NewValues[keys[count]]);

            into += keys[count] + ",";
            values += string.Format("'{0}',", e.NewValues[keys[count]]);
            // updateCommand += entry + " , ";


        }
        if (into[into.Length - 1].CompareTo(',') == 0) { into = into.Remove(into.Length - 1); }
        if (values[values.Length - 1].CompareTo(',') == 0) { values = values.Remove(values.Length - 1); }

        updateCommand += into + ") " + values + ") ";
        DataAccess.ExecuteSqlStatement(updateCommand, "");
        e.Cancel = true;
        this.ASPxGridView1.CancelEdit();

    }
    protected void ASPxGridView1_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    {
        foreach (GridViewColumn column in this.ASPxGridView1.Columns)
        {
            GridViewDataColumn dataColumn = column as GridViewDataColumn;
            if (dataColumn == null) continue;
            if (e.NewValues[dataColumn.FieldName] == null)
            {
                e.Errors[dataColumn] = "Value can't be null.";
            }
        }
        if (e.Errors.Count > 0) e.RowError = "Please, fill all fields.";
        if (e.NewValues["EmpEmail"] != null && !e.NewValues["EmpEmail"].ToString().Contains("@"))
        {
            AddError(e.Errors, this.ASPxGridView1.Columns["EmpEmail"], "Invalid e-mail.");
        }
    }
    void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
    {
        if (errors.ContainsKey(column)) return;
        errors[column] = errorText;
    }
    protected void ASPxGridView1_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (!object.Equals(e.RowType, GridViewRowType.Data)) return;

        bool hasError = string.IsNullOrEmpty(e.GetValue("EmpFName").ToString());
        hasError = hasError || string.IsNullOrEmpty(e.GetValue("EmpLName").ToString());
        hasError = hasError || !e.GetValue("EmpEmail").ToString().Contains("@");
        if (hasError)
        {
            e.Row.ForeColor = System.Drawing.Color.Red;
        }
    }
}