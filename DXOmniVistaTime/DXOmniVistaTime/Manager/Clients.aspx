﻿<%@ Page Title="Manage Clients" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="Clients.aspx.cs" Inherits="Manager_Projects" %>

<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v15.2, Version=15.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <style>
        .templateTable {
        }
        .value {
        }
        .testCom {
            padding: 10px !Important;
            margin-right: 0px !Important;
            border: 0px solid #FFF !important;
        }
        .testCom2 {
             padding: 10px !Important;
            margin-right: 0px !Important;
            border: 2px solid #FFF !important;
            background-color: rgba(234,234,234,1);
        }
    </style>
     <dx:ASPxCardView ID="Clients" runat="server" Width="100%"  AutoGenerateColumns="False" OnCardInserting="Projects_CardInserting" OnCustomButtonCallback="Projects_CustomButtonCallback" KeyFieldName="ClientID" OnBatchUpdate="Clients_BatchUpdate" OnCardDeleting="Clients_CardDeleting" OnCardUpdating="Clients_CardUpdating">
        <Columns>

            
           
            <dx:CardViewColumn FieldName="ClientID" />
            <dx:CardViewColumn FieldName="ClientFedID" />
            <dx:CardViewColumn FieldName="ClientFName" Caption="First Name" />
            <dx:CardViewColumn FieldName="ClientLName" Caption="Last Name" />
            <dx:CardViewColumn FieldName="ClientCompany" VisibleIndex="5" />
            <dx:CardViewColumn FieldName="ClientStreet" VisibleIndex="6" />
            <dx:CardViewColumn FieldName="MobileNumber" VisibleIndex="7" />
            <dx:CardViewColumn FieldName="ClientEmail" VisibleIndex="8" />
            
           
            
        </Columns>
         
        
         
         <SettingsPager AlwaysShowPager="True">
         </SettingsPager>
         <SettingsEditing Mode="EditForm" >
         </SettingsEditing>
        <SettingsSearchPanel Visible="true"  ShowClearButton="true"/>
      
         <Styles>
             <Card BackColor="White">
                 <Border BorderWidth="0px" />
             </Card>

         </Styles>
         <CardLayoutProperties>
             <SettingsAdaptivity   />
             <Styles>
                 <LayoutItem>
                     <Border BorderColor="#cccccc" BorderStyle="Solid" BorderWidth="1px" />
                     <BorderRight BorderColor="red" />
                      
                 </LayoutItem>
                
                
                 
             </Styles>
             <Items>
                 
                 <dx:CardViewCommandLayoutItem HorizontalAlign="Right" ShowDeleteButton="True"  ShowEditButton="true"  CssClass="testCom">
                     <CustomButtons>
                         
                         <dx:CardViewCustomCommandButton Text="Projects" ID="ManageProjects"></dx:CardViewCustomCommandButton>

                     </CustomButtons>
                    
                 </dx:CardViewCommandLayoutItem>
                  
                 <dx:CardViewColumnLayoutItem ColumnName="ClientID" CssClass="testCom2">
                 </dx:CardViewColumnLayoutItem>                
                 <dx:CardViewColumnLayoutItem ColumnName="ClientFedID" CssClass="testCom2">
                 </dx:CardViewColumnLayoutItem>
                 <dx:CardViewColumnLayoutItem ColumnName="ClientFName" CssClass="testCom2">
                 </dx:CardViewColumnLayoutItem>
                 <dx:CardViewColumnLayoutItem ColumnName="ClientLName" CssClass="testCom2">
                 </dx:CardViewColumnLayoutItem>
                 <dx:CardViewColumnLayoutItem ColumnName="ClientCompany" CssClass="testCom2">
                 </dx:CardViewColumnLayoutItem>
                 <dx:CardViewColumnLayoutItem ColumnName="ClientStreet" CssClass="testCom2">
                 </dx:CardViewColumnLayoutItem>
                 <dx:CardViewColumnLayoutItem ColumnName="MobileNumber" CssClass="testCom2">
                 </dx:CardViewColumnLayoutItem>
                 <dx:CardViewColumnLayoutItem ColumnName="ClientEmail" CssClass="testCom2">
                 </dx:CardViewColumnLayoutItem>
                  
                 
             </Items>
         </CardLayoutProperties>
         <Styles>
             <Card BackColor="White">
             </Card>
         </Styles>
    </dx:ASPxCardView>
</asp:Content>

