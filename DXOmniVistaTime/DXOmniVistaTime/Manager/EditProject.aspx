﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="EditProject.aspx.cs" Inherits="Manager_EditProjet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <dx:ASPxCallbackPanel ID="DetailPanel" runat="server" ClientInstanceName="detailPanelSmall" Width="100%" CssClass="detailPanelLarge" Collapsible="false"  SettingsLoadingPanel-Enabled ="false" >
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <Styles>
            <ExpandBar Width="100%" CssClass="bar">
            </ExpandBar>
            <ExpandedExpandBar CssClass="expanded">
            </ExpandedExpandBar>
        </Styles>
        <BorderTop BorderWidth="0px">
        </BorderTop>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                Project Details [<span id="ProjectNameSpan" ></span>]
                <br />               
               <div class="profile">
                        <dx:ASPxTextBox ID="EmployeeID" Caption="Employee ID" runat="server" Enabled ="false" AutoPostBack="false" 
                        CssClass="editor" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption">
                       </dx:ASPxTextBox>
               </div>
              <div class="profile">
                        <dx:ASPxTextBox ID="LoginName" Caption="Login Name" runat="server" Enabled ="false" AutoPostBack="false" 
                        CssClass="editor" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption">      
                       </dx:ASPxTextBox>
               </div>
                <div class="profile">
                        <dx:ASPxTextBox ID="Email" Caption="Email" runat="server"  AutoPostBack="false" 
                        CssClass="editor" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption">                   
                       </dx:ASPxTextBox>
               </div>   
            </dx:PanelContent>
        </PanelCollection>
        <Paddings Padding="8px" />
    </dx:ASPxCallbackPanel>
        
     
</asp:Content>

