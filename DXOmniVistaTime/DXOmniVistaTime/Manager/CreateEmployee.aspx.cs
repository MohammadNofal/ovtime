﻿using DevExpress.Web;
using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_CreateEmployee : System.Web.UI.Page
{
    protected DataTable projects_;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && !IsCallback)
        {
            string EmployeeID = Request.QueryString["id"].ToString();
            InsertIntoEmployee(EmployeeID);
            DataRow Employee = DataAccess.GetDataTableBySqlSyntax("select * from Employee where EmployeeID ='" + EmployeeID, "'").Rows[0];           
            
            MembershipUser user = Membership.GetUser(EmployeeID);
            this.roles.DataSource = CustomGetRoles();

            ////load Projects
            string sql = "Select e.*  from EmployeeControl e  Left Join Project p on p.ProjectID = e.ControlID  where e.EmployeeID = '" + EmployeeID + "' and e.ControlType = 1 and p.ParentProjectID is null";
            projects_ = DataAccess.GetDataTableBySqlSyntax(sql, "");           
            this.managers.DataSource = Roles.GetUsersInRole("Manager");
            this.managers.DataBind();
            DataBind();
            PrintAccess(user);
            ShowDataOnView(Employee);

        }
       
       
    }
    public void submitButton_Click(object sender,EventArgs e)
    {
      
      DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
      string EmpID = this.EmployeeID.Text.ToString();
      string EmpFName = this.EmpFName.Text.ToString();
      string EmpLName = this.EmpLName.Text.ToString();
      string EmployeeID = this.EmployeeID.Text.ToString();
      string EmpTitle = this.EmpTitle.Text.ToString();
      string EmpStreet = this.EmpStreet.Text.ToString();
      string EmpStreet2 = this.EmpStreet2.Text.ToString();
      string EmpCity = this.EmpCity.Text.ToString();
      string EmpState = this.EmpState.Text.ToString();
      string EmpZip = this.EmpZip.Text.ToString();
      string EmpDateHired = this.EmpDateHired.Text.ToString();
      string EmpContact = this.EmpContact.Text.ToString();
      string EmpDepartment = this.EmpDepartment.Text.ToString();
      string EmpBillRate = this.EmpBillRate.Text.ToString();
      string EmpCostRate = this.EmpCostRate.Text.ToString();
      string EmpMemo = this.EmpMemo.Text.ToString();
      string EmpOTCostRate = this.EmpOTCostRate.Text.ToString();
      string EmpOTBillRate = this.EmpOTBillRate.Text.ToString();
      string EmpNextImpDate = this.EmpNextImpDate.Text.ToString();
      string EmpVac = this.EmpVac.Text.ToString();
      string EmpSick = this.EmpSick.Text.ToString();
      string EmpHol = this.EmpHol.Text.ToString();
      string ManagerID = "";
      if (this.managers.SelectedItem != null)
      {
          ManagerID = this.managers.SelectedItem.Text;
      }
      

      string SQL = "Update Employee set EmpFName='" + EmpFName + "' , EmpLName= '" + EmpLName + "' , EmpEmail= '" + this.AccessEmail.Text + "',EmpTitle='" + EmpTitle + "' ,EmpStreet='" + EmpStreet + "'" + ",EmpStreet2='" + EmpStreet2;
      SQL += "'" + ",EmpCity='" + EmpCity + "'" + " ,EmpState='" + EmpState + "'" + " ,EmpZip='" + EmpZip;
      SQL += "'" + " ,EmpDateHired='" + EmpDateHired + "'" + " ,EmpContact='" + EmpContact + "'";
      SQL += " ,EmpDepartment='" + EmpDepartment + "'" + " ,EmpBillRate='" + EmpBillRate + "'";
      SQL += " ,EmpCostRate='" + EmpCostRate + "'" + " ,EmpMemo='" + EmpMemo;
      SQL += "' ,EmpOTCostRate='" + EmpOTCostRate + "'" + " ,EmpOTBillRate='" + EmpOTBillRate + "'" + " ,EmpNextImpDate='" + EmpNextImpDate;
      SQL += "' ,EmpVac='" + EmpVac + "'" + " ,EmpSick='" + EmpSick + "'" + " ,EmpHol='" + EmpHol+"'" +" , ManagerID = '"+ManagerID+"'";
      SQL += " where EmployeeID = '" + EmployeeID + "'";
     
      
      DataAccess.ExecuteSqlStatement(SQL,"");

/////////update  password here
      MembershipUser user = Membership.GetUser(EmployeeID);
      if(this.AccessPassword.Text != "**********")
      {
          string oldpass = user.ResetPassword();
          user.ChangePassword(oldpass, this.AccessPassword.Text);
      }


      foreach (ListEditItem li in this.roles.SelectedItems)
      {
          if (!Roles.IsUserInRole(user.UserName,li.Text))
          {
              Roles.AddUserToRole(user.UserName, li.Text);
          }

      }
      foreach (ListEditItem li in this.roles.Items)
      {
          if(!li.Selected)
          {
              if (Roles.IsUserInRole(user.UserName, li.Text))
              {
                  Roles.RemoveUserFromRole(user.UserName, li.Text);
              }
          }
        }

      ////update email  here
      string query = "Update aspnetdb.dbo.aspnet_Membership Set aspnetdb.dbo.aspnet_Membership.Email = '" + this.AccessEmail.Text + "'  where  aspnetdb.dbo.aspnet_Membership.UserId ='"+ user.ProviderUserKey+"'";
      DataAccess.ExecuteSqlStatement(query,"");

    }
    private void ShowDataOnView(DataRow Employee)
        {
            this.EmployeeID.Text = Employee["EmployeeID"].ToString();
            this.LoginName.Text = Employee["LoginID"].ToString();
            this.Email.Text = Employee["EmpEmail"].ToString();
            this.EmpFName.Text = Employee["EmpFName"].ToString();
            this.EmpLName.Text = Employee["EmpLName"].ToString();
            this.EmpTitle.Text = Employee["EmpTitle"].ToString();
            this.EmpStreet.Text = Employee["EmpStreet"].ToString();
            this.EmpStreet2.Text = Employee["EmpStreet2"].ToString();
            this.EmpCity.Text = Employee["EmpCity"].ToString();
            this.EmpState.Text = Employee["EmpState"].ToString();
            this.EmpZip.Text = Employee["EmpZip"].ToString();
            this.EmpDateHired.Text = Employee["EmpDateHired"].ToString();
            this.EmpContact.Text = Employee["EmpContact"].ToString();
            this.EmpDepartment.Text = Employee["EmpDepartment"].ToString();
            this.EmpBillRate.Text = Employee["EmpBillRate"].ToString();
            this.EmpCostRate.Text = Employee["EmpCostRate"].ToString();
            this.EmpMemo.Text = Employee["EmpMemo"].ToString();
            this.EmpOTCostRate.Text = Employee["EmpOTCostRate"].ToString();
            this.EmpOTBillRate.Text = Employee["EmpOTBillRate"].ToString();
            this.EmpNextImpDate.Text = Employee["EmpNextImpDate"].ToString();
            this.EmpVac.Text = Employee["EmpVac"].ToString();
            this.EmpSick.Text = Employee["EmpSick"].ToString();
            this.EmpHol.Text = Employee["EmpHol"].ToString();
            this.spanName.Text = string.Format("{0} {1}", Employee["EmpFName"], Employee["EmpLName"]);
            if (Employee["ManagerID"].ToString() != "")
            {
                this.managers.Items.FindByText(Employee["ManagerID"].ToString()).Selected = true ;
            }
        }
    private void PrintAccess(MembershipUser User)
        {
           
            this.Email.Text = User.Email;
            this.EmployeeID.Text = User.UserName;
            this.LoginName.Text = User.UserName;
            ////for editing
            this.AccessEmail.Text = User.Email;
            this.AccessLogin.Text = User.UserName;
            this.AccessPassword.Text = "**********";
            string[] roles = Roles.GetRolesForUser(User.UserName);
           foreach(string role in roles){
            this.roles.Items.FindByValue(role).Selected = true ;
           }
        
         

        }
    private bool InsertIntoEmployee(string EmployeeID)
    {
        ///get user data
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        string query = string.Format("select Count(1) from Employee where EmployeeID = '{0}'", EmployeeID);
        int count = Int32.Parse(DataAccess.GetDataTableBySqlSyntax(query, "").Rows[0].ItemArray[0].ToString());
        if (count == 0)
        {
            query = string.Format("select aspnetdb.dbo.aspnet_Membership.UserId, aspnetdb.dbo.aspnet_Membership.Email ,aspnetdb.dbo.aspnet_Membership.Password ,aspnetdb.dbo.aspnet_Users.UserName From  aspnetdb.dbo.aspnet_Users join aspnetdb.dbo.aspnet_Membership on aspnetdb.dbo.aspnet_Membership.UserId = aspnetdb.dbo.aspnet_Users.UserId and aspnetdb.dbo.aspnet_Users.UserName = '{0}'", EmployeeID);
            DataTable employee = DataAccess.GetDataTableBySqlSyntax(query, "");
            string employeeUserName = employee.Rows[0]["UserName"].ToString();
            string employeeUserEmail = employee.Rows[0]["Email"].ToString();
            string employeePassword = employee.Rows[0]["Password"].ToString();
            query = string.Format("insert into Employee(EmployeeID,LoginID,EmpEmail) values ('" + employeeUserName + "','" + EmployeeID + "','" + employeeUserEmail + "')");
            DataAccess.ExecuteSqlStatement(query, "");          
            return false;
        }
        return true;      
    }
    private string[] CustomGetRoles()
    {
        string user = Membership.GetUser(User.Identity.Name).UserName;

        string[] roles = Roles.GetAllRoles();
        if (Roles.IsUserInRole(user, "Administrator"))
        {
            return roles;
        }
        if (Roles.IsUserInRole(user, "Manager"))
        {
            List<string> list = new List<string>();
            list = roles.ToList<string>();
            list.Remove("Administrator");
            return list.ToArray();
        }


        return null;
    }

    
}