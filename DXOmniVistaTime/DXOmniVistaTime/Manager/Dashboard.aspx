﻿<%@ Page Title="OVTime : Dashboard" Language="C#" AutoEventWireup="true" MasterPageFile="~/Nonav.master" CodeFile="Dashboard.aspx.cs" Inherits="Dashboard" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="WcfOmniVistaTimeRESTfulJSonService" %>

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">

    <script>
        var usrName = '<%=HttpUtility.JavaScriptStringEncode(HttpContext.Current.User.Identity.Name)%>';
    </script>
    <script>
        function onComboChange(s, e) {
            ChartProjectID = s.GetSelectedItem().value;
            //alert(s.GetSelectedItem().value);
            detailPanelSmallNav.PerformCallback();
        }

    </script>

    <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" FixedPosition="WindowLeft" ClientInstanceName="leftPane" Width="300px" CssClass="leftPane2" Collapsible="true" ScrollBars="None" SettingsLoadingPanel-Enabled="false" Paddings-Padding="0">
        <SettingsAdaptivity CollapseAtWindowInnerWidth="1023" />
        <ClientSideEvents/>
        <SettingsLoadingPanel Enabled="true">
        </SettingsLoadingPanel>
        
        <PanelCollection>
            <dx:PanelContent ID="PanelContent3" runat="server" SupportsDisabledAttribute="True">
                <dx:ASPxPanel ID="ASPxPanel2" runat="server" CssClass="detailPanelSmallHeaderBlue"></dx:ASPxPanel>
                <dx:ASPxPanel ID="ASPxPanel3" runat="server" CssClass="detailPanelSmallBlue">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                          Project List:
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>

                <br />
                            
                <table>
                    <tr>
                        <th><dx:ASPxComboBox ID="comboProjectId" runat="server" Caption=" " OnTextChanged="comboProjectId_TextChanged" ClientEnabled="true" ClientInstanceName="comboProjectId" AutoPostBack="false">
                            <ClientSideEvents TextChanged="onComboChange" />
                            </dx:ASPxComboBox>  </th>
                        <th>&nbsp; </th>
                        <th><dx:ASPxButton ID="Submit" runat="server" RenderMode="Link" Image-Url="~/Content/Images/Icons/search-icon.png" Image-Height="20px" Image-Width="20px" Height="34px"></dx:ASPxButton></th>
                    </tr>
                </table>
                             
                <br />
                <dx:ASPxPanel ID="ASPxPanel4" runat="server" CssClass="detailPanelSmallHeaderBlue">
                </dx:ASPxPanel>
                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel2" runat="server" ClientInstanceName="detailPanelSmallNav" Width="100%" CssClass="detailPanelNoBgMargin" Collapsible="false"  SettingsLoadingPanel-Enabled ="false" >
                    <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
                    <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
                    <Styles>
                        <ExpandBar Width="100%" CssClass="bar">
                        </ExpandBar>
                        <ExpandedExpandBar CssClass="expanded">
                        </ExpandedExpandBar>
                        
                    </Styles>
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent5" runat="server" SupportsDisabledAttribute="True">
                            <dx:ASPxNavBar Width="100%" EnableViewState="False" CssClass="LeftNavBar" Visible="true"
                                        ID="navbar" runat="server" AutoCollapse="False" EncodeHtml="False" AllowSelectItem="False" ItemStyle-HoverStyle-BackColor ="LightGray" >
                                <GroupHeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" >
                                <HoverStyle BackColor="LightGray">
                                </HoverStyle>
                                </ItemStyle>
                                <ItemTextTemplate>
                                    <span style="vertical-align: top; display: block; margin: 1px 0 0 1px"><%# Eval("Text") %></span>
                                </ItemTextTemplate>
                            </dx:ASPxNavBar>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>

            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>

    

    

    <dx:ASPxPanel ID="ASPxPanel1" runat="server" CssClass="detailPanelSmallHeader">
    </dx:ASPxPanel>
    <dx:ASPxCallbackPanel ID="DetailPanel" runat="server" ClientInstanceName="detailPanelSmall" Width="100%" CssClass="detailPanelSmall" Collapsible="false"  SettingsLoadingPanel-Enabled ="false">
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <Styles>
            <ExpandBar Width="100%" CssClass="bar">
            </ExpandBar>
            <ExpandedExpandBar CssClass="expanded">
            </ExpandedExpandBar>
        </Styles>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
                Sub Project Detail:
                <br />
                <table>
                    <tr>
                    <th><dx:ASPxTextBox ID="TotalHours" runat="server" Caption=" Total Hours:"></dx:ASPxTextBox></th>
                    <th>&nbsp;</th>
                        <th><dx:ASPxTextBox ID="TotalApprovedHours" runat="server" Caption=" Total Approved Hours:"></dx:ASPxTextBox></th>
                    <th>&nbsp;</th>
                        <th><dx:ASPxTextBox ID="TotalUnapprovedHours" runat="server" Caption=" Total Unapproved Hours:"></dx:ASPxTextBox></th>
                    </tr>
                </table>                
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>

    <dx:ASPxPanel ID="ASPxPanel5" runat="server" ClientInstanceName="detailPanel" Width="100%" CssClass="detailPanel" Collapsible="False" Paddings-Padding="0">
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <Styles>
            <ExpandBar Width="100%" CssClass="bar"></ExpandBar>
            <ExpandedExpandBar CssClass="expanded"></ExpandedExpandBar>
        </Styles>
        <BorderTop BorderWidth="0px"></BorderTop>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent6" runat="server" SupportsDisabledAttribute="True">
                <!--<div id="projectChartContainer" class="graphContainer" style="float:left;width:40%;" ></div>
                <div id="barChartBillable" class="graphContainer" style="float:right;width:40%;" ></div>
                <div id="barChartProfit" class="graphContainer" style="float:left;width:40%;padding:30px;" ></div>
                <div id="lineChartContainer2" class="graphContainer" style="float:right;width:40%;padding:30px;" ></div>
                <div id="barChartHours" class="graphContainer" style="float:left;width:40%;padding:30px;" ></div>
                -->

                <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="True" ClientInstanceName="ASPxGridView1"
                    Width="100%" AutoPostBack="true" >
                    <%--OnDataBinding ="ASPxGridView1_DataBinding" 
                    OnDataBound = "ASPxGridView1_DataBound" 
                    OnBeforeColumnSortingGrouping="ASPxGridView1_BeforeColumnSortingGrouping"
                    Onsummarydisplaytext="ASPxGridView1_SummaryDisplayText"> --%>
                    <SettingsPager PageSize="50" />
                    <Paddings Padding="0px" />
                    <Border BorderWidth="0px" />
                    <BorderBottom BorderWidth="1px" />
                    <Settings ShowFooter="True" />
                    <Styles Header-Wrap="True" />
                    <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="ProjectID" VisibleIndex="0">
                            <HeaderCaptionTemplate>
                                <table>
                                    <tr>
                                    <th><dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="~/Content/Images/Icons/Write-Document-icon.png" /></th>
                                    <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Project Id" ForeColor="White"/></th>
                                    </tr>
                                </table>
                            </HeaderCaptionTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="ProjectName" VisibleIndex="1">
                            <HeaderCaptionTemplate>
                                <table>
                                    <tr>
                                    <th><dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="~/Content/Images/Icons/Remove-Document-icon.png" /></th>
                                    <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Project Name" ForeColor="White"/></th>
                                    </tr>
                                </table>
                            </HeaderCaptionTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="TotalHours" VisibleIndex="4">
                            <HeaderCaptionTemplate>
                                <table>
                                    <tr>
                                    <th><dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="~/Content/Images/Icons/Internet-History-icon.png" /></th>
                                    <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Total Hours" ForeColor="White"/></th>
                                    </tr>
                                </table>
                            </HeaderCaptionTemplate>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <Styles>
                        <AlternatingRow Enabled="true" />
                    </Styles>
                </dx:ASPxGridView>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxPanel>

    <dx:ASPxPanel ID="ASPxPanel6" runat="server" CssClass="detailPanelSmallHeader">
    </dx:ASPxPanel>
    <dx:ASPxCallbackPanel ID="ASPxCallbackPanel3" runat="server" ClientInstanceName="detailPanelSmall" Width="100%" CssClass="detailPanelSmall" Collapsible="false"  SettingsLoadingPanel-Enabled ="false" >
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <Styles>
            <ExpandBar Width="100%" CssClass="bar">
            </ExpandBar>
            <ExpandedExpandBar CssClass="expanded">
            </ExpandedExpandBar>
        </Styles>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                Employee Detail:
                <br />
                <br />
                <dx:ASPxButton ID="ASPxButton2" runat="server" Text="View Detailed Time Sheet" ImagePosition = "Right" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption"  AutoPostBack="false" Image-Url="~/Content/Images/Icons/check-64-icon.png" Image-Height="20px" Image-Width="20px" Height="36px">
                </dx:ASPxButton>
                <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Approve Hours" ImagePosition = "Right" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption"  AutoPostBack="false" Image-Url="~/Content/Images/Icons/check-64-icon.png" Image-Height="20px" Image-Width="20px" Height="36px">
                </dx:ASPxButton>
                
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>

    <dx:ASPxPanel ID="ASPxPanel7" runat="server" ClientInstanceName="detailPanel" Width="100%" CssClass="detailPanel" Collapsible="False" Paddings-Padding="0">
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <Styles>
            <ExpandBar Width="100%" CssClass="bar"></ExpandBar>
            <ExpandedExpandBar CssClass="expanded"></ExpandedExpandBar>
        </Styles>
        <BorderTop BorderWidth="0px"></BorderTop>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent7" runat="server" SupportsDisabledAttribute="True">

                <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="True" ClientInstanceName="ASPxGridView1"
                    Width="100%" AutoPostBack="true" >
                    <%--OnDataBinding ="ASPxGridView1_DataBinding" 
                    OnDataBound = "ASPxGridView1_DataBound" 
                    OnBeforeColumnSortingGrouping="ASPxGridView1_BeforeColumnSortingGrouping"
                    Onsummarydisplaytext="ASPxGridView1_SummaryDisplayText"> --%>
                    <SettingsPager PageSize="50" />
                    <Paddings Padding="0px" />
                    <Border BorderWidth="0px" />
                    <BorderBottom BorderWidth="1px" />
                    <Settings ShowFooter="True" />
                    <Styles Header-Wrap="True" />
                    <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="EmployeeID" VisibleIndex="0">
                            <HeaderCaptionTemplate>
                                <table>
                                    <tr>
                                    <th><dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="~/Content/Images/Icons/Write-Document-icon.png" /></th>
                                    <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Emplpoyee Id" ForeColor="White"/></th>
                                    </tr>
                                </table>
                            </HeaderCaptionTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="FirstName" VisibleIndex="1">
                            <HeaderCaptionTemplate>
                                <table>
                                    <tr>
                                    <th><dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="~/Content/Images/Icons/Remove-Document-icon.png" /></th>
                                    <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="First Name" ForeColor="White"/></th>
                                    </tr>
                                </table>
                            </HeaderCaptionTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="LastName" VisibleIndex="1">
                            <HeaderCaptionTemplate>
                                <table>
                                    <tr>
                                    <th><dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="~/Content/Images/Icons/Remove-Document-icon.png" /></th>
                                    <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Last Name" ForeColor="White"/></th>
                                    </tr>
                                </table>
                            </HeaderCaptionTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="TotalHours" VisibleIndex="4">
                            <HeaderCaptionTemplate>
                                <table>
                                    <tr>
                                    <th><dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="~/Content/Images/Icons/Internet-History-icon.png" /></th>
                                    <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Total Hours" ForeColor="White"/></th>
                                    </tr>
                                </table>
                            </HeaderCaptionTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="UnapprovedHours" VisibleIndex="4">
                            <HeaderCaptionTemplate>
                                <table>
                                    <tr>
                                    <th><dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="~/Content/Images/Icons/Internet-History-icon.png" /></th>
                                    <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Unapproved Hours" ForeColor="White"/></th>
                                    </tr>
                                </table>
                            </HeaderCaptionTemplate>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <Styles>
                        <AlternatingRow Enabled="true" />
                    </Styles>
                </dx:ASPxGridView>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxPanel>
</asp:Content>
