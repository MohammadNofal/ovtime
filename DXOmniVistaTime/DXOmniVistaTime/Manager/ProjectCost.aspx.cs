﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DXOmniVistaTimeEngine;
using System.Web.Security;
using DevExpress.Web;
using System.Text;
using DevExpress.Export;
using DevExpress.XtraPrinting;
using System.Xml;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using DevExpress.XtraCharts;
using System.Drawing;
using DevExpress.Utils;
using DevExpress.XtraCharts.Web;
using DevExpress.Data;
using System.Globalization;

public partial class ProjectCost : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            Session["DataGridDisciplines"] = null; //Mohammad added this
            Session["DataRoles"] = null;//Mohammad added this
            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            //Mohammad changed the following
            //DataTable projects = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["ParentProjects"], "");
            string sql = ConfigurationManager.AppSettings["ParentProjects"];
           DataTable projects = DataAccess.GetDataTableBySqlSyntax(sql, "");
            //.....................
            ProjectComboBox.DataSource = projects;
            ProjectComboBox.DataBind();

            Series series1 = new Series("A Pie Series", ViewType.Pie);
            series1.Label.TextPattern = "{A}: {VP:p0}";

            DisciplineWebChartControl.Series.Add(series1);
            series1.ArgumentDataMember = "Discipline";
            series1.ValueDataMembers[0] = "Cost";

            DisciplineWebChartControl.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False;

            Series series2 = new Series("A Pie Series", ViewType.Pie);
            series2.Label.TextPattern = "{A}: {VP:p0}";

            RoleWebChartControl.Series.Add(series2);
            series2.ArgumentDataMember = "Role";
            series2.ValueDataMembers[0] = "Cost";

            RoleWebChartControl.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False;
        }
    }
    //Mohammad added this
    protected void Page_Init(object sender, EventArgs e)
    {
        // initialize SomeDataTable
        if (Session["DataGridDisciplines"] != null)
        {
            this.DisciplineGridView.DataSource = (DataTable)Session["DataGridDisciplines"];
            DisciplineGridView.DataBind();
        }
        if (Session["DataRoles"] != null)
        {
            this.RoleGridView.DataSource = (DataTable)Session["DataRoles"];
            RoleGridView.DataBind();
        }
    }
    //...........................
    protected void GridViewPannel_CallBack(object sender, CallbackEventArgsBase e)
    {
        if (ProjectComboBox.SelectedItem != null)
        {
            DisciplineGridView.Visible = true;
            RoleGridView.Visible = true;

            DisciplineWebChartControl.Visible = true;
            RoleWebChartControl.Visible = true;

            String chosenProject = ProjectComboBox.SelectedItem.ToString();
            //Getting clients data from database and binding it to gridview
            DataTable disciplines = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["ProjectCostsPerDiscipline"], "WHERE ProjectID = '" + chosenProject + "' AND e.EmpDepartment != '' AND e.EmpDepartment IS NOT NULL GROUP BY E.EmpDepartment");
            Session["DataGridDisciplines"] = disciplines;
            DisciplineGridView.DataSource = disciplines;
            DisciplineWebChartControl.DataSource = disciplines;

            DisciplineGridView.DataBind();
            DisciplineWebChartControl.DataBind();

            DataTable roles = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["ProjectCostsPerRole"], "WHERE ProjectID = '" + chosenProject + "' GROUP BY R.RoleName");
            Session["DataRoles"] = roles;
            RoleWebChartControl.DataSource = roles;
            RoleGridView.DataSource = roles;

            RoleWebChartControl.DataBind();
            RoleGridView.DataBind();
        }
    }
}