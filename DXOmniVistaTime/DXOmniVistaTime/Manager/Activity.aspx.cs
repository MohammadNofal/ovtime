﻿using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_Tasks : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        LoadData();
        
    }
    private void LoadData()
    {
        string sql = "Select * From Activity";
        DataTable tasks =DataAccess.GetDataTableBySqlSyntax(sql, "");
        this.ASPxGridView1.DataSource = tasks;
        this.ASPxGridView1.DataBind();
    }
    protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        e.Cancel = true;
        string ActivityID = e.Values["ActivityID"].ToString().ToString();
        string sql = "Delete from Activity where ActivityID = '" + ActivityID+"'";
        DataAccess.ExecuteSqlStatement(sql, "");
        LoadData();

    }
    protected void ASPxGridView1_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        e.Cancel = true;
        string ActivityCode = "";
        string ActivityDescription = "";
        string ActivityID = "";
        string ActivitySub = "";

        if (e.NewValues["ActivityID"] != null)
        {
            ActivityID = e.NewValues["ActivityID"].ToString();
        }
        if (e.NewValues["ActivityCode"] != null)
        {
            ActivityCode = e.NewValues["ActivityCode"].ToString();
        }
        if (e.NewValues["ActivityDescription"] != null)
        {
            ActivityDescription = e.NewValues["ActivityDescription"].ToString();
        }
        if (e.NewValues["ActivitySub"] != null)
        {
            ActivityID = e.NewValues["ActivityID"].ToString();
        }
        string sql = "Insert Into Activity(ActivityID,ActivityCode,ActivityDescription,ActivitySub) Values ('" + ActivityID + "' , '" + ActivityCode + "', '" + ActivityDescription + "', '" + ActivitySub + "')";
        DataAccess.ExecuteSqlStatement(sql,"");
        e.Cancel = true;
        this.ASPxGridView1.CancelEdit();
        LoadData();

    }


    protected void ASPxGridView1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        e.Cancel = true;
        string ActivityCode = "";
        string ActivityDescription = "";
        string ActivityID = "";
        string ActivitySub = "";


        if (e.NewValues["ActivityID"] != null)
        {
            ActivityID = e.NewValues["ActivityID"].ToString();
        }
        if (e.NewValues["ActivityCode"] != null)
        {
            ActivityCode = e.NewValues["ActivityCode"].ToString();
        }
        if (e.NewValues["ActivityDescription"] != null)
        {
            ActivityDescription = e.NewValues["ActivityDescription"].ToString();
        }
        if (e.NewValues["ActivitySub"] != null)
        {
            ActivityID = e.NewValues["ActivityID"].ToString();
        }


        string sql = "Update Activity SET  ActivityCode = '" + ActivityCode + "', ActivityDescription = '" + ActivityDescription + "' , ActivitySub='" + ActivitySub + "'  where ActivityID ='" + ActivityID + "'";
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        DataAccess.ExecuteSqlStatement(sql, "");
       
        this.ASPxGridView1.CancelEdit();
        LoadData();
    }
    [WebMethod]
    [ScriptMethod]
    public static string AddActivity(string Client, bool editflag)
    {
        Dictionary<string, object> obj = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(Client);


        if (!editflag)
        {
            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            string sql = string.Format("select * from Activity where ActivityID ='{0}'", obj.Values.ElementAt(0));
            if (DataAccess.GetDataTableBySqlSyntax(sql, "").Rows.Count > 0)
            {
                return "Activity already exists.";
            }

            sql = "Insert into Activity(";
            string into = "";
            string values = " Values (";
            for (int i = 0; i < obj.Count(); i++)
            {
                if (obj.Values.ElementAt(i) == String.Empty)
                {
                    continue;
                }
                into += obj.Keys.ElementAt(i) + ",";
                values += "'" + obj.Values.ElementAt(i) + "',";
            }
            into = into.Substring(0, into.Length - 1) + ")";
            values = values.Substring(0, values.Length - 1) + ")";

            sql += into + values;
            DataAccess.ExecuteSqlStatement(sql, "");
            return "";
        }
        else
        {

            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            string sql = "Update Activity set ";

            for (int i = 0; i < obj.Count(); i++)
            {
                if (obj.Values.ElementAt(i) == String.Empty)
                {
                    continue;
                }
                sql += obj.Keys.ElementAt(i) + "=";
                sql += "'" + obj.Values.ElementAt(i) + "',";
            }
            sql = sql.Substring(0, sql.Length - 1);
            sql += " where ActivityID = '" + obj.Values.ElementAt(0).ToString() + "'";


            DataAccess.ExecuteSqlStatement(sql, "");
            return "";

        }
    }
    [WebMethod]
    [ScriptMethod]
    public static string GetActivityInfo(string ActivityID)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        string sql = "select * from Activity where ActivityID ='" + ActivityID + "'";
        DataTable user = DataAccess.GetDataTableBySqlSyntax(sql, "");


        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> row;
        foreach (DataRow dr in user.Rows)
        {
            row = new Dictionary<string, object>();
            foreach (DataColumn col in user.Columns)
            {
                row.Add(col.ColumnName, dr[col]);
            }
            rows.Add(row);
        }
        return new JavaScriptSerializer().Serialize(rows);
    }

}