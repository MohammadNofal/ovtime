﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="Roles.aspx.cs" Inherits="Manager_Roles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <script>
        function SaveRole() {
            RolesGrid.PerformCallback("AddRole");
        }
    </script>
     <dx:ASPxCallbackPanel ID="FilterPanel" runat="server" ClientInstanceName="FilterPanel" Width="100%" CssClass="detailPanelMidian" Collapsible="false"  SettingsLoadingPanel-Enabled ="false"  >
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <SettingsLoadingPanel Enabled="False"></SettingsLoadingPanel>

        <Styles>
            <ExpandBar Width="100%" CssClass="bar">
            </ExpandBar>
            <ExpandedExpandBar CssClass="expanded">
            </ExpandedExpandBar>
        </Styles>
        <BorderTop BorderWidth="0px">
        </BorderTop>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                
                <div class="PageTitle">
                   <span> Roles Management:</span>
                 </div>
                 <br />
                <br />
                 <dx:ASPxTextBox Caption="Role Name"   runat="server" ID="roleName" CssClass="editorText"  >

                    <CaptionCellStyle CssClass="editorCaption" />
                    <RootStyle CssClass="editorContainer"/>
                      <ValidationSettings>
                          <RequiredField IsRequired="True" />
                      </ValidationSettings>
                    </dx:ASPxTextBox> 
                <dx:ASPxButton ID="submitRole"  runat="server" Text="Search "  ClientInstanceName="submitRole"
                    ImagePosition = "Right" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption" onclick="addRole"  
                    AutoPostBack="false" Image-Url="~/Content/Images/Icons/check-64-icon.png" Image-Height="20px" Image-Width="20px" Height="36px">
                     <Image Height="20px" Width="20px" Url="~/Content/Images/Icons/Search-icon.png"></Image>
                    <ClientSideEvents Click="SaveRole()" />
                </dx:ASPxButton>
            </dx:PanelContent>
        </PanelCollection>
        <Paddings Padding="0px"   />
    </dx:ASPxCallbackPanel>
       <div class="row" style="margin-right:0px;margin-left:0px;margin: 10px;" > 
    <dx:ASPxCallbackPanel ID="userdetailpanel" ClientInstanceName="userdetailpanel" runat="server" >
        <PanelCollection>
            <dx:PanelContent>
                <dx:ASPxGridView ID="RolesGrid" runat="server" AutoGenerateColumns="False" OnRowUpdating="RolesGrid_RowUpdating"
                   KeyFieldName="RoleName" OnRowDeleting="RolesGrid_RowDeleting" ClientInstanceName="RolesGrid"  >
                    <SettingsBehavior ConfirmDelete="True" />
                    <SettingsText ConfirmDelete="Are you sure?" /> 
                    <Columns>
                        <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn Caption="Role Name" Name="Role" FieldName="RoleName" 
                            ShowInCustomizationForm="True" VisibleIndex="1">
                            <PropertiesTextEdit>
                                <ValidationSettings>
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>      
                    </Columns>
<Styles AdaptiveDetailButtonWidth="22"></Styles>
                </dx:ASPxGridView>
             </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
   </div>
</asp:Content>

