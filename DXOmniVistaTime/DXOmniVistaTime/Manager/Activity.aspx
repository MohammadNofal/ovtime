﻿<%@ Page Title="OVTime : Activity View" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="Activity.aspx.cs" Inherits="Manager_Tasks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <script src="../Content/js/Common.js"></script>
     <script src="../Content/js/Activities.js"></script>
    <style>
       .detailPanelLarge {
           height:36px;
           margin-bottom:0px;
       }
         .editorText {
            margin-left: 27px;
            margin-right: 35px;
        }
        .editorCaption {
            padding-left:13px;
        }
        .modal-body {
            position: relative;
            padding: 15px;
            float: left;
            border-bottom: 3px solid #e5e5e5;
            margin-bottom: 10px;
        }
        .modal-content {
            width: 40%;
            height: inherit;
            margin: 0 auto;
        }
      
   </style>
     <dx:ASPxCallbackPanel ID="DetailPanel" runat="server" ClientInstanceName="FilterPanel" Width="100%" CssClass="detailPanelLarge" Collapsible="false"  SettingsLoadingPanel-Enabled ="false" >
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
<SettingsLoadingPanel Enabled="False"></SettingsLoadingPanel>

        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                 <label style="float:left;width:30%;"> Activities Listing </label> 
            

            </dx:PanelContent>
        </PanelCollection>
        <Paddings Padding="8px" PaddingBottom="2px" />
    </dx:ASPxCallbackPanel>
     <dx:ASPxPanel ID="EmployeeSelectorPanel" runat="server" ClientInstanceName="userdetailpanel">
            <PanelCollection>
                <dx:PanelContent>
    <dx:ASPxGridView ClientInstanceName="Tasks"  ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" KeyFieldName="ActivityID" 
        OnRowDeleting="ASPxGridView1_RowDeleting" Width="100%">
         <SettingsPager Visible="true" PageSize="20" >
        </SettingsPager>
        <Settings UseFixedTableLayout="True" />
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsSearchPanel Visible="True" />
        <SettingsText ConfirmDelete="Are you sure? " />
        <SettingsCommandButton>
            
                 <EditButton >
                     <Image AlternateText="New" ToolTip="New" Url="~/Content/Images/Icons/comment-user-add-icon.png">
                    </Image>
                </EditButton>
             
                <DeleteButton>
                     <Image AlternateText="Delete" Url="~/Content/Images/Icons/comment-user-close-icon.png" ToolTip="Delete">
                     </Image>
                </DeleteButton>
        </SettingsCommandButton> 
         <ClientSideEvents CustomButtonClick="Activity_CustomeBTNClick" />
            
        
         <Columns>
            <dx:GridViewCommandColumn ShowNewButtonInHeader="false" VisibleIndex="0" ShowEditButton="false" ShowDeleteButton="True" ShowClearFilterButton="True">
                <HeaderTemplate>
                     <dx:ASPxButton ID="ASPxButton1" CausesValidation="false" RenderMode="Link" Text="New"  Image-Url="~/Content/Images/Icons/comment-user-add-icon.png" runat="server" AutoPostBack="false">
                                <ClientSideEvents  Click="function(s,e){ShowAddActivity();}" />
                         <Paddings PaddingLeft="30%" />

                            </dx:ASPxButton>
                </HeaderTemplate>
                <CustomButtons>
                    <dx:GridViewCommandColumnCustomButton ID="update"  Text="Edit">
                            <Image Url="~/Content/Images/Icons/comment-user-add-icon.png"></Image>
                        </dx:GridViewCommandColumnCustomButton>
                </CustomButtons>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="ActivityID" Name="ActivityID" Caption="Activity ID" VisibleIndex="1">
                <PropertiesTextEdit>
                    <ValidationSettings SetFocusOnError="True">
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </PropertiesTextEdit>
             </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn FieldName="ActivityCode" Name="ActivityCode" Caption="Activity Code" VisibleIndex="2">
                <PropertiesTextEdit>
                    <ValidationSettings SetFocusOnError="True">
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </PropertiesTextEdit>
             </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Activity Sub" FieldName="ActivitySub" Name="TaskSub" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataMemoColumn Caption="Activity Info" FieldName="ActivityDescription" Name="Description" VisibleIndex="3">
            </dx:GridViewDataMemoColumn>
        </Columns>

       <Settings VerticalScrollBarMode="Visible" VerticalScrollableHeight="100" />   
        <Styles Header-Wrap="True" >
                        <Header Wrap="True">
                        </Header>
                        <AlternatingRow Enabled="True">
                        </AlternatingRow>
                    </Styles>
                    <Paddings Padding="0px" PaddingLeft="8px" PaddingRight="8px"/>
                    <SettingsPager PageSize="20" />
                    <SettingsEditing Mode="EditForm" />
                    <Settings ShowFooter="false" UseFixedTableLayout="True" />
    </dx:ASPxGridView>
                     </dx:PanelContent>
            </PanelCollection>
           
        </dx:ASPxPanel>
     <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="addActivity">
        <div class="vertical-alignment-helper"> 
     <div class="modal-dialog vertical-align-center"> 
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span style="color: #0e0e0e;font-size: 26px;" aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add new activity</h4>
              </div>
                <div class="modal-body">
                     <div class="profile">
                         <dx:ASPxLabel runat="server" ID="ActivityStatus" ClientInstanceName="ActivityStatus" Text="" ForeColor="Red" ></dx:ASPxLabel>
                     </div>
                        <div class="profile">
                  
                                <dx:ASPxTextBox   ID="ActivityID" ClientInstanceName="ActivityID" Caption="Activity ID" runat="server"  AutoPostBack="false"  ValidationSettings-RequiredField-IsRequired="true" >
                               </dx:ASPxTextBox>
                        </div>
                      
                        <div class="profile">
                                <dx:ASPxTextBox ID="ActivityCode" ClientInstanceName="ActivityCode" Caption="Activity Code" runat="server"  AutoPostBack="false" ValidationSettings-RequiredField-IsRequired="false">                   
                               </dx:ASPxTextBox>
                           </div>  
                        <div class="profile">
                            <dx:ASPxTextBox ID="ActivityDescription"  Caption="Activity Info" runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="ActivityDescription" ValidationSettings-RequiredField-IsRequired="false">                                                                
                            </dx:ASPxTextBox>
                        </div>
                        <div class="profile">
                            <dx:ASPxTextBox ID="ActivitySub"  Caption="Activity Sub" runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="ActivitySub" ValidationSettings-RequiredField-IsRequired="false">                                                                
                            </dx:ASPxTextBox>
                        </div>
                </div>
                 <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
                <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Submit" AutoPostBack="false" ClientInstanceName="addActivityBtn" ClientSideEvents-Click="function (s,e){addActivityFunctionOptions();}" />
              </div>
            </div>
            </div> 
            </div>
    </div>
       <script type="text/javascript">
           // <![CDATA[
           ASPxClientControl.GetControlCollection().ControlsInitialized.AddHandler(function (s, e) {

               UpdateGridHeightCom(Tasks);
           });
           ASPxClientControl.GetControlCollection().BrowserWindowResized.AddHandler(function (s, e) {

               UpdateGridHeightCom(Tasks);
           });
           // ]]> 
        </script>
</asp:Content>


