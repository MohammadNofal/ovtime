﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Restricted Issuer.aspx.cs" Inherits="Example" %>

<%-- DXCOMMENT: Page Root.master is a master page that contains the root layout (it includes Header, Cental Area, and Footer) --%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
</head>
    <body runat="server" id="Body">
    <form id="form1" runat="server">
              

                <script type="text/javascript">
                    function DuplicatedIssuer(s, e) {
                        console.log("Called");
                        if (s.cpDuplicated) {
                            alert("Issuer already exists in Restricted Lists");
                            s.cpDuplicated = false;
                        }
                    }
    </script>

             <dx:ASPxGridView ID="ASPxGridView1" runat="server" SettingsText-Title="Custom Issuer List Restrictions" Settings-ShowTitlePanel="true" AutoGenerateColumns="False" ClientInstanceName="ASPxGridView1" 
                 OnCellEditorInitialize="ASPxGridView1_CellEditorInitialize"
                 OnRowDeleting="ASPxGridView1_RowDeleting"
                 OnRowUpdating="ASPxGridView1_RowUpdating"
                 OnRowInserting="ASPxGridView1_RowInserting"
                  Width="75%" AutoPostBack="true" KeyFieldName="ISSUER_CD">
            <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
            <Paddings PaddingLeft="10px" PaddingTop="10px" Padding="0px" />
            <Border BorderWidth="0px" />
            <BorderBottom BorderWidth="1px" />
            <SettingsPager PageSize="10" />
            <SettingsEditing Mode="EditForm" EditFormColumnCount="2" />
            <Settings ShowFooter="True"/>
            <SettingsBehavior ConfirmDelete="true" AllowSelectByRowClick="true" AllowFocusedRow="true" ProcessFocusedRowChangedOnServer="true" />
            <SettingsSearchPanel CustomEditorID="Search" Visible="false" ShowApplyButton="false" HighlightResults="true" Delay="800" />
            <SettingsText ConfirmDelete="Do you want delete this issuer from restricted list?"/>
                 <ClientSideEvents EndCallback="DuplicatedIssuer" />
            <Columns>
                <dx:GridViewCommandColumn ShowDeleteButton="true" ShowEditButton="false" ShowNewButtonInHeader="true" VisibleIndex="0" Width="1%"/>
                <dx:GridViewDataComboBoxColumn FieldName="ISSUER_CD"/>
                <dx:GridViewDataColumn FieldName="ISSUER_NAME" />
            </Columns>
        </dx:ASPxGridView>

        </form>
        </body>
    </html>
