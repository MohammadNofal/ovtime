﻿using DevExpress.Export;
using DevExpress.Web;
using DevExpress.XtraPrinting;
using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_ProjectsDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["ProjectsDetailsDataGrid"] = null;
            // LoadData();
            GetProjects();
            GetEmployees();
            GetDisciplines();
        }
            //Filter_Click(null, null);
    }
    public void LoadData(string filter="")
    {
        ASPxGridView1.Visible = true;

        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ToString());
        if (filter != string.Empty && filter != "null,null,null,null,null" )
        {
            string sql = "EXEC OVS_GetProjectsDetails " + filter;
            this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(sql, "");
            Session["ProjectsDetailsDataGrid"] = this.ASPxGridView1.DataSource;
            this.ASPxGridView1.DataBind();
        }
    }
    public void GetDisciplines()
    {
        //Discipline
        DisciplineComboBox.Items.Clear();//Mohammad added this
        DisciplineComboBox.Items.Add("");
        DataTable roles = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["RoleNames"], "");
        DisciplineComboBox.DataSource = roles;
        DisciplineComboBox.DataBind();
        //DisciplineComboBox.Items.Add("Management");
        //DisciplineComboBox.Items.Add("Architecture");
        //DisciplineComboBox.Items.Add("Structure");
        //DisciplineComboBox.Items.Add("Mechanical");
        //DisciplineComboBox.Items.Add("Electrical");
        //DisciplineComboBox.Items.Add("QS");
    }

    public void GetProjects()
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ToString());
        //Mohammad eddited the following 
        //this.ProjectsComboBox.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["ParentProjects"], "");
        DataTable projects = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["ParentProjects"], "");
        DataRow emptyDataRow = projects.NewRow();
        projects.Rows.InsertAt(emptyDataRow, 0);
        this.ProjectsComboBox.DataSource = projects;
        //..............................
        this.ProjectsComboBox.ValueField = "ProjectID";
        this.ProjectsComboBox.TextField = "ProjectID";
        this.ProjectsComboBox.DataBind();
    }
    public void GetEmployees()
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ToString());
        string sql = ConfigurationManager.AppSettings["EmployeeIDsNames"];
        //Mohammad eddited the following 
        //this.EmployeeComboBox.DataSource = DataAccess.GetDataTableBySqlSyntax(sql, "");
        DataTable employees = DataAccess.GetDataTableBySqlSyntax(sql, "");
        DataRow emptyDataRow = employees.NewRow();
        employees.Rows.InsertAt(emptyDataRow, 0);
        this.EmployeeComboBox.DataSource = employees;
        //............................................
        this.EmployeeComboBox.ValueField = "EmployeeID";
        this.EmployeeComboBox.TextField = "FullName";
        this.EmployeeComboBox.DataBind();
    }

    protected void ProjectsComboBox_SelectedIndexChanged(object sender, EventArgs e)
    {
        

    }
    protected void EmployeeID_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    //Mohammad added this
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["ProjectsDetailsDataGrid"] != null)
        {
            this.ASPxGridView1.DataSource = (DataTable)Session["ProjectsDetailsDataGrid"];
            this.ASPxGridView1.DataBind();
        }
    }
    //.......................

        protected void GridViewPannel_CallBack(object sender, CallbackEventArgsBase e)
    {
        string query = BuildQuery();
        string[] parts = query.Split(',');
        if (query != string.Empty && !parts[0].Equals("null")){//mohammad eddited the following
            LoadData(query);
        }
        //string MainProjectID = this.ProjectsComboBox.Value.ToString();
        //LoadData(MainProjectID);
    }

    public string BuildQuery()
    {
        string query = string.Empty;

        if (this.ProjectsComboBox.Value != null)
        {
            query += " '" + this.ProjectsComboBox.Value.ToString() + "' ";
        }
        else
        {
            query += "null";
        }

        if (query != string.Empty) query += ",";

        if (this.EmployeeComboBox.Value != null)
        {
            //if (query != string.Empty) query += " AND ";
            //query += "[EmployeeID/Vendor] = '" + this.EmployeeComboBox.Value + "'";
            query += "'" + this.EmployeeComboBox.Value + "'";
        }
        else
        {
            query += "null";
        }

        if (query != string.Empty) query += ",";

        if (this.DisciplineComboBox.Value != null)
        {
            string str = this.DisciplineComboBox.Value.ToString();
            query += "'" + str + "'";
        }
        else
        {
            query += "null";
        }

        if (query != string.Empty) query += " , ";

        if (this.From.Value != null)
        {
           // if (query != string.Empty) query += " AND ";
          //  query += "tedate >= CAST('" + this.From.Value.ToString() + "' as DATE)";
            query += "'" + this.From.Value.ToString() + "'";
        }
        else
        {
            query += "null";
        }
        if (query != string.Empty) query += ",";

        if (this.To.Value != null)
        {
            query += "'" + this.To.Value.ToString() + "'";
        }
        else
        {
            query += "null";
        }
        
        return query;
    }

    protected void EportBtn_ServerClick(object sender, EventArgs e)
    {
        string fileName = "Project Details - " ;
        if (this.ProjectsComboBox.Value != null)
        {
            fileName += " " + this.ProjectsComboBox.Value.ToString() + " ";
        } 
        if (this.From.Value != null)
        { 
            fileName += " from " + this.From.Value.ToString() + "'";
        }  
        if (this.To.Value != null)
        { 
            fileName += " to " + this.To.Value.ToString() + " ";
        } 

        if (this.EmployeeComboBox.Value != null)
        { 
            fileName += " " + this.EmployeeComboBox.Value + " ";
        } 
        gridExport.FileName = fileName + " as of "+DateTime.Now.ToString("yyyy-MM-dd");
        gridExport.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.DataAware });
    }

    protected void ASPXDateEdit_CustomCell(object sender, CalendarDayCellPreparedEventArgs e)
    {
        // #c00000 (red active)
        // #333333 (black active)
        // #ececec (grey not active)
        if (!e.IsOtherMonthDay)
        {
            if (e.Date.DayOfWeek == DayOfWeek.Sunday) e.Cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#333333");
            else if (e.Date.DayOfWeek == DayOfWeek.Friday) e.Cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#c00000"); //#c00000
        }
    }

}