﻿using DevExpress.Export;
using DevExpress.Web;
using DevExpress.XtraPrinting;
using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_ProjectsSummary : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Works for first time
        if (!IsPostBack)
        {
            Session["ProjectsSummaryDataGrid"] = null;
            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ToString());
            LoadData();
        }
        //filter_Click(null, null);
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        // initialize SomeDataTable
        if (Session["ProjectsSummaryDataGrid"] != null)
        {
            this.ASPxGridView1.DataSource = (DataTable)Session["ProjectsSummaryDataGrid"];
            ASPxGridView1.DataBind();
        }
    }

    public void LoadData()
    {
        this.ClientsCombobox.DataSource = GetClients();
        this.ClientsCombobox.DataBind();
        this.ProjectsCombobox.DataSource = GetProjects();
        this.ProjectsCombobox.DataBind();

        //Discipline
        DisciplineComboBox.Items.Add("");
        DataTable roles = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["RoleNames"], "");
        DisciplineComboBox.DataSource = roles;
        DisciplineComboBox.DataBind();
        //DisciplineComboBox.Items.Add("Management");
        //DisciplineComboBox.Items.Add("Architecture");
        //DisciplineComboBox.Items.Add("Structure");
        //DisciplineComboBox.Items.Add("Mechanical");
        //DisciplineComboBox.Items.Add("Electrical");
        //DisciplineComboBox.Items.Add("QS");
    }
    public DataTable GetClients()
    {
        //Mohammad eddited the following
        //string sql = "SELECT CLIENTID FROM CLIENT WHERE  CLIENTID IS NOT NULL";
        //string sql = "SELECT CLIENTID FROM CLIENT WHERE  CLIENTID IS NOT NULL AND CLIENTID !='' ORDER BY CLIENTID ASC";
        string sql = ConfigurationManager.AppSettings["ClientIDs"]; 
        //.....
        DataTable Clients = DataAccess.GetDataTableBySqlSyntax(sql, "");
        // Inserting an empty row at the beginning of the list
        DataRow emptyDataRow = Clients.NewRow();
        Clients.Rows.InsertAt(emptyDataRow, 0);
        return Clients;
    }
    public DataTable GetProjects()
    {
        //Mohammad changed the following
        //string sql = "SELECT ProjectID,ProjectName FROM PROJECT WHERE PROJECTID IS NOT NULL and PARENTPROJECTID IS NULL";
        //string sql = "select distinct [dbo].ProjectParent(projectid) as projectid from project WHERE [dbo].ProjectParent(projectid) != '' ORDER BY [dbo].ProjectParent(projectid) ASC";
        string sql = ConfigurationManager.AppSettings["ParentProjects"];
        //............
        DataTable Projects = DataAccess.GetDataTableBySqlSyntax(sql, "");
        // Inserting an empty row at the beginning of the list
        DataRow emptyDataRow = Projects.NewRow();
        Projects.Rows.InsertAt(emptyDataRow, 0);
        return Projects;
    }

    public DataTable GetActivites()
    {
        DataTable activities = new DataTable();
        activities.Columns.Add("Activity");
        activities.Rows.Add(new Object[] {"Design"});
        activities.Rows.Add(new Object[] {"Supervision"});
        return activities;
    }

    public void LoadProjects(string query="")
    {
        ASPxGridView1.Visible = true;

        if (query == string.Empty || query == "null,null,null,null,null")
        {
            Session["ProjectsSummaryDataGrid"] = DataAccess.GetDataTableBySqlSyntax("exec [OVS_GetProjectsSummary] null,null,null,null,null", "");
            this.ASPxGridView1.DataSource = (DataTable)Session["ProjectsSummaryDataGrid"];
        }
        else
        {
            Session["ProjectsSummaryDataGrid"] = DataAccess.GetDataTableBySqlSyntax("exec [OVS_GetProjectsSummary] " + query, "");
            this.ASPxGridView1.DataSource = (DataTable)Session["ProjectsSummaryDataGrid"];
        }

        this.ASPxGridView1.DataBind();
    }

    protected void GridViewPannel_CallBack(object sender, CallbackEventArgsBase e)
    {
        string query = BuildQuery();
        LoadProjects(query);

    }

    public string BuildQuery()
    {
        string query = string.Empty;
       
        if (this.From.Value != null)
        {
            query += "'" + this.From.Value.ToString() + "'";
        }
        else
        {
            query += "null";
        }
        if (query != string.Empty) query += ",";
        if (this.To.Value != null)
        {
             query += "'" + this.To.Value.ToString() + "'";
        }
        else
        {
            query += "null";
        }
        if (query != string.Empty) query += ",";

        if (this.ClientsCombobox.Value != null)
        {
             query += "'" + this.ClientsCombobox.Value + "'";
        }
        else
        {
            query += "null";
        }

        if (query != string.Empty) query += ",";

        if (this.ProjectsCombobox.Value != null)
        {
            query += "'" + this.ProjectsCombobox.Value + "'";
        }
        else
        {
            query += "null";
        }

        if (query != string.Empty) query += ",";

        if (this.DisciplineComboBox.Value != null)
        {
            query += "'" + this.DisciplineComboBox.Value + "'";
        }
        else
        {
            query += "null";
        }

        return query; 
    }
   
    protected void EportBtn_ServerClick(object sender, EventArgs e)
    {
        gridExport.FileName = "Projects Summary " + DateTime.Now.ToString("yyyy-MM-dd");
        gridExport.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.DataAware });
    }

    protected void ASPXDateEdit_CustomCell(object sender, CalendarDayCellPreparedEventArgs e)
    {
        // #c00000 (red active)
        // #333333 (black active)
        // #ececec (grey not active)
        if (!e.IsOtherMonthDay)
        {
            if (e.Date.DayOfWeek == DayOfWeek.Sunday) e.Cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#333333");
            else if (e.Date.DayOfWeek == DayOfWeek.Friday) e.Cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#c00000"); //#c00000
        }
    }

}