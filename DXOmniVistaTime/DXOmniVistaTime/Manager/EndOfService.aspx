﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/OVMaster.master" CodeFile="EndOfService.aspx.cs" Inherits="EndOfService" %>

<asp:Content ID="header_" ContentPlaceHolderID="Header" runat="server">
    <h4><span class="text-semibold">Manager / End Of Service</span></h4>
    <%--<div class="heading-elements">--%>
    <%-- <div class="heading-btn-group">--%>
    <%--<a href="#" class="btn btn-link btn-float has-text" >--%>
    <%--<i class="pdf_icon text-primary"></i><span>PDF</span>--%>
    <%--</a>--%>
    <%--<a runat="server" id="ExportBtn" onserverclick="EportBtn_ServerClick" class="btn btn-link btn-float has-text"><i class="excel_icon  text-primary"></i><span>Excel</span></a>--%>

    <%--</div>--%>
    <%-- </div>--%>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="ContentPage" runat="server">
    <style>
         .dxWeb_pPrevDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNext {
             background: url('../content/images/iconmonstr-arrow-63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pPrev {
             background: url('../content/images/iconmonstr-arrow-64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNextDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }
            .TextBoxEdit {
            width: 100%;
            padding: 7px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }
             .newFont * 
        {
            font-family: Calibri;
            font-size: 16px;
            text-align: center;
        }

.pagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #333333 !important;
            text-decoration: none !important;
            padding-left: 7px !important;
            padding-right: 7px !important;
        }
        .pagenumber:hover{
            background-color : #333333;
            color: #FFFFFF !important;
        }
        .currentpagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #FFFFFF !important;
            background-color: #333333;
            text-decoration: none !important;
        }
        .dxeButtonEditButton{
            background: none !important;
            border: none !important;
        }
    </style>
    <script>
         var textSeparator = ";"
        function updateText(s, e) {
            var selectedValues = checkListBox.GetSelectedValues();
            if (e.index == 0) {
                //var arr = checkListBox.GetSelectedValues();
                if (selectedValues[0] == "SelectAll") { checkListBox.SelectAll(); checkComboBox.SetText("All Employees Selected"); }
                else { checkListBox.UnselectAll(); checkComboBox.SetText(""); }
            }
            else {
                var x = new Array(); x[0] = 0;
                checkListBox.UnselectIndices(x);
                selectedValues = checkListBox.GetSelectedValues();
                checkComboBox.SetText(selectedValues);
            }

            selectedValues = checkListBox.GetSelectedValues();

            GridViewPannel.PerformCallback(selectedValues);
        }
    </script>
    
    <dx:ASPxCallbackPanel ID="DetailPanel" runat="server" ClientInstanceName="DetailPanel" Width="100%" CssClass="detailPanelLarge" Collapsible="false" SettingsLoadingPanel-Enabled="false">
        <PanelCollection>
            <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                <table>
                    <tr>
                        <td style="padding: 7px">
                            <dx:ASPxDropDownEdit ClientInstanceName="checkComboBox" CssClass="TextBoxEdit" Caption="Employees:" ID="ASPxDropDownEdit1" Width="285px" runat="server" AnimationType="Fade"
                                CaptionCellStyle-Paddings-PaddingTop="10px">
                                <DropDownButton >
                        <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                    </DropDownButton>
                                <DropDownWindowStyle BackColor="#EDEDED" />
                                <DropDownWindowTemplate>
                                    <dx:ASPxListBox Width="100%" ID="listBox" ClientInstanceName="checkListBox" SelectionMode="CheckColumn"
                                        runat="server" Height="200">

                                        <Border BorderStyle="None" />
                                        <BorderBottom BorderStyle="Solid" BorderWidth="1px" BorderColor="#DCDCDC" />

                                        <Items>

                                            <dx:ListEditItem Text="Select All" Value="SelectAll" />

                                        </Items>
                                        <ClientSideEvents SelectedIndexChanged="updateText" Init="function(s, e) { 
        $('[class*=dxeListBoxItemRow]').eq(1).css({ 'font-weight': 'bold' }); // Highlight the first item (Select All)
    }" />
                                    </dx:ASPxListBox>
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="padding: 4px">
                                                <dx:ASPxButton ID="ASPxButton1" AutoPostBack="False" runat="server" Text="Close" Style="float: right">
                                                    <ClientSideEvents Click="function(s, e){ checkComboBox.HideDropDown(); }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>

                                </DropDownWindowTemplate>

                            </dx:ASPxDropDownEdit>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>

        </PanelCollection>
    </dx:ASPxCallbackPanel>
     <div style="overflow-y: auto;height: calc(100vh - 267px);">
    <dx:ASPxCallbackPanel ID="GridViewPannel" runat="server" ClientInstanceName="GridViewPannel" Collapsible="false"
        SettingsLoadingPanel-Enabled="true" OnCallback="GridViewPannel_CallBack">
        <PanelCollection>
            <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                <dx:ASPxGridView ID="ASPxGridView1" runat="server" KeyFieldName="UserId" ClientInstanceName="ASPxGridView1" 
                     Paddings-PaddingTop="20px"
                    Width="100%" AutoPostBack="true" CssClass="newFont"  Border-BorderColor="#dbdbdb">
                    <SettingsPager CurrentPageNumberFormat="{0}">
                    </SettingsPager>
                    <StylesPager>
                        <PageNumber CssClass="pagenumber"></PageNumber>
                        <CurrentPageNumber CssClass="currentpagenumber"></CurrentPageNumber>
                    </StylesPager>
                     <Settings ShowTitlePanel ="true"  />
                    
                    <SettingsEditing EditFormColumnCount="4" Mode="Inline"/>
                    <Styles>
                        <AlternatingRow Enabled="true" />
                        <Header HorizontalAlign="Center"></Header>
                    </Styles>

                    <SettingsPopup>
                        <EditForm Width="100%" Modal="false"/>
                    </SettingsPopup>

                    <SettingsPager PageSize="50" />
                    <Paddings Padding="0px" />
                    <Border BorderWidth="0px" />
                    <BorderBottom BorderWidth="1px" />
                    <Settings ShowFooter="True" />
                    <Styles Header-Wrap="True" />
                    <Columns>
                         <dx:GridViewDataTextColumn FieldName="EmployeeId" Caption="Employee Name" CellStyle-HorizontalAlign="Left" />
                        <dx:GridViewDataTextColumn FieldName="HireDate" Caption="Starting Date" CellStyle-HorizontalAlign="Left" />
                        <dx:GridViewDataTextColumn FieldName="CashLiability" Caption="End Of Service Cost" CellStyle-HorizontalAlign="Left" />
                    </Columns>
                </dx:ASPxGridView>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
        </div>
</asp:Content>
