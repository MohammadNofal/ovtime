﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/OVMaster.master"  CodeFile="ProjectsSummary.aspx.cs" Inherits="Manager_ProjectsSummary" %>

<asp:Content ID="header_" ContentPlaceHolderID="Header" runat="server">
 <style>
      .dxWeb_pPrevDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNext {
             background: url('../content/images/iconmonstr-arrow-63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pPrev {
             background: url('../content/images/iconmonstr-arrow-64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNextDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }
        .month1_border 
        {
            border-left-width: 2px !important;
        }
        .head_container 
        {
            background-color: #EEE;
            float: right;
            margin-right: 7.8%;
        }

        .Month1_css 
        {
            border-style: solid;
            border-width: medium;
            float: right;
            background-color: red;
        }
        .Month2_css 
        {
            border-style: solid;
            border-width: medium;
            float: right;
        }

        .delete_icon 
        {
            position: relative;
            font-family: "Courier New" !important;
            color: #ee784a !important;
            font-size: 200% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .delete_icon:hover 
        {
            color: #FF0000 !important;
        }
        .delete_icon::before 
        {
            content: "\000D7" !important;
        }
        .new_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 20px !important;
            padding: 1px 1px;
            text-decoration: none !important;
        }

        .new_icon:hover 
        {
            color: #7373FF !important;
        }
        .new_icon:before 
        {
           content: "\f067" !important;
        }
        .edit_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #f3cb76 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .edit_icon:hover 
        {
            color: #FF8000 !important;
        }
        .edit_icon:before 
        {
            content: "\f044" !important;
        }
        .save_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
            text-align: center !important;
        }

        .save_icon:before 
        {
            content: "\f0c7" !important;
        }
        .location_icon
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
        }
        .location_icon:before 
        {
            content: "\f041" !important;
        }
        .book_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
        }
        .book_icon:before 
        {
            content: "\f02d" !important;
        }
        .cancel_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #ee784a !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .cancel_icon:hover 
        {
            color: #FF8080 !important;
        }
        .cancel_icon:before
        {
            content: "\f0e2" !important;
        }
        .update_icon
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .update_icon:hover 
        {
            color: #7373FF !important;
        }
        .update_icon:before 
        {
            content: "\f05d " !important;
        }
        .header 
        {
            font-size: 1.25em !important;
        }
        .ProjectDescription 
        {
            font-size: 14px !important;
        }
           .TextBoxEdit {
            width: 100%;
            padding: 7px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;


        }
            .SubmitButton {
              background-image: none !important;
              background-color: #4CAF50;
              color: white;
              padding: 6px 10px;
              border: none;
              border-radius: 4px;
              cursor: pointer;
              margin-top: 0px;
         }
            
.pagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #333333 !important;
            text-decoration: none !important;
            padding-left: 7px !important;
            padding-right: 7px !important;
        }
        .pagenumber:hover{
            background-color : #333333;
            color: #FFFFFF !important;
        }
        .currentpagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #FFFFFF !important;
            background-color: #333333;
            text-decoration: none !important;
        }
         .dxeButtonEditButton{
            background: none !important;
            border: none !important;
        }
        /*.newFont * 
        {
            font-family: Calibri;
            font-size: 16px;
            text-align: center;
        }*/
        
    </style>
    <script>
        var usrName = '<%=HttpUtility.JavaScriptStringEncode(HttpContext.Current.User.Identity.Name)%>';
        function SubmitBtnCLicked(s, e) {
            GridViewPannel.PerformCallback();
        }

        function onDateChange(s, e) {
            GridViewPannel.PerformCallback();
        }
        function onSubmition(s, e) {
            ASPxButton2_Click();
            GridViewPannel.PerformCallback();

        }
        function onGridChange(s, e) {
            detailPanelSmall.PerformCallback();
        }

        var oldwidth = 0;
        var showExport = true;
        $(document).ready(function () {

            var height = Math.max(0, document.documentElement.clientHeight);
            var width = Math.max(0, document.documentElement.clientWidth);

            if (width <= 1023) {
                showExport = false;
            }
            else {
                showExport = true;
            }

            //run on first instance
            if (oldwidth == 0) {
                var height = Math.max(0, document.documentElement.clientHeight);
                var width = Math.max(0, document.documentElement.clientWidth);
                if (width <= 1023) {
                    showExport = false;
                }
                else {
                    showExport = true;
                }

                //show/hide export button first
                detailPanelSmall.PerformCallback();

                //resize grid
                ASPxGridView1.PerformCallback(height + ";" + width);
            }
            //update width
            oldwidth = $(window).width();

            //fire on resize
            $(window).resize(function () {
                var nw = $(window).width();
                //compare new and old width      
                if (oldwidth != nw) {
                    var height = Math.max(0, document.documentElement.clientHeight);
                    var width = Math.max(0, document.documentElement.clientWidth);
                    if (width <= 1023) {
                        showExport = false;
                    }
                    else {
                        showExport = true;
                    }


                    //show/hide export button first
                    detailPanelSmall.PerformCallback();

                    //resize grid

                    ASPxGridView1.PerformCallback(height + ";" + width);
                }

                oldwidth = nw;
            });
        });

    </script>
    <script type="text/javascript">
        var textSeparator = ";";
        function Export() {
            console.log('here');
            Callback.PerformCallback("Excel");
        }
        function OnListBoxSelectionChanged(listBox, args) {
            if (args.index == 0)
                args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
            UpdateSelectAllItemState();
            UpdateText();
        }
        function UpdateSelectAllItemState() {
            IsAllSelected() ? checkListBox.SelectIndices([0]) : checkListBox.UnselectIndices([0]);
        }
        function IsAllSelected() {
            var selectedDataItemCount = checkListBox.GetItemCount() - (checkListBox.GetItem(0).selected ? 0 : 1);
            return checkListBox.GetSelectedItems().length == selectedDataItemCount;
        }
        function UpdateText() {
            var selectedItems = checkListBox.GetSelectedItems();
            checkComboBox.SetText(GetSelectedItemsText(selectedItems));
        }
        function SynchronizeListBoxValues(dropDown, args) {
            checkListBox.UnselectAll();
            var texts = dropDown.GetText().split(textSeparator);
            var values = GetValuesByTexts(texts);
            checkListBox.SelectValues(values);
            UpdateSelectAllItemState();
            UpdateText(); // for remove non-existing texts
        }
        function GetSelectedItemsText(items) {
            var texts = [];
            for (var i = 0; i < items.length; i++)
                if (items[i].index != 0)
                    texts.push(items[i].text);
            return texts.join(textSeparator);
        }
        function GetValuesByTexts(texts) {
            var actualValues = [];
            var item;
            for (var i = 0; i < texts.length; i++) {
                item = checkListBox.FindItemByText(texts[i]);
                if (item != null)
                    actualValues.push(item.value);
            }
            return actualValues;
        }
        function InitOnStart() {
            checkListBox.SelectAll();
            UpdateText(); 
        }
         
    </script>
<h4>
   <span class="text-semibold">Manager / Projects Hours Summary</span></h4>
    <div class="heading-elements">
                            <div class="heading-btn-group">
                                <a href="#" class="btn btn-link btn-float has-text" >
                                    <%--<i class="pdf_icon text-primary"></i><span>PDF</span>--%>
                                </a>
                                <%--<a runat="server" id="ExportBtn" onserverclick="EportBtn_ServerClick" class="btn btn-link btn-float has-text"><i class="excel_icon  text-primary"></i><span>Excel</span></a>--%> 
                            </div>
     </div>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="ContentPage" runat="server"> 
    
    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { LoadingPanel.Hide(); }" />
    </dx:ASPxCallback>
    <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel"
        Modal="True">
    </dx:ASPxLoadingPanel>
    
    <dx:ASPxCallbackPanel ID="DetailPanel" runat="server" ClientInstanceName="detailPanelSmall" Width="100%" 
        CssClass="detailPanelLarge" Collapsible="false"   SettingsLoadingPanel-Enabled="false">
        
        <PanelCollection>
       <dx:PanelContent ID="PanelContent3" runat="server" SupportsDisabledAttribute="True"> 
                       <table style="width: 99%; margin-left: 7px;">
                           <tr>
                               <td style="width:18.66%;">
                        <dx:ASPxComboBox ID="ClientsCombobox" Width="100%" DropDownStyle="DropDown" CaptionCellStyle-Paddings-PaddingTop="10px"
                            Caption="Clients" runat="server" ValueField="CLIENTID" TextField="CLIENTID" CssClass="TextBoxEdit" RootStyle-CssClass="editorContainer"> 
                                <DropDownButton >
                        <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                    </DropDownButton>
                            <RootStyle CssClass="editorContainer"></RootStyle>
                        </dx:ASPxComboBox>
              </td>
                               <td style="width:18.66%;">
                        <dx:ASPxComboBox ID="ProjectsCombobox"  CaptionCellStyle-Paddings-PaddingLeft="4px" CaptionCellStyle-Paddings-PaddingTop="10px"
                            Width="100%"  DropDownStyle="DropDown" Caption="Projects" runat="server" ValueField="projectId" TextField="projectId" CssClass="TextBoxEdit" RootStyle-CssClass="editorContainer"> 
                                <DropDownButton >
                        <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                    </DropDownButton>
                    <RootStyle CssClass="editorContainer"></RootStyle>
                    </dx:ASPxComboBox> 
                    </td>
                   
                        <td style="width:18.66%;">
                    <dx:ASPxDateEdit ID="From"  runat="server"  OnCalendarDayCellPrepared="ASPXDateEdit_CustomCell"  Width="100%" 
                        Caption="From Date" CssClass="TextBoxEdit" CaptionCellStyle-Paddings-PaddingTop="10px"
                        RootStyle-CssClass="editorContainer"  CaptionCellStyle-Paddings-PaddingLeft="4px">
                            <DropDownButton >
                        <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                    </DropDownButton>
                    <RootStyle CssClass="editorContainer"></RootStyle>
                    </dx:ASPxDateEdit> 
                            </td>
                   <%--</tr>
                    <tr>--%>
                        <td style="width:18.66%;">
                    <dx:ASPxDateEdit ID="To"  runat="server"  OnCalendarDayCellPrepared="ASPXDateEdit_CustomCell"  Width="100%"  Caption="To Date" CssClass="TextBoxEdit" CaptionCellStyle-Paddings-PaddingTop="10px"
                        RootStyle-CssClass="editorContainer"  CaptionCellStyle-Paddings-PaddingLeft="4px">
                            <DropDownButton >
                        <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                    </DropDownButton>
                        <RootStyle CssClass="editorContainer"></RootStyle>
                    </dx:ASPxDateEdit>
                    </td>
                        <td style="width:18.66%;">
                    <dx:ASPxComboBox ID="DisciplineComboBox"  Width="100%"  Caption="Discipline"  CaptionCellStyle-Paddings-PaddingLeft="4px" CaptionCellStyle-Paddings-PaddingTop="10px"
                        DropDownStyle="DropDown" runat="server" ValueField="RoleName" TextField="RoleName"  CssClass="TextBoxEdit" RootStyle-CssClass="editorContainer"> 
                            <DropDownButton >
                        <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                    </DropDownButton>
                        <RootStyle CssClass="editorContainer"></RootStyle>
                    </dx:ASPxComboBox>
                   </td>
                        <td style="width:6.66%; margin: 10px;
            float: left;
            margin-bottom: 24px; padding-bottom: 4px;">
                    <dx:ASPxButton ID="filter"   Width="100%"  Text="Submit" runat="server" AutoPostBack="false" CssClass="SubmitButton">
                        <ClientSideEvents Click="SubmitBtnCLicked" />
                    </dx:ASPxButton>
    </td>
                        </tr>

                       </table>
         </dx:PanelContent>
            </PanelCollection>
    </dx:ASPxCallbackPanel>
    <div style="overflow-y: auto;height: calc(100vh - 267px);">
    <dx:ASPxCallbackPanel ID="GridViewPannel" runat="server" ClientInstanceName="GridViewPannel" Collapsible="false" OnCallback="GridViewPannel_CallBack"   SettingsLoadingPanel-Enabled ="true" >
        <PanelCollection>
            <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
               
                <dx:ASPxGridView Visible="false" ID="ASPxGridView1"
                    runat="server" AutoGenerateColumns="False"
                    ClientInstanceName="ASPxGridView1"
                    SettingsDataSecurity-AllowEdit="true"
                    SettingsDataSecurity-AllowInsert="true"
                    SettingsDataSecurity-AllowDelete="true"
                    Width="100%"
                    AutoPostBack="true" 
                    KeyFieldName="TEID"
                    CssClass="newFont"
                    >
                    <SettingsPager CurrentPageNumberFormat="{0}">
                    </SettingsPager>
                    <StylesPager>
                        <PageNumber CssClass="pagenumber"></PageNumber>
                        <CurrentPageNumber CssClass="currentpagenumber"></CurrentPageNumber>
                    </StylesPager>
                    <Settings ShowTitlePanel ="true"  />
                    
                    <SettingsEditing EditFormColumnCount="4" Mode="Inline"/>
                    <Styles>
                        <AlternatingRow Enabled="true" />
                        <Header HorizontalAlign="Center"></Header>
                    </Styles>
                    <ClientSideEvents EndCallback="onGridChange" />                  
                    <Styles>
                        <AlternatingRow Enabled="true" />
                    </Styles>

                    <SettingsPopup>
                        <EditForm Width="100%" Modal="false"/>
                    </SettingsPopup>

                    <SettingsPager PageSize="50" />
                    <Paddings Padding="0px" />
                    <Border BorderWidth="0px" />
                    <BorderBottom BorderWidth="1px" />
                    <Settings ShowFooter="True" />
                    <Styles Header-Wrap="True" />

                    <SettingsCommandButton>
                        <DeleteButton Text=" ">
                            <Styles Style-CssClass="delete_icon"></Styles>
                        </DeleteButton>
                        <NewButton Text=" ">
                            <Styles Style-CssClass="new_icon"></Styles>
                        </NewButton>
                        <EditButton Text=" ">
                            <Styles Style-CssClass="edit_icon"></Styles>
                        </EditButton>
                        <UpdateButton Text=" ">
                            <Styles Style-CssClass="update_icon"></Styles>
                        </UpdateButton>
                        <CancelButton Text=" ">
                            <Styles Style-CssClass="cancel_icon"></Styles>
                        </CancelButton>
                    </SettingsCommandButton>
                    <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
                    <Columns>
                        <%--<dx:GridViewCommandColumn VisibleIndex="0" ShowNewButtonInHeader="true" ShowDeleteButton="true" ShowEditButton="true" />--%>
                        <dx:GridViewDataTextColumn FieldName="Project" Caption="Project ID" VisibleIndex="0" CellStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"> 
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Project Hrs" VisibleIndex="2" CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" 
                             PropertiesTextEdit-DisplayFormatString="N">
                           <%-- <HeaderCaptionTemplate>
                               <i class="book_icon"></i> 
                            </HeaderCaptionTemplate>
                            <EditFormSettings VisibleIndex="3" ColumnSpan="1" />
                            <PropertiesComboBox TextField="ActivityDescription" ValueField="ActivityID" EnableSynchronization="False"
                                IncrementalFilteringMode="StartsWith">
                            </PropertiesComboBox>--%>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn  FieldName="Profit"   VisibleIndex="3" Visible="false">
                        </dx:GridViewDataTextColumn>
                       
                        <dx:GridViewDataTextColumn FieldName="Cost"   VisibleIndex="4" Visible="false">
                        </dx:GridViewDataTextColumn>
                        
                        <dx:GridViewDataTextColumn FieldName="BillableAmt" VisibleIndex="5" Visible="false">
                         </dx:GridViewDataTextColumn>
                        
                        <dx:GridViewDataTextColumn FieldName="BilledAmt" VisibleIndex="6" Visible="false">
                        </dx:GridViewDataTextColumn>
                       
                        <dx:GridViewDataTextColumn FieldName="UnBilled" VisibleIndex="7" Visible="false">
                        </dx:GridViewDataTextColumn> 
                    </Columns>
                    
                </dx:ASPxGridView>
                 <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="ASPxGridView1" ExportSelectedRowsOnly="false"  />
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
        </div>
</asp:Content>
