﻿using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_Notification : System.Web.UI.Page
{
   public DataTable allNot;
    protected void Page_Load(object sender, EventArgs e)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        allNot = GetAllNotification();
       // this.Notifications.DataSource = GetAllNotification();
    }
    protected DataTable GetAllNotification()
    {
        string sql = "Select * from Notifications ";
        return DataAccess.GetDataTableBySqlSyntax(sql,""); 
    }
    [WebMethod]
    [ScriptMethod]
    public static void Delete(string id)
    {
        string sql = string.Format("delete from Notifications where id = {0}", Int32.Parse(id));
        DataAccess.ExecuteSqlStatement(sql,"");

    }

    [WebMethod]
    [ScriptMethod]
    public static string GetAllNotificationAjax()
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        string sql = "Select * from Notifications where seen = 0";
        DataTable dt = DataAccess.GetDataTableBySqlSyntax(sql,"");
        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> row;
        foreach (DataRow dr in dt.Rows)
        {
            row = new Dictionary<string, object>();
            foreach (DataColumn col in dt.Columns)
            {
                row.Add(col.ColumnName, dr[col]);
            }
            rows.Add(row);
        }
      
        return new JavaScriptSerializer().Serialize(rows);
    }
}