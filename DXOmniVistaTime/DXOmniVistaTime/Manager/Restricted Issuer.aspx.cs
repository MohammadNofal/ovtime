﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Text;
using DevExpress.Web;
using DXOmniVistaTimeEngine;

public partial class Example : System.Web.UI.Page
{
    DataTable dt_Issuer = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Issuer"] == null)
        {
            DataAccess.SetSqlConnection("Data Source=edb-omni-prod01;Initial Catalog=CRD; User ID=tm_dev; Password=tm_dev");
            DataTable dt = DataAccess.GetDataTableBySqlSyntax("select * from CSM_RESTRICT_ISSUER", string.Empty);
            dt.PrimaryKey = new DataColumn[] { dt.Columns["ISSUER_CD"] };


            Session["Issuer"] = dt;
            dt_Issuer = dt;
        }
        else
            dt_Issuer = (DataTable)Session["Issuer"];

        if (Session["Valid Issuer"] == null)
        {
            DataAccess.SetSqlConnection("Data Source=edb-omni-prod01;Initial Catalog=CRD; User ID=tm_dev; Password=tm_dev");
            DataTable dt = DataAccess.GetDataTableBySqlSyntax("select ISSUER_CD,ISSUER_NAME from CSM_ISSUER WHERE ISSUER_CD IN ('100801','861196','186130','101695','101743')", string.Empty);
            dt.PrimaryKey = new DataColumn[] { dt.Columns["ISSUER_CD"] };
            Session["Valid Issuer"] = dt;
        }

        this.ASPxGridView1.DataSource = dt_Issuer;
        this.ASPxGridView1.DataBind();
    }
    protected void ASPxGridView1_CellEditorInitialize(object sender, DevExpress.Web.ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "ISSUER_CD")
        {
            ASPxComboBox cmb = new ASPxComboBox();
            cmb = (ASPxComboBox)e.Editor;
            cmb.Columns.Add("ISSUER_CD");
            cmb.Columns.Add("ISSUER_NAME");
            DataTable dt = (DataTable)Session["Valid Issuer"];
            cmb.DataSource = dt;
            cmb.ValueField = "ISSUER_CD";//"ISSUER_CD";
            cmb.TextField = "ISSUER_CD";
            cmb.DataBind();
        }
        else
            e.Editor.ClientVisible = false;
    }
    protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string sql = "DELETE CSM_RESTRICT_ISSUER where ISSUER_CD = '" + e.Keys["ISSUER_CD"].ToString() + "'";
        DataAccess.ExecuteSqlStatement(sql, "");

        DataTable dt = DataAccess.GetDataTableBySqlSyntax("select ISSUER_CD,ISSUER_NAME from CSM_RESTRICT_ISSUER", string.Empty);
        dt.PrimaryKey = new DataColumn[] { dt.Columns["ISSUER_CD"] };
        Session["Issuer"] = dt;
        this.ASPxGridView1.DataSource = dt;
        this.ASPxGridView1.DataBind();
        e.Cancel = true;
        this.ASPxGridView1.CancelEdit();
    }
    protected void ASPxGridView1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {

    }
    protected void ASPxGridView1_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        string Issuer_CD = e.NewValues["ISSUER_CD"].ToString();
        if (((DataTable)Session["Issuer"]).Select("ISSUER_CD = '" + Issuer_CD + "'").Count() > 0)
            this.ASPxGridView1.JSProperties["cpDuplicated"] = true;
        else
        {
            this.ASPxGridView1.JSProperties["cpDuplicated"] = false;
            DataTable _d1 = DataAccess.GetDataTableBySqlSyntax("select ISSUER_NAME from CSM_ISSUER where ISSUER_CD = '" + Issuer_CD + "'", String.Empty);
            string Issuer_Name = _d1.Rows[0][0].ToString();
            if (Issuer_Name.Contains("'"))
                Issuer_Name=Issuer_Name.Insert(Issuer_Name.IndexOf("'"), "'");
            string sql = "INSERT INTO CSM_RESTRICT_ISSUER VALUES ('" + Issuer_CD + "', '" + Issuer_Name + "')";

            DataAccess.ExecuteSqlStatement(sql, "");

            DataTable dt = DataAccess.GetDataTableBySqlSyntax("select ISSUER_CD,ISSUER_NAME from CSM_RESTRICT_ISSUER", string.Empty);
            dt.PrimaryKey = new DataColumn[] { dt.Columns["ISSUER_CD"] };
            Session["Issuer"] = dt;
            this.ASPxGridView1.DataSource = dt;
            this.ASPxGridView1.DataBind();
        }
        e.Cancel = true;
        this.ASPxGridView1.CancelEdit();
    }


}


