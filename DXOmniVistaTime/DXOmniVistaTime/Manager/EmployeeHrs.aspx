﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/OVMaster.master" CodeFile="EmployeeHrs.aspx.cs" Inherits="Manager_EmployeeHrs" %>

<asp:Content ID="header_" ContentPlaceHolderID="Header" runat="server">
<h4><span class="text-semibold">Manager / Employee Hours Detail</span></h4>
    <div class="heading-elements">
                            <div class="heading-btn-group">
                                <a href="#" class="btn btn-link btn-float has-text" >
                                    <%--<i class="pdf_icon text-primary"></i><span>PDF</span>--%>
                                </a>
                                <%--<a runat="server" id="ExportBtn" onserverclick="EportBtn_ServerClick" class="btn btn-link btn-float has-text"><i class="excel_icon  text-primary"></i><span>Excel</span></a>--%>
                            
                            </div>
                        </div>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="ContentPage" runat="server">
    <style>
         .dxWeb_pPrevDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNext {
             background: url('../content/images/iconmonstr-arrow-63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pPrev {
             background: url('../content/images/iconmonstr-arrow-64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNextDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }
        .month1_border 
        {
            border-left-width: 2px !important;
        }
        .head_container 
        {
            background-color: #EEE;
            float: right;
            margin-right: 7.8%;
        }

        .Month1_css 
        {
            border-style: solid;
            border-width: medium;
            float: right;
            background-color: red;
        }
        .Month2_css 
        {
            border-style: solid;
            border-width: medium;
            float: right;
        }
        .delete_icon 
        {
            position: relative;
            font-family: "Courier New" !important;
            color: #ee784a !important;
            font-size: 200% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .delete_icon:hover 
        {
            color: #FF0000 !important;
        }
        .delete_icon::before 
        {
            content: "\000D7" !important;
        }
        .new_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 20px !important;
            padding: 1px 1px;
            text-decoration: none !important;
        }

        .new_icon:hover 
        {
            color: #7373FF !important;
        }
        .new_icon:before 
        {
           content: "\f067" !important;
        }
        .edit_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #f3cb76 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .edit_icon:hover 
        {
            color: #FF8000 !important;
        }
        .edit_icon:before 
        {
            content: "\f044" !important;
        }
        .save_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
            text-align: center !important;
        }

        .save_icon:before 
        {
            content: "\f0c7" !important;
        }
        .location_icon
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
        }
        .location_icon:before 
        {
            content: "\f041" !important;
        }
        .book_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
        }
        .book_icon:before 
        {
            content: "\f02d" !important;
        }
        .cancel_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #ee784a !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .cancel_icon:hover 
        {
            color: #FF8080 !important;
        }
        .cancel_icon:before
        {
            content: "\f0e2" !important;
        }
        .update_icon
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .update_icon:hover 
        {
            color: #7373FF !important;
        }
        .update_icon:before 
        {
            content: "\f05d " !important;
        }
        .header 
        {
            font-size: 1.25em !important;
        }
        .ProjectDescription 
        {
            font-size: 14px !important;
        }

        /*.newFont * 
        {
            font-family: Calibri;
            font-size: 16px;
            text-align: center;
        }*/
        .editorContainer {
            width: 100%;
        }

        .TextBoxEdit {
            width: 100%;
            padding: 7px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;


        }
        .SubmitButton {
              background-image: none !important;
              background-color: #4CAF50;
              color: white;
              padding: 6px 10px;
              border: none;
              border-radius: 4px;
              cursor: pointer;
              margin-top: 0px;
         }
        
.pagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #333333 !important;
            text-decoration: none !important;
            padding-left: 7px !important;
            padding-right: 7px !important;
        }
        .pagenumber:hover{
            background-color : #333333;
            color: #FFFFFF !important;
        }
        .currentpagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #FFFFFF !important;
            background-color: #333333;
            text-decoration: none !important;
        }
         .dxeButtonEditButton{
            background: none !important;
            border: none !important;
        }
    </style>
    <script>
        var usrName = '<%=HttpUtility.JavaScriptStringEncode(HttpContext.Current.User.Identity.Name)%>';

        function onDateChange(s, e) {
            GridViewPannel.PerformCallback();
        }
        function onSubmition(s, e) {
            ASPxButton2_Click();
            GridViewPannel.PerformCallback();

        }
        function onGridChange(s, e) {
            detailPanelSmall.PerformCallback();
        }

        var oldwidth = 0;
        var showExport = true;
        $(document).ready(function () {

            var height = Math.max(0, document.documentElement.clientHeight);
            var width = Math.max(0, document.documentElement.clientWidth);

            if (width <= 1023) {
                showExport = false;
            }
            else {
                showExport = true;
            }

            //run on first instance
            if (oldwidth == 0) {
                var height = Math.max(0, document.documentElement.clientHeight);
                var width = Math.max(0, document.documentElement.clientWidth);
                if (width <= 1023) {
                    showExport = false;
                }
                else {
                    showExport = true;
                }

                //show/hide export button first
                detailPanelSmall.PerformCallback();

                //resize grid
                ASPxGridView1.PerformCallback(height + ";" + width);
            }
            //update width
            oldwidth = $(window).width();

            //fire on resize
            $(window).resize(function () {
                var nw = $(window).width();
                //compare new and old width      
                if (oldwidth != nw) {
                    var height = Math.max(0, document.documentElement.clientHeight);
                    var width = Math.max(0, document.documentElement.clientWidth);
                    if (width <= 1023) {
                        showExport = false;
                    }
                    else {
                        showExport = true;
                    }


                    //show/hide export button first
                    detailPanelSmall.PerformCallback();

                    //resize grid

                    ASPxGridView1.PerformCallback(height + ";" + width);
                }

                oldwidth = nw;
            });
        });



    </script>
  
    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { LoadingPanel.Hide(); }" />
    </dx:ASPxCallback>
    <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel"
        Modal="True">
    </dx:ASPxLoadingPanel>
    
    <dx:ASPxCallbackPanel ID="DetailPanel" runat="server" ClientInstanceName="detailPanelSmall" Width="100%" CssClass="detailPanelLarge" Collapsible="false"   SettingsLoadingPanel-Enabled="false">
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <Styles>
            <ExpandBar Width="100%" CssClass="bar">
            </ExpandBar>
            <ExpandedExpandBar CssClass="expanded">
            </ExpandedExpandBar>
        </Styles>
        <BorderTop BorderWidth="0px"></BorderTop>
      
      
         <PanelCollection>
        <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
            <table style="width: 99%; margin-left: 7px;">
    <tr>
        <td style="width: 23%;" >
            <dx:ASPxComboBox ID="EmployeesComboBox" Caption="Employee" runat="server" AutoPostBack="false" ClientInstanceName="UserNameComboBox"
                        CssClass="TextBoxEdit" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption" ClearButton-Visibility="True" 
                        OnSelectedIndexChanged="EmployeesComboBox_SelectedIndexChanged" ValidationSettings-RequiredField-IsRequired="true" 
                ValidationSettings-ErrorDisplayMode="None"  CaptionCellStyle-Paddings-PaddingTop="10px" Width="100%"   >
                    <DropDownButton >
                        <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                    </DropDownButton>
                    <%--<ClearButton Visibility="True" DisplayMode="Always"></ClearButton>--%>
                   <%-- <Paddings PaddingLeft="30px" />--%>
                    <CaptionCellStyle CssClass="editorCaption" />
                    
                
                    <RootStyle CssClass="editorContainer"/>
    </dx:ASPxComboBox>
        </td>
        <td style="width: 23%;">
             <dx:ASPxComboBox ID="ProjectsComboBox" Caption="Project ID" runat="server" AutoPostBack="false" ClientInstanceName="UserNameComboBox"  Width="100%"
                        CssClass="TextBoxEdit" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption" ClearButton-Visibility="True" CaptionCellStyle-Paddings-PaddingLeft="4px"
                 CaptionCellStyle-Paddings-PaddingTop="10px" >
                 <DropDownButton >
                        <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                    </DropDownButton>
                    <%--<ClearButton Visibility="True" DisplayMode="Always"></ClearButton>--%>
                    <%--<Paddings PaddingLeft="30px" />--%>
                    <CaptionCellStyle CssClass="editorCaption" />
                    <RootStyle CssClass="editorContainer"/>
            </dx:ASPxComboBox>
        </td>
          <td style="width: 23%;"> 
              <dx:ASPxDateEdit ID="From" Caption="Start Date"  OnCalendarDayCellPrepared="ASPXDateEdit_CustomCell" runat="server" CssClass="TextBoxEdit" RootStyle-CssClass="editorContainer" Width="100%" CaptionCellStyle-Paddings-PaddingTop="10px"
                  CaptionCellStyle-Paddings-PaddingLeft="4px">
                  <DropDownButton >
                        <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                    </DropDownButton>
              </dx:ASPxDateEdit> 
            </td>
            <td style="width: 23%;">
              <dx:ASPxDateEdit ID="To" Caption="End Date" runat="server"  OnCalendarDayCellPrepared="ASPXDateEdit_CustomCell" CssClass="TextBoxEdit" RootStyle-CssClass="editorContainer" Width="100%" CaptionCellStyle-Paddings-PaddingTop="10px" 
                  CaptionCellStyle-Paddings-PaddingLeft="4px">
                  <DropDownButton >
                        <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                    </DropDownButton>
             </dx:ASPxDateEdit>
            </td>
        <td style=" margin: 10px;
            float: left;
            margin-bottom: 24px;
            width: 8%;">
            <dx:ASPxButton ID="Filter" runat="server" OnClick="Filter_Click" Text="Submit" Width="100%" CssClass="SubmitButton" ></dx:ASPxButton>
        </td>
    </tr>
    </table>
            </dx:PanelContent>
             </PanelCollection>
    </dx:ASPxCallbackPanel>

    

   

       <div style="overflow-y: auto;height: calc(100vh - 267px);">
    <dx:ASPxCallbackPanel ID="GridViewPannel" runat="server" ClientInstanceName="GridViewPannel" Collapsible="false"   SettingsLoadingPanel-Enabled ="true" >
        <PanelCollection>
            <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
               
                <dx:ASPxGridView Visible="false" ID="ASPxGridView1"
                    runat="server" AutoGenerateColumns="False"
                    ClientInstanceName="ASPxGridView1"
                    SettingsDataSecurity-AllowEdit="true"
                    SettingsDataSecurity-AllowInsert="true"
                    SettingsDataSecurity-AllowDelete="true"
                    Width="100%"
                    AutoPostBack="true"
                 
                    KeyFieldName="tedate"
                    CssClass="newFont"
                    >
                    <SettingsPager CurrentPageNumberFormat="{0}">
                    </SettingsPager>
                    <StylesPager>
                        <PageNumber CssClass="pagenumber"></PageNumber>
                        <CurrentPageNumber CssClass="currentpagenumber"></CurrentPageNumber>
                    </StylesPager>
                    <Settings ShowTitlePanel ="true"  />
                    
                   <SettingsEditing EditFormColumnCount="4" Mode="Inline"/>
                    <ClientSideEvents EndCallback="onGridChange" />                  
                    <Styles>
                        <AlternatingRow Enabled="true" />
                        <Header HorizontalAlign="Center"></Header>
                    </Styles>

                    <SettingsPopup>
                        <EditForm Width="100%" Modal="false"/>
                    </SettingsPopup>

                    <SettingsPager PageSize="50" />
                    <Paddings Padding="0px" />
                    <Border BorderWidth="0px" />
                    <BorderBottom BorderWidth="1px" />
                    <Settings ShowFooter="True" />
                    <Styles Header-Wrap="True" />

                    <SettingsCommandButton>
                        <DeleteButton Text=" ">
                            <Styles Style-CssClass="delete_icon"></Styles>
                        </DeleteButton>
                        <NewButton Text=" ">
                            <Styles Style-CssClass="new_icon"></Styles>
                        </NewButton>
                        <EditButton Text=" ">
                            <Styles Style-CssClass="edit_icon"></Styles>
                        </EditButton>
                        <UpdateButton Text=" ">
                            <Styles Style-CssClass="update_icon"></Styles>
                        </UpdateButton>
                        <CancelButton Text=" ">
                            <Styles Style-CssClass="cancel_icon"></Styles>
                        </CancelButton>
                    </SettingsCommandButton>
                    <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
                    <Columns>
                        <%--<dx:GridViewCommandColumn VisibleIndex="0" ShowNewButtonInHeader="true" ShowDeleteButton="true" ShowEditButton="true" />--%>
                         
                        <%--<dx:GridViewDataComboBoxColumn FieldName="EmployeeID/Vendor" Caption="EmployeeID" VisibleIndex="1"> 
                        </dx:GridViewDataComboBoxColumn>--%>
                        <dx:GridViewDataComboBoxColumn FieldName="tedate" VisibleIndex="2" Visible="false" >
                           <%-- <HeaderCaptionTemplate>
                               <i class="book_icon"></i> 
                            </HeaderCaptionTemplate>
                            <EditFormSettings VisibleIndex="3" ColumnSpan="1" />
                            <PropertiesComboBox TextField="ActivityDescription" ValueField="ActivityID" EnableSynchronization="False"
                                IncrementalFilteringMode="StartsWith">
                            </PropertiesComboBox>--%>
                        </dx:GridViewDataComboBoxColumn>

                        <dx:GridViewDataTextColumn  FieldName="Project" Caption="Project ID"   VisibleIndex="3" CellStyle-HorizontalAlign="Left" >
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn  FieldName="EmployeeID" Caption="Employee ID"   VisibleIndex="5" Visible="false" CellStyle-HorizontalAlign="Right">
                        </dx:GridViewDataTextColumn>
                        
                        <dx:GridViewDataTextColumn  FieldName="ProjectConAmt" VisibleIndex="3" Visible="false">
                        </dx:GridViewDataTextColumn>
                       
                        <dx:GridViewDataTextColumn FieldName="ProjectCost"   VisibleIndex="4" Visible="false">
                        </dx:GridViewDataTextColumn>
                        
                        <dx:GridViewDataTextColumn FieldName="Project Hrs" VisibleIndex="5" Caption="Total Hours" CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center"  >
                         </dx:GridViewDataTextColumn>
                        
                        <dx:GridViewDataTextColumn FieldName="ProjectTimeCost" VisibleIndex="6" Visible="false">
                        </dx:GridViewDataTextColumn>
                       
                        <dx:GridViewDataTextColumn FieldName="ProjectTimeCostOH" VisibleIndex="7" Visible="false">
                        </dx:GridViewDataTextColumn> 
                        <dx:GridViewDataTextColumn FieldName="EmployeeProjectTimeCost" VisibleIndex="8" Visible="false">
                        </dx:GridViewDataTextColumn> 

                        <dx:GridViewDataTextColumn FieldName="EmployeeProjectTimeCost" VisibleIndex="9" Visible="false">
                        </dx:GridViewDataTextColumn> 

                        <dx:GridViewDataTextColumn FieldName="EmpCostRate" VisibleIndex="10" Visible="false">
                        </dx:GridViewDataTextColumn> 

                        <dx:GridViewDataTextColumn FieldName="TECostRate" VisibleIndex="11" Visible="false">
                        </dx:GridViewDataTextColumn> 

                         <dx:GridViewDataTextColumn FieldName="OverheadFactor" VisibleIndex="12" Visible="false">
                        </dx:GridViewDataTextColumn> 

                        <dx:GridViewDataTextColumn FieldName="VendorFlag" VisibleIndex="13" Visible="false">
                        </dx:GridViewDataTextColumn> 

                        <dx:GridViewDataTextColumn FieldName="ProjectExpense" VisibleIndex="14" Visible="false">
                        </dx:GridViewDataTextColumn> 

                        <dx:GridViewDataTextColumn FieldName="BillableAmt" VisibleIndex="15" Visible="false">
                        </dx:GridViewDataTextColumn> 

                         <dx:GridViewDataTextColumn FieldName="BilledAmt" VisibleIndex="16" Visible="false">
                        </dx:GridViewDataTextColumn> 
                         <dx:GridViewDataTextColumn FieldName="ProfitLoss" VisibleIndex="7" Visible="false">
                        </dx:GridViewDataTextColumn> 

                    </Columns>
                    
                </dx:ASPxGridView>
                 <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="ASPxGridView1" ExportSelectedRowsOnly="false" />
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
  </div>
</asp:Content>
