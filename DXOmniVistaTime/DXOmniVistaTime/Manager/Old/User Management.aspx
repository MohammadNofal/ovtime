﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Main.master" CodeFile="User Management.aspx.cs" Inherits="Users" %>

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function onSubmit(s, e) {
            userdetailpanel.PerformCallback("Filter");
        }

        function FilterPanaelCallback(s, e) {
            //var error = "";
            //if (s.cpInvalidUserName) {
            //    s.cpInvalidUserName = false;
            //    error = error + "UserName already Exists. ";
            //}
            //if (s.cpInvalidEmail) {
            //    s.cpInvalidEmail = false;
            //    error = error + "Invalid Email";
            //}
            //if (error != "") {
            //    alert(error);
            //}
            //else {
                FilterPanel.PerformCallback("Filter");
            //}
        }
    </script>
    
    <dx:ASPxCallbackPanel ID="FilterPanel" runat="server" ClientInstanceName="FilterPanel" Width="100%" CssClass="detailPanelMidian" Collapsible="false" OnCallback="FilterPanel_Callback"  SettingsLoadingPanel-Enabled ="false" >
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <Styles>
            <ExpandBar Width="100%" CssClass="bar">
            </ExpandBar>
            <ExpandedExpandBar CssClass="expanded">
            </ExpandedExpandBar>
        </Styles>
        <BorderTop BorderWidth="0px">
        </BorderTop>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                Accounts Management:
                <br />
                <br />
                <dx:ASPxComboBox ID="UserNameComboBox" Caption="UserName" runat="server" AutoPostBack="false"
                        CssClass="editor" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption" ClearButton-Visibility="True">
                    <CaptionCellStyle CssClass="editorCaption" />
                    <RootStyle CssClass="editorContainer"/>
                </dx:ASPxComboBox>
                <dx:ASPxComboBox ID="RoleCombobox" Caption="Role" runat="server" AutoPostBack="false"
                        CssClass="editor" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption" ClearButton-Visibility="True">
                    <CaptionCellStyle CssClass="editorCaption" />
                    <RootStyle CssClass="editorContainer"/>
                </dx:ASPxComboBox>
                <dx:ASPxButton ID="Submit" runat="server" Text="Submit " ImagePosition = "Right" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption"  AutoPostBack="false" Image-Url="~/Content/Images/Icons/check-64-icon.png" Image-Height="20px" Image-Width="20px" Height="36px">
                    <ClientSideEvents Click="onSubmit" />                
                </dx:ASPxButton>
            </dx:PanelContent>
        </PanelCollection>
        <Paddings Padding="8px" />
    </dx:ASPxCallbackPanel>

    <dx:ASPxCallbackPanel ID="userdetailpanel" ClientInstanceName="userdetailpanel" runat="server" OnCallback="userdetailpanel_Callback">
        <PanelCollection>
            <dx:PanelContent>
                <dx:ASPxGridView ID="LoginGridView" runat="server" AutoGenerateColumns="False" 
                    ClientInstanceName="LoginGridView" Width="75%" AutoPostBack="true" KeyFieldName="UserId"
                    OnRowValidating="LoginGridView_RowValidating"
                    OnRowDeleting = "LoginGridView_RowDeleting"
                    OnRowUpdating = "LoginGridView_RowUpdating"
                    OnRowInserting ="LoginGridView_RowInserting"
                    OnCellEditorInitialize="LoginGridView_CellEditorInitialize">
                    <ClientSideEvents EndCallback="FilterPanaelCallback"/>
                    <%--OnBeforeColumnSortingGrouping="LoginGridView_BeforeColumnSortingGrouping"--%>
                    <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
                    <Paddings PaddingLeft="10px" />

                    <Styles Header-Wrap="True" >
                        <Header Wrap="True">
                        </Header>
                        <AlternatingRow Enabled="True">
                        </AlternatingRow>
                    </Styles>

                    <Paddings PaddingTop="10px" />
                    <SettingsCommandButton>
                        <NewButton Styles-Style-ForeColor="White" Image-Url="~/Content/Images/Icons/comment-user-add-icon.png"/>
                        <DeleteButton Image-Url="~/Content/Images/Icons/comment-user-close-icon.png"/>
                        <EditButton Image-Url="~/Content/Images/Icons/comment-user-page-icon.png"/> 
                        <UpdateButton Image-Url="~/Content/Images/Icons/comment-user-page-icon.png" />
                        <CancelButton Image-Url="~/Content/Images/Icons/comment-user-close-icon.png" />
                    </SettingsCommandButton>
                    <SettingsPager PageSize="20" />
                    <SettingsEditing Mode="EditForm" />
                    <Settings ShowFooter="True" />
                    <SettingsBehavior ConfirmDelete="true" />
                    <SettingsText ConfirmDelete="Do you want delete this user?" />
                    <Columns>
                        <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="true" ShowNewButtonInHeader="true" VisibleIndex="0" Width="10%"/>
                        <dx:GridViewDataColumn FieldName="UserName" HeaderStyle-Font-Size="Medium" ReadOnly="true"/>
                        <dx:GridViewDataColumn FieldName="Email" HeaderStyle-Font-Size="Medium"/>
                        <dx:GridViewDataDateColumn FieldName="CreateDate" HeaderStyle-Font-Size="Medium" ReadOnly="true" Visible="false"/>
                        <dx:GridViewDataDateColumn FieldName="LastLoginDate" HeaderStyle-Font-Size="Medium" ReadOnly="true" Visible="false">
                            <PropertiesDateEdit DisplayFormatString="MM/dd/yyyy hh:mm tt"></PropertiesDateEdit><HeaderStyle Font-Size="Medium"></HeaderStyle>
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataDateColumn FieldName="LastActivityDate" HeaderStyle-Font-Size="Medium" ReadOnly="true" Visible="false">
                            <PropertiesDateEdit DisplayFormatString="MM/dd/yyyy hh:mm tt"></PropertiesDateEdit><HeaderStyle Font-Size="Medium"></HeaderStyle>
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataColumn FieldName="Password" HeaderStyle-Font-Size="Medium"/>
                        <dx:GridViewDataComboBoxColumn FieldName="Role" HeaderStyle-Font-Size="Medium">
                        </dx:GridViewDataComboBoxColumn>
                        <dx:GridViewDataCheckColumn FieldName="IsLockedOut" Caption="Locked Out" HeaderStyle-Font-Size="Medium"/>
                        <dx:GridViewDataCheckColumn FieldName="IsLoggedIn" Caption="Logged In" HeaderStyle-Font-Size="Medium" ReadOnly="true"/>
                    </Columns>

                    <Styles>
                        <Header Wrap="True">
                        </Header>
                        <AlternatingRow Enabled="true" />
                    </Styles>
                    <Paddings Padding="0px" />
                    <Border BorderWidth="0px" />
                    <BorderBottom BorderWidth="1px" />
                </dx:ASPxGridView>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
</asp:Content>
