﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Light.master" CodeFile="ProjectSummary.aspx.cs" Inherits="Attendance" %>

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">

        <dx:ASPxCallbackPanel ID="LeftPane2" runat="server" FixedPosition="WindowLeft" ClientInstanceName="leftPane" CssClass="leftPane2" Collapsible="true" OnCallback="LeftPane_Callback" ScrollBars="Auto" SettingsLoadingPanel-Enabled="false">
        
        <SettingsAdaptivity CollapseAtWindowInnerWidth="1023" />
            <ClientSideEvents/>
            
            <Styles>
                <Panel CssClass="panel"></Panel>
            </Styles>
            <PanelCollection>
                <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
                        

                             <dx:ASPxNavBar Width="100%" EnableViewState="False" CssClass="LeftNavBar"
                                ID="navbar" runat="server" AutoCollapse="False" EncodeHtml="False" AllowSelectItem="False" ItemStyle-HoverStyle-BackColor ="LightGray" >
                                <GroupHeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTextTemplate>
                                    <span style="vertical-align: top; display: block; margin: 1px 0 0 1px">

                                        <%# Eval("Text") %>

                                    </span>
                                </ItemTextTemplate>
                            </dx:ASPxNavBar>
                            
                            
                                    
                        <%--<dx:ASPxLabel ID="txtBox" runat="server"></dx:ASPxLabel>
                        <div id="listWidget"></div>--%>
                                    
                                   
                        
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>

    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="True" ClientInstanceName="ASPxGridView1"
    Width="90%" AutoPostBack="true"  
        OnDataBinding ="ASPxGridView1_DataBinding" 
        OnDataBound = "ASPxGridView1_DataBound"
        OnBeforeColumnSortingGrouping="ASPxGridView1_BeforeColumnSortingGrouping">
        <SettingsPager PageSize="50" />
        <Paddings Padding="0px" />
        <Paddings PaddingLeft="10px" />
        <Paddings PaddingTop="10px" />
        <Border BorderWidth="0px" />
        <BorderBottom BorderWidth="1px" />
        <Settings ShowFooter="True" />
        <Styles Header-Wrap="True" />
        <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
        <Columns>
            <dx:GridViewDataColumn GroupIndex="1" FieldName="ProjectID" HeaderStyle-Font-Size="Small" CellStyle-Font-Size="Small" CellStyle-HorizontalAlign="Left" VisibleIndex="1">
            </dx:GridViewDataColumn>
            <dx:GridViewDataTextColumn FieldName="SchDate" HeaderStyle-Font-Size="Small" CellStyle-Font-Size="Small" CellStyle-HorizontalAlign="Left" VisibleIndex="2">
                <PropertiesTextEdit DisplayFormatInEditMode="True" DisplayFormatString="{0:dd/MM/yyyy}" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ScheduleNotes" Caption="Sch Notes" HeaderStyle-Font-Size="Small" CellStyle-Font-Size="Small" CellStyle-HorizontalAlign="Left" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Percent" HeaderStyle-Font-Size="Small" CellStyle-Font-Size="Small" CellStyle-HorizontalAlign="Left" VisibleIndex="4">
                <PropertiesTextEdit DisplayFormatString="#,###.00%" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="SchAmt" HeaderStyle-Font-Size="Small" CellStyle-Font-Size="Small" CellStyle-HorizontalAlign="Left" VisibleIndex="5">
                <PropertiesTextEdit DisplayFormatString="Dhs #,###.00" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="InvNum" HeaderStyle-Font-Size="Small" CellStyle-Font-Size="Small" CellStyle-HorizontalAlign="Left" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="InvDate" HeaderStyle-Font-Size="Small" CellStyle-Font-Size="Small" CellStyle-HorizontalAlign="Left" VisibleIndex="7">
                <PropertiesTextEdit DisplayFormatInEditMode="True" DisplayFormatString="{0:dd/MM/yyyy}" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="BillAmt" HeaderStyle-Font-Size="Small" CellStyle-Font-Size="Small" CellStyle-HorizontalAlign="Left" VisibleIndex="8">
                <PropertiesTextEdit DisplayFormatString="Dhs #,###.00" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Retainage" HeaderStyle-Font-Size="Small" CellStyle-Font-Size="Small" CellStyle-HorizontalAlign="Left" VisibleIndex="9">
                <PropertiesTextEdit DisplayFormatString="Dhs #,###.00" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="PayDate" HeaderStyle-Font-Size="Small" CellStyle-Font-Size="Small" CellStyle-HorizontalAlign="Left" VisibleIndex="10">
                <PropertiesTextEdit DisplayFormatInEditMode="True" DisplayFormatString="{0:dd/MM/yyyy}" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="AmtPaid" HeaderStyle-Font-Size="Small" CellStyle-Font-Size="Small" CellStyle-HorizontalAlign="Left" VisibleIndex="11">
                <PropertiesTextEdit DisplayFormatString="Dhs #,###.00" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="InvBalance" HeaderStyle-Font-Size="Small" CellStyle-Font-Size="Small" CellStyle-HorizontalAlign="Left" VisibleIndex="12">
                <PropertiesTextEdit DisplayFormatString="Dhs #,###.00" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Balance" HeaderStyle-Font-Size="Small" CellStyle-Font-Size="Small" CellStyle-HorizontalAlign="Left" VisibleIndex="13">
                <PropertiesTextEdit DisplayFormatString="Dhs #,###.00" />
            </dx:GridViewDataTextColumn>
        </Columns>
        <Settings ShowGroupPanel="true" ShowFooter="true" ShowGroupFooter="VisibleIfExpanded"  />
        <GroupSummary>
            <dx:ASPxSummaryItem FieldName="SchAmt" ShowInGroupFooterColumn="SchAmt" SummaryType="Sum" DisplayFormat="Dhs #,###.00" />
            <dx:ASPxSummaryItem FieldName="BillAmt" ShowInGroupFooterColumn="BillAmt" SummaryType="Sum" DisplayFormat="Dhs #,###.00" />
            <dx:ASPxSummaryItem FieldName="Retainage" ShowInGroupFooterColumn="Retainage" SummaryType="Sum" DisplayFormat="Dhs #,###.00" />
            <dx:ASPxSummaryItem FieldName="AmtPaid" ShowInGroupFooterColumn="AmtPaid" SummaryType="Sum" DisplayFormat="Dhs #,###.00" />
            <dx:ASPxSummaryItem FieldName="InvBalance" ShowInGroupFooterColumn="InvBalance" SummaryType="Sum" DisplayFormat="Dhs #,###.00" />
            <dx:ASPxSummaryItem FieldName="Balance" ShowInGroupFooterColumn="Balance" SummaryType="Sum" DisplayFormat="Dhs #,###.00" />
        </GroupSummary>
        <Styles>
            <AlternatingRow Enabled="true" />
        </Styles>
    </dx:ASPxGridView>

</asp:Content>
