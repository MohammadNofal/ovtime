﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Nonav.master" CodeFile="HolidayCalendar.aspx.cs" Inherits="HolidayCalendar" %>

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">

        <dx:ASPxCallbackPanel ID="LeftPane2" runat="server" FixedPosition="WindowLeft" ClientInstanceName="leftPane" Width="400px" CssClass="leftPane2" Collapsible="true" OnCallback="LeftPane_Callback" ScrollBars="Auto" SettingsLoadingPanel-Enabled="false">
        <SettingsAdaptivity CollapseAtWindowInnerWidth="1023" />
        <ClientSideEvents/>
        <SettingsLoadingPanel Enabled="False">
        </SettingsLoadingPanel>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
                <dx:ASPxPanel ID="ASPxPanel2" runat="server" CssClass="detailPanelSmallHeaderBlue">
                </dx:ASPxPanel>
                <dx:ASPxPanel ID="ASPxPanel3" runat="server" CssClass="detailPanelSmallBlue">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent3" runat="server" SupportsDisabledAttribute="True">
                          Grid List:
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
                <dx:ASPxNavBar Width="100%" EnableViewState="False" CssClass="LeftNavBar"
                            ID="navbar" runat="server" AutoCollapse="False" EncodeHtml="False" AllowSelectItem="False" ItemStyle-HoverStyle-BackColor ="LightGray" >
                    <GroupHeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" >
                    <HoverStyle BackColor="LightGray">
                    </HoverStyle>
                    </ItemStyle>
                    <ItemTextTemplate>
                        <span style="vertical-align: top; display: block; margin: 1px 0 0 1px"><%# Eval("Text") %></span>
                    </ItemTextTemplate>
                </dx:ASPxNavBar>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
    <dx:ASPxPanel ID="ASPxPanel1" runat="server" CssClass="detailPanelSmallHeader">
    </dx:ASPxPanel>

    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" ClientInstanceName="ASPxGridView1" Width="65%" AutoPostBack="true" KeyFieldName="HolidayName;HolidayDate"
        OnRowInserting = "ASPxGridView1_RowInserting"
        OnRowDeleting = "ASPxGridView1_RowDeleting"
        OnRowUpdating = "ASPxGridView1_RowUpdating"
        OnDataBinding ="ASPxGridView1_DataBinding" 
        OnDataBound = "ASPxGridView1_DataBound"
        OnBeforeColumnSortingGrouping="ASPxGridView1_BeforeColumnSortingGrouping">
        <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
        <Paddings Padding="0px" />
        <Styles>
            <Header Wrap="True">
            </Header>
            <AlternatingRow Enabled="true" />
        </Styles>
        <Paddings PaddingLeft="10px" />
        <SettingsPager PageSize="50" />
        <Settings ShowFooter="True" />
        <SettingsBehavior ConfirmDelete="true" />
        <SettingsText ConfirmDelete="Do you want delete this row?" />
        <SettingsEditing Mode="Batch" />
        <Columns>
            <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="False" ShowNewButtonInHeader="True" VisibleIndex="0" Width="1%">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="HolidayName" HeaderStyle-Font-Size="Medium" CellStyle-Font-Size="Medium" Width="16%" CellStyle-HorizontalAlign="Left" VisibleIndex="1">
                <HeaderStyle Font-Size="Medium"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" Font-Size="Medium">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="HolidayDate" HeaderStyle-Font-Size="Medium" CellStyle-Font-Size="Medium" Width="16%" CellStyle-HorizontalAlign="Left" VisibleIndex="2">
                <HeaderStyle Font-Size="Medium"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" Font-Size="Medium">
                </CellStyle>
            </dx:GridViewDataDateColumn>
        </Columns>
        <Styles Header-Wrap="True" >
            <Header Wrap="True">
            </Header>
            <AlternatingRow Enabled="True">
            </AlternatingRow>
        </Styles>
        <Paddings PaddingTop="10px" />
        <Border BorderWidth="0px" />
        <BorderBottom BorderWidth="1px" />
    </dx:ASPxGridView>

</asp:Content>
