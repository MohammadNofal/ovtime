﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DXOmniVistaTimeEngine;
using System.Web.Security;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;

public partial class Attendance : System.Web.UI.Page
{
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);

        this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["EmployeeSummary"]);
        this.ASPxGridView1.SettingsPager.Mode = DevExpress.Web.GridViewPagerMode.ShowAllRecords; // this disables pagination, was taking too long if second page clicked
        this.ASPxGridView1.DataBind();

        NavBarDataBind();
    }

    protected void LeftPane_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
        //txtBox.Value = "records may need refresh";
        //NavBarDataBind();

    }

    private void NavBarDataBind()
    {
        //string currentGroup = "Date Filter";
        //DevExpress.Web.NavBarGroup gr = new DevExpress.Web.NavBarGroup();
        //gr.Text = currentGroup;
        //gr.Name = currentGroup;
        //navbar.Groups.Add(gr);

        //string gr1 = "Date Filter";
        //DevExpress.Web.NavBarGroup navBarGr = navbar.Groups.FindByName(gr1);

        //DevExpress.Web.NavBarItem it = new DevExpress.Web.NavBarItem();
        //DevExpress.Web.GridViewDataDateColumn.CreateColumn();
        //string result = "<dx:ASPxDateEdit ID=\"deStart\" ClientInstanceName=\"deStart\" runat=\"server\" Caption=\"Start Date\"></dx:ASPxDateEdit>";
        //it.Text = result;
        //navbar.Groups[navBarGr.Index].Items.Add(it);
    }

    public class Project
    {
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }
    }

    protected void ASPxGridView1_CustomColumnSort(object sender, DevExpress.Web.CustomColumnSortEventArgs e)
    {

    }

    protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
    {

    }

    protected void ASPxGridView1_DataBound(object sender, EventArgs e)
    {

    }

    protected void ASPxGridView1_BeforeColumnSortingGrouping(object sender, DevExpress.Web.ASPxGridViewBeforeColumnGroupingSortingEventArgs e)
    {
        this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["EmployeeSummary"]);
        this.ASPxGridView1.DataBind();
    }

    protected void txtDateTime_DateChanged(object sender, EventArgs e)
    {
        string start = this.StartDate.Value.ToString();
        string end = this.EndDate.Value.ToString();
        string query = String.Format(";WITH cte AS ( SELECT e.EmployeeID, SUBSTRING(e.EmpFName, 1, 1) + LOWER(SUBSTRING(e.EmpFName, 2, LEN(e.EmpFName))) + ' ' + SUBSTRING(e.EmpLName, 1, 1) + LOWER(SUBSTRING(e.EmpLName, 2, LEN(e.EmpLName))) as EmployeeName, ISNULL(e.EmpVac,0) * 8 AS Vacation, ISNULL(e.EmpSick,0) * 8 AS Sick, SUM(case when ActivityID = 'GEN:VAC' THEN ISNULL(TEHours,0) ELSE 0 END) AS UsedVacation, SUM(case when ActivityID = 'GEN:SICK' THEN ISNULL(TEHours,0) ELSE 0 END) AS UsedSick FROM TimeEntry t JOIN Employee e on e.EmployeeID = t.EmployeeID WHERE t.ProjectID LIKE '40%' AND t.CreatedOn > '{0}' AND t.CreatedOn < '{1}' GROUP BY e.EmployeeID, e.EmpVac, e.EmpSick, e.EmpHol, SUBSTRING(e.EmpFName, 1, 1) + LOWER(SUBSTRING(e.EmpFName, 2, LEN(e.EmpFName))) + ' ' + SUBSTRING(e.EmpLName, 1, 1) + LOWER(SUBSTRING(e.EmpLName, 2, LEN(e.EmpLName))) HAVING 1=1 ), cte2 AS ( SELECT e.EmployeeID, SUM(ISNULL(TEHours,0)) as HoursWorked FROM TimeEntry t JOIN Employee e on e.EmployeeID = t.EmployeeID WHERE t.ProjectID NOT LIKE '40%' AND t.CreatedOn > '{0}' AND t.CreatedOn < '{1}' GROUP BY e.EmployeeID, SUBSTRING(e.EmpFName, 1, 1) + LOWER(SUBSTRING(e.EmpFName, 2, LEN(e.EmpFName))) + ' ' + SUBSTRING(e.EmpLName, 1, 1) + LOWER(SUBSTRING(e.EmpLName, 2, LEN(e.EmpLName))) HAVING 1=1 ) SELECT c.*, c2.HoursWorked FROM cte c JOIN cte2 c2 ON c2.EmployeeID = c.EmployeeID", start, end);
        
        this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(query);
        this.ASPxGridView1.DataBind();
    }

}
