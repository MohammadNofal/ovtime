﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DXOmniVistaTimeEngine;
using System.Web.Security;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using DevExpress.Web.Data;
using DevExpress.Web;
using System.Web.UI;

public partial class HolidayCalendar : System.Web.UI.Page
{
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        DataTable table = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["HolidayCalendar"], string.Empty);

        DataColumn[] columns = new DataColumn[2];
        columns[0] = table.Columns["HolidayName"];
        columns[1] = table.Columns["HolidayDate"];

        table.PrimaryKey = columns;

        if (Session["GridDataHoliday"] == null)
        {
            Session["GridDataHoliday"] = table;
        }

        this.ASPxGridView1.DataSource = Session["GridDataHoliday"];
        //this.ASPxGridView1.SettingsPager.Mode = DevExpress.Web.GridViewPagerMode.ShowAllRecords; // this disables pagination, was taking too long if second page clicked
        this.ASPxGridView1.DataBind();

        

        NavBarDataBind();
    }

    protected void LeftPane_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
        //txtBox.Value = "records may need refresh";
        //NavBarDataBind();

    }

    private void NavBarDataBind()
    {
        List<string> fileName = new List<string>();
        List<string> filePath = new List<string>();

        fileName.Add("Holiday Calendar");
        filePath.Add("HolidayCalendar.aspx");

        var fileNameArray = fileName.ToArray();
        var filePathArray = filePath.ToArray();

        string currentGroup = "Grids";
        DevExpress.Web.NavBarGroup gr = new DevExpress.Web.NavBarGroup();
        gr.Text = currentGroup;
        gr.Name = currentGroup;
        navbar.Groups.Add(gr);

        for (int i = 0; i < fileNameArray.Length; i++)
        {
            string gr1 = currentGroup;
            DevExpress.Web.NavBarGroup navBarGr = navbar.Groups.FindByName(gr1);
            if (navBarGr != null)
            {
                DevExpress.Web.NavBarItem it = new DevExpress.Web.NavBarItem();

                string result = "<a href=\"" + filePathArray[i] + "\"><div class='Content'><div class='LeftPanel'><div class='Title'> " + fileNameArray[i] + "</div></a>";

                it.Text = result;

                navbar.Groups[navBarGr.Index].Items.Add(it);
            }
        }
    }

    public class Project
    {
        public string HolidayName { get; set; }
        public string ProjectName { get; set; }
    }

    protected void ASPxGridView1_CustomColumnSort(object sender, DevExpress.Web.CustomColumnSortEventArgs e)
    {

    }

    protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
    {

    }

    protected void ASPxGridView1_DataBound(object sender, EventArgs e)
    {

    }

    protected void ASPxGridView1_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ((DataTable)Session["GridDataHoliday"]).Rows.Add(new object[] { e.NewValues["HolidayName"], e.NewValues["HolidayDate"] });
        e.Cancel = true;
        ASPxGridView1.CancelEdit();
        ASPxGridView1.DataBind();

        String query = String.Format("INSERT INTO OMV_HolidayCalendar VALUES ('{0}', '{1}')", e.NewValues["HolidayName"], e.NewValues["HolidayDate"]);
        int success = DataAccess.ExecuteSqlStatement(query, null);
        if (success <= 0) throw new Exception("Error. Row(s) have not been inserted to database.");
    }

    protected void ASPxGridView1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        string newName = e.NewValues["HolidayName"].ToString();
        string newDate = e.NewValues["HolidayDate"].ToString();
        string oldName = e.OldValues["HolidayName"].ToString();
        string oldDate = e.OldValues["HolidayDate"].ToString();

        object[] eventArgs = new object[2] { e.Keys["HolidayName"], e.Keys["HolidayDate"] };
        DataRow row = ((DataTable)Session["GridDataHoliday"]).Rows.Find(eventArgs);
        row["HolidayName"] = newName;
        row["HolidayDate"] = newDate;
        e.Cancel = true;
        ASPxGridView1.CancelEdit();
        ASPxGridView1.DataBind();

        String query = String.Format("UPDATE o SET HolidayName = '{0}', HolidayDate = '{1}' FROM OMV_HolidayCalendar o  WHERE HolidayName = '{2}' AND HolidayDate = '{3}'", newName, newDate, oldName, oldDate);
        int success = DataAccess.ExecuteSqlStatement(query, null);
        if (success <= 0) throw new Exception("Error. Row update(s) have not been saved to database.");
    }

    protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        object[] eventArgs = new object[2] { e.Keys["HolidayName"], e.Keys["HolidayDate"] };
        DataRow row = ((DataTable)Session["GridDataHoliday"]).Rows.Find(eventArgs);
        ((DataTable)Session["GridDataHoliday"]).Rows.Remove(row);
        e.Cancel = true;
        ASPxGridView1.DataBind();

        String query = String.Format("DELETE FROM OMV_HolidayCalendar WHERE HolidayName = '{0}' AND HolidayDate = '{1}'", e.Values["HolidayName"], e.Values["HolidayDate"]);
        int success = DataAccess.ExecuteSqlStatement(query, null);
        if (success <= 0) throw new Exception("Error. Row(s) have not been deleted from database.");
    }

    protected void ASPxGridView1_BeforeColumnSortingGrouping(object sender, DevExpress.Web.ASPxGridViewBeforeColumnGroupingSortingEventArgs e)
    {
        this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["HolidayCalendar"], string.Empty);
        this.ASPxGridView1.DataBind();
    }

    protected void txtDateTime_DateChanged(object sender, EventArgs e)
    {
        //string start = this.StartDate.Value.ToString();
        //string end = this.EndDate.Value.ToString();
        //string query = String.Format(";WITH cte AS ( SELECT e.EmployeeID, SUBSTRING(e.EmpFName, 1, 1) + LOWER(SUBSTRING(e.EmpFName, 2, LEN(e.EmpFName))) + ' ' + SUBSTRING(e.EmpLName, 1, 1) + LOWER(SUBSTRING(e.EmpLName, 2, LEN(e.EmpLName))) as EmployeeName, ISNULL(e.EmpVac,0) * 8 AS Vacation, ISNULL(e.EmpSick,0) * 8 AS Sick, SUM(case when ActivityID = 'GEN:VAC' THEN ISNULL(TEHours,0) ELSE 0 END) AS UsedVacation, SUM(case when ActivityID = 'GEN:SICK' THEN ISNULL(TEHours,0) ELSE 0 END) AS UsedSick FROM TimeEntry t JOIN Employee e on e.EmployeeID = t.EmployeeID WHERE t.HolidayName LIKE '40%' AND t.CreatedOn > '{0}' AND t.CreatedOn < '{1}' GROUP BY e.EmployeeID, e.EmpVac, e.EmpSick, e.EmpHol, SUBSTRING(e.EmpFName, 1, 1) + LOWER(SUBSTRING(e.EmpFName, 2, LEN(e.EmpFName))) + ' ' + SUBSTRING(e.EmpLName, 1, 1) + LOWER(SUBSTRING(e.EmpLName, 2, LEN(e.EmpLName))) HAVING 1=1 ), cte2 AS ( SELECT e.EmployeeID, SUM(ISNULL(TEHours,0)) as HoursWorked FROM TimeEntry t JOIN Employee e on e.EmployeeID = t.EmployeeID WHERE t.HolidayName NOT LIKE '40%' AND t.CreatedOn > '{0}' AND t.CreatedOn < '{1}' GROUP BY e.EmployeeID, SUBSTRING(e.EmpFName, 1, 1) + LOWER(SUBSTRING(e.EmpFName, 2, LEN(e.EmpFName))) + ' ' + SUBSTRING(e.EmpLName, 1, 1) + LOWER(SUBSTRING(e.EmpLName, 2, LEN(e.EmpLName))) HAVING 1=1 ) SELECT c.*, c2.HoursWorked FROM cte c JOIN cte2 c2 ON c2.EmployeeID = c.EmployeeID", start, end);
        
        //this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(query, string.Empty);
        //this.ASPxGridView1.DataBind();
    }

}
