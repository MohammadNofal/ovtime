﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DXOmniVistaTimeEngine;
using System.Web.Security;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using DevExpress.Web;

public partial class Attendance : System.Web.UI.Page
{
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);

        string idParameter = Request.QueryString["id"];
        string projectId = (string.IsNullOrEmpty(idParameter)) ? "0" : idParameter;

        this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["ECGProjectSummary"], "HAVING MasterProjectID = " + projectId);
        this.ASPxGridView1.SettingsPager.Mode = DevExpress.Web.GridViewPagerMode.ShowAllRecords; // this disables pagination, was taking too long if second page clicked
        this.ASPxGridView1.ExpandAll();
        this.ASPxGridView1.DataBind();
        this.ASPxGridView1.Styles.GroupFooter.HorizontalAlign = HorizontalAlign.Left;
        this.ASPxGridView1.Styles.GroupFooter.Font.Size = FontUnit.Small;
        this.ASPxGridView1.Styles.GroupFooter.Font.Italic = true;

        WebRequest request = WebRequest.Create("http://eap-omni-prod01/WCFOVTimeJSonQA/RestServiceImpl.svc/project") as HttpWebRequest;
        WebResponse response = request.GetResponse();

        using (Stream stream = response.GetResponseStream())
        {
            Type serializationTargetType = typeof(List<Project>);
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(serializationTargetType);

            List<Project> jsonDeserialized = (List<Project>)jsonSerializer.ReadObject(stream);

            dt = new DataTable();
            dt.Columns.Add("Group", typeof(string));
            dt.Columns.Add("ProjectId", typeof(string));
            dt.Columns.Add("ProjectName", typeof(string));

            dt.PrimaryKey = new DataColumn[] { dt.Columns["ProjectId"] };

            foreach (Project p in jsonDeserialized)
            {
                DataRow dr = dt.NewRow();
                dr[0] = "Project Name";
                dr[1] = p.ProjectId;
                dr[2] = p.ProjectName;
                dt.Rows.Add(dr);
            }

            if (dt.Rows.Count == 0)
            {
                DataRow dr = dt.NewRow();
                dr[0] = "Project Summary";
                dr[1] = "No Project Data";
                dr[2] = "No Project Data";
                dt.Rows.Add(dr);
            }

        }

        NavBarDataBind();
    }

    protected void LeftPane_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
        //txtBox.Value = "records may need refresh";
        //NavBarDataBind();

    }

    private void NavBarDataBind()
    {
        //create groups:

        string currentGroup = dt.Rows[0].ItemArray[0].ToString();
        DevExpress.Web.NavBarGroup gr = new DevExpress.Web.NavBarGroup();
        gr.Text = currentGroup;
        gr.Name = currentGroup;
        navbar.Groups.Add(gr);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string rowGroup = dt.Rows[i].ItemArray[0].ToString();
            if (rowGroup != currentGroup)
            {
                DevExpress.Web.NavBarGroup rowGr = new DevExpress.Web.NavBarGroup();
                rowGr.Text = rowGroup;
                rowGr.Name = rowGroup;
                navbar.Groups.Add(rowGr);
                currentGroup = rowGroup;
            }
        }

        //fill items:

        for (int j = 0; j < dt.Rows.Count; j++)
        {
            string gr1 = dt.Rows[j].ItemArray[0].ToString();
            DevExpress.Web.NavBarGroup navBarGr = navbar.Groups.FindByName(gr1);
            if (navBarGr != null)
            {
                DevExpress.Web.NavBarItem it = new DevExpress.Web.NavBarItem();

                string result = "<a href=\"?id=" + dt.Rows[j].ItemArray[1].ToString() + "\"><div class='Content'><div class='LeftPanel'><div class='Title'>" + dt.Rows[j].ItemArray[2].ToString() + "</div></a>";

                it.Text = result;

                navbar.Groups[navBarGr.Index].Items.Add(it);
            }
        }
    }

    public class Project
    {
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }
    }

    protected void ASPxGridView1_CustomColumnSort(object sender, DevExpress.Web.CustomColumnSortEventArgs e)
    {

    }

    protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
    {

    }

    protected void ASPxGridView1_DataBound(object sender, EventArgs e)
    {

    }

    protected void ASPxGridView1_BeforeColumnSortingGrouping(object sender, DevExpress.Web.ASPxGridViewBeforeColumnGroupingSortingEventArgs e)
    {
        string idParameter = Request.QueryString["id"];
        string projectId = (string.IsNullOrEmpty(idParameter)) ? "0" : idParameter;
        this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["ECGProjectSummary"], "HAVING MasterProjectID = " + projectId);
        this.ASPxGridView1.DataBind();
    }

}
