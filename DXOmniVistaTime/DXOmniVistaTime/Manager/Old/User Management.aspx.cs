﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Text.RegularExpressions;
using DXOmniVistaTimeEngine;
using System.Web.Security;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using DevExpress.Web.Data;
using DevExpress.Web;
using System.Collections.Specialized;

public partial class Users : System.Web.UI.Page
{
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["GridDataUser"] == null || (this.RoleCombobox.Value == null && this.UserNameComboBox.Value==null))
        {
            SessionUpdate();
        }

        this.LoginGridView.DataSource = (DataTable)Session["GridDataUser"];

        this.RoleCombobox.DataSource = Roles.GetAllRoles();

        this.UserNameComboBox.DataSource = (List<string>)Session["UserNames"];
        DataBind();

    }

    protected void FilterPanel_Callback(object sender, CallbackEventArgsBase e)
    {
        this.RoleCombobox.DataSource = Roles.GetAllRoles();

        this.UserNameComboBox.DataSource = (List<string>)Session["UserNames"];
        DataBind();
    }

    protected void userdetailpanel_Callback(object sender, CallbackEventArgsBase e)
    {
        string role = this.RoleCombobox.Value == null ? String.Empty : this.RoleCombobox.Value.ToString();
        string user = this.UserNameComboBox.Value == null ? String.Empty : this.UserNameComboBox.Value.ToString();
        SessionUpdate();
        if (role != String.Empty)
        {
            DataRow[] row = ((DataTable)Session["GridDataUser"]).Select("Role = '" + role + "'");
            if (row.Count() != 0)
                Session["GridDataUser"] = row.CopyToDataTable();
            else
                Session["GridDataUser"] = null;
        }
        if (user != String.Empty && Session["GridDataUser"] != null)
        {
            DataRow[] row = ((DataTable)Session["GridDataUser"]).Select("UserName = '" + user + "'");
            if (row.Count() != 0)
                Session["GridDataUser"] = row.CopyToDataTable();
            else
                Session["GridDataUser"] = null;
        }

        if (Session["GridDataUser"] != null)
        {
            DataTable _dt;
            _dt = (DataTable)Session["GridDataUser"];
            DataColumn[] columns = new DataColumn[1];
            columns[0] = _dt.Columns["UserId"];

            _dt.PrimaryKey = columns;
            Session["GridDataUser"] = _dt;
        }
        this.LoginGridView.DataSource = (DataTable)Session["GridDataUser"];
        DataBind();
    }
    
    protected void LoginGridView_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (this.LoginGridView.IsNewRowEditing && e.Column.FieldName == "UserName")
        {
            ASPxTextBox _UserName = (ASPxTextBox)e.Editor;
            _UserName.ReadOnly = false;
        }
        if (e.Column.FieldName == "Role")
        {
            ASPxComboBox _role = (ASPxComboBox)e.Editor;
            _role.DataSource = Roles.GetAllRoles();
            _role.DataBind();
        }
    }

    protected void LoginGridView_RowInserting(object sender, ASPxDataInsertingEventArgs e)
    {
        string UserNameNew = e.NewValues["UserName"].ToString();
        string Email = e.NewValues["Email"].ToString();
        string Password = e.NewValues["Password"].ToString();
        string role = e.NewValues["Role"] == null ? String.Empty : e.NewValues["Role"].ToString();
        //bool valid = true;
        //if (((List<string>)Session["UserNames"]).Contains(UserNameNew))
        //{
        //    this.LoginGridView.JSProperties["cpInvalidUserName"] = true;
        //    valid = false;
        //}

        //if (!Emailvalidation(Email))
        //{
        //    valid = false;
        //    this.LoginGridView.JSProperties["cpInvalidEmail"] = true;
        //}

        //if (valid)
        //{
        Membership.CreateUser(UserNameNew, Password, Email);
        if (role!=String.Empty)
            Roles.AddUsersToRole(new string[] { UserNameNew }, role);
        SessionUpdate();

        this.LoginGridView.DataSource = (DataTable)Session["GridDataUser"];
        this.LoginGridView.DataBind();
        //}
        e.Cancel = true;
        this.LoginGridView.CancelEdit();
    }

    protected void LoginGridView_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        string UserId = e.Keys["UserId"].ToString();
        string UserNameNew = e.NewValues["UserName"].ToString();
        string UserNameOld = e.OldValues["UserName"].ToString();
        string Email = e.NewValues["Email"].ToString();
        string EmailOld = e.OldValues["Email"].ToString();
        //string CreateDate = e.NewValues["CreateDate"].ToString();
        //string LastLoginDate = e.NewValues["LastLoginDate"].ToString();
        //string LastActivityDate = e.NewValues["LastActivityDate"].ToString();
        string IsLoggedIn = e.NewValues["IsLoggedIn"].ToString();
        string IsLockedOut = e.NewValues["IsLockedOut"].ToString();
        string IsLockedOutOld = e.OldValues["IsLockedOut"].ToString();
        string Password = e.NewValues["Password"].ToString();
        string role_new = e.NewValues["Role"] == null ? String.Empty : e.NewValues["Role"].ToString();
        string role_old=e.OldValues["Role"]==null?String.Empty:e.OldValues["Role"].ToString();
        MembershipUser user = Membership.GetUser(UserNameOld);

        if (IsLockedOut != IsLockedOutOld && IsLockedOut == "False")
        {
            user.UnlockUser();
            Membership.UpdateUser(user);
        }
        if (Email != EmailOld)
        {
            user.Email = Email;
            Membership.UpdateUser(user);
        }
        if (IsLockedOut != IsLockedOutOld && IsLockedOut == "True")
        {
            string query = String.Format("UPDATE m SET IsLockedOut = 1 FROM aspnetdb.dbo.aspnet_Membership m JOIN aspnetdb.dbo.aspnet_Users u ON m.UserId = u.UserId WHERE u.UserName = '{0}'", UserNameOld);
            int success = DataAccess.ExecuteSqlStatement(query, null);
            if (success <= 0) throw new Exception("Error. User was not locked out.");
        }
        if (Password != "**********")
        {
            string Error_Message = String.Empty;
            string _newp = user.ResetPassword();
            user.ChangePassword(_newp, Password);
        }
        if (role_new != role_old)
        {
            if (role_new != String.Empty)
            {
                Roles.AddUsersToRole(new string[] { user.UserName }, role_new);
            }
            if (role_old != String.Empty)
            {
                Roles.RemoveUserFromRole(user.UserName, role_old);
            }
        }
        object[] eventArgs = new object[1] { e.Keys["UserId"] };
        DataRow row = ((DataTable)Session["GridDataUser"]).Rows.Find(eventArgs);
        row["UserName"] = UserNameNew;
        row["Email"] = Email;
        //row["CreateDate"] = CreateDate;
        //row["LastLoginDate"] = LastLoginDate;
        //row["LastActivityDate"] = LastActivityDate;
        row["IsLoggedIn"] = IsLoggedIn;
        row["IsLockedOut"] = IsLockedOut;
        if (role_new != String.Empty)
            row["Role"] = role_new;
        e.Cancel = true;
        LoginGridView.CancelEdit();
    }

    protected void LoginGridView_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        bool success = Membership.DeleteUser(e.Values["UserName"].ToString());
        if (!success) throw new Exception("Error. Row(s) have not been deleted from database.");
        SessionUpdate();
        this.LoginGridView.DataSource = (DataTable)Session["GridDataUser"];
        this.LoginGridView.DataBind();
        e.Cancel = true;
        this.LoginGridView.CancelEdit();
    }

    protected void LoginGridView_RowValidating(object sender, ASPxDataValidationEventArgs e)
    {
        string UserNameNew = e.NewValues["UserName"].ToString();
        string Email = e.NewValues["Email"].ToString();
        string Password = e.NewValues["Password"].ToString();


        if (((List<string>)Session["UserNames"]).Contains(UserNameNew) &&e.IsNewRow==true)//don't check UserName when updating since it's read only 
        {
            e.Errors[LoginGridView.Columns["UserName"]] = "UserName already Exists";
        }
        if (Password.Length < 6)
        {
            e.Errors[LoginGridView.Columns["Password"]] = "Password minimal length is 6";
        }
        if (!Emailvalidation(Email))
        {
            e.Errors[LoginGridView.Columns["Email"]] = "Email format invalid";
        }
        if (string.IsNullOrEmpty(e.RowError) && e.Errors.Count > 0) e.RowError = "Please, correct all errors.";
    }

    protected void LoginGridView_BeforeColumnSortingGrouping(object sender, DevExpress.Web.ASPxGridViewBeforeColumnGroupingSortingEventArgs e)
    {
        DataTable table = new DataTable();

        table.Columns.Add("UserName", Type.GetType("System.String"));
        table.Columns.Add("Email", Type.GetType("System.String"));
        table.Columns.Add("CreateDate", Type.GetType("System.DateTime"));
        table.Columns.Add("LastLoginDate", Type.GetType("System.DateTime"));
        table.Columns.Add("LastActivityDate", Type.GetType("System.DateTime"));
        table.Columns.Add("IsLoggedIn", Type.GetType("System.Boolean"));
        table.Columns.Add("IsLockedOut", Type.GetType("System.Boolean"));
        table.Columns.Add("UserId", Type.GetType("System.String"));

        MembershipUserCollection muc = Membership.GetAllUsers();
        foreach (MembershipUser user in muc)
        {
            DataRow dr = table.NewRow();
            dr["UserName"] = user.UserName;
            dr["Email"] = user.Email;
            dr["CreateDate"] = user.CreationDate;
            dr["LastLoginDate"] = user.LastLoginDate;
            dr["LastActivityDate"] = user.LastActivityDate;
            dr["IsLoggedIn"] = user.IsOnline;
            dr["IsLockedOut"] = user.IsLockedOut;
            dr["UserId"] = user.ProviderUserKey;

            table.Rows.Add(dr);
        }

        DataColumn[] columns = new DataColumn[1];
        columns[0] = table.Columns["UserId"];

        table.PrimaryKey = columns;

        Session["GridDataUser"] = table;

        this.LoginGridView.DataSource = Session["GridDataUser"];
        this.LoginGridView.DataBind();
    }

    private void SessionUpdate()
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);

        DataTable table = new DataTable();

        table.Columns.Add("UserName", Type.GetType("System.String"));
        table.Columns.Add("Email", Type.GetType("System.String"));
        table.Columns.Add("CreateDate", Type.GetType("System.DateTime"));
        table.Columns.Add("LastLoginDate", Type.GetType("System.DateTime"));
        table.Columns.Add("LastActivityDate", Type.GetType("System.DateTime"));
        table.Columns.Add("IsLoggedIn", Type.GetType("System.Boolean"));
        table.Columns.Add("IsLockedOut", Type.GetType("System.Boolean"));
        table.Columns.Add("UserId", Type.GetType("System.String"));
        table.Columns.Add("Password", Type.GetType("System.String"));
        table.Columns.Add("Role", Type.GetType("System.String"));

        List<string> Username = new List<string>();
        MembershipUserCollection muc = Membership.GetAllUsers();
        foreach (MembershipUser user in muc)
        {
            DataRow dr = table.NewRow();
            dr["UserName"] = user.UserName;
            dr["Email"] = user.Email;
            dr["CreateDate"] = user.CreationDate;
            dr["LastLoginDate"] = user.LastLoginDate;
            dr["LastActivityDate"] = user.LastActivityDate;
            dr["IsLoggedIn"] = user.IsOnline;
            dr["IsLockedOut"] = user.IsLockedOut;
            dr["UserId"] = user.ProviderUserKey;
            dr["Password"] = "**********";
            dr["Role"] = String.Join(",",Roles.GetRolesForUser(user.UserName));
            table.Rows.Add(dr);
            Username.Add(user.UserName);
        }

        DataColumn[] columns = new DataColumn[1];
        columns[0] = table.Columns["UserId"];

        table.PrimaryKey = columns;

        Session["GridDataUser"] = table;
        Session["UserNames"] = Username;
    }

    private bool Emailvalidation(string _email)
    {
        Regex regex=new Regex(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))\z");
        Match mathc=regex.Match(_email);
        if (mathc.Success)
            return true;
        else
            return false;
    }
}
