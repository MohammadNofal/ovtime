﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_Roles : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        RolesData();

    }
    protected void addRole(object sender, EventArgs e)
    {
        if (!Roles.IsUserInRole(Membership.GetUser(User.Identity.Name).UserName, "Administrator"))
        {
            this.roleName.Visible = false;
            this.submitRole.Visible = false;
        }

        string role = this.roleName.Text;
        if(!Roles.RoleExists(role)){
            Roles.CreateRole(role);
        }
       

    }
    protected void RolesData()
    {
      DataTable dt = new DataTable();
      dt.Columns.Add("RoleName");
      string[] roles = Roles.GetAllRoles();

        foreach(var role in roles){
            DataRow dr = dt.NewRow();
            dr["RoleName"] = role;
            dt.Rows.Add(dr);
        }

        this.RolesGrid.DataSource = dt;
       
    }


    protected void RolesGrid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {

    }
    protected void RolesGrid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        var role = e.Keys["RoleName"].ToString();
        if (Roles.RoleExists(role))
        {
            Roles.DeleteRole(role);
        }
        

    }
}