﻿using DevExpress.Web;
using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxTreeList;
using System.Web.Security;

public partial class Manager_Projects : System.Web.UI.Page
{
    DataTable Projects;
    DataTable Activities;
    DataTable Employees;
    

    protected void Page_Load(object sender, EventArgs e)
    {
            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            GetProjects();
           
            if (Session["Employees"] != null)
            {
                this.ASPxGridView1.DataSource = (DataTable)Session["Employees"];


            }
           // GetActivitiesSession();

        
       this.ProjectTree.DataSource = Session["Projects"];
        
    }
    private void GetProjects()
    {
        string sql = "Select * From Project ";
        DataTable Projects = DataAccess.GetDataTableBySqlSyntax(sql, "");
        Session["Projects"] = Projects;
        this.ProjectTree.DataSource = Projects;
        DataBind();

    }
    private void SearchTree(string searchQuery)
    {
        DataTable results = new DataTable();
        GetProjects();
        DataTable projects = (DataTable)Session["Projects"];

        var test = projects.Select("ProjectID like '%" + searchQuery + "%' or ProjectName like '%" + searchQuery + "%' ");
        if (test.Count() > 0)
        {
            this.ProjectTree.DataSource = projects.Select("ProjectID like '%" + searchQuery + "%' or ProjectName like '%" + searchQuery + "%' ").CopyToDataTable();
            Session["Projects"] = projects.Select("ProjectID like '%" + searchQuery + "%' or ProjectName like '%" + searchQuery + "%' ").CopyToDataTable();
        }
        else
        {
            this.ProjectTree.DataSource = test;
            this.ProjectTree.DataBind();
        }

       
        DataBind();
    }
    private void RefreshTree()
    {              
        GetProjects();
    }
    protected void ProjectTree_CustomCallback(object sender, DevExpress.Web.ASPxTreeList.TreeListCustomCallbackEventArgs e)
    {
        if (e.Argument == "Expand")
        {

            this.ProjectTree.CollapseAll();
            this.ProjectTree.ExpandToLevel(4);
        }
        else if (e.Argument == "Collapse")
        {

            this.ProjectTree.CollapseAll();
            // this.ProjectTree.ExpandToLevel(0);
        }
        else if (e.Argument == "Search")
        {
            SearchTree(this.searchTxt.Text);
        }
        else if (e.Argument == "Refresh")
        {
            RefreshTree();
        }
        else
        {
            string ProjectID = e.Argument;
            GetProjectBasicData(ProjectID);
           
        }

    }
    protected void grid_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (!ASPxGridView1.IsEditing) return;
        //   if (e.KeyValue == DBNull.Value || e.KeyValue == null) return;
        e.Editor.ReadOnly = false;
        if (e.Column.FieldName == "EmployeeID")
        {
            ASPxComboBox combo = e.Editor as ASPxComboBox;
            combo.Enabled = true;
            // List<string> employess = GetEmployees();
            combo.DataSource = GetEmployees();
            combo.DataBind();
          //  combo.AllowNull = true;

        }
        if (e.Column.FieldName == "ControlID")
        {
            ASPxComboBox combo = e.Editor as ASPxComboBox;
            combo.Enabled = true;
            combo.DataSource = GetActivities(this.EditProjectID.Text);
            combo.DataBind();
         //   combo.AllowNull = true;
        }



    }
    
    List<string> GetCities(string country)
    {
        return null;
    }

    public String GetText(Object container)
    {
        GridViewDataItemTemplateContainer cont = container as GridViewDataItemTemplateContainer;

        if (cont.DataItem != null)
        {
            try
            {
                Object rel = DataBinder.Eval(cont.DataItem, "EmployeeID");      // rel isn't null or empty

                return rel.ToString();
            }
            catch (Exception e)
            {

            }
        }
        return "";
    }
    private DataTable GetEmployees()
    {
        string sql = "Select * from Employee ";
        DataTable employees = DataAccess.GetDataTableBySqlSyntax(sql, "");
        return employees;
    }
    protected void ASPxCallbackPanel2_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {

        
    }
    ////get activities
    private DataTable GetActivities(string ProjectID)
    {
        string sql = "Select  EmployeeControl.ControlID   from EmployeeControlExt  Left join EmployeeControl on EmployeeControl.EmployeeControlID = EmployeeControlExt.EmployeeControlID where ProjectID = '" + ProjectID + "'  and EmployeeControl.ControlType=2";
        DataTable activities = DataAccess.GetDataTableBySqlSyntax(sql, "");
        return activities;
    }
    
    private void GetAllActivities()
    {
        string sql = "Select * From Activity ";
        Activities = DataAccess.GetDataTableBySqlSyntax(sql, "");
        this.lbAvailable.DataSource = Activities;
        DataBind();
    }
    
     
     
    private void GetProjectBasicData(string ProjectID)
    {
        string query = string.Format("Select * from Project where ProjectID = '{0}'", ProjectID);
        DataRow Project = DataAccess.GetDataTableBySqlSyntax(query, "").Rows[0];
        this.EditProjectID.Text = Project["ProjectID"].ToString();
        this.EditProjectCode.Text = Project["ProjectCode"].ToString();
        this.EditProjectPhase.Text = Project["ProjectPhase"].ToString();
        this.EditProjectName.Text = Project["ProjectName"].ToString();
        this.EditProjectStreet.Text = Project["ProjectStreet"].ToString();
        this.EditProjectCity.Text = Project["ProjectCity"].ToString();
        this.EditProjectState.Text = Project["ProjectState"].ToString();
        this.EditProjectZip.Text = Project["ProjectZip"].ToString();
        this.EditProjectStartDate.Date = Project["ProjectStartDate"].ToString() == String.Empty ? DateTime.Now : DateTime.Parse(Project["ProjectStartDate"].ToString()); 
        this.EditProjectStatus.Text = Project["ProjectStatus"].ToString();
        this.EditFSID.Text = Project["FSID"].ToString();
       
        this.EditEmployeeID.Text = Project["EmployeeID"].ToString();
        // head of the edit panel
        this.ProjectNameHead.Text = Project["ProjectID"].ToString();
        this.ProjectNameHead.Value = Project["ProjectID"].ToString();
        this.EditClientID.Text = Project["ClientID"].ToString();
        this.EditProjectConType.Text = Project["ProjectConType"].ToString();
        this.DropDownEdit.Text = Project["ParentProjectID"].ToString();
        this.EditProjectOther1.Text = Project["ProjectOther1"].ToString();
        this.EditProjectOther2.Text = Project["ProjectOther2"].ToString();
        this.EditProjectOther3.Text = Project["ProjectOther3"].ToString();
        this.EditProjectOther4.Text = Project["ProjectOther4"].ToString();
        

    }
    protected void ASPxGridView1_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        string ProjectId = this.EditProjectID.Text;
        e.Cancel = true;


        ASPxComboBox cmbStatus = (sender as ASPxGridView).FindEditRowCellTemplateControl(
                    (sender as ASPxGridView).Columns["EmployeeID"] as GridViewDataColumn,
                    "newEmp") as ASPxComboBox;



        string EmployeeID = cmbStatus.Value.ToString();
        string ActivityID = e.NewValues["ControlID"].ToString();
        string EmpBillRate = e.NewValues["EmpBillRate"] == null ? "0" : e.NewValues["EmpBillRate"].ToString();
        string EmpCostRate = e.NewValues["EmpCostRate"] == null ? "0" : e.NewValues["EmpCostRate"].ToString();
        string EmpOTCostRate = e.NewValues["EmpOTCostRate"] == null ? "0" : e.NewValues["EmpOTCostRate"].ToString();
        string EmpOTBillRate = e.NewValues["EmpOTBillRate"] == null ? "0" : e.NewValues["EmpOTBillRate"].ToString();
        string chk = "Select * from EmployeeControl where EmployeeID = '" + EmployeeID
            + "' and ControlType = 1   and ControlID = '" + this.EditProjectID.Text + "'";
        if (DataAccess.GetDataTableBySqlSyntax(chk, "").Rows.Count < 1)
        {
            string insert = "insert into EmployeeControl (EmployeeID,ControlType,ControlID,EmployeeControlID) values ('" +
                EmployeeID + "','1','" + this.EditProjectID.Text + "',NEWID())";
            DataAccess.ExecuteSqlStatement(insert, "");
        }

        //   string query = "insert into EmployeeControl (EmployeeID,ControlType,ControlID,EmployeeControlID,EmpBillRate,EmpCostRate,EmpOTCostRate,EmpOTBillRate) values ('" + EmployeeID + "','2','" + ActivityID + "',NEWID()," + EmpBillRate + "," + EmpCostRate + "," + EmpOTCostRate + "," + EmpOTBillRate + ")";
        //   DataAccess.ExecuteSqlStatement(query, "");

        ////check the same activity with the same employee if exist or not 
        string chk1 = "select * from EmployeeControl where EmployeeID = '" + EmployeeID + "' and ControlID = '" + ActivityID + "' and ControlType=2";
        DataTable dtchk1 = DataAccess.GetDataTableBySqlSyntax(chk1, "");
        if (dtchk1.Rows.Count > 1)
        {
            string EmployeeCOntrolID = dtchk1.Rows[0]["EmployeeControlID"].ToString();
            chk1 = "select * from EmplyeeControlExt where EmployeeControlID = '" + EmployeeCOntrolID + "'";
            if (DataAccess.GetDataTableBySqlSyntax(chk1, "").Rows.Count > 0)
            {
                return;
            }
        }


        string sql = "Insert INTO EmployeeControlExt(ProjectID,EmployeeControlID) values ( ";
        sql += string.Format("'{0}',", ProjectId);
        sql += "NEWID())";
        sql += "";
        DataAccess.ExecuteSqlStatement(sql, "");
        string getLastID = "SELECT top 1 EmployeeControlID FROM EmployeeControlExt ORDER BY ID DESC";
        string lastID = DataAccess.GetDataTableBySqlSyntax(getLastID, "").Rows[0]["EmployeeControlID"].ToString();
        string sql1 = "insert into EmployeeControl (EmployeeID,ControlType,ControlID,EmployeeControlID,EmpBillRate,EmpCostRate,EmpOTCostRate,EmpOTBillRate) values ('" + EmployeeID + "','2','" + ActivityID + "','" + lastID + "'," + EmpBillRate + "," + EmpCostRate + "," + EmpOTCostRate + "," + EmpOTBillRate + ")";
        //  string sql1 = "Insert Into EmployeeControl(EmployeeControlID,ControlType,ControlID) Values ('" +
        //    lastID + "',2,'" + ActivityID + "')";
        DataAccess.ExecuteSqlStatement(sql1, "");
        this.ASPxGridView1.CancelEdit();

        GetEmployeeControl(this.EditProjectID.Text.ToString());
    }
    protected void ASPxGridView1_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType == GridViewRowType.Data && e.VisibleIndex % 2 == 0)
        {
            e.Row.BackColor = System.Drawing.Color.Azure;
        }
        if (e.RowType == GridViewRowType.Detail && e.VisibleIndex % 2 == 0)
        {
            e.Row.BackColor = System.Drawing.Color.Azure;
        }
    }
    protected void newEmp_Init(object sender, EventArgs e)
    {
        ASPxComboBox combo = sender as ASPxComboBox;
        combo.Enabled = true;
        // List<string> employess = GetEmployees();
        combo.DataSource = GetEmployees();
        //  DataBind();
        combo.AllowNull = false;

    }
    private DataTable GetProjectsV2()
    {
        
        string sql = "Select * From Project ";
        DataTable Projects = DataAccess.GetDataTableBySqlSyntax(sql, "");

        return Projects;
    }
    protected void TreeList_Init(object sender, EventArgs e)
    {
        ASPxTreeList tree = (ASPxTreeList)sender;
        tree.DataSource = GetProjectsV2();
        tree.DataBind();
        tree.CollapseAll();
    }
    private void GetEmployeeControl(string ProjectID)
    {
        string sql1 = "Select  EmployeeControl.ControlID  From EmployeeControl  Left Join EmployeeControlExt  on EmployeeControl.EmployeeControlID = EmployeeControlExt.EmployeeControlID  where  ProjectId =  '" + ProjectID + "' and EmployeeControl.EmployeeID is null";
        DataTable Activities = DataAccess.GetDataTableBySqlSyntax(sql1, "");
        DataTable dt = new DataTable();
        if (dt.Rows.Count > 0)
        {
           
            foreach (DataRow dr in Activities.Rows)
            {
                string sql2 = "select *,Employee.* from EmployeeControl left join Employee on EmployeeControl.EmployeeID = Employee.EmployeeID where ControlID = '" + dr["ControlID"].ToString() + "' and EmployeeControl.EmployeeID is not null and EmployeeControl.ControlType=2";
                dt.Merge(DataAccess.GetDataTableBySqlSyntax(sql2, ""));
            }
        }
        else
        {
            string sql = "Select   EmployeeControl.EmployeeControlID,Employee.EmployeeID ,Employee.* from Employee inner join EmployeeControl on EmployeeControl.EmployeeID =  Employee.EmployeeID where ControlType=1 and ControlID = '" + ProjectID + "'";
            dt = DataAccess.GetDataTableBySqlSyntax(sql, "");
            DataColumn dc = new DataColumn("ControlID", typeof(System.String));
            dc.DefaultValue = "Empty";
            dt.Columns.Add(dc);
        }
      
        //string sql = "Select   EmployeeControl.*,EmployeeControl.ControlID ,Employee.*  From EmployeeControl  Left Join Employee  on EmployeeControl.EmployeeID = Employee.EmployeeID  where  ControlID =  '" + ProjectID + "' ";
        // string sql = "Select Employee.* , EmployeeControl.*,EmployeeControlExt.* From EmployeeControl Left Join EmployeeControlExt  on ControlID = ProjectID  Left Join Employee  on EmployeeControl.EmployeeID = Employee.EmployeeID where  ControlID = '" + ProjectID + "' ";
        // string sql = "Select * From EmployeeControl where ControlType = 1 and ControlID = '" + ProjectID + "' ";
        // DataTable Employees = DataAccess.GetDataTableBySqlSyntax(sql, "");

        Session["Employees"] = dt;

        this.ASPxGridView1.DataSource = dt;
        
        DataBind();

    }
    protected void searchBtn_Click(object sender, EventArgs e)
    {
        TreeListNodeIterator iterator = new TreeListNodeIterator(this.ProjectTree.RootNode);
        while (iterator.Current != null)
        {
           // CheckNode(iterator.Current);
            iterator.GetNext();
        }
        
    }
    protected string GetCellText(TreeListDataCellTemplateContainer container)
    {
        string searchText = searchTxt.Text;
        string cell_text = container.Text;
        if (searchText.Length > cell_text.Length)
            return cell_text;
        if (searchText != "")
        {
            string cell_text_lower = cell_text.ToLower();
            string serchText_lower = searchText.ToLower();
            int start_pos = cell_text_lower.IndexOf(serchText_lower);
            int span_length = ("<span class='highlight'>").Length;
            if (start_pos >= 0)
            {
                cell_text = cell_text.Insert(start_pos, "<span class='highlight'>");
                cell_text = cell_text.Insert(start_pos + span_length + serchText_lower.Length, "</span>");
            }
        }
        return cell_text;
    }
    private void CheckNode(TreeListNode node) {
            string s_text = searchTxt.Text.ToLower();
            object node_value = node.GetValue("ProjectID");
            if (node_value == null)
                return;
            if (node_value.ToString().ToLower().IndexOf(s_text) >= 0)
            {               
                node.MakeVisible();
            }

        }
    private void GetAllActivities(string ProjectID)
    {
        string sql = "Select * From Activity ";
        Activities = DataAccess.GetDataTableBySqlSyntax(sql, "");
        string sql2 = "Select  EmployeeControl.ControlID as ActivityID from EmployeeControlExt  Left join EmployeeControl on EmployeeControl.EmployeeControlID = EmployeeControlExt.EmployeeControlID where ProjectID = '" + ProjectID + "'  and EmployeeControl.ControlType=2";
        // string sql2 = string.Format("Select * from EmployeeControlExt where ProjectID = '{0}'", ProjectID);
        DataTable choosen = DataAccess.GetDataTableBySqlSyntax(sql2, "");

        for (int i = 0; i < choosen.Rows.Count; i++)
        {
            for (int j = 0; j < Activities.Rows.Count; j++)
            {
                if (Activities.Rows[j].RowState != DataRowState.Deleted)
                {
                    if (choosen.Rows[i]["ActivityID"].ToString().Equals(Activities.Rows[j]["ActivityID"].ToString()))
                    {
                        Activities.Rows[j].Delete();
                        break;
                    }
                }
            }
        }

        this.lbChoosen.DataSource = choosen;
        this.lbAvailable.DataSource = Activities;
    }
    private void BindCombo()
    {
        this.managers.DataSource = Roles.GetUsersInRole("Manager");
        MembershipUserCollection users = Membership.GetAllUsers();
        this.EditEmployeeID.DataSource = users;
        this.EditProjectOther1.DataSource = users;
        this.EditProjectOther2.DataSource = users;
        this.EditProjectOther3.DataSource = users;
        this.EditProjectOther4.DataSource = users;
        this.roles.DataSource = Roles.GetAllRoles();
        this.EditClientID.DataSource = DataAccess.GetDataTableBySqlSyntax("Select * from Client", "");
        this.EditProjectStatus.DataSource = DataAccess.GetDataTableBySqlSyntax("Select * from Status", "");
        this.EditProjectConType.DataSource = DataAccess.GetDataTableBySqlSyntax("Select * from ContractType", "");
    }
    protected void ASPxCallbackPanel1_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
        Session["Employees"] = null;
        this.ASPxGridView1.DataSource = null;
        this.lbAvailable.DataSource = null;
        this.ASPxGridView1.DataBind();
        this.lbAvailable.DataBind();
        this.lbChoosen.DataSource = null;
        this.lbChoosen.DataBind();
        this.lbChoosen.Items.Clear();
        this.lbAvailable.Selection.UnselectAll();
        GetAllActivities();
        BindCombo();
        if (e.Parameter == "AddNewProject")
        {
            this.EditProjectID.Enabled = true;
            DataBind();
        }
        else if (e.Parameter.Contains("ProjectID="))
        {
            this.EditProjectID.Enabled = false;
            var ProjectID = e.Parameter.Replace("ProjectID=", "");
            //  ProjectTree.CollapseAll();

            if (IsPostBack)
            {

                GetProjectBasicData(ProjectID);
                GetEmployeeControl(ProjectID);
                DataBind();
            }
            else
            {
                DataBind();
            }


        }
        else if (e.Parameter.Contains("InfoID="))
        {
            //this.EditProjectID.Enabled = false;
            //var ProjectID = e.Parameter.Replace("InfoID=", "");
            ////  ProjectTree.CollapseAll();

            //if (IsPostBack)
            //{

            //    GetProjectBasicData(ProjectID);
            //    GetEmployeeControl(ProjectID);
            //    DataBind();
            //}
            //else
            //{
            //    DataBind();
            //}


        }
        
    }
    [WebMethod]
    [ScriptMethod]
    public static void SaveBasicInfoClick(string project)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        Dictionary<string, object> obj = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(project);
        string sql = "Update Project Set ";
        for (int i = 0; i < obj.Count(); i++)
        {
            if (obj.Keys.ElementAt(i).ToString() == "OldID")
            {
                continue;
            }
            if (i + 1 != obj.Count)
            {
                sql += string.Format("{0} = '{1}',", obj.Keys.ElementAt(i).Replace("Edit", ""), obj.Values.ElementAt(i));
            }
            else
            {
                sql += string.Format("{0} = '{1}'", obj.Keys.ElementAt(i).Replace("Edit", ""), obj.Values.ElementAt(i));
            }
        }
        sql += string.Format(" where ProjectID = '{0}'", obj["OldID"]);
        DataAccess.ExecuteSqlStatement(sql, "");

    }
    [WebMethod]
    [ScriptMethod]
    public static void SaveActivities(string ProjectID, string activities)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        List<string> obj = new JavaScriptSerializer().Deserialize<List<string>>(activities);
        ///here we updating activites
        // string delete = "Delete from EmployeeControlExt where ProjectID='" + ProjectID + "'";
        //  DataAccess.ExecuteSqlStatement(delete, "");
        for (int i = 0; i < obj.Count(); i++)
        {

            ////check if exist
            // string sql2 = "Select * from EmployeeControlExt where ProjectID='" + ProjectID + "' and ActivityID ='" + obj[i] + "'";
            // DataTable dt = DataAccess.GetDataTableBySqlSyntax(sql2, "");
            //  if (dt.Rows.Count > 0)
            //  {
            //      continue;
            //  }

            string sql = "Insert INTO EmployeeControlExt(ProjectID,EmployeeControlID) values ( ";
            sql += string.Format("'{0}',", ProjectID);
            sql += "NEWID())";
            sql += "";
            DataAccess.ExecuteSqlStatement(sql, "");
            string getLastID = "SELECT top 1 EmployeeControlID FROM EmployeeControlExt ORDER BY ID DESC";
            string lastID = DataAccess.GetDataTableBySqlSyntax(getLastID, "").Rows[0]["EmployeeControlID"].ToString();
            string sql1 = "Insert Into EmployeeControl(EmployeeControlID,ControlType,ControlID) Values ('" + lastID + "',2,'" + obj[i] + "')";
            DataAccess.ExecuteSqlStatement(sql1, "");

        }




    }
    [WebMethod]
    [ScriptMethod]
    public static void AddBasicInfoClick(string project)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        Dictionary<string, object> obj = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(project);
        string sql = "Insert Into Project(  ";
        string into = "";
        string values = "";

        for (int i = 0; i < obj.Count(); i++)
        {

            if (i + 1 != obj.Count)
            {
                //into += string.Format("{0} = '{1}',", obj.Keys.ElementAt(i).Replace("New", ""), obj.Values.ElementAt(i));
                into += obj.Keys.ElementAt(i).Replace("New", "") + ",";
                values += string.Format("'{0}',", obj.Values.ElementAt(i));
            }
            else
            {
                // values += string.Format("{0} = '{1}'", obj.Keys.ElementAt(i).Replace("New", ""), obj.Values.ElementAt(i));
                into += obj.Keys.ElementAt(i).Replace("New", "");
                values += string.Format("'{0}'", obj.Values.ElementAt(i));
            }
        }
        sql = string.Format("{0}{1}) values({2})", sql, into, values);
        DataAccess.ExecuteSqlStatement(sql, "");
    }
    #region AsPgrdiView3 Functions
    protected void ASPxCallbackPanel3_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
        bool enableAnimation = true;
        if (ASPxCallbackPanel2.IsCallback)
        {
            // Intentionally pauses server-side processing, 
            // to demonstrate the Loading Panel functionality.
            if (!enableAnimation)
                Thread.Sleep(500);
        }
        if (IsPostBack)
        {
            GetAllActivities();
            DataBind();
        }

    }

    #endregion

    
    protected void ASPxGridView1_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        GridViewDataComboBoxColumn combo = (GridViewDataComboBoxColumn)this.ASPxGridView1.DataColumns["EmployeeID"];


        // List<string> employess = GetEmployees();
        combo.PropertiesComboBox.DataSource = GetEmployees();

        combo.PropertiesComboBox.AllowNull = true;
        DataBind();
    }
    protected void ASPxGridView1_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        // e.Args[0]
        // e.Args = "Test";
    }
     
    
    protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string EmployeeID = e.Values["EmployeeID"].ToString();
        string ActivityID = e.Values["ControlID"].ToString();
        string sql = "Delete  from EmployeeControl where EmployeeID = '" + EmployeeID + "' and ControlID = '" + ActivityID + "' and ControlType=2";
        e.Cancel = true;
        DataAccess.ExecuteSqlStatement(sql, "");
        GetEmployeeControl(this.EditProjectID.Text.ToString());

    }

    protected void ASPxGridView1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        string updateCommand = "Update EmployeeControl Set  EmployeeID = '" + e.NewValues["EmployeeID"] + "' , ControlID = '" + e.NewValues["ControlID"] + "' ";


        updateCommand += string.Format(" where EmployeeControlID ='{0}'", e.Keys["EmployeeControlID"]);
        DataAccess.ExecuteSqlStatement(updateCommand, "");
        GetEmployeeControl(this.EditProjectID.Text.ToString());
        e.Cancel = true;
        this.ASPxGridView1.CancelEdit();
    }
    protected void ProjectTree_NodeInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        e.Cancel = true;
        string ProjectID = e.NewValues["ProjectID"].ToString();
        string ProjectName = e.NewValues["ProjectName"].ToString();
        string ProjectStartDate = e.NewValues["ProjectStartDate"].ToString();
        string ProjectStatus = e.NewValues["ProjectStatus"].ToString();
        string ProjectCode = e.NewValues["ProjectCode"].ToString();
        string ParentProjectID = this.ProjectTree.NewNodeParentKey;
        string sql = string.Format("Insert into Project(ProjectID,ProjectName,ProjectStartDate,ProjectStatus,ClientID,ParentProjectID,ProjectCode) values ( '{0}' ,'{1}','{2}','{3}' ,'{4}' ,'{5}','{6}')", ProjectID, ProjectName, ProjectStartDate, ProjectStatus, ClientID, ParentProjectID, ProjectCode);
        DataAccess.ExecuteSqlStatement(sql, "");
        this.ProjectTree.CancelEdit();
        GetProjects();

    }
    protected void ProjectTree_NodeDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string ProjectID = e.Values["ProjectID"].ToString();
        string sql = string.Format("Delete From Project where ProjectID ='{0}' ", ProjectID); // delete
        DataAccess.ExecuteSqlStatement(sql, "");
        e.Cancel = true;
        GetProjects();
    }
    protected void GetActivitiesSession(string ProjectID = null)
    {

        string sql = "Select * From Activity ";
        Activities = DataAccess.GetDataTableBySqlSyntax(sql, "");
        Session["Activities"] = Activities;

        this.lbAvailable.DataSource = Session["Activities"];


    }
}