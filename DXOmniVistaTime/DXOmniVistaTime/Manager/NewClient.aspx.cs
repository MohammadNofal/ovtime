﻿using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_NewClient : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        LoadData();

    }
    private void LoadData()
    {
        string sql = "Select * From Client order by Client.LastUpdated DESC";
        DataTable tasks = DataAccess.GetDataTableBySqlSyntax(sql, "");
        this.Clients.DataSource = tasks;
        this.Clients.DataBind();
    }
    protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        e.Cancel = true;
        string ClientID = e.Values["ClientID"].ToString().ToString();
        string sql = "Delete from Client where ClientID = '" + ClientID + "'";
        DataAccess.ExecuteSqlStatement(sql, "");
        LoadData();
    }
    protected void ASPxGridView1_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        e.Cancel = true;
        string ClientID = "";
        string ClientEmail = "";
        string ClientLName = "";
        string ClientFName ="";
        string ClientFedID ="";
        string MobileNumber = "";
        string ClientCompany = "";
        string ClientStreet = "";
        

        if (e.NewValues["ClientID"] != null)
        {
            ClientID = e.NewValues["ClientID"].ToString();
        }
        if (e.NewValues["ClientFedID"] != null)
        {
            ClientFedID = e.NewValues["ClientFedID"].ToString();
        }
        if (e.NewValues["ClientEmail"] != null)
        {
            ClientEmail = e.NewValues["ClientEmail"].ToString();
        }
        if (e.NewValues["ClientLName"] != null)
        {
            ClientLName = e.NewValues["ClientLName"].ToString();
        }
        if (e.NewValues["ClientFName"] != null)
        {
            ClientFName = e.NewValues["ClientFName"].ToString();
        }
        if (e.NewValues["MobileNumber"] != null)
        {
            MobileNumber = e.NewValues["MobileNumber"].ToString();
        }
        if (e.NewValues["ClientCompany"] != null)
        {
            ClientCompany = e.NewValues["ClientCompany"].ToString();
        }
        if (e.NewValues["ClientStreet"] != null)
        {
            ClientStreet = e.NewValues["ClientStreet"].ToString();
        }
        string sql = "Insert Into Client (ClientID,ClientFedID,ClientEmail,ClientLName,ClientFName,MobileNumber,ClientCompany,ClientStreet,LastUpdated) Values ('" + ClientID + "' , '" + ClientFedID + "', '" + ClientEmail + "', '" + ClientLName + "', '" + ClientFName + "', '" + MobileNumber + "', '" + ClientCompany + "', '" + ClientStreet + "','GETDATE()')";
        DataAccess.ExecuteSqlStatement(sql, "");
        e.Cancel = true;
        this.Clients.CancelEdit();
        LoadData();
    }
    protected void ASPxGridView1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        e.Cancel = true;
        string ClientID = "";
        string ClientEmail = "";
        string ClientLName = "";
        string ClientFName = "";
        string ClientFedID = "";
        string MobileNumber = "";
        string ClientCompany = "";
        string ClientStreet = "";


        if (e.NewValues["ClientID"] != null)
        {
            ClientID = e.NewValues["ClientID"].ToString();
        }
        if (e.NewValues["ClientFedID"] != null)
        {
            ClientFedID = e.NewValues["ClientFedID"].ToString();
        }
        if (e.NewValues["ClientEmail"] != null)
        {
            ClientEmail = e.NewValues["ClientEmail"].ToString();
        }
        if (e.NewValues["ClientLName"] != null)
        {
            ClientLName = e.NewValues["ClientLName"].ToString();
        }
        if (e.NewValues["ClientFName"] != null)
        {
            ClientFName = e.NewValues["ClientFName"].ToString();
        }
        if (e.NewValues["MobileNumber"] != null)
        {
            MobileNumber = e.NewValues["MobileNumber"].ToString();
        }
        if (e.NewValues["ClientCompany"] != null)
        {
            ClientCompany = e.NewValues["ClientCompany"].ToString();
        }
        if (e.NewValues["ClientStreet"] != null)
        {
            ClientStreet = e.NewValues["ClientStreet"].ToString();
        }
        string ClientID_ = e.OldValues["ClientID"].ToString();
        string sql = "Update  Client  set ClientID ='" + ClientID + "',ClientFedID='" + ClientFedID + "',ClientEmail='" + ClientEmail + "',ClientLName='" + ClientLName + "',ClientFName='" + ClientLName + "',MobileNumber='" + MobileNumber + "',ClientCompany='" + ClientCompany + "',ClientStreet='" + ClientStreet + "' LastUpdated='GETDATE()' where ClientID = '" + ClientID_ + "'";
        DataAccess.ExecuteSqlStatement(sql, "");
        e.Cancel = true;
        this.Clients.CancelEdit();
        LoadData();
    }
    protected void ASPxGridView1_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {
        if (e.ButtonID == "ManageProjects")
        {
            string ClientID = this.Clients.GetRowValues(e.VisibleIndex, "ClientID").ToString();
            DevExpress.Web.ASPxWebControl.RedirectOnCallback("~/Manager/ManageClientsProjects.aspx?ClientID=" + ClientID);
        }
    }

    [WebMethod]
    [ScriptMethod]
    public static string AddClient(string Client,  bool editflag)
    {
        Dictionary<string, object> obj = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(Client);
       

        if (!editflag)
        {
            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            string sql = string.Format("select * from Client where ClientID ='{0}'", obj.Values.ElementAt(0));
            if (DataAccess.GetDataTableBySqlSyntax(sql, "").Rows.Count > 0)
            {
                return "Client already exists.";
            }
          
            sql = "Insert into Client(";
            string into = "";
            string values = " Values (";
            for (int i = 0; i < obj.Count(); i++)
            {
                if (obj.Values.ElementAt(i) == String.Empty )
                {
                    continue;
                }
                into += obj.Keys.ElementAt(i) + ",";
                values += "'" + obj.Values.ElementAt(i) + "',";
            }
            into = into.Substring(0, into.Length - 1) + ",lastUpdated)";
            values = values.Substring(0, values.Length - 1) + ",GETDATE())";

            sql += into + values;
            DataAccess.ExecuteSqlStatement(sql, "");
            return "";
        }
        else
        {
           
            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            string sql = "Update Client set ";

            for (int i = 0; i < obj.Count(); i++)
            {
                if (obj.Values.ElementAt(i) == String.Empty )
                {
                    continue;
                }
                sql += obj.Keys.ElementAt(i) + "=";
                sql += "'" + obj.Values.ElementAt(i) + "',";
            }
            sql = sql.Substring(0, sql.Length - 1);
            sql += " ,lastUpdated = GETDATE() where ClientID = '" + obj.Values.ElementAt(0).ToString() + "'";


            DataAccess.ExecuteSqlStatement(sql, "");
            return "";

        }
    }
    
          [WebMethod]
    [ScriptMethod]
    public static string GetClientInfo(string ClientID)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        string sql = "select * from Client where ClientID ='" + ClientID + "'";
        DataTable user = DataAccess.GetDataTableBySqlSyntax(sql, "");
        
       
        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> row;
        foreach (DataRow dr in user.Rows)
        {
            row = new Dictionary<string, object>();
            foreach (DataColumn col in user.Columns)
            {
                row.Add(col.ColumnName, dr[col]);
            }
            rows.Add(row);
        }
        return new JavaScriptSerializer().Serialize(rows);
    }

}