﻿<%@ Page Title="Users View" Language="C#" AutoEventWireup="true" MasterPageFile="~/Main.master" CodeFile="User Management.aspx.cs" Inherits="Users" %>

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
      <script src="../Content/js/Common.js"></script>
      <script src="../Content/js/Users.js"></script>
     <style>
        .editorText {
                margin-left: 27px;
                margin-right: 35px;
        }
        .editorCaption {
            padding-left:13px;
        }
         .check_true {
             color: green;
             font-size: 20px;
         }
         .check_false {
              color: #b3a6a6;
             font-size: 20px;
         }
    </style>
    <script type="text/javascript">

        function onSubmit(s, e) {
            userdetailpanel.PerformCallback("Filter");
        }

        function FilterPanaelCallback(s, e) {
            //var error = "";
            //if (s.cpInvalidUserName) {
            //    s.cpInvalidUserName = false;
            //    error = error + "UserName already Exists. ";
            //}
            //if (s.cpInvalidEmail) {
            //    s.cpInvalidEmail = false;
            //    error = error + "Invalid Email";
            //}
            //if (error != "") {
            //    alert(error);
            //}
            //else {
            FilterPanel.PerformCallback("Filter");
            //}
        }
        function AddRole(s, e)
        {
           
        }
    </script>
    <%
        string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

        
         %>
    <dx:ASPxCallbackPanel ID="FilterPanel" runat="server" ClientInstanceName="FilterPanel" Width="100%" CssClass="detailPanelMidian" Collapsible="false"  OnCallback="FilterPanel_Callback"  SettingsLoadingPanel-Enabled ="false"  >
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <SettingsLoadingPanel Enabled="False"></SettingsLoadingPanel>

        <Styles>
            <ExpandBar Width="100%" CssClass="bar">
            </ExpandBar>
            <ExpandedExpandBar CssClass="expanded">
            </ExpandedExpandBar>
        </Styles>
        <BorderTop BorderWidth="0px">
        </BorderTop>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                
                <div class="PageTitle">
                   <span> Employees Listing:</span>
                 </div>
                 <br />
                <br />
                
                <dx:ASPxComboBox ID="UserNameComboBox" Caption="EmployeeID" runat="server" AutoPostBack="false" ClientInstanceName="UserNameComboBox"
                        CssClass="editor" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption" ClearButton-Visibility="True" OnCallback="UserNameComboBox_Callback">
<ClearButton Visibility="True" DisplayMode="Always"></ClearButton>
                    <Paddings PaddingLeft="30px" />
                    <CaptionCellStyle CssClass="editorCaption" />
                    <RootStyle CssClass="editorContainer"/>
                </dx:ASPxComboBox>
                <dx:ASPxComboBox ID="RoleCombobox" Caption="Role" runat="server" AutoPostBack="false" TextField="RoleName" ValueField="RoleName"
                        CssClass="editor" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption" ClearButton-Visibility="True">
<ClearButton Visibility="True" DisplayMode="Always"></ClearButton>

                    <CaptionCellStyle CssClass="editorCaption" />
                    <RootStyle CssClass="editorContainer"/>
                </dx:ASPxComboBox>
                <dx:ASPxButton ID="Submit"  runat="server" Text=""   ImagePosition = "Right" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption"  AutoPostBack="false" Image-Url="~/Content/Images/Icons/check-64-icon.png" Image-Height="20px" Image-Width="20px" Height="36px">
                    <ClientSideEvents Click="onSubmit" />

<Image Height="20px" Width="20px" Url="~/Content/Images/Icons/Search-icon.png"></Image>
                </dx:ASPxButton>
            </dx:PanelContent>
        </PanelCollection>
        <Paddings Padding="0px"   />
    </dx:ASPxCallbackPanel>
    <div class="row" style="margin-right:0px;margin-left:0px;" > 
    <dx:ASPxCallbackPanel ID="userdetailpanel" ClientInstanceName="userdetailpanel" runat="server" OnCallback="userdetailpanel_Callback">
        <PanelCollection>
            <dx:PanelContent>
                <dx:ASPxGridView ID="LoginGridView" runat="server" AutoGenerateColumns="False" ClientInstanceName="LoginGridView" Width="98%" AutoPostBack="false" KeyFieldName="UserId"
                    OnRowValidating="LoginGridView_RowValidating"
                    OnRowDeleting = "LoginGridView_RowDeleting"
                    OnRowUpdating = "LoginGridView_RowUpdating"
                    OnRowInserting ="LoginGridView_RowInserting"
                    OnCellEditorInitialize="LoginGridView_CellEditorInitialize" 
                    OnCustomColumnDisplayText="LoginGridView_CustomColumnDisplayText" 
                    OnCustomButtonCallback="LoginGridView_CustomButtonCallback" >
                    <%--OnBeforeColumnSortingGrouping="LoginGridView_BeforeColumnSortingGrouping"--%><%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
                    <Paddings Padding="0px" PaddingLeft="8px" PaddingRight="8px" />
                     <ClientSideEvents CustomButtonClick="LoginGrid_CustomeBTNClick" />
               
                    <Styles>
                        <Header Wrap="True">
                        </Header>
                        <AlternatingRow Enabled="true" />
                    </Styles>
                    <Paddings PaddingLeft="10px" />
                    <Settings VerticalScrollBarMode="Visible" VerticalScrollableHeight="100" />
                    <ClientSideEvents EndCallback="FilterPanaelCallback" />
                    <SettingsPager PageSize="20" />
                    <SettingsEditing Mode="EditForm" />
                    <Settings ShowFooter="false" UseFixedTableLayout="True" />
                    <SettingsBehavior ConfirmDelete="true" />
                    <SettingsCommandButton>
                        <NewButton  Styles-Style-ForeColor="White" Image-Url="~/Content/Images/Icons/comment-user-add-icon.png">
                            <Image Url="~/Content/Images/Icons/comment-user-add-icon.png">
                            </Image>
                            <Styles>
                                <Style ForeColor="White">
                                </Style>
                            </Styles>
                           
                        </NewButton>
                        <DeleteButton Image-Url="~/Content/Images/Icons/comment-user-close-icon.png">
                            <Image Url="~/Content/Images/Icons/comment-user-close-icon.png">
                            </Image>
                        </DeleteButton>
                        <EditButton Image-Url="~/Content/Images/Icons/comment-user-page-icon.png">
                            <Image Url="~/Content/Images/Icons/comment-user-page-icon.png">
                            </Image>
                        </EditButton>
                        <UpdateButton Image-Url="~/Content/Images/Icons/comment-user-page-icon.png" >
                            <Image Url="~/Content/Images/Icons/comment-user-page-icon.png">
                            </Image>
                        </UpdateButton>
                        <CancelButton Image-Url="~/Content/Images/Icons/comment-user-close-icon.png" >
                            <Image Url="~/Content/Images/Icons/comment-user-close-icon.png">
                            </Image>
                        </CancelButton>
                    </SettingsCommandButton>
                    <SettingsDataSecurity AllowEdit="False" />
                    <SettingsText ConfirmDelete="Do you want delete this user?" 
                        EmptyDataRow="No search results"
                         />
                    <Columns>
                        <dx:GridViewCommandColumn   ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="true" VisibleIndex="2" Width="24%">
                        <CustomButtons>
                            <dx:GridViewCommandColumnCustomButton ID="update"  Text="Edit">
                                <Image Url="~/Content/Images/Icons/comment-user-add-icon.png"></Image>
                            </dx:GridViewCommandColumnCustomButton>
                             <dx:GridViewCommandColumnCustomButton ID="changepassword" Text="Password"   runat="server" >
                                 <Image Height="25px" Url="~/Content/Images/Icons/change_password.png" ></Image>
                            </dx:GridViewCommandColumnCustomButton>
                        </CustomButtons>
                            <HeaderTemplate>
                            <dx:ASPxButton CausesValidation="false" RenderMode="Link" Text="New"  Image-Url="~/Content/Images/Icons/comment-user-add-icon.png" runat="server" AutoPostBack="false">
                                <ClientSideEvents  Click="function(s,e){ShowAddEmployee();}" />
                                <Paddings PaddingLeft="30%" />
                            </dx:ASPxButton>
                        </HeaderTemplate>
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataDateColumn FieldName="CreateDate" HeaderStyle-Font-Size="Medium" ReadOnly="true" Visible="false">
                            <PropertiesDateEdit>
                                <TimeSectionProperties>
                                    <TimeEditProperties>
                                        <ClearButton Visibility="Auto">
                                        </ClearButton>
                                    </TimeEditProperties>
                                </TimeSectionProperties>
                                <ClearButton Visibility="Auto">
                                </ClearButton>
                            </PropertiesDateEdit>
                            <HeaderStyle Font-Size="Medium"></HeaderStyle>
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataDateColumn FieldName="LastLoginDate" HeaderStyle-Font-Size="Medium" ReadOnly="true" Visible="false" VisibleIndex="0">
                            <PropertiesDateEdit DisplayFormatString="MM/dd/yyyy hh:mm tt">
                                <TimeSectionProperties>
                                    <TimeEditProperties>
                                        <ClearButton Visibility="Auto">
                                        </ClearButton>
                                    </TimeEditProperties>
                                </TimeSectionProperties>
                                <ClearButton Visibility="Auto">
                                </ClearButton>
                            </PropertiesDateEdit>
                            <HeaderStyle Font-Size="Medium"></HeaderStyle>
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataDateColumn FieldName="LastActivityDate" HeaderStyle-Font-Size="Medium" ReadOnly="true" Visible="false" VisibleIndex="1">
                            <PropertiesDateEdit DisplayFormatString="MM/dd/yyyy hh:mm tt">
                                <TimeSectionProperties>
                                    <TimeEditProperties>
                                        <ClearButton Visibility="Auto">
                                        </ClearButton>
                                    </TimeEditProperties>
                                </TimeSectionProperties>
                                <ClearButton Visibility="Auto">
                                </ClearButton>
                            </PropertiesDateEdit>
                            <HeaderStyle Font-Size="Medium"></HeaderStyle>
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataComboBoxColumn FieldName="Role" HeaderStyle-Font-Size="Medium" VisibleIndex="7">
                            <PropertiesComboBox AllowNull="False">
                                <ClearButton Visibility="Auto">
                                </ClearButton>
                                <ValidationSettings>
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesComboBox>
                            <HeaderStyle Font-Size="Medium"></HeaderStyle>
                        </dx:GridViewDataComboBoxColumn>
                         
                        <dx:GridViewDataCheckColumn FieldName="IsLockedOut" Caption="Locked Out" HeaderStyle-Font-Size="Medium" VisibleIndex="8" Width="10%">
                            <EditFormSettings Visible="False" />
                            <HeaderStyle Font-Size="Medium"></HeaderStyle>
                            <DataItemTemplate>
                                  <%# (Eval("IsLockedOut").ToString().Contains("False")) ? "<i class='fa fa-check-circle check_false' title='Not Locked'  aria-hidden='true'></i>" : "<i class='fa fa-check-circle check_true' title='Locked'    aria-hidden='true'></i>" %>
                            </DataItemTemplate>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataCheckColumn FieldName="IsLoggedIn" Caption="Logged In" HeaderStyle-Font-Size="Medium" ReadOnly="true" VisibleIndex="9" Width="10%">
                            <EditFormSettings Visible="False" />
                            <HeaderStyle Font-Size="Medium"></HeaderStyle>
                            <DataItemTemplate>
                                  <%# (Eval("IsLoggedIn").ToString().Contains("False")) ? "<i class='fa fa-check-circle check_false' title='Not Logged'     aria-hidden='true'></i>" : "<i class='fa fa-check-circle check_true' title='Logged in'  aria-hidden='true'></i>" %>
                            </DataItemTemplate>
                        </dx:GridViewDataCheckColumn>
                         
                        <dx:GridViewDataTextColumn Caption="Activate" FieldName="Activated"  HeaderStyle-Font-Size="Medium" Name="Activated" ShowInCustomizationForm="True" VisibleIndex="11" Width="7%">
                            <EditFormSettings Visible="False" />
                            <HeaderStyle Font-Size="Medium"></HeaderStyle>
                            <DataItemTemplate>
                            
                             <%# (Eval("Activated").ToString().Contains("0")||Eval("Activated").ToString()=="") ? "<div  title='Not Active'   id='link_"+Eval("UserId")+"' ><a href=\"javascript:Activate('"+ Eval("UserId") +"',this)\">Activate</a></div>" : "<i   title='Active'  class='fa fa-check-circle check_true'  aria-hidden='true'></i>" %>
                         </DataItemTemplate>
                         </dx:GridViewDataTextColumn>                      

                         

                        <dx:GridViewDataTextColumn FieldName="UserName" ReadOnly="True"  HeaderStyle-Font-Size="Medium" ShowInCustomizationForm="True" VisibleIndex="3" Caption="EmployeeID">
                            <PropertiesTextEdit>
                                <ValidationSettings>
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                             
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Email"  HeaderStyle-Font-Size="Medium" ShowInCustomizationForm="True" VisibleIndex="4">
                            <PropertiesTextEdit>
                                <ValidationSettings>
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Password"  ShowInCustomizationForm="True" VisibleIndex="5" Visible="False">
                            <PropertiesTextEdit ClientInstanceName="EmpPassword" Password="True">
                                <ValidationSettings>
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <EditFormSettings Visible="True" />
                        </dx:GridViewDataTextColumn>
                    </Columns>
               
                    <Styles Header-Wrap="True" >
                        <Header Wrap="True">
                        </Header>
                        <AlternatingRow Enabled="True">
                        </AlternatingRow>
                    </Styles>
                    <Paddings PaddingTop="0px" />
                    <Border BorderWidth="0px" />
                    <BorderBottom BorderWidth="1px" />
                </dx:ASPxGridView>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
   </div>
        

    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="addEmployee">
        <div class="vertical-alignment-helper"> 
     <div class="modal-dialog vertical-align-center"> 
            <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="ResetFieldsEmployee();"><span style="color: #0e0e0e;font-size: 26px;" aria-hidden="true">&times;</span></button>
                  <img class="headerimg_" src="../Content/Images/Icons/comment-user-add-icon.png" /> <h4 class="modal-title" id="myModalLabel">Create employee</h4>
              </div>
              <div class="modal-body">
                    <dx:ASPxCallbackPanel runat="server" ID="ASPxCallbackPanel1"  ClientInstanceName="EmployeePanel" RenderMode="Table" OnCallback="ASPxCallbackPanel1_Callback">
                 
                <PanelCollection>
                    <dx:PanelContent ID="PanelContent3" runat="server">
                  <div class="profile">
                    <dx:ASPxLabel runat="server" ID="statusEmp" ClientInstanceName="statusEmp" Text="" ForeColor="Red"></dx:ASPxLabel>
                  </div>
                 <div class="profile_tab">
                  
                                <dx:ASPxTextBox ID="EmployeeID" ClientInstanceName="EmployeeID" Caption="EmployeeID" runat="server"  AutoPostBack="false"  ValidationSettings-RequiredField-IsRequired="true" >
                               <ValidationSettings ErrorDisplayMode="ImageWithTooltip" >
                               </ValidationSettings>
                                    <ClientSideEvents Validation="ValidateGeneral" />
                                </dx:ASPxTextBox>
                       </div>
                      
                        <div class="profile_tab">
                                <dx:ASPxTextBox ID="Email" ClientInstanceName="Email" Caption="Email" runat="server" 
                                     AutoPostBack="false" ValidationSettings-RequiredField-IsRequired="true" EnableClientSideAPI="True" >                                                                        
                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" >
                        <RegularExpression  ErrorText="Please correct your email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                      <RequiredField IsRequired="True" ErrorText="E-mail is required" />

                        </ValidationSettings>
                                     
                               </dx:ASPxTextBox>
                            
                           </div>  
                        <div class="profile_tab">
                            <dx:ASPxTextBox ID="EmpPassword" Password="true" Caption="Password" runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="EmpPassword1" ValidationSettings-RequiredField-IsRequired="true">                                                                
                            <ClientSideEvents Validation="ValidatePassword1" TextChanged="ValidatePassword2" />
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ErrorText="Pasassword is empty !" >
                               </ValidationSettings>

                            </dx:ASPxTextBox>
                        </div>
                           <div class="profile_tab">
                            <dx:ASPxTextBox ID="EmpPassword2" Password="true" Caption="Retype password" runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="EmpPassword2"  ValidationSettings-RequiredField-IsRequired="true">                                                                
                            <ClientSideEvents Validation="ValidatePassword2" />
                                 <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ErrorText="Retype password is empty !" >
                                     </ValidationSettings>
                                 </dx:ASPxTextBox>
                        </div>
                   <div class="profile_tab" >
                            <dx:ASPxCheckBox Text="Send Email" ID="SendEmail"  runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="SendEmail" Checked="true">                                                                
                            </dx:ASPxCheckBox>
                        </div>
                         <div class="profile_tab" >
                            <dx:ASPxCheckBox Text="Activate" ID="ActivateModal2"  runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="ActivateModal" Checked="true">                                                                
                             </dx:ASPxCheckBox>
                          </div>
                          <div class="profile" >
                            <dx:ASPxCheckBox Text="Lockout" ID="Lockout"  runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="Lockout" >                                                                
                             </dx:ASPxCheckBox>
                          </div> 
                         <div>

                              <!-- Nav tabs -->
                              <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active" id="activeTab"><a href="#basic" aria-controls="home" role="tab" data-toggle="tab">Basic Information</a></li>
                                 <li role="presentation" id="profileTab"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Employee Access</a></li>
                                 <li role="presentation"id="hrTab" ><a href="#hr" aria-controls="hr" role="tab" data-toggle="tab">HR Information</a></li>
                                <li role="presentation" id="hrsTab"><a href="#hrs" aria-controls="profile" role="tab" data-toggle="tab">Hourly Rate</a></li>
                               
                              </ul>

                              <!-- Tab panes -->
                              <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="basic">
                                    <div class="tabScroll_cu">
                                                            
                                        <div class="profile_tab">
                                            <dx:ASPxTextBox ID="EmpFName" ClientInstanceName="EmpFName" Caption="First Name" runat="server" AutoPostBack="false" ValidationSettings-RequiredField-IsRequired="true">
                                          <ClientSideEvents Validation="ValidateGeneral"  />
                                             <ValidationSettings ErrorDisplayMode="ImageWithTooltip"  >
                                     </ValidationSettings>
                                        </dx:ASPxTextBox>

                                        </div>
                                        <div class="profile_tab">
                                            <dx:ASPxTextBox ID="EmpLName" ClientInstanceName="EmpLName" Caption="Last Name" runat="server" AutoPostBack="false" ValidationSettings-RequiredField-IsRequired="true">
                                             <ClientSideEvents Validation="ValidateGeneral"  />
                                             <ValidationSettings ErrorDisplayMode="ImageWithTooltip"  >
                                     </ValidationSettings>
                                            </dx:ASPxTextBox>

                                        </div>
                                        <div class="profile_tab">
                                            <dx:ASPxTextBox ID="EmpTitle" ClientInstanceName="EmpTitle" Caption="Title" runat="server" AutoPostBack="false" >
                                            </dx:ASPxTextBox>

                                        </div>
                                        <div class="profile_tab">
                                            <dx:ASPxTextBox ID="EmpStreet" ClientInstanceName="EmpStreet" Caption="Address One" runat="server" AutoPostBack="false">
                                            </dx:ASPxTextBox>

                                        </div>
                                        <div class="profile_tab">
                                            <dx:ASPxTextBox ID="EmpStreet2" ClientInstanceName="EmpStreet2" Caption="Address Two" runat="server" AutoPostBack="false">
                                            </dx:ASPxTextBox>

                                        </div>
                                        <div class="profile_tab">
                                            <dx:ASPxTextBox ID="EmpCity" ClientInstanceName="EmpCity" Caption="City" runat="server" AutoPostBack="false">
                                            </dx:ASPxTextBox>

                                        </div>
                                        <div class="profile_tab">
                                            <dx:ASPxTextBox ID="EmpState" ClientInstanceName="EmpState" Caption="EmpState" runat="server" AutoPostBack="false">
                                            </dx:ASPxTextBox>
                                        </div>
                                        <div class="profile_tab">
                                            <dx:ASPxTextBox ID="EmpZip" ClientInstanceName="EmpZip" Caption="ZipCode" runat="server" AutoPostBack="false">
                                            </dx:ASPxTextBox>
                                        </div>
                                        </div>
                                </div>
                                  <div role="tabpanel" class="tab-pane" id="hrs">
                                  <div class="tabScroll_cu">
                                                         <div class="profile_tab">
                                                                    <dx:ASPxSpinEdit ID="EmpBillRate"    ClientInstanceName="EmpBillRate" Caption="Bill Rate" runat="server" AutoPostBack="false">
                                                                    </dx:ASPxSpinEdit>
                                         </div>
                               
                                        <div class="profile_tab">
                                            <dx:ASPxSpinEdit ID="EmpCostRate"     Caption="Cost Rate" ClientInstanceName="EmpCostRate" runat="server" AutoPostBack="false">
                                           
                                                 </dx:ASPxSpinEdit>
                                        </div>
                                                 <div class="profile_tab">
                                                        <dx:ASPxSpinEdit ID="EmpOTCostRate"      Caption="OT Cost Rate" ClientInstanceName="EmpOTCostRate" runat="server" AutoPostBack="false">
                                                      
                                                              </dx:ASPxSpinEdit>
                                                    </div>
                                                    <div class="profile_tab">
                                                        <dx:ASPxSpinEdit ID="EmpOTBillRate"     Caption="OT BillRate" runat="server" AutoPostBack="false" ClientInstanceName="EmpOTBillRate">
                                                       
                                                             </dx:ASPxSpinEdit>
                                                    </div>
                                  </div>
                          
                                  </div>
                                <div role="tabpanel" class="tab-pane" id="hr">
                                    <div class="tabScroll_cu">
                                                                 <div class="profile_tab">
                                                                    <dx:ASPxDateEdit ID="EmpDateHired" Caption="Hiring Date" runat="server" AutoPostBack="false" ClientInstanceName="EmpDateHired">
                                                                    </dx:ASPxDateEdit>
                                                                </div>
                                                                <div class="profile_tab">
                                                                    <dx:ASPxTextBox ID="EmpContact" Caption="Contact" runat="server" AutoPostBack="false" ClientInstanceName="EmpContact">
                                                                    </dx:ASPxTextBox>
                                                                </div>
                                                                <div class="profile_tab">
                                                                    <dx:ASPxTextBox ID="EmpDepartment" Caption="Department" runat="server" AutoPostBack="false" ClientInstanceName="EmpDepartment">
                                                                    </dx:ASPxTextBox>
                                                                </div>
                                                      
                                                                <div class="profile_tab">
                                                                    <dx:ASPxTextBox ID="EmpMemo" Caption="Memo" runat="server" AutoPostBack="false" ClientInstanceName="EmpMemo" ClientVisible="false">
                                                                    </dx:ASPxTextBox>
                                                                </div>
                                                      
                                                                
                                                                <div class="profile_tab">
                                                                    <dx:ASPxSpinEdit ID="EmpVac" Caption="Vacations" runat="server" AutoPostBack="false" ClientInstanceName="EmpVac">
                                                                    </dx:ASPxSpinEdit>
                                                                </div>
                                                                <div class="profile_tab">
                                                                    <dx:ASPxSpinEdit DisplayFormatString="N2" ID="EmpSick" Caption="Sick" runat="server" AutoPostBack="false" ClientInstanceName="EmpSick">
                                                                      
                                                                    </dx:ASPxSpinEdit>
                                                                </div>
                                                                <div class="profile_tab">
                                                                    <dx:ASPxSpinEdit ID="EmpHol" Caption="Holidays" runat="server" AutoPostBack="false" ClientInstanceName="EmpHol">
                                                                    </dx:ASPxSpinEdit>
                                                                </div> 
                                                                <div class="profile_tab">
                                                                    <dx:ASPxDateEdit ID="EmpNextImpDate" Caption="Visa Date" runat="server" AutoPostBack="false" ClientInstanceName="EmpNextImpDate">
                                                                    </dx:ASPxDateEdit>
                                                                </div>
                                                                <div class="profile_tab">
                                                                    <dx:ASPxDateEdit ID="UDF1" Caption="Passport Expiry" runat="server" AutoPostBack="false" ClientInstanceName="UDF1">
                                                                    </dx:ASPxDateEdit>
                                                                </div>    
                                                                <div class="profile_tab">
                                                                    <dx:ASPxDateEdit ID="UDF2" Caption="Emirates ID" runat="server" AutoPostBack="false" ClientInstanceName="UDF2">
                                                                    </dx:ASPxDateEdit>
                                                                </div>
                                                                <div class="profile_tab">
                                                                    <dx:ASPxDateEdit ID="UDF3" Caption="Labor Card" runat="server" AutoPostBack="false" ClientInstanceName="UDF3">
                                                                    </dx:ASPxDateEdit>
                                                                </div>
                                                                <div class="profile_tab">
                                                                    <dx:ASPxDateEdit ID="UDF4" Caption="AC. Card" runat="server" AutoPostBack="false" ClientInstanceName="UDF4">
                                                                    </dx:ASPxDateEdit>
                                                                </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="profile">      
                                             <div class="tabScroll_cu">
                                                      
                                                    <div class="profile">
                                                      <dx:ASPxComboBox ID="managers" Caption="Manager" runat="server"  AllowNull="False" AutoPostBack="false" ClientInstanceName="managers" ValidationSettings-RequiredField-IsRequired="false" >
                                                        <ClearButton Visibility="Auto">
                                                        </ClearButton>
                                                      </dx:ASPxComboBox>
                                                    </div>
                                                    <div> 
                                                    <div class="profile_tab">
                                                        <dx:ASPxListBox ID="roles" runat="server" Width="66%" Height="140px" SelectionMode="CheckColumn" Caption="Roles" ClientInstanceName="roles" ValueField="RoleName" TextField="RoleName">
                                                             <CaptionSettings Position="Top" />
                                                        </dx:ASPxListBox>
                                                    </div>
                                                         
                                                </div>
                                                       
                            </div>
                                </div>
     
                              </div>

                            </div>
                     </dx:PanelContent>
                </PanelCollection>
                </dx:ASPxCallbackPanel>
              </div>
              <div class="modal-footer">
                  <div style="float:left;">
                      <dx:ASPxLabel ClientInstanceName="_blankFields"   ClientVisible="false" runat="server" ForeColor="red" Text="Some required fields are blank "></dx:ASPxLabel>
                  </div>
                 <br />
                  <label style="float:left;color:red;">* required fields </label>
                    <dx:ASPxLabel runat="server" ID="ASPxLabel10" ClientInstanceName="status_employee" ClientVisible="false"  CssClass="project_save_status"></dx:ASPxLabel>
                 <button type="button" class="btn btn-default" data-dismiss="modal" onclick="ResetFieldsEmployee();">Close</button>
                <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Submit" AutoPostBack="false" 
                    ClientInstanceName="addEmployee" 
                     ClientSideEvents-Click="function (s,e){addEmployeeFunctionOptions();}" />
              </div>
            </div>
            </div> 
            </div>
       </div>
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="ChangePassword">
        <div class="vertical-alignment-helper"> 
     <div class="modal-dialog vertical-align-center"> 
            <div class="modal-content" style="  width: 33%;
            height: inherit;
            margin: 0 auto;">
              <div class="modal-header" style="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span style="color: #0e0e0e;font-size: 26px;" aria-hidden="true">&times;</span></button>
               <img class="headerimg_" src="../Content/Images/Icons/change_password.png" /> <h4 class="modal-title" id="H1">Change Password</h4>
              </div>
                <div class="modal-body" style="  position: relative;
            padding: 15px;
            float: left;
            border-bottom: 3px solid #e5e5e5;
            margin-bottom: 10px;">
                         <div class="profile">
                         <dx:ASPxLabel runat="server" ID="statusPassword" ClientInstanceName="statusPassword" Text="" ForeColor="Red"></dx:ASPxLabel>
                         </div>
                        <div class="profile">
                                <dx:ASPxTextBox ID="PassowrdUserName" ClientInstanceName="PassowrdUserName" ClientVisible="false" runat="server"  AutoPostBack="false"  >
                               </dx:ASPxTextBox>
                               
                        </div>
                      
                        <div class="profile">
                                <dx:ASPxTextBox ID="NewEmpPassword1" Password="true" ClientInstanceName="NewEmpPassword1" Caption="New Password" runat="server"  AutoPostBack="false" ValidationSettings-RequiredField-IsRequired="true">                   
                               
                                 <ClientSideEvents Validation="ValidateNewPassword1" TextChanged="ValidateNewPassword2"  />
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ErrorText="Pasassword is empty !" >
                               </ValidationSettings>
                                </dx:ASPxTextBox>
                           </div>  
                        <div class="profile">
                            <dx:ASPxTextBox ID="NewEmpPassword2" Password="true" Caption="Retype" runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="NewEmpPassword2" ValidationSettings-RequiredField-IsRequired="true">                                                                
                            <ClientSideEvents Validation="ValidateNewPassword2" />
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ErrorText="Pasassword is empty !" >
                               </ValidationSettings>
                                 </dx:ASPxTextBox>
                        </div>
                     
                </div>
                 <div class="modal-footer">
                    <div style="width:100%;float:left;"> <label style="float:left;color:red;">* required fields</label> 
                    <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
                    <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Submit" AutoPostBack="false" ClientSideEvents-Click="function (s,e){ChangePassword();}" />
                    </div>
                   
              </div>
            </div>
            </div> 
            </div>
         </div>
     <script type="text/javascript">
         // <![CDATA[
         ASPxClientControl.GetControlCollection().ControlsInitialized.AddHandler(function (s, e) {

             UpdateGridHeightCom(LoginGridView);
         });
         ASPxClientControl.GetControlCollection().BrowserWindowResized.AddHandler(function (s, e) {

             UpdateGridHeightCom(LoginGridView);
         });
         // ]]> 


        </script>
</asp:Content>
