﻿using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_Projects : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        
        LoadData();
        
    }
    public void LoadData()
    {
        string sql = "Select * From Client ";
        DataTable tasks = DataAccess.GetDataTableBySqlSyntax(sql, "");
        this.Clients.DataSource = tasks;
        this.Clients.DataBind();
    }
    protected void Projects_CardInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {

    }
    protected void Projects_CustomButtonCallback(object sender, DevExpress.Web.ASPxCardViewCustomButtonCallbackEventArgs e)
    {
       
        if (e.ButtonID == "Edit")
        {
            string ClientID = this.Clients.GetCardValues(e.VisibleIndex, "ClientID").ToString();
            DevExpress.Web.ASPxWebControl.RedirectOnCallback("~/Manager/EditClient.aspx?ClientID=" + ClientID);
        }
        if (e.ButtonID == "ManageProjects")
        {
            string ClientID = this.Clients.GetCardValues(e.VisibleIndex, "ClientID").ToString();
            DevExpress.Web.ASPxWebControl.RedirectOnCallback("~/Manager/ManageClientsProjects.aspx?ClientID=" + ClientID);
        }
    }
    protected void Clients_BatchUpdate(object sender, DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs e)
    {
        for (int i = 0; i < e.UpdateValues.Count;i++ )
        {
            var card = e.UpdateValues[i];

            string updateCommand = "Update Client set ";
            string[] keys = new string[card.NewValues.Count];

            card.NewValues.Keys.CopyTo(keys, 0);

            for (int count = 0; count < card.NewValues.Count; count++)
            {
                if (card.NewValues[this.Clients.KeyFieldName] == card.NewValues[keys[count]])
                {
                   // continue;
                }
                string entry = string.Format("{0} = '{1}'", keys[count], card.NewValues[keys[count]]);
                if (count + 1 == card.NewValues.Count)
                {
                    updateCommand += entry;
                }
                else
                {
                    updateCommand += entry + " , ";
                }

            }
            updateCommand += string.Format(" where ClientID='{0}'", card.NewValues[this.Clients.KeyFieldName]);
            DataAccess.ExecuteSqlStatement(updateCommand,"");          
        }

        for (int i = 0; i < e.InsertValues.Count; i++)
        {
            var card = e.InsertValues[i];

            string insertCommand = "Insert into Client  ";
            string[] keys = new string[card.NewValues.Count];
            string into = "(";
            string values = " Values (";
            card.NewValues.Keys.CopyTo(keys, 0);

            for (int count = 0; count < card.NewValues.Count; count++)
            {
                if (card.NewValues[this.Clients.KeyFieldName] == card.NewValues[keys[count]])
                {
                    // continue;
                }
                into += keys[count];
                values += string.Format("'{0}'", card.NewValues[keys[count]]);
                
                if (count + 1 == card.NewValues.Count)
                {
                    
                    into += ")";
                    values += ")";
                }
                else
                {
                     
                    into += ",";
                    values  += ",";
                }

            }
            insertCommand += into + values;
            DataAccess.ExecuteSqlStatement(insertCommand,"");
        }

        for (int i = 0; i < e.DeleteValues.Count; i++)
        {
            var card = e.DeleteValues[i];

            string DeleteCommand = "Delete From  Client  ";
            string[] keys = new string[card.Values.Count];
            DeleteCommand += "where ClientID = " +"'"+ card.Values["ClientID"]+"'";

                 

           
          //  DeleteCommand += into + values;
            DataAccess.ExecuteSqlStatement(DeleteCommand,"");
        }

        e.Handled = true;
    }

    protected void Clients_CardUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {

        string updateCommand = "Update Client set  ";
        string[] keys = new string[e.NewValues.Count];
        
        e.NewValues.Keys.CopyTo(keys, 0);
        for (int count = 0; count < e.NewValues.Count; count++)
        {
            if (e.NewValues[this.Clients.KeyFieldName] == e.NewValues[keys[count]])
            {
                // continue;
            }
            string entry = string.Format("{0} = '{1}'", keys[count], e.NewValues[keys[count]]);
            if (count + 1 == e.NewValues.Count)
            {
                updateCommand += entry;
            }
            else
            {
                updateCommand += entry + " , ";
            }

        }
        updateCommand += string.Format(" where ClientID='{0}'", e.NewValues[this.Clients.KeyFieldName]);
        DataAccess.ExecuteSqlStatement(updateCommand, "");
        e.Cancel = true;
        this.Clients.CancelEdit();
        LoadData();
    }
    protected void Clients_CardDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        e.Cancel = true;
        string DeleteCommand = "Delete From  Client  ";
        string[] keys = new string[e.Values.Count];
        DeleteCommand += "where ClientID = " + "'" + e.Values["ClientID"] + "'";        
        DataAccess.ExecuteSqlStatement(DeleteCommand, "");
        LoadData();
    }
}