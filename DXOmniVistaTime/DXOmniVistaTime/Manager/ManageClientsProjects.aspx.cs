﻿using DevExpress.Web;
using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using DevExpress.Web.ASPxTreeList;

public partial class Manager_ManageClientsProjects : System.Web.UI.Page
{
    DataTable Projects;
    DataTable Activities;
    DataTable Employees;
    static string baseurl = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        ///test
        ///
       
       
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        if (!IsCallback)
        {
            string ClientID = Request.QueryString["ClientID"];
            this.ClientName.InnerText = ClientID;
            GetClientProjects(ClientID);
            this.managers.DataSource = Roles.GetUsersInRole("Manager");
            MembershipUserCollection users = Membership.GetAllUsers();
            this.EditEmployeeID.DataSource = users;
            this.EditProjectOther1.DataSource = users;
            this.EditProjectOther2.DataSource = users;
            this.EditProjectOther3.DataSource = users;
            this.EditProjectOther4.DataSource = users;
            this.roles.DataSource = Roles.GetAllRoles();
            this.EditClientID.DataSource = DataAccess.GetDataTableBySqlSyntax("Select * from Client", "");
            this.EditProjectStatus.DataSource = DataAccess.GetDataTableBySqlSyntax("Select * from Status","");
            this.EditProjectConType.DataSource = DataAccess.GetDataTableBySqlSyntax("Select * from ContractType", "");
            this.Page.Title = "OVTime : " + ClientID + " Projects";
            
        }
        else
        {
            if (Session["Employees"] != null)
            {
                this.ASPxGridView1.DataSource = (DataTable)Session["Employees"];
                

            }
            GetActivitiesSession();
            
        }
       this.ProjectTree.DataSource = Session["Projects"];


    }
    protected void TreeList_CustomJSProperties(object sender, TreeListCustomJSPropertiesEventArgs e)
    {
         
    }
    private void GetClientProjects(string ClientID)
    {
        string sql = string.Format("Select * From Project where ClientID = '{0}'", ClientID);
        DataTable Projects = DataAccess.GetDataTableBySqlSyntax(sql, "");
        Session["Projects"] = Projects;
        this.ProjectTree.DataSource = Session["Projects"];
        this.ProjectTree.DataBind();

    }
    private void RefreshTree()
    {
         

        string ClientID  = this.ClientName.InnerText  ;
        GetClientProjects(ClientID);
        
         
    }
    protected void ProjectTree_CustomCallback(object sender, DevExpress.Web.ASPxTreeList.TreeListCustomCallbackEventArgs e)
    {
        if (e.Argument == "Expand")
        {
            
            this.ProjectTree.CollapseAll();
            this.ProjectTree.ExpandToLevel(4);
        }
        else if (e.Argument == "Collapse")
        {

            this.ProjectTree.CollapseAll();
            // this.ProjectTree.ExpandToLevel(0);
        }
        else if (e.Argument == "Search")
        {
            SearchTree(this.searchTxt.Text);
        }
        else if (e.Argument == "Refresh")
        {
            RefreshTree();
        }
        else
        {
            string ProjectID = e.Argument;
            GetProjectBasicData(ProjectID);
        }
       
    }
    private void SearchTree(string searchQuery)
    {
        DataTable results = new DataTable();
        string ClientID = this.ClientName.InnerText;
        GetClientProjects(ClientID);
        DataTable projects = (DataTable)Session["Projects"];
        
        var test = projects.Select("ProjectID like '%" + searchQuery + "%' or ProjectName like '%" + searchQuery + "%' ");
        if (test.Count() > 0)
        {
            this.ProjectTree.DataSource = projects.Select("ProjectID like '%" + searchQuery + "%' or ProjectName like '%" + searchQuery + "%' ").CopyToDataTable();
            Session["Projects"] = projects.Select("ProjectID like '%" + searchQuery + "%' or ProjectName like '%" + searchQuery + "%' ").CopyToDataTable();
        }
        else
        {
            this.ProjectTree.DataSource = test;
            this.ProjectTree.DataBind();
        }

        DataBind();
    }
    protected void grid_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (!ASPxGridView1.IsEditing ) return;
        //   if (e.KeyValue == DBNull.Value || e.KeyValue == null) return;
        e.Editor.ReadOnly = false;
        if (e.Column.FieldName == "EmployeeID")
        {
            ASPxComboBox combo = e.Editor as ASPxComboBox;
            combo.Enabled = true;
            // List<string> employess = GetEmployees();
            combo.DataSource = GetEmployees();
            combo.DataBind();
            combo.AllowNull = true;
          
        }
        if (e.Column.FieldName == "ControlID")
        {
            ASPxComboBox combo = e.Editor as ASPxComboBox;
            combo.Enabled = true;
           // combo.DataSource = GetActivities(this.EditProjectID.Text);
         // combo.DataSource =   ;
           // combo.DataBind();
            combo.Items.AddRange(this.lbAvailable.GetSelectedFieldValues("ActivityID"));
           // combo.AllowNull = true;
        }



    }

    List<string> GetCities(string country)
    {
        return null;
    }

    protected void FillCityCombo(ASPxComboBox cmb, string country)
    {
        if (string.IsNullOrEmpty(country)) return;

        List<string> cities = GetCities(country);
        cmb.Items.Clear();
        foreach (string city in cities)
            cmb.Items.Add(city);
    }
    private DataTable GetEmployees()
    {
        string sql = "Select * from Employee ";
        DataTable employees = DataAccess.GetDataTableBySqlSyntax(sql, "");
        return employees;
       
    }

    ////get activities
    private DataTable GetActivities(string ProjectID)
    {
        string sql = string.Format("Select EmployeeControl.ControlID   from EmployeeControlExt  Left join EmployeeControl on EmployeeControl.EmployeeControlID = EmployeeControlExt.EmployeeControlID where ProjectID = '{0}'  and EmployeeControl.ControlType=2", ProjectID);
        DataTable activities = DataAccess.GetDataTableBySqlSyntax(sql, "");
        return activities;
    }
   
    private void GetProjectBasicData(string ProjectID)
    {
        string query = string.Format("Select * from Project where ProjectID = '{0}'", ProjectID);
        DataRow Project = DataAccess.GetDataTableBySqlSyntax(query, "").Rows[0];
        this.DropDownEdit.Text = Project["ParentProjectID"].ToString();
        this.DropDownEdit.Value = Project["ParentProjectID"].ToString();
        this.EditProjectID.Text = Project["ProjectID"].ToString();
        this.EditProjectCode.Text = Project["ProjectCode"].ToString();
        this.EditProjectPhase.Text = Project["ProjectPhase"].ToString();
        this.EditProjectName.Text = Project["ProjectName"].ToString();
        this.EditProjectStreet.Text = Project["ProjectStreet"].ToString();
        this.EditProjectCity.Text = Project["ProjectCity"].ToString();
        this.EditProjectState.Text = Project["ProjectState"].ToString();
        this.EditProjectZip.Text = Project["ProjectZip"].ToString();
        this.EditProjectStartDate.Date = Project["ProjectStartDate"].ToString() == String.Empty ? DateTime.Now : DateTime.Parse(Project["ProjectStartDate"].ToString());  
        this.EditProjectStatus.Text = Project["ProjectStatus"].ToString();
        this.EditEmployeeID.Text = Project["EmployeeID"].ToString();
        this.EditFSID.Text = Project["FSID"].ToString();
        this.EditClientID.Text = Project["ClientID"].ToString();
        this.EditProjectConType.Text = Project["ProjectConType"].ToString();
        this.EditProjectOther1.Text = Project["ProjectOther1"].ToString();
        this.EditProjectOther2.Text = Project["ProjectOther2"].ToString();
        this.EditProjectOther3.Text = Project["ProjectOther3"].ToString();
        this.EditProjectOther4.Text = Project["ProjectOther4"].ToString();
        // head of the edit panel
        this.ProjectNameHead.Text = Project["ProjectID"].ToString();
        this.ProjectNameHead.Value = Project["ProjectID"].ToString();

    }
    private void GetEmployeeControl(string ProjectID)
    {
      //  string sql1 = "Select  EmployeeControl.ControlID  From EmployeeControl  Left Join EmployeeControlExt  on EmployeeControl.EmployeeControlID = EmployeeControlExt.EmployeeControlID  where  ProjectId =  '" + ProjectID + "' and EmployeeControl.EmployeeID is null";
      //  DataTable Activities = DataAccess.GetDataTableBySqlSyntax(sql1, "");
        //var Activities = this.ASPxGridView1.GetSelectedFieldValues("ActivityID");
        var Activities = GetAllActivities(ProjectID);
        DataTable dt = new DataTable();
        string sql2 = " Select  EmployeeControl.* from EmployeeControlExt   Left join EmployeeControl on EmployeeControl.EmployeeControlID = EmployeeControlExt.EmployeeControlID where ProjectID = '" + ProjectID + "'";
        dt = DataAccess.GetDataTableBySqlSyntax(sql2, "");
        if (Activities.Rows.Count > 0)
        {
            foreach (DataRow dr in Activities.Rows)
            {
            //    string sql2 = " Select  EmployeeControl.* from EmployeeControlExt   Left join EmployeeControl on EmployeeControl.EmployeeControlID = EmployeeControlExt.EmployeeControlID where ProjectID = '" + ProjectID + "'";
            //    dt.Merge(DataAccess.GetDataTableBySqlSyntax(sql2, ""));
            }
        }
        else
        {
            //string sql = "Select  EmployeeControl.EmployeeControlID,Employee.EmployeeID ,Employee.* from Employee inner join EmployeeControl on EmployeeControl.EmployeeID =  Employee.EmployeeID where ControlType=1 and ControlID = '"+ProjectID+"'";
            //dt = DataAccess.GetDataTableBySqlSyntax(sql, "");
            //DataColumn dc = new DataColumn("ControlID", typeof(System.String));
            //dc.DefaultValue = "Empty";
            //dt.Columns.Add(dc);
        }

        //string sql = "Select   EmployeeControl.*,EmployeeControl.ControlID ,Employee.*  From EmployeeControl  Left Join Employee  on EmployeeControl.EmployeeID = Employee.EmployeeID  where  ControlID =  '" + ProjectID + "' ";
        // string sql = "Select Employee.* , EmployeeControl.*,EmployeeControlExt.* From EmployeeControl Left Join EmployeeControlExt  on ControlID = ProjectID  Left Join Employee  on EmployeeControl.EmployeeID = Employee.EmployeeID where  ControlID = '" + ProjectID + "' ";
        // string sql = "Select * From EmployeeControl where ControlType = 1 and ControlID = '" + ProjectID + "' ";
        // DataTable Employees = DataAccess.GetDataTableBySqlSyntax(sql, "");

        Session["Employees"] = dt;
        
        this.ASPxGridView1.DataSource = dt;
        this.ASPxGridView1.DataBind();
    }
    
    private void GetAllActivitiesNew()
    {
        string sql = "Select * From Activity ";
        Activities = DataAccess.GetDataTableBySqlSyntax(sql, "");
        this.lbAvailable.DataSource = Activities;
        DataBind();
    }
    
    private DataTable GetAllActivities(string ProjectID)
    {
        string sql = "Select * From Activity ";
        Activities = DataAccess.GetDataTableBySqlSyntax(sql, "");
        string sql2 = "Select  EmployeeControl.ControlID as ActivityID from EmployeeControlExt  Left join EmployeeControl on EmployeeControl.EmployeeControlID = EmployeeControlExt.EmployeeControlID where ProjectID = '" + ProjectID + "'  and EmployeeControl.ControlType=2";
        // string sql2 = string.Format("Select * from EmployeeControlExt where ProjectID = '{0}'", ProjectID);
        this.lbAvailable.DataSource = Activities;
        DataTable choosen = new DataTable();
        if (this.lbChoosen.Items.Count > 0)
        {
            choosen.Columns.Add("ActivityID");
            foreach(ListEditItem str in this.lbChoosen.Items)
            {
               DataRow dr =  choosen.NewRow();
               dr["ActivityID"] = str.Text;
               choosen.Rows.Add(dr);

            }
            choosen.Rows.Add(this.lbChoosen.Items);
        }
        else
        {
           choosen = DataAccess.GetDataTableBySqlSyntax(sql2, "");
        }
       
        for (int i = 0; i < choosen.Rows.Count; i++)
        {
            for (int j = 0; j < Activities.Rows.Count; j++)
            {
                if (Activities.Rows[j].RowState != DataRowState.Deleted)
                {
                    if (choosen.Rows[i]["ActivityID"].ToString().Equals(Activities.Rows[j]["ActivityID"].ToString()))
                    {
                        lbAvailable.Selection.SelectRowByKey(Activities.Rows[j]["ActivityID"].ToString());
                        //Activities.Rows[j].Delete();

                        break;
                    }
                }
            }
        }
        return choosen;
 //       this.lbChoosen.DataSource = choosen;
       
       
    }
    
    protected void ASPxCallbackPanel2_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
       
        //if (ProjectTree.FocusedNode != null)
        //{

        //    string ProjectID = ProjectTree.FocusedNode["ProjectID"].ToString();
        //    ProjectTree.CollapseAll();
        //    bool enableAnimation = true;
        //    if (ASPxCallbackPanel2.IsCallback)
        //    {
        //        // Intentionally pauses server-side processing, 
        //        // to demonstrate the Loading Panel functionality.
        //        if (!enableAnimation)
        //            Thread.Sleep(500);
        //    }
        //    if (IsPostBack)
        //    {
        //        GetProjectBasicDataView(ProjectID);
        //        GetAllActivitiesView(ProjectID);
        //        GetEmployeeControl(ProjectID);
        //        DataBind();
        //    }
        //}
    }
   
    protected void ASPxCallbackPanel1_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
        Session["Employees"] = null;
        this.ASPxGridView1.DataSource = null;
        this.lbAvailable.DataSource = null;
        this.ASPxGridView1.DataBind();
        this.lbAvailable.DataBind();
        this.lbChoosen.DataSource = null;
        this.lbChoosen.DataBind();
        this.lbChoosen.Items.Clear();
        this.lbAvailable.Selection.UnselectAll();
        if (e.Parameter == "AddNewProject")
        {
            this.EditProjectID.Enabled = true;
            GetAllActivitiesNew();
            DataBind();
        }
        else if (e.Parameter.Contains("ProjectID="))
        {
            this.EditProjectID.Enabled = false;
            var ProjectID = e.Parameter.Replace("ProjectID=","");
            //  ProjectTree.CollapseAll();

            if (IsPostBack)
            {

                GetProjectBasicData(ProjectID);
                GetEmployeeControl(ProjectID);
                DataBind();
            }
            else
            {
                DataBind();
            }
                 
            
        }
        
    }


    [WebMethod]
    [ScriptMethod]
    public static string AddEmployee(string Employee,string roles,string baseURL)
    {
        Dictionary<string, object> obj = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(Employee);
        Dictionary<object, object> rolesL = new JavaScriptSerializer().Deserialize<Dictionary<object, object>>(roles);
        string[] rolesV = new string[rolesL.Count];
        for (int count = 0; count < rolesV.Length;count++ )
        {
            rolesV[count] = rolesL.Values.ElementAt(count).ToString();
        }
         
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString);
        string sql = string.Format("select * from aspnet_Users where UserName ='{0}'", obj.Values.ElementAt(0));
        if (DataAccess.GetDataTableBySqlSyntax(sql, "").Rows.Count > 0)
        {
            return "Employee already exists.";
        }

        MembershipUser user = Membership.CreateUser(obj.Values.ElementAt(0).ToString(), obj.Values.ElementAt(3).ToString(), obj.Values.ElementAt(2).ToString());
        Roles.AddUserToRoles(obj.Values.ElementAt(0).ToString(), rolesV);
        ///activate part
        ///
        string sql2 = "Update aspnet_Users set Activated = 1 where UserName ='" + obj.Values.ElementAt(0).ToString() + "' ";
        DataAccess.ExecuteSqlStatement(sql2, "");

        CommonFunctions.SendActivationEmail(user, obj.Values.ElementAt(4).ToString(), obj.Values.ElementAt(5).ToString(), baseURL);
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);       
        sql = "Insert into Employee(";
        string into = "";
        string values = " Values (";
        for (int i = 0; i < obj.Count(); i++)
        {
            if (obj.Values.ElementAt(i) == String.Empty || obj.Keys.ElementAt(i) == "EmpPassword" || obj.Keys.ElementAt(i) == "SendEmail" || obj.Keys.ElementAt(i) == "Activate" || obj.Keys.ElementAt(i) == "roles")
            {
                continue;
            }

            
                           
                into += obj.Keys.ElementAt(i)+",";
                values += "'"+obj.Values.ElementAt(i)+"',";
             
           
        }
        into = into.Substring(0, into.Length - 1) +")";
        values = values.Substring(0, values.Length - 1) + ")";

        sql +=  into +values  ;
        DataAccess.ExecuteSqlStatement(sql, "");
        return "";
    }

    [WebMethod]
    [ScriptMethod]
    public static void SaveBasicInfoClick(string project)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        Dictionary<string, object> obj = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(project);
        string sql = "Update Project Set ";
        for (int i = 0; i < obj.Count(); i++)
        {
            if (obj.Keys.ElementAt(i) == "OldID")
            {
                continue;
            }           
            sql += string.Format("{0} = '{1}',", obj.Keys.ElementAt(i).Replace("Edit", ""), obj.Values.ElementAt(i));
            
        }
        sql = sql.Substring(0, sql.Length - 1); 

        sql += string.Format(" where ProjectID = '{0}'", obj["OldID"]);
        DataAccess.ExecuteSqlStatement(sql, "");
    }
    [WebMethod]
    [ScriptMethod]
    public static string AddBasicInfoClick(string project)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        Dictionary<string, object> obj = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(project);
        string projectID = obj.Values.ElementAt(0).ToString() ;
        string check = "select * from Project where ProjectID = '"+projectID+"'";
        if(DataAccess.GetDataTableBySqlSyntax(check, "").Rows.Count >0){
            return "Project ID exists, Please change it.";  
        }

        string sql = "Insert Into Project(  ";
        string into = "";
        string values = "";
          
        for (int i = 0; i < obj.Count(); i++)
        {
            if (obj.Values.ElementAt(i) == String.Empty)
            {
                continue;
            }
            if (i + 1 != obj.Count)
            {
                //into += string.Format("{0} = '{1}',", obj.Keys.ElementAt(i).Replace("New", ""), obj.Values.ElementAt(i));
                into += obj.Keys.ElementAt(i).Replace("Edit", "") +",";
                values += string.Format("'{0}',", obj.Values.ElementAt(i));
            }
            else
            {
               // values += string.Format("{0} = '{1}'", obj.Keys.ElementAt(i).Replace("New", ""), obj.Values.ElementAt(i));
                into += obj.Keys.ElementAt(i).Replace("Edit", "") ;
                values += string.Format("'{0}'", obj.Values.ElementAt(i));
            }
        }
        if (into[into.Length - 1]==',')
        {
            into = into.Remove(into.Length - 1);
        }
        if (values[values.Length - 1] == ',')
        {
            values = values.Remove(values.Length - 1);
        }


        sql = string.Format("{0}{1}) values({2})", sql, into, values);

        DataAccess.ExecuteSqlStatement(sql, "");
        return "";
    }

    [WebMethod]
    [ScriptMethod]
    public static void SaveActivities(string ProjectID, string activities)
    {
        //DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        //List<string> obj = new JavaScriptSerializer().Deserialize<List<string>>(activities);
        /////here we updating activites
        // string delete = "Delete from EmployeeControlExt where ProjectID='" + ProjectID + "'";
        //  DataAccess.ExecuteSqlStatement(delete, "");
        //for (int i = 0; i < obj.Count(); i++)
        //{

        //    ////check if exist
        //    // string sql2 = "Select * from EmployeeControlExt where ProjectID='" + ProjectID + "' and ActivityID ='" + obj[i] + "'";
        //    // DataTable dt = DataAccess.GetDataTableBySqlSyntax(sql2, "");
        //    //  if (dt.Rows.Count > 0)
        //    //  {
        //    //      continue;
        //    //  }

        //    string sql = "Insert INTO EmployeeControlExt(ProjectID,EmployeeControlID) values ( ";
        //    sql += string.Format("'{0}',", ProjectID);
        //    sql += "NEWID())";
        //    sql += "";
        //    DataAccess.ExecuteSqlStatement(sql, "");
        //    string getLastID = "SELECT top 1 EmployeeControlID FROM EmployeeControlExt ORDER BY ID DESC";
        //    string lastID = DataAccess.GetDataTableBySqlSyntax(getLastID, "").Rows[0]["EmployeeControlID"].ToString();
        //    string sql1 = "Insert Into EmployeeControl(EmployeeControlID,ControlType,ControlID) Values ('" + lastID + "',2,'" + obj[i] + "')";
        //    DataAccess.ExecuteSqlStatement(sql1, "");

        //}

    }

    protected void ASPxGridView1_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        GridViewDataComboBoxColumn combo = (GridViewDataComboBoxColumn)this.ASPxGridView1.DataColumns["EmployeeID"];

        
        // List<string> employess = GetEmployees();
        combo.PropertiesComboBox.DataSource = GetEmployees();
         
        combo.PropertiesComboBox.AllowNull = true;
        DataBind();
    }
    protected void ASPxGridView1_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        // e.Args[0]
        // e.Args = "Test";
    }
     

    #region ASPgridView functions

    protected void ASPxGridView1_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        string ProjectId = this.EditProjectID.Text;
        e.Cancel = true;
       
       
        ASPxComboBox cmbStatus = (sender as ASPxGridView).FindEditRowCellTemplateControl(
                    (sender as ASPxGridView).Columns["EmployeeID"]as GridViewDataColumn,
                    "newEmp") as ASPxComboBox;
             


       string EmployeeID = cmbStatus.Value.ToString();       
        string ActivityID = e.NewValues["ControlID"].ToString();
        string EmpBillRate = e.NewValues["EmpBillRate"] == null ? "0" : e.NewValues["EmpBillRate"].ToString();
        string EmpCostRate = e.NewValues["EmpCostRate"] == null ? "0" : e.NewValues["EmpCostRate"].ToString();
        string EmpOTCostRate = e.NewValues["EmpOTCostRate"] == null ? "0" : e.NewValues["EmpOTCostRate"].ToString();
        string EmpOTBillRate = e.NewValues["EmpOTBillRate"] == null ? "0" : e.NewValues["EmpOTBillRate"].ToString();
        string chk = "Select * from EmployeeControl where EmployeeID = '" + EmployeeID 
            + "' and ControlType = 1   and ControlID = '" + this.EditProjectID.Text + "'";
        if (DataAccess.GetDataTableBySqlSyntax(chk, "").Rows.Count < 1)
        {
            string insert = "insert into EmployeeControl (EmployeeID,ControlType,ControlID,EmployeeControlID) values ('" +
                EmployeeID + "','1','" + this.EditProjectID.Text + "',NEWID())";
            DataAccess.ExecuteSqlStatement(insert, "");
        }

     //   string query = "insert into EmployeeControl (EmployeeID,ControlType,ControlID,EmployeeControlID,EmpBillRate,EmpCostRate,EmpOTCostRate,EmpOTBillRate) values ('" + EmployeeID + "','2','" + ActivityID + "',NEWID()," + EmpBillRate + "," + EmpCostRate + "," + EmpOTCostRate + "," + EmpOTBillRate + ")";
     //   DataAccess.ExecuteSqlStatement(query, "");

        ////check the same activity with the same employee if exist or not 
        string chk1 = "select * from EmployeeControl where EmployeeID = '"+EmployeeID+"' and ControlID = '"+ActivityID+"' and ControlType=2";
        DataTable dtchk1 = DataAccess.GetDataTableBySqlSyntax(chk1, "");
        if (dtchk1.Rows.Count > 1)
        {
            string EmployeeCOntrolID = dtchk1.Rows[0]["EmployeeControlID"].ToString();
            chk1 = "select * from EmplyeeControlExt where EmployeeControlID = '" + EmployeeCOntrolID + "'";
            if (DataAccess.GetDataTableBySqlSyntax(chk1, "").Rows.Count>0)
            {
                return;
            }
        }


        string sql = "Insert INTO EmployeeControlExt(ProjectID,EmployeeControlID) values ( ";
        sql += string.Format("'{0}',", ProjectId);
        sql += "NEWID())";
        sql += "";
        DataAccess.ExecuteSqlStatement(sql, "");
        string getLastID = "SELECT top 1 EmployeeControlID FROM EmployeeControlExt ORDER BY ID DESC";
        string lastID = DataAccess.GetDataTableBySqlSyntax(getLastID, "").Rows[0]["EmployeeControlID"].ToString();
        string sql1 = "insert into EmployeeControl (EmployeeID,ControlType,ControlID,EmployeeControlID,EmpBillRate,EmpCostRate,EmpOTCostRate,EmpOTBillRate) values ('" + EmployeeID + "','2','" + ActivityID + "','" + lastID + "'," + EmpBillRate + "," + EmpCostRate + "," + EmpOTCostRate + "," + EmpOTBillRate + ")";
      //  string sql1 = "Insert Into EmployeeControl(EmployeeControlID,ControlType,ControlID) Values ('" +
        //    lastID + "',2,'" + ActivityID + "')";
        DataAccess.ExecuteSqlStatement(sql1, "");
        this.ASPxGridView1.CancelEdit();
        
        GetEmployeeControl(this.EditProjectID.Text.ToString());
    }
    protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    { 
      
         
        string sql = "Delete  from EmployeeControl   where EmployeeControlID ='"+ e.Keys["EmployeeControlID"]+"'";
        e.Cancel = true;
        DataAccess.ExecuteSqlStatement(sql, "");
        GetEmployeeControl(this.EditProjectID.Text.ToString());

    }
    protected void ASPxGridView1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxComboBox cmbStatus = (sender as ASPxGridView).FindEditRowCellTemplateControl(
                 (sender as ASPxGridView).Columns["EmployeeID"] as GridViewDataColumn,
                 "newEmp") as ASPxComboBox;

      
        string EmployeeID = cmbStatus.Value.ToString();
        string EmpBillRate = e.NewValues["EmpBillRate"] == null ? String.Empty : e.NewValues["EmpBillRate"].ToString();
        string EmpCostRate = e.NewValues["EmpCostRate"] == null ? String.Empty : e.NewValues["EmpCostRate"].ToString();
        string EmpOTCostRate = e.NewValues["EmpOTCostRate"] == null ? String.Empty : e.NewValues["EmpOTCostRate"].ToString();
        string EmpOTBillRate = e.NewValues["EmpOTBillRate"] == null ? String.Empty : e.NewValues["EmpOTBillRate"].ToString();

        string q1 = "select * from EmployeeControl where EmployeeControlID = '" + e.Keys["EmployeeControlID"] + "' ";
        DataTable oldV = DataAccess.GetDataTableBySqlSyntax(q1,"");

        string updateCommand = "Update EmployeeControl Set  EmployeeID = '" + EmployeeID + "' , ControlID = '" + e.NewValues["ControlID"] +
            "' ,EmpBillRate = " + EmpBillRate + ",EmpCostRate = " + EmpCostRate + ",EmpOTCostRate = " + EmpOTCostRate + ",EmpOTBillRate = " + EmpOTBillRate;

        updateCommand += string.Format(" where EmployeeControlID ='{0}'", e.Keys["EmployeeControlID"]);

        DataAccess.ExecuteSqlStatement(updateCommand, "");
        ///we shuld update employee access also
        ///

        string OldEmpID = oldV.Rows[0]["EmployeeID"].ToString();
        string ProjectID = this.EditProjectID.Text;

        string up = "Update EmployeeControl set EmployeeID = '" + EmployeeID + "' where ControlID = '" + ProjectID + "' and EmployeeID = '" + OldEmpID + "'";
        DataAccess.ExecuteSqlStatement(up, "");
        GetEmployeeControl(this.EditProjectID.Text);
        e.Cancel = true;
        this.ASPxGridView1.CancelEdit();
    }
    protected void ASPxGridView1_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
    {
        
    }
    protected void ASPxGridView1_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType == GridViewRowType.Data && e.VisibleIndex % 2 == 0)
        {
            e.Row.BackColor = System.Drawing.Color.Azure;
        }
        if (e.RowType == GridViewRowType.Detail && e.VisibleIndex % 2 == 0)
        {
            e.Row.BackColor = System.Drawing.Color.Azure;
        }
    }

    #endregion
    
    protected void ProjectTree_NodeInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        e.Cancel = true;
        string ProjectID = e.NewValues["ProjectID"].ToString();
        string ProjectName = e.NewValues["ProjectName"].ToString();
        string ProjectStartDate = e.NewValues["ProjectStartDate"].ToString();
        string ProjectStatus = e.NewValues["ProjectStatus"].ToString();
        string ProjectCode = e.NewValues["ProjectID"].ToString();
        string ClientID = this.ClientName.InnerText;
        string ParentProjectID = this.ProjectTree.NewNodeParentKey;
        string sql = string.Format("Insert into Project(ProjectID,ProjectName,ProjectStartDate,ProjectStatus,ClientID,ParentProjectID,ProjectCode) values ( '{0}' ,'{1}','{2}','{3}' ,'{4}' ,'{5}','{6}')", ProjectID, ProjectName, ProjectStartDate, ProjectStatus, ClientID, ParentProjectID, ProjectCode);
        DataAccess.ExecuteSqlStatement(sql,"");
        this.ProjectTree.CancelEdit();
        GetClientProjects(ClientID);

    }
    protected void ProjectTree_NodeDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string ProjectID = e.Values["ProjectID"].ToString();
        string sql = string.Format("Delete From Project where ProjectID ='{0}' ", ProjectID); // delete
        DataAccess.ExecuteSqlStatement(sql, "");
        string ClientID = this.ClientName.InnerText;         
        e.Cancel = true;
        GetClientProjects(ClientID);
    }
     

    [WebMethod]
    [ScriptMethod]
    public static string AddActivity(string project)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        Dictionary<string, object> obj = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(project);
        string chk = "select * from Activity where ActivityID = '" + obj.Values.ElementAt(0) + "'";
        if (DataAccess.GetDataTableBySqlSyntax(chk,"").Rows.Count > 0)
        {

            return "Activity with the same id already exist . ";
        }
        string sql = "Insert into activity (ActivityID,ActivityCode,ActivityDescription,ActivitySub) values ('" + obj.Values.ElementAt(0) + "','" + obj.Values.ElementAt(1) + "','" + obj.Values.ElementAt(2) + "','" + obj.Values.ElementAt(3) + "') ";     

        DataAccess.ExecuteSqlStatement(sql, "");
        return "";
    }

    protected void lbAvailable_Callback(object sender, CallbackEventArgsBase e)
    {
        string sql = "Select * From Activity ";
        Activities = DataAccess.GetDataTableBySqlSyntax(sql, "");
        this.lbAvailable.DataSource = Activities;
        DataBind();

    }
    public String GetText(Object container)
    {
        GridViewDataItemTemplateContainer cont = container as GridViewDataItemTemplateContainer;

        if (cont.DataItem != null)
        {
            try
            {
                Object rel = DataBinder.Eval(cont.DataItem, "EmployeeID");      // rel isn't null or empty

                return rel.ToString() ;
            }
            catch (Exception e)
            {
                 
            }
        }
        return "";
    }
    protected void newEmp_Init(object sender, EventArgs e)
    {
        ASPxComboBox combo = sender as ASPxComboBox;
        combo.Enabled = true;
        // List<string> employess = GetEmployees();
        combo.DataSource = GetEmployees();
      //  DataBind();
        combo.AllowNull = false;
        
    }
    protected void GetActivitiesSession(string ProjectID=null)
    {
           
                    string sql = "Select * From Activity ";
                    Activities = DataAccess.GetDataTableBySqlSyntax(sql, "");
                    Session["Activities"] = Activities;
              
            this.lbAvailable.DataSource = Session["Activities"]; 
              
        
    }

    protected void TreeList_Init(object sender, EventArgs e)
    {
        ASPxTreeList tree = (ASPxTreeList)sender;
        tree.DataSource = GetClientProjectsV2();
        tree.DataBind();
        tree.CollapseAll();
    }
    private DataTable GetClientProjectsV2()
    {
        string ClientID = this.ClientName.InnerText; 
        string sql = string.Format("Select * From Project where ClientID = '{0}'", ClientID);
        DataTable Projects = DataAccess.GetDataTableBySqlSyntax(sql, "");
        return Projects;
    }
    
}