﻿<%@ Page Title="OVTime : Clients Veiw" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="NewClient.aspx.cs" Inherits="Manager_NewClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <script src="../Content/js/Common.js"></script>
    <script src="../Content/js/Clients.js"></script>
    <style>
        .detailPanelLarge {
            height:36px;
            margin-bottom:0px;
       }
         .editorText {
            margin-left: 27px;
            margin-right: 35px;
        }
        .editorCaption {
            padding-left:13px;
        }
        .modal-body {
            position: relative;
            padding: 15px;
            float: left;
            border-bottom: 3px solid #e5e5e5;
            margin-bottom: 10px;
        }
        .modal-content {
            width: 55%;
            height: inherit;
            margin: 0 auto;
        }
    </style>
    
      <dx:ASPxCallbackPanel ID="DetailPanel" runat="server" ClientInstanceName="FilterPanel" Width="100%" CssClass="detailPanelLarge" Collapsible="false"  SettingsLoadingPanel-Enabled ="false" >
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
<SettingsLoadingPanel Enabled="False"></SettingsLoadingPanel>

        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                 <label style="float:left;width:30%;">Client Listing </label> 
            

            </dx:PanelContent>
        </PanelCollection>
        <Paddings Padding="8px" PaddingBottom="2px" />
    </dx:ASPxCallbackPanel>

     <dx:ASPxPanel ID="EmployeeSelectorPanel" runat="server" ClientInstanceName="userdetailpanel">
            <PanelCollection>
                <dx:PanelContent>
    <dx:ASPxGridView ID="Clients" ClientInstanceName="Clients" runat="server" AutoGenerateColumns="False" KeyFieldName="ClientID" 
        OnRowDeleting="ASPxGridView1_RowDeleting" Width="100%" OnCustomButtonCallback="ASPxGridView1_CustomButtonCallback">
        <SettingsPager Visible="true" PageSize="20" >
        </SettingsPager>
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsSearchPanel Visible="True"   HighlightResults="true" />
          
        <SettingsText ConfirmDelete="Are you sure? " />
        <SettingsCommandButton>
            
                 <EditButton  >
                     <Image   ToolTip="Edit" Url="~/Content/Images/Icons/comment-user-add-icon.png">
                    </Image>
                </EditButton>
               <NewButton >
                    <Image AlternateText="New" ToolTip="New" Url="~/Content/Images/Icons/comment-user-add-icon.png">
                    </Image>
                </NewButton>
                <DeleteButton  >
                     <Image   Url="~/Content/Images/Icons/comment-user-close-icon.png" ToolTip="Delete">
                     </Image>
                </DeleteButton>
          
        </SettingsCommandButton> 
            <Styles Header-Wrap="True" >
                <Header Wrap="True"></Header>
                <AlternatingRow Enabled="True"></AlternatingRow>
            </Styles>
         <ClientSideEvents CustomButtonClick="Clients_CustomeBTNClick" />
         <Columns>
            <dx:GridViewCommandColumn Width="25%" ShowNewButtonInHeader="True" VisibleIndex="0" ShowEditButton="false" ShowDeleteButton="True" ShowClearFilterButton="True">               
             <HeaderTemplate>
                     <dx:ASPxButton ID="ASPxButton1" CausesValidation="false" RenderMode="Link" Text="New"  Image-Url="~/Content/Images/Icons/comment-user-add-icon.png" runat="server" AutoPostBack="false">
                                <ClientSideEvents  Click="function(s,e){ShowAddClient();}" />
                         <Paddings PaddingLeft="30%" />

                            </dx:ASPxButton>
                </HeaderTemplate>
               
                <CustomButtons>
                     <dx:GridViewCommandColumnCustomButton ID="update"  Text="Edit">
                                <Image Url="~/Content/Images/Icons/comment-user-add-icon.png"></Image>
                            </dx:GridViewCommandColumnCustomButton>
                              
                <dx:GridViewCommandColumnCustomButton Text="Projects"  ID="ManageProjects">
                    <Image ToolTip="projects" url="../Content/Images/Icons/Write-Document-icon.png" ></Image>
                </dx:GridViewCommandColumnCustomButton>
            </CustomButtons>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="ClientID" Name="ClientID" Caption="ID" VisibleIndex="1">
                <PropertiesTextEdit>
                    <ValidationSettings SetFocusOnError="True">
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </PropertiesTextEdit>
             </dx:GridViewDataTextColumn>            
            <dx:GridViewDataTextColumn FieldName="ClientFedID" Name="ClientFedID" Caption="Fed ID" VisibleIndex="2">
                <PropertiesTextEdit>
                    <ValidationSettings SetFocusOnError="True">
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </PropertiesTextEdit>
             </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="First Name" FieldName="ClientFName" Name="ClientFName" VisibleIndex="4">
                <PropertiesTextEdit>
                    <ValidationSettings SetFocusOnError="True">
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ClientCompany" Name="ClientCompany" VisibleIndex="3" Caption="Company">
                <PropertiesTextEdit>
                    <ValidationSettings SetFocusOnError="True">
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Last Name" FieldName="ClientLName" Name="ClientLName" VisibleIndex="5">
                 <PropertiesTextEdit>
                     <ValidationSettings SetFocusOnError="True">
                         <RequiredField IsRequired="True" />
                     </ValidationSettings>
                 </PropertiesTextEdit>
             </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Street" FieldName="ClientStreet" VisibleIndex="6" Visible="false">
             </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Mobile" FieldName="MobileNumber" Name="MobileNumber" VisibleIndex="7">
             </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Email" FieldName="ClientEmail" Name="ClientEmail" VisibleIndex="8">
                 <PropertiesTextEdit>
                     <ValidationSettings SetFocusOnError="True">
                         <RequiredField IsRequired="True" />
                     </ValidationSettings>
                 </PropertiesTextEdit>
             </dx:GridViewDataTextColumn>
        </Columns>
               <Settings VerticalScrollBarMode="Visible" VerticalScrollableHeight="100" />   

        <Styles Header-Wrap="True" >
                        <Header Wrap="True">
                        </Header>
                        <AlternatingRow Enabled="True">
                        </AlternatingRow>
                    </Styles>
                    <Paddings Padding="0px" PaddingLeft="8px" PaddingRight="8px"/>
                    <SettingsPager PageSize="20" />
                    <SettingsEditing Mode="EditForm" />
                    <Settings ShowFooter="false" UseFixedTableLayout="True" />
    </dx:ASPxGridView>
     </dx:PanelContent>
            </PanelCollection>
           
        </dx:ASPxPanel>
     <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="addClient">
        <div class="vertical-alignment-helper"> 
           <div class="modal-dialog vertical-align-center"> 
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span style="color: #0e0e0e;font-size: 26px;" aria-hidden="true">&times;</span></button>
                <img class="headerimg_" src="../Content/Images/Icons/comment-user-add-icon.png" />
                  <h4 class="modal-title" id="myModalLabel">Add new client</h4>
              </div>
                <div class="modal-body">
                      <div class="profile">
                         <dx:ASPxLabel runat="server" ID="status_client0" ClientInstanceName="status_client0" Text="" ForeColor="Red" ></dx:ASPxLabel>
                  </div>
                        <div class="profile">
                  
                                <dx:ASPxTextBox ID="ClientID" ClientInstanceName="ClientID" Caption="Client ID" runat="server"  AutoPostBack="false"  ValidationSettings-RequiredField-IsRequired="true" >
                                      <ValidationSettings ErrorDisplayMode="ImageWithTooltip" >
                                   </ValidationSettings>
                                    <ClientSideEvents Validation="ValidateGeneral" />
                               </dx:ASPxTextBox>
                       </div>                    
                        <div class="profile">
                                <dx:ASPxTextBox ID="ClientFedID" ClientInstanceName="ClientFedID" Caption="Fed ID" runat="server"  AutoPostBack="false" ValidationSettings-RequiredField-IsRequired="false">                   
                               </dx:ASPxTextBox>
                           </div>  
                        <div class="profile">
                            <dx:ASPxTextBox ID="ClientCompany"  Caption="Company" runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="ClientCompany" ValidationSettings-RequiredField-IsRequired="false">                                                                
                            </dx:ASPxTextBox>
                        </div>
                       
                     <!-- Nav tabs -->
                              <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#contacts" aria-controls="home" role="tab" data-toggle="tab">Contacts</a></li>
                                 <li role="presentation"><a href="#address" aria-controls="profile" role="tab" data-toggle="tab">Address</a></li>                               
                              </ul>
                      <!-- Tab panes -->
                              <div class="tab-content">
                                
                                    <div id="contacts" role="tabpanel" class="tab-pane active">
                        
                                            <div class="profile_tab">
                                            <dx:ASPxTextBox ID="ClientFName"  Caption="First Name" runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="ClientFName" ValidationSettings-RequiredField-IsRequired="false">                                                                
                                            </dx:ASPxTextBox>
                                            </div>
                                          <div class="profile_tab"><dx:ASPxTextBox ID="ClientLName"  Caption="Last Name"  runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="ClientLName" ValidationSettings-RequiredField-IsRequired="false">                                                                
                                            </dx:ASPxTextBox>
                                        </div>
                                         <div class="profile_tab">
                                            <dx:ASPxTextBox ID="ClientEmail"  Caption="Email" runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="ClientEmail" ValidationSettings-RequiredField-IsRequired="false">                                                                
                                              <ValidationSettings ErrorDisplayMode="ImageWithTooltip"  >
                                                <RegularExpression ErrorText="Please correct your email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                                <RequiredField IsRequired="false"></RequiredField>

                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </div>
                                        <div class="profile_tab">
                                            <dx:ASPxTextBox ID="ClientPhone"  Caption="Phone" runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="ClientPhone" ValidationSettings-RequiredField-IsRequired="false">                                                                
                                            </dx:ASPxTextBox>
                                        </div>
                                    <div class="profile_tab">
                                            <dx:ASPxTextBox ID="ClientFax"  Caption="Fax" runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="ClientFax" ValidationSettings-RequiredField-IsRequired="false">                                                                
                                            </dx:ASPxTextBox>
                                        </div>
                                     <div class="profile_tab">
                                            <dx:ASPxTextBox ID="MobileNumber"  Caption="Mobile Number" runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="MobileNumber" ValidationSettings-RequiredField-IsRequired="false">                                                                
                                            </dx:ASPxTextBox>
                                        </div>
                     
                                    </div>
                       
                                     <div id="address" role="tabpanel" class="tab-pane"> 
                                         <div class="profile_tab">
                                            <dx:ASPxTextBox ID="ClientStreet"  Caption="Address One" runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="ClientStreet" ValidationSettings-RequiredField-IsRequired="false">                                                                
                                            </dx:ASPxTextBox>
                                        </div>
                                        <div class="profile_tab">
                                            <dx:ASPxTextBox ID="ClientStreet2"  Caption="Address two" runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="ClientStreet2" ValidationSettings-RequiredField-IsRequired="false">                                                                
                                            </dx:ASPxTextBox>
                                        </div>
                                         <div class="profile_tab">
                                            <dx:ASPxTextBox ID="ClientCity"  Caption="ClientCity" runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="ClientCity" ValidationSettings-RequiredField-IsRequired="false">                                                                
                                            </dx:ASPxTextBox>
                                        </div>
                                        <div class="profile_tab">
                                            <dx:ASPxTextBox ID="ClientState"  Caption="State" runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="ClientState" ValidationSettings-RequiredField-IsRequired="false">                                                                
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div> 
                               </div>
                </div>

                 <div class="modal-footer">
                       <label style="float:left;color:red;">* required fields </label>
                    <dx:ASPxLabel runat="server" ID="status_client" ClientInstanceName="status_client" ClientVisible="false"  CssClass="project_save_status"></dx:ASPxLabel>
                 <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
                <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Submit" AutoPostBack="false" ClientInstanceName="addClient" ClientSideEvents-Click="function (s,e){addClientFunctionOptions();}" />
                  </div>
            </div>
            </div> 
            </div>
         </div>
     <script type="text/javascript">
         // <![CDATA[
         ASPxClientControl.GetControlCollection().ControlsInitialized.AddHandler(function (s, e) {

             UpdateGridHeightCom(Clients);
         });
         ASPxClientControl.GetControlCollection().BrowserWindowResized.AddHandler(function (s, e) {

             UpdateGridHeightCom(Clients);
         });
         // ]]> 
        </script>
</asp:Content>

