﻿<%@ Page Title="Projects Management" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="Projects.aspx.cs" Inherits="Manager_Projects" %>

<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v15.2, Version=15.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <script src="../Content/js/Common.js"></script>
     <script src="../Content/js/Projects.js"></script>
     <%--<script src="../Content/js/Modal.js"></script>--%>

         <style>
             .dxeCaptionCell_OVTime {
                    width: 130px !Important;
                }
             .detailPanelLarge {       
                position:fixed;
              }
             .dxtc-leftIndent {
                 display:none;
             }
             .HeadNew {
             }
             .ClientNameHead {
             }
             .tabScroll_cu {
                 height:276px;
                 padding:10px;

             }
           
             .modal-body {
            position: relative;
            padding: 15px;
            float: left;
            border-bottom: 3px solid #e5e5e5;
            margin-bottom: 10px;
        }
        .modal-content {
          
        }
    </style>
    
    
    <dx:ASPxGlobalEvents ID="GlobalEvents" runat="server">
        <ClientSideEvents ControlsInitialized="function(s, e) { UpdateButtonState('edit'); }" />
    </dx:ASPxGlobalEvents>
    
  
    <dx:ASPxCallbackPanel ID="DetailPanel" runat="server" ClientInstanceName="detailPanelSmall" Width="100%" CssClass="detailPanelLarge" Collapsible="false"  SettingsLoadingPanel-Enabled ="false" >
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
<SettingsLoadingPanel Enabled="False"></SettingsLoadingPanel>

        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                 <label style="float:left;width:30%;"> Projects Listing </label> 
                     <table style="float: right;width: 30%;">
                    <tr>
                        <td style="width: 50px;">
                            <dx:ASPxTextBox ID="searchTxt" runat="server" Width="170px" Height="25px" Theme="MetropolisBlue">
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <div style="margin-left: 10px;margin-top: -3px;"> 
                           <i class="fa fa-search" aria-hidden="true" onclick="searchBtn_Click()" style="cursor:pointer;"></i>
                              
                            </div>
                        </td>
                                         
                                             
                    </tr>
                </table>
                    

            </dx:PanelContent>
        </PanelCollection>
        <Paddings Padding="8px" PaddingBottom="2px" />
    </dx:ASPxCallbackPanel>
    <dx:ASPxPanel ID="EmployeeSelectorPanel" runat="server" ClientInstanceName="employeeSelectorPanel">
            <PanelCollection>
                <dx:PanelContent>
                    
             
     <div class="TreeHolder">
    <dx:ASPxTreeList Theme="OVTime"  ID="ProjectTree"  runat="server" AutoGenerateColumns="False" 
        KeyFieldName="ProjectID" 
        ParentFieldName="ParentProjectID" ClientInstanceName="ProjectTree" 
        OnCustomCallback="ProjectTree_CustomCallback" 
        OnNodeInserting="ProjectTree_NodeInserting" OnNodeDeleting="ProjectTree_NodeDeleting" Width="100%"  
        EnableRowsCache="false" EnableViewState="false" >       
        
         <Columns>             
            <dx:TreeListTextColumn FieldName="ProjectName" Name="ProjectName" VisibleIndex="1">
                <HeaderStyle Wrap="True" >
                <Paddings Padding="0px" />
                </HeaderStyle>
                <HeaderCaptionTemplate>
                        <table>
                            <tr>
                            <th><dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="../Content/Images/Icons/Write-Document-icon.png" /></th>
                            <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Project Name" ForeColor="White"/></th>
                            </tr>
                        </table>
                 </HeaderCaptionTemplate>
                 <PropertiesTextEdit>
                    <ValidationSettings>
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                     <Style Wrap="True">
                     </Style>
                </PropertiesTextEdit>
            </dx:TreeListTextColumn>
            <dx:TreeListDateTimeColumn FieldName="ProjectStartDate"  VisibleIndex="2">
              <HeaderStyle Wrap="True" />
                 <HeaderCaptionTemplate>
                <table>
                    <tr>
                    <th><dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="../Content/Images/Icons/Add-Appointment-icon.png" /></th>
                    <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Start Date" ForeColor="White"/></th>
                    </tr>
                </table>
            </HeaderCaptionTemplate>
                  <PropertiesDateEdit>
                    <TimeSectionProperties>
                        <TimeEditProperties>
                            <ClearButton Visibility="Auto">
                            </ClearButton>
                        </TimeEditProperties>
                    </TimeSectionProperties>
                    <ClearButton Visibility="Auto">
                    </ClearButton>
                    <ValidationSettings>
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </PropertiesDateEdit>
            </dx:TreeListDateTimeColumn>
            <dx:TreeListTextColumn Caption="Status" FieldName="ProjectStatus" Name="ProjectStatus" VisibleIndex="3">
                 <HeaderCaptionTemplate>
                <table>
                    <tr>
                    <th><dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="../Content/Images/Icons/Appointment-Cool-icon.png" /></th>
                    <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Status" ForeColor="White"/></th>
                    </tr>
                </table>
                 </HeaderCaptionTemplate>
                <PropertiesTextEdit>
                    <ValidationSettings>
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:TreeListTextColumn>
            <dx:TreeListCommandColumn VisibleIndex="0" ShowNewButtonInHeader="false">
               <CustomButtons>
                   <dx:TreeListCommandColumnCustomButton ID="update" Text="Edit">
                        <Image Url="~/Content/Images/Icons/comment-user-add-icon.png"></Image>
                   </dx:TreeListCommandColumnCustomButton>
                    <dx:TreeListCommandColumnCustomButton ID="info" Text="Info" Visibility="Hidden">
                        <Image Url="~/Content/Images/Icons/comment-user-add-icon.png"></Image>
                   </dx:TreeListCommandColumnCustomButton>
               </CustomButtons>
                <DeleteButton Visible="True">
                     <Image AlternateText="Delete" Url="~/Content/Images/Icons/comment-user-close-icon.png" ToolTip="Delete">
                     </Image>
                </DeleteButton>
                <HeaderCaptionTemplate>
                    
                     <div style="width:150px;">
                         <a href="javascript:AddNewProject()" style="float:left;margin-top: 9px;">
                          <img src="../Content/Images/Icons/comment-user-add-icon.png" alt="New" /><span style="padding: 2px;">New</span></a>
                    <i class="fa fa-expand employees_i" aria-hidden="true" style="margin-left: 10px;color: #FAFAFA;margin-top: 11px;float: left;" onclick="Expand()"></i>
                    <i class="fa fa-compress employees_i" aria-hidden="true" style="margin-left: 10px;color: #FAFAFA;
    margin-top: 11px;
    float: left;" onclick="Collapse()" ></i>
                     </div>
                   
                </HeaderCaptionTemplate>
            </dx:TreeListCommandColumn>
            <dx:TreeListTextColumn Caption="ID" FieldName="ProjectID" Name="ProjectID" VisibleIndex="0"> 
                <HeaderCaptionTemplate>
                         <table>
                            <tr>
                            <th><dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="../Content/Images/Icons/Write-Document-icon.png" /></th>
                            <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Project ID" ForeColor="White"/></th>
                            </tr>
                        </table>
                    </HeaderCaptionTemplate>
                
                
                
                
            </dx:TreeListTextColumn>
            <dx:TreeListTextColumn Caption="Project Code" FieldName="ProjectCode" Name="ProjectCode" VisibleIndex="4" Visible="false">
                <PropertiesTextEdit>
                    <ValidationSettings>
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:TreeListTextColumn>
            
        </Columns>
        <Settings VerticalScrollBarMode="Visible"   />   
        <Styles>
            <AlternatingNode Enabled="true" />
        </Styles>
        <Settings GridLines="Both"    />
        <SettingsPager Mode="ShowPager">
            <PageSizeItemSettings Items="10, 20, 50" Visible="true" />
        </SettingsPager>
        <SettingsBehavior AutoExpandAllNodes="True" AllowFocusedNode="True" />
        <SettingsText ConfirmDelete="are you sure you want to delete this project ?" />
        <Paddings   />
        <ClientSideEvents CustomButtonClick="CustomBTN"  Init="OnTreeListInit"    />
    </dx:ASPxTreeList>
    </div>
       </dx:PanelContent>
            </PanelCollection>
            <Paddings Padding="8px" />
        </dx:ASPxPanel>
    

<!-- Button trigger modal -->

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="addActivity" data-focus-on="input:first" style="z-index: 999999999;">
        <div class="vertical-alignment-helper"> 
           <div class="modal-dialog vertical-align-center"> 
            <div class="modal-content" style="  width: 33%;height: inherit;margin: 0 auto;">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="ResetFields()"><span style="color: #0e0e0e;font-size: 26px;" aria-hidden="true">&times;</span></button>
                 <img class="headerimg_" src="../Content/Images/Icons/comment-user-add-icon.png" />
                <h4 class="modal-title" >New activity</h4>
              </div>
                <div class="modal-body">
                    
                  <div class="profile">
                         <dx:ASPxLabel runat="server" ID="statusAc" ClientInstanceName="statusAc" Text="" ForeColor="Red" ></dx:ASPxLabel>
                  </div>
                        <div class="profile">
                  
                               <dx:ASPxTextBox ID="ActivityID" ClientInstanceName="ActivityID" Caption="Activity ID" runat="server"  AutoPostBack="false"  ValidationSettings-RequiredField-IsRequired="true" >
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" >
                               </ValidationSettings>
                                    <ClientSideEvents Validation="ValidateGeneral" />
                               </dx:ASPxTextBox>
                        </div>
                      
                        <div class="profile">
                                <dx:ASPxTextBox ID="ActivityCode" ClientInstanceName="ActivityCode" Caption="Activity Code" runat="server"  AutoPostBack="false" ValidationSettings-RequiredField-IsRequired="false">                   
                               </dx:ASPxTextBox>
                           </div>  
                        <div class="profile">
                            <dx:ASPxTextBox ID="ActivityDescription"  Caption="Activity Description" runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="ActivityDescription" ValidationSettings-RequiredField-IsRequired="false">                                                                
                            </dx:ASPxTextBox>
                        </div>
                        <div class="profile">
                            <dx:ASPxTextBox ID="ActivitySub"  Caption="Activity Sub" runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="ActivitySub" ValidationSettings-RequiredField-IsRequired="false">                                                                
                            </dx:ASPxTextBox>
                        </div>
                </div>
                 <div class="modal-footer">
                        <label style="float:left;color:red;">* required fields</label>
                 <button type="button" class="btn btn-default" data-dismiss="modal" onclick="ResetFields()">Close</button>
                <dx:ASPxButton ID="ASPxButton10" runat="server" Text="Submit" AutoPostBack="false" ClientInstanceName="addActivityBtn" ClientSideEvents-Click="function (s,e){AddActivity();}" />
              </div>
            </div>
            </div> 
            </div>
         </div>  


   
      <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="addEmployee" style="z-index: 999999999;">
        <div class="vertical-alignment-helper"> 
     <div class="modal-dialog vertical-align-center"> 
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <img class="headerimg_" src="../Content/Images/Icons/comment-user-add-icon.png" />
                <h4 class="modal-title" id="myModalLabel">Create employee</h4>
              </div>
               <div class="modal-body">
                    <dx:ASPxCallbackPanel runat="server" ID="ASPxCallbackPanel2"  ClientInstanceName="EmployeePanel" RenderMode="Table" OnCallback="ASPxCallbackPanel1_Callback">
                 
                <PanelCollection>
                    <dx:PanelContent ID="PanelContent1" runat="server">
                  <div class="profile">
                         <dx:ASPxLabel runat="server" ID="statusEmp" ClientInstanceName="statusEmp" Text="" ForeColor="Red"></dx:ASPxLabel>
                  </div>
                 <div class="profile_tab">
                  
                                <dx:ASPxTextBox ID="EmployeeID" ClientInstanceName="EmployeeID" Caption="User Name" runat="server"  AutoPostBack="false"  ValidationSettings-RequiredField-IsRequired="true" >
                               <ValidationSettings ErrorDisplayMode="ImageWithTooltip" >
                               </ValidationSettings>
                                    <ClientSideEvents Validation="ValidateGeneral" />
                                </dx:ASPxTextBox>
                       </div>
                      
                        <div class="profile_tab">
                                <dx:ASPxTextBox ID="Email" ClientInstanceName="Email" Caption="Email" runat="server"  AutoPostBack="false" ValidationSettings-RequiredField-IsRequired="true">                                                                        
                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip"  >
                        <RegularExpression ErrorText="Please correct your email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                        <RequiredField IsRequired="True"></RequiredField>

                        </ValidationSettings>
                                     <ClientSideEvents Validation="ValidateGeneral" />
                               </dx:ASPxTextBox>
                           </div>  
                        <div class="profile_tab">
                            <dx:ASPxTextBox ID="EmpPassword" Password="true" Caption="Password" runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="EmpPassword1" ValidationSettings-RequiredField-IsRequired="true">                                                                
                            <ClientSideEvents Validation="ValidatePassword1" />
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ErrorText="Pasassword is empty !" >
                               </ValidationSettings>
                            </dx:ASPxTextBox>
                        </div>
                   <div class="profile_tab">
                            <dx:ASPxTextBox ID="EmpPassword2" Password="true" Caption="Retype password" runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="EmpPassword2"  ValidationSettings-RequiredField-IsRequired="true">                                                                
                            <ClientSideEvents Validation="ValidatePassword2" />
                                 <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ErrorText="Retype password is empty !" >
                                     </ValidationSettings>
                                 </dx:ASPxTextBox>
                        </div>
                   <div class="profile_tab" >
                            <dx:ASPxCheckBox Text="Send Email" ID="SendEmail"  runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="SendEmail" Checked="true">                                                                
                            </dx:ASPxCheckBox>
                        </div>
                         <div class="profile_tab" >
                            <dx:ASPxCheckBox Text="Activate" ID="ActivateModal2"  runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="ActivateModal" Checked="true">                                                                
                             </dx:ASPxCheckBox>
                          </div>
                          <div class="profile" >
                            <dx:ASPxCheckBox Text="Lockout" ID="Lockout"  runat="server" Enabled ="true" AutoPostBack="false" ClientInstanceName="Lockout" ClientVisible="false" >                                                                
                             </dx:ASPxCheckBox>
                          </div> 
                         <div>

                              <!-- Nav tabs -->
                              <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active" id="activeTab"><a href="#basic" aria-controls="home" role="tab" data-toggle="tab">Basic Information</a></li>
                                 <li role="presentation" id="profileTab"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Employee Access</a></li>
                                 <li role="presentation"id="hrTab" ><a href="#hr" aria-controls="hr" role="tab" data-toggle="tab">HR Information</a></li>
                                <li role="presentation" id="hrsTab"><a href="#hrs" aria-controls="profile" role="tab" data-toggle="tab">Hourly Rate</a></li>
                               
                              </ul>

                              <!-- Tab panes -->
                              <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="basic">
                                    <div class="tabScroll_cu">
                                                            
                                        <div class="profile_tab">
                                            <dx:ASPxTextBox ID="EmpFName" ClientInstanceName="EmpFName" Caption="First Name" runat="server" AutoPostBack="false" ValidationSettings-RequiredField-IsRequired="true">
                                          <ClientSideEvents Validation="ValidateGeneral"  />
                                             <ValidationSettings ErrorDisplayMode="ImageWithTooltip"  >
                                     </ValidationSettings>
                                        </dx:ASPxTextBox>

                                        </div>
                                        <div class="profile_tab">
                                            <dx:ASPxTextBox ID="EmpLName" ClientInstanceName="EmpLName" Caption="Last Name" runat="server" AutoPostBack="false" ValidationSettings-RequiredField-IsRequired="true">
                                             <ClientSideEvents Validation="ValidateGeneral"  />
                                             <ValidationSettings ErrorDisplayMode="ImageWithTooltip"  >
                                     </ValidationSettings>
                                            </dx:ASPxTextBox>

                                        </div>
                                        <div class="profile_tab">
                                            <dx:ASPxTextBox ID="EmpTitle" ClientInstanceName="EmpTitle" Caption="Title" runat="server" AutoPostBack="false" >
                                            </dx:ASPxTextBox>

                                        </div>
                                        <div class="profile_tab">
                                            <dx:ASPxTextBox ID="EmpStreet" ClientInstanceName="EmpStreet" Caption="Address One" runat="server" AutoPostBack="false">
                                            </dx:ASPxTextBox>

                                        </div>
                                        <div class="profile_tab">
                                            <dx:ASPxTextBox ID="EmpStreet2" ClientInstanceName="EmpStreet2" Caption="Address Two" runat="server" AutoPostBack="false">
                                            </dx:ASPxTextBox>

                                        </div>
                                        <div class="profile_tab">
                                            <dx:ASPxTextBox ID="EmpCity" ClientInstanceName="EmpCity" Caption="City" runat="server" AutoPostBack="false">
                                            </dx:ASPxTextBox>

                                        </div>
                                        <div class="profile_tab">
                                            <dx:ASPxTextBox ID="EmpState" ClientInstanceName="EmpState" Caption="EmpState" runat="server" AutoPostBack="false">
                                            </dx:ASPxTextBox>
                                        </div>
                                        <div class="profile_tab">
                                            <dx:ASPxTextBox ID="EmpZip" ClientInstanceName="EmpZip" Caption="ZipCode" runat="server" AutoPostBack="false">
                                            </dx:ASPxTextBox>
                                        </div>
                                        </div>
                                </div>
                                  <div role="tabpanel" class="tab-pane" id="hrs">
                                  <div class="tabScroll_cu">
                                                         <div class="profile_tab">
                                                                    <dx:ASPxSpinEdit ID="EmpBillRate"    ClientInstanceName="EmpBillRate" Caption="Bill Rate" runat="server" AutoPostBack="false">
                                                                    </dx:ASPxSpinEdit>
                                         </div>
                               
                                        <div class="profile_tab">
                                            <dx:ASPxSpinEdit ID="EmpCostRate"     Caption="Cost Rate" ClientInstanceName="EmpCostRate" runat="server" AutoPostBack="false">
                                           
                                                 </dx:ASPxSpinEdit>
                                        </div>
                                                 <div class="profile_tab">
                                                        <dx:ASPxSpinEdit ID="EmpOTCostRate"      Caption="OT Cost Rate" ClientInstanceName="EmpOTCostRate" runat="server" AutoPostBack="false">
                                                      
                                                              </dx:ASPxSpinEdit>
                                                    </div>
                                                    <div class="profile_tab">
                                                        <dx:ASPxSpinEdit ID="EmpOTBillRate"     Caption="OT BillRate" runat="server" AutoPostBack="false" ClientInstanceName="EmpOTBillRate">
                                                       
                                                             </dx:ASPxSpinEdit>
                                                    </div>
                                  </div>
                          
                                  </div>
                                <div role="tabpanel" class="tab-pane" id="hr">
                                    <div class="tabScroll_cu">
                                                                 <div class="profile_tab">
                                                                    <dx:ASPxDateEdit ID="EmpDateHired" Caption="Hiring Date" runat="server" AutoPostBack="false" ClientInstanceName="EmpDateHired">
                                                                    </dx:ASPxDateEdit>
                                                                </div>
                                                                <div class="profile_tab">
                                                                    <dx:ASPxTextBox ID="EmpContact" Caption="Contact" runat="server" AutoPostBack="false" ClientInstanceName="EmpContact">
                                                                    </dx:ASPxTextBox>
                                                                </div>
                                                                <div class="profile_tab">
                                                                    <dx:ASPxTextBox ID="EmpDepartment" Caption="Department" runat="server" AutoPostBack="false" ClientInstanceName="EmpDepartment">
                                                                    </dx:ASPxTextBox>
                                                                </div>
                                                      
                                                                <div class="profile_tab">
                                                                    <dx:ASPxTextBox ID="EmpMemo" Caption="Memo" runat="server" AutoPostBack="false" ClientInstanceName="EmpMemo" ClientVisible="false">
                                                                    </dx:ASPxTextBox>
                                                                </div>
                                                      
                                                                <div class="profile_tab">
                                                                    <dx:ASPxDateEdit ID="EmpNextImpDate" Caption="Visa Date" runat="server" AutoPostBack="false" ClientInstanceName="EmpNextImpDate">
                                                                    </dx:ASPxDateEdit>
                                                                </div>
                                                                <div class="profile_tab">
                                                                    <dx:ASPxSpinEdit ID="EmpVac" Caption="Vacations" runat="server" AutoPostBack="false" ClientInstanceName="EmpVac">
                                                                    </dx:ASPxSpinEdit>
                                                                </div>
                                                                <div class="profile_tab">
                                                                    <dx:ASPxSpinEdit DisplayFormatString="N2" ID="EmpSick" Caption="Sick" runat="server" AutoPostBack="false" ClientInstanceName="EmpSick">
                                                                      
                                                                    </dx:ASPxSpinEdit>
                                                                </div>
                                                                <div class="profile_tab">
                                                                    <dx:ASPxSpinEdit ID="EmpHol" Caption="Holidays" runat="server" AutoPostBack="false" ClientInstanceName="EmpHol">
                                                                    </dx:ASPxSpinEdit>
                                                                </div>     
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="profile">      
                                             <div class="tabScroll_cu">
                                                      
                                                    <div class="profile">
                                                      <dx:ASPxComboBox ID="managers" Caption="Manager" runat="server"  AllowNull="False" AutoPostBack="false" ClientInstanceName="managers" ValidationSettings-RequiredField-IsRequired="true" >
                                                        <ClearButton Visibility="Auto">
                                                        </ClearButton>
                                                          <ClientSideEvents Validation="ValidateGeneral"  />
                                                             <ValidationSettings ErrorDisplayMode="ImageWithTooltip"  >
                                                     </ValidationSettings>                                                           
                                                      </dx:ASPxComboBox>
                                                    </div>
                                                   
                                                    <div> 
                                                    <div class="profile_tab">
                                                        <dx:ASPxListBox ID="roles" runat="server" Width="66%" Height="140px" SelectionMode="Multiple" Caption="Roles" ClientInstanceName="roles" >
                                                             <CaptionSettings Position="Top" />
                                                        </dx:ASPxListBox>
                                                    </div>
                                                         
                                                </div>
                                                       
                            </div>
                                </div>
     
                              </div>

                            </div>
                     </dx:PanelContent>
                </PanelCollection>
                </dx:ASPxCallbackPanel>
              </div>
              <div class="modal-footer">
                  <label style="float:left;color:red;">* required fields </label>
                    <dx:ASPxLabel runat="server" ID="ASPxLabel6" ClientInstanceName="status_employee" ClientVisible="false"  CssClass="project_save_status"></dx:ASPxLabel>
                 <button type="button" class="btn btn-default" data-dismiss="modal" onclick="ResetFieldsEmployee();">Close</button>
                <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Submit" AutoPostBack="false" ClientInstanceName="addEmployee"
                     ClientSideEvents-Click="function (s,e){addEmployeeFunctionOptions();}" />
              </div>
            </div>
            </div> 
            </div>
       </div>
       
    <script type="text/javascript">
        // <![CDATA[
        ASPxClientControl.GetControlCollection().ControlsInitialized.AddHandler(function (s, e) {

            UpdateGridHeight();
        });
        ASPxClientControl.GetControlCollection().BrowserWindowResized.AddHandler(function (s, e) {

            UpdateGridHeight();
        });

        // ]]> 
        </script>
    <!-- add new project modal -->
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="ProjectModal">
        <div class="vertical-alignment-helper"> 
     <div class="modal-dialog vertical-align-center"> 
            <div class="modal-content" style="width:80%;">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span style="color: #0e0e0e;font-size: 26px;" aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="H2">
                    <dx:ASPxImage ID="ASPxImage2" runat="server" ImageUrl="../Content/Images/Icons/Write-Document-icon.png" />
                     <dx:ASPxLabel Text="" runat="server" ID="ProjectNameHead" ClientInstanceName="ProjectNameHead" ForeColor="White">
                     </dx:ASPxLabel>
                </h4>
              </div>
                <div class="modal-body">
                      <div class="profile">
                         <dx:ASPxLabel runat="server" ID="StatusProject" ClientInstanceName="StatusProject" Text="" ForeColor="Red" ></dx:ASPxLabel>
                  </div>
                <dx:ASPxCallbackPanel runat="server" ID="ASPxCallbackPanel1"  ClientInstanceName="CallbackPanel" RenderMode="Table" OnCallback="ASPxCallbackPanel1_Callback">
                <ClientSideEvents EndCallback="OnEndCallback" >
                </ClientSideEvents>
                <PanelCollection>
                    <dx:PanelContent ID="PanelContent3" runat="server">
                       
                        <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0"  ClientInstanceName="ASPxPageControl1" TabAlign="left" Width="100%" TabPosition="Left" >
                           <Paddings Padding="0"/> 
                            <TabPages>
                                <dx:TabPage Text="Basic Information">
                                    <ContentCollection>
                                        <dx:ContentControl ID="ContentControl1" runat="server">
                                            <div class="tabScroll">
                                        
                                                <div class="profile_tab">
                                                    <dx:ASPxTextBox ID="EditProjectID" ClientInstanceName="EditProjectID" Caption="Project ID" runat="server" AutoPostBack="false">
                                                                <ValidationSettings SetFocusOnError="True"  ErrorTextPosition="Bottom">
                                                                    <RequiredField IsRequired="True" />
                                                                </ValidationSettings>
                                                        <ClientSideEvents KeyUp="ProjectIDKeyUp" Validation="function(s,e){
                                                 
                                e.errorText = 'ProjectID is required'
                                e.isValid = (EditProjectID.GetText()  != '' );
                                                }
                                                " />
                                                                </dx:ASPxTextBox>
                                                </div>
                                                <div class="profile_tab">
                                                    <dx:ASPxTextBox ID="EditProjectCode" ClientInstanceName="EditProjectCode" Caption="Project Code" runat="server" AutoPostBack="false">
                                                                <ValidationSettings SetFocusOnError="True" ErrorTextPosition="Bottom">
                                                                    <RequiredField IsRequired="True" />
                                                                </ValidationSettings>
                                                         <ClientSideEvents Validation="function(s,e){
                                                 
                                e.errorText = 'ProjectCode is required'
                                e.isValid = (EditProjectCode.GetText()  != '' );
                                                }
                                                " />
                                                                </dx:ASPxTextBox>
                                                </div>
                                             <div class="profile_tab">
                                         <dx:ASPxDropDownEdit ID="DropDownEdit" runat="server" ClientInstanceName="DropDownEdit"
        Width="170px" AllowUserInput="False" AnimationType="None" Caption="Parent Project">
        <DropDownWindowStyle>
            <Border BorderWidth="0px" />
        </DropDownWindowStyle>
        <ClientSideEvents  DropDown="OnDropDown" />
        <DropDownWindowTemplate>
            <div><%--OnCustomJSProperties="TreeList_CustomJSProperties"--%>
                <dx:ASPxTreeList ID="TreeList" ClientInstanceName="TreeList" runat="server"
                    Width="500px"   
                    KeyFieldName="ProjectID" ParentFieldName="ParentProjectID" OnInit="TreeList_Init">
                    <Settings VerticalScrollBarMode="Auto" ScrollableHeight="150" />
                    <ClientSideEvents FocusedNodeChanged="function(s,e){ selectButton.SetEnabled(true); }" />
                    <BorderBottom BorderStyle="Solid" />
                    <SettingsBehavior AllowFocusedNode="true" AutoExpandAllNodes="true" FocusNodeOnLoad="false" />
                    <SettingsPager Mode="ShowAllNodes">
                    </SettingsPager>
                    <Styles>
                        <Node Cursor="pointer">
                        </Node>
                        <Indent Cursor="default">
                        </Indent>
                    </Styles>
                    <Columns>
                        <dx:TreeListTextColumn FieldName="ProjectID" VisibleIndex="1">
                        </dx:TreeListTextColumn>
                        <dx:TreeListTextColumn FieldName="ProjectCode" VisibleIndex="2">
                        </dx:TreeListTextColumn>
                        <dx:TreeListDateTimeColumn FieldName="ProjectName" VisibleIndex="3">
                        </dx:TreeListDateTimeColumn>
                    </Columns>
                </dx:ASPxTreeList>
            </div>
            <table style="background-color: White; width: 100%;">
                <tr>
                    <td style="padding: 10px;">
                        <dx:ASPxButton ID="clearButton" ClientEnabled="false" ClientInstanceName="clearButton"
                            runat="server" AutoPostBack="false" Text="Clear">
                            <ClientSideEvents Click="ClearSelection" />
                        </dx:ASPxButton>
                    </td>
                    <td style="text-align: right; padding: 10px;">
                        <dx:ASPxButton ID="selectButton" ClientEnabled="false" ClientInstanceName="selectButton"
                            runat="server" AutoPostBack="false" Text="Select">
                            <ClientSideEvents Click="UpdateSelection" />
                        </dx:ASPxButton>
                        <dx:ASPxButton ID="closeButton" runat="server" AutoPostBack="false" Text="Close">
                            <ClientSideEvents Click="function(s,e) { DropDownEdit.HideDropDown(); }" />
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </DropDownWindowTemplate>
    </dx:ASPxDropDownEdit>
                                            </div>
                                             <div class="profile_tab">
                                                            <dx:ASPxComboBox ID="EditClientID" Caption="Client" runat="server"  TextField="ClientID" ValueField="ClientID" AllowNull="False" AutoPostBack="false" ClientInstanceName="EditClientID" ValidationSettings-RequiredField-IsRequired="false" >                                                         
                                                        <ClearButton Visibility="Auto">
                                                        </ClearButton>                                                           
                                                        </dx:ASPxComboBox>
                                                    </div>    
                                                 <div class="profile_tab">
                                                            <dx:ASPxComboBox ID="EditEmployeeID" Caption="Manager" runat="server"  AllowNull="False" AutoPostBack="false" ClientInstanceName="EditEmployeeID" ValidationSettings-RequiredField-IsRequired="false" >                                                         
                                                        <ClearButton Visibility="Auto">
                                                        </ClearButton>                                                           
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                
                                                <div class="profile_tab">
                                                    <dx:ASPxTextBox ID="EditProjectPhase" ClientInstanceName="EditProjectPhase" Caption="Project Phase" runat="server" AutoPostBack="false">
                                                                 
                                                                </dx:ASPxTextBox>
                                                </div>
                                                <div class="profile_tab">
                                                    <dx:ASPxTextBox ID="EditProjectName" ClientInstanceName="EditProjectName" Caption="Project Name" runat="server" AutoPostBack="false">
                                                                 
                                                                </dx:ASPxTextBox>
                                                </div>
                                               
                                              
                                                <div class="profile_tab">
                                                    <dx:ASPxDateEdit ID="EditProjectStartDate" ClientInstanceName="EditProjectStartDate" Caption="Start Date" runat="server" AutoPostBack="false">
                                                                </dx:ASPxDateEdit>
                                                </div>
                                                <div class="profile_tab">
                                                    <dx:ASPxComboBox ID="EditProjectStatus" ClientInstanceName="EditProjectStatus" Caption="Project Status" runat="server" AutoPostBack="false" ValueField="Status" TextField="Status">
                                                    </dx:ASPxComboBox>
                                                </div>
                                                 <div class="profile_tab">
                                                    <dx:ASPxComboBox ID="EditProjectConType" ClientInstanceName="EditProjectConType" Caption="Contract Type" runat="server" AutoPostBack="false" ValueField="ContractType" TextField="ContractType">
                                                     </dx:ASPxComboBox>
                                                </div>
                                                <div class="profile_tab">
                                                    <dx:ASPxTextBox ID="EditFSID" ClientInstanceName="EditFSID"  Caption="FSID" runat="server" AutoPostBack="false">
                                                                </dx:ASPxTextBox>
                                                </div>
                                                  <div class="profile_tab">
                                                    <dx:ASPxTextBox ID="EditProjectMemo"  ClientInstanceName="EditProjectMemo" Caption="Project Memo" runat="server" AutoPostBack="false" ClientVisible="false" >
                                                                </dx:ASPxTextBox>
                                                </div>
                                          
                                            
                                            </div>
                                        </dx:ContentControl>
                                    </ContentCollection>
                                </dx:TabPage>
                                 <dx:TabPage Text="Contacts Info">
                                    <ContentCollection>
                                        <dx:ContentControl ID="ContentControl3" runat="server" >
                                            <div class="tabScroll">
                                             <div class="profile_tab">
                                                    <dx:ASPxTextBox ID="EditProjectStreet" ClientInstanceName="EditProjectStreet" Caption="Street" runat="server" AutoPostBack="false">
                                                                </dx:ASPxTextBox>
                                                </div>
                                                <div class="profile_tab">
                                                    <dx:ASPxTextBox ID="EditProjectCity" ClientInstanceName="EditProjectCity" Caption="Project City" runat="server" AutoPostBack="false">
                                                                </dx:ASPxTextBox>
                                                </div>
                                                <div class="profile_tab">
                                                    <dx:ASPxTextBox ID="EditProjectState" ClientInstanceName="EditProjectState" Caption="Project State" runat="server" AutoPostBack="false">
                                                                </dx:ASPxTextBox>
                                                </div>
                                                <div class="profile_tab">
                                                    <dx:ASPxTextBox ID="EditProjectZip" ClientInstanceName="EditProjectZip" Caption="Project Zip" runat="server" AutoPostBack="false">
                                                                </dx:ASPxTextBox>
                                                </div>
                                                 <div class="profile_tab">
                                                    <dx:ASPxComboBox DropDownStyle="DropDown"   ID="EditProjectOther1" ClientInstanceName="EditProjectOther1" Caption="ARC Team Leader" runat="server" AutoPostBack="false">
                                                                </dx:ASPxComboBox>
                                                </div>
                                                 <div class="profile_tab">
                                                    <dx:ASPxComboBox  DropDownStyle="DropDown"    ID="EditProjectOther2" ClientInstanceName="EditProjectOther2" Caption="Structural Manager" runat="server" AutoPostBack="false">
                                                                </dx:ASPxComboBox>
                                                </div>
                                                 <div class="profile_tab">
                                                    <dx:ASPxComboBox  DropDownStyle="DropDown"    ID="EditProjectOther3" ClientInstanceName="EditProjectOther3" Caption="STR Team Leader" runat="server" AutoPostBack="false">
                                                                </dx:ASPxComboBox>
                                                </div>
                                                 <div class="profile_tab">
                                                    <dx:ASPxComboBox  DropDownStyle="DropDown"    ID="EditProjectOther4" ClientInstanceName="EditProjectOther4" Caption="MEP Manager" runat="server" AutoPostBack="false">
                                                                </dx:ASPxComboBox>
                                                </div>

                                             </div>
                                        </dx:ContentControl>
                                    </ContentCollection>
                                </dx:TabPage>
                                <dx:TabPage Text="Activities">
                                    <ContentCollection>
                                        <dx:ContentControl ID="ContentControl2" runat="server">
                                            <div class="tabScroll">
                                            <table style="width: 100%" class="Activities_table">
                                                <tr>
                                                    <td class="first">
                                                       <div style="float:left;width: 83%;">
                                                         <dx:ASPxGridView ID="lbAvailable" ClientInstanceName="lbAvailable" runat="server" 
                                                        KeyFieldName="ActivityID" Width="100%"  >
                                                            <Columns>
                                                                <dx:GridViewCommandColumn Width="6%" ShowSelectCheckbox="True" VisibleIndex="0" Caption="Included">
                                                                </dx:GridViewCommandColumn>
                                                                <dx:GridViewDataColumn FieldName="ActivityID" VisibleIndex="1" Width="50px" />
                                                                <dx:GridViewDataColumn FieldName="ActivityCode" VisibleIndex="1" Width="50px" />
                                                           
                                                            </Columns>
                                                             <SettingsPager Visible="false"></SettingsPager>
                                                             <Settings VerticalScrollableHeight="160" VerticalScrollBarMode="Visible" />
                                                             <SettingsSearchPanel Visible="true"  />
                                                            <ClientSideEvents SelectionChanged="grid_SelectionChanged" />
                                                        </dx:ASPxGridView>
                                                       </div>
                                                         <div style="float: left;width: 18%;">
                                                      
                                                              <button type="button" class="btn btn-primary btn-lg modal_btn" data-toggle="modal" data-target="#addActivity" style="float:left;font-size:inherit;padding-bottom:20px;">
                                                                     Add new activity
                                                                    </button>

                                                       </div>
                                                        
                                                    </td>
                                                   
                                                    <td class="last">
                                                       
                                                        <dx:ASPxListBox ID="lbChoosen" runat="server" ClientInstanceName="lbChoosen" Width="100%"
                                                                        Height="240px" SelectionMode="Single"  TextField="ActivityID" ValueField="ActivityID">
                                                            <CaptionSettings Position="Top" />
                                                            <Columns>
                                                                <dx:ListBoxColumn Caption="Included Activities" FieldName="ActivityID" Name="ActivityID" />
                                                            </Columns>
                                                            
                                                        </dx:ASPxListBox>
                                                    </td>
                                                </tr>
                                            </table>
                                           
                                            </div>
                                        </dx:ContentControl>
                                    </ContentCollection>
                                </dx:TabPage>
                                <dx:TabPage Text="Employees">
                                    <ContentCollection>
                                        <dx:ContentControl ID="ContentControl5" runat="server" >
                                            <div class="tabScroll">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxGridView ID="ASPxGridView1"  runat="server" AutoGenerateColumns="False" 
                                                            OnCellEditorInitialize="grid_CellEditorInitialize" Width="100%" ClientInstanceName="grid" 
                                                            KeyFieldName="EmployeeControlID" OnAfterPerformCallback="ASPxGridView1_AfterPerformCallback" 
                                                            OnRowInserting="ASPxGridView1_RowInserting" OnRowDeleting="ASPxGridView1_RowDeleting" 
                                                            OnRowUpdating="ASPxGridView1_RowUpdating" OnCustomCallback="ASPxGridView1_CustomCallback" 
                                                           OnHtmlRowPrepared="ASPxGridView1_HtmlRowPrepared" >
                                                            <Settings UseFixedTableLayout="True" />
                                                             <Settings VerticalScrollBarMode="Visible" VerticalScrollableHeight="250" /> 
                                                            <Columns>
                                                                <dx:GridViewCommandColumn ShowNewButtonInHeader="True" VisibleIndex="0" ShowEditButton="True" ShowDeleteButton="True">
                                                                </dx:GridViewCommandColumn>
                                                                <dx:GridViewDataComboBoxColumn    FieldName="EmployeeID" Name="EmployeeID"   VisibleIndex="1" >
                                                                    <EditFormSettings Caption="Employee" ColumnSpan="1" />
                                                                    <PropertiesComboBox AllowNull="False" TextField="EmployeeID" ValueField="EmployeeID" ClientInstanceName="cmbnew">
                                                                      
                                                                        
                                                                        <ValidationSettings SetFocusOnError="false">
                                                                            <RequiredField IsRequired="true" />
                                                                        </ValidationSettings>
                                                                     </PropertiesComboBox>
                                                                     
                                                                     <EditItemTemplate>
                                                                         
                                                                        <table class="EditEmployeeControl">
                                                                            <tr>
                                                                               
                                                                                <td class="second" style="width: 200px;padding: 0px;float: left;">
                                                                                  <dx:ASPxComboBox   OnInit="newEmp_Init"  FieldName="EmployeeID"  TextField="EmployeeID" ValueField="EmployeeID" ID="newEmp" runat="server" 
                                                                                      Value='<%# GetText(Container)  %>'   ValueType="System.String" ValidationSettings-RequiredField-IsRequired="true" ></dx:ASPxComboBox>
                                                                      
                                                                                  </td>
                                                                                <td style="cursor:pointer;"> <i title="Add Employee" onclick="addEmployeeView()" class="fa fa-plus-square" aria-hidden="true" style="color: #2f71a9;font-size: 22px;"></i></td>
                                                                            </tr>
                                                                        </table>
                                                                        
                                                                    </EditItemTemplate>
                                                                 
                                                                     <HeaderCaptionTemplate>
                                                                <table>
                                                                    <tr>
                                                                    <th><i class="fa fa-users employees_i" aria-hidden="true" ></i></th>
                                                                    <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Employees" ForeColor="White"/></th>
                                                                    </tr>
                                                                </table>
                                                                 </HeaderCaptionTemplate>
                                                                </dx:GridViewDataComboBoxColumn>
                                                                <dx:GridViewDataComboBoxColumn FieldName="ControlID" Name="ActivityID" Caption="Activities" VisibleIndex="2">
                                                                    <PropertiesComboBox AllowNull="False" TextField="ControlID" ValueField="ControlID">
                                                                        <ClearButton Visibility="Auto">
                                                                        </ClearButton>
                                                                        <ValidationSettings SetFocusOnError="True">
                                                                            <RequiredField IsRequired="True" />
                                                                        </ValidationSettings>
                                                                    </PropertiesComboBox>
                                                                    <HeaderCaptionTemplate>
                                                                <table>
                                                                    <tr>
                                                                    <th><i class="fa fa-tasks employees_i" aria-hidden="true"></i></th>
                                                                    <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Tasks" ForeColor="White"/></th>
                                                                    </tr>
                                                                </table>
                                                                 </HeaderCaptionTemplate>
                                                                </dx:GridViewDataComboBoxColumn>
                                                                <dx:GridViewDataSpinEditColumn Caption="Bill Rate" VisibleIndex="3" FieldName="EmpBillRate" ReadOnly="True" >
                                                                 <EditFormSettings Visible="True" />
                                                                     <HeaderCaptionTemplate>
                                                                <table>
                                                                    <tr>
                                                                    <th><i class="fa fa-usd employees_i" aria-hidden="true"></i></th>
                                                                    <th><dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Bill Rate" ForeColor="White"/></th>
                                                                    </tr>
                                                                </table>
                                                                 </HeaderCaptionTemplate>
                                                                     </dx:GridViewDataSpinEditColumn>
                                                                <dx:GridViewDataSpinEditColumn Caption="Cost Rate" FieldName="EmpCostRate" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="4">
                                                                <EditFormSettings Visible="True" />
                                                                    <HeaderCaptionTemplate>
                                                                <table>
                                                                    <tr>
                                                                    <th><i class="fa fa-usd employees_i" aria-hidden="true"></i></th>
                                                                    <th><dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Cost Rate" ForeColor="White"/></th>
                                                                    </tr>
                                                                </table>
                                                                 </HeaderCaptionTemplate>
                                                                </dx:GridViewDataSpinEditColumn>
                                                                <dx:GridViewDataSpinEditColumn Caption="OT Cost Rate" FieldName="EmpOTCostRate" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="5">
                                                             <EditFormSettings Visible="True" />
                                                                      <HeaderCaptionTemplate>
                                                                <table>
                                                                    <tr>
                                                                    <th><i class="fa fa-usd employees_i" aria-hidden="true"></i></th>
                                                                    <th><dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="OT Cost Rate" ForeColor="White"/></th>
                                                                    </tr>
                                                                </table>
                                                                 </HeaderCaptionTemplate>
                                                                     </dx:GridViewDataSpinEditColumn>
                                                                <dx:GridViewDataSpinEditColumn Caption="OT Bill Rate" FieldName="EmpOTBillRate" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="6">
                                                               <EditFormSettings Visible="True" />
                                                                     <HeaderCaptionTemplate>
                                                                <table>
                                                                    <tr>
                                                                    <th><i class="fa fa-usd employees_i" aria-hidden="true"></i></th>
                                                                    <th><dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="OT Bill Rate" ForeColor="White"/></th>
                                                                    </tr>
                                                                </table>
                                                                </HeaderCaptionTemplate>
                                                                </dx:GridViewDataSpinEditColumn>
                                                                <dx:GridViewDataSpinEditColumn FieldName="EmployeeControlID" Name="EmployeeControlID" ShowInCustomizationForm="True" Visible="False" VisibleIndex="7">
                                                                </dx:GridViewDataSpinEditColumn>
                                                            </Columns>
                                                             
                                                        </dx:ASPxGridView>
                                                    </td>
                                                </tr>
                                                
                                            </table>
                                             </div>
                                        </dx:ContentControl>
                                    </ContentCollection>
                                </dx:TabPage>
                            </TabPages>
                        </dx:ASPxPageControl>
                    </dx:PanelContent>
                </PanelCollection>
                </dx:ASPxCallbackPanel>
                </div>
                 <div class="modal-footer">
                     <label style="float:left;color:red;">* required fields</label>
                    
                 <button type="button" class="btn btn-default" data-dismiss="modal"  onclick="ResetFieldsProject();">Close</button>
                     
                <dx:ASPxButton ID="ASPxButton11" runat="server" Text="Save" AutoPostBack="false" ClientInstanceName="SaveProjectData" ClientSideEvents-Click="function (s,e){SaveAllProjectData();}" />
              </div>
            </div>
            </div> 
            </div>
    </div>


    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="InfoModal">
        <div class="vertical-alignment-helper"> 
     <div class="modal-dialog vertical-align-center"> 
            <div class="modal-content" style="width:80%;">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span style="color: #0e0e0e;font-size: 26px;" aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="H1">
                    <dx:ASPxImage ID="ASPxImage3" runat="server" ImageUrl="../Content/Images/Icons/Write-Document-icon.png" />
                     <dx:ASPxLabel Text="" runat="server" ID="ASPxLabel7" ClientInstanceName="InfoProjectNameHead" ForeColor="White">
                     </dx:ASPxLabel>
                                    
                                       
                </h4>
              </div>
                
                 <div class="modal-footer">
                     <label style="float:left;color:red;">* required fields</label>
                    <dx:ASPxLabel runat="server" ID="ASPxLabel11" ClientInstanceName="statusProjectModal" Text=""   CssClass="project_save_status"></dx:ASPxLabel>
                 <button type="button" class="btn btn-default" data-dismiss="modal"  onclick="ResetFieldsProject();">Close</button>
                     
                <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Save" AutoPostBack="false" ClientInstanceName="SaveProjectData" ClientSideEvents-Click="function (s,e){SaveAllProjectData();}" />
              </div>
            </div>
            </div> 
            </div>
    </div>

</asp:Content>