﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DXOmniVistaTimeEngine;
using System.Web.Security;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using WcfOmniVistaTimeRESTfulJSonService;

public partial class Dashboard : System.Web.UI.Page
{

    DataTable dt = new DataTable();
    public string projectID = "Undefined";
    public string projectName = "Undefined";
    public string projectConAmt = "0";
    public string percentComplete = "0";

    protected void Page_Load(object sender, EventArgs e)
    {
        string idParameter = comboProjectId.Text;
        string projectId = (string.IsNullOrEmpty(idParameter)) ? "0" : idParameter;
        JsonDataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        DataTable dt2 = JsonDataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["Project"], "");
        DataRow[] projectInfo = dt2.Select("MasterProjectID = '" + projectId + "'");

        if (projectInfo.Length > 0 && projectInfo[0].ItemArray.Length == 4)
        {
            projectID = projectInfo[0].ItemArray[0].ToString();
            projectName = projectInfo[0].ItemArray[1].ToString();
            projectConAmt = String.Format("{0:n}", decimal.Parse(projectInfo[0].ItemArray[2].ToString()));
            percentComplete = projectInfo[0].ItemArray[3].ToString();
        }
        else
        {
            projectID = "Undefined";
            projectName = "Undefined";
            projectConAmt = "0";
            percentComplete = "0";
        }

        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        this.comboProjectId.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["Project"], string.Empty);
        comboProjectId.DropDownStyle = DevExpress.Web.DropDownStyle.DropDownList;
        comboProjectId.ValueField = "MasterProjectID";
        comboProjectId.TextField = "MasterProjectID";
        //comboBox.ValueType = typeof(Int32);
        //comboBox.Columns.Add(textField);
        //comboBox.Columns.Add(valueField);
        comboProjectId.DataBind();

        WebRequest request = WebRequest.Create("http://eap-omni-prod01/WCFOVTimeJSon/RestServiceImpl.svc/project") as HttpWebRequest;
        WebResponse response = request.GetResponse();

        using (Stream stream = response.GetResponseStream())
        {
            Type serializationTargetType = typeof(List<Project>);
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(serializationTargetType);

            List<Project> jsonDeserialized = (List<Project>)jsonSerializer.ReadObject(stream);

            dt = new DataTable();
            dt.Columns.Add("Group", typeof(string));
            dt.Columns.Add("ProjectId", typeof(string));
            dt.Columns.Add("ProjectName", typeof(string));

            dt.PrimaryKey = new DataColumn[] { dt.Columns["ProjectId"] };

            foreach (Project p in jsonDeserialized)
            {
                DataRow dr = dt.NewRow();
                dr[0] = "Project Detail";
                dr[1] = p.ProjectId;
                dr[2] = p.ProjectName;
                dt.Rows.Add(dr);
            }

            if (dt.Rows.Count == 0)
            {
                DataRow dr = dt.NewRow();
                dr[0] = "Project Detail";
                dr[1] = "No Project Data";
                dr[2] = "No Project Data";
                dt.Rows.Add(dr);
            }

        }

        NavBarDataBind();


        
    }

    protected void LeftPane_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
        //txtBox.Value = "records may need refresh";
        //NavBarDataBind();

    }

    private void NavBarDataBind()
    {
        //create groups:

        string currentGroup = dt.Rows[0].ItemArray[0].ToString();
        DevExpress.Web.NavBarGroup gr = new DevExpress.Web.NavBarGroup();
        gr.Text = currentGroup;
        gr.Name = currentGroup;
        navbar.Groups.Add(gr);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string rowGroup = dt.Rows[i].ItemArray[0].ToString();
            if (rowGroup != currentGroup)
            {
                DevExpress.Web.NavBarGroup rowGr = new DevExpress.Web.NavBarGroup();
                rowGr.Text = rowGroup;
                rowGr.Name = rowGroup;
                navbar.Groups.Add(rowGr);
                currentGroup = rowGroup;
            }
        }

        //fill items:


        //test data for view only
        DevExpress.Web.NavBarItem it1 = new DevExpress.Web.NavBarItem();
        string result1 = "<div class='Content'><div class='LeftPanel'><div class='Title'> <img class='imgProject' />" + "Project ID" + "</div>";
        result1 += "<div id='CallbackTime' class='ValueSmall'><span>" + projectID  + "</span><span></span></div></div></div>";
        it1.Text = result1;
        navbar.Groups[0].Items.Add(it1);

        it1 = new DevExpress.Web.NavBarItem();
        result1 = "<div class='Content'><div class='LeftPanel'><div class='Title'>" + "Project Name" + "</div>";
        result1 += "<div id='CallbackTime' class='ValueSmall'><span>" + projectName + "</span><span></span></div></div></div>";
        it1.Text = result1;
        navbar.Groups[0].Items.Add(it1);

        it1 = new DevExpress.Web.NavBarItem();
        result1 = "<div class='Content'><div class='LeftPanel'><div class='Title'>" + "Contract Amt" + "</div>";
        result1 += "<div id='CallbackTime' class='ValueSmall'><span>" + projectConAmt + "</span><span></span></div></div></div>";
        it1.Text = result1;
        navbar.Groups[0].Items.Add(it1);

        it1 = new DevExpress.Web.NavBarItem();
        result1 = "<div class='Content'><div class='LeftPanel'><div class='Title'>" + "Percent Complete" + "</div>";
        result1 += "<div id='CallbackTime' class='ValueSmall'><span>" + percentComplete + "</span><span>%</span></div></div></div>";
        it1.Text = result1;
        navbar.Groups[0].Items.Add(it1);

        return;


        //skip for now----new fields need to be added to pull additional items per project
        for (int j = 0; j < dt.Rows.Count; j++)
        {
            string gr1 = dt.Rows[j].ItemArray[0].ToString();
            DevExpress.Web.NavBarGroup navBarGr = navbar.Groups.FindByName(gr1);
            if (navBarGr != null)
            {
                DevExpress.Web.NavBarItem it = new DevExpress.Web.NavBarItem();

                //string result = "<a href=\"?id=" + dt.Rows[j].ItemArray[1].ToString() + "\"><div class='Content'><div class='LeftPanel'><div class='Title'>" + dt.Rows[j].ItemArray[2].ToString() + "</div></a>";
                string result = "<div class='Content'><div class='LeftPanel'><div class='Title'>" + dt.Rows[j].ItemArray[1].ToString() + "</div>";
                result += "<div id='CallbackTime' class='Value'><span>" + dt.Rows[j].ItemArray[2].ToString() + "</span><span>hrs</span></div></div></div>";


                it.Text = result;

                navbar.Groups[navBarGr.Index].Items.Add(it);
            }
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }
    protected void comboProjectId_TextChanged(object sender, EventArgs e)
    {
        DevExpress.Web.ASPxComboBox comboBox = (DevExpress.Web.ASPxComboBox)sender;
        ASPxCallbackPanel1.JSProperties["cpProjectID"] = comboBox.Text;
    }
}

public class Project
{
    public string ProjectId { get; set; }
    public string ProjectName { get; set; }
}
