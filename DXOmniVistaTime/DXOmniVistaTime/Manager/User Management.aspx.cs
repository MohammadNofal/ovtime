﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Text.RegularExpressions;
using DXOmniVistaTimeEngine;
using System.Web.Security;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using DevExpress.Web.Data;
using DevExpress.Web;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

public partial class Users : System.Web.UI.Page
{
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
       
     ////just for testing
      
         if (!IsCallback)
        {
            if (Session["GridDataUser"] == null || (this.RoleCombobox.Value == null && this.UserNameComboBox.Value == null))
            {
                SessionUpdate();
            } 
            this.LoginGridView.DataSource = (DataTable)Session["GridDataUser"]; 
            this.RoleCombobox.DataSource = CustomRoles.GetActiveRoles(); 
            this.UserNameComboBox.DataSource = (List<string>)Session["UserNames"];
            this.managers.DataSource = Roles.GetUsersInRole("Manager");
            DataBind();  
        }
        else
        {
            this.managers.DataSource = Roles.GetUsersInRole("Manager");
            this.LoginGridView.DataSource = (DataTable)Session["GridDataUser"];
            DataBind();

        }
    }
    private string[] CustomGetRoles()
    {
       string  user = Membership.GetUser(User.Identity.Name).UserName;

       string[] roles = Roles.GetAllRoles();
        List<string> array = new  List<string>();
        foreach(string role in roles){
            if(Roles.IsUserInRole(user,role)){
                array.Add(role);
            }
        }
        return array.ToArray();
    }
     
    protected void FilterPanel_Callback(object sender, CallbackEventArgsBase e)
    {
        this.RoleCombobox.DataSource = CustomRoles.GetActiveRoles();
        //SessionUpdate();
       this.UserNameComboBox.DataSource = (List<string>)Session["UserNames"];
       DataBind();
    }

    protected void userdetailpanel_Callback(object sender, CallbackEventArgsBase e)
    {
        string role = this.RoleCombobox.Value == null ? String.Empty : this.RoleCombobox.Value.ToString();
        string user = this.UserNameComboBox.Value == null ? String.Empty : this.UserNameComboBox.Value.ToString();
        SessionUpdate();
        if (role != String.Empty)
        {
            DataRow[] row = ((DataTable)Session["GridDataUser"]).Select("Role = '" + role + "'");
            if (row.Count() != 0)
                Session["GridDataUser"] = row.CopyToDataTable();
            else
                Session["GridDataUser"] = null;
        }
        if (user != String.Empty && Session["GridDataUser"] != null)
        {
            DataRow[] row = ((DataTable)Session["GridDataUser"]).Select("UserName = '" + user + "'");
            if (row.Count() != 0)
                Session["GridDataUser"] = row.CopyToDataTable();
            else
                Session["GridDataUser"] = null;
        }

        if (Session["GridDataUser"] != null)
        {
            DataTable _dt;
            _dt = (DataTable)Session["GridDataUser"];
            DataColumn[] columns = new DataColumn[1];
            columns[0] = _dt.Columns["UserId"];

            _dt.PrimaryKey = columns;
            Session["GridDataUser"] = _dt;
        }
        this.LoginGridView.DataSource = (DataTable)Session["GridDataUser"];
        DataBind();
    }
    
    protected void LoginGridView_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (this.LoginGridView.IsNewRowEditing && e.Column.FieldName == "UserName")
        {
            ASPxTextBox _UserName = (ASPxTextBox)e.Editor;
            _UserName.ReadOnly = false;
        }
        if (e.Column.FieldName == "Role")
        {
            ASPxComboBox _role = (ASPxComboBox)e.Editor;
            _role.DataSource = CustomRoles.GetActiveRoles();
            _role.DataBind();
        }
        if (e.Column.FieldName == "ManagerID")
        {
            ASPxComboBox _manager = (ASPxComboBox)e.Editor;
            _manager.DataSource = Roles.GetUsersInRole("Manager");
            _manager.DataBind();
        }
        if (e.Column.FieldName.Contains("Emp"))
        {
            return;
        }
    }

    protected void LoginGridView_RowInserting(object sender, ASPxDataInsertingEventArgs e)
    {
        string UserNameNew = e.NewValues["UserName"].ToString();
        string Email = e.NewValues["Email"].ToString();
        string Password = e.NewValues["Password"].ToString();
        string role = e.NewValues["Role"] == null ? String.Empty : e.NewValues["Role"].ToString();
        //bool valid = true;
        //if (((List<string>)Session["UserNames"]).Contains(UserNameNew))
        //{
        //    this.LoginGridView.JSProperties["cpInvalidUserName"] = true;
        //    valid = false;
        //}

        //if (!Emailvalidation(Email))
        //{
        //    valid = false;
        //    this.LoginGridView.JSProperties["cpInvalidEmail"] = true;
        //}

        //if (valid)
        //{
       MembershipUser user =  Membership.CreateUser(UserNameNew, Password, Email);
        if (role != String.Empty)
            Roles.AddUsersToRole(new string[] { UserNameNew }, role);

        user.LastActivityDate = DateTime.UtcNow.AddMinutes(-(Membership.UserIsOnlineTimeWindow + 1));
        Membership.UpdateUser(user);
        ///insert new Employee 
        string EmpFName = e.NewValues["EmpFName"].ToString() == null ? String.Empty : e.NewValues["EmpFName"].ToString();
        string EmpLName = e.NewValues["EmpLName"] == null ? String.Empty : e.NewValues["EmpLName"].ToString();
        string EmpTitle = e.NewValues["EmpTitle"] == null ? String.Empty : e.NewValues["EmpTitle"].ToString();
        string EmpStreet = e.NewValues["EmpStreet"] == null ? String.Empty : e.NewValues["EmpStreet"].ToString();
        string EmpStreet2 = e.NewValues["EmpStreet2"]== null ? String.Empty : e.NewValues["EmpStreet2"].ToString();
        string EmpCity = e.NewValues["EmpCity"] == null ? String.Empty : e.NewValues["EmpCity"].ToString();
        string EmpState = e.NewValues["EmpState"]== null ? String.Empty : e.NewValues["EmpState"].ToString();
        string EmpZip = e.NewValues["EmpZip"] == null ? String.Empty : e.NewValues["EmpZip"].ToString();
        string EmpDepartment = e.NewValues["EmpDepartment"] == null ? String.Empty : e.NewValues["EmpDepartment"].ToString();
        string EmpDateHired = e.NewValues["EmpDateHired"] == null ? String.Empty : e.NewValues["EmpDateHired"].ToString();
        string EmpDateReleased = e.NewValues["EmpDateReleased"] == null ? String.Empty : e.NewValues["EmpDateReleased"].ToString();
        string EmpSick = e.NewValues["EmpSick"] == null ? String.Empty : e.NewValues["EmpSick"].ToString();
        string EmpHol = e.NewValues["EmpHol"] == null ? String.Empty : e.NewValues["EmpHol"].ToString();
        string EmpVac = e.NewValues["EmpVac"] == null ? String.Empty : e.NewValues["EmpVac"].ToString();
        string EmpMemo = e.NewValues["EmpMemo"] == null ? String.Empty : e.NewValues["EmpMemo"].ToString();
        string EmpBillRate = e.NewValues["EmpBillRate"] == null ? String.Empty : e.NewValues["EmpBillRate"].ToString();
        string EmpCostRate = e.NewValues["EmpCostRate"] == null ? String.Empty : e.NewValues["EmpCostRate"].ToString();
        string EmpOTCostRate = e.NewValues["EmpOTCostRate"] == null ? String.Empty : e.NewValues["EmpOTCostRate"].ToString();
        string EmpOTBillRate = e.NewValues["EmpOTBillRate"] == null ? String.Empty : e.NewValues["EmpOTBillRate"].ToString();
         
        
        string EmployeeID = UserNameNew;
        string LoginID = EmployeeID;




        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        string sql = "Insert into Employee(EmployeeID,EmpFName,EmpLName,EmpTitle,EmpStreet,EmpStreet2,EmpCity,EmpState,EmpZip,LoginID,EmpDepartment,EmpDateHired,EmpDateReleased,EmpSick,EmpHol,EmpVac,EmpMemo,EmpBillRate,EmpCostRate,EmpOTCostRate,EmpOTBillRate) " 
            + " values ('" + EmployeeID + "','" + EmpFName + "','" + EmpLName + "','" + EmpTitle + "','" + EmpStreet + "','" + EmpStreet2 + "','" + EmpCity + "','" + EmpState + "','" + EmpZip + "' ,'" + LoginID + "' ,'" + EmpDepartment + "','" + EmpDateHired + "','" + EmpDateReleased + "','" + EmpSick + "','" + EmpHol + "','" + EmpVac + "','" + EmpMemo + "' ,'" + EmpBillRate+"','"+EmpCostRate+"','"+EmpOTCostRate+"','"+EmpOTBillRate + "' )";
        DataAccess.ExecuteSqlStatement(sql,"");

        role = e.NewValues["Role"] == null ? String.Empty : e.NewValues["Role"].ToString();
         

        
        SessionUpdate();

        this.LoginGridView.DataSource = (DataTable)Session["GridDataUser"];
        this.LoginGridView.DataBind();
///// send activation Email
        if (e.NewValues["SendEmail"] != null && Boolean.Parse(e.NewValues["SendEmail"].ToString()))
        {
             SendActivationEmail(Membership.GetUser(UserNameNew),EmpFName,EmpLName);
        }
        if (e.NewValues["Activate"] != null && Boolean.Parse(e.NewValues["Activate"].ToString()))
        {
            Users.Activate(Membership.GetUser(UserNameNew).UserName);
        }
        e.Cancel = true;
        this.LoginGridView.CancelEdit();
    }
    
    private void SendActivationEmail(MembershipUser user, string EmpFName, string EmpLName)
    {
        string constr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
       // string activationCode = Guid.NewGuid().ToString();
        string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";
        using (MailMessage mm = new MailMessage("support2@omnivistasolutions.com", user.Email))
        {
            mm.Subject = "Activate my OVTime Account";
            string link = string.Format("{0}Account/Login.aspx?ActivationCode={1}", baseUrl, user.ProviderUserKey);
            string body = createEmailBody(link, user.UserName, EmpFName, EmpLName);
            mm.Body = body;
            mm.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();

            smtp.Host = "mail028-1.exch028.serverdata.net";
            smtp.EnableSsl = true;
            NetworkCredential NetworkCred = new NetworkCredential("support2@omnivistasolutions.com", "Solutions1");
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mm);
        }
    }
 
    private string createEmailBody(string link, string id,string first_name,string last_name)
    {

        string body = string.Empty;
        //using streamreader for reading my htmltemplate   
      
        using (StreamReader reader = new StreamReader(Server.MapPath("~/HtmlTemplate.html")))
        {

            body = reader.ReadToEnd();

        }
        
        body = body.Replace("{LINK}", link); //replacing the required things  
        body = body.Replace("[YEAR]", DateTime.Today.ToString("yyyy"));
        body = body.Replace("{first_name}", first_name);
        body = body.Replace("{last_name}", last_name);
        body = body.Replace("{id}", id);

        return body;

    }  
    protected void LoginGridView_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        string UserId = e.Keys["UserId"].ToString();
        string UserNameNew = e.NewValues["UserName"].ToString();
        string UserNameOld = e.OldValues["UserName"].ToString();
        string Email = e.NewValues["Email"].ToString();
        string EmailOld = e.OldValues["Email"].ToString();
        //string CreateDate = e.NewValues["CreateDate"].ToString();
        //string LastLoginDate = e.NewValues["LastLoginDate"].ToString();
        //string LastActivityDate = e.NewValues["LastActivityDate"].ToString();
        string IsLoggedIn = e.NewValues["IsLoggedIn"].ToString();
        string IsLockedOut = e.NewValues["IsLockedOut"].ToString();
        string IsLockedOutOld = e.OldValues["IsLockedOut"].ToString();
        string Password = e.NewValues["Password"].ToString();
        string role_new = e.NewValues["Role"] == null ? String.Empty : e.NewValues["Role"].ToString();
        string role_old = e.OldValues["Role"] == null ? String.Empty : e.OldValues["Role"].ToString();
        MembershipUser user = Membership.GetUser(UserNameOld);

        if (IsLockedOut != IsLockedOutOld && IsLockedOut == "False")
        {
            user.UnlockUser();
            Membership.UpdateUser(user);
        }
        if (Email != EmailOld)
        {
            user.Email = Email;
            Membership.UpdateUser(user);
        }
        if (IsLockedOut != IsLockedOutOld && IsLockedOut == "True")
        {
            string query = String.Format("UPDATE m SET IsLockedOut = 1 FROM aspnetdb.dbo.aspnet_Membership m JOIN aspnetdb.dbo.aspnet_Users u ON m.UserId = u.UserId WHERE u.UserName = '{0}'", UserNameOld);
            int success = DataAccess.ExecuteSqlStatement(query, null);
            if (success <= 0) throw new Exception("Error. User was not locked out.");
        }
        if (Password != "**********")
        {
            string Error_Message = String.Empty;
            string _newp = user.ResetPassword();
            user.ChangePassword(_newp, Password);
        }
        if (role_new != role_old)
        {
            if (role_new != String.Empty)
            {
                Roles.AddUsersToRole(new string[] { user.UserName }, role_new);
            }
            if (role_old != String.Empty)
            {
                Roles.RemoveUserFromRole(user.UserName, role_old);
            }
        }
        object[] eventArgs = new object[1] { e.Keys["UserId"] };
        DataRow row = ((DataTable)Session["GridDataUser"]).Rows.Find(eventArgs);
        row["UserName"] = UserNameNew;
        row["Email"] = Email;
        //row["CreateDate"] = CreateDate;
        //row["LastLoginDate"] = LastLoginDate;
        //row["LastActivityDate"] = LastActivityDate;
        row["IsLoggedIn"] = IsLoggedIn;
        row["IsLockedOut"] = IsLockedOut;
        if (role_new != String.Empty)
            row["Role"] = role_new;
        e.Cancel = true;
        LoginGridView.CancelEdit();
    }

    protected void LoginGridView_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        bool success = Membership.DeleteUser(e.Values["UserName"].ToString());
        if (!success) throw new Exception("Error. Row(s) have not been deleted from database.");

        if(success)
        {
            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            string query = "delete from Employee where EmployeeID = '" + e.Values["UserName"].ToString() + "'";
            DataAccess.ExecuteSqlStatement(query,"");
        }
        SessionUpdate();
        this.LoginGridView.DataSource = (DataTable)Session["GridDataUser"];
        this.LoginGridView.DataBind();
        e.Cancel = true;
        this.LoginGridView.CancelEdit();
    }

    protected void LoginGridView_RowValidating(object sender, ASPxDataValidationEventArgs e)
    {
        string UserNameNew = e.NewValues["UserName"].ToString();
        string Email = e.NewValues["Email"].ToString();
        string Password = e.NewValues["Password"].ToString();
        string Password2 = e.NewValues["EmpPassword2"].ToString();


        if (((List<string>)Session["UserNames"]).Contains(UserNameNew) && e.IsNewRow == true)//don't check UserName when updating since it's read only 
        {
            e.Errors[LoginGridView.Columns["UserName"]] = "UserName already Exists";
        }
        if (Password.Length < 6)
        {
            e.Errors[LoginGridView.Columns["Password"]] = "Password minimal length is 6";
        }
        if (Password != Password2)
        {
            e.Errors[LoginGridView.Columns["Password"]] = "Passwords are not matchs";
            e.Errors[LoginGridView.Columns["EmpPassword2"]] = "Passwords are not matchs";
        }
        if (!Emailvalidation(Email))
        {
            e.Errors[LoginGridView.Columns["Email"]] = "Email format invalid";
        }
        
        if (string.IsNullOrEmpty(e.RowError) && e.Errors.Count > 0) e.RowError = "Please, correct all errors.";
    }

    protected void LoginGridView_BeforeColumnSortingGrouping(object sender, DevExpress.Web.ASPxGridViewBeforeColumnGroupingSortingEventArgs e)
    {
        DataTable table = new DataTable();

        table.Columns.Add("UserName", Type.GetType("System.String"));
        table.Columns.Add("Email", Type.GetType("System.String"));
        table.Columns.Add("CreateDate", Type.GetType("System.DateTime"));
        table.Columns.Add("LastLoginDate", Type.GetType("System.DateTime"));
        table.Columns.Add("LastActivityDate", Type.GetType("System.DateTime"));
        table.Columns.Add("IsLoggedIn", Type.GetType("System.Boolean"));
        table.Columns.Add("IsLockedOut", Type.GetType("System.Boolean"));


        MembershipUserCollection muc = Membership.GetAllUsers();
        foreach (MembershipUser user in muc)
        {
            DataRow dr = table.NewRow();
            dr["UserName"] = user.UserName;
            dr["Email"] = user.Email;
            dr["CreateDate"] = user.CreationDate;
            dr["LastLoginDate"] = user.LastLoginDate;
            dr["LastActivityDate"] = user.LastActivityDate;
            dr["IsLoggedIn"] = user.IsOnline;
            dr["IsLockedOut"] = user.IsLockedOut;


            table.Rows.Add(dr);
        }

        DataColumn[] columns = new DataColumn[1];
        columns[0] = table.Columns["UserId"];

        table.PrimaryKey = columns;

        Session["GridDataUser"] = table;

        this.LoginGridView.DataSource = Session["GridDataUser"];
        this.LoginGridView.DataBind();
    }
    
    private void SessionUpdate()
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);

        DataTable table = new DataTable();

        table.Columns.Add("UserName", Type.GetType("System.String"));
        table.Columns.Add("Email", Type.GetType("System.String"));
        table.Columns.Add("CreateDate", Type.GetType("System.DateTime"));
        table.Columns.Add("LastLoginDate", Type.GetType("System.DateTime"));
        table.Columns.Add("LastActivityDate", Type.GetType("System.DateTime"));
        table.Columns.Add("IsLoggedIn", Type.GetType("System.Boolean"));
        table.Columns.Add("IsLockedOut", Type.GetType("System.Boolean"));
        table.Columns.Add("UserId", Type.GetType("System.String"));
        table.Columns.Add("Password", Type.GetType("System.String"));
        table.Columns.Add("Role", Type.GetType("System.String"));      
        table.Columns.Add("Activated", Type.GetType("System.String"));
        
        List<string> Username = new List<string>();
        MembershipUserCollection muc = Membership.GetAllUsers();
        foreach (MembershipUser user in muc)
        {
            DataRow dr = table.NewRow();
            dr["UserName"] = user.UserName;
            dr["Email"] = user.Email;
            dr["CreateDate"] = user.CreationDate;
            dr["LastLoginDate"] = user.LastLoginDate;
            dr["LastActivityDate"] = user.LastActivityDate;
            dr["IsLoggedIn"] = user.IsOnline;
            dr["IsLockedOut"] = user.IsLockedOut;
            dr["UserId"] = user.ProviderUserKey;
            dr["Password"] = "**********";
                     
            dr["Role"] = String.Join(",", Roles.GetRolesForUser(user.UserName));
            dr["Activated"] = GetActive(user.UserName);
            ///get employee data 
          

            table.Rows.Add(dr);
            Username.Add(user.UserName);

        }

        DataColumn[] columns = new DataColumn[1];
        columns[0] = table.Columns["UserId"];
        table.PrimaryKey = columns;
        Session["GridDataUser"] = table;
        Session["UserNames"] = Username;
    }
    private string GetActive(string userName)
    {
         DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString);
            string sql = string.Format("Select * from dbo.aspnet_Users where UserName = '{0}' ", userName);
            DataTable tb = DataAccess.GetDataTableBySqlSyntax(sql, "");
            if (tb.Rows.Count > 0)
            {
                if (tb.Rows[0]["Activated"].ToString() == "" || tb.Rows[0]["Activated"].ToString() == "0")
                {
                    return "0";
                }
                else
                {
                    return "1";
                }
            }
            else
            {
                return "0";
            }
    }
    private bool Emailvalidation(string _email)
    {
        Regex regex = new Regex(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))\z");
        Match mathc = regex.Match(_email);
        if (mathc.Success)
            return true;
        else
            return false;
    }
    protected void LoginGridView_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "HasProfile")
        {
           // e.EncodeHtml = true;
          //  e.DisplayText = string.Format("<a href='/Manager/CreateProfile.aspx?ID={0}'>Create Profile</a>", e.Value);

        }
    }
  
    [WebMethod]
    [ScriptMethod]
    public static void Activate(string userID)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString);
        string token = userID;
        if (token != null && token != String.Empty)
        {
            string sql = "select * from aspnet_Users where UserId ='" + token + "'";
            DataTable user = DataAccess.GetDataTableBySqlSyntax(sql, "");
            if (user.Rows.Count > 0 && user.Rows[0]["Activated"] == DBNull.Value)
            {
                string sql2 = "Update   aspnet_Users set Activated = 1 where UserId ='" + token + "' ";
                DataAccess.ExecuteSqlStatement(sql2, "");
            }
           
        }
        else
        {
            return;
        }
    }
    [WebMethod]
    [ScriptMethod]
    public static void ActivateL(string userID)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString);
        string token = userID;
        if (token != null && token != String.Empty)
        {
            string sql = "select * from aspnet_Users where UserName ='" + token + "'";
            DataTable user = DataAccess.GetDataTableBySqlSyntax(sql, "");
            if (user.Rows.Count > 0 && user.Rows[0]["Activated"] == DBNull.Value)
            {
                string sql2 = "Update   aspnet_Users set Activated = 1 where UserName ='" + token + "' ";
                DataAccess.ExecuteSqlStatement(sql2, "");
            }

        }
        else
        {
            return;
        }
    }
   
    [WebMethod]
    [ScriptMethod]
    public static string GetEmployeeInfo(string userName)
    {
        if(userName!=null){
        MembershipUser userAccess = Membership.GetUser(userName);  
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        string sql = "select * from Employee where EmployeeID ='" + userName + "'";
        DataTable user = DataAccess.GetDataTableBySqlSyntax(sql, "");
        
        user.Columns.Add("Roles", Type.GetType("System.String"));   
        string roles = "";
        foreach(string str in Roles.GetRolesForUser(userName) ){
            roles+=str+"," ; 
        }
     

        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> row;
        foreach (DataRow dr in user.Rows)
        {
            row = new Dictionary<string, object>();
            foreach (DataColumn col in user.Columns)
            {
                row.Add(col.ColumnName, dr[col]);
            }
            rows.Add(row);
        }
        rows[0]["Roles"] = roles;
        rows[0]["EmpEmail"] = userAccess.Email; 
        return new JavaScriptSerializer().Serialize(rows);
        }
        return null;
    }
   
    [WebMethod]
    [ScriptMethod]
    public static void ChangePassword(string userID, string password)
    {
      MembershipUser user =  Membership.GetUser(userID);
      user.UnlockUser();
      string tempPassword = user.ResetPassword();
      user.ChangePassword(tempPassword, password);
    }
   
    [WebMethod]
    [ScriptMethod]
    public static string AddEmployee(string Employee, string roles, string baseURL, bool editflag)
    {
        Dictionary<string, object> obj = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(Employee);
        Dictionary<object, object> rolesL = new JavaScriptSerializer().Deserialize<Dictionary<object, object>>(roles);
        string[] rolesV = new string[rolesL.Count];
        for (int count = 0; count < rolesV.Length; count++)
        {
            rolesV[count] = rolesL.Values.ElementAt(count).ToString();
        }

        if (!editflag)
        {
            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString);
            string sql = string.Format("select * from aspnet_Users where UserName ='{0}'", obj.Values.ElementAt(0));
            if (DataAccess.GetDataTableBySqlSyntax(sql, "").Rows.Count > 0)
            {
                 
                return "User Already exists .";
            }
            MembershipUser user = Membership.CreateUser(obj.Values.ElementAt(0).ToString(), obj.Values.ElementAt(3).ToString(), obj.Values.ElementAt(2).ToString());
            if (rolesV.Length > 0)
            {
                Roles.AddUserToRoles(obj.Values.ElementAt(0).ToString(), rolesV);
            }
            user.LastActivityDate = DateTime.UtcNow.AddMinutes(-(Membership.UserIsOnlineTimeWindow + 1));
            user.Email = obj.Values.ElementAt(2).ToString();
            Membership.UpdateUser(user);
          

            if (obj.Values.ElementAt(12).ToString() != null && Boolean.Parse(obj.Values.ElementAt(12).ToString()))
            {
                CommonFunctions.SendActivationEmail(user, obj.Values.ElementAt(4).ToString(), obj.Values.ElementAt(5).ToString(), baseURL);
            }
            if (obj.Values.ElementAt(13).ToString() != null && Boolean.Parse(obj.Values.ElementAt(13).ToString()))
            {
                Users.ActivateL(user.UserName);
            }

           //// CommonFunctions.SendActivationEmail(user, obj.Values.ElementAt(4).ToString(), obj.Values.ElementAt(5).ToString(), baseURL);
            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            sql = "Insert into Employee(";
            string into = "";
            string values = " Values (";
            for (int i = 0; i < obj.Count(); i++)
            {
                if (obj.Keys.ElementAt(i).Contains("UDF") || obj.Values.ElementAt(i) == String.Empty || obj.Keys.ElementAt(i) == "EmpPassword" || obj.Keys.ElementAt(i) == "SendEmail" || obj.Keys.ElementAt(i) == "Activate" || obj.Keys.ElementAt(i) == "roles" || obj.Keys.ElementAt(i) == "Lockout")
                {
                    continue;
                }
                into += obj.Keys.ElementAt(i) + ",";
                values += "'" + obj.Values.ElementAt(i) + "',";
            }
            into = into.Substring(0, into.Length - 1) + ")";
            values = values.Substring(0, values.Length - 1) + ")";

            sql += into + values;
            DataAccess.ExecuteSqlStatement(sql, "");
            string UDF1 = obj.Values.ElementAt(27).ToString();
            string UDF2 = obj.Values.ElementAt(28).ToString();
            string UDF3 = obj.Values.ElementAt(29).ToString();
            string UDF4 = obj.Values.ElementAt(30).ToString();
            string FullName = obj.Values.ElementAt(4).ToString()+" "+obj.Values.ElementAt(5).ToString();
            InsertUDF(user.UserName, FullName, UDF1, UDF2, UDF3, UDF4);

            return "";
        }
        else
        {
           MembershipUser user=  Membership.GetUser(obj.Values.ElementAt(0).ToString());
           user.Email = obj.Values.ElementAt(2).ToString();
          //// CommonFunctions.SendActivationEmail(user, obj.Values.ElementAt(4).ToString(), obj.Values.ElementAt(5).ToString(), baseURL);

          //  MembershipUser user = Membership.CreateUser(obj.Values.ElementAt(0).ToString(), obj.Values.ElementAt(3).ToString(), obj.Values.ElementAt(2).ToString());
            if (rolesV.Length > 0)
            {
                for(int i=0;i<rolesV.Length;i++)
                {
                    if (!Roles.IsUserInRole(obj.Values.ElementAt(0).ToString(), rolesV[i]))
                    {
                        Roles.AddUserToRole(obj.Values.ElementAt(0).ToString(), rolesV[i]);
                    }
                }
                
            }
            if (Boolean.Parse(obj.Values.ElementAt(14).ToString()))
            {
                string query = String.Format("UPDATE m SET IsLockedOut = 1 FROM aspnetdb.dbo.aspnet_Membership m JOIN aspnetdb.dbo.aspnet_Users u ON m.UserId = u.UserId WHERE u.UserName = '{0}'", user.UserName);
                DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString);
                DataAccess.ExecuteSqlStatement(query, "");
            }
            else
            {
                string query = String.Format("UPDATE m SET IsLockedOut = 0 FROM aspnetdb.dbo.aspnet_Membership m JOIN aspnetdb.dbo.aspnet_Users u ON m.UserId = u.UserId WHERE u.UserName = '{0}'", user.UserName);
                DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString);
                DataAccess.ExecuteSqlStatement(query, "");
            }
            user.LastActivityDate = DateTime.UtcNow.AddMinutes(-(Membership.UserIsOnlineTimeWindow + 1));
            Membership.UpdateUser(user);
            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            string sql = "Update Employee set ";
             
            for (int i = 0; i < obj.Count(); i++)
            {
                if (obj.Keys.ElementAt(i)=="EmployeeID" || obj.Keys.ElementAt(i).Contains("UDF")|| obj.Values.ElementAt(i).ToString() == String.Empty || obj.Keys.ElementAt(i) == "EmpPassword" || 
                    obj.Keys.ElementAt(i) == "SendEmail" || obj.Keys.ElementAt(i) == "Activate" || obj.Keys.ElementAt(i) == "roles" 
                    || obj.Keys.ElementAt(i) == "Lockout")
                {
                    continue;
                }
                sql  += obj.Keys.ElementAt(i) + "=";
                sql += "'" + obj.Values.ElementAt(i) + "',";
            }
            sql = sql.Substring(0, sql.Length - 1);
            sql += " where EmployeeID = '" + obj.Values.ElementAt(0).ToString() + "'";

            DataAccess.ExecuteSqlStatement(sql, "");
            string UDF1 = obj.Values.ElementAt(27).ToString();
            string UDF2 = obj.Values.ElementAt(28).ToString();
            string UDF3 = obj.Values.ElementAt(29).ToString();
            string UDF4 = obj.Values.ElementAt(30).ToString();
            string FullName = obj.Values.ElementAt(4).ToString() + " " + obj.Values.ElementAt(5).ToString();
            InsertUDF(user.UserName,FullName,UDF1,UDF2,UDF3,UDF4);
            return "";
        }
    }

    protected static void InsertUDF(string EmployeeID,string FullName,string UDF1,string UDF2,
        string UDF3,string UDF4,string UDF5="",string UDF6="")
    {
        bool insertFlag = true;
        string sql = "select * from UDF where EmployeeID = '"+EmployeeID+"'";
        if(DataAccess.GetDataTableBySqlSyntax(sql, "").Rows.Count>0) insertFlag = false;

        if (insertFlag)
        {
             sql = "Insert into UDF(EmployeeID,FullName,UDF1,UDF2,UDF3,UDF4) values('" + EmployeeID + "','" + FullName + "','" + UDF1 +
            "','" + UDF2 + "','" + UDF3 + "','" + UDF4 + "')";
            DataAccess.ExecuteSqlStatement(sql, "");
        }
        else
        {
             sql = "Update UDF set FullName='" + FullName + "' ,UDF1 = '" + UDF1 + "', UDF2='" + UDF2 +
              "',UDF3='" + UDF3 + "',UDF4='" + UDF4 + "' where EmployeeID = '" + EmployeeID + "'";
            DataAccess.ExecuteSqlStatement(sql, "");
        }
    }
    protected void LoginGridView_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
    {
        this.LoginGridView.DataSource = (DataTable)Session["GridDataUser"];
    }
    protected void ASPxCallbackPanel1_Callback(object sender, CallbackEventArgsBase e)
    {
        if (e.Parameter != null && e.Parameter != String.Empty)
        {
            this.roles.DataSource = CustomRoles.GetActiveRoles();
            this.roles.DataBind();
            string userName = e.Parameter;
            MembershipUser userAccess = Membership.GetUser(userName);
            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            string sql = "select * from Employee where EmployeeID ='" + userName + "'";
            DataTable user = DataAccess.GetDataTableBySqlSyntax(sql, "");
            if (user.Rows.Count > 0)
            {
                DataRow u = user.Rows[0];
                string roles = "";
                foreach (string str in Roles.GetRolesForUser(userName))
                {
                    roles += str + ",";
                    ListEditItem item = this.roles.Items.FindByValue(str);
                    if (item != null)
                    {
                        item.Selected = true;
                    }
                }
                this.EmployeeID.Text = u["EmployeeID"].ToString();
                this.Email.Text = userAccess.Email;
                this.EmpFName.Text = u["EmpFName"].ToString();
                this.EmpLName.Text = u["EmpLName"].ToString();
                this.EmpTitle.Text = u["EmpTitle"].ToString();
                this.EmpStreet.Text = u["EmpStreet"].ToString();
                this.EmpStreet2.Text = u["EmpStreet2"].ToString();
                this.EmpCity.Text = u["EmpCity"].ToString();
                this.EmpState.Text = u["EmpState"].ToString();
                this.EmpZip.Text = u["EmpZip"].ToString();
                this.managers.Text = u["ManagerID"].ToString();
                if (u["EmpDateHired"].ToString() != String.Empty)
                {
                    this.EmpDateHired.Date = DateTime.Parse(u["EmpDateHired"].ToString());
                }
                this.EmpContact.Text = u["EmpContact"].ToString();
                this.EmpDepartment.Text = u["EmpDepartment"].ToString();
                this.EmpVac.Text = u["EmpVac"].ToString();
                this.EmpHol.Text = u["EmpHol"].ToString();
                this.EmpSick.Text = u["EmpSick"].ToString();
                if (u["EmpNextImpDate"].ToString() != String.Empty)
                {
                    this.EmpNextImpDate.Date = DateTime.Parse(u["EmpNextImpDate"].ToString());
                }
                // this.EmpNextImpDate.Date = (DateTime)(u["EmpNextImpDate"]);
                this.EmpBillRate.Text = u["EmpBillRate"].ToString();
                this.EmpCostRate.Text = u["EmpCostRate"].ToString();
                this.EmpOTCostRate.Text = u["EmpOTCostRate"].ToString();
                this.EmpOTBillRate.Text = u["EmpOTBillRate"].ToString();
                this.EmpPassword.ClientVisible = false;
                this.EmpPassword2.ClientVisible = false;
                this.SendEmail.ClientVisible = false;
                this.ActivateModal2.ClientVisible = false;
                this.Lockout.ClientVisible = true;
                this.Lockout.Checked = userAccess.IsLockedOut;
                this.EmployeeID.Enabled = false;
                //user defined fields 
                sql = "select * from UDF where EmployeeID ='" + userName + "'";
                DataTable udfs =  DataAccess.GetDataTableBySqlSyntax(sql, "");
                foreach(DataRow udf in udfs.Rows)
                {
                   if (udf["UDF1"] != null && udf["UDF1"].ToString() != String.Empty) this.UDF1.Date = DateTime.Parse(udf["UDF1"].ToString());
                   if (udf["UDF2"] != null && udf["UDF2"].ToString() != String.Empty)
                   {
                       this.UDF2.Date = DateTime.Parse(udf["UDF2"].ToString());
                       this.EmpNextImpDate.Date = DateTime.Parse(udf["UDF2"].ToString());
                   }
                   if (udf["UDF3"] != null && udf["UDF3"].ToString() != String.Empty) this.UDF3.Date = DateTime.Parse(udf["UDF3"].ToString());
                   if (udf["UDF4"] != null && udf["UDF4"].ToString() != String.Empty) this.UDF4.Date = DateTime.Parse(udf["UDF4"].ToString());

                }
                
            }
        }
       
    }
    protected void UserNameComboBox_Callback(object sender, CallbackEventArgsBase e)
    {
        MembershipUserCollection muc = Membership.GetAllUsers();
        List<string> Username = new List<string>();
        foreach (MembershipUser dr in muc)
        {
            Username.Add(dr.UserName);
        }
        Session["UserNames"] = Username;
        this.UserNameComboBox.DataSource = (List<string>)Session["UserNames"];
        DataBind();
    }
}
