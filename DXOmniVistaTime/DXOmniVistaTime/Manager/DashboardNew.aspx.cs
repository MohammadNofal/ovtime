﻿using DevExpress.XtraCharts;
using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.XtraCharts.Web;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Native;

public partial class Manager_DashboardNew : System.Web.UI.Page
{
    ASPxComboBox cbAppearance;
    ASPxComboBox cbPalette;
    Series series1;
    int width;

    void PrepareComboBox(ASPxComboBox comboBox, string[] items, string defaultItem)
    {
        comboBox.Items.Clear();
        comboBox.Items.AddRange(items);
        comboBox.SelectedIndex = defaultItem != null ? comboBox.Items.IndexOfText(defaultItem) : 0;
    }

    public void LoadData()
    {
        //LoadProjects();

        this.ClientsCombobox.DataSource = GetClients();
        this.ClientsCombobox.DataBind();
        this.ProjectsCombobox.DataSource = GetProjects();
        this.ProjectsCombobox.DataBind();

        //Discipline
        DisciplineComboBox.Items.Add("");
        DataTable roles = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["RoleNames"], "");
        DisciplineComboBox.DataSource = roles;
        DisciplineComboBox.DataBind();
        //DisciplineComboBox.Items.Add("Management");
        //DisciplineComboBox.Items.Add("Architecture");
        //DisciplineComboBox.Items.Add("Structure");
        //DisciplineComboBox.Items.Add("Mechanical");
        //DisciplineComboBox.Items.Add("Electrical");
        //DisciplineComboBox.Items.Add("QS");
    }

    public DataTable GetClients()
    {
        //Mohammad changed this
        //string sql = "SELECT CLIENTID,CLIENTCOMPANY FROM CLIENT WHERE CLIENTID IS NOT NULL AND ClientCompany IS NOT NULL";
        //string sql = "SELECT CLIENTID FROM CLIENT WHERE  CLIENTID IS NOT NULL AND CLIENTID !='' ORDER BY CLIENTID ASC";
        string sql = ConfigurationManager.AppSettings["ClientIDs"];
        //..................
        DataTable Clients = DataAccess.GetDataTableBySqlSyntax(sql, "");
        // Inserting an empty row at the beginning of the list
        DataRow emptyDataRow = Clients.NewRow();
        Clients.Rows.InsertAt(emptyDataRow, 0);
        return Clients;
    }

    public DataTable GetProjects()
    {
        //Mohammad eddited the following
         DataTable Projects = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["ParentProjects"], "");
        //DataTable Projects = DataAccess.GetDataTableBySqlSyntax("select distinct [dbo].ProjectParent(projectid) as projectid from project WHERE [dbo].ProjectParent(projectid) !='' ORDER BY [dbo].ProjectParent(projectid) ASC", "");
        //..............................
        // Inserting an empty row at the beginning of the list
        DataRow emptyDataRow = Projects.NewRow();
        Projects.Rows.InsertAt(emptyDataRow, 0);
        return Projects;
    }

    public string BuildQuery()
    {
        string query = string.Empty;

        if (this.From.Value != null)
        {
            query += "'" + this.From.Value.ToString() + "'";
        }
        else
        {
            query += "null";
        }
        if (query != string.Empty) query += ",";
        if (this.To.Value != null)
        {
            query += "'" + this.To.Value.ToString() + "'";
        }
        else
        {
            query += "null";
        }
        if (query != string.Empty) query += ",";

        if (this.ClientsCombobox.Value != null)
        {
            query += "'" + this.ClientsCombobox.Value + "'";
        }
        else
        {
            query += "null";
        }

        if (query != string.Empty) query += ",";

        if (this.ProjectsCombobox.Value != null)
        {
            query += "'" + this.ProjectsCombobox.Value + "'";
        }
        else
        {
            query += "null";
        }

        if (query != string.Empty) query += ",";

        if (this.DisciplineComboBox.Value != null)
        {
            query += "'" + this.DisciplineComboBox.Value + "'";
        }
        else
        {
            query += "null";
        }
        return query;
    }



    public void LoadProjects(string query = "")
    {
        WebChartControl1.Visible = true;
        DashboardToolbar.Visible = true;
        series1 = new Series("My Series", ViewType.Bar);
        //series1.LabelsVisibility = DefaultBoolean.True;

        BarSeriesLabel label = (BarSeriesLabel)series1.Label;
        label.TextPattern = "{A}";

        label.ResolveOverlappingMode = ResolveOverlappingMode.Default;

        series1.ArgumentScaleType = ScaleType.Auto;
        series1.ArgumentDataMember = "Project";
        series1.ValueScaleType = ScaleType.Numerical;
        series1.ValueDataMembers.AddRange(new string[] { "Project Hrs" });
        series1.ShowInLegend = false;

        if (query == string.Empty || query == "null,null,null,null,null")
        {
            series1.DataSource = DataAccess.GetDataTableBySqlSyntax("exec [OVS_GetProjectsSummary] null,null,null,null,null", "");
            Session["DashboardNewDataGrid"] = series1.DataSource;
        }

        else
        {
            series1.DataSource = DataAccess.GetDataTableBySqlSyntax("exec [OVS_GetProjectsSummary] " + query, "");
            Session["DashboardNewDataGrid"] = series1.DataSource;

        }
        if (Session["DashboardNewDataGrid"] != null) { DataTable t = (DataTable)Session["DashboardNewDataGrid"];
            if (t.Rows.Count <= 0)
            {
                WebChartControl1.Visible = false;
                DashboardToolbar.Visible = false;
            }
        }
        for (int i=0; i<WebChartControl1.Series.Count; i++)
        {
            WebChartControl1.Series.RemoveAt(i);
        }
        WebChartControl1.Series.Add(series1);
        XYDiagram diagram = (XYDiagram)WebChartControl1.Diagram;
        diagram.Rotated = true;

        int items = series1.Points.Count;
        WebChartControl1.Height = 66* items;
        
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
        


        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ToString());

        if (IsPostBack)
        {
            if (Session["DashboardNewDataGrid"] != null)
            {
                double x = (double)ASPxHiddenScreenWidth.Get("Width");
                WebChartControl1.Width = (int)x;

                MenuItem mnuAppearance = DashboardToolbar.Items.FindByName("mnuAppearance");
                cbAppearance = mnuAppearance.FindControl("cbAppearance") as ASPxComboBox;
                MenuItem mnuPalett1e1 = DashboardToolbar.Items.FindByName("mnuPalette");
                cbPalette = mnuPalett1e1.FindControl("cbPalette") as ASPxComboBox;

                if (cbAppearance.Text.Equals("") || cbPalette.Text.Equals(""))
                    WebControlMenu(WebChartControl1.AppearanceName, WebChartControl1.PaletteName);
                else WebControlMenu(cbAppearance.Text, cbPalette.Text);

                series1 = new Series("My Series", ViewType.Bar);
                //series1.LabelsVisibility = DefaultBoolean.True;

                BarSeriesLabel label = (BarSeriesLabel)series1.Label;
                label.TextPattern = "{A}";

                label.ResolveOverlappingMode = ResolveOverlappingMode.Default;

                series1.ArgumentScaleType = ScaleType.Auto;
                series1.ArgumentDataMember = "Project";
                series1.ValueScaleType = ScaleType.Numerical;
                series1.ValueDataMembers.AddRange(new string[] { "Project Hrs" });
                series1.ShowInLegend = false;
                series1.DataSource = (DataTable)Session["DashboardNewDataGrid"];
                for (int i = 0; i < WebChartControl1.Series.Count; i++)
                {
                    WebChartControl1.Series.RemoveAt(i);
                }
                WebChartControl1.Series.Add(series1);
                XYDiagram diagram = (XYDiagram)WebChartControl1.Diagram;
                diagram.Rotated = true;
                int items = series1.Points.Count;
                WebChartControl1.Height = 66 * items;
            }
            //WebChartControl1.Width = width;

            //this.ChartLoadingPanel.ContainerElementID = "WebChartControl1Pannel";

            //DataTable data = DataAccess.GetDataTableBySqlSyntax("EXEC [OVS_GetProjectsSummary] NULL,NULL,NULL,NULL,NULL", "");


            //// Create DataView
            //DataView view = new DataView(data);

            //// Sort by State and ZipCode column in descending order
            //view.Sort = "Project Hrs DESC";
            //view.Sort = "Project DESC";
            //this.WebChartControl1.DataSource = view.ToTable().Rows.Cast<System.Data.DataRow>().Take(15).CopyToDataTable();
            //XYDiagram diagram = (XYDiagram)WebChartControl1.Diagram;
            //diagram.EnableAxisXScrolling = true;
            //diagram.EnableAxisYScrolling = true;
            //diagram.EnableAxisXZooming = true;
            //diagram.EnableAxisYZooming = true;
            //this.WebChartControl1.DataBind();

            //series1 = new Series("My Series", ViewType.Bar);
            ////series1.LabelsVisibility = DefaultBoolean.True;

            //BarSeriesLabel label = (BarSeriesLabel)series1.Label;
            //label.TextPattern = "{A}";

            //label.ResolveOverlappingMode = ResolveOverlappingMode.Default;
            ////if(WebChartControl1.Series.Count > 0){
            ////    for (int i = 0; i < WebChartControl1.Series.Count; i++)
            ////    {
            ////        WebChartControl1.Series.RemoveAt(i);
            ////    }
            ////}
            //WebChartControl1.Series.Add(series1);

            //series1.ArgumentScaleType = ScaleType.Auto;
            //series1.ArgumentDataMember = "Project";
            //series1.ValueScaleType = ScaleType.Numerical;
            //series1.ValueDataMembers.AddRange(new string[] { "Project Hrs" });
            //series1.ShowInLegend = false;




            //XYDiagram diagram = (XYDiagram)WebChartControl1.Diagram;
            //diagram.Rotated = true;

            //MenuItem mnuAppearance = DashboardToolbar.Items.FindByName("mnuAppearance");
            //if (mnuAppearance != null)
            //{
            //    cbAppearance = mnuAppearance.FindControl("cbAppearance") as ASPxComboBox;
            //    if (cbAppearance != null)
            //        PrepareComboBox(cbAppearance, WebChartControl1.GetAppearanceNames(), WebChartControl1.AppearanceName);
            //}
            //MenuItem mnuPalette = DashboardToolbar.Items.FindByName("mnuPalette");
            //if (mnuPalette != null)
            //{
            //    cbPalette = mnuPalette.FindControl("cbPalette") as ASPxComboBox;
            //    if (cbPalette != null)
            //        PrepareComboBox(cbPalette, WebChartControl1.GetPaletteNames(), WebChartControl1.PaletteName);
            //}
            // filter_Click(null, null);

        }
        if (!IsPostBack)
        {
            LoadData();

        }
    }

    

    private void WebControlMenu(string AppearanceName, string PaletteName)
    {
        MenuItem mnuAppearance = DashboardToolbar.Items.FindByName("mnuAppearance");
        if (mnuAppearance != null)
        {
            cbAppearance = mnuAppearance.FindControl("cbAppearance") as ASPxComboBox;
            if (cbAppearance != null)
            {
                WebChartControl1.AppearanceName = AppearanceName;
                PrepareComboBox(cbAppearance, WebChartControl1.GetAppearanceNames(), AppearanceName);
            }
        }
        MenuItem mnuPalette = DashboardToolbar.Items.FindByName("mnuPalette");
        if (mnuPalette != null)
        {
            cbPalette = mnuPalette.FindControl("cbPalette") as ASPxComboBox;
            if (cbPalette != null)
            {
                WebChartControl1.PaletteName = PaletteName;
                PrepareComboBox(cbPalette, WebChartControl1.GetPaletteNames(), PaletteName);
            }
        }
    }

    protected void WebChartControl_CallBack (object sender, CallbackEventArgsBase e) {
        
        double x = (double)ASPxHiddenScreenWidth.Get("Width");
        WebChartControl1.Width = (int)x ;

        MenuItem mnuAppearance = DashboardToolbar.Items.FindByName("mnuAppearance");
        cbAppearance = mnuAppearance.FindControl("cbAppearance") as ASPxComboBox;
        MenuItem mnuPalett1e1 = DashboardToolbar.Items.FindByName("mnuPalette");
        cbPalette = mnuPalett1e1.FindControl("cbPalette") as ASPxComboBox;
   

        switch (e.Parameter)
        {
            case "Palette":
            case "Appearance":
                WebControlMenu(cbAppearance.Text, cbPalette.Text);

                break;
            case "": if (cbAppearance.Text.Equals("") || cbPalette.Text.Equals(""))
                    WebControlMenu(WebChartControl1.AppearanceName, WebChartControl1.PaletteName);
                else WebControlMenu(cbAppearance.Text, cbPalette.Text);

                break;
        }

        string query = BuildQuery();
        LoadProjects(query);

      


    }
    //protected void WebChartControl1_CustomCallback(object sender, DevExpress.XtraCharts.Web.CustomCallbackEventArgs e)
    //{
    //    //string[] parameterValues = e.Parameter.Split(';');

    //    //width = Convert.ToInt32(parameterValues[1]);

    //    //WebChartControl1.Width = width;
 

     
    //}

    //protected void chart_CustomCallback(object sender, CustomCallbackEventArgs e)
    //{
    //    switch (e.Parameter)
    //    {
    //        case "Palette":
    //            if (cbPalette != null)
    //                WebChartControl1.PaletteName = cbPalette.Text;
    //            break;
    //        case "Appearance":
    //            if (cbAppearance != null)
    //            {
    //                WebChartControl1.AppearanceName = cbAppearance.Text;
    //                if (cbPalette != null)
    //                    cbPalette.SelectedIndex = cbPalette.Items.IndexOfText(WebChartControl1.PaletteName);
    //            }
    //            break;
    //    }
    //}

    protected void ExportBtn_ServerClick(object sender, EventArgs e)
    {

    }

    protected void ASPXDateEdit_CustomCell(object sender, CalendarDayCellPreparedEventArgs e)
    {
        // #c00000 (red active)
        // #333333 (black active)
        // #ececec (grey not active)
        if (!e.IsOtherMonthDay)
        {
            if (e.Date.DayOfWeek == DayOfWeek.Sunday) e.Cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#333333");
            else if (e.Date.DayOfWeek == DayOfWeek.Friday) e.Cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#c00000"); //#c00000
        }
    }
}