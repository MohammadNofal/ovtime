﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DXOmniVistaTimeEngine;
using System.Web.Security;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using DevExpress.Web.Data;
using DevExpress.Web;
using System.Collections.Specialized;

public partial class EmployeeSchedule : System.Web.UI.Page
{
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);

        DataTable table = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["EmployeeSchedule"], string.Empty);
        DataTable ProjectID = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["Project"], string.Empty);
        DataTable EmployeeID = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["Employee"], string.Empty);

        GridViewDataComboBoxColumn col = ASPxGridView1.Columns["ProjectID"] as GridViewDataComboBoxColumn;
        col.PropertiesComboBox.DataSource = ProjectID;
        col.PropertiesComboBox.ValueField = "MasterProjectID";
        col.PropertiesComboBox.TextField = "MasterProjectID";

        GridViewDataComboBoxColumn col2 = ASPxGridView1.Columns["EmployeeID"] as GridViewDataComboBoxColumn;
        col2.PropertiesComboBox.DataSource = EmployeeID;
        col2.PropertiesComboBox.ValueField = "EmployeeID";
        col2.PropertiesComboBox.TextField = "EmployeeID";

        DataColumn[] columns = new DataColumn[1];
        columns[0] = table.Columns["ID"];
        

        GridViewDataDateColumn col3 = ASPxGridView1.Columns["StartDate"] as GridViewDataDateColumn;
        GridViewDataDateColumn col4 = ASPxGridView1.Columns["EndDate"] as GridViewDataDateColumn;
        //col4.PropertiesDateEdit.DisplayFormatString = "M/d/yyyy";
        
        
        table.PrimaryKey = columns;

        //if (Session["GridDataEmployee"] == null)
        //{
            Session["GridDataEmployee"] = table;
        //}

        this.ASPxGridView1.DataSource = Session["GridDataEmployee"];
        //this.ASPxGridView1.SettingsPager.Mode = DevExpress.Web.GridViewPagerMode.ShowAllRecords; // this disables pagination, was taking too long if second page clicked

        this.ASPxGridView1.DataBind();

        NavBarDataBind();
    }

    protected void LeftPane_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
        //txtBox.Value = "records may need refresh";
        //NavBarDataBind();

    }

    private void NavBarDataBind()
    {
        //string currentGroup = "Date Filter";
        //DevExpress.Web.NavBarGroup gr = new DevExpress.Web.NavBarGroup();
        //gr.Text = currentGroup;
        //gr.Name = currentGroup;
        //navbar.Groups.Add(gr);

        //string gr1 = "Date Filter";
        //DevExpress.Web.NavBarGroup navBarGr = navbar.Groups.FindByName(gr1);

        //DevExpress.Web.NavBarItem it = new DevExpress.Web.NavBarItem();
        //DevExpress.Web.GridViewDataDateColumn.CreateColumn();
        //string result = "<dx:ASPxDateEdit ID=\"deStart\" ClientInstanceName=\"deStart\" runat=\"server\" Caption=\"Start Date\"></dx:ASPxDateEdit>";
        //it.Text = result;
        //navbar.Groups[navBarGr.Index].Items.Add(it);
    }

    public class Project
    {
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }
    }

    protected void ASPxGridView1_CustomColumnSort(object sender, DevExpress.Web.CustomColumnSortEventArgs e)
    {

    }

    protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
    {

    }

    protected void ASPxGridView1_DataBound(object sender, EventArgs e)
    {

    }

    protected void ASPxGridView1_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        string newProject = e.NewValues["ProjectID"].ToString();
        string newEmployee = e.NewValues["EmployeeID"].ToString();
        string sunVal = !(e.NewValues["Sunday"] == null) ? e.NewValues["Sunday"].ToString() : "0";
        string monVal = !(e.NewValues["Monday"] == null) ? e.NewValues["Monday"].ToString() : "0";
        string tueVal = !(e.NewValues["Tuesday"] == null) ? e.NewValues["Tuesday"].ToString() : "0";
        string wedVal = !(e.NewValues["Wednesday"] == null) ? e.NewValues["Wednesday"].ToString() : "0";
        string thuVal = !(e.NewValues["Thursday"] == null) ? e.NewValues["Thursday"].ToString() : "0";
        string friVal = !(e.NewValues["Friday"] == null) ? e.NewValues["Friday"].ToString() : "0";
        string satVal = !(e.NewValues["Saturday"] == null) ? e.NewValues["Saturday"].ToString() : "0";

        string halfDayVal = !(e.NewValues["HalfDayMin"] == null) ? e.NewValues["HalfDayMin"].ToString() : "NULL";
        string fullDayVal = !(e.NewValues["FullDayMin"] == null) ? e.NewValues["FullDayMin"].ToString() : "NULL";

        string startDate = !(e.NewValues["StartDate"] == null) ? e.NewValues["StartDate"].ToString() : "NULL";
        string endDate = !(e.NewValues["EndDate"] == null) ? e.NewValues["EndDate"].ToString() : "NULL";


        //((DataTable)Session["GridDataEmployee"]).Rows.Add(new object[] {null, newProject, newEmployee, sunVal, monVal, tueVal, wedVal, thuVal, friVal, satVal, halfDayVal, fullDayVal });
        e.Cancel = true;
        ASPxGridView1.CancelEdit();
        ASPxGridView1.DataBind();

        string query = String.Format("INSERT INTO OMV_EmployeeSchedule (ProjectID, EmployeeID, Sunday, Monday, Tuesday, Wednesday, Thursday, Friday,  Saturday, HalfDayMin, FullDayMin, StartDate, EndDate ) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', {9}, {10}, {11}, {12})", newProject, newEmployee, sunVal, monVal, tueVal, wedVal, thuVal, friVal, satVal, halfDayVal, fullDayVal, startDate, endDate);
        int success = DataAccess.ExecuteSqlStatement(query, null);
        if (success <= 0) throw new Exception("Error. Row(s) have not been inserted to database.");
    }

    protected void ASPxGridView1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        string id = e.Keys["ID"].ToString();
        string newProject = e.NewValues["ProjectID"].ToString();
        string newEmployee = e.NewValues["EmployeeID"].ToString();
        string oldProject = e.OldValues["ProjectID"].ToString();
        string oldEmployee = e.OldValues["EmployeeID"].ToString();
        string sunVal = e.NewValues["Sunday"].ToString();
        string monVal = e.NewValues["Monday"].ToString();
        string tueVal = e.NewValues["Tuesday"].ToString();
        string wedVal = e.NewValues["Wednesday"].ToString();
        string thuVal = e.NewValues["Thursday"].ToString();
        string friVal = e.NewValues["Friday"].ToString();
        string satVal = e.NewValues["Saturday"].ToString();
        string halfDayVal = !(e.NewValues["HalfDayMin"] == null) ? e.NewValues["HalfDayMin"].ToString() : "NULL";
        string fullDayVal = !(e.NewValues["FullDayMin"] == null) ? e.NewValues["FullDayMin"].ToString() : "NULL";
        string startDate = !(e.NewValues["StartDate"] == null) ? "'" + e.NewValues["StartDate"].ToString() + "'" : "NULL";
        string endDate = !(e.NewValues["EndDate"] == null) ? "'" + e.NewValues["EndDate"].ToString() + "'" : "NULL";


        object[] eventArgs = new object[1] { e.Keys["ID"]};
        DataRow row = ((DataTable)Session["GridDataEmployee"]).Rows.Find(eventArgs);
        row["ID"] = id;
        row["ProjectID"] = newProject;
        row["EmployeeID"] = newEmployee;
        row["Sunday"] = sunVal;
        row["Monday"] = monVal;
        row["Tuesday"] = tueVal;
        row["Wednesday"] = wedVal;
        row["Thursday"] = thuVal;
        row["Friday"] = friVal;
        row["Saturday"] = satVal;
        row["HalfDayMin"] = halfDayVal;
        row["FullDayMin"] = fullDayVal;
        if (startDate == "NULL")
        {
            row["startDate"] = DBNull.Value;
        }
        else
        {
            row["startDate"] = DateTime.Parse(e.NewValues["StartDate"].ToString());
        }
        if (endDate == "NULL")
        {
            row["endDate"] = DBNull.Value;
        }
        else
        {
            row["endDate"] = DateTime.Parse(e.NewValues["EndDate"].ToString());
        }

        e.Cancel = true;
        ASPxGridView1.CancelEdit();
        ASPxGridView1.DataBind();

        string query = String.Format("UPDATE o SET ProjectID = '{0}', EmployeeID = '{1}', Sunday = '{2}', Monday = '{3}',  Tuesday = '{4}', Wednesday = '{5}', Thursday = '{6}', Friday = '{7}', Saturday = '{8}', HalfDayMin = {9}, FullDayMin = {10}, StartDate = {11}, EndDate ={12} FROM OMV_EmployeeSchedule o  WHERE ID = '{13}' ", newProject, newEmployee, sunVal.Replace("-", "").Replace("1", "-1"), monVal.Replace("-", "").Replace("1", "-1"), tueVal.Replace("-", "").Replace("1", "-1"), wedVal.Replace("-", "").Replace("-", "").Replace("1", "-1"), thuVal.Replace("-", "").Replace("1", "-1"), friVal.Replace("-", "").Replace("1", "-1"), satVal.Replace("-", "").Replace("1", "-1"), halfDayVal, fullDayVal, startDate, endDate, id);
        int success = DataAccess.ExecuteSqlStatement(query, null);
        if (success <= 0) throw new Exception("Error. Row update(s) have not been saved to database.");
    }

    protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        object[] eventArgs = new object[] { e.Keys["ID"] };
        DataRow row = ((DataTable)Session["GridDataEmployee"]).Rows.Find(eventArgs);
        ((DataTable)Session["GridDataEmployee"]).Rows.Remove(row);
        e.Cancel = true;
        ASPxGridView1.DataBind();

        String query = String.Format("DELETE FROM OMV_EmployeeSchedule WHERE ID = '{0}'", e.Values["ID"]);
        int success = DataAccess.ExecuteSqlStatement(query, null);
        if (success <= 0) throw new Exception("Error. Row(s) have not been deleted from database.");
    }

    protected void ASPxGridView1_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
    {
       /* e.NewValues["Sunday"] = "False";
        e.NewValues["Monday"] = "False";
        e.NewValues["Tuesday"] = "False";
        e.NewValues["Wednesday"] = "False";
        e.NewValues["Thursday"] = "False";
        e.NewValues["Friday"] = "False";
        e.NewValues["Saturday"] = "False";
        * */
        
    }

    protected void ASPxGridView1_BeforeColumnSortingGrouping(object sender, DevExpress.Web.ASPxGridViewBeforeColumnGroupingSortingEventArgs e)
    {
        this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["EmployeeSchedule"], string.Empty);
        this.ASPxGridView1.DataBind();
    }

    protected void txtDateTime_DateChanged(object sender, EventArgs e)
    {
        //string start = this.StartDate.Value.ToString();
        //string end = this.EndDate.Value.ToString();
        //string query = String.Format(";WITH cte AS ( SELECT e.EmployeeID, SUBSTRING(e.EmpFName, 1, 1) + LOWER(SUBSTRING(e.EmpFName, 2, LEN(e.EmpFName))) + ' ' + SUBSTRING(e.EmpLName, 1, 1) + LOWER(SUBSTRING(e.EmpLName, 2, LEN(e.EmpLName))) as EmployeeName, ISNULL(e.EmpVac,0) * 8 AS Vacation, ISNULL(e.EmpSick,0) * 8 AS Sick, SUM(case when ActivityID = 'GEN:VAC' THEN ISNULL(TEHours,0) ELSE 0 END) AS UsedVacation, SUM(case when ActivityID = 'GEN:SICK' THEN ISNULL(TEHours,0) ELSE 0 END) AS UsedSick FROM TimeEntry t JOIN Employee e on e.EmployeeID = t.EmployeeID WHERE t.ProjectID LIKE '40%' AND t.CreatedOn > '{0}' AND t.CreatedOn < '{1}' GROUP BY e.EmployeeID, e.EmpVac, e.EmpSick, e.EmpHol, SUBSTRING(e.EmpFName, 1, 1) + LOWER(SUBSTRING(e.EmpFName, 2, LEN(e.EmpFName))) + ' ' + SUBSTRING(e.EmpLName, 1, 1) + LOWER(SUBSTRING(e.EmpLName, 2, LEN(e.EmpLName))) HAVING 1=1 ), cte2 AS ( SELECT e.EmployeeID, SUM(ISNULL(TEHours,0)) as HoursWorked FROM TimeEntry t JOIN Employee e on e.EmployeeID = t.EmployeeID WHERE t.ProjectID NOT LIKE '40%' AND t.CreatedOn > '{0}' AND t.CreatedOn < '{1}' GROUP BY e.EmployeeID, SUBSTRING(e.EmpFName, 1, 1) + LOWER(SUBSTRING(e.EmpFName, 2, LEN(e.EmpFName))) + ' ' + SUBSTRING(e.EmpLName, 1, 1) + LOWER(SUBSTRING(e.EmpLName, 2, LEN(e.EmpLName))) HAVING 1=1 ) SELECT c.*, c2.HoursWorked FROM cte c JOIN cte2 c2 ON c2.EmployeeID = c.EmployeeID", start, end);
        
        //this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(query, string.Empty);
        //this.ASPxGridView1.DataBind();
    }

}
