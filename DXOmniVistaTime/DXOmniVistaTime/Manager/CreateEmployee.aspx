﻿<%@ Page Title="Employee Profile" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="CreateEmployee.aspx.cs" Inherits="Manager_CreateEmployee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <script type="text/javascript">
         var ero = '';
         var ProjectArray = Array();
         function DeleteProjectItem(ItemID,guid) {
             $('#' + guid).remove();
             var index = ProjectArray.indexOf(ItemID);
             if (index > -1) {
                 ProjectArray.splice(index, 1);
             }

            // $('#' + ItemID).remove();
         }
         function generateUUID() {
             var d = new Date().getTime();
             var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                 var r = (d + Math.random() * 16) % 16 | 0;
                 d = Math.floor(d / 16);
                 return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
             });
             return uuid;
         }; 
        
</script>
 <div>
         <div class="PageTitle">
           <span> Employee Profile Details:</span>
         </div>
                <br />               
               <div class="profile">
                        <dx:ASPxTextBox ID="EmployeeID" Caption="Employee ID" runat="server" Enabled ="false" AutoPostBack="false" 
                        CssClass="editor" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption">
                       </dx:ASPxTextBox>
               </div>
              <div class="profile">
                        <dx:ASPxTextBox ID="LoginName" Caption="Login Name" runat="server" Enabled ="false" AutoPostBack="false" 
                        CssClass="editor" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption">      
                       </dx:ASPxTextBox>
               </div>
                <div class="profile">
                        <dx:ASPxTextBox ID="Email" Caption="Email" runat="server"  AutoPostBack="false" 
                        CssClass="editor" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption">                   
                       </dx:ASPxTextBox>
               </div>   

 </div>
    <asp:Panel ID="Panel1" runat="server" Height="170px">
       
        
        <br />
        <dx:ASPxFormLayout ID="ASPxFormLayout1" runat="server" RequiredMarkDisplayMode="Auto" Styles-LayoutGroupBox-Caption-CssClass="layoutGroupBoxCaption"
            AlignItemCaptionsInAllGroups="true" Width="100%">
            <Items>
                <dx:LayoutGroup ShowCaption="false" GroupBoxDecoration="HeadingLine" SettingsItemCaptions-HorizontalAlign="Right">
                    <Items>
                        <dx:LayoutItem ShowCaption="False">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer>
                                    <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" TabAlign="left" Width="100%" TabPosition="Left"  >
                                        <TabPages>
                                            <dx:TabPage Text="Basic Information">
                                                <ContentCollection>
                                                    <dx:ContentControl ID="ContentControl1" runat="server">
                                                         <div class="tabScroll">
                                                            
                                                            <div class="profile_tab">
                                                                <dx:ASPxTextBox ID="EmpFName"  Caption="First Name" runat="server" AutoPostBack="false">
                                                                </dx:ASPxTextBox>

                                                            </div>
                                                            <div class="profile_tab">
                                                                <dx:ASPxTextBox ID="EmpLName" Caption="Last Name" runat="server" AutoPostBack="false">
                                                                </dx:ASPxTextBox>

                                                            </div>
                                                            <div class="profile_tab">
                                                                <dx:ASPxTextBox ID="EmpTitle" Caption="Title" runat="server" AutoPostBack="false">
                                                                </dx:ASPxTextBox>

                                                            </div>
                                                            <div class="profile_tab">
                                                                <dx:ASPxTextBox ID="EmpStreet" Caption="Address One" runat="server" AutoPostBack="false">
                                                                </dx:ASPxTextBox>

                                                            </div>
                                                            <div class="profile_tab">
                                                                <dx:ASPxTextBox ID="EmpStreet2" Caption="Address Two" runat="server" AutoPostBack="false">
                                                                </dx:ASPxTextBox>

                                                            </div>
                                                            <div class="profile_tab">
                                                                <dx:ASPxTextBox ID="EmpCity" Caption="City" runat="server" AutoPostBack="false">
                                                                </dx:ASPxTextBox>

                                                            </div>
                                                            <div class="profile_tab">
                                                                <dx:ASPxTextBox ID="EmpState" Caption="EmpState" runat="server" AutoPostBack="false">
                                                                </dx:ASPxTextBox>

                                                            </div>
                                                            <div class="profile_tab">
                                                                <dx:ASPxTextBox ID="EmpZip" Caption="ZipCode" runat="server" AutoPostBack="false">
                                                                </dx:ASPxTextBox>

                                                            </div>
                                                          </div>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="HR Information">
                                                <ContentCollection>
                                                    <dx:ContentControl ID="ContentControl2" runat="server">
                                                        <div class="tabScroll">
                                                        <div class="profile_tab">
                                                            <dx:ASPxTextBox ID="EmpDateHired" Caption="Hiring Date" runat="server" AutoPostBack="false">
                                                            </dx:ASPxTextBox>
                                                        </div>
                                                        <div class="profile_tab">
                                                            <dx:ASPxTextBox ID="EmpContact" Caption="Contact" runat="server" AutoPostBack="false">
                                                            </dx:ASPxTextBox>
                                                        </div>
                                                        <div class="profile_tab">
                                                            <dx:ASPxTextBox ID="EmpDepartment" Caption="Department" runat="server" AutoPostBack="false">
                                                            </dx:ASPxTextBox>
                                                        </div>
                                                        <div class="profile_tab">
                                                            <dx:ASPxTextBox ID="EmpBillRate" Caption="Bill Rate" runat="server" AutoPostBack="false">
                                                            </dx:ASPxTextBox>
                                                        </div>
                                                        <div class="profile_tab">
                                                            <dx:ASPxTextBox ID="EmpCostRate" Caption="Cost Rate" runat="server" AutoPostBack="false">
                                                            </dx:ASPxTextBox>
                                                        </div>
                                                        <div class="profile_tab">
                                                            <dx:ASPxTextBox ID="EmpMemo" Caption="Memo" runat="server" AutoPostBack="false">
                                                            </dx:ASPxTextBox>
                                                        </div>
                                                        <div class="profile_tab">
                                                            <dx:ASPxTextBox ID="EmpOTCostRate" Caption="OT Cost Rate" runat="server" AutoPostBack="false">
                                                            </dx:ASPxTextBox>
                                                        </div>
                                                        <div class="profile_tab">
                                                            <dx:ASPxTextBox ID="EmpOTBillRate" Caption="OT BillRate" runat="server" AutoPostBack="false">
                                                            </dx:ASPxTextBox>
                                                        </div>
                                                        <div class="profile_tab">
                                                            <dx:ASPxTextBox ID="EmpNextImpDate" Caption="Imp Date" runat="server" AutoPostBack="false">
                                                            </dx:ASPxTextBox>
                                                        </div>
                                                        <div class="profile_tab">
                                                            <dx:ASPxTextBox ID="EmpVac" Caption="Vacations" runat="server" AutoPostBack="false">
                                                            </dx:ASPxTextBox>
                                                        </div>
                                                        <div class="profile_tab">
                                                            <dx:ASPxTextBox ID="EmpSick" Caption="Sick" runat="server" AutoPostBack="false">
                                                            </dx:ASPxTextBox>
                                                        </div>
                                                        <div class="profile_tab">
                                                            <dx:ASPxTextBox ID="EmpHol" Caption="Holidays" runat="server" AutoPostBack="false">
                                                            </dx:ASPxTextBox>
                                                        </div>
                                                            </div>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                             <dx:TabPage Text="Employee Access">
                                                <ContentCollection>
                                                    <dx:ContentControl ID="ContentControl5" runat="server">
                                                      <div class="tabScroll">
                                                        <div class="profile_tab">
                                                                <dx:ASPxTextBox ID="AccessLogin" Caption="Login Name" runat="server" Enabled ="false" AutoPostBack="false" >                                
                                                               </dx:ASPxTextBox>
                                                       </div>
                                                        <div class="profile_tab">
                                                                <dx:ASPxTextBox ID="AccessEmail" Caption="Email" runat="server"  AutoPostBack="false">                   
                                                                </dx:ASPxTextBox>
                                                        </div> 
                                                         <div class="profile_tab">
                                                              <dx:ASPxComboBox ID="managers" Caption="Manager" runat="server"  AllowNull="False" AutoPostBack="false">                                                         
                                                            <ClearButton Visibility="Auto">
                                                            </ClearButton>                                                           
                                                         </dx:ASPxComboBox>
                                                        </div>
                                                    <div class="profile_tab">
                                                            <dx:ASPxTextBox ID="AccessPassword" Caption="Password" runat="server" Enabled ="false" AutoPostBack="false">                                                                
                                                            </dx:ASPxTextBox>
                                                    </div>
                                                    <div> 
                                                        <div class="profile_tab">
                                                          <dx:ASPxListBox ID="roles" runat="server" 
                                                    Width="100%" Height="240px" SelectionMode="Multiple" Caption="Roles">
                                                    <CaptionSettings Position="Top" />
                                                           </dx:ASPxListBox>
                                                        </div>
                                                         
                                                    </div>
                                                       
</div>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                               <dx:TabPage Text="Projects">
                                                <ContentCollection>
                                                    <dx:ContentControl ID="ContentControl4" runat="server">
                                                        <div class="tabScroll">
                                                        <h4 style="float:left;width:100%">Projects Related to 
                                                            <dx:ASPxLabel ID="spanName" runat="server" Text="ASPxLabel"></dx:ASPxLabel>
                                                         </h4>
                                                        <div id="projectsRec">
                                                           <% if (projects_ != null)
                                                              {
                                                                  foreach (System.Data.DataRow r in projects_.Rows)
                                                                  { %>
                                                            <div class="ProjectItem"><h5><%=r["ControlID"].ToString()%> </h5></div>
                                                            <%}
                                                              } %> 
                                                        </div>
                                                            </div>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                             
                                        </TabPages>
                                    </dx:ASPxPageControl>
                                    <dx:ASPxButton ID="ASPxButton2" OnClick="submitButton_Click" runat="server" Text="Save " ImagePosition="Right" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption" AutoPostBack="false" Image-Url="~/Content/Images/Icons/check-64-icon.png" Image-Height="20px" Image-Width="20px" Height="36px">
                                        <Image Height="20px" Url="~/Content/Images/Icons/check-64-icon.png" Width="20px">
                                        </Image>
                                    </dx:ASPxButton>
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                    </Items>
                    <SettingsItemCaptions HorizontalAlign="Right" />
                </dx:LayoutGroup>

            </Items>

            <Styles>
                <LayoutGroupBox>
                    <Caption CssClass="layoutGroupBoxCaption">
                    </Caption>
                </LayoutGroupBox>
            </Styles>

        </dx:ASPxFormLayout>
            <br />
        
           <br /><br /><br /><br />
    </asp:Panel>
   
   
</asp:Content>

