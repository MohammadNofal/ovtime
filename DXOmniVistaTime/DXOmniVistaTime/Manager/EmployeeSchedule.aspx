﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Main.master" CodeFile="EmployeeSchedule.aspx.cs" Inherits="EmployeeSchedule" %>

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" ClientInstanceName="ASPxGridView1" Width="70%" AutoPostBack="true" KeyFieldName="ID"
        OnRowInserting = "ASPxGridView1_RowInserting"
        OnRowDeleting = "ASPxGridView1_RowDeleting"
        OnRowUpdating = "ASPxGridView1_RowUpdating"
        OnDataBinding ="ASPxGridView1_DataBinding" 
        OnDataBound = "ASPxGridView1_DataBound"
        OnBeforeColumnSortingGrouping="ASPxGridView1_BeforeColumnSortingGrouping">
        <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
        <Paddings Padding="0px" />
        <Styles>
            <Header Wrap="True">
            </Header>
            <AlternatingRow Enabled="true" />
        </Styles>
        <Paddings PaddingLeft="10px" />
        <SettingsPager PageSize="50" />
        <Settings ShowFooter="True" />
        <SettingsBehavior ConfirmDelete="true" />
        <SettingsText ConfirmDelete="Do you want delete this row?" />
        <SettingsEditing Mode="Batch" />
        <Columns>
            <dx:GridViewDataColumn FieldName="ID" HeaderStyle-Font-Size="Medium" Visible="false">
            </dx:GridViewDataColumn>
            <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="False" ShowNewButtonInHeader="True" VisibleIndex="0" Width="1%">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataComboBoxColumn FieldName="ProjectID" HeaderStyle-Font-Size="Medium">
                <PropertiesComboBox ValueType="System.String">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn FieldName="EmployeeID" HeaderStyle-Font-Size="Medium" >
                <PropertiesComboBox ValueType="System.String">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataCheckColumn FieldName="Sunday" HeaderStyle-Font-Size="Medium">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataCheckColumn FieldName="Monday" HeaderStyle-Font-Size="Medium">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataCheckColumn FieldName="Tuesday" HeaderStyle-Font-Size="Medium">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataCheckColumn FieldName="Wednesday" HeaderStyle-Font-Size="Medium">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataCheckColumn FieldName="Thursday" HeaderStyle-Font-Size="Medium">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataCheckColumn FieldName="Friday" HeaderStyle-Font-Size="Medium">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataCheckColumn FieldName="Saturday" HeaderStyle-Font-Size="Medium">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataColumn FieldName="HalfDayMin" HeaderStyle-Font-Size="Medium">
            </dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="FullDayMin" HeaderStyle-Font-Size="Medium">
            </dx:GridViewDataColumn>
            <dx:GridViewDataDateColumn FieldName="StartDate" HeaderStyle-Font-Size="Medium">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataDateColumn FieldName="EndDate" HeaderStyle-Font-Size="Medium">
            </dx:GridViewDataDateColumn>
        </Columns>
        <Styles Header-Wrap="True" >
            <Header Wrap="True">
            </Header>
            <AlternatingRow Enabled="True">
            </AlternatingRow>
        </Styles>
        <Paddings PaddingTop="10px" />
        <Border BorderWidth="0px" />
        <BorderBottom BorderWidth="1px" />
    </dx:ASPxGridView>

</asp:Content>
