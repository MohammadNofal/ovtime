﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="Notification.aspx.cs" Inherits="Manager_Notification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <script src="../Content/js/Modal.js"></script>
    <style>
        .detailPanelLarge {
            height:36px;
            margin-bottom:0px;
       }
         .editorText {
            margin-left: 27px;
            margin-right: 35px;
        }
        .editorCaption {
            padding-left:13px;
        }
        </style>
     <dx:ASPxCallbackPanel ID="DetailPanel" runat="server" ClientInstanceName="FilterPanel" Width="100%" CssClass="detailPanelLarge" Collapsible="false"  SettingsLoadingPanel-Enabled ="false" >
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
<SettingsLoadingPanel Enabled="False"></SettingsLoadingPanel>

        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                 <label style="float:left;width:30%;">Notification List... </label> 
            

            </dx:PanelContent>
        </PanelCollection>
        <Paddings Padding="8px" PaddingBottom="2px" />
    </dx:ASPxCallbackPanel>

     <dx:ASPxPanel ID="EmployeeSelectorPanel" runat="server" ClientInstanceName="userdetailpanel">
            <PanelCollection>
                <dx:PanelContent>
      <% foreach(System.Data.DataRow row in allNot.Rows){ %>
                    <blockquote id="blockquote_<%=row["id"] %>" style=" padding: 10px 20px;
    margin: 0 0 20px;
    font-size: 17.5px;
    border-left: 5px solid #eee;
    margin: 10px;
    border-bottom: 1px solid #eee;
    border-top: 1px solid #eee;
    border-right: 1px solid #eee;
    width: 97%;">
  <p><%=row["Message"]%>
<i onclick="deleteNot('<%=row["id"] %>')" class="fa fa-times-circle" aria-hidden="true" style="    float: right;
    color: #b7adad;cursor:pointer"></i>
  </p>
                        
</blockquote>
                    <% } %>



    
     </dx:PanelContent>
            </PanelCollection>
           
        </dx:ASPxPanel>
     <script type="text/javascript">
         // <![CDATA[
         ASPxClientControl.GetControlCollection().ControlsInitialized.AddHandler(function (s, e) {

             UpdateGridHeightCom(Notifications);
         });
         ASPxClientControl.GetControlCollection().BrowserWindowResized.AddHandler(function (s, e) {

             UpdateGridHeightCom(Clients);
         });
         // ]]> 
         function deleteNot(id) {
             
             $.ajax({
                 type: "POST",
                 url: "Notification.aspx/Delete",
                 data: "{id :'" + id + "'}",
                 contentType: "application/json; charset=utf-8",

             }).done(function () {

                 $('#blockquote_' + id).remove();

             });
         }
        </script>
</asp:Content>

