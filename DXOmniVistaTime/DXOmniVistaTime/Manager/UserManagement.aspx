﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Main.master" CodeFile="UserManagement.aspx.cs" Inherits="Manager_UserManagement" %>

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    
    <dx:ASPxPanel ID="GridViewPannel" runat="server" ClientInstanceName="GridViewPannel">
        <PanelCollection>
            <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" KeyFieldName="EmployeeID" EnablePagingCallbackAnimation="True" OnRowDeleting="ASPxGridView1_RowDeleting" OnRowUpdated="ASPxGridView1_RowUpdated" OnRowUpdating="ASPxGridView1_RowUpdating" OnRowInserting="ASPxGridView1_RowInserting" OnHtmlRowPrepared="ASPxGridView1_HtmlRowPrepared" OnRowValidating="ASPxGridView1_RowValidating">
                    <SettingsPager Mode="ShowAllRecords">
                    </SettingsPager>
                    <Settings ShowGroupPanel="True" ShowFooter="True" />
                    <SettingsBehavior AllowGroup="False" ConfirmDelete="True" />
                    <SettingsCommandButton>
                        <NewButton Text="New">
                        </NewButton>
                    </SettingsCommandButton>
                    <SettingsText ConfirmDelete="Are you sure?" /> 
                    <EditFormLayoutProperties ColCount="11">
                    </EditFormLayoutProperties>
                    <Columns>
                        
                        <dx:GridViewCommandColumn ShowNewButton="true" ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" VisibleIndex="0" >
                           
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="EmployeeID" Name="Employee ID" ShowInCustomizationForm="True" VisibleIndex="1" Caption="ID">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="EmpFName" Name="First Name" ShowInCustomizationForm="True" VisibleIndex="2" Caption="First Name">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="EmpDateHired" Name="Hiring" ShowInCustomizationForm="True" VisibleIndex="4" Caption="Hired Date">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="EmpLName" Name="Last Name" ShowInCustomizationForm="True" VisibleIndex="3" Caption="Last Name">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Title" FieldName="EmpTitle" Name="EmpTitle" ShowInCustomizationForm="True" VisibleIndex="5">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Email" FieldName="EmpEmail" Name="EmpEmail" ShowInCustomizationForm="True" VisibleIndex="6">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Status" FieldName="Status" Name="Status" ShowInCustomizationForm="True" VisibleIndex="7">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="From" FieldName="EmpCountry" Name="EmpCountry" ShowInCustomizationForm="True" VisibleIndex="8">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Login" FieldName="LoginID" Name="LoginID" ShowInCustomizationForm="True" VisibleIndex="9">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <Styles AdaptiveDetailButtonWidth="22">
                    </Styles>
                </dx:ASPxGridView>
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"></asp:ObjectDataSource>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxPanel>
   
</asp:Content>

