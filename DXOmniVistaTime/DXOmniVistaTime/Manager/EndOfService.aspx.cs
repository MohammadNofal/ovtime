﻿using DevExpress.Web;
using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Runtime.Serialization.Json;
using DevExpress.Utils.OAuth.Provider;
using System.IO;
using System.Text;
using Newtonsoft.Json;
public partial class EndOfService : System.Web.UI.Page
{
    private DataTable dt_grid = new DataTable();
    private Dictionary<String, RootObject> hash = new Dictionary<String, RootObject>();


    protected void Page_Load(object sender, EventArgs e)
    {

        //Works for first time
        if (!IsPostBack)
        {
            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            Session["HashMap"] = null;
            Session["EndOfServiceDataGrid"] = null;
            Session["SelectedValues"] = null;
            bool isManager = checkifmanager();
            if (isManager)
            {
                GetEmployees();
                AddEmployeesToList();
                refreshDataGrid();
            }
            else
            {
                ASPxDropDownEdit1.Text = Membership.GetUser(User.Identity.Name).ToString();
                ASPxDropDownEdit1.Enabled = false;
                GetEmployeeDetailsFromServer("" + Membership.GetUser(User.Identity.Name));
                if (Session["HashMap"] != null)
                {
                    foreach (KeyValuePair<String, RootObject> v in (Dictionary<String, RootObject>)(Session["HashMap"])) //only 1 item in hash
                    {
                        AddEmployeeToDataGrid(v.Value);
                    }
                }
                refreshDataGrid();
            }
        }
        else
        {

        }
     }
    //Mohammad added this
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["EndOfServiceDataGrid"] != null)
        {
            this.ASPxGridView1.DataSource = (DataTable)Session["EndOfServiceDataGrid"];
            this.ASPxGridView1.DataBind();
        }
    }
    //.......................

    private bool checkifmanager()
    {
        DataTable dt = DataAccess.GetDataTableBySqlSyntax("SELECT EmpFName + EmpLName as 'EmployeeName', EmployeeID FROM Employee WHERE ManagerID='" + Membership.GetUser(User.Identity.Name) + "'", "");
        if (dt.Rows.Count == 0)
        {
            return false;
        }
        return true;
    }

    private void GetEmployees()
    {
        DataTable dt_employees = DataAccess.GetDataTableBySqlSyntax("SELECT e.EmployeeID, e.EmpFName + ' ' + e.EmpLName as 'EmployeeName' FROM Employee e WHERE e.ManagerID='" + Membership.GetUser(User.Identity.Name) + "'", "");
        DataTable logedinUser = DataAccess.GetDataTableBySqlSyntax("SELECT e.EmployeeID, e.EmpFName + ' ' + e.EmpLName as 'EmployeeName' FROM Employee e WHERE e.EmployeeID='" + Membership.GetUser(User.Identity.Name) + "'", "");

        dt_employees.Merge(logedinUser);
        Session["EmployeeNameData"] = dt_employees;
    }

    private void AddEmployeesToList()
    {
        ASPxListBox list = ASPxDropDownEdit1.FindControl("listBox") as ASPxListBox;
        if (Session["EmployeeNameData"] != null)
        {
            foreach (DataRow row in ((DataTable)Session["EmployeeNameData"]).Rows)
            {
                list.Items.Add(row.Field<String>("EmployeeName"), row.Field<String>("EmployeeID"));//.Add("Text", "Value");
            }
            list.DataBind();
        }
    }

    private void refreshDataGrid()
    {
        ASPxGridView1.DataSource = (DataTable)Session["EndOfServiceDataGrid"];
        ASPxGridView1.DataBind();
        if (ASPxGridView1.VisibleRowCount == 0) ASPxGridView1.Visible = false;
        else ASPxGridView1.Visible = true;
    }

    protected void GridViewPannel_CallBack(object sender, CallbackEventArgsBase e)
    {
        String[] SelectedValues = e.Parameter.Split(',');
        Session["SelectedValues"] = SelectedValues;
        if ((DataTable)Session["EndOfServiceDataGrid"] != null)//Session is null initially 
        {
            dt_grid = (DataTable)Session["EndOfServiceDataGrid"];
        }
        dt_grid.Clear();
        Session["EndOfServiceDataGrid"] = dt_grid;
       
        if (Session["HashMap"] != null)
        {
           
           hash = (Dictionary<String, RootObject>)(Session["HashMap"]);
        }

        for (int i = 0; i < SelectedValues.Length; i++)
        {
            if (!hash.ContainsKey(SelectedValues[i])) //if Hash already contains employee JSON then dont POST
            { 
                GetEmployeeDetailsFromServer(SelectedValues[i]);
            }
        }
        //for loop get employees from Hash then add to Grid DataTable
        if (Session["HashMap"] != null)
        {
            foreach (KeyValuePair<String, RootObject> v in (Dictionary<String, RootObject>)(Session["HashMap"]))
            {
                if (SelectedValues.Contains(v.Key)) AddEmployeeToDataGrid(v.Value);
            }
        }
        refreshDataGrid();

    }

    private void GetEmployeeDetailsFromServer(String employeeID)
    {
        if (!employeeID.Equals("") && !employeeID.Equals("SelectAll"))
        {
            WebRequest req = WebRequest.Create(@ConfigurationManager.AppSettings["EndOfService"] + employeeID); 
            req.Method = "GET";
            try
            {
                HttpWebResponse resp = req.GetResponse() as HttpWebResponse;
                Object result;
                if (resp.StatusCode == HttpStatusCode.OK)
                {
                    using (Stream respStream = resp.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(respStream, Encoding.UTF8);
                        result = reader.ReadToEnd();
                        String resultS = (String)result;
                        RootObject JSONobj = JsonConvert.DeserializeObject<RootObject>(resultS); //Create JSONobject for employee
                        if (Session["HashMap"] != null)
                        {
                            hash = (Dictionary<String, RootObject>)(Session["HashMap"]);
                        }
                        hash.Add(employeeID, JSONobj); //Add JSONobject to the hash
                        Session["HashMap"] = hash;
                    }
                }
                else
                {
                    //Error connecting to database
                    //Console.WriteLine(string.Format("Status Code: {0}, Status Description: {1}", resp.StatusCode, resp.StatusDescription));
                }
            }
            catch(WebException ex)
            {
                //Error connecting to database
            }
            // Console.Read();

        }

    }

    private void AddEmployeeToDataGrid(RootObject JSONobj)
    {
        //Create DataTable for Employee
        DataTable empDetails = new DataTable();
        empDetails.Columns.Add("EmployeeId", typeof(String));
        empDetails.Columns.Add("HireDate", typeof(String));
        empDetails.Columns.Add("CashLiability", typeof(String));
        DataRow row = empDetails.NewRow();//Use JSONobj to add new row to empDetails
        row[0] = JSONobj.EmployeeId;
        row[1] = JSONobj.HireDate;
        row[2] = JSONobj.CashLiability;
        empDetails.Rows.Add(row);
        DataTable dt_grid = new DataTable();
        if (Session["EndOfServiceDataGrid"] != null) dt_grid = (DataTable)Session["EndOfServiceDataGrid"];
        dt_grid.Merge(empDetails);
        Session["EndOfServiceDataGrid"] = dt_grid;
    }

    private class RootObject //incomplete
    {
        public String EmployeeId { get; set; }
        public String HireDate { get; set; }
        public String CashLiability { get; set; }
    }
}