﻿using DevExpress.Export;
using DevExpress.Web;
using DevExpress.XtraPrinting;
using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_EmployeeHrs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ToString());
        //LoadData();
        GetEmployees();
        GetProjects();
        //Filter_Click(null, null);
    }

    //Mohammad added this
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["EmployeeHrsDataGrid"] != null)
        {
            this.ASPxGridView1.DataSource = (DataTable)Session["EmployeeHrsDataGrid"];
            this.ASPxGridView1.DataBind();
        }
    }
    //.......................

    protected void EmployeesComboBox_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.EmployeesComboBox.Value == null) return;
        string EmployeeID = this.EmployeesComboBox.Value.ToString();
        LoadData(EmployeeID);
    }
    public void LoadData(string query = "")
    {
        ASPxGridView1.Visible = true;

        if (query != string.Empty && query != "1=1")
        {
           // string sql = "SELECT p2.ProjectID,p2.ProjectName,p1.[EmployeeID/Vendor] as 'EmployeeID',sum(p1.ProjectHrs) as TotalHrs FROM	[dbo].[vw_OVS_ProjectDetailByEmployeeVendorNoSuper] p1 
            //Join Project p2 on (p1.MasterProjectID+':')  = p2.projectid where  " + query + " group by p1.[EmployeeID/Vendor],p2.ProjectName,p2.ProjectID";
            string sql = "EXEC OVS_GetEmployeeHours " + query;
            this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(sql, "");
            Session["EmployeeHrsDataGrid"] = this.ASPxGridView1.DataSource; //Mohammad added this
            this.ASPxGridView1.DataBind();
        }
    }
    public void GetProjects()
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ToString());
        //Mohammad changed the following
        //this.ProjectsComboBox.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["ParentProjects"], "");
        string sql = ConfigurationManager.AppSettings["ParentProjects"];
        this.ProjectsComboBox.DataSource = DataAccess.GetDataTableBySqlSyntax(sql, "");
        //.....................
        this.ProjectsComboBox.ValueField = "ProjectID";
        this.ProjectsComboBox.TextField = "ProjectID";
        this.ProjectsComboBox.DataBind();
    }
    public void GetEmployees()
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ToString());
        string sql = ConfigurationManager.AppSettings["EmployeeIDsNames"];
        this.EmployeesComboBox.DataSource = DataAccess.GetDataTableBySqlSyntax(sql, "");
        this.EmployeesComboBox.ValueField = "EmployeeID";
        this.EmployeesComboBox.TextField = "FullName";
        this.EmployeesComboBox.DataBind();
    }

    protected void Filter_Click(object sender, EventArgs e)
    {
        string query = BuildQuery();
        if (query != string.Empty)
        {
            LoadData(query);
        }
    }
    public string BuildQuery()
    {
        string query = string.Empty;

        if (this.EmployeesComboBox.Value != null)
        {
            //if (query != string.Empty) query += " AND ";
            query += "'" + this.EmployeesComboBox.Value + "'";
        }
        else
        {
            query += "null";
        }
        query += " , ";

        if (this.ProjectsComboBox.Value != null)
        {
            query += "'" + this.ProjectsComboBox.Value.ToString() + "'";
        }
        else
        {
            query += "null";
        }

        query += " , ";
       
        if (this.From.Value != null)
        {
            query += "'" + this.From.Value.ToString() + "'";
        }
        else
        {
            query += "null";
        }

        query += " , ";
        if (this.To.Value != null)
        {
            query += "'" + this.To.Value.ToString() + "'";
        }
        else
        {
            query += "null";
        }
        
        //if (query == string.Empty) query = "1=1";S
        return query;
    }
    protected void EportBtn_ServerClick(object sender, EventArgs e)
    {
        if (this.EmployeesComboBox.Value != null)
        {
            gridExport.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.DataAware, SheetName = this.EmployeesComboBox.Value.ToString() });
        }
        else
        {
            gridExport.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.DataAware, SheetName = "Employee Hrs" });
        }
    }

    protected void ASPXDateEdit_CustomCell(object sender, CalendarDayCellPreparedEventArgs e)
    {
        // #c00000 (red active)
        // #333333 (black active)
        // #ececec (grey not active)
        if (!e.IsOtherMonthDay)
        {
            if (e.Date.DayOfWeek == DayOfWeek.Sunday) e.Cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#333333");
            else if (e.Date.DayOfWeek == DayOfWeek.Friday) e.Cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#c00000"); //#c00000
        }
    }
}