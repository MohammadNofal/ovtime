﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/OVMaster.master"  CodeFile="DashboardNew.aspx.cs" Inherits="Manager_DashboardNew" %>

<%@ Register Assembly="DevExpress.XtraCharts.v15.2.Web, Version=15.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v15.2, Version=15.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="dx" %>


<asp:Content ID="header_" ContentPlaceHolderID="Header" runat="server">
<!-- DevExtreme dependencies -->
<script type="text/javascript" src="../Script/dx/js/jquery-2.1.4.min"></script>
<script type="text/javascript" src="../Script/DX/js/globalize.js"></script>
<script type="text/javascript" src="../Script/DX/js/dx.chartjs.js"></script>
<!-- DevExtreme themes -->
<link rel="stylesheet" type="text/css" href="../Script/dx/css/dx.common.css" />
<link rel="stylesheet" type="text/css" href="../Script/dx/css/dx.light.css" />
<!-- A DevExtreme library -->
<script type="text/javascript" src="../Script/dx/js/dx.all.js"></script>
<style>
        .chart {
            background-color: lightblue;
        }
        .month1_border 
        {
            border-left-width: 2px !important;
        }
        .head_container 
        {
            background-color: #EEE;
            float: right;
            margin-right: 7.8%;
        }

        .Month1_css 
        {
            border-style: solid;
            border-width: medium;
            float: right;
            background-color: red;
        }
        .Month2_css 
        {
            border-style: solid;
            border-width: medium;
            float: right;
        }
        .delete_icon 
        {
            position: relative;
            font-family: "Courier New" !important;
            color: #ee784a !important;
            font-size: 200% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .delete_icon:hover 
        {
            color: #FF0000 !important;
        }
        .delete_icon::before 
        {
            content: "\000D7" !important;
        }
        .new_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 20px !important;
            padding: 1px 1px;
            text-decoration: none !important;
        }

        .new_icon:hover 
        {
            color: #7373FF !important;
        }
        .new_icon:before 
        {
           content: "\f067" !important;
        }
        .edit_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #f3cb76 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .edit_icon:hover 
        {
            color: #FF8000 !important;
        }
        .edit_icon:before 
        {
            content: "\f044" !important;
        }
        .location_icon
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
        }
        .location_icon:before 
        {
            content: "\f041" !important;
        }
        .book_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
        }
        .book_icon:before 
        {
            content: "\f02d" !important;
        }
        .cancel_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #ee784a !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .cancel_icon:hover 
        {
            color: #FF8080 !important;
        }
        .cancel_icon:before
        {
            content: "\f0e2" !important;
        }
        .update_icon
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .update_icon:hover 
        {
            color: #7373FF !important;
        }
        .update_icon:before 
        {
            content: "\f05d " !important;
        }
        .header 
        {
            font-size: 1.25em !important;
        }
        .ProjectDescription 
        {
            font-size: 14px !important;
        }
     .Dashboard_Chart {

     }
     .TextBoxEdit {
            width: 100%;
            padding: 7px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }
     .SubmitButton {
              background-image: none !important;
              background-color: #4CAF50;
              color: white;
              padding: 6px 10px;
              border: none;
              border-radius: 4px;
              cursor: pointer;
              margin-top: 0px;
         }
     /*Mohammad added thead following*/ 
      .print_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #222222 !important;
            font-size: 20px !important;
            padding: 1px 1px 1px 1px;
            text-decoration: none !important;
        }
        .print_icon:hover 
        {
            color: #1d1d1d !important;
        }
        .print_icon:before 
        {
           content: "\f02f" !important;
        }
        .save_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #222222 !important;
            font-size: 20px !important;
            padding: 1px 1px 1px 1px;
            text-decoration: none !important;
        }
        .save_icon:hover 
        {
            color: #1d1d1d !important;
        }
        .save_icon:before 
        {
           content: "\f0c7" !important;
        }
         .view_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #222222 !important;
            font-size: 20px !important;
            padding: 1px 1px 1px 1px;
            text-decoration: none !important;
        }
        .view_icon:hover 
        {
            color: #1d1d1d !important;
        }
        .view_icon:before 
        {
           content: "\f06e" !important;
        }
        .dxmLite .dxm-horizontal.dxmtb .dxm-image-l .dxm-content{
            padding: 0px;
        }
         .dxeButtonEditButton{
            background: none !important;
            border: none !important;
        }
        /*.............................*/
    </style>
    <script>
        var usrName = '<%=HttpUtility.JavaScriptStringEncode(HttpContext.Current.User.Identity.Name)%>';

        function onDateChange(s, e) {
            GridViewPannel.PerformCallback();
        }
        function onSubmition(s, e) {
            ASPxButton2_Click();
            GridViewPannel.PerformCallback();
        }
        function onGridChange(s, e) {
            detailPanelSmall.PerformCallback();
        }

        var oldwidth = 0;
        var showExport = true;
        $(document).ready(function () {

            var height = Math.max(0, document.documentElement.clientHeight);
            var width = Math.max(0, document.documentElement.clientWidth);

            if (width <= 1023) {
                showExport = false;
            }
            else {
                showExport = true;
            }

            //run on first instance
            if (oldwidth == 0) {
                var height = Math.max(0, document.documentElement.clientHeight);
                var width = Math.max(0, document.documentElement.clientWidth);
                if (width <= 1023) {
                    showExport = false;
                }
                else {
                    showExport = true;
                }

                //show/hide export button first
                detailPanelSmall.PerformCallback();

                //resize grid
                ASPxGridView1.PerformCallback(height + ";" + width);
            }
            //update width
            oldwidth = $(window).width();

            //fire on resize
            $(window).resize(function () {
                var nw = $(window).width();
                //compare new and old width      
                if (oldwidth != nw) {
                    var height = Math.max(0, document.documentElement.clientHeight);
                    var width = Math.max(0, document.documentElement.clientWidth);
                    if (width <= 1023) {
                        showExport = false;
                    }
                    else {
                        showExport = true;
                    }


                    //show/hide export button first
                    detailPanelSmall.PerformCallback();

                    //resize grid

                    ASPxGridView1.PerformCallback(height + ";" + width);
                }

                oldwidth = nw;
            });
        });
    </script>
    <script type="text/javascript">
        var textSeparator = ";";
        function Export() {
            console.log('here');
            Callback.PerformCallback("Excel");
        }
        function OnListBoxSelectionChanged(listBox, args) {
            if (args.index == 0)
                args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
            UpdateSelectAllItemState();
            UpdateText();
        }
        function UpdateSelectAllItemState() {
            IsAllSelected() ? checkListBox.SelectIndices([0]) : checkListBox.UnselectIndices([0]);
        }
        function IsAllSelected() {
            var selectedDataItemCount = checkListBox.GetItemCount() - (checkListBox.GetItem(0).selected ? 0 : 1);
            return checkListBox.GetSelectedItems().length == selectedDataItemCount;
        }
        function UpdateText() {
            var selectedItems = checkListBox.GetSelectedItems();
            checkComboBox.SetText(GetSelectedItemsText(selectedItems));
        }
        function SynchronizeListBoxValues(dropDown, args) {
            checkListBox.UnselectAll();
            var texts = dropDown.GetText().split(textSeparator);
            var values = GetValuesByTexts(texts);
            checkListBox.SelectValues(values);
            UpdateSelectAllItemState();
            UpdateText(); // for remove non-existing texts
        }
        function GetSelectedItemsText(items) {
            var texts = [];
            for (var i = 0; i < items.length; i++)
                if (items[i].index != 0)
                    texts.push(items[i].text);
            return texts.join(textSeparator);
        }
        function GetValuesByTexts(texts) {
            var actualValues = [];
            var item;
            for (var i = 0; i < texts.length; i++) {
                item = checkListBox.FindItemByText(texts[i]);
                if (item != null)
                    actualValues.push(item.value);
            }
            return actualValues;
        }
    </script>
    <h4><span class="text-semibold">Manager / Projects Hours Dashboard</span></h4>
    <div class="heading-elements">
                            <div class="heading-btn-group">
                                <a href="#" class="btn btn-link btn-float has-text" >
<%--                                    <i class="pdf_icon text-primary"></i><span>PDF</span>--%>
                                </a>
<%--                                <a runat="server" id="ExportBtn" onserverclick="ExportBtn_ServerClick" class="btn btn-link btn-float has-text"><i class="excel_icon  text-primary"></i><span>Excel</span></a> --%>
                            </div>
                        </div>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="ContentPage" runat="server">

   <%-- <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { LoadingPanel.Hide(); }" />
    </dx:ASPxCallback>
    <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel"
        Modal="True">
    </dx:ASPxLoadingPanel>--%>
    
  
    
    <dx:ASPxCallbackPanel ID="GridViewPannel" Width="100%" runat="server" ClientInstanceName="GridViewPannel" Collapsible="false"   SettingsLoadingPanel-Enabled ="true" >
        <PanelCollection>
            <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                <div class="demo-container">
            <!-- <div id="chartContainer" style="height:5000px; max-width:700px; margin: 0 auto"> -->
   
            <!--WebChart Filter -->
                    <dx:PanelContent ID="PanelContent3" runat="server" SupportsDisabledAttribute="True"> 
                     <table id="table" style="width: 99%; margin-left: 7px;">
                           <tr>
                               <td style="width:18.66%;">
                        <dx:ASPxComboBox ID="ClientsCombobox" DropDownStyle="DropDown" Caption="Clients" runat="server" Width="100%"
                            CaptionCellStyle-Paddings-PaddingTop="10px"
                            ValueField="CLIENTID" TextField="CLIENTID" CssClass="TextBoxEdit" RootStyle-CssClass="editorContainer">
                            <DropDownButton>
                                <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                            </DropDownButton>
                            <RootStyle CssClass="editorContainer"></RootStyle>
                        </dx:ASPxComboBox>
                    </td>
                        <td style="width:18.66%;">
                        <dx:ASPxComboBox ID="ProjectsCombobox" DropDownStyle="DropDown" Caption="Projects" Width="100%" 
                             CaptionCellStyle-Paddings-PaddingTop="10px"  CaptionCellStyle-Paddings-PaddingLeft="4px"
                            runat="server" ValueField="projectId" TextField="projectId" CssClass="TextBoxEdit" RootStyle-CssClass="editorContainer"> 
                            <DropDownButton>
                                <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                            </DropDownButton>
                    <RootStyle CssClass="editorContainer"></RootStyle>
                    </dx:ASPxComboBox> 
                    </td>
                         <td style="width:18.66%;"> 
                    <dx:ASPxDateEdit ID="From" Caption="From Date"  runat="server" CssClass="TextBoxEdit" RootStyle-CssClass="editorContainer" 
                        OnCalendarDayCellPrepared="ASPXDateEdit_CustomCell"  CaptionCellStyle-Paddings-PaddingTop="10px"
                         CaptionCellStyle-Paddings-PaddingLeft="4px" Width="100%">
                        <DropDownButton>
                                <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                            </DropDownButton>
                    <RootStyle CssClass="editorContainer"></RootStyle>
                    </dx:ASPxDateEdit> 
                   </td>
                   <td style="width:18.66%;">
                    <dx:ASPxDateEdit ID="To"  runat="server" Caption="To Date" OnCalendarDayCellPrepared="ASPXDateEdit_CustomCell" CssClass="TextBoxEdit" Width="100%" RootStyle-CssClass="editorContainer"  CaptionCellStyle-Paddings-PaddingTop="10px"
                         CaptionCellStyle-Paddings-PaddingLeft="4px">
                        <DropDownButton>
                                <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                            </DropDownButton>
                        <RootStyle CssClass="editorContainer"></RootStyle>
                    </dx:ASPxDateEdit>
                  </td>
                    <td style="width:18.66%;">
                    <dx:ASPxComboBox ID="DisciplineComboBox" Caption="Discipline" DropDownStyle="DropDown" runat="server"  Width="100%"
                         CaptionCellStyle-Paddings-PaddingTop="10px"  CaptionCellStyle-Paddings-PaddingLeft="4px"
                        ValueField="RoleName" TextField="RoleName"  CssClass="TextBoxEdit" RootStyle-CssClass="editorContainer"> 
                        <DropDownButton>
                                <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                            </DropDownButton>
                        <RootStyle CssClass="editorContainer"></RootStyle>
                    </dx:ASPxComboBox>
                   </td>
  <td style="width:6.66%; margin: 10px;
            float: left;
            margin-bottom: 24px; padding-bottom: 4px;">
           <dx:ASPxButton ID="filter" Text="Submit" runat="server" AutoPostBack="false" CssClass="SubmitButton" >
               <ClientSideEvents Click="function(s,e){  ASPxHiddenScreenWidth.Set('Width',document.getElementById('table').offsetWidth -13);
                   WebChartControl1Pannel.PerformCallback();
                  
                   }" />
           </dx:ASPxButton>
         </td>
                               </tr>
                               </table>
         </dx:PanelContent>
                     <div style="overflow-y: auto;height: calc(100vh - 267px);">
<dx:ASPxCallbackPanel ID="WebChartControl1Pannel" runat="server" ClientInstanceName="WebChartControl1Pannel" OnCallback="WebChartControl_CallBack" Collapsible="false" SettingsLoadingPanel-Enabled ="false" >

<SettingsLoadingPanel Enabled="False"></SettingsLoadingPanel>
            <PanelCollection>
                <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
                    <div style=" margin-left: 7px;">
        <!--WebChart Toolbar-->
       
            <dx:ASPxMenu Visible="false" ID="DashboardToolbar" TextField="" runat="server" ClientInstanceName="mnuToolbar" ApplyItemStyleToTemplates="True" EnableViewState="False" ShowAsToolbar="true">
                <Items>
                    <dx:MenuItem Name="mnuPrint" Text="" ToolTip="Print the chart" Image-AlternateText ="" Image-SpriteProperties-CssClass="nothing">
                        <ItemStyle CssClass="print_icon" />
                       <%-- <Image Url="~/Toolbar/BtnPrint.png" />--%>
                    </dx:MenuItem>
                    <dx:MenuItem Name="mnuSaveToDisk" Text="" ToolTip="Export a chart and save it to the disk" BeginGroup="True" Image-AlternateText ="" Image-SpriteProperties-CssClass="nothing">
                        <ItemStyle CssClass="save_icon" />
                        <%-- <Image Url="~/Toolbar/BtnSave.png" />--%>
                    </dx:MenuItem>
                    <dx:MenuItem Name="mnuSaveToWindow" Text="" ToolTip="Export a chart and show it in a new window"  Image-AlternateText ="" Image-SpriteProperties-CssClass="nothing">
                        <ItemStyle CssClass="view_icon" />
                        <%--<Image Url="~/Toolbar/BtnSaveWindow.png" />--%>
                    </dx:MenuItem>
                    <dx:MenuItem Name="mnuFormat">
                        <Template>
                            <dx:ASPxComboBox runat="server" Width="60px" ValueType="System.String" ID="cbFormat" SelectedIndex="0" ClientInstanceName="cbFormat">
                                <Items>
                                    <dx:ListEditItem Value="pdf" Text="pdf" />
                                    <dx:ListEditItem Value="xls" Text="xls" />
                                    <dx:ListEditItem Value="png" Text="png" />
                                    <dx:ListEditItem Value="jpeg" Text="jpeg" />
                                    <dx:ListEditItem Value="bmp" Text="bmp" />
                                    <dx:ListEditItem Value="tiff" Text="tiff" />
                                    <dx:ListEditItem Value="gif" Text="gif" />
                                </Items>
                            </dx:ASPxComboBox>
                        </Template>
                    </dx:MenuItem>
                    <dx:MenuItem Name="mnuLblAppearance" BeginGroup="True">
                        <Template>
                            <p style="margin-bottom:0px; margin-top:3px;">Chart Appearance:</p>
                           <%-- <dx:ASPxLabel ID="lblAppearance" Text="Chart Appearance:" runat="server" />--%>
                        </Template>
                    </dx:MenuItem>
                    <dx:MenuItem Name="mnuAppearance">
                        <Template>
                            <dx:ASPxComboBox runat="server" Width="120px" ValueType="System.String" ID="cbAppearance" ClientInstanceName="cbAppearance">
                                <ClientSideEvents SelectedIndexChanged="function(s, e) {   ASPxHiddenScreenWidth.Set('Width',document.getElementById('table').offsetWidth -13);
                                   
                                    WebChartControl1Pannel.PerformCallback('Appearance');
                                    
                                    }" />
                            </dx:ASPxComboBox>
                        </Template>
                    </dx:MenuItem>
                    <dx:MenuItem Name="mnuLblPalette">
                        <Template>
                              <p style="margin-bottom:0px; margin-top:3px;">Palette:</p>
                           <%-- <dx:ASPxLabel ID="lblPalette" Text="Palette:" runat="server" />--%>
                        </Template>
                    </dx:MenuItem>
                    <dx:MenuItem Name="mnuPalette">
                        <Template>
                            <dx:ASPxComboBox runat="server" Width="120px" ValueType="System.String" ID="cbPalette" ClientInstanceName="cbPalette">
                                <ClientSideEvents SelectedIndexChanged="function(s, e) {   ASPxHiddenScreenWidth.Set('Width',document.getElementById('table').offsetWidth -13);
                                    WebChartControl1Pannel.PerformCallback('Palette');
                                    }" />
                            </dx:ASPxComboBox>
                        </Template>
                    </dx:MenuItem>
                </Items>
                <ClientSideEvents ItemClick="function(s, e) {
                    if (e.item.name == 'mnuPrint'){
	                    
                    chart.Print();
                    }
                    if (e.item.name == 'mnuSaveToDisk')
                        chart.SaveToDisk(cbFormat.GetText());
                    if (e.item.name == 'mnuSaveToWindow')
                        chart.SaveToWindow(cbFormat.GetText());
                }" />
                </dx:ASPxMenu>
      
        <br />
     
                <dx:ASPxHiddenField ID="ASPxHiddenScreenWidth" ClientInstanceName="ASPxHiddenScreenWidth" Text="" runat="server"></dx:ASPxHiddenField>   
                <dx:WebChartControl Visible="false" ID="WebChartControl1" runat="server" ClientInstanceName="chart" EnableClientSideAPI="true"
                    CrosshairEnabled="True" Height="500px" Width="1300px">
                    <%--<ClientSideEvents  EndCallback="function(s, e) {
                    ChartLoadingPanel.Hide();
                    chart.SetVisible(true);
                    }" BeginCallback="function(s, e) {
	                ChartLoadingPanel.Show();
                    chart.SetVisible(false);
                    }"></ClientSideEvents>--%>
                </dx:WebChartControl>
                    </div>

                     <%--<dx:ASPxLoadingPanel ID="ChartLoadingPanel" runat="server" ClientInstanceName="ChartLoadingPanel"></dx:ASPxLoadingPanel>--%>
               </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
                         </div>
                    </div>
                         <!-- <div id="chartContainer" style="max-width: 700px; height: 400px;"></div> -->

                 
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
   
 

   <script type="text/javascript">

       //var barChartDataSource = new DevExpress.data.DataSource({
       //    load: function (loadOptions) {
       //        var d = $.Deferred();
       //        var id = location.href.substring(location.href.search(['[id=]']) + 3, location.href.length).toString();
       //        var location1 = "http://eap-omni-prod01/WCFOVTimeJSon/RestServiceImpl.svc/GetProjectsSummary?callback=?";

       //        $.getJSON(location1).done(function (data) {
       //            d.resolve(data);
       //        });

       //        return d.promise();
       //    }
       //});

       /*$("#chartContainer").dxChart({
                dataSource: barChartDataSource,
               commonSeriesSettings: {
                   type: 'bar',
                   argumentField: 'projectId'
               },
               series: [
                   { valueField: 'ProjectHrs', name: 'ProjectHrs' }
               ],
               commonAxisSettings: {
                   label: {
                       overlappingBehavior: { mode: 'enlargeTickInterval' }
                   },
                   grid: { visible: true }
               },
               rotated: true
       });*/

       $(window).resize(function () {
           //var height = Math.max(0, document.documentElement.clientHeight);
           //var width = Math.max(0, $("#wrapper").width());
           ASPxHiddenScreenWidth.Set('Width',document.getElementById('table').offsetWidth -13);
           WebChartControl1Pannel.PerformCallback();
       });

       function DoCallback() {
           alert("callback");
           window.document.form1.elements['chartWidth'].value = document.body.offsetWidth;
           chart.PerformCallback();
       }

       function ResizeChart(s, e) {
           alert("resizeCharts");
           window.document.form1.elements['callbackState'].value = 0;
           s.GetMainElement().style.width = window.document.form1.elements['chartWidth'].value + "px";
       }

       function ResetCallbackState() {
           window.document.form1.elements['callbackState'].value = 1;
       }

   </script>
     
</asp:Content>