﻿var pieChartDataSource1 = new DevExpress.data.DataSource({
    load: function (loadOptions) {
        var d = $.Deferred();
        var location1 = "http://eap-omni-prod01/WCFOVTimeJSon/RestServiceImpl.svc/timeentry/" + usrName + "?callback=?";
        var location2 = "/Content/data/suppliers.js";
        var location3 = "http://www.filltext.com?rows=10&ProjectId={firstName}&TotalHours={firstName}?";

        $.getJSON(location1).done(function (data) {
            
            d.resolve(data);
        });

        return d.promise();
    }
});

var pieChartDataSource = [
    { ProjectID: 'Oceania', TotalHours: 35 },
    { ProjectID: 'Africa', TotalHours: 1016 },
    { ProjectID: 'Americas', TotalHours: 936 },
    { ProjectID: 'Asia', TotalHours: 4149 },
    { ProjectID: 'Europe', TotalHours: 728 }
];

$(function () {
    $("#pieChartContainer").dxPieChart({
        dataSource: pieChartDataSource1,
        series: {
            argumentField: 'ProjectID',
            valueField: 'TotalHours',
            type: 'doughnut',
            hoverStyle: {
                color: "#ffd700" 
            },
            label: {
                visible: true,
                
                connector: {
                    visible: true,
                    width:1
                },
                position: "columns",
                format: "fixedPoint",
                customizeText: function (point) {
                    return point.argumentText + " " + point.percentText + "";
                }
            },
            smallValuesGrouping: {
                mode: "topN",
                topCount: 4
            },
        },
        
        pathModified: true,
        tooltip: {
            enabled: true,
            percentPrecision: 2,
            customizeTooltip: function (argument) {
                return {
                    text: argument.argument + ' ' + argument.percentText
                };
            }
        },
        title: {
            text: ''
        },
        legend: {
            horizontalAlignment: 'center',
            verticalAlignment: 'top'
        },
        onPointClick: function (arg) {
            arg.target.select();
        }
    });
});

