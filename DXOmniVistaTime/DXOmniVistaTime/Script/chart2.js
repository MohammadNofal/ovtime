﻿//$(document).ready(function() {
    window.alert("test");

    var dataSource = [
        { argument: '1', value1: 120, value2: 125, value3: 112, value4: 100, value5: 457 },
        { argument: '2', value1: 150, value2: 120, value3: 135, value4: 115, value5: 520 }
    ];

    $("#chartContainer").dxChart({
        dataSource: dataSource,
        series: {
            argumentField: "argument",
            valueField: "value1"
        }
    });