﻿var isPageSizeCallbackRequired = false;
function OnInit(s, e) {
    OnResized();
}
function OnBeginCallback(s, e) {
    if (e.command != "CUSTOMCALLBACK")
        isPageSizeCallbackRequired = true;
}
function OnEndCallback(s, e) {
    if (isPageSizeCallbackRequired === true)
        OnResized();
}
function OnControlsInitialized(s, e) {
    ASPxClientUtils.AttachEventToElement(window, "resize", function (evt) {
        OnResized();
    });
}
function OnResized() {
    var height = Math.max(0, document.documentElement.clientHeight);
    grid.PerformCallback(height);
    isPageSizeCallbackRequired = false;
}