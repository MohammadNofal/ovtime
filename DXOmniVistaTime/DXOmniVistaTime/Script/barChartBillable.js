﻿var barChartDataSource1 = new DevExpress.data.DataSource({
    load: function (loadOptions) {
        var d = $.Deferred();
        var id = location.href.substring(location.href.search(['[id=]']) + 3, location.href.length).toString();
        var location1 = "http://eap-omni-prod01/WCFOVTimeJSonQA/RestServiceImpl.svc/chartbar/" + id + "?callback=?";
        var location2 = "/Content/data/suppliers.js";
        var location3 = "http://www.filltext.com?rows=10&ProjectId={firstName}&TotalHours={firstName}?";

        $.getJSON(location1).done(function (data) {

            d.resolve(data);
        });

        return d.promise();
    }
});

var barChartDataSource = [{
    title: "",
    BilledAmt: 100,
    BillableAmt: 200,
    UnBilled: 300
}];

$(function () {
    $.when(populteAllJasonlData()).done(function () {
        $("#barChartBillable").dxChart({
            dataSource: barChartDataSource1,
            commonSeriesSettings: {
                argumentField: "title",
                type: "bar",
                hoverStyle: {
                    color: "#ffd700"
                },
                label: {
                    visible: true,
                    connector: {
                        visible: true,
                        width: 1
                    },
                    customizeText: function (arg) {
                        return "$" + arg.valueText.toString()//.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    },
                    position: "columns",
                    format: "fixedPoint",

                },
                smallValuesGrouping: {
                    mode: "smallValueThreshold",
                    threshold: 40
                },
            },
            series: [
                { valueField: "BillableAmt", name: "Billable" },
                { valueField: "BilledAmt", name: "Billed" },
                { valueField: "UnBilled", name: "UnBilled" },
            ],
            argumentAxis: {
                type: 'discrete',
                grid: { visible: true }
            },
            valueAxis: { //argumentAxis for vertical
                label: {
                    customizeText: function (arg) {
                        return "" + arg.valueText.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                },
                valueType: 'numeric'
            },
            tooltip: {
                enabled: true,
                location: "edge",
                customizeTooltip: function (arg) {
                    return {
                        text: "$" + arg.valueText.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    };
                }
            },
            title: {
                text: 'Billability Analysis'
            },
            legend: {
                horizontalAlignment: 'center',
                verticalAlignment: 'top',
                precision: 0
            }
       });
    });
});

