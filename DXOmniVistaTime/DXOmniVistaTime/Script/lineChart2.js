﻿var globallabel5, globallabel6, globallabel7, globallocation2;


// Now using `then` for all
function getAllJasonData() {
    var globallocationInFunction2 = "http://eap-omni-prod01/WCFOVTimeJSonQA/RestServiceImpl.svc/chartline/" + location.href.substring(location.href.search(['[id=]']) + 3, location.href.length).toString() + "?callback=?";

    return $.getJSON(globallocationInFunction2).then(function (lableDataInFunction) {
        return {
            glabel1: lableDataInFunction[0].ProjectIdText1,
            glabel2: lableDataInFunction[0].ProjectIdText2,
            glabel3: lableDataInFunction[0].ProjectIdText3

        }
    });
}


function populteAllJasonlData() {
    //and in your call will listen for the custom deferred's done
    return getJasonData().then(function (returndata) {
        //alert("return after complete - new function");
        globallabel5 = returndata.glabel1;
        //alert("return after complete - new function");
        globallabel6 = returndata.glabel2;
        globallabel7 = returndata.glabel3;
        globallabel8 = returndata.glabel4;
    });

    alert("return before complete - new function");
    return "none";
}

// Now using `then`
function getJasonData(labelNum) {
    var globallocationInFunction2 = "http://eap-omni-prod01/WCFOVTimeJSonQA/RestServiceImpl.svc/chartline/" + location.href.substring(location.href.search(['[id=]']) + 3, location.href.length).toString() + "?callback=?";

    return $.getJSON(globallocationInFunction2).then(function (lableDataInFunction) {
        return {
            glabel1: lableDataInFunction[0].ProjectIdText1,
            glabel2: lableDataInFunction[0].ProjectIdText2,
            glabel3: lableDataInFunction[0].ProjectIdText3

        }
    });
}

function populteJasonlData(labelNum) {
    //and in your call will listen for the custom deferred's done
    return getJasonData(labelNum).then(function (returndata) {
        if (labelNum == 1) {
            alert("return after complete - new function");
            globallabel5 = returndata.glabel1;
        }
        if (labelNum == 2) {
            globallabel6 = returndata.glabel2;
            //return returndata.glabel2;
        }
        if (labelNum == 3) {
            globallabel7 = returndata.glabel3;
            //return returndata.glabel3;
        }
    });

    alert("return before complete - new function");
    return "none";
}


var pieChartDataSource6 = new DevExpress.data.DataSource({
    load: function (loadOptions) {
        var d = $.Deferred();
        var location1 = "http://eap-omni-prod01/WCFOVTimeJSonQA/RestServiceImpl.svc/chartline/" + location.href.substring(location.href.search(['[id=]']) + 3, location.href.length).toString() + "?callback=?";
        var location2 = "/Content/data/suppliers.js";
        var location3 = "http://www.filltext.com?rows=10&ProjectId={firstName}&TotalHours={firstName}?";

        $.getJSON(location1).done(function (data) {
            //alert('Received ' + data.length + ' Length');

            d.resolve(data);
        });

        return d.promise();
    }
});



var types = ["spline", "stackedSpline", "fullStackedSpline", "line"];


$(function () {
    //$.when(populteJasonlData(1), populteJasonlData(2), populteJasonlData(3), populteJasonlData(4)).done(function () {
    $.when(populteAllJasonlData()).done(function () {
        // the code here will be executed when all four ajax requests resolve.
        // a1, a2, a3 and a4 are lists of length 3 containing the response text,
        // status, and jqXHR object for each of the four ajax calls respectively.



        $("#lineChartContainer2").dxChart({

            dataSource: pieChartDataSource6,
            commonSeriesSettings: {
                type: types[0],
                argumentField: "Month",
            },
            commonAxisSettings: {
                grid: {
                    visible: true
                }
            },
            margin: {
                bottom: 20
            },
            series: [
                {
                    name: "Cost", //globallabel5,
                    valueField: "Cost"
                },
                {
                    name: "Billable", //globallabel6,
                    valueField: "Billable"
                },
                {
                    name: "Billed", //globallabel7,
                    valueField: "Billed"
                }
            ],

            tooltip: {
                enabled: true
            },
            legend: {

                verticalAlignment: "top",
                horizontalAlignment: "center"
            },
            useAggregation: false,
            ignoreEmptyPoints: true,

            valueAxis: {
                valueType: 'numeric',
                label: {
                    customizeText: function (arg) {
                        return "" + arg.valueText.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                }
            },
            argumentAxis: {
                argumentType: 'numeric',
                tickInterval: 1,
                title: 'Month'
            },

            tooltip: {
                enabled: true,
                location: "edge",
                customizeTooltip: function (arg) {
                    return {
                        text: "$" + arg.valueText.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    };
                }
            },

            title: "Cost Billability Analysis",
            commonPaneSettings: {
                border: {
                    visible: true
                }
            }
        });
    });
});

/*"Labor Allocation by Top 4 Projects Over Time (Count)",*/
/*
$(function () {
    $("#chart").dxChart(chartOptions).dxChart("instance");

    $("#types").dxSelectBox({
        dataSource: types,
        value: types[0],
        onValueChanged: function (e) {
            chart.option("commonSeriesSettings.type", e.value);
        }
    })
});*/

