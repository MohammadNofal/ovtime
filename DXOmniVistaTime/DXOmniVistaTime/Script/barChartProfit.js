﻿var barChartDataSource3 = new DevExpress.data.DataSource({
    load: function (loadOptions) {
        var d = $.Deferred();
        var id = location.href.substring(location.href.search(['[id=]']) + 3, location.href.length).toString();
        var location1 = "http://eap-omni-prod01/WCFOVTimeJSonQA/RestServiceImpl.svc/chartbar/" + id + "?callback=?";
        var location2 = "/Content/data/suppliers.js";
        var location3 = "http://www.filltext.com?rows=10&ProjectId={firstName}&TotalHours={firstName}?";

        $.getJSON(location1).done(function (data) {

            d.resolve(data);
        });

        return d.promise();
    }
});

var barChartDataSource2 = [{
    state: "Illinois",
    year1998: 423.721,
    year2001: 476.851,
    year2004: 528.904
}, {
    state: "Indiana",
    year1998: 178.719,
    year2001: 195.769,
    year2004: 227.271
}, {
    state: "Michigan",
    year1998: 308.845,
    year2001: 335.793,
    year2004: 372.576
}, {
    state: "Ohio",
    year1998: 348.555,
    year2001: 374.771,
    year2004: 418.258
}, {
    state: "Wisconsin",
    year1998: 160.274,
    year2001: 182.373,
    year2004: 211.727
}];

$(function () {
    $.when(populteAllJasonlData()).done(function () {
        $("#barChartProfit").dxChart({
            dataSource: barChartDataSource3,
            commonSeriesSettings: {
                argumentField: "title",
                type: "bar",
                hoverStyle: {
                    color: "#ffd700"
                },
                label: {
                    visible: true,

                    connector: {
                        visible: true,
                        width: 1
                    },
                    customizeText: function (arg) {
                        return "$" + arg.valueText.toString()//.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    },
                    position: "columns",
                    format: "fixedPoint"
                },
                smallValuesGrouping: {
                    mode: "smallValueThreshold",
                    threshold: 40
                },
            },
            series: [
                { valueField: "Cost", name: "Cost" },
                { valueField: "BillableAmt", name: "Billable" },
                { valueField: "Profit", name: "Profit" }
            ],
            valueAxis: {
                label: {
                    customizeText: function (arg) {
                        return "" + arg.valueText.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                },
                valueType: 'numeric'
            },
            tooltip: {
                enabled: true,
                location: "edge",
                customizeTooltip: function (arg) {
                    return {
                        text: "$" + arg.valueText.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    };
                }
            },
            title: {
                text: 'Profit Analysis'
            },
            legend: {
                horizontalAlignment: 'center',
                verticalAlignment: 'top',
                precision: 0
            }
        });
    });
});

