﻿var globallabel1;

function populteAllJasonlData() {
    //and in your call will listen for the custom deferred's done
    return getJasonData().then(function (returndata) {
        //alert("from chart:" + cpGlobalProjectID);
        globallabel1 = returndata.glabel1; 
         
    });

    alert("return before complete - new function");
    return "none";
}

// Now using `then`
function getJasonData(labelNum) {
    var globallocationInFunction = "http://eap-omni-prod01/WCFOVTimeJSon/RestServiceImpl.svc/GetProjectsSummary?callback=?";

    return $.getJSON(globallocationInFunction).then(function (lableDataInFunction) {
        return {
            glabel1: lableDataInFunction[0].projectId
        }
    });
}

var EmployeeProjectLineChartDataSource = new DevExpress.data.DataSource({
    load: function (loadOptions) {
        //alert("test");
        //$.when(populteAllJasonlData()).done(function () {
        //    alet("loaded");
        //});

        var d = $.Deferred();
        var location1 = "http://eap-omni-prod01/WCFOVTimeJSon/RestServiceImpl.svc/GetProjectsSummary?callback=?";

        $.getJSON(location1).done(function (data) {
            //alert('Received ' + data.length + ' Length');
            d.resolve(data);
        });

        return d.promise();

    }
});

var EmployeeProjectLineChartSeries = [
            {
                name: "projectId",
                valueField: "ProjectHrs"
            } 
]; 
var types = ["bar"];


$(function () {
    //$.when(populteJasonlData(1), populteJasonlData(2), populteJasonlData(3), populteJasonlData(4)).done(function () {
    $.when(populteAllJasonlData(), EmployeeProjectLineChartSeries).done(function () {
        // the code here will be executed when all four ajax requests resolve.
        // a1, a2, a3 and a4 are lists of length 3 containing the response text,
        // status, and jqXHR object for each of the four ajax calls respectively.

        $("#projectsSummaryContainer").dxChart({

            dataSource: EmployeeProjectLineChartDataSource,
            commonSeriesSettings: {
                type: 'bar',
                argumentField: "projectId"
            },
            commonAxisSettings: {
                grid: {
                    visible: true
                }
            },
            margin: {
                bottom: 20
            },
            series: [
                {
                    name: globallabel1,
                    valueField: "ProjectHrs"
                } 
            ],

            tooltip: {
                enabled: true
            },
            legend: {

                verticalAlignment: "top",
                horizontalAlignment: "center"
            },
            useAggregation: false,
            ignoreEmptyPoints: true,

            valueAxis: {
                valueType: 'numeric'
            },
            argumentAxis: {
                argumentType: 'string',
                tickInterval: 1,
                title: 'Projects'
            },

            title: "",
            commonPaneSettings: {
                border: {
                    visible: false
                }
            }
        });
    });
});
 