﻿
var projectChartDataSource1 = new DevExpress.data.DataSource({
    load: function (loadOptions) {
        var d = $.Deferred();
        var id = location.href.substring(location.href.search(['[id=]']) + 3, location.href.length).toString();
        //var location1="http://eap-omni-prod01/WCFOVTimeJSon/RestServiceImpl.svc/timeentry/"+usrName+"?callback=?";
        var location1 = "http://eap-omni-prod01/WCFOVTimeJSonQA/RestServiceImpl.svc/chartpie/" + id + "?callback=?";
        var location2 = "/Content/data/suppliers.js";
        var location3 = "http://www.filltext.com?rows=10&ProjectId={firstName}&TotalHours={firstName}?";

        $.getJSON(location1).done(function (data) {

            d.resolve(data);
        });

        return d.promise();
    }
});

var projectChartDataSource = [
    { ProjectID: 'Cost', TotalHours: 35 },
    { ProjectID: 'Profit', TotalHours: 1016 },
    { ProjectID: 'Americas', TotalHours: 936 },
    { ProjectID: 'Asia', TotalHours: 4149 },
    { ProjectID: 'Europe', TotalHours: 728 }
];


$(function () {
    $.when(populteAllJasonlData()).done(function () {
        $("#projectChartContainer").dxPieChart({
            dataSource: projectChartDataSource1,
            series: {
                argumentField: 'Type',
                valueField: 'ProfitLoss',
                type: 'pie',
                hoverStyle: {
                    color: "#ffd700"
                },
                label: {
                    visible: true,

                    connector: {
                        visible: true,
                        width: 1
                    },
                    position: "columns",
                    format: "fixedPoint",
                    customizeText: function (point) {
                        return point.argumentText + " " + point.percentText + "";
                    }
                },
                smallValuesGrouping: {
                    mode: "smallValueThreshold",
                    threshold: 40
                },
            },

            pathModified: true,
            tooltip: {
                enabled: true,
                percentPrecision: 2,
                customizeTooltip: function (argument) {
                    return {
                        text: argument.argument + ' $' + argument.valueText.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    };
                }
            },
            title: {
                text: 'Profit Cost'
            },
            legend: {
                horizontalAlignment: 'center',
                verticalAlignment: 'top'
            },
            onPointClick: function (arg) {
                arg.target.select();
            }
        });
    });
});

