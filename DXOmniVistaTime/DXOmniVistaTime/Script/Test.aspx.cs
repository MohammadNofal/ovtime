﻿using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Scripts_Test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetProjectsSummary()
    {
        //DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDBProduction"].ToString());
        //DataTable dt = DataAccess.GetDataTableBySqlSyntax("exec [OVS_GetProjectsDashboard] null,null,null,null,'Design,Supervision,Variation,Other'", "");
        //string test = DataTableToJSONWithJavaScriptSerializer(dt);
        string test = "[{\"projectId\":\"000:\",\"ProjectHrs\":0},{\"projectId\":\"1086\",\"ProjectHrs\":4}]";
        return test;
        
    }
    private static string DataTableToJSONWithJavaScriptSerializer(DataTable table)
    {
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
        Dictionary<string, object> childRow;
        foreach (DataRow row in table.Rows)
        {
            childRow = new Dictionary<string, object>();
            foreach (DataColumn col in table.Columns)
            {
                if (col.ColumnName != "projectId" && col.ColumnName != "ProjectHrs") continue;
                childRow.Add(col.ColumnName, row[col]);
            }
            parentRow.Add(childRow);
        }
        return jsSerializer.Serialize(parentRow);
    }  
}