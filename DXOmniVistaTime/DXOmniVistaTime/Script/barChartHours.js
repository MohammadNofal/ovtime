﻿var barChartDataSource5 = new DevExpress.data.DataSource({
    load: function (loadOptions) {
        var d = $.Deferred();
        var id = location.href.substring(location.href.search(['[id=]']) + 3, location.href.length).toString();
        var location1 = "http://eap-omni-prod01/WCFOVTimeJSonQA/RestServiceImpl.svc/barchartbudget/" + id + "?callback=?";
        var location2 = "/Content/data/suppliers.js";
        var location3 = "http://www.filltext.com?rows=10&ProjectId={firstName}&TotalHours={firstName}?";

        $.getJSON(location1).done(function (data) {

            d.resolve(data);
        });

        return d.promise();
    }
});

var barChartDataSource4 = [{
    title: "",
    BilledAmt: 100,
    BillableAmt: 200,
    UnBilled: 300
}];

$(function () {
    $.when(populteAllJasonlData()).done(function () {
        $("#barChartHours").dxChart({
            rotated: true,
            dataSource: barChartDataSource5,
            commonSeriesSettings: {
                argumentField: "title",
                type: "bar",
                hoverStyle: {
                    color: "#ffd700"
                },
                label: {
                    visible: true,
                    connector: {
                        visible: true,
                        width: 1
                    },
                    customizeText: function (arg) {
                        return "" + arg.valueText.toString()//.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    },
                    position: "columns",
                    format: "fixedPoint",

                },
                smallValuesGrouping: {
                    mode: "smallValueThreshold",
                    threshold: 40
                },
            },
            series: [
                { valueField: "<%@ Master Language="C#" AutoEventWireup="true" CodeFile="OVMaster.master.cs" Inherits="OVMaster" %>

<%-- DXCOMMENT: Page Root.master is a master page that contains the root layout (it includes Header, Cental Area, and Footer) --%>

<style>
    @font-face 
    {
        font-family: 'FontAwesome';
        src: url('font/fontawesome-webfont.eot');
        src: url('font/fontawesome-webfont.eot?#iefix') format('embedded-opentype'), url('font/fontawesome-webfont.woff') format('woff'), url('font/fontawesome-webfont.ttf') format('truetype'), url('fonts/fontawesome-webfont.svg#svgFontName') format('svg');
    }

    .excel_icon 
    {
        position: relative;
        font-family: FontAwesome !important;    
        font-size: 18px !important;
        font-weight: 500 !important;
        font-style: normal !important;
        }
    .excel_icon:before 
    {
        content: "\f1c3" !important;
    }
    .pdf_icon 
    {
        position: relative;
        font-family: FontAwesome !important;  
        font-size: 18px !important;
        font-weight: 500 !important;
        font-style: normal !important;
    }
    .pdf_icon:before 
    {
        content: "\f1c1" !important;
    }
</style>

<!DOCTYPE html lang="en">
<html>
<head id="Head2" runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <style>
        .mainContentPane2 
        {
            float: left;
            background-color: red;
            border: 2px solid #ffd800;
            width: 100%;
        }
        .breadcrumb_root  a 
        {
            color:#333333;
        }
         .breadcrumb_root  a:hover 
        {
            color:#707070;
        }
        .current_Node 
        {
            color: #abafb8;
        }
    </style>

    <title>OVTime</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css" />
    <link href="/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/colors.css" rel="stylesheet" type="text/css" />
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="/assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="/assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/pickers/daterangepicker.js"></script>

    <script type="text/javascript" src="/assets/js/core/app.js"></script>
    <script type="text/javascript" src="/assets/js/pages/dashboard.js"></script>
    <!-- /theme JS files -->

    <!-- /FontAwesome-->
    <link rel="stylesheet" type="text/css" href="Content/font-awesome.css" />
    <link rel="stylesheet" type="text/css" href="Content/font-awesome.min.css" />
    <!-- /FontAwesome-->
    <link rel="stylesheet" type="text/css" href="Content/OVSite.css" />
    <%--<link rel="stylesheet" type="text/css" href="Content/Site.css" />--%>
    
</head>
<body runat="server" id="Body">
    <!--Beginning of Header-->
    <div class="navbar navbar-inverse">
        <!--Shows the header ribbon and logo-->

        <div class="navbar-header">
            <div class="templateTitle">
                <a id="TitleLink" href="~/" runat="server">
                    <img src="http://omnivista.net/OVTime/Content/Images/OVTimeLogo2.png" alt="OVTime" style="width: 28px; height: 28px; border: 0" />
                    OVTime</a>
            </div>
            <ul class="nav navbar-nav visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        </div>

        <div class="navbar-collapse collapse" id="navbar-mobile">
            <!--Shows the two buttons on the right of the logo  -->
            <ul class="nav navbar-nav">
                <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-git-compare"></i>
                        <span class="visible-xs-inline-block position-right">Git updates</span>
                        <span class="badge bg-warning-400">9</span>
                    </a>

                    <div class="dropdown-menu dropdown-content">
                        <div class="dropdown-content-heading">
                            Git updates
							    <ul class="icons-list">
                                    <li><a href="#"><i class="icon-sync"></i></a></li>
                                </ul>
                        </div>

                        <ul class="media-list dropdown-content-body width-350">
                            <li class="media">
                                <div class="media-left">
                                    <a href="#" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
                                </div>

                                <div class="media-body">
                                    Drop the IE <a href="#">specific hacks</a> for temporal inputs
									    <div class="media-annotation">4 minutes ago</div>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left">
                                    <a href="#" class="btn border-warning text-warning btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-commit"></i></a>
                                </div>

                                <div class="media-body">
                                    Add full font overrides for popovers and tooltips
									    <div class="media-annotation">36 minutes ago</div>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left">
                                    <a href="#" class="btn border-info text-info btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-branch"></i></a>
                                </div>

                                <div class="media-body">
                                    <a href="#">Chris Arney</a> created a new <span class="text-semibold">Design</span> branch
									    <div class="media-annotation">2 hours ago</div>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left">
                                    <a href="#" class="btn border-success text-success btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-merge"></i></a>
                                </div>

                                <div class="media-body">
                                    <a href="#">Eugene Kopyov</a> merged <span class="text-semibold">Master</span> and <span class="text-semibold">Dev</span> branches
									    <div class="media-annotation">Dec 18, 18:36</div>
                                </div>
                            </li>

                            <li class="media">
                                <div class="media-left">
                                    <a href="#" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
                                </div>

                                <div class="media-body">
                                    Have Carousel ignore keyboard events
									    <div class="media-annotation">Dec 12, 05:46</div>
                                </div>
                            </li>
                        </ul>

                        <div class="dropdown-content-footer">
                            <a href="#" data-popup="tooltip" title="All activity"><i class="icon-menu display-block"></i></a>
                        </div>
                    </div>
                </li>
            </ul>

            <!--Shows the green online picture-->
            <p class="navbar-text"><span class="label bg-success">Online</span></p>

            <!--Shows all the options that are on the right of the header-->
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown language-switch">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/assets/images/flags/us.png" class="position-left" alt="">
                        English
						    <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">

                        <li><a class="english">
                            <img src="/assets/images/flags/us.png" alt="">
                            English</a></li>

                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-bubbles4"></i>
                        <span class="visible-xs-inline-block position-right">Messages</span>
                        <span class="badge bg-warning-400">2</span>
                    </a>

                    <div class="dropdown-menu dropdown-content width-350">
                        <div class="dropdown-content-heading">
                            Messages
							    <ul class="icons-list">
                                    <li><a href="#"><i class="icon-compose"></i></a></li>
                                </ul>
                        </div>

                        <ul class="media-list dropdown-content-body">
                            <li class="media">
                                <div class="media-left">
                                    <img src="/assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
                                    <span class="badge bg-danger-400 media-badge">5</span>
                                </div>
                                <div class="media-body">
                                    <a href="#" class="media-heading">
                                        <span class="text-semibold"><asp:LoginName ID="LoginName3" runat="server" /></span>
                                        <span class="media-annotation pull-right">04:58</span>
                                    </a>
                                    <span class="text-muted">This is an example of a message</span>
                                </div>
                            </li> 
                        </ul>

                        <div class="dropdown-content-footer">
                            <a href="#" data-popup="tooltip" title="All messages"><i class="icon-menu display-block"></i></a>
                        </div>
                    </div>
                </li>

                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/assets/images/placeholder.jpg" alt="">
                        <span>
                            <asp:LoginName ID="HeadLoginName" runat="server" />
                        </span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-user-plus"></i>My profile</a></li>

                        <li><a href="#"><span class="badge bg-teal-400 pull-right">58</span> <i class="icon-comment-discussion"></i>Messages</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="icon-cog5"></i>Account settings</a></li>
                        <li><a href="#"><i class="icon-switch2"></i>Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!--End of Header-->


    <!-- Page container -->
    <div class="page-container">
        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main">
                <!-- This is the bar on the left-->
                <div class="sidebar-content">

                    <!-- User menu -->
                    <div class="sidebar-user">
                        <div class="category-content">
                            <div class="media">
                                <a href="#" class="media-left">
                                    <img src="/assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                                <div class="media-body">
                                    <span class="media-heading text-semibold">
                                        <asp:LoginName ID="LoginName2" runat="server" />
                                    </span>
                                    
                                </div>

                                <div class="media-right media-middle">
                                    <ul class="icons-list">
                                        <li>
                                            <a href="#"><i class="icon-cog3"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /user menu -->


                    <!-- Main navigation -->
                    <div class="sidebar-category sidebar-category-visible">
                        <div class="category-content no-padding">
                            <ul id='menu' class="navigation navigation-main navigation-accordion">

                                <!-- Main -->

                                <li id="li-home" style="display:none"><a href="../Default.aspx" ><i class="icon-home4"></i><span>Home</span></a></li>

                                <li id="li-TimeSheet" >
                                    <a href="../TimeSheet/OVTimeSheet.aspx" ><i class="icon-calendar3"></i><span>Time Sheet</span></a>
                                </li>
                                <li id="li-Manager">
                                    <a href="#"><i class="icon-copy"></i><span>Manager</span></a>
                                    <ul>
                                        <li style="display:none"><a href="../Manager/Dashboard.aspx" id="Dashboard">Dashboard</a></li>
                                        <li><a href="../Manager/ProjectsSummary.aspx" id="A1">Projects Summary</a></li>
                                        <li><a href="../Manager/ProjectsDetails.aspx" id="A2">Project Details</a></li>
                                        <li><a href="../Manager/EmployeeHrs.aspx" id="A3">Employee Hours</a></li>
                                        <li><a href="../Manager/DashboardNew.aspx" id="A4">Dashboard</a></li>
                                    </ul>
                                </li>
                                <li id="li-Administration" style="display:none">
                                    <a href="#"><i class="icon-people"></i><span>Administration</span></a>
                                    <ul>
                                        <li><a href="../Manager/HolidayCalendar.aspx">Holiday Calendar</a></li>
                                        <li><a href="../Manager/EmployeeSchedule.aspx">Employee Schedule</a></li>

                                    </ul>
                                </li>
                                <li id="li-Reports" style="display:none">
                                    <a href="#"><i class="icon-stack"></i><span>Reports</span></a>
                                    <ul>
                                        <li><a href="../Reports/Analysis.aspx">Project Analysis</a></li>
                                        <li><a href="../Reports/Reports.aspx">Reports</a></li>

                                    </ul>
                                </li>
                                <li id="li-Demo" style="display:none">
                                    <a><i class="icon-select2"></i><span>Demo</span></a>
                                    <ul>
                                        <li><a href="../Demo.aspx">Demo Page</a></li>
                                    </ul>
                                </li>
                            <%-- DXCOMMENT: Configure the header menu --%>

                                <%--The following script highlights the selected header in green --%>
                                <script>
                               
                                var path = window.location.pathname;
                                if (path == "/TimeSheet/OVTimeSheet.aspx") {
                                    $("#li-TimeSheet").addClass("active");   
                                }
                                else if (path == "/Default.aspx") {
                                    $("#li-home").addClass("active");
                                }
                                else if (path == "/Manager/Dashboard.asp") {
                                    $("#li-Manager").addClass("active");
                                }
                                else if (path == "/Manager/HolidayCalendar.aspx" || path == "/Manager/EmployeeSchedule.aspx") {
                                    $("#li-Administration").addClass("active");
                                }
                                else if (path == "/Reports/Analysis.aspx" || path == "/Reports/Reports.aspx") {
                                    $("#li-Reports").addClass("active");
                                }
                                else if (path == "/Demo.aspx"){
                                    $("#li-Demo").addClass("active");
                                }
                                </script> 
                                <!-- /main -->


                            </ul>
                        </div>
                    </div>
                    
                    <!-- /main navigation -->

                </div>
            </div>
            <!-- /main sidebar -->

            <!-- Main content -->
          
            <div class="content-wrapper">
                <!-- Make a place for the content in the middle of page -->

                <!-- Page header -->
                <div class="page-header page-header-default">
                    
                 
                    <div class="page-header-content">
                        <div class="page-title">
                             <asp:ContentPlaceHolder ID="Header" runat="server" /> 
                           
                        </div>

                        
                    </div>

                    <div class="breadcrumb-line">
                       
                        <ul class="breadcrumb">
                            <i class="icon-home2 position-left"></i> 
                            <asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" />
                              <asp:SiteMapPath ID="SiteMap1" runat="server" CssClass="breadcrumb_root" >
                                  <PathSeparatorTemplate>
                                      / 
                                  </PathSeparatorTemplate>
                                  <CurrentNodeStyle CssClass="current_Node" />
                            </asp:SiteMapPath>
                        </ul>
                       
                        <ul class="breadcrumb-elements">
                            <li><a href="#"><i class="icon-comment-discussion position-left"></i>Support</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-gear position-left"></i>
                                    Settings
									    <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="#"><i class="icon-user-lock"></i>Account security</a></li>
                                    <li><a href="#"><i class="icon-statistics"></i>Analytics</a></li>
                                    <li><a href="#"><i class="icon-accessibility"></i>Accessibility</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="icon-gear"></i>All settings</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    </div>
                <!-- /page header -->

                <form id="formBody" runat="server">
                    <asp:ContentPlaceHolder ID="ContentPage" runat="server" />
               </form>

            </div>

            <!-- Main content -->
        </div>
      
        <!-- Page content -->

    </div>
    <!-- Page container -->


</body>
    
</html>

", name: "Budgeted Hours" },
                { valueField: "Calc_Total_AHours", name: "Actual Hours" },
                { valueField: "PercentComplete", name: "% Complete" },
            ],
            argumentAxis: {
                type: 'discrete',
                grid: { visible: true }
            },
            valueAxis: { //argumentAxis for vertical
                label: {
                    customizeText: function (arg) {
                        return "" + arg.valueText.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                },
                valueType: 'numeric'
            },
            tooltip: {
                enabled: true,
                location: "edge",
                customizeTooltip: function (arg) {
                    return {
                        text: "" + arg.valueText.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    };
                }
            },
            title: {
                text: 'Budgeted / Actual Hours'
            },
            legend: {
                horizontalAlignment: 'center',
                verticalAlignment: 'top',
                precision: 0
            }
        });
    });
});

