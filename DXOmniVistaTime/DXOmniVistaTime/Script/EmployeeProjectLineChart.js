﻿var globallabel1, globallabel2, globallabel3, globallabel4, globallocation1;

function populteAllJasonlData() {
    //and in your call will listen for the custom deferred's done
     return getJasonData().then(function (returndata) {
         //alert("from chart:" + cpGlobalProjectID);
         globallabel1 = returndata.glabel1;
         //alert("return after complete - new function");
        globallabel2 = returndata.glabel2;
        globallabel3 = returndata.glabel3;
        globallabel4 = returndata.glabel4;
    });

    alert("return before complete - new function");
    return "none";
}

// Now using `then`
function getJasonData(labelNum) {
    var globallocationInFunction = "http://eap-omni-prod01/WCFOVTimeJSon/RestServiceImpl.svc/allocationsummary/" + globalUsrName + "?callback=?";

    return $.getJSON(globallocationInFunction).then(function (lableDataInFunction) {
        return {
            glabel1: lableDataInFunction[0].ProjectIdText1,
            glabel2: lableDataInFunction[0].ProjectIdText2,
            glabel3: lableDataInFunction[0].ProjectIdText3,
            glabel4: lableDataInFunction[0].ProjectIdText4

        }
    });
}

var EmployeeProjectLineChartDataSource = new DevExpress.data.DataSource({
    load: function (loadOptions) {
        //alert("test");
        //$.when(populteAllJasonlData()).done(function () {
        //    alet("loaded");
        //});
            
            var d = $.Deferred();
            var location1 = "http://eap-omni-prod01/WCFOVTimeJSon/RestServiceImpl.svc/allocationsummary/" + globalUsrName + "?callback=?";
            
            $.getJSON(location1).done(function (data) {
                //alert('Received ' + data.length + ' Length');
                d.resolve(data);
            });

            return d.promise();
        
    }
});

var EmployeeProjectLineChartSeries  = [
            {
                name: "globallabel1",
                valueField: "ProjectIdValue1"
            },
            {
                name: globallabel2, 
                valueField: "ProjectIdValue2"
            },
            {
                name: globallabel3,
                valueField: "ProjectIdValue3"
            },
            {
                name: globallabel4, 
                valueField: "ProjectIdValue4"
            }
];

var types = ["spline", "stackedSpline", "fullStackedSpline", "line"];


$(function () {
    //$.when(populteJasonlData(1), populteJasonlData(2), populteJasonlData(3), populteJasonlData(4)).done(function () {
    $.when(populteAllJasonlData(), EmployeeProjectLineChartSeries).done(function () {
        // the code here will be executed when all four ajax requests resolve.
        // a1, a2, a3 and a4 are lists of length 3 containing the response text,
        // status, and jqXHR object for each of the four ajax calls respectively.

    $("#lineChartContainer").dxChart({
        
        dataSource: EmployeeProjectLineChartDataSource,
        commonSeriesSettings: {
            type: types[0],
            argumentField: "MonthNumber"
        },
        commonAxisSettings: {
            grid: {
                visible: true
            }
        },
        margin: {
            bottom: 20
        },
        series: [
            {
                name: globallabel1,
                valueField: "ProjectIdValue1"
            },
            {
                name: globallabel2,
                valueField: "ProjectIdValue2"
            },
            {
                name: globallabel3,
                valueField: "ProjectIdValue3"
            },
            {
                name: globallabel4,
                valueField: "ProjectIdValue4"
            }
        ],
    
        tooltip: {
            enabled: true
        },
        legend: {
            
            verticalAlignment: "top",
            horizontalAlignment: "center"
        },
        useAggregation: false,
        ignoreEmptyPoints:true,

        valueAxis: {
            valueType: 'numeric'
        },
        argumentAxis: {
            argumentType: 'numeric',
            tickInterval: 1,
            title: 'Month'
        },
        
        title: "",
        commonPaneSettings: {
            border: {
                visible: false
            }
        }
    });
    });
});

function refreshData() {
    
    //only poupulate after all data is aquired
    $.when(populteAllJasonlData()).done(function () {
        //alert("refresh data for user:" + globalUsrName);
        //update data source
        $("#lineChartContainer").dxChart("instance").option('dataSource').load();

        //$("#lineChartContainer").dxChart("instance").option({
        //    dataSource: new DevExpress.data.DataSource({ load: EmployeeProjectLineChartDataSource })
        //});

        //alert("updating labels to:" + globallabel1 + "|" + globallabel2 + "|" + globallabel3 + "|" + globallabel4);
        //update labels
        $('#lineChartContainer').dxChart('instance').option({
        series: [
            {
                name: globallabel1, //globallabel1,
                valueField: "ProjectIdValue1"
            },
            {
                name: globallabel2, //globallabel2,
                valueField: "ProjectIdValue2"
            },
            {
                name: globallabel3, //globallabel3,
                valueField: "ProjectIdValue3"
            },
            {
                name: globallabel4, //globallabel4,
                valueField: "ProjectIdValue4"
            }
        ]
        });

        

        //alert("data refreshed for user:" + globalUsrName);
    
    });
};
