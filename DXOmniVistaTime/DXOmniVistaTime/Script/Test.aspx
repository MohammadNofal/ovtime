﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Test.aspx.cs" Inherits="Scripts_Test" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- DevExtreme dependencies -->
    
<script src="../script/dx/js/globalize.js"></script>
<script type="text/javascript" src="../script/dx/js/jquery-2.1.4.min.js"></script>
<!-- DevExtreme themes -->
<link rel="stylesheet" type="text/css" href="../script/dx/css/dx.common.css" />
<link rel="stylesheet" type="text/css" href="../script/dx/css/dx.light.css" />
<!-- A DevExtreme library -->
<script type="text/javascript" src="../script/dx/js/dx.all.js"></script>
<!-- <script type="text/javascript" src="js/dx.mobile.js"></script> -->
<!-- <script type="text/javascript" src="js/dx.web.js"></script> -->
<!-- <script type="text/javascript" src="js/dx.viz.js"></script> -->
<!-- <script type="text/javascript" src="js/dx.viz-web.js"></script> -->
</head>
<body class="dx-viewport">
    <div class="dx-viewport demo-container">
        <div id="chart"></div>
    </div>
</body>
</html>

   <script type="text/javascript">
       
        function getJasonData() {
           console.log("here");
           //var globallocationInFunction = "http://eap-omni-prod01/WCFOVTimeJSon/RestServiceImpl.svc/allocationsummary/" + globalUsrName + "?callback=?";
           var globallocationInFunction = "Test.aspx/GetProjectsSummary";
           var globallocationInFunction = "http://eap-omni-prod01/WCFOVTimeJSon/RestServiceImpl.svc/GetProjectsSummary?callback=?";
            
           return $.getJSON(globallocationInFunction).then(function (lableDataInFunction) {
               console.log(lableDataInFunction);
                return lableDataInFunction;
           });
       }


       var ProjectSummaryDataSource = new DevExpress.data.DataSource({
           load: function (loadOptions) { 
               var d = $.Deferred();
               //var location1 = "http://eap-omni-prod01/WCFOVTimeJSon/RestServiceImpl.svc/allocationsummary/" + globalUsrName + "?callback=?";
               
               var location1 = "http://eap-omni-prod01/WCFOVTimeJSon/RestServiceImpl.svc/GetProjectsSummary?callback=?"; 
               $.getJSON(location1).done(function (data) {
                   //alert('Received ' + data.length + ' Length'); 
                   d.resolve(data);
               }); 
               return d.promise(); 
           }
       });
       $(function () {
           //$.when(populteJasonlData(1), populteJasonlData(2), populteJasonlData(3), populteJasonlData(4)).done(function () {
           $.when(getJasonData).done(function () {
               // the code here will be executed when all four ajax requests resolve.
               // a1, a2, a3 and a4 are lists of length 3 containing the response text,
               // status, and jqXHR object for each of the four ajax calls respectively.
               console.log(ProjectSummaryDataSource);
               $("#chart").dxChart({
                   dataSource: ProjectSummaryDataSource,
                               palette: "soft",
                               title: {
                                   text: "Projects Summary",
                                   subtitle: "as of January 2017"
                               },
                               size: {
                                   height: 700
                               },
                               commonSeriesSettings: {
                                   type: "bar",
                                   valueField: "ProjectHrs",
                                   argumentField: "projectId"
                               },
                               equalBarWidth: false,
                               seriesTemplate: {
                                   nameField: "projectId"
                               },
                               legend: {
                                   verticalAlignment: 'bottom',
                                   horizontalAlignment: 'center'
                               }
                           });
           });
       });


                
          
   </script>