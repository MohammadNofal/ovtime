﻿using CrystalDecisions;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using DevExpress.Web;
using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NewMyReports : System.Web.UI.Page
{
    DataTable dt = new DataTable();
    string reportsFolder = "~/Reports2/";
    string htmlFolder = "~/HTML/";
    private ParamList paramList = new ParamList();

    ReportDocument report = new ReportDocument();

    protected void Page_Load(object sender, EventArgs e)
    {
        string idParameter = Request.QueryString["id"];

        if (idParameter == null || idParameter == string.Empty)
            return;
        string file = Path.GetFileName(Directory.GetFiles(Server.MapPath(reportsFolder))[0]);
        string reportName = (string.IsNullOrEmpty(idParameter)) ? file : idParameter;

        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        crConnectionInfo.ServerName = "edb-omni-prod01";
        crConnectionInfo.DatabaseName = "OVTIME_OMV";
        crConnectionInfo.UserID = "tm_dev";
        crConnectionInfo.Password = "tm_dev";

        if (!IsPostBack)
        {
            //Dispose report and clear cache
            if (Session["report"] != null)
            {
                report = (ReportDocument)Session["report"];
                if (report != null)
                {
                    report.Close();
                    report.Dispose();
                    CrystalReportViewer1.ReportSource = null;

                    CrystalReportViewer1.Dispose();

                    //CrystalReportViewer1 = new CrystalDecisions.Web.CrystalReportViewer();
                }
            }
            report = new ReportDocument();
            report.Load(Server.MapPath(reportsFolder + reportName), OpenReportMethod.OpenReportByDefault);
            Session["report"] = report;

            TableLogOnInfo crTableLogoninfo = new TableLogOnInfo();

            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in report.Database.Tables)
            {
                crTableLogoninfo = CrTable.LogOnInfo;
                crTableLogoninfo.ConnectionInfo = crConnectionInfo;
                CrTable.ApplyLogOnInfo(crTableLogoninfo);
            }
            foreach (ReportDocument subreport in report.Subreports)
            {
                foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in subreport.Database.Tables)
                {
                    crTableLogoninfo = CrTable.LogOnInfo;
                    crTableLogoninfo.ConnectionInfo = crConnectionInfo;
                    CrTable.ApplyLogOnInfo(crTableLogoninfo);
                }
            }

            CrystalReportViewer1.ReportSource = report;
            Session[reportsFolder + reportName] = report;



        }
        else
        {

        }
        report = (ReportDocument)Session["report"];
        CrystalReportViewer1.ReportSource = report;

        string myLabel = string.Empty;
        string strTable = string.Empty;
        string strValueField = string.Empty;
        string strTextField = string.Empty;
        string strSql = string.Empty;

        for (int i = 0; i < CrystalReportViewer1.ParameterFieldInfo.Count; i++)
        {
            /* For audit purposes only 1*/
            myLabel += "ParamName=" + CrystalReportViewer1.ParameterFieldInfo[i].Name + "|";
            myLabel += "isDynamic=" + isParameterDynamic(report, i, true).ToString() + "|";
            myLabel += "Type=" + CrystalReportViewer1.ParameterFieldInfo[i].ParameterValueType.ToString() + "|";

            switch (CrystalReportViewer1.ParameterFieldInfo[i].ParameterValueType.ToString())
            {
                case "DateParameter":
                    AddDateTimeFilter(CrystalReportViewer1.ParameterFieldInfo[i]);
                    break;
                case "StringParameter":
                    if (CrystalReportViewer1.ParameterFieldInfo[i].DefaultValues.Count > 0 || isParameterDynamic(report, i, true))
                    {
                        if (isParameterDynamic(report, i, true))
                        {
                            //get sql table by TableId from FieldId (before the ".")
                            strTable = getParameterFieldID(report, i).Substring(0, getParameterFieldID(report, i).ToString().IndexOf("."));
                            CrystalDecisions.ReportAppServer.DataDefModel.CommandTable cd = (CrystalDecisions.ReportAppServer.DataDefModel.CommandTable)report.ReportClientDocument.DatabaseController.Database.Tables.FindTableByAlias(strTable);
                            strValueField = getParameterFieldID(report, i).Substring(getParameterFieldID(report, i).ToString().IndexOf(".") + 1);
                            strTextField = strValueField;
                            strSql = cd.CommandText;

                            myLabel += "FieldId=" + getParameterFieldID(report, i) + "|";
                            myLabel += "Table=" + strTable + "|";
                            myLabel += "ValueField=" + strValueField + "|";
                            myLabel += "TextField=" + strTextField + "|";
                            myLabel += "SqlTableByFieldID=" + strSql + "|";

                        }

                        AddMultiSelectFilter(CrystalReportViewer1.ParameterFieldInfo[i], isParameterDynamic(report, i, true), strTable, strValueField, strTextField, strSql);
                    }
                    else
                    {
                        AddStringFilter(CrystalReportViewer1.ParameterFieldInfo[i]);
                    }
                    break;

            }

        }

        AddSubmitButton();
    }

    public Boolean isParameterDynamic(CrystalDecisions.CrystalReports.Engine.ReportDocument rpt, int iCnt, bool YorN)
    {
        if (rpt.DataDefinition.ParameterFields[iCnt].Attributes != null && rpt.DataDefinition.ParameterFields[iCnt].Attributes.ContainsKey("IsDCP"))
        {
            System.Collections.Hashtable objAttributes = rpt.DataDefinition.ParameterFields[iCnt].Attributes;
            YorN = (Boolean)objAttributes["IsDCP"];
            return YorN;
        }
        return YorN;
    }

    private void AddDateTimeFilter(CrystalDecisions.Shared.ParameterField param)
    {
        try
        {
            //RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption"

            ASPxDateEdit dateEdit = new ASPxDateEdit();
            dateEdit.ID = param.Name;
            dateEdit.CssClass = "DateEdit";
            dateEdit.Text = param.PromptText;
            dateEdit.ToolTip = param.PromptText;
            dateEdit.Caption = param.PromptText;
            dateEdit.AutoPostBack = false;
            dateEdit.RootStyle.CssClass = "editorContainer";
            dateEdit.CaptionCellStyle.CssClass = "editorCaption";
            dateEdit.ClientSideEvents.TextChanged = "function(source,e){{e.processOnServer = true;)}}";
            dateEdit.ValueChanged += new System.EventHandler(this.Param_Changed);

            PanelContent4.Controls.Add(dateEdit);
            paramList.AddParam(new Param(dateEdit.ID, ParamType.DateTime, dateEdit, dateEdit.Text));
            Session["paramList"] = paramList;
        }
        catch
        {
            throw;
        }
    }

    public string getParameterFieldID(CrystalDecisions.CrystalReports.Engine.ReportDocument rpt, int iCnt)
    {
        if (rpt.DataDefinition.ParameterFields[iCnt].Attributes != null && rpt.DataDefinition.ParameterFields[iCnt].Attributes.ContainsKey("FieldID"))
        {
            System.Collections.Hashtable objAttributes = rpt.DataDefinition.ParameterFields[iCnt].Attributes;
            return (string)objAttributes["FieldID"];

        }
        return string.Empty;
    }

    private void AddMultiSelectFilter(CrystalDecisions.Shared.ParameterField param, bool isDynamic, string tableName, string valueField, string textField, string sql)
    {
        try
        {
            ASPxComboBox comboBox = new ASPxComboBox();
            comboBox.ID = param.Name;
            //Need to add as soon as combo is  created
            PanelContent4.Controls.Add(comboBox);

            comboBox.ToolTip = param.PromptText;
            comboBox.Caption = param.PromptText;
            comboBox.CssClass = "ComboBox";
            comboBox.AutoPostBack = false;
            comboBox.RootStyle.CssClass = "editorContainer";
            comboBox.CaptionCellStyle.CssClass = "editorCaption";

            if (comboBox.ID == "EmployeeID")
            {
                comboBox.Visible = false;
            }

            comboBox.Text = Membership.GetUser().UserName.ToString();

            //add combo values
            if (isDynamic)
            {
                //for dynamic list
                DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);

                comboBox.DataSource = DataAccess.GetDataTableBySqlSyntax(sql, string.Empty);
                comboBox.DropDownStyle = DropDownStyle.DropDownList;
                comboBox.ValueField = valueField;
                comboBox.TextField = textField;
                //comboBox.ValueType = typeof(Int32);
                //comboBox.Columns.Add(textField);
                //comboBox.Columns.Add(valueField);
                comboBox.DataBind();

            }
            else
            {
                //for static list
                for (int i = 0; i < param.DefaultValues.Count; i++)
                {
                    ParameterDiscreteValue paramDV = new ParameterDiscreteValue();
                    paramDV = (ParameterDiscreteValue)param.DefaultValues[i];
                    ListEditItem myItem = new ListEditItem(paramDV.Value.ToString());
                    comboBox.Items.Add(myItem);
                }
            }

            //comboBox.ClientSideEvents.TextChanged = "function(source,e){{e.processOnServer = true;)}}";
            comboBox.ValueChanged += new System.EventHandler(this.Param_Changed);



            //PanelContent4.Controls.Add(comboBox);
            paramList.AddParam(new Param(comboBox.ID, ParamType.MultiSelect, comboBox, comboBox.Text));
            Session["paramList"] = paramList;

        }
        catch
        {
            throw;
        }
    }

    void Param_Changed(object sender, EventArgs e)
    {
        paramList.UpdateParam(sender);
        Session["paramList"] = paramList;
    }

    private void AddStringFilter(CrystalDecisions.Shared.ParameterField param)
    {
        try
        {
            ASPxTextBox textBox = new ASPxTextBox();
            textBox.ID = param.Name;
            textBox.CssClass = "TextBox";
            textBox.Caption = param.PromptText;
            textBox.ToolTip = param.PromptText;
            textBox.AutoPostBack = false;
            textBox.RootStyle.CssClass = "editorContainer";
            textBox.CaptionCellStyle.CssClass = "editorCaption";
            textBox.ClientSideEvents.TextChanged = "fnction(source,e){{e.processOnServer = true;)}}";
            textBox.ValueChanged += new System.EventHandler(Param_Changed);

            PanelContent4.Controls.Add(textBox);

            paramList.AddParam(new Param(textBox.ID, ParamType.String, textBox, textBox.Text));
            Session["paramList"] = paramList;

        }
        catch
        {
            throw;
        }
    }

    private void AddSubmitButton()
    {
        try
        {
            ASPxButton submitButton = new ASPxButton();
            submitButton.Text = "Submit";
            submitButton.ID = "Submit";
            submitButton.AutoPostBack = false;
            submitButton.CssClass = "SubmitButton";

            submitButton.Click += new System.EventHandler(Submit_Click);
            PanelContent4.Controls.Add(submitButton);
        }
        catch
        {
            throw;
        }
    }

    void Submit_Click(object sender, EventArgs e)
    {
        //Clear Cache
        CrystalReportViewer1.ReportSource = null;
        CrystalReportViewer1.RefreshReport();

        //Used to generate dynamic HTML
        //ReportDocument report = (ReportDocument)Session["report"];
        report = (ReportDocument)Session["report"];
        CrystalReportViewer1.ReportSource = report;

        paramList = (ParamList)Session["paramList"];

        for (int i = 0; i < CrystalReportViewer1.ParameterFieldInfo.Count; i++)
        {
            ParameterDiscreteValue objDiscreteValue =
                                       new ParameterDiscreteValue();
            ParameterField objParameterField = new ParameterField();
            string paramValue = paramList.GetParamValueByIndex(i);
            string paramType = paramList.GetParamTypeByIndex(i);
            if (paramValue != "")
            {
                switch (paramType)
                {
                    case ParamType.DateTime:
                        paramValue += " 12:00";
                        DateTime d = DateTime.Parse(paramValue);
                        objDiscreteValue.Value = d;//DateTime.ParseExact(d.ToString(),"yyyy-MM-dd HH:mm", null);
                        break;
                    case ParamType.MultiSelect:
                        objDiscreteValue.Value = paramValue;
                        break;
                    case ParamType.String:
                        objDiscreteValue.Value = paramValue;
                        break;
                }
            }
            else
            {
                objDiscreteValue.Value = null;
            }
            objParameterField = CrystalReportViewer1.ParameterFieldInfo[i];
            CrystalReportViewer1.ParameterFieldInfo.RemoveAt(i);

            //need to clear all old values first, otherwise duplicates will be created
            objParameterField.CurrentValues.Clear();
            objParameterField.CurrentValues.Add(objDiscreteValue);

            CrystalReportViewer1.ParameterFieldInfo.Insert(i, objParameterField);

            //Used to generate dynamic HTML
            //report.SetParameterValue(i, objParameterField.CurrentValues);

        }

        //Used to generate dynamic HTML
        //Session["report"] = report;
        //genenrateHTML();
        formateCrystalViewer();
        CrystalReportViewer1.Visible = true;
    }

    private void formateCrystalViewer()
    {
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
        CrystalReportViewer1.HasToggleGroupTreeButton = false;

        CrystalReportViewer1.BestFitPage = false;
        CrystalReportViewer1.EnableParameterPrompt = false;
        CrystalReportViewer1.HasToggleParameterPanelButton = false;

        CrystalReportViewer1.HasCrystalLogo = false;
        CrystalReportViewer1.HasDrilldownTabs = false;

        CrystalReportViewer1.HasDrillUpButton = false;
        CrystalReportViewer1.EnableDrillDown = false;

        CrystalReportViewer1.HasExportButton = true;
        CrystalReportViewer1.HasGotoPageButton = true;
        CrystalReportViewer1.HasPageNavigationButtons = true;
        CrystalReportViewer1.HasPrintButton = true;
        CrystalReportViewer1.HasRefreshButton = false;
        CrystalReportViewer1.HasSearchButton = true;

        CrystalReportViewer1.HasToggleParameterPanelButton = false;
        CrystalReportViewer1.HasZoomFactorList = false;
        CrystalReportViewer1.DisplayToolbar = true;

        //CrystalReportViewer1.BorderWidth = 0;
        CrystalReportViewer1.BorderStyle = BorderStyle.None;
        //CrystalReportViewer1.BorderColor = System.Drawing.Color.White;
        //CrystalReportViewer1.ForeColor = System.Drawing.Color.White;
        //CrystalReportViewer1.BackColor = System.Drawing.Color.White;

        CrystalReportViewer1.DocumentView = DocumentViewType.WebLayout;
        //CrystalReportViewer1.DocumentView = DocumentViewType.PrintLayout;
        //CrystalReportViewer1.ControlStyle.BorderColor = System.Drawing.Color.White;

    }
}



public class ParamListMyReports
{
    private List<ParamMyReports> paramList = new List<ParamMyReports>();

    public ParamListMyReports()
    {
    }

    public void AddParam(ParamMyReports param)
    {
        paramList.Add(param);
    }

    public void UpdateParam(object param)
    {
        ParamMyReports oldParam = new ParamMyReports();
        switch (param.GetType().ToString())
        {
            case "DevExpress.Web.ASPxDateEdit":
                paramList.Find(x => x.ParamName.Contains(((ASPxDateEdit)param).ID)).ParamValue = ((ASPxDateEdit)param).Text;
                break;
            case "DevExpress.Web.ASPxComboBox":
                paramList.Find(x => x.ParamName.Contains(((ASPxComboBox)param).ID)).ParamValue = ((ASPxComboBox)param).Text;
                break;
            case "DevExpress.Web.ASPxTextBox":
                paramList.Find(x => x.ParamName.Contains(((ASPxTextBox)param).ID)).ParamValue = ((ASPxTextBox)param).Text;

                break;
        }

    }

    public string GetParamValueByIndex(int i)
    {
        string returnText = "";
        switch (paramList[i].ParamType)
        {
            case ParamTypeMyReports.DateTime:
                returnText = ((ASPxDateEdit)paramList[i].ParamObject).Text;
                break;
            case ParamTypeMyReports.MultiSelect:
                returnText = ((ASPxComboBox)paramList[i].ParamObject).Text;
                break;
            case ParamTypeMyReports.String:
                returnText = ((ASPxTextBox)paramList[i].ParamObject).Text;
                break;
        }
        return returnText;
    }

    public string GetParamTypeByIndex(int i)
    {
        return paramList[i].ParamType;
    }
}

public class ParamMyReports
{
    public ParamMyReports()
    {
    }
    public ParamMyReports(string paramName, string paramType, object paramObject, string paramValue)
    {
        ParamName = paramName;
        ParamType = paramType;
        ParamObject = paramObject;
        ParamValue = paramValue;
    }
    public string ParamName { get; set; }
    public string ParamType { get; set; }
    public object ParamObject { get; set; }
    public string ParamValue { get; set; }

}

public class ParamTypeMyReports
{
    public const string DateTime = "DateTime";
    public const string MultiSelect = "MultiSelect";
    public const string String = "String";
    public ParamTypeMyReports()
    {
    }
}
