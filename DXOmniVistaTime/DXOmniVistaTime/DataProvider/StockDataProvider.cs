using System;
using System.Collections.Generic;
using System.Text;
using com.UniversaLink.OmniLink.Core;
using com.UniversaLink.OmniLink.Events;
using System.Threading;
using com.UniversaLink.HorizonLink.DataProvider;
using OmniLinkUI.Framework;
using com.UniversaLink.ELink.Core;

namespace com.UniversaLink.OmniLink.UI.DataProvider
{
    public class StockDataProvider : MarshalByRefObject
    {
        public static StockDataProvider instance;
        
        //Commented by Hemal
        //private com.UniversaLink.OmniLink.DataProvider.StockDataProvider stockDataProvider;

        private com.UniversaLink.HorizonLink.DataProvider.StockDataProvider stockDataProvider;

        //  Events
        public event TickerDataEventHandler TickerDataChanged;
        public event MarketDepthEventHandler MarketDepthChanged;
        public event TradeDetailsEventHandler TradeDetailsChanged;

        private StockDataProvider()
        {
            //stockDataProvider = OmniLinkUIApplication.Instance.Services.StockDataProvider;
            try
            {
                //Thread HorizonLinkUIApplicationConnect = new Thread(new ThreadStart(StockDataProviderThread));
                //HorizonLinkUIApplicationConnect.Start();

                StockDataProviderThread();

                /*stockDataProvider = HorizonLinkUIApplication.Instance.Services.StockDataProvider;

                if (stockDataProvider != null)
                {
                    StockDataProviderHelper helper = new StockDataProviderHelper(stockDataProvider);
                    helper.TickerDataChanged += new TickerDataEventHandler(helper_TickerDataChanged);
                    helper.MarketDepthChanged += new MarketDepthEventHandler(helper_MarketDepthChanged);
                    helper.TradeDetailsChanged += new TradeDetailsEventHandler(helper_TradeDetailsChanged);
                }
                */
            }
            catch (Exception ex)
            {
               //
            }
        }

        private void StockDataProviderThread()
        {
            try
            {
                //while (1 == 1)
                //{
                    stockDataProvider = HorizonLinkUIApplication.Instance.Services.StockDataProvider;

                    if (stockDataProvider != null)
                    {
                        StockDataProviderHelper helper = new StockDataProviderHelper(stockDataProvider);
                        helper.TickerDataChanged += new TickerDataEventHandler(helper_TickerDataChanged);
                        helper.MarketDepthChanged += new MarketDepthEventHandler(helper_MarketDepthChanged);
                        helper.TradeDetailsChanged += new TradeDetailsEventHandler(helper_TradeDetailsChanged);
                    }
                //}
            }
            catch (Exception ex)
            {
                //
            }
        }

        void helper_TradeDetailsChanged(object source, TradeDetailsEventArgs e)
        {
            if (TradeDetailsChanged != null)
            {
                TradeDetailsChanged(source, e);
            }
        }

        void helper_MarketDepthChanged(object source, MarketDepthEventArgs e)
        {
            if (MarketDepthChanged != null)
            {
                MarketDepthChanged(source, e);
            }
        }

        void helper_TickerDataChanged(object source, TickerDataEventArgs e)
        {
            if (TickerDataChanged != null)
            {
                TickerDataChanged(source, e);
            }
        }

        public void RaiseTickerDataEvent(Exchange message)
        {
            TickerDataEventArgs args = new TickerDataEventArgs(message);
            if (TickerDataChanged != null)
            {
                TickerDataChanged(this, args);
            }
        }

        public void RaiseMarketDepthEvent(Exchange message)
        {
            MarketDepthEventArgs args = new MarketDepthEventArgs(message);
            if (MarketDepthChanged != null)
            {
                MarketDepthChanged(this, args);
            }
        }

        public void RaiseTradeDetailsEvent(Exchange message)
        {
            TradeDetailsEventArgs args = new TradeDetailsEventArgs(message);
            if (TradeDetailsChanged != null)
            {
                TradeDetailsChanged(this, args);
            }
        }

        public static StockDataProvider Instance
        {
            get
            {
                if (instance == null)
                    instance = new StockDataProvider();
                else if (instance.stockDataProvider == null)
                    instance = new StockDataProvider();
                return instance;
            }
        }

    }
}
