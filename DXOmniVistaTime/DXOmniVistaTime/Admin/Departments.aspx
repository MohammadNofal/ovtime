﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="Departments.aspx.cs" Inherits="Admin_Departments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <dx:ASPxGridView ID="DepartmentsGridView" runat="server" AutoGenerateColumns="true" 
        ClientInstanceName="LoginGridView" Width="98%" AutoPostBack="true" KeyFieldName="DepartmentID"
                    OnRowValidating="DepartmentsGridView_RowValidating"
                    OnRowDeleting = "DepartmentsGridView_RowDeleting"
                    OnRowUpdating = "DepartmentsGridView_RowUpdating"
                    OnRowInserting ="DepartmentsGridView_RowInserting" 
        >
        <SettingsCommandButton>
                        <NewButton  Styles-Style-ForeColor="White" Image-Url="~/Content/Images/Icons/comment-user-add-icon.png">
                            <Image Url="~/Content/Images/Icons/comment-user-add-icon.png">
                            </Image>
                            <Styles>
                                <Style ForeColor="White">
                                </Style>
                            </Styles>
                           
                        </NewButton>
                        <DeleteButton Image-Url="~/Content/Images/Icons/comment-user-close-icon.png">
                            <Image Url="~/Content/Images/Icons/comment-user-close-icon.png">
                            </Image>
                        </DeleteButton>
                        <EditButton Image-Url="~/Content/Images/Icons/comment-user-page-icon.png">
                            <Image Url="~/Content/Images/Icons/comment-user-page-icon.png">
                            </Image>
                        </EditButton>
                        <UpdateButton Image-Url="~/Content/Images/Icons/comment-user-page-icon.png" >
                            <Image Url="~/Content/Images/Icons/comment-user-page-icon.png">
                            </Image>
                        </UpdateButton>
                        <CancelButton Image-Url="~/Content/Images/Icons/comment-user-close-icon.png" >
                            <Image Url="~/Content/Images/Icons/comment-user-close-icon.png">
                            </Image>
                        </CancelButton>
                    </SettingsCommandButton>
        <Columns>
            <dx:GridViewCommandColumn  ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="true" VisibleIndex="0"  >
            </dx:GridViewCommandColumn> 
            <dx:GridViewDataTextColumn FieldName="DepartmentName"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="DepartmentDescription"></dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn FieldName="DepartmentID" Visible="false"></dx:GridViewDataTextColumn>
         </Columns>

    </dx:ASPxGridView>

</asp:Content>

