﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DXOmniVistaTimeEngine;


public partial class Admin_Departments : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadDepartments();
    }
    protected void DepartmentsGridView_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    {

    }
    protected void DepartmentsGridView_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        e.Cancel = true;
        string command = "Delete from Departments where DepartmentID = " + e.Keys["DepartmentID"] ;
        DataAccess.ExecuteSqlStatement(command, "");
        LoadDepartments();
      
     }
    protected void DepartmentsGridView_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {

        string DepartmentID = e.Keys["DepartmentID"].ToString();
        string DepartmentName = String.Empty;
        string DepartmentDescription = String.Empty;
        if (e.NewValues["DepartmentName"] != null)
        {
            DepartmentName = e.NewValues["DepartmentName"].ToString();
        }
        if (e.NewValues["DepartmentDescription"] != null)
        {
            DepartmentDescription = e.NewValues["DepartmentDescription"].ToString();
        }
        string updateCommand = "Update Departments set DepartmentName = '" + DepartmentName + "' , DepartmentDescription =   '" + DepartmentDescription + "' ";

        updateCommand += string.Format(" where DepartmentID={0}", DepartmentID);
        DataAccess.ExecuteSqlStatement(updateCommand,"");
        e.Cancel = true;
        this.DepartmentsGridView.CancelEdit();
        LoadDepartments();
 
    }
    protected void DepartmentsGridView_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        e.Cancel = true;
        string DepartmentName = String.Empty;
        string DepartmentDescription = String.Empty;
        if (e.NewValues["DepartmentName"] != null)
        {
            DepartmentName = e.NewValues["DepartmentName"].ToString();
        }
        if (e.NewValues["DepartmentDescription"] != null)
        {
            DepartmentDescription = e.NewValues["DepartmentDescription"].ToString();
        }

        string sql = "Insert Into Departments (DepartmentName,DepartmentDescription) Values ('" + DepartmentName + "' , '" + DepartmentDescription + "')";
        DataAccess.ExecuteSqlStatement(sql, "");
        e.Cancel = true;
        this.DepartmentsGridView.CancelEdit();
        LoadDepartments();
     }
    void LoadDepartments()
    {
        DataTable Departments = DataAccess.GetDataTableBySqlSyntax("Select * from Departments", "");
        this.DepartmentsGridView.DataSource = Departments;
        this.DepartmentsGridView.DataBind();
    }
}