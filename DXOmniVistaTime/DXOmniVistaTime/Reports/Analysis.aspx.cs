﻿using DevExpress.Web;
using DevExpress.Web.ASPxTreeList;
using DXOmniVistaTimeEngine;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxPivotGrid;
using System.Configuration;
using DevExpress.XtraPrinting;
using DevExpress.Utils;
using DevExpress.Export;

public partial class Reports_Analysis : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        if(this.DropDownEdit.SelectedItem != null)
        {
            string ProjectId = this.DropDownEdit.SelectedItem.Value.ToString();
            if (ProjectId != String.Empty)
                Analysis_Click(ProjectId);
        }

    }
    protected void TreeList_Init(object sender, EventArgs e)
    {
         ASPxComboBox dropdown = (ASPxComboBox )sender;
         dropdown.DataSource = GetProjects();
         dropdown.DataBind();
         
    }
    private DataTable GetProjects()
    { 
        string sql = "Select * From Project where ParentProjectID is null";
        DataTable Projects = DataAccess.GetDataTableBySqlSyntax(sql, "");
        return Projects;
    }
    protected void Analysis_Click(string ProjectID)
    {
        string sql = "Select ISNULL(p1.ProjectName,'N/A')  as DesignStage,ISNULL(p1.ProjectID,'N/A') as ProjectID,ISNULL(p2.ProjectName,'N/A') as DetailsStage,ISNULL(p2.ProjectID,'N/A'),ISNULL(p3.ProjectName,'N/A') as " +
"DetailsStage2,ISNULL(p3.ProjectID,'N/A'),t.EmployeeID,t.aHours ,ISNULL(e.EmpFName +' ' + E.EmpLName,'N/A') as EmpName ,e.EmpBillRate,e.EmpCostRate " +
",(t.aHours * e.EmpCostRate) as EmpCost " +
"from Project p0 "+
"left join Project p1 on p1.ParentProjectID  = p0.ProjectID    "+
"left join Project p2 on p2.ParentProjectID  = p1.ProjectID  "+
"left join Project p3 on p3.ParentProjectID  = p2.ProjectID "+
"left join TimeEntry t on t.ProjectID in(p0.ProjectID,p1.ProjectID,p2.ProjectID,p3.ProjectID)"+
"left join Employee e on t.EmployeeID = e.EmployeeID "+
"where p0.ParentProjectID is null and p0.ProjectID = '"+ProjectID+"'" ;

         DataTable Stages = DataAccess.GetDataTableBySqlSyntax(sql, "");
         pivotGrid.DataSource = Stages;
         pivotGrid.DataBind();

    }
    protected void ASPxCallbackPanel2_Callback(object sender, CallbackEventArgsBase e)
    {
        string ProjectId = this.DropDownEdit.SelectedItem.Value.ToString();
        if(ProjectId!=String.Empty)
            Analysis_Click(ProjectId);

    }

    void Export(bool saveAs)
    {
        foreach (PivotGridField field in pivotGrid.Fields)
        {
            if (field.ValueFormat != null && !string.IsNullOrEmpty(field.ValueFormat.FormatString))
                field.UseNativeFormat =  DefaultBoolean.True;
        }

        ASPxPivotGridExporter1.OptionsPrint.PrintHeadersOnEveryPage = true;
        ASPxPivotGridExporter1.OptionsPrint.MergeColumnFieldValues = true;
        ASPxPivotGridExporter1.OptionsPrint.MergeRowFieldValues = true;

        ASPxPivotGridExporter1.OptionsPrint.PrintFilterHeaders =  DefaultBoolean.True;
        ASPxPivotGridExporter1.OptionsPrint.PrintColumnHeaders = DefaultBoolean.True;
        ASPxPivotGridExporter1.OptionsPrint.PrintRowHeaders = DefaultBoolean.True;
        ASPxPivotGridExporter1.OptionsPrint.PrintDataHeaders = DefaultBoolean.True;

        const string fileName = "PivotGrid";
        XlsxExportOptionsEx options;
        switch (listExportFormat.SelectedIndex)
        {
            case 0:
                ASPxPivotGridExporter1.ExportPdfToResponse(fileName, saveAs);
                break;
            case 1:
                options = new XlsxExportOptionsEx() { ExportType = ExportType.WYSIWYG };
                ASPxPivotGridExporter1.ExportXlsxToResponse(fileName, options, saveAs);
                break;
            case 2:
                ASPxPivotGridExporter1.ExportMhtToResponse(fileName, "utf-8", "ASPxPivotGrid Printing Sample", true, saveAs);
                break;
            case 3:
                ASPxPivotGridExporter1.ExportRtfToResponse(fileName, saveAs);
                break;
            case 4:
                ASPxPivotGridExporter1.ExportTextToResponse(fileName, saveAs);
                break;
            case 5:
                ASPxPivotGridExporter1.ExportHtmlToResponse(fileName, "utf-8", "ASPxPivotGrid Printing Sample", true, saveAs);
                break;
            case 6:
                options = new XlsxExportOptionsEx()
                {
                    ExportType = ExportType.DataAware,
                    AllowGrouping =   DefaultBoolean.True,
                    TextExportMode =   TextExportMode.Text ,
                    AllowFixedColumns =  DefaultBoolean.True,
                    AllowFixedColumnHeaderPanel =   DefaultBoolean.True,
                    RawDataMode = true
                };
                ASPxPivotGridExporter1.ExportXlsxToResponse(fileName, options, saveAs);
                break;
        }

    }

    protected void buttonOpen_Click(object sender, EventArgs e)
    {
        Export(false);
    }
    protected void buttonSaveAs_Click(object sender, EventArgs e)
    {
        Export(true);
    }
}