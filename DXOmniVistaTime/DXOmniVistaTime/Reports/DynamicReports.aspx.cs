﻿using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web.Export;
using DevExpress.XtraPrinting;
using DXOmniVistaTimeEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;

public partial class DynamicReports : System.Web.UI.Page
{
    Dictionary<string,ASPxDropDownEdit> DropDownList = new Dictionary<string,ASPxDropDownEdit>();
    Dictionary<string, ASPxSpinEdit> IntBox1List = new Dictionary<string,ASPxSpinEdit>();
    Dictionary<string, ASPxSpinEdit> IntBox2List = new Dictionary<string,ASPxSpinEdit>();
    Dictionary<string, ASPxDateEdit> Calendar1List = new Dictionary<string, ASPxDateEdit>();
    Dictionary<string, ASPxDateEdit> Calendar2List = new Dictionary<string, ASPxDateEdit>();
    Dictionary<string,ASPxTextBox> ColumnNameList = new Dictionary<string, ASPxTextBox>();
    Dictionary<string, ASPxCheckBox> CheckBoxList = new Dictionary<string, ASPxCheckBox>();

    protected void Page_Load(object sender, EventArgs e)
    {

        //Works for first time
        if (!IsPostBack)
        {
           
            Session["DynamicReports_DropDownList"] = null;
            Session["DynamicReports_IntBox1List"] = null;
            Session["DynamicReports_IntBox2List"] = null;
            Session["DynamicReports_Calendar1List"] = null;
            Session["DynamicReports_Calendar2List"] = null;
            Session["DynamicReports_ColumnNameList"] = null;
            Session["DynamicReports_CheckBoxList"] = null;
            Session["DynamicReports_ids"] = null;
            Session["DynamicReports_ReportGrid"] = null;
            Session["DynamicReports_GroupbyValye"] = null;
            Session["DynamicReports_GroupByReportGrid"] = null;
            Session["DynamicReports_ReportID"] = null;
            Session["DynamicReports_ReportName"] = null;
            Session["DynamicReports_OVReportTags"] = null;
            Session["DynamicReports_ReportDescription"] = null;
            //Session["DynamicReports_GridView"] = null;
           
            AssignReportTitlePanel();
        }
        else
        {

            if (Session["DynamicReports_GroupByReportGrid"] != null)
            {
                GroupByRefreshGrid();
            }
            else
            refreshDataGrid();
        }

    }
    private void AssignReportTitlePanel()
    {
        DataTable t = DataAccess.GetDataTableBySqlSyntax("SELECT Id, Name, TagId, Description FROM OVReports WHERE EmployeeID= '" + Membership.GetUser(User.Identity.Name) + "'  ORDER BY NAME ASC", "");

        int timesheetDescriptionCounter = 0;
        int financialDescriptionCounter = 0;
        int ManagementDescriptionCounter = 0;
        int DescriptionMinWidth = 120;
        int DescriptionMaxWidth = 555;
        foreach (DataRow row in t.Rows) {
           // System.Web.UI.HtmlControls.HtmlGenericControl li = new System.Web.UI.HtmlControls.HtmlGenericControl("li");
            System.Web.UI.HtmlControls.HtmlGenericControl a = new System.Web.UI.HtmlControls.HtmlGenericControl("a");
            System.Web.UI.HtmlControls.HtmlGenericControl span = new System.Web.UI.HtmlControls.HtmlGenericControl("span");

            a.InnerText = row["Name"].ToString();
            a.ID = "|ReportID|" + row["Id"].ToString() + "|" + row["Name"].ToString() + "|";
            a.Attributes.Add("onclick", "changereport(event)");

            span.Attributes.Remove("onclick");
            span.Attributes.Add("onclick", "donothing()");
            span.InnerHtml = row["Description"].ToString();
            span.Attributes.Add("class", "dropdownreportDescritpion tooltiptext");
            if (row["Description"].ToString().Length == 0) span.Style.Add("width", "0");
            else if (row["Description"].ToString().Length <= 11) span.Style.Add("width", DescriptionMinWidth.ToString());
            else
            {
                double newWidth = DescriptionMinWidth + (row["Description"].ToString().Length * 3.7);
                if (newWidth >= DescriptionMaxWidth) span.Style.Add("width", DescriptionMaxWidth.ToString());
                else
                {
                    span.Style.Add("width", newWidth.ToString());
                }

            }
            a.Controls.Add(span);

            if (row["TagId"].ToString() == "1")
            {
                int descriptionPosition = 60 + (25 * timesheetDescriptionCounter);
                span.Style.Add("top", descriptionPosition.ToString());
                this.TimeSheetUl.Controls.Add(a);
                timesheetDescriptionCounter++;

            }
            else if (row["TagId"].ToString() == "2")
            {
                int descriptionPosition = 60 + (25 * financialDescriptionCounter);
                span.Style.Add("top", descriptionPosition.ToString());
                this.FinancialUl.Controls.Add(a);
                financialDescriptionCounter++;
            }
            else if (row["TagId"].ToString() == "3")
            {
                int descriptionPosition = 60 + (25 * ManagementDescriptionCounter);
                span.Style.Add("top", descriptionPosition.ToString());
                this.ManagementUl.Controls.Add(a);
                ManagementDescriptionCounter++;
            }

        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["DynamicReports_GroupByReportGrid"] != null)
        {
            ASPxGridViewReport.DataSource = (DataTable)Session["DynamicReports_GroupByReportGrid"];
            this.ASPxGridViewReport.DataBind();
        }
        else
        {
            if (Session["DynamicReports_ReportGrid"] != null)
            {
                ASPxGridViewReport.DataSource = (DataTable)Session["DynamicReports_ReportGrid"];
                this.ASPxGridViewReport.DataBind();
            }
        }
    }
   

    private void refreshDataGrid()
    {
        if (Session["DynamicReports_ReportGrid_ColumnList"] != null)
        {
            DataTable ColumnList = (DataTable)Session["DynamicReports_ReportGrid_ColumnList"];
            ASPxGridViewReport.Columns.Clear();

            foreach (DataRow dr in ColumnList.Rows)
            {
                if (dr["ColumnName"].ToString().Equals("TEDate"))
                {
                    GridViewDataDateColumn Column = new GridViewDataDateColumn();
                    Column.Caption = dr["ColumnDisplayName"].ToString();
                    Column.FieldName = dr["ColumnDisplayName"].ToString();
                    Column.PropertiesDateEdit.DisplayFormatString = "dd/MM/yyyy";
                    Column.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                    ASPxGridViewReport.Columns.Add(Column);
                }
                else
                {
                    GridViewDataColumn Column = new GridViewDataColumn();
                    Column.Caption = dr["ColumnDisplayName"].ToString();
                    Column.FieldName = dr["ColumnDisplayName"].ToString();
                    ASPxGridViewReport.Columns.Add(Column);
                }
            }
        }
        if (Session["DynamicReports_ReportGrid"] != null)
        {
            ASPxGridViewReport.DataSource = (DataTable)Session["DynamicReports_ReportGrid"];
            this.ASPxGridViewReport.DataBind();
        }
    }
    #region Call Backs
    protected void DisplayReportExportOptions_CallBack(object sender, CallbackEventArgsBase e)
    {
        this.ExporttoExecBtn.Visible = true;
        this.ExporttoPDFBtn.Visible = true;
        this.CustomizePDFBtn.Visible = true;
        if (e.Parameter == "Hide")
        {
            this.ExporttoExecBtn.Visible = false;
            this.ExporttoPDFBtn.Visible = false;
            this.CustomizePDFBtn.Visible = false;
        }
        //set report title
        if (Session["DynamicReports_ReportName"] != null) this.reportNameTitle.InnerHtml = "Reports : " + Session["DynamicReports_ReportName"].ToString();
    }
    protected void ReportPanel_CallBack(object sender, CallbackEventArgsBase e)
    {
        string[] parts = e.Parameter.Split(new[] { ',' }, 2);
        if (e.Parameter == "PopUpNewReport") //Show the New report popup 
        {
            ASPxEdit.ClearEditorsInContainer(this, "popupValidationGroup");
            NewReportPopUpControl.HeaderText = "New Report";
            ReportNameTextBox.Text = "";
            this.ReportDescriptionTextBox.Text = "";
            UnSelectAllTreeNodes(ref ASPxtreeViewEmployee);
            UnSelectAllTreeNodes(ref ASPxtreeProject);
            UnSelectAllTreeNodes(ref ASPxTreeClient);
            UnSelectAllTreeNodes(ref ASPxTreeTimeSheet);
            ASPxtreeViewEmployee.ExpandAll();
            ASPxtreeProject.ExpandAll();
            ASPxTreeClient.ExpandAll();
            ASPxTreeTimeSheet.ExpandAll();
            this.NewReportPopUpControl.ShowOnPageLoad = true;
            AssignTagstoComboBox();
        }
        else if (parts[0] == "SaveNewReport0")//Save the new report and assign to the combo box to display it
        {
            if (Session["DynamicReports_OVReportTags"] !=null)
            {
                this.ASPxComboBoxReportTag.DataSource = (DataTable)Session["DynamicReports_OVReportTags"];
                this.ASPxComboBoxReportTag.DataBind();
            }
            int reportid = -1; string reportName = ""; string ReportDescription = "";
            SaveReport(parts[1], ref reportid, ref reportName , ref ReportDescription);
            Session["DynamicReports_ReportDescription"] = ReportDescription;
            Session["DynamicReports_ReportName"] = reportName;
            string sql = "";
            BindGrid(reportid, ref sql);
            Session["DynamicReports_ReportID"] = reportid;
            Session["DynamicReports_sql"] = sql;
            this.ComboBoxGroupBy.Text = "";
            
            Session["DynamicReports_GroupbyValye"] = null;
            Session["DynamicReports_GroupByReportGrid"] = null;

            //this.ComboBoxReports.SelectedItem = this.ComboBoxReports.Items.FindByText(reportName);
        }
        else if (parts[0] == "SaveNewReport1")//Modify Report (Delete old report then save the new modified report)
        {
            int reportID = (int)Session["DynamicReports_ReportID"];
            string query = "DELETE FROM OVReports WHERE Id=" + reportID + ";";
            query += "DELETE FROM OVReportColumns WHERE ReportId=" + reportID + ";";
            query += "DELETE FROM OVReportFilters WHERE ReportId=" + reportID + ";";
            query += "DELETE FROM OVReportJoins WHERE ReportId=" + reportID + ";";
            DataAccess.ExecuteSqlStatement(query, "");

            if (Session["DynamicReports_OVReportTags"] != null)
            {
                this.ASPxComboBoxReportTag.DataSource = (DataTable)Session["DynamicReports_OVReportTags"];
                this.ASPxComboBoxReportTag.DataBind();
            }
            int reportid = -1; string reportName = ""; string ReportDescription = "";
            SaveReport(parts[1], ref reportid, ref reportName, ref ReportDescription);
            Session["DynamicReports_ReportDescription"] = ReportDescription;
            Session["DynamicReports_ReportName"] = reportName;
            string sql = "";
            BindGrid(reportid, ref sql);
            Session["DynamicReports_ReportID"] = reportid;
            Session["DynamicReports_sql"] = sql;
            this.ComboBoxGroupBy.Text = "";
            
            Session["DynamicReports_GroupbyValye"] = null;
            Session["DynamicReports_GroupByReportGrid"] = null;

            //ComboBoxReports.Items.Clear();
            
           
        }
        else if (parts[0] == "DisplayReport")
        {
            string[] reportdetails = parts[1].Split(',');
            //Get report Data and Assign Grid with report Data
            string sql = "";
            BindGrid(int.Parse(reportdetails[0]), ref sql);
            Session["DynamicReports_ReportID"] = int.Parse(reportdetails[0]);
            
            Session["DynamicReports_ReportDescription"] = DataAccess.GetDataTableBySqlSyntax("SELECT top 1 Description FROM OVReports WHERE Id=" + reportdetails[0], "").Rows[0]["Description"].ToString(); 
            Session["DynamicReports_ReportName"] = reportdetails[1];
            Session["DynamicReports_sql"] = sql;

            this.ComboBoxGroupBy.Text = "";
            Session["DynamicReports_GroupbyValye"] = null;
            Session["DynamicReports_GroupByReportGrid"] = null;
        }
        else if (parts[0] == "GroupbyChanged")
        {
            refreshDataGrid();
            if (parts[1].Equals(""))
            {
                Session["DynamicReports_GroupbyValye"] = null;
                Session["DynamicReports_GroupByReportGrid"] = null;
            }
            else Session["DynamicReports_GroupbyValye"] = parts[1];
            //ShowReportItems();
        }
        else if (parts[0] == "DateFromChanged")
        {
            //string sql = (string)Session["DynamicReports_sql"];
            string DateFrom = parts[1];
            int ReportID = (int)Session["DynamicReports_ReportID"];
            string DeleteDateQuery = "DELETE FROM OVReportFilters WHERE ReportId = " + ReportID + " AND FilterName='TEDate' AND TableName='TimeEntry' AND DisplayName='Date' AND FilterOperation='>='";
            DataAccess.ExecuteSqlStatement(DeleteDateQuery, "");
            if (!DateFrom.Equals("NoDate"))
            {
                string AddDateQuery = "INSERT INTO OVReportFilters (FilterName,DisplayName,ReportId,TableName,FilterTypeId,FilterOperation,FilterValue,AliasTableName) VALUES ('TEDate','Date'," + ReportID + ",'TimeEntry',2,'>=', '" + DateFrom + "', 'TimeEntry');";
                DataAccess.ExecuteSqlStatement(AddDateQuery, "");
            }
            string sql = "";
            BindGrid(ReportID, ref sql);
            Session["DynamicReports_sql"] = sql;


        }
        else if (parts[0] == "DateToChanged")
        {
            // string sql = (string)Session["DynamicReports_sql"];
            string DateTo = parts[1];
            int ReportID = (int)Session["DynamicReports_ReportID"];
            string DeleteDateQuery = "DELETE FROM OVReportFilters WHERE ReportId = " + ReportID + " AND FilterName='TEDate' AND TableName='TimeEntry' AND DisplayName='Date' AND FilterOperation='<='";
            DataAccess.ExecuteSqlStatement(DeleteDateQuery, "");
            if (!DateTo.Equals("NoDate"))
            {
                string AddDateQuery = "INSERT INTO OVReportFilters (FilterName,DisplayName,ReportId,TableName,FilterTypeId,FilterOperation,FilterValue,AliasTableName) VALUES ('TEDate','Date'," + ReportID + ",'TimeEntry',2,'<=', '" + DateTo + "', 'TimeEntry');";
                DataAccess.ExecuteSqlStatement(AddDateQuery, "");
            }
            string sql = "";
            BindGrid(ReportID, ref sql);
            Session["DynamicReports_sql"] = sql;
        }
        else if (e.Parameter == "DeleteReport")
        {
            int reportID = (int)Session["DynamicReports_ReportID"];
            string query = "DELETE FROM OVReports WHERE Id=" + reportID + ";";
            query += "DELETE FROM OVReportColumns WHERE ReportId=" + reportID + ";";
            query += "DELETE FROM OVReportFilters WHERE ReportId=" + reportID + ";";
            query += "DELETE FROM OVReportJoins WHERE ReportId=" + reportID + ";";

            DataAccess.ExecuteSqlStatement(query, "");
            
            Session["DynamicReports_ReportID"] = null;
            Session["DynamicReports_sql"] = null; 

        }
        else if (e.Parameter == "ModifyReport")
        {
            ASPxEdit.ClearEditorsInContainer(this, "popupValidationGroup");
            ModifyReport();
           
        }
       
       
        AssignReportTitlePanel();
        ShowReportItems();
        //AddTotalGridRow();
          

        

    }

    private void ShowReportItems()
    {
       // if (!this.ComboBoxReports.Text.Equals("")) //ref for report navigation
        if (Session["DynamicReports_ReportID"] !=null)
        {
            //Show From and To Date if TimeEntry table is choosen
            string sql = (string)Session["DynamicReports_sql"];
            string[] sqlparts = sql.Split(new string[] { "From" }, StringSplitOptions.None);
            if (sqlparts.Length > 1)
            {
                if (sqlparts[1].Contains("TimeEntry"))
                {
                    
                    int reportid = (int)Session["DynamicReports_ReportID"];
                    DataTable t = DataAccess.GetDataTableBySqlSyntax("SELECT FilterValue, FilterOperation FROM OVReportFilters WHERE ReportId=" + reportid + " AND TableName='TimeEntry' AND FilterName='TEDate' AND FilterTypeId=2", "");
                    if (t.Rows.Count == 0) { this.ASPxDateFrom.Date = DateTime.MinValue; this.ASPxDateTo.Date = DateTime.MinValue; }
                    foreach (DataRow row in t.Rows)
                    {
                        if (row["FilterOperation"].ToString().Equals(">="))
                        {
                            string dateFormated = DateTime.ParseExact(row["FilterValue"].ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
                            ASPxDateFrom.Date = DateTime.ParseExact(dateFormated, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        }
                        else if (row["FilterOperation"].ToString().Equals("<="))
                        {
                            string dateFormated = DateTime.ParseExact(row["FilterValue"].ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
                            ASPxDateTo.Date = DateTime.ParseExact(dateFormated, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            
                        }
                    }
                    ASPxDateFrom.Visible = true;
                    ASPxDateTo.Visible = true;
                }
            }
            //Assign Group By based on the report columns 
            int id = (int)Session["DynamicReports_ReportID"];
            XmlDocument doc = new XmlDocument();
            doc.Load(Server.MapPath("~/App_Data/GroupByColumns.xml"));
            XmlNodeList elementList = doc.GetElementsByTagName("Column");
            this.ComboBoxGroupBy.Items.Clear();
            this.ComboBoxGroupBy.Items.Add("", "");
            for (int i = 0; i < elementList.Count; i++)
            {
                DataTable captionName = DataAccess.GetDataTableBySqlSyntax(" SELECT distinct DisplayName FROM OVReportColumns WHERE ReportId =" + id + " AND ColumnName='" + elementList[i].InnerXml + "'", "");
                if (captionName != null)
                {
                    if (captionName.Rows.Count != 0 && this.ASPxGridViewReport.Columns[captionName.Rows[0][0].ToString()] != null) //Grid has the column
                    {
                        //Text is from the Grid Column Caption
                        if (elementList[i].InnerXml.Equals("TEDate"))
                        {
                            this.ComboBoxGroupBy.Items.Add("Day", this.ASPxGridViewReport.Columns[captionName.Rows[0][0].ToString()].Caption);
                            this.ComboBoxGroupBy.Items.Add("Week", "Week");
                            this.ComboBoxGroupBy.Items.Add("Month", "Month");
                            this.ComboBoxGroupBy.Items.Add("Year","Year");
                        }
                        else 
                            this.ComboBoxGroupBy.Items.Add(this.ASPxGridViewReport.Columns[captionName.Rows[0][0].ToString()].Caption, elementList[i].InnerXml);
                    }
                }

            }
            ASPxListBox list = this.checkComboBox.FindControl("listBoxGroupBy") as ASPxListBox;
            list.Items.Clear();
            for (int i = 0; i < elementList.Count; i++)
            {
                DataTable captionName = DataAccess.GetDataTableBySqlSyntax(" SELECT distinct DisplayName FROM OVReportColumns WHERE ReportId =" + id + " AND ColumnName='" + elementList[i].InnerXml + "'", "");
                if (captionName != null)
                {
                    if (captionName.Rows.Count != 0 && this.ASPxGridViewReport.Columns[captionName.Rows[0][0].ToString()] != null) //Grid has the column
                    {
                        //Text is from the Grid Column Caption
                        if (elementList[i].InnerXml.Equals("TEDate"))
                        {
                            list.Items.Add("Day", this.ASPxGridViewReport.Columns[captionName.Rows[0][0].ToString()].Caption);
                            list.Items.Add("Week", "Week");
                            list.Items.Add("Month", "Month");
                            list.Items.Add("Year", "Year");
                        }
                        else
                            list.Items.Add(this.ASPxGridViewReport.Columns[captionName.Rows[0][0].ToString()].Caption, elementList[i].InnerXml);
                    }
                }

            }
            list.Height = list.Items.Count * 27;
            //Show Group By option and apply Group By
            //this.checkComboBox.Visible = true;
           ComboBoxGroupBy.Visible = true;
            
            if (Session["DynamicReports_GroupbyValye"] == null)
            {
                foreach (GridViewDataColumn c in this.ASPxGridViewReport.Columns)
                {
                    this.ASPxGridViewReport.UnGroup(c);
                    //this.ASPxGridViewReport.Styles.AlternatingRow.Enabled = DevExpress.Utils.DefaultBoolean.True;
                }
            }
            else
            {
                Session["DynamicReports_GroupByReportGrid"] = null;
                string GroubyValue = (string)Session["DynamicReports_GroupbyValye"];
                if (GroubyValue.Equals("Week"))
                {
                    //Get the SQL
                    string GroupByWeekQuery = (string)Session["DynamicReports_sql"];
                    //MOdify SQL so that SUM(HOURS & COST) is every week
                    GroupByWeekQuery = "SET DATEFIRST 7;" + GroupByWeekQuery;
                   
                    string columnName = (string)this.ComboBoxGroupBy.Items.FindByText("Day").Value;

                    string selectquery = "(DATEADD(dd, @@DATEFIRST - DATEPART(dw, TEDate) - 6, TEDate)) AS 'Week'";
                    string groupbyquery = "(DATEADD(dd, @@DATEFIRST - DATEPART(dw, TEDate) - 6, TEDate))";
                    string orderbyquery = " ORDER BY (DATEADD(dd, @@DATEFIRST - DATEPART(dw, TEDate) - 6, TEDate))";
                    GroupByWeekQuery = CreateGroupBySQL(GroupByWeekQuery, columnName, selectquery, groupbyquery, orderbyquery);

                   // string[] parts = GroupByWeekQuery.Split(new string[] { "From" }, StringSplitOptions.None);
                   //   parts[0] = parts[0].Replace("TimeEntry.TEDate as '" + columnName + "'", "(DATEADD(dd, @@DATEFIRST - DATEPART(dw, TEDate) - 6, TEDate)) AS 'Week'");
                   // GroupByWeekQuery = parts[0] + " From " + parts[1];

                    // string[] groupBySplit = GroupByWeekQuery.Split(new string[] { "GROUP BY" }, StringSplitOptions.None);
                    //   groupBySplit[1] = groupBySplit[1].Replace("TimeEntry.TEDate", "(DATEADD(dd, @@DATEFIRST - DATEPART(dw, TEDate) - 6, TEDate))");
                    // GroupByWeekQuery = groupBySplit[0] + " GROUP BY " + groupBySplit[1];
                    //GroupByWeekQuery = GroupByWeekQuery + " ORDER BY (DATEADD(dd, @@DATEFIRST - DATEPART(dw, TEDate) - 6, TEDate))";

                    //Create new DataTable from the new SQl
                    DataTable t = DataAccess.GetDataTableBySqlSyntax(GroupByWeekQuery, "");
                    //Assign DataTable to Session["DynamicReports_ReportGrid"]
                    Session["DynamicReports_GroupByReportGrid"] = t;

                    GroupByRefreshGrid();

                    //Assign the grid group by
                    //string columnName = (string)this.ComboBoxGroupBy.Items.FindByText("Day").Value;
                    ////  this.ASPxGridViewReport.SortBy(this.ASPxGridViewReport.Columns[c], DevExpress.Data.ColumnSortOrder.Descending);
                    
                    this.ASPxGridViewReport.GroupBy(this.ASPxGridViewReport.Columns["Week"]);


                }
                else if (GroubyValue.Equals("Month"))
                {
                    string GroupByWeekQuery = (string)Session["DynamicReports_sql"];
                    string columnName = (string)this.ComboBoxGroupBy.Items.FindByText("Day").Value;
                    string selectquery = "CONVERT(DATETIME,LEFT(CONVERT(varchar(9), TimeEntry.TEDate, 120),7) + '-01',120) AS 'Month'";
                    string groupbyquery = "LEFT(CONVERT(varchar(9), TimeEntry.TEDate, 120),7)";
                    string orderbyquery = " ORDER BY MIN(TimeEntry.TEDate) DESC";
                    GroupByWeekQuery = CreateGroupBySQL(GroupByWeekQuery, columnName, selectquery, groupbyquery, orderbyquery);
                    
                    DataTable t = DataAccess.GetDataTableBySqlSyntax(GroupByWeekQuery, "");
                    Session["DynamicReports_GroupByReportGrid"] = t;

                    GroupByRefreshGrid();
                    this.ASPxGridViewReport.GroupBy(this.ASPxGridViewReport.Columns["Month"]);
                }
                else if (GroubyValue.Equals("Year")) {

                    string GroupByWeekQuery = (string)Session["DynamicReports_sql"];
                    string columnName = (string)this.ComboBoxGroupBy.Items.FindByText("Day").Value;
                    string selectquery = "CONVERT(DATETIME,LEFT(CONVERT(varchar(9), TimeEntry.TEDate, 120),4) + '-01-01',120) AS 'Year'";
                    string groupbyquery = "LEFT(CONVERT(varchar(9), TimeEntry.TEDate, 120),4)";
                    string orderbyquery = " ORDER BY MIN(TimeEntry.TEDate) DESC";
                    GroupByWeekQuery = CreateGroupBySQL(GroupByWeekQuery, columnName, selectquery, groupbyquery, orderbyquery);

                    DataTable t = DataAccess.GetDataTableBySqlSyntax(GroupByWeekQuery, "");
                    Session["DynamicReports_GroupByReportGrid"] = t;

                    GroupByRefreshGrid();
                    this.ASPxGridViewReport.GroupBy(this.ASPxGridViewReport.Columns["Year"]);
                }
                else if (GroubyValue.Equals("Day"))
                {
                    string c = (string)this.ComboBoxGroupBy.Items.FindByText("Day").Value;
                    Session["DynamicReports_GroupbyGridDay"] = this.ASPxGridViewReport.Columns[c].Caption;
                    this.ASPxGridViewReport.GroupBy(this.ASPxGridViewReport.Columns[c]);
                   
                }
                else
                {
                    this.ASPxGridViewReport.GroupBy(this.ASPxGridViewReport.Columns[GroubyValue]);   
                }
                //this.ASPxGridViewReport.Styles.AlternatingRow.Enabled = DevExpress.Utils.DefaultBoolean.False;
                this.ASPxGridViewReport.ExpandAll();
            }

            ////set report title
            //if (Session["DynamicReports_ReportName"] != null) this.reportNameTitle.InnerHtml = Session["DynamicReports_ReportName"].ToString();
            //set report description
            if (Session["DynamicReports_ReportDescription"] != null) this.reportDescription.InnerHtml = Session["DynamicReports_ReportDescription"].ToString();
            //Show grid
            ASPxGridViewReport.Visible = true;
            //Show Modify Report Button
            //ModifyBtn.Visible = true;
            ModifyBtn2.Visible = true;
            //Show Delete Report Button
            //DeleteBtn.Visible = true;
            DeleteBtn2.Visible = true;
            //Show Export to PDF btn
            //ExporttoPDFBtn.Visible = true;
            //CustomizePDFBtn.Visible = true;
        }
    }
    private void GroupByRefreshGrid()
    {
        if (Session["DynamicReports_GroupByReportGrid"] != null)
        {
            this.ASPxGridViewReport.Columns.Clear();
            DataTable t = (DataTable)Session["DynamicReports_GroupByReportGrid"];
            foreach (DataColumn column in t.Columns)
            {

                if (column.Caption.Equals("Week") || column.Caption.Equals("Month") || column.Caption.Equals("Year"))
                {
                    GridViewDataDateColumn gridcolumn = new GridViewDataDateColumn();
                    gridcolumn.Caption = column.Caption;
                    gridcolumn.FieldName = column.Caption;
                    ASPxGridViewReport.Columns.Add(gridcolumn);
                }
                else
                {
                    GridViewDataColumn gridcolumn = new GridViewDataColumn();
                    gridcolumn.Caption = column.Caption;
                    gridcolumn.FieldName = column.Caption;
                    ASPxGridViewReport.Columns.Add(gridcolumn);
                }
                
            }
            this.ASPxGridViewReport.DataSource = (DataTable)Session["DynamicReports_GroupByReportGrid"];
            this.ASPxGridViewReport.DataBind();
        }
    }
    private void AddTotalGridRow()
    {
        //if (this.ASPxGridViewReport.Columns[])
    }
    protected void ASPxGridViewReport_CustomGroupDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "Week")
        {
            e.DisplayText = ((DateTime)e.GetFieldValue("Week")).ToString("dd/MM/yyyy");
        }
        else if (e.Column.FieldName == "Month")
        {
            e.DisplayText = ((DateTime)e.GetFieldValue("Month")).ToString("MMM yyyy");
        }
        else if (e.Column.FieldName == "Year")
        {
            e.DisplayText = ((DateTime)e.GetFieldValue("Year")).ToString("yyyy");
        }
        else if (Session["DynamicReports_GroupbyGridDay"] != null)
        {
            string day = (string)Session["DynamicReports_GroupbyGridDay"];
            if (e.Column.FieldName == day)
            {
                e.DisplayText = ((DateTime)e.GetFieldValue(day)).ToString("dd/MM/yyyy");
            }
        }
    }
    private string CreateGroupBySQL(string query, string columnName, string selectColumnQuery, string groupbycolumnquery, string orderquery)
    {
        string[] parts = query.Split(new string[] { "From" }, StringSplitOptions.None);
        parts[0] = parts[0].Replace("TimeEntry.TEDate as '" + columnName + "'", selectColumnQuery);
        query = parts[0] + " From " + parts[1];

        string[] groupBySplit = query.Split(new string[] { "GROUP BY" }, StringSplitOptions.None);
        groupBySplit[1] = groupBySplit[1].Replace("TimeEntry.TEDate", groupbycolumnquery);
        query = groupBySplit[0] + " GROUP BY " + groupBySplit[1];
        query = query + orderquery;

        return query;

    }
    protected void FilterPanel_CallBack(object sender, CallbackEventArgsBase e)
    {
        if (e.Parameter == "ShowFilters0") //New report
        {
            AssignFilters(false);
            this.ASPxPopupControlFilters.HeaderText = "New Report";
            this.SubmitBttn.Text = "Create Report";
            this.ASPxPopupControlFilters.ShowOnPageLoad = true;
        }
        else if (e.Parameter == "ShowFilters1") //Modify report
        {

            //Check if each Selected Tree already has filters already in DB (if yes, assign filters, if no create a normal row)
            AssignFilters(true);
            //Show the popup
            this.ASPxPopupControlFilters.HeaderText = "Modify Report";
            this.SubmitBttn.Text = "Modify Report";
            this.ASPxPopupControlFilters.ShowOnPageLoad = true;
        }
    }
    protected void ValueChanged_CallBack(object sender, CallbackEventArgsBase e)
    {
        string[] es = e.Parameter.Split(new[] { '|' }, 2);//We dont need the first part
        string[] parts = es[1].Split(',');
        string id = parts[0] + "," + parts[1] + "," + parts[2] + "," + parts[3] + "," + parts[4];
        string tablename = parts[0];
        string columnName = parts[1];
        string type = parts[2];
        //Update the filter values
        if (parts[5] == "textbox")
        {
            if (Session["DynamicReports_ColumnNameList"] != null) ColumnNameList = (Dictionary<String, ASPxTextBox>)Session["DynamicReports_ColumnNameList"];
            ColumnNameList[id].Value = parts[6];
            ColumnNameList[id].Text = parts[6];
            Session["DynamicReports_ColumnNameList"] = ColumnNameList;
        }
        else if (parts[5] == "checkbox")
        {
            if (Session["DynamicReports_CheckBoxList"] != null) CheckBoxList = (Dictionary<string, ASPxCheckBox>)Session["DynamicReports_CheckBoxList"];
            if (parts[6] == "true")
            {
                CheckBoxList[id].Checked = true;
            }
            else if (parts[6] == "false")
            {
                CheckBoxList[id].Checked = false;
            }
            Session["DynamicReports_CheckBoxList"] = CheckBoxList;
        }
        else if (type == "DropDownList")
        {
            string listbox = parts[5];
            if (Session["DynamicReports_DropDownList"] != null) DropDownList = (Dictionary<string, ASPxDropDownEdit>)Session["DynamicReports_DropDownList"];
            //foreach (string i in DropDownList.Keys)
            //{
            //    if (DropDownList[i].ID == id)
            //    {
            ASPxListBox list = DropDownList[id].FindControl('|' + id + ",listbox") as ASPxListBox;
            list.UnselectAll();
            //DropDownList[id].Text = "";
            if (parts[6] == "SelectAll")
            {
                list.SelectAll();
                //DropDownList[id].Text = "All items are selected";
            }
            else if (parts[6] == "")
            {
                list.UnselectAll();
                //DropDownList[i].Text = "";
            }
            else
            {
                for (int j = 6; j < parts.Length; j++)
                {
                    int index = list.Items.IndexOfValue(parts[j]);
                    list.Items[index].Selected = true;
                    //DropDownList[i].Text += parts[j] + ",";
                }
                //    }
                //}
            }


        }
        else if (type == "IntBox")
        {
            if (parts[5] == "1")
            {
                if (Session["DynamicReports_IntBox1List"] != null) IntBox1List = (Dictionary<string, ASPxSpinEdit>)Session["DynamicReports_IntBox1List"];
                IntBox1List[id].Text = parts[6];
                Session["DynamicReports_IntBox1List"] = IntBox1List;
            }
            else
            {
                if (Session["DynamicReports_IntBox2List"] != null) IntBox2List = (Dictionary<string, ASPxSpinEdit>)Session["DynamicReports_IntBox2List"];
                IntBox2List[id].Text = parts[6];
                Session["DynamicReports_IntBox2List"] = IntBox2List;
            }
        }
        else if (type == "Calendar")
        {
            if (parts[5] == "1")
            {
                if (Session["DynamicReports_Calendar1List"] != null) Calendar1List = (Dictionary<string, ASPxDateEdit>)Session["DynamicReports_Calendar1List"];
                if (parts[6] == "NoDate")
                {
                    Calendar1List[id].Date = DateTime.MinValue;
                }
                else
                {
                  
                    Calendar1List[id].Date = DateTime.ParseExact(parts[6], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Session["DynamicReports_Calendar1List"] = Calendar1List;
                }
            }
            else
            {
                if (Session["DynamicReports_Calendar2List"] != null) Calendar2List = (Dictionary<string, ASPxDateEdit>)Session["DynamicReports_Calendar2List"];
                if (parts[6] == "NoDate")
                {
                    Calendar2List[id].Date = DateTime.MinValue;
                }
                else
                {
                    
                    Calendar2List[id].Date = DateTime.ParseExact(parts[6], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Session["DynamicReports_Calendar2List"] = Calendar2List;
                }
            }
        }

    }
    protected void CustomizePDF_CallBack(object sender, CallbackEventArgsBase e)
    {
       
        if (e.Parameter == "CustomizePDF")
        {
            SetInitialPDFHeaderFooterText();
            this.ASPxPopUpCostumizePDF.ShowOnPageLoad = true;
        }
        else if (e.Parameter == "SetInitialPDFValues")
        {
            SetInitialPDFHeaderFooterText();
        }
        else if (e.Parameter == "PDFHeaderFooterChanged")
        {
            //save the new custom pdf  (Incomplete)
        }
        else
        {
            string[] parts = e.Parameter.Split('|');
            SetCustomPDFHeaderFooterText(parts);
            this.ASPxPopUpCostumizePDF.ShowOnPageLoad = true;
        }
        SetPDFGrid();
    }
  
    #endregion

    #region PDF
    private void SetCustomPDFHeaderFooterText(string [] parts)
    {
        this.ASPxTextBoxCompanyNamePDF.Text = parts[1];
        //this.CheckBoxCompanyNamePDF.Checked = Convert.ToBoolean(parts[2]);
        this.ASPxTextBoxReportTitlePDF.Text = parts[3];
        //this.CheckBoxReportTitlePDF.Checked = Convert.ToBoolean(parts[4]);
        this.ASPxTextBoxDatePdf.Text = parts[5];
        //this.CheckBoxDatePDF.Checked = Convert.ToBoolean(parts[6]);
        this.ASPxTextBoxEmployeeNamePDF.Text = parts[7];
        this.ASPxTextBoxFromToDatePDF.Text = parts[8];
    }
    private void SetInitialPDFHeaderFooterText()
    {
        this.ASPxTextBoxCompanyNamePDF.Text = "ECG";
        if (Session["DynamicReports_ReportName"] !=null)
            this.ASPxTextBoxReportTitlePDF.Text = Session["DynamicReports_ReportName"].ToString();
        this.ASPxTextBoxDatePdf.Text = DateTime.Now.ToLongDateString();
        this.ASPxTextBoxEmployeeNamePDF.Text = Membership.GetUser(User.Identity.Name).ToString();
        this.ASPxTextBoxPageNumberPDF.Text = "Page Number";
        string from = "";
        string to = "";
        if (!this.ASPxDateFrom.Date.Year.ToString().Equals("1") || !this.ASPxDateTo.Date.Year.ToString().Equals("1"))
        {

            if (!this.ASPxDateFrom.Date.Year.ToString().Equals("1")) from = "From " + this.ASPxDateFrom.Date.ToShortDateString();
            if (!this.ASPxDateTo.Date.Year.ToString().Equals("1")) to = " To " + this.ASPxDateTo.Date.ToShortDateString();
        }
        this.ASPxTextBoxFromToDatePDF.Text = from + to;

        //this.CheckBoxCompanyNamePDF.Checked = true;
        //this.CheckBoxReportTitlePDF.Checked = true;
        //this.CheckBoxDatePDF.Checked = true;
    }
    private void SetPDFGrid()
    {
        //Adding columns to grid
        if (Session["DynamicReports_ReportGrid_ColumnList"] != null)
        {
            DataTable ColumnList = (DataTable)Session["DynamicReports_ReportGrid_ColumnList"];
            this.ASPxGridViewPDF.Columns.Clear();

            foreach (DataRow dr in ColumnList.Rows)
            {
                if (dr["ColumnName"].ToString().Equals("TEDate"))
                {
                    GridViewDataDateColumn Column = new GridViewDataDateColumn();
                    Column.Caption = dr["ColumnDisplayName"].ToString();
                    Column.FieldName = dr["ColumnDisplayName"].ToString();
                    Column.PropertiesDateEdit.DisplayFormatString = "dd/MM/yyyy";
                    Column.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                    this.ASPxGridViewPDF.Columns.Add(Column);
                }
                else
                {
                    GridViewDataColumn Column = new GridViewDataColumn();
                    Column.Caption = dr["ColumnDisplayName"].ToString();
                    Column.FieldName = dr["ColumnDisplayName"].ToString();
                    this.ASPxGridViewPDF.Columns.Add(Column);
                }
            }
        }
        
        //Choosing only the first 5 rows
        if (Session["DynamicReports_ReportGrid"] != null)
        {
            DataTable fulltable = (DataTable)Session["DynamicReports_ReportGrid"];
            DataTable tableof5Rows = fulltable.Clone();
            int demorows = 5;
            if (fulltable.Rows.Count < 5) demorows = fulltable.Rows.Count;
            for (int i=0; i< demorows; i++)
            {
                tableof5Rows.ImportRow(fulltable.Rows[i]);
            }
            this.ASPxGridViewPDF.DataSource = tableof5Rows;
            this.ASPxGridViewPDF.DataBind();
        }
    }
    // int pageNumber = 0;
    protected void ExportToPDFClicked(object sender, EventArgs e)
    {
        //GridExport.ReportHeader = this.ComboBoxReports.SelectedItem.Text;
        //GridExport.PageHeader.Left = "ECG";
        //GridExport.PageHeader.Right = "OVTime";
        //GridExport.PageHeader.Center = this.ComboBoxReports.SelectedItem.Text;

        //GridExport.PageFooter.Right = DateTime.Now.ToShortTimeString();
        //GridExport.PageFooter.Left = "OmniVista Solutions";


        //this.GridExport.WritePdfToResponse("ovtime-" + Membership.GetUser(User.Identity.Name) + "-" + ReportNameTextBox.Text + ".pdf");
        double A4_Size = 8.3;
        int webDPI = 96;
        int margins = 50;

        int PdfDocWidth = Convert.ToInt32(A4_Size * webDPI) - 2 * margins;
        foreach (GridViewDataColumn c in this.ASPxGridViewReport.Columns)
        {
            c.ExportWidth = Convert.ToInt32(PdfDocWidth / this.ASPxGridViewReport.Columns.Count);

        }

        this.GridExport.Styles.Header.BorderColor = System.Drawing.ColorTranslator.FromHtml("#9F9F9F");
        this.GridExport.Styles.Header.BackColor = System.Drawing.ColorTranslator.FromHtml("#DCDCDC");
        this.GridExport.Styles.Header.BorderSize = 1;

        this.GridExport.Styles.Title.BackColor = System.Drawing.ColorTranslator.FromHtml("#ACACAC");
        this.GridExport.Styles.Title.BorderColor = System.Drawing.ColorTranslator.FromHtml("#ACACAC");
        this.GridExport.Styles.Title.BorderSize = 5;

        this.GridExport.Styles.AlternatingRowCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#ededeb");
        this.GridExport.Styles.AlternatingRowCell.BorderSize = 1;
        this.GridExport.Styles.AlternatingRowCell.BorderColor = System.Drawing.ColorTranslator.FromHtml("#CFCFCF");


        GridViewLink link = new GridViewLink(GridExport);


        //phf.Header.Content.Clear();
        //phf.Footer.Content.Clear();
        ////phf.Header.Content.Add(this.ComboBoxReports.SelectedItem.Text);
        //phf.Header.Content.AddRange(new string[] { "", this.ComboBoxReports.SelectedItem.Text, "" });

        //// phf.Header.LineAlignment = BrickAlignment.Center;
        //phf.Header.LineAlignment = BrickAlignment.Far;
        //phf.Header.Font = new System.Drawing.Font("Arial", 14);



        //phf.Footer.Content.Add(DateTime.Now.ToShortTimeString());
        //phf.Footer.LineAlignment = BrickAlignment.Far;
        // pageNumber = 0;
        link.CreateMarginalHeaderArea += new CreateAreaEventHandler(CreateCustomHeaderArea);
        //link.CreateMarginalFooterArea += new CreateAreaEventHandler(CreateCustomFooterArea);
        PageHeaderFooter phf = (PageHeaderFooter)link.PageHeaderFooter;
        phf.Footer.Content.Clear();
        //string from = "";
        //string to = "";
        //if (!this.ASPxDateFrom.Date.Year.ToString().Equals("1") || !this.ASPxDateTo.Date.Year.ToString().Equals("1"))
        //{

        //    if (!this.ASPxDateFrom.Date.Year.ToString().Equals("1")) from = "From " + this.ASPxDateFrom.Date.ToShortDateString();
        //    if (!this.ASPxDateTo.Date.Year.ToString().Equals("1")) to = " To " + this.ASPxDateTo.Date.ToShortDateString();
        //}

        //get footer order --> assign footer
        string footerorder = (string)this.FooterOrder.Get("FooterOrder");
        string[] footerparts = footerorder.Split('~');

        string[] footer = new string [3];
        int i = 0;
        foreach (string f in footerparts)
        {
            if (i > 3) break;//So that the index will not exeed 3
            if (f == "divfooter1") { footer[i] = this.ASPxTextBoxEmployeeNamePDF.Text; }
            else if (f == "divfooter2") { footer[i] = this.ASPxTextBoxFromToDatePDF.Text; }
            else if (f == "divfooter3") { footer[i] = "[Page # of Pages #]"; }
            i++;
        }
        phf.Footer.Content.AddRange(footer);
        link.PrintingSystemBase = new PrintingSystemBase();

        using (MemoryStream stream = new MemoryStream())
        {

            link.Margins.Left = margins;
            link.Margins.Right = margins;
            //link.Margins.Top = 24 + 14 + 4 + 14 + 3;
            //link.Margins.Bottom = 10 + 3;
            link.PaperKind = System.Drawing.Printing.PaperKind.A4;
            link.CreateDocument(false);
            ASPxButton btn = (ASPxButton)sender;
            if (btn.ID == "ExporttoExecBtn") link.PrintingSystemBase.ExportToXlsx(stream);
            else if (btn.ID == "ExporttoPDFBtn") link.PrintingSystemBase.ExportToPdf(stream);
            Response.Clear();
            Response.Buffer = false;

            if (btn.ID == "ExporttoPDFBtn") Response.AppendHeader("Content-Type", "application/pdf");
            else if (btn.ID == "ExporttoExecBtn") Response.AppendHeader("Content-Type", "application/xlsx");
            Response.AppendHeader("Content-Transfer-Encoding", "binary");

            //Response.AppendHeader("Content-Disposition", "attachment; test.pdf");
            string reportName = "";
            if (Session["DynamicReports_ReportName"] != null) reportName = Session["DynamicReports_ReportName"].ToString();
                if (btn.ID == "ExporttoPDFBtn") Response.AppendHeader("Content-Disposition", "attachment; filename=" + Membership.GetUser(User.Identity.Name) + "-" + reportName + " .pdf");
            else if (btn.ID == "ExporttoExecBtn") Response.AppendHeader("Content-Disposition", "attachment; filename=" + Membership.GetUser(User.Identity.Name) + "-" + reportName + " .xlsx");
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }
    }
    // this.GridExport.Landscape = true;
    protected void CreateCustomFooterArea(object sender, CreateAreaEventArgs e)
    {
        float width = e.Graph.ClientPageSize.Width;
        e.Graph.Font = new Font("Arial", 10);

        //e.Graph.PrintingSystem.PageSettings.BottomMarginF = e.Graph.Font.Height + 150 ;
        //e.Graph.PrintingSystem.PageSettings.Assign()
        e.Graph.StringFormat.Value.Alignment = StringAlignment.Near;
        e.Graph.StringFormat.Value.LineAlignment = StringAlignment.Near;
        e.Graph.DrawString(Membership.GetUser(User.Identity.Name).ToString(), new RectangleF(0, 0, width / 3, e.Graph.Font.Height));

        e.Graph.StringFormat.Value.Alignment = StringAlignment.Center;
        e.Graph.StringFormat.Value.LineAlignment = StringAlignment.Center;
        if (!this.ASPxDateFrom.Date.Year.ToString().Equals("1") || !this.ASPxDateTo.Date.Year.ToString().Equals("1"))
        {
            string from = "";
            string to = "";
            if (!this.ASPxDateFrom.Date.Year.ToString().Equals("1")) from = "From " + this.ASPxDateFrom.Date.ToShortDateString();
            if (!this.ASPxDateTo.Date.Year.ToString().Equals("1")) to = " To " + this.ASPxDateTo.Date.ToShortDateString();
            e.Graph.DrawString(from + to, new RectangleF(width / 3, 0, width / 3, e.Graph.Font.Height));
        }

        float datexposition = width / 3;
        float pagexpositon = (width / 3) * 2;


        e.Graph.StringFormat.Value.Alignment = StringAlignment.Far;
        e.Graph.StringFormat.Value.LineAlignment = StringAlignment.Far;
        e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, "Page {0} of {1}", Color.Black, new RectangleF(100, 0, width / 3, e.Graph.Font.Height), BorderSide.None);

    }
    protected void CreateCustomHeaderArea(object sender, CreateAreaEventArgs e)
    {
        // e.Graph.BackColor = Color.FromArgb(100, 0, 27, 47);
        //if (pageNumber == 0)
        //{
        float width = e.Graph.ClientPageSize.Width;
        System.Drawing.Image logo = System.Drawing.Image.FromFile(Page.MapPath("~/Content/Images/OVTimeLogo2.png"));
        // leftHandImage.si
        //logo = (System.Drawing.Image)(new Bitmap(logo, new Size(10,10)));
        logo = ResizeImage(logo, 10, 10);
        //e.Graph.DrawImage(logo, new RectangleF(0, 0, width / 3, 10));
        // e.Graph.PrintingSystem.PageSettings.TopMargin = (24 + 14 + 4 + 14 + 3)*3;
        e.Graph.BorderWidth = 0;
        e.Graph.StringFormat.Value.Alignment = StringAlignment.Center;
        e.Graph.StringFormat.Value.LineAlignment = StringAlignment.Center;

        e.Graph.Font = new Font("Arial", 20);
        e.Graph.ForeColor = System.Drawing.ColorTranslator.FromHtml("#3d393c");
        e.Graph.DrawString(this.ASPxTextBoxCompanyNamePDF.Text, new RectangleF(0, 0, width, e.Graph.Font.Height));

        float prevheight1 = e.Graph.Font.Height;
        e.Graph.Font = new Font("Arial", 10);
        e.Graph.ForeColor = System.Drawing.ColorTranslator.FromHtml("#333333");
        e.Graph.DrawString(this.ASPxTextBoxReportTitlePDF.Text, new RectangleF(0, prevheight1 + 3, width, e.Graph.Font.Height));

        float prevheight2 = e.Graph.Font.Height + prevheight1 + 3;
        e.Graph.Font = new Font("Arial", 10);
        e.Graph.ForeColor = System.Drawing.ColorTranslator.FromHtml("#3d393c");
        e.Graph.DrawString(this.ASPxTextBoxDatePdf.Text, new RectangleF(0, prevheight2, width, e.Graph.Font.Height));


        //    pageNumber++;
        //}

    }
    public static Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
    {
        var destRect = new Rectangle(0, 0, width, height);
        var destImage = new Bitmap(width, height);

        destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

        using (var graphics = Graphics.FromImage(destImage))
        {
            graphics.CompositingMode = CompositingMode.SourceCopy;
            graphics.CompositingQuality = CompositingQuality.HighQuality;
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

            using (var wrapMode = new ImageAttributes())
            {
                wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
            }
        }

        return destImage;
    }
    protected void GridExport_RenderBrick(object sender, ASPxGridViewExportRenderingEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
            return;
        //if (e.RowType == GridViewRowType.Header)
        //{
        //    e.BrickStyle.BackColor = Color.Yellow;
        //}
        if ((e.Column as GridViewDataColumn).FieldName == "Cost")
        {
            // e.BrickStyle.BackColor = Color.Yellow;
        }
        //if (e.RowType == GridViewRowType.Header) 
        //{
        //    e.BrickStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#DCDCDC");
        //    e.BrickStyle.BorderColor = System.Drawing.ColorTranslator.FromHtml("#9F9F9F");
        //    e.BrickStyle.BorderWidth = 1;
        //    e.BrickStyle.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;

        //}
        //if (e.RowType == GridViewRowType.Title)
        //{
        //    e.BrickStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#ACACAC");
        //    e.BrickStyle.BorderWidth = 5;
        //    e.BrickStyle.BorderColor = System.Drawing.ColorTranslator.FromHtml("#ACACAC");

        //}
        //if(e.Ro)
    }
    #endregion
    #region Modify Report
    private void ModifyReport()
    {
        int reportID = (int)Session["DynamicReports_ReportID"];
        //string sql = (string)Session["DynamicReports_sql"];

        DataTable RName = DataAccess.GetDataTableBySqlSyntax("SELECT Name, Description FROM OVReports WHERE Id=" + reportID, "");
        string reportName = RName.Rows[0]["Name"].ToString();

        this.ReportNameTextBox.Text = reportName;
        this.ReportDescriptionTextBox.Text = RName.Rows[0]["Description"].ToString();

        UnSelectAllTreeNodes(ref ASPxtreeViewEmployee);
        UnSelectAllTreeNodes(ref ASPxtreeProject);
        UnSelectAllTreeNodes(ref ASPxTreeClient);
        UnSelectAllTreeNodes(ref ASPxTreeTimeSheet);

        ASPxtreeViewEmployee.ExpandAll();
        DataTable EmployeeSelectedData = DataAccess.GetDataTableBySqlSyntax("SELECT DISTINCT ColumnName FROM OVReportColumns WHERE ReportId=" + reportID + " AND (TableName='Employee' OR TableName='UDF' OR TableName='OMV_EmployeeSalary') UNION SELECT DISTINCT FilterName as 'ColumnName' FROM OVReportFilters WHERE ReportId=" + reportID + " AND (TableName='Employee' OR TableName='UDF' OR TableName='OMV_EmployeeSalary')", "");
        SelectChoosenTreeItems(ref ASPxtreeViewEmployee, EmployeeSelectedData);
        ASPxtreeProject.ExpandAll();
        DataTable ProjectSelectedData = DataAccess.GetDataTableBySqlSyntax("SELECT DISTINCT ColumnName FROM OVReportColumns WHERE ReportId=" + reportID + " AND (TableName='Project' OR TableName='ProjectDetails') UNION SELECT DISTINCT FilterName as 'ColumnName' FROM OVReportFilters WHERE ReportId=" + reportID + " AND (TableName='Project' OR TableName='ProjectDetails')", "");
        SelectChoosenTreeItems(ref ASPxtreeProject, ProjectSelectedData);
        ASPxTreeClient.ExpandAll();
        DataTable ClientSelectedData = DataAccess.GetDataTableBySqlSyntax("SELECT DISTINCT ColumnName FROM OVReportColumns WHERE ReportId=" + reportID + " AND TableName='Client' UNION SELECT DISTINCT FilterName as 'ColumnName' FROM OVReportFilters WHERE ReportId=" + reportID + " AND TableName='Client'", "");
        SelectChoosenTreeItems(ref ASPxTreeClient, ClientSelectedData);
        ASPxTreeTimeSheet.ExpandAll();
        DataTable TimeSheetSelectedData = DataAccess.GetDataTableBySqlSyntax("SELECT DISTINCT ColumnName FROM OVReportColumns WHERE ReportId=" + reportID + " AND TableName='TimeEntry' UNION SELECT DISTINCT FilterName as 'ColumnName' FROM OVReportFilters WHERE ReportId=" + reportID + " AND TableName='TimeEntry'", "");
        SelectChoosenTreeItems(ref ASPxTreeTimeSheet, TimeSheetSelectedData);

        this.NewReportPopUpControl.HeaderText = "Modify Report";
        this.NewReportPopUpControl.ShowOnPageLoad = true;
        AssignTagstoComboBox(reportID.ToString());

    }
    private void SelectChoosenTreeItems(ref ASPxTreeView tree, DataTable SelectedData)
    {
        foreach (DataRow row in SelectedData.Rows)
        {
            string selectedColumn = row[0].ToString();
            for (int i = 0; i < tree.Nodes[0].Nodes.Count; i++)
            {
                //if (tree.Nodes[0].Nodes[i].Name.Contains(selectedColumn))
                if (tree.Nodes[0].Nodes[i].Name.Split(',')[1].Equals(selectedColumn) || tree.Nodes[0].Nodes[i].Name.Split(',')[4].Equals(selectedColumn))
                {
                    tree.Nodes[0].Nodes[i].Checked = true;
                    break;
                }
            }
        }
    }
    private void UnSelectAllTreeNodes(ref ASPxTreeView tree)
    {
        foreach (TreeViewNode node in tree.Nodes)
        {
            node.Checked = false;
        }
    }
    #endregion

    #region Report Tags
    private void AssignTagstoComboBox(string reportID = "New Report")
    {
        Session["DynamicReports_OVReportTags"] = this.ASPxComboBoxReportTag.DataSource = DataAccess.GetDataTableBySqlSyntax("SELECT Id, Tag From OVReportTags", "");
        this.ASPxComboBoxReportTag.DataBind();
        if (!reportID.Equals("New Report"))
        {
            DataTable t = DataAccess.GetDataTableBySqlSyntax("SELECT DISTINCT TagId FROM OVReports WHERE Id=" + reportID, "");
            
            this.ASPxComboBoxReportTag.SelectedItem = this.ASPxComboBoxReportTag.Items.FindByValue(t.Rows[0]["TagId"].ToString());
        }
    }
    #endregion


    #region Display Filters
    private void GetSelectedTreeItems(ASPxTreeView tree, ref List<string> selectedchildrenName, ref List<string> slectedchildrenText, ref string selectedparentName, ref string selectedparentText)
    {
        selectedparentName = "";
        selectedparentText = "";
        selectedchildrenName = new List<string>();
        slectedchildrenText = new List<string>();
        bool ParentIsSelected = false;
        for (int i = 0; i < tree.Nodes[0].Nodes.Count; i++)
        {
            if (tree.Nodes[0].Nodes[i].Checked)
            {
                selectedchildrenName.Add(tree.Nodes[0].Nodes[i].Name);
                slectedchildrenText.Add(tree.Nodes[0].Nodes[i].Text);
                ParentIsSelected = true;
            }
        }
        if (ParentIsSelected)
        {
            selectedparentName = tree.Nodes[0].Name;
            selectedparentText = tree.Nodes[0].Text;
        }
    }
    private void AssignFilters(bool isModify)
    {
        //Getting the selected items from the Trees
        List<string> SelectedEmployeeChildrenName = new List<string>();
        List<string> SelectedEmployeeChildrenText = new List<string>();
        List<string> SelectedProjectChildrenName = new List<string>();
        List<string> SelectedProjectChildrenText = new List<string>();
        List<string> SelectedClientChildrenName = new List<string>();
        List<string> SelectedClientChildrenText = new List<string>();

        List<string> SelectedTimeSheetChildrenName = new List<string>();
        List<string> SelectedTimeSheetChildrenText = new List<string>();
        string SelectedTimeSheetParentName = "";
        string SelectedTimeSheetParentText = "";

        string SelectedEmployeeParentName ="";
        string SelectedEmployeeParentText = "";
        string SelectedProjectParentName = "";
        string SelectedProjectParentText = "";
        string SelectedClientParentName = "";
        string SelectedClientParentText = "";

        GetSelectedTreeItems(ASPxtreeViewEmployee, ref SelectedEmployeeChildrenName,ref SelectedEmployeeChildrenText, ref SelectedEmployeeParentName, ref SelectedEmployeeParentText);
        GetSelectedTreeItems(ASPxtreeProject,ref SelectedProjectChildrenName,ref SelectedProjectChildrenText, ref SelectedProjectParentName, ref SelectedProjectParentText);
        GetSelectedTreeItems(ASPxTreeClient,ref SelectedClientChildrenName,ref SelectedClientChildrenText, ref SelectedClientParentName,ref SelectedClientParentText);
        GetSelectedTreeItems(ASPxTreeTimeSheet, ref SelectedTimeSheetChildrenName, ref SelectedTimeSheetChildrenText, ref SelectedTimeSheetParentName, ref SelectedTimeSheetParentText);

        //Clear all filter choosen options
        ClearFilters();
        //Generate Filters

        if (isModify)
        {
            //loop based on Column Index SELECT FROM Columns and Filters and order by ColumnIndex (and if item selected from tree)
            int reportID = (int)Session["DynamicReports_ReportID"];
            DataTable orderedColumns = DataAccess.GetDataTableBySqlSyntax("SELECT DISTINCT ColumnName, TableName, ColumnIndex FROM OVReportColumns  WHERE ReportId=" + reportID + " ORDER BY ColumnIndex ASC", "");
            foreach (DataRow row in orderedColumns.Rows)
            {
                if (row["TableName"].ToString().Equals("Employee") || row["TableName"].ToString().Equals("UDF") || row["TableName"].ToString().Equals("OMV_EmployeeSalary"))
                {
                    for (int i = 0; i < SelectedEmployeeChildrenName.Count; i++)
                    {
                        if (SelectedEmployeeChildrenName[i].Contains(row["ColumnName"].ToString()))
                        {
                            GenerateFilter(SelectedEmployeeChildrenName[i], SelectedEmployeeChildrenText[i], isModify);
                            SelectedEmployeeChildrenName.RemoveAt(i);
                            SelectedEmployeeChildrenText.RemoveAt(i);
                            break;
                        }
                    }
                }
                else if (row["TableName"].ToString().Equals("Project") || row["TableName"].ToString().Equals("ProjectDetails"))
                {
                    for (int i = 0; i < SelectedProjectChildrenName.Count; i++)
                    {
                        if (SelectedProjectChildrenName[i].Contains(row["ColumnName"].ToString()))
                        {
                            GenerateFilter(SelectedProjectChildrenName[i], SelectedProjectChildrenText[i], isModify);
                            SelectedProjectChildrenName.RemoveAt(i);
                            SelectedProjectChildrenText.RemoveAt(i);
                            break;
                        }
                    }
                }
                else if (row["TableName"].ToString().Equals("Client"))
                {
                    for (int i = 0; i < SelectedClientChildrenName.Count; i++)
                    {
                        if (SelectedClientChildrenName[i].Contains(row["ColumnName"].ToString()))
                        {
                            GenerateFilter(SelectedClientChildrenName[i], SelectedClientChildrenText[i], isModify);
                            SelectedClientChildrenName.RemoveAt(i);
                            SelectedClientChildrenText.RemoveAt(i);
                            break;
                        }
                    }
                }
                else if (row["TableName"].ToString().Equals("TimeEntry"))
                {
                    for (int i = 0; i < SelectedTimeSheetChildrenName.Count; i++)
                    {
                        if (SelectedTimeSheetChildrenName[i].Contains(row["ColumnName"].ToString()))
                        {
                            GenerateFilter(SelectedTimeSheetChildrenName[i], SelectedTimeSheetChildrenText[i], isModify);
                            SelectedTimeSheetChildrenName.RemoveAt(i);
                            SelectedTimeSheetChildrenText.RemoveAt(i);
                            break;
                        }
                    }
                }


            }
            //loop on the new chossen items from trees

        }
        for (int i=0; i<SelectedEmployeeChildrenName.Count; i++)
        {
            GenerateFilter(SelectedEmployeeChildrenName[i], SelectedEmployeeChildrenText[i], isModify);
        }
        for (int i = 0; i < SelectedProjectChildrenName.Count; i++)
        {
            GenerateFilter(SelectedProjectChildrenName[i], SelectedProjectChildrenText[i], isModify);
        }
        for (int i = 0; i < SelectedClientChildrenName.Count; i++)
        {
            GenerateFilter(SelectedClientChildrenName[i], SelectedClientChildrenText[i], isModify);
        }
       for (int i=0; i<SelectedTimeSheetChildrenName.Count; i++)
        {
            GenerateFilter(SelectedTimeSheetChildrenName[i], SelectedTimeSheetChildrenText[i], isModify);
        }

        


    }
    private void ClearFilters()
    {
        if (Session["DynamicReports_DropDownList"] != null)
        {
            DropDownList = (Dictionary<string, ASPxDropDownEdit>)Session["DynamicReports_DropDownList"];
            DropDownList.Clear();
            Session["DynamicReports_DropDownList"] = DropDownList;
        }
        if (Session["DynamicReports_IntBox1List"] != null) {
            IntBox1List = (Dictionary<string, ASPxSpinEdit>)Session["DynamicReports_IntBox1List"];
            IntBox1List.Clear();
            Session["DynamicReports_IntBox1List"] = IntBox1List;
        }
        if (Session["DynamicReports_IntBox2List"] != null) {
            IntBox2List = (Dictionary<string, ASPxSpinEdit>)Session["DynamicReports_IntBox2List"];
            IntBox2List.Clear();
            Session["DynamicReports_IntBox2List"] = IntBox2List;
        }
        if (Session["DynamicReports_Calendar1List"] != null) {
            Calendar1List = (Dictionary<string, ASPxDateEdit > )Session["DynamicReports_Calendar1List"];
            Calendar1List.Clear();
            Session["DynamicReports_Calendar1List"] = Calendar1List;
        }
        if (Session["DynamicReports_Calendar2List"] != null) {
            Calendar2List = (Dictionary<string, ASPxDateEdit>)Session["DynamicReports_Calendar2List"];
            Calendar2List.Clear();
            Session["DynamicReports_Calendar2List"] = Calendar2List;
        }
        if (Session["DynamicReports_ColumnNameList"] != null) {
            ColumnNameList = (Dictionary<String, ASPxTextBox>)Session["DynamicReports_ColumnNameList"];
            ColumnNameList.Clear();
            Session["DynamicReports_ColumnNameList"] = ColumnNameList;
        }
        if (Session["DynamicReports_CheckBoxList"] != null)
        {
            CheckBoxList = (Dictionary<string, ASPxCheckBox>)Session["DynamicReports_CheckBoxList"];
            CheckBoxList.Clear();
            Session["DynamicReports_CheckBoxList"] = CheckBoxList;
        }
        divFilters.Controls.Clear();
    }
    private void GenerateFilter(string parm, string columnname, bool isModify)
    {
        string [] parts = parm.Split(',');
        if (parts[2] != "null")    //null = Dont add a filter option
        {
            System.Web.UI.HtmlControls.HtmlGenericControl row = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
            row.Attributes["class"] = "row slide";
            row.Style.Add(HtmlTextWriterStyle.PaddingLeft, "13%");
            row.Style.Add(HtmlTextWriterStyle.MarginRight, "0");
            row.ID = "|row," + parm;
            row.Attributes["onmouseover"] = "this.style.backgroundColor='#e6e6e6'";
            row.Attributes["onmouseout"] = "this.style.backgroundColor=''";
            

            if (parts[2] == "DropDownList")
        {
            //Add a drop down list (ASPxDropDownEdit --> ASPxListBox)
            ASPxDropDownEdit dropdown = new ASPxDropDownEdit();
            dropdown.CssClass = "TextBox";
            dropdown.ID = parm;
            dropdown.AnimationType = AnimationType.Fade;
            dropdown.DropDownWindowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#EDEDED");
            dropdown.DropDownWindowTemplate = new MyList(parm);
            dropdown.ClientInstanceName = parm;
                dropdown.DropDownButton.Image.Url = "../Content/Images/iconmonstr-arrow-65-32.png";
                dropdown.DropDownButton.Image.Width = 16;
                dropdown.DropDownButton.Image.Height= 16;
                dropdown.Text = "All Selected";
            
                if (isModify)
                {
                    string tablename = parts[0];
                    string columnName = parts[1];
                    string type = parts[2];
                    int reportID = (int)Session["DynamicReports_ReportID"];
                    DataTable filterValues = DataAccess.GetDataTableBySqlSyntax("SELECT FilterValue FROM OVReportFilters WHERE FilterName='" + columnName + "' AND TableName='" + tablename + "' AND ReportId=" + reportID + " AND FilterTypeId = (SELECT Id FROM OVReportFilterTypes WHERE Type='" + type + "')", "");
                    if (filterValues.Rows.Count > 0)
                    {
                        //Check the listbox based on filterValues 
                        string id = dropdown.ID;
                        ASPxListBox list = dropdown.FindControl("|" + id + ",listbox") as ASPxListBox;
                        list.UnselectAll();
                        dropdown.Text = "";
                        foreach (DataRow filterRow in filterValues.Rows)
                        {
                               list.Items.FindByText(filterRow[0].ToString()).Selected = true;
                            dropdown.Text += filterRow[0].ToString() + ",";
                        }
                        dropdown.Text = dropdown.Text.Remove(dropdown.Text.Length - 1);
                    }
                }

                LiteralControl label1 = new LiteralControl("<label>" +columnname + "</label>");
                System.Web.UI.HtmlControls.HtmlGenericControl col10_1 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                col10_1.Attributes["class"] = "col-10";
                //LiteralControl col10_1 = new LiteralControl("<div class='col-10'>");
                col10_1.Controls.Add(label1);
                row.Controls.Add(col10_1);

                System.Web.UI.HtmlControls.HtmlGenericControl col25_1 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                col25_1.Attributes["class"] = "col-25";
                col25_1.ID = "col," + parm;
                //LiteralControl col25_1 = new LiteralControl("<div class='col-25'>");
                col25_1.Controls.Add(dropdown);
                row.Controls.Add(col25_1);

                System.Web.UI.HtmlControls.HtmlGenericControl col10_2 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                col10_2.Attributes["class"] = "col-10";
                //LiteralControl col10_2 = new LiteralControl("<div class='col-10'>");
                row.Controls.Add(col10_2);

                if (Session["DynamicReports_DropDownList"] != null) DropDownList = (Dictionary<string, ASPxDropDownEdit>)Session["DynamicReports_DropDownList"];
                DropDownList.Add(parm ,dropdown);
                Session["DynamicReports_DropDownList"] = DropDownList;

                

        }
        else if (parts[2] == "IntBox")
        {
                //Add 2 text box that allows only numbers
                ASPxSpinEdit value1 = new ASPxSpinEdit();
                ASPxSpinEdit value2 = new ASPxSpinEdit();
                value1.ID = "|" + parm + ",1";
                value2.ID ="|" + parm + ",2";
                value1.ClientInstanceName = parm + ",1";
                value2.ClientInstanceName = parm + ",2";
                value1.MaxLength = 11;
                value2.MaxLength = 11;
                value1.CssClass = "TextBox";
                value2.CssClass = "TextBox";
                value1.SpinButtons.ShowIncrementButtons = false;
                value2.SpinButtons.ShowIncrementButtons = false;
                value1.SpinButtons.ShowLargeIncrementButtons = false;
                value2.SpinButtons.ShowLargeIncrementButtons = false;
                value1.ClientSideEvents.ValueChanged = "IntBoxValueChanged";
                value2.ClientSideEvents.ValueChanged = "IntBoxValueChanged";
                value1.NullText = "0";
                value2.NullText = "Max Value";

                if (isModify)
                {
                    string tablename = parts[0];
                    string columnName = parts[1];
                    string type = parts[2];
                    int reportID = (int)Session["DynamicReports_ReportID"];
                    DataTable filterValues = DataAccess.GetDataTableBySqlSyntax("SELECT FilterValue, FilterOperation FROM OVReportFilters WHERE FilterName='" + columnName + "' AND TableName='" + tablename + "' AND ReportId=" + reportID + " AND FilterTypeId = (SELECT Id FROM OVReportFilterTypes WHERE Type='" + type + "')", "");
                    if (filterValues.Rows.Count > 0)
                    {
                        foreach (DataRow filterRow in filterValues.Rows)
                        {
                            if (filterRow[1].ToString().Equals("<="))//To
                            {
                                value2.Text = filterRow[0].ToString();
                            }
                            else if (filterRow[1].ToString().Equals(">="))//From
                            {
                                value1.Text = filterRow[0].ToString();
                            }
                        }
                    }
                }


                        System.Web.UI.HtmlControls.HtmlGenericControl col10_1 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                col10_1.Attributes["class"] = "col-10";
                LiteralControl lable1 = new LiteralControl("<label>" + columnname + " From</label>");
                col10_1.Controls.Add(lable1);
                row.Controls.Add(col10_1);

                System.Web.UI.HtmlControls.HtmlGenericControl col10_2 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                col10_2.Attributes["class"] = "col-10";
                col10_2.ID = "col1," + parm;
                col10_2.Controls.Add(value1);
                row.Controls.Add(col10_2);

                System.Web.UI.HtmlControls.HtmlGenericControl col2point_break = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                col2point_break.Attributes["class"] = "col-2point5";
                row.Controls.Add(col2point_break);

                System.Web.UI.HtmlControls.HtmlGenericControl col10_3 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                col10_3.Attributes["class"] = "col-2point5";
                LiteralControl lable2 = new LiteralControl("<label>To</label>");
                col10_3.Controls.Add(lable2);
                row.Controls.Add(col10_3);

                System.Web.UI.HtmlControls.HtmlGenericControl col10_4 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                col10_4.Attributes["class"] = "col-10";
                col10_4.ID = "col2," + parm;
                col10_4.Controls.Add(value2);
                row.Controls.Add(col10_4);

                System.Web.UI.HtmlControls.HtmlGenericControl col10_break2 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                col10_break2.Attributes["class"] = "col-10";
                row.Controls.Add(col10_break2);

                if (Session["DynamicReports_IntBox1List"] != null) IntBox1List = (Dictionary<string, ASPxSpinEdit>)Session["DynamicReports_IntBox1List"];
                if (Session["DynamicReports_IntBox2List"] != null) IntBox2List = (Dictionary<string, ASPxSpinEdit>)Session["DynamicReports_IntBox2List"];
                IntBox1List.Add(parm, value1);
                IntBox2List.Add(parm, value2);
                Session["DynamicReports_IntBox1List"] = IntBox1List;
                Session["DynamicReports_IntBox2List"] = IntBox2List;

            }
        else if (parts[2] == "Calendar")
        {
                //Add 2 ASPxDateEdit
                ASPxDateEdit dt1 = new ASPxDateEdit();
                ASPxDateEdit dt2 = new ASPxDateEdit();
                dt1.ID = "|" + parm + ",1";
                dt2.ID = "|" + parm + ",2";
                dt1.ClientInstanceName = parm + ",1";
                dt2.ClientInstanceName = parm + ",1";
                dt1.Style.Add(HtmlTextWriterStyle.Width, "100%");
                dt2.Style.Add(HtmlTextWriterStyle.Width, "100%");
                dt1.CssClass = "DateEdit";
                dt2.CssClass = "DateEdit";
                //dt1.CalendarDayCellPrepared += new EventHandler<CalendarDayCellPreparedEventArgs>(ASPXDateEdit_CustomCell);
               // dt2.CalendarDayCellPrepared += new EventHandler<CalendarDayCellPreparedEventArgs>(ASPXDateEdit_CustomCell);
                dt1.EditFormatString = "dd/MM/yyyy";
                dt2.EditFormatString = "dd/MM/yyyy";
                dt1.DisplayFormatString = "dd/MM/yyyy";
                dt2.DisplayFormatString = "dd/MM/yyyy";
                dt1.ClientSideEvents.DateChanged = "DateEditDateChanged";
                dt2.ClientSideEvents.DateChanged = "DateEditDateChanged";
                dt1.NullText = "Any Date";
                dt2.NullText = "Any Date";
                dt1.DropDownButton.Image.Url = "../Content/Images/iconmonstr-arrow-65-32.png";
                dt1.DropDownButton.Image.Width = 16;
                dt1.DropDownButton.Image.Height = 16;
                dt2.DropDownButton.Image.Url = "../Content/Images/iconmonstr-arrow-65-32.png";
                dt2.DropDownButton.Image.Width = 16;
                dt2.DropDownButton.Image.Height = 16;
                if (isModify)
                {
                    string tablename = parts[0];
                    string columnName = parts[1];
                    string type = parts[2];
                    int reportID = (int)Session["DynamicReports_ReportID"];
                    DataTable filterValues = DataAccess.GetDataTableBySqlSyntax("SELECT FilterValue, FilterOperation FROM OVReportFilters WHERE FilterName='" + columnName + "' AND TableName='" + tablename + "' AND ReportId=" + reportID + " AND FilterTypeId = (SELECT Id FROM OVReportFilterTypes WHERE Type='" + type + "')", "");
                    if (filterValues.Rows.Count > 0)
                    {
                        foreach (DataRow filterRow in filterValues.Rows)
                        {
                            if (filterRow[1].ToString().Equals("<="))//To
                            {
                                DateTime date; /*DateTime.ParseExact(filterRow[0].ToString(), "MM/dd/yyyy HH:mm:ss tt", null);*/
                                DateTime.TryParse(filterRow[0].ToString(), out date);
                                dt2.Date = date;
                               
                            }
                            else if (filterRow[1].ToString().Equals(">="))//From
                            {
                                DateTime date; /*DateTime.ParseExact(filterRow[0].ToString(), "MM/dd/yyyy HH:mm:ss tt", null);*/
                                DateTime.TryParse(filterRow[0].ToString(), out date);
                                dt1.Date = date;
                            }
                        }
                    }
                }

                //text box for column name
                System.Web.UI.HtmlControls.HtmlGenericControl col10_1 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                col10_1.Attributes["class"] = "col-10";
                LiteralControl lable1 = new LiteralControl("<label>" + columnname + " From</label>");
                col10_1.Controls.Add(lable1);
                row.Controls.Add(col10_1);

                System.Web.UI.HtmlControls.HtmlGenericControl col10_2 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                col10_2.Attributes["class"] = "col-10";
                col10_2.ID = "col1," + parm;
                col10_2.Controls.Add(dt1);
                row.Controls.Add(col10_2);

                System.Web.UI.HtmlControls.HtmlGenericControl col2point_break = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                col2point_break.Attributes["class"] = "col-2point5";
                row.Controls.Add(col2point_break);

                System.Web.UI.HtmlControls.HtmlGenericControl col10_3 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                col10_3.Attributes["class"] = "col-2point5";
                LiteralControl lable2 = new LiteralControl("<label>To</label>");
                col10_3.Controls.Add(lable2);
                row.Controls.Add(col10_3);

                System.Web.UI.HtmlControls.HtmlGenericControl col10_4 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                col10_4.Attributes["class"] = "col-10";
                col10_4.ID = "col2," + parm;
                col10_4.Controls.Add(dt2);
                row.Controls.Add(col10_4);

                System.Web.UI.HtmlControls.HtmlGenericControl col10_break2 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                col10_break2.Attributes["class"] = "col-10";
                row.Controls.Add(col10_break2);

                if (Session["DynamicReports_Calendar1List"] != null) Calendar1List = (Dictionary<string, ASPxDateEdit>)Session["DynamicReports_Calendar1List"];
                if (Session["DynamicReports_Calendar2List"] != null) Calendar2List = (Dictionary<string, ASPxDateEdit>)Session["DynamicReports_Calendar2List"];
                Calendar1List.Add(parm ,dt1);
                Calendar2List.Add(parm, dt2);
                Session["DynamicReports_Calendar1List"] = Calendar1List;
                Session["DynamicReports_Calendar2List"] = Calendar2List;

            }
            else if (parts[2] == "Date")
            {
                LiteralControl label2 = new LiteralControl("<label>" + columnname + "</label>");
                System.Web.UI.HtmlControls.HtmlGenericControl col10_1 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                col10_1.Attributes["class"] = "col-10";
                col10_1.Controls.Add(label2);
                row.Controls.Add(col10_1);

                LiteralControl label1 = new LiteralControl("<label>" + columnname + " can be filtered after creating the report</label>");
                System.Web.UI.HtmlControls.HtmlGenericControl col25_1 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                col25_1.Attributes["class"] = "col-25";
                col25_1.Controls.Add(label1);
                row.Controls.Add(col25_1);

                System.Web.UI.HtmlControls.HtmlGenericControl col10_2 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                col10_2.Attributes["class"] = "col-10";
                row.Controls.Add(col10_2);
            }
            //Add text box for column name
            System.Web.UI.HtmlControls.HtmlGenericControl col10_5 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
            col10_5.Attributes["class"] = "col-10";
            LiteralControl label3 = new LiteralControl("<label>" + columnname + " Column Name</label>");
            col10_5.Controls.Add(label3);
            row.Controls.Add(col10_5);

            ASPxTextBox textbox = new ASPxTextBox();
            textbox.Text = columnname;
            textbox.Value = columnname;
            textbox.ID = "|" + parm + ",textbox";
            textbox.CssClass = "TextBox";
            textbox.Style.Add(HtmlTextWriterStyle.Width, "100%");
            textbox.MaxLength = 25;
            textbox.ValidationSettings.ValidationGroup = "popupValidationGroup";
            textbox.ValidationSettings.Display = Display.Dynamic;
            textbox.ValidationSettings.RequiredField.IsRequired = true;
            textbox.ClientSideEvents.ValueChanged = "TextBoxValueChanged";

            //Add checkbox to display column
            ASPxCheckBox checkbox = new ASPxCheckBox();
            checkbox.ID = "|" + parm + ",checkbox";
            checkbox.ClientSideEvents.ValueChanged = "CheckBoxValueChanged";
            checkbox.Value = true;
            checkbox.CheckedImage.Url = "../Content/Images/iconmonstr-checkbox-4-16.png";
            checkbox.CheckedImage.Height = 16;
            checkbox.CheckedImage.Width = 16;
            checkbox.UncheckedImage.Url = "../Content/Images/iconmonstr-square-4-16.png";
            checkbox.UncheckedImage.Height = 16;
            checkbox.UncheckedImage.Width = 16;
            checkbox.Style.Add(HtmlTextWriterStyle.PaddingTop, "10px");
            checkbox.Style.Add(HtmlTextWriterStyle.PaddingLeft, "45px");
            if (isModify)
            {
                string tablename = parts[0];
                string columnName = parts[1];
                string type = parts[2];
                int reportID = (int)Session["DynamicReports_ReportID"];
                DataTable filterValues = new DataTable();
                if (parts[4].Equals("null"))
                {
                     filterValues = DataAccess.GetDataTableBySqlSyntax("SELECT DisplayName FROM OVReportColumns WHERE ReportId=" + reportID + " AND ColumnName='" + columnName + "' AND TableName='" + tablename + "'", "");
                }
                else
                {
                    filterValues = DataAccess.GetDataTableBySqlSyntax("SELECT DisplayName FROM OVReportColumns WHERE ReportId=" + reportID + " AND ColumnName='" + parts[4] + "' AND TableName='" + tablename + "'", "");
                }
                if (filterValues.Rows.Count == 1)
                {
                    textbox.Text = filterValues.Rows[0][0].ToString();
                    textbox.Value = filterValues.Rows[0][0].ToString();
                }
                else if (filterValues.Rows.Count == 0)
                {
                    checkbox.Value = false;
                }
            }

            System.Web.UI.HtmlControls.HtmlGenericControl col25_2 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
            col25_2.Attributes["class"] = "col-25";
            col25_2.ID = "colColumnName" + parm;
            // LiteralControl col25_2 = new LiteralControl("<div class='col-25'>");
            col25_2.Controls.Add(textbox);
            row.Controls.Add(col25_2);

            System.Web.UI.HtmlControls.HtmlGenericControl col10_10 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
            col10_10.Attributes["class"] = "col-10";
            col10_10.Controls.Add(checkbox);
            row.Controls.Add(col10_10);


            divFilters.Controls.Add(row);
            if (Session["DynamicReports_ColumnNameList"] != null) ColumnNameList = (Dictionary<String, ASPxTextBox>)Session["DynamicReports_ColumnNameList"];
            ColumnNameList.Add(parm, textbox);
            Session["DynamicReports_ColumnNameList"] = ColumnNameList;

            if (Session["DynamicReports_CheckBoxList"] != null) CheckBoxList = (Dictionary<string, ASPxCheckBox>)Session["DynamicReports_CheckBoxList"];
            CheckBoxList.Add(parm, checkbox);
            Session["DynamicReports_CheckBoxList"] = CheckBoxList;

        }
    }
    public class MyList: ITemplate
    {
       private string tableName;
       private string columnName;
        private string type;
        private string parm;
       public MyList(string parm)
       {
            this.parm = parm;
            string[] parts = parm.Split(',');
            tableName = parts[0] ;
            columnName = parts[1];
            type = parts[2];
       }
        void ITemplate.InstantiateIn(Control container)
        {
            ASPxListBox listbox = new ASPxListBox();
            listbox.ID = "|"+ parm + ",listbox";
            listbox.SelectionMode = ListEditSelectionMode.CheckColumn;
            
            listbox.BorderBottom.BorderStyle = BorderStyle.Solid;
            listbox.BorderBottom.BorderWidth = 1;
            listbox.BorderBottom.BorderColor = System.Drawing.ColorTranslator.FromHtml("#DCDCDC");
            listbox.ClientSideEvents.SelectedIndexChanged = "updateText";
            listbox.ClientInstanceName = "|" + parm + ",listbox";
            listbox.CssClass = "listboxstyle";

            String sql = "SELECT DISTINCT " + columnName + " FROM " + tableName + " WHERE " + columnName + " IS NOT NULL AND " + columnName + " !=''";
            DataTable t = DataAccess.GetDataTableBySqlSyntax(sql, "");

            //listbox.Items.Add("Select All", "SelectAll");//Text,Value
            listbox.Items.Add(new ListEditItem("Select All", "SelectAll"));
            listbox.ClientSideEvents.Init = "function(s, e) { $('[class*=dxeListBoxItemRow]').eq(1).css({ 'font-weight': 'bold' }); }";//Not working
           
            foreach (DataRow row in t.Rows)
            {
                if (row[0].ToString().Contains(","))
                {
                    string value = row[0].ToString().Replace(',', '|');
                    listbox.Items.Add(new ListEditItem(row[0].ToString(), value));
                }
                else
                {
                    listbox.Items.Add(new ListEditItem(row[0].ToString(), row[0].ToString()));
                }
            }
           
        
            listbox.SelectAll();
            if(t.Rows.Count >= 6)
            {
                listbox.Height = 200;
            }
            else { listbox.Height = (t.Rows.Count +1) * 30; }
            container.Controls.Add(listbox);
            //Table table = new Table();

            //TableRow tr = new TableRow();
            //TableCell td = new TableCell();

            //ASPxButton btn = 
            //table.Controls.Add(tr);

        }

    }
    #endregion

    #region Generate SQL to save report
    private void SaveReport(string columnOrderArray, ref int ReportID, ref string ReportName, ref string ReportDescription) //returns reportID
    {
        //Getting the MaxID
        string query2 = "INSERT INTO [OVReports] OUTPUT Inserted.Id VALUES ('" + ReportNameTextBox.Text + "', " + this.ASPxComboBoxReportTag.SelectedItem.Value + ", '" + this.ReportDescriptionTextBox.Text + "','" + Membership.GetUser(User.Identity.Name) + "');";//r
        DataTable maxid = DataAccess.GetDataTableBySqlSyntax(query2, "");
        //DataTable maxid = DataAccess.GetDataTableBySqlSyntax("SELECT MAX(Id)+1 as 'maxID' FROM OVReports", "");
        int newReportID = (int)maxid.Rows[0][0];
        ReportID = newReportID;
        ReportName = ReportNameTextBox.Text;
        ReportDescription = this.ReportDescriptionTextBox.Text;
        //Generate SQL query based on selected items

        string query = "";
            
            if (Session["DynamicReports_DropDownList"] != null) DropDownList = (Dictionary<string, ASPxDropDownEdit>)Session["DynamicReports_DropDownList"];
            if (Session["DynamicReports_IntBox1List"] != null) IntBox1List = (Dictionary<string, ASPxSpinEdit>)Session["DynamicReports_IntBox1List"];
            if (Session["DynamicReports_IntBox2List"] != null) IntBox2List = (Dictionary<string, ASPxSpinEdit>)Session["DynamicReports_IntBox2List"];
            if (Session["DynamicReports_Calendar1List"] != null) Calendar1List = (Dictionary<string, ASPxDateEdit>)Session["DynamicReports_Calendar1List"];
            if (Session["DynamicReports_Calendar2List"] != null) Calendar2List = (Dictionary<string, ASPxDateEdit>)Session["DynamicReports_Calendar2List"];
            if (Session["DynamicReports_ColumnNameList"] != null) ColumnNameList = (Dictionary<String, ASPxTextBox>)Session["DynamicReports_ColumnNameList"];
            if (Session["DynamicReports_CheckBoxList"] != null) CheckBoxList = (Dictionary<string, ASPxCheckBox>)Session["DynamicReports_CheckBoxList"];
        bool EmployeeTableChoosen = false;
        bool TimeEntryTableChoosen = false;
        bool ProjectTableChoosen = false;
        bool ClientTableChoosen = false;
        bool UDFChoosen = false;
        bool OMV_EmployeeSalaryChoosen = false;
        bool ProjectDetailsChoosen = false;
        string[] columnOrder = columnOrderArray.Split('~');
            int columnindex = -1;
            foreach (string column in columnOrder)
            {
                string[] parts = column.Split('|');
                string[] rowID = parts[1].Split(',');
                string id = rowID[1] + "," + rowID[2] + "," + rowID[3] + "," + rowID[4] + "," + rowID[5];
                string tableName = rowID[1];
                string columnName = rowID[2];
                string type = rowID[3];
                string formula = rowID[4];
                string formulacolumn = rowID[5];
                string DisplayName = ColumnNameList[id].Text;
                TableisChoosen(tableName, ref EmployeeTableChoosen, ref TimeEntryTableChoosen, ref ProjectTableChoosen, ref ClientTableChoosen, ref UDFChoosen, ref OMV_EmployeeSalaryChoosen, ref ProjectDetailsChoosen);
                if ((bool)CheckBoxList[id].Value)
                {
                    columnindex++; //Add the column index to the OVReportColumns table in DB
                                   //handle the TimeEntry Columns here
                    if (formula.Equals("null"))
                        query += "INSERT INTO OVReportColumns (ReportId,ColumnName,DisplayName,TableName,AliasTableName,Formula,ColumnIndex) VALUES ( '" + newReportID+ "', '" + columnName+ "','"+ DisplayName+ "','" + tableName + "','" + tableName + "', NULL, "+ columnindex + ");";
                    else 
                        query += "INSERT INTO OVReportColumns (ReportId,ColumnName,DisplayName,TableName,AliasTableName,Formula,ColumnIndex) VALUES ( '" + newReportID + "', '" + formulacolumn + "','" + DisplayName + "','" + tableName + "','" + tableName + "','"+ formula + "', " + columnindex + ");";
                }
            }

            

            foreach (string i in DropDownList.Keys) {
                string parm = DropDownList[i].ID;
                ASPxListBox list = DropDownList[i].FindControl("|" + parm + ",listbox") as ASPxListBox;
                if (list.SelectedItems.Count != 0 && !list.SelectedItems[0].Text.Equals("Select All")) //Dont add filters of nothing or all are selected
                {
                    string[] parts = parm.Split(',');
                    string TableName = parts[0];
                    string ColumnName = parts[1];
                    string type = parts[2];
                    string DisplayName = (string)ColumnNameList[i].Text;
                    string formula = parts[3];
                    string formulaColumn = parts[4];
                    if (!formula.Equals("null")) ColumnName = formulaColumn;
                TableisChoosen(TableName, ref EmployeeTableChoosen, ref TimeEntryTableChoosen, ref ProjectTableChoosen, ref ClientTableChoosen, ref UDFChoosen, ref OMV_EmployeeSalaryChoosen, ref ProjectDetailsChoosen);
                foreach (ListEditItem item in list.SelectedItems)
                    {
                        string filterValue = item.Text.ToString();
                        query += "INSERT INTO OVReportFilters (FilterName,DisplayName,ReportId,TableName,FilterTypeId,FilterOperation,FilterValue,AliasTableName) VALUES ('" + ColumnName + "','" + DisplayName + "'," + newReportID + " , '" + TableName + "', (SELECT Id FROM OVReportFilterTypes WHERE Type = '" + type + "'),'IN', '" + filterValue + "','" + TableName + "');";
                    }
                }
            }
            foreach(string i in IntBox1List.Keys)
            {

                string[] parts = IntBox1List[i].ClientInstanceName.Split(',');
                string TableName = parts[0];
                string ColumnName = parts[1];
                string formula = parts[3];
                string formulaColumn = parts[4];
                if (!formula.Equals("null")) ColumnName = formulaColumn;
                TableisChoosen(TableName, ref EmployeeTableChoosen, ref TimeEntryTableChoosen, ref ProjectTableChoosen, ref ClientTableChoosen, ref UDFChoosen, ref OMV_EmployeeSalaryChoosen, ref ProjectDetailsChoosen);
                if (!ColumnName.Equals("Cost") && !ColumnName.Equals("TEHours"))
                {
                    string type = parts[2];
                    string DisplayName = (string)ColumnNameList[i].Text;

                    string from = (string)IntBox1List[i].Text;
                    string to = (string)IntBox2List[i].Text;
                    if (from.Equals("") && from.Equals(""))//Dont add filter
                    {
                        continue;
                    }
                    else if (from.Equals(""))//From 0 to X
                    {
                        query += "INSERT INTO OVReportFilters (FilterName,DisplayName,ReportId,TableName,FilterTypeId,FilterOperation,FilterValue,AliasTableName) VALUES ('" + ColumnName + "','" + DisplayName + "'," + newReportID + " , '" + TableName + "', (SELECT Id FROM OVReportFilterTypes WHERE Type = '" + type + "'),'<=', '" + to + "','" + TableName + "');";
                    }
                    else if (to.Equals(""))//From X to infinite
                    {
                        query += "INSERT INTO OVReportFilters (FilterName,DisplayName,ReportId,TableName,FilterTypeId,FilterOperation,FilterValue,AliasTableName) VALUES ('" + ColumnName + "','" + DisplayName + "'," + newReportID + " , '" + TableName + "', (SELECT Id FROM OVReportFilterTypes WHERE Type = '" + type + "'),'>=', '" + from + "','" + TableName + "');";
                    }
                    else if (to.Equals(from)) //Equal
                    {
                        query += "INSERT INTO OVReportFilters (FilterName,DisplayName,ReportId,TableName,FilterTypeId,FilterOperation,FilterValue,AliasTableName) VALUES ('" + ColumnName + "','" + DisplayName + "'," + newReportID + " , '" + TableName + "', (SELECT Id FROM OVReportFilterTypes WHERE Type = '" + type + "'),'=', '" + from + "','" + TableName + "');";
                    }
                    else if (int.Parse(from) > int.Parse(to)) //error
                    {

                    }
                    else //both
                    {
                        query += "INSERT INTO OVReportFilters (FilterName,DisplayName,ReportId,TableName,FilterTypeId,FilterOperation,FilterValue,AliasTableName) VALUES ('" + ColumnName + "','" + DisplayName + "'," + newReportID + " , '" + TableName + "', (SELECT Id FROM OVReportFilterTypes WHERE Type = '" + type + "'),'<=', '" + to + "','" + TableName + "');";
                        query += "INSERT INTO OVReportFilters (FilterName,DisplayName,ReportId,TableName,FilterTypeId,FilterOperation,FilterValue,AliasTableName) VALUES ('" + ColumnName + "','" + DisplayName + "'," + newReportID + " , '" + TableName + "', (SELECT Id FROM OVReportFilterTypes WHERE Type = '" + type + "'),'>=', '" + from + "','" + TableName + "');";
                    }
                }
            }
            foreach(string i in Calendar1List.Keys)
            {
                string[] parts = Calendar1List[i].ClientInstanceName.Split(',');
                string TableName = parts[0];
                string ColumnName = parts[1];
                string type = parts[2];
                string DisplayName = (string)ColumnNameList[i].Text;
                string formula = parts[3];
                string formulaColumn = parts[4];
                if (!formula.Equals("null")) ColumnName = formulaColumn;

                TableisChoosen(TableName, ref EmployeeTableChoosen, ref TimeEntryTableChoosen, ref ProjectTableChoosen, ref ClientTableChoosen, ref UDFChoosen, ref OMV_EmployeeSalaryChoosen, ref ProjectDetailsChoosen);

                DateTime from = Calendar1List[i].Date;
                DateTime to = (DateTime)Calendar2List[i].Date;
                if (from.Date.Year.ToString().Equals("1") && to.Date.Year.ToString().Equals("1")){ //both are empty filter not needed
                    continue;
                }
                else if (from.Date.Year.ToString().Equals("1"))//From is empty (FROM infinity To X
                {
                    query += "INSERT INTO OVReportFilters (FilterName,DisplayName,ReportId,TableName,FilterTypeId,FilterOperation,FilterValue,AliasTableName) VALUES ('" + ColumnName + "','" + DisplayName + "'," + newReportID + " , '" + TableName + "', (SELECT Id FROM OVReportFilterTypes WHERE Type = '" + type + "'),'<=', '" + to + "','" + TableName + "');";
                }
                else if (to.Date.Year.ToString().Equals("1"))//to is Empty (FRom x to infinity)
                {
                    query += "INSERT INTO OVReportFilters (FilterName,DisplayName,ReportId,TableName,FilterTypeId,FilterOperation,FilterValue,AliasTableName) VALUES ('" + ColumnName + "','" + DisplayName + "'," + newReportID + " , '" + TableName + "', (SELECT Id FROM OVReportFilterTypes WHERE Type = '" + type + "'),'>=', '" + from + "','" + TableName + "');";
                }
                else if (from.Date == to.Date)
                {
                    query += "INSERT INTO OVReportFilters (FilterName,DisplayName,ReportId,TableName,FilterTypeId,FilterOperation,FilterValue,AliasTableName) VALUES ('" + ColumnName + "','" + DisplayName + "'," + newReportID + " , '" + TableName + "', (SELECT Id FROM OVReportFilterTypes WHERE Type = '" + type + "'),'=', '" + from + "','" + TableName + "');";
                }
                else if (from > to) //error
                {

                }
                else //both
                {
                    query += "INSERT INTO OVReportFilters (FilterName,DisplayName,ReportId,TableName,FilterTypeId,FilterOperation,FilterValue,AliasTableName) VALUES ('" + ColumnName + "','" + DisplayName + "'," + newReportID + " , '" + TableName + "', (SELECT Id FROM OVReportFilterTypes WHERE Type = '" + type + "'),'<=', '" + to + "','" + TableName + "');";
                    query += "INSERT INTO OVReportFilters (FilterName,DisplayName,ReportId,TableName,FilterTypeId,FilterOperation,FilterValue,AliasTableName) VALUES ('" + ColumnName + "','" + DisplayName + "'," + newReportID + " , '" + TableName + "', (SELECT Id FROM OVReportFilterTypes WHERE Type = '" + type + "'),'>=', '" + from + "','" + TableName + "');";
                }
            }

       string Employee_ProjectjoinQuery = "INSERT INTO OVReportJoins (ReportId, Table1, Table2, AliasTable1, AliasTable2, JoinType, JoinTable1Key, JoinTable2Key) VALUES(" + newReportID + ", 'Employee','Project', 'Employee', 'Project', 'join' , 'EmployeeID', 'EmployeeID');";
        string Employee_TimeEntryjoinQuery = "INSERT INTO OVReportJoins (ReportId, Table1, Table2, AliasTable1, AliasTable2, JoinType, JoinTable1Key, JoinTable2Key) VALUES( " + newReportID + ", 'Employee','TimeEntry', 'Employee', 'TimeEntry', 'join' , 'EmployeeID', 'EmployeeID');";
        string TimeEntry_ProjectjoinQuery = "INSERT INTO OVReportJoins (ReportId, Table1, Table2, AliasTable1, AliasTable2, JoinType, JoinTable1Key, JoinTable2Key) VALUES( " + newReportID + ", 'TimeEntry','Project', 'TimeEntry', 'Project', 'join' , 'ProjectID', 'ProjectID');";
        string Employee_ClientjoinQuery = "INSERT INTO OVReportJoins (ReportId, Table1, Table2, AliasTable1, AliasTable2, JoinType, JoinTable1Key, JoinTable2Key) VALUES( " + newReportID + ", 'Employee','Client', 'Employee', 'Client', 'join' , 'EmployeeID', 'EmployeeID');";
        string Project_ClientjoinQuery = "INSERT INTO OVReportJoins (ReportId, Table1, Table2, AliasTable1, AliasTable2, JoinType, JoinTable1Key, JoinTable2Key) VALUES( " + newReportID + ", 'Client','Project', 'Client', 'Project', 'join' , 'ClientID', 'ClientID');";
        string Employee_UDFjoinQuery = "INSERT INTO OVReportJoins (ReportId, Table1, Table2, AliasTable1, AliasTable2, JoinType, JoinTable1Key, JoinTable2Key) VALUES( " + newReportID + ", 'Employee','UDF', 'Employee', 'UDF', 'join' , 'EmployeeID', 'EmployeeID');";
        string Employee_OMV_EmployeeSalaryjoinQuery = "INSERT INTO OVReportJoins (ReportId, Table1, Table2, AliasTable1, AliasTable2, JoinType, JoinTable1Key, JoinTable2Key) VALUES( " + newReportID + ", 'Employee','OMV_EmployeeSalary', 'Employee', 'OMV_EmployeeSalary', 'join' , 'EmployeeID', 'UserId');";
        string Project_ProjectDetailsjoinQuery = "INSERT INTO OVReportJoins (ReportId, Table1, Table2, AliasTable1, AliasTable2, JoinType, JoinTable1Key, JoinTable2Key) VALUES( " + newReportID + ", 'Project','ProjectDetails', 'Project', 'ProjectDetails', 'join' , 'ProjectID', 'ProjectID');";

        //bools are for the joins
        if (EmployeeTableChoosen && !TimeEntryTableChoosen && !ClientTableChoosen && !ProjectTableChoosen)
            query += "INSERT INTO OVReportJoins (ReportId, Table1, Table2, AliasTable1, AliasTable2, JoinType, JoinTable1Key, JoinTable2Key) VALUES(" + newReportID + ", 'Employee',NULL, 'Employee', NULL, NULL , NULL, NULL);";
        else if (TimeEntryTableChoosen && !EmployeeTableChoosen && !ProjectTableChoosen && !ClientTableChoosen)
            query += "INSERT INTO OVReportJoins (ReportId, Table1, Table2, AliasTable1, AliasTable2, JoinType, JoinTable1Key, JoinTable2Key) VALUES(" + newReportID + ", 'TimeEntry',NULL, 'TimeEntry', NULL, NULL , NULL, NULL);";
        else if (ProjectTableChoosen && !TimeEntryTableChoosen && !EmployeeTableChoosen && !ClientTableChoosen)
            query += "INSERT INTO OVReportJoins (ReportId, Table1, Table2, AliasTable1, AliasTable2, JoinType, JoinTable1Key, JoinTable2Key) VALUES(" + newReportID + ", 'Project',NULL, 'Project', NULL, NULL , NULL, NULL);";
        else if (ClientTableChoosen && !ProjectTableChoosen && !EmployeeTableChoosen && !TimeEntryTableChoosen)
            query += "INSERT INTO OVReportJoins (ReportId, Table1, Table2, AliasTable1, AliasTable2, JoinType, JoinTable1Key, JoinTable2Key) VALUES(" + newReportID + ", 'Client',NULL, 'Client', NULL, NULL , NULL, NULL);";

        if (EmployeeTableChoosen && TimeEntryTableChoosen) query += Employee_TimeEntryjoinQuery;
        if ((!EmployeeTableChoosen && !ProjectTableChoosen && ClientTableChoosen && TimeEntryTableChoosen) || (ProjectTableChoosen && TimeEntryTableChoosen)) query += TimeEntry_ProjectjoinQuery;
        if (EmployeeTableChoosen && ProjectTableChoosen && !TimeEntryTableChoosen) query += Employee_ProjectjoinQuery;
        if ((!EmployeeTableChoosen && !ProjectTableChoosen && ClientTableChoosen && TimeEntryTableChoosen) || (ProjectTableChoosen && ClientTableChoosen)) query += Project_ClientjoinQuery;
        if (EmployeeTableChoosen && ClientTableChoosen && !ProjectTableChoosen) query += Employee_ClientjoinQuery;
        if (UDFChoosen && EmployeeTableChoosen) query += Employee_UDFjoinQuery;
        if (EmployeeTableChoosen && OMV_EmployeeSalaryChoosen) query += Employee_OMV_EmployeeSalaryjoinQuery;
        if (ProjectTableChoosen && ProjectDetailsChoosen) query += Project_ProjectDetailsjoinQuery;


        DataAccess.ExecuteSqlStatement(query, "");
        //AssignReportComboBox();

    }
    private void TableisChoosen(string tablename, ref bool Employee, ref bool TimeEntry, ref bool Project, ref bool Client, ref bool UDF, ref bool OMV_EmployeeSalary, ref bool ProjectDetails)
    {
        if (tablename.Equals("TimeEntry")) TimeEntry = true;
        else if (tablename.Equals("Employee")) Employee = true;
        else if (tablename.Equals("UDF")) UDF = true;
        else if (tablename.Equals("OMV_EmployeeSalary")) OMV_EmployeeSalary = true;
        else if (tablename.Equals("Project")) Project = true;
        else if (tablename.Equals("ProjectDetails")) ProjectDetails = true;
        else if (tablename.Equals("Client")) Client = true;
    }
    #endregion


    //protected void ASPxGridViewReport_CustomColumnGroup(object sender, CustomColumnSortEventArgs e)
    //{
    //   if( e.Column.FieldName == "Date") //Date should be based on the report Date Column Name
    //   {
            
    //        DateTime t1 = (DateTime)e.Value1;
    //        DateTime t2 = (DateTime)e.Value2;
    //        var val1 = t1.Month + " " + t1.Year;
    //        var val2 = t2.Month + " " + t2.Year;
    //        e.Result = val1.CompareTo(val2);
    //        e.Handled = true;
    //   }

    //}

    #region Should be used
    protected void ASPXDateEdit_CustomCell(object sender, CalendarDayCellPreparedEventArgs e)
    {
        // #c00000 (red active)
        // #333333 (black active)
        // #ececec (grey not active)
        if (!e.IsOtherMonthDay)
        {
            if (e.Date.DayOfWeek == DayOfWeek.Sunday) e.Cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#333333");
            else if (e.Date.DayOfWeek == DayOfWeek.Friday) e.Cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#c00000"); //#c00000
        }
    }
    #endregion

    #region Creating SQL for Grid (Wiaam's code with Mohammad changes)
    private void BindGrid(int ReportID, ref string sql)
    {
        DataTable ColumnList = GetColumns(ReportID);
        Session["DynamicReports_ReportGrid_ColumnList"] = ColumnList;
        string Query = GetQueryForReprot(ReportID);
        sql = Query;

        ASPxGridViewReport.AutoGenerateColumns = false;
        ASPxGridViewReport.Columns.Clear();
        foreach (DataRow dr in ColumnList.Rows)
        {
            if (dr["ColumnName"].ToString().Equals("TEDate")) {
                GridViewDataDateColumn Column = new GridViewDataDateColumn();
                Column.Caption = dr["ColumnDisplayName"].ToString();
                Column.FieldName = dr["ColumnDisplayName"].ToString();
                Column.PropertiesDateEdit.DisplayFormatString = "dd/MM/yyyy";
                Column.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                ASPxGridViewReport.Columns.Add(Column);
            }
            else
            {
                GridViewDataColumn Column = new GridViewDataColumn();
                //GridViewDataTextColumn Column = new GridViewDataTextColumn();
                Column.Caption = dr["ColumnDisplayName"].ToString();
                Column.FieldName = dr["ColumnDisplayName"].ToString();
                ASPxGridViewReport.Columns.Add(Column);
            }
          
        }
        DataTable grid = DataAccess.GetDataTableBySqlSyntax(Query, "");
        Session["DynamicReports_ReportGrid"] = grid;
        ASPxGridViewReport.DataSource = grid; //By Muhammad
        ASPxGridViewReport.DataBind();
    }
    protected DataTable GetColumns(int ReportId)
    {
        //Mohammad eddited the query to use column index
        string query = "select distinct ColumnName, ColumnDisplayName,ColumnTableName,CAliasTableName,Formula, ColumnIndex from OVReportDetailsView where ReportId=" + ReportId + "ORDER BY ColumnIndex ASC";// where ReportId=" + ReportId ;

        //SqlCommand Command = new SqlCommand(query, Con);
        //Con.Open();
        //SqlDataReader reader = Command.ExecuteReader();
        DataTable dt = new DataTable();
        dt = DataAccess.GetDataTableBySqlSyntax(query,"");
        //dt.Load(reader);
        //Con.Close();
        return (dt);

    }

    protected string GetQueryForReprot(int ReportId)
    {
        DataTable ColumnList = GetColumns(ReportId);
        DataTable FilterList = GetFilters(ReportId);
        DataTable JoinList = GetJoins(ReportId);
        List<object> Filters = new List<object>();
        DataTable UserFilterList = new DataTable();
        DataTable INFilterList = GetINFliters(ReportId); //Mohammad added this
        string FromDate, ToDate = "";
        //if (FromTextBox.Visible == true)
        //{
        //    FromDate = From.SelectedDate.ToShortDateString();
        //}
        //if (ToTextBox.Visible == true)
        //{
        //    ToDate = To.SelectedDate.ToShortDateString();
        //}
        //List<DropDownList> ddlList = new List<DropDownList>();
        //foreach (Control c in PlaceHolder1.Controls)
        //{
        //    if (c.GetType() == typeof(DropDownList))
        //    {
        //        ddlList.Add((DropDownList)c);
        //    }
        //}
        //foreach (DataRow dr in FilterList.Rows)
        //{
        //    foreach (DropDownList ddl in ddlList)
        //    {
        //        if (dr["FilterName"].ToString() == ddl.DataTextField && !ddl.SelectedValue.ToString().Contains(dr["FilterValue"].ToString()))
        //        {
        //            dr["FilterValue"] = ddl.SelectedValue.ToString();
        //        }
        //    }
        //}

        String query = BuildSelectStmt(ColumnList) + BuildFromStmt2(GetJoinsOrder(JoinList).Item1, GetJoinsOrder(JoinList).Item2, JoinList) + BuildWhereStmt(FilterList, INFilterList) + BuildGroupByStmt(ColumnList);

        return (query);
    }

    protected DataTable GetFilters(int ReportId)
    {

        //var Compiler = new SqlServerCompiler();
        //var q = new Query("OVReportDetailsView").Where("ReportId", ReportId.ToString());
        //SqlResult result = Compiler.Compile(q);
        //string sql = result.Sql;

        //Mohammad changed the following
        string query = "select distinct FilterName, FilterDisplayName,FilterTableName, FilterTypeId, FilterValue, FilterOperation, FAliasTableName,FilterTypeName from OVReportDetailsView where ReportId=" + ReportId + "AND FilterOperation !='IN' AND FilterOperation !='NOT IN'";// where ReportId=" + ReportId ;

        //SqlCommand Command = new SqlCommand(query, Con);
        //Con.Open();
        //SqlDataReader reader = Command.ExecuteReader();
        DataTable dt = new DataTable();
        dt = DataAccess.GetDataTableBySqlSyntax(query, "");
        //dt.Load(reader);
        //Con.Close();
        return (dt);

    }


    protected DataTable GetJoins(int ReportId)
    {
        string query = "select distinct JoinId, JoinTable1, JoinTable2, JoinTable1Key, AliasTable1, AliasTable2, JoinType, JoinTable2Key from OVReportDetailsView where ReportId=" + ReportId + "order by JoinId";// where ReportId="+ ReportId ;

        //SqlCommand Command = new SqlCommand(query, Con);
        //Con.Open();
        //SqlDataReader reader = Command.ExecuteReader();
        DataTable dt = new DataTable();
        dt = DataAccess.GetDataTableBySqlSyntax(query, "");
        //if (dt.Rows[0][0].ToString().Equals("")) return (new DataTable());
        //dt.Load(reader);
        //Con.Close();
        return (dt);

    }

    protected string BuildFromStmt2(Queue<string> q, DataRow[] SeperateJoins, DataTable JoinList)
    {
        string Initial = " From ";
        string FromStmt = Initial;

        DataTable TempJoins = new DataTable();
        DataTable JoinsDone = new DataTable();
        bool IsSeperate = false;
        bool NoJoin = false;
        TempJoins = JoinList.Copy();

        List<string> Tables = new List<string>();

 
        if (TempJoins.Rows.Count == 1 && TempJoins.Rows[0]["JoinTable2"].ToString().Equals("")) //Mohammad added the if statement
        {
            FromStmt += TempJoins.Rows[0]["JoinTable1"].ToString() + " " + TempJoins.Rows[0]["AliasTable1"].ToString();
        }
        else
        {
            while (TempJoins.Rows.Count != 0)
            {
                string key = q.Dequeue();
                if (key == null)
                    continue;
                DataRow[] T1 = TempJoins.Select("AliasTable1 = '" + key + "' or AliasTable2 = '" + key + "'");



                foreach (DataRow dr in T1)
                {
                    string Alias1 = (dr["AliasTable1"] == null ? null : dr["AliasTable1"].ToString());
                    string Alias2 = (dr["AliasTable2"] == null ? null : dr["AliasTable2"].ToString());

                    if (Alias1 == "" || Alias2 == "")
                    {
                        NoJoin = true;
                    }

                    //DataRow Seperate = SeperateJoins.Select(p => p["AliasTable1"].ToString() == Alias1 && p["AliasTable2"].ToString()==Alias2);
                    foreach (DataRow e in SeperateJoins)
                    {
                        if (e != null && e["AliasTable1"] == Alias1 && e["AliasTable2"] == Alias2)
                        {
                            IsSeperate = true;
                        }
                    }

                    //JoinsDone.Rows.Add(dr.ItemArray);




                    Tables.Add(key);

                    if (FromStmt == Initial)
                    {
                        FromStmt += dr["JoinTable1"].ToString() + " " + Alias1 + " " + dr["JoinType"].ToString() + " " + dr["JoinTable2"].ToString() + " " + Alias2 + " on " +
                    Alias1 + "." + dr["JoinTable1Key"].ToString() + " = " + Alias2 + "." + dr["JoinTable2Key"].ToString();
                    }
                    else

                        if (Tables.Contains(Alias1) && !IsSeperate && !NoJoin)
                    {

                        FromStmt += " " + dr["JoinType"].ToString() + " " + dr["JoinTable2"].ToString() + " " + Alias2 + " on "
                            + Alias1 + "." + dr["JoinTable1Key"].ToString() + " = " + Alias2 + "." + dr["JoinTable2Key"].ToString();
                    }
                    else
                            if (Tables.Contains(Alias2) && !IsSeperate && !NoJoin)
                    {
                        FromStmt += " " + dr["JoinType"].ToString() + " " + dr["JoinTable1"].ToString() + " " + Alias1 + " on "
                            + Alias1 + "." + dr["JoinTable1Key"].ToString() + " = " + Alias2 + "." + dr["JoinTable2Key"].ToString();
                    }
                    else
                                if (IsSeperate)
                    {
                        Tables.Add(Alias1);
                        Tables.Add(Alias2);
                        FromStmt += " , " + dr["JoinTable1"].ToString() + " " + Alias1 + " " + dr["JoinType"].ToString() + " " + dr["JoinTable2"].ToString() + " " + Alias2 + " on "
                                    + Alias1 + "." + dr["JoinTable1Key"].ToString() + " = " + Alias2 + "." + dr["JoinTable2Key"].ToString();
                    }
                    else
                                    if (NoJoin)
                    {
                        if (dr["JoinTable1"] != null)
                        {
                            FromStmt += " , " + dr["JoinTable1"].ToString() + " " + Alias1;
                        }
                        else
                        {
                            FromStmt += " , " + dr["JoinTable2"].ToString() + " " + Alias2;
                        }
                    }
                    TempJoins.Rows.Remove(dr);

                }
            }
        }
        //foreach (DataRow dr in SeperateJoins)
        //{

        //    if (dr != null)
        //    {
        //        string Alias1 = dr["AliasTable1"].ToString();
        //        string Alias2 = dr["AliasTable2"].ToString();
        //        FromStmt += " , " + dr["JoinTable1"].ToString().Replace(" ", "") + " " + Alias1 + " " + dr["JoinType"].ToString() + " " + dr["JoinTable2"].ToString().Replace(" ", "") + " " + Alias2 + " on "
        //               + Alias1 + "." + dr["JoinTable1Key"].ToString().Replace(" ", "") + " = " + Alias2 + "." + dr["JoinTable2Key"].ToString().Replace(" ", "");
        //    }
        //}


        return (FromStmt);
    }

    protected DataTable GetINFliters(int ReportId)
    {
        string query = "SELECT distinct f2.AliasTableName, f2.FilterOperation, f2.FilterName,  substring( (SELECT ',''' + f1.FilterValue + '''' AS [text()] FROM OVReportFilters f1 WHERE f1.FilterName = f2.FilterName AND f1.ReportId=" + ReportId + " AND f1.FilterOperation='IN' ORDER BY f1.FilterName FOR XML PATH('')),2,8000) [OVReportFilters] FROM OVReportFilters f2 WHERE f2.ReportID=" + ReportId + " AND f2.FilterOperation='IN' ";
        DataTable dt = new DataTable();
        dt = DataAccess.GetDataTableBySqlSyntax(query, "");
        return dt;
    }

    protected string BuildWhereStmt(DataTable FilterList, DataTable INFilterList)
    {
        string whereStmt = " Where ";
        int i = 0;
        if (INFilterList.Rows.Count == 0 && FilterList.Rows.Count == 0) return (""); //Mohamamd added this to handle if there is no filters

        foreach (DataRow dr in FilterList.Rows)
        {
            whereStmt += dr["FAliasTableName"].ToString() + "." + dr["FilterName"].ToString() + " " +
                dr["FilterOperation"].ToString() + "'" + dr["FilterValue"].ToString() + "'";
            i++;
            if (i < FilterList.Rows.Count)
                whereStmt += " and ";
        }
        i = 0;
        if (FilterList.Rows.Count != 0 && INFilterList.Rows.Count !=0) whereStmt += " and ";
        foreach (DataRow dr in INFilterList.Rows)
        {
            whereStmt += dr["AliasTableName"].ToString() + "." + dr["FilterName"].ToString() + " " +
                dr["FilterOperation"].ToString() + "(" + dr["OVReportFilters"].ToString() + ")";
            i++;
            if (i < INFilterList.Rows.Count)
                whereStmt += " and ";
        }


        return (whereStmt);
    }

    protected string BuildSelectStmt(DataTable ColumnList)
    {
        string SelectStmt = "Select ";
        int i = 0;

        foreach (DataRow dr in ColumnList.Rows)
        {
            if (dr["Formula"].ToString().Equals("SUM")) //Mohammad added the if statement
            {
                SelectStmt += "SUM(" + dr["CAliasTableName"].ToString() + "." + dr["ColumnName"].ToString() + ") as '" + dr["ColumnDisplayName"].ToString() + "' ";
            }
            else
            {
                SelectStmt += dr["CAliasTableName"].ToString() + "." + dr["ColumnName"].ToString() + " as '" + dr["ColumnDisplayName"].ToString() + "' ";
            }
            i++;
            if (i < ColumnList.Rows.Count)
                SelectStmt += ", ";
        }


        return (SelectStmt);
    }

    protected string BuildGroupByStmt(DataTable ColumnList)
    {
        bool NoGroup = true;
        string GroupStmt = " GROUP BY ";
        
        foreach (DataRow dr in ColumnList.Rows)
        {
            if (dr["Formula"].ToString().Equals("SUM"))
            {
                NoGroup = false;
              
            }
            else
            {
                GroupStmt += dr["CAliasTableName"].ToString() + "." + dr["ColumnName"].ToString() + ",";
            
               
            }
        }
        if (NoGroup) return ("");
        else
        {
            GroupStmt = GroupStmt.Remove(GroupStmt.Length - 1);
            return GroupStmt;
        }
    }

    protected Tuple<Queue<string>, DataRow[]> GetJoinsOrder(DataTable JoinList)
    {
        DataTable TempJoins = new DataTable();
        TempJoins = JoinList.Copy();

        Queue<string> q = new Queue<string>();

        DataRow[] SeperateJoins = new DataRow[JoinList.Rows.Count];

        int SeperateLength = 0;
        
        while (TempJoins.Rows.Count != 0)
        {
            DataRow r = null;

            r = TempJoins.Rows[0];

            string Alias1 = (r["AliasTable1"] == null ? null : r["AliasTable1"].ToString());
            string Alias2 = (r["AliasTable2"] == null ? null : r["AliasTable2"].ToString());

            if (Alias1 == "" || Alias2 == "")
            {
                q.Enqueue(Alias1);
                q.Enqueue(Alias2);
                TempJoins.Rows.Remove(r);
            }

            else

                if (!q.Contains(Alias1) && !q.Contains(Alias2) && TempJoins.Rows.Count != 0 && q.Count != 0)
            {
                DataRow[] SR = JoinList.Select("AliasTable1 = '" + Alias1 + "' and AliasTable2 = " + "'" + Alias2 + "'");
                SeperateJoins[SeperateLength] = SR[0];
                SeperateLength++;
                
                TempJoins.Rows.Remove(r);
                continue;
            }
            else
            {
                if (q.Count == 0)
                {
                    q.Enqueue(Alias1);
                    q.Enqueue(Alias2);
                    TempJoins.Rows.Remove(r);
                }
                else
                {
                    r = TempJoins.Rows[0];

                    Alias1 = r["AliasTable1"].ToString();
                    Alias2 = r["AliasTable2"].ToString();
                    q.Enqueue(q.Contains(Alias1) == true ? null : Alias1);
                    q.Enqueue(q.Contains(Alias2) == true ? null : Alias2);
                    TempJoins.Rows.Remove(r);
                }

                DataRow[] T1 = TempJoins.Select("AliasTable1 = '" + Alias1 + "' or AliasTable2 = " + "'" + Alias1 + "'");

                DataRow[] T2 = TempJoins.Select("AliasTable1 = '" + Alias2 + "' or AliasTable2 = " + "'" + Alias2 + "'");


                if (T1 != null)
                    foreach (DataRow d in T1)
                    {
                        q.Enqueue(q.Contains(d["AliasTable1"].ToString()) == true ? null : d["AliasTable1"].ToString());
                        q.Enqueue(q.Contains(d["AliasTable2"].ToString()) == true ? null : d["AliasTable2"].ToString());
                        DataRow[] dr = TempJoins.Select("AliasTable1 = " + "'" + d["AliasTable1"].ToString() + "'" + " and AliasTable2 =" + "'" + d["AliasTable2"].ToString() + "'");
                        TempJoins.Rows.Remove(dr[0]);
                    }

                if (T2 != null)
                    foreach (DataRow d in T2)
                    {
                        q.Enqueue(q.Contains(d["AliasTable1"].ToString()) == true ? null : d["AliasTable1"].ToString());
                        q.Enqueue(q.Contains(d["AliasTable2"].ToString()) == true ? null : d["AliasTable2"].ToString());
                        DataRow[] dr = TempJoins.Select("AliasTable1 = " + "'" + d["AliasTable1"].ToString() + "'" + " and AliasTable2 =" + "'" + d["AliasTable2"].ToString() + "'");
                        TempJoins.Rows.Remove(dr[0]);
                    }


            }
        }

        for (int i = 0; i <= SeperateLength; i++)
        {
            if (SeperateJoins[i] != null)
            {
                DataRow d = SeperateJoins[i];
                q.Enqueue(q.Contains(d["AliasTable1"].ToString()) == true ? null : d["AliasTable1"].ToString());
                q.Enqueue(q.Contains(d["AliasTable2"].ToString()) == true ? null : d["AliasTable2"].ToString());
                //SeperateJoins[i].Delete();
            }

        }
        return Tuple.Create(q, SeperateJoins);
    }
    #endregion

    #region Not Used
    protected void Grid_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        //var result = new Hashtable();
        var result = new Dictionary<int, string>();//column index, column name
        foreach (var col in this.ASPxGridView1.AllColumns)
        {
            var dataCol = col as GridViewDataColumn;
          
            if (dataCol != null)
            {
                result.Add(dataCol.VisibleIndex, dataCol.FieldName);
                //result[dataCol.FieldName] = new Dictionary<string, object>() {
                //    { "VisibleIndex", dataCol.VisibleIndex},
                //    { "sortIndex", dataCol.SortIndex },
                //    { "sortOrder", dataCol.SortOrder }
                };
            }
        
        Session["DynamicReports_GridColumns"] = result;
      //  e.Properties["cpColumnsProp"] = result;
    }
    protected void ColumnPanel_CallBack(object sender, CallbackEventArgsBase e)
    {
        if (e.Parameter == "ShowColumnsOrder")
        {
            //Add column Names to list box lbAvailable
            if (Session["DynamicReports_ColumnNameList"] != null) ColumnNameList = (Dictionary<String, ASPxTextBox>)Session["DynamicReports_ColumnNameList"];
            foreach (String i in ColumnNameList.Keys)
            {
                lbAvailable.Items.Add(new ListEditItem(ColumnNameList[i].Text, i));
            }
            //foreach (ASPxTextBox TextBox in ColumnNameList.Values)
            //{
            //    lbAvailable.Items.Add(new ListEditItem(TextBox.Text, TextBox.Value));
            //}

            lbChoosen.Items.Clear();
            //Session["DynamicReports_lbAvailable"] = lbAvailable.Items;
            //Session["DynamicReports_lbChoosen"] = lbChoosen.Items;
            this.ASPxPopUpControlColumns.ShowOnPageLoad = true;
        }

    }
    protected void GridPrev_CallBack(object sender, CallbackEventArgsBase e)
    {
        updateGridColumns();
    }
    private void updateGridColumns()
    {
        this.ASPxGridView1.Columns.Clear();
        DataTable t = new DataTable();
        for (int i = 0; i < lbChoosen.Items.Count; i++)
        {
            string columnName = lbChoosen.Items[i].Text;
            //GridViewDataTextColumn c = new GridViewDataTextColumn();
            //c.Caption = columnName;
            //this.ASPxGridView1.Columns.Add(c);
            t.Columns.Add(columnName);
        }
        this.ASPxGridView1.DataSource = t;
        Session["DynamicReports_GridView"] = t;

        if (lbChoosen.Items.Count == 0)
        {
            this.Gridlabel1.Visible = false;
            this.Gridlabel2.Visible = false;
            this.ASPxGridView1.Visible = false;
        }
        else
        {
            this.Gridlabel1.Visible = true;
            this.Gridlabel2.Visible = true;
            this.ASPxGridView1.Visible = true;
        }
        this.ASPxGridView1.DataBind();
        //DataTable test = (DataTable)Session["DynamicReports_GridView"];
        //ASPxGridView1.DataBind();
    }
    // private void refreshfilters()
    // {
    //     if (Session["DynamicReports_DropDownList"] != null) DropDownList = (Dictionary<string, ASPxDropDownEdit>)Session["DynamicReports_DropDownList"];
    //     if (Session["DynamicReports_IntBox1List"] != null) IntBox1List = (Dictionary<string, ASPxSpinEdit>)Session["DynamicReports_IntBox1List"];
    //     if (Session["DynamicReports_IntBox2List"] != null) IntBox2List = (Dictionary<string, ASPxSpinEdit>)Session["DynamicReports_IntBox2List"];
    //     if (Session["DynamicReports_Calendar1List"] != null) Calendar1List = (Dictionary<string, ASPxDateEdit>)Session["DynamicReports_Calendar1List"];
    //     if (Session["DynamicReports_Calendar2List"] != null) Calendar2List = (Dictionary<string, ASPxDateEdit>)Session["DynamicReports_Calendar2List"];
    //     if (Session["DynamicReports_ColumnNameList"] != null) ColumnNameList = (Dictionary<String, ASPxTextBox>)Session["DynamicReports_ColumnNameList"];
    //     if (Session["DynamicReports_ids"] != null) ids = (List<string>)Session["DynamicReports_ids"];
    //     foreach (string id in ids)
    //     {
    //         System.Web.UI.HtmlControls.HtmlGenericControl row = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
    //         row.Attributes["class"] = "row";
    //         row.Style.Add(HtmlTextWriterStyle.PaddingLeft, "15%");
    //         row.ID = "row," + id;
    //         if (DropDownList.ContainsKey(id))
    //         {
    //             string name = getColumnName(DropDownList[id].ID);
    //             HtmlGenericControl label1 = Addlabel1(name);
    //             row.Controls.Add(label1);
    //             HtmlGenericControl dropdown = AddDropDown(DropDownList[id]);
    //             row.Controls.Add(dropdown);
    //             HtmlGenericControl breakcol10 = AddBreak();
    //             row.Controls.Add(breakcol10);
    //             HtmlGenericControl label2 = Addlabel2(name);
    //             row.Controls.Add(label2);
    //             HtmlGenericControl columnname = AddColumnName(ColumnNameList[id + ",ColumnName"]);
    //             row.Controls.Add(columnname);

    //         }
    //         divFilters.Controls.Add(row);
    //     }
    // }
    // private string getColumnName(string id)
    // {
    //     return "TEST";
    // }
    // private HtmlGenericControl Addlabel2(string columnname)
    // {
    //     System.Web.UI.HtmlControls.HtmlGenericControl col10_5 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
    //     col10_5.Attributes["class"] = "col-10";
    //     LiteralControl label3 = new LiteralControl("<label>" + columnname + " Column Name</label>");
    //     col10_5.Controls.Add(label3);
    //     return col10_5;
    // }
    // private HtmlGenericControl AddBreak()
    // {
    //     System.Web.UI.HtmlControls.HtmlGenericControl col10_2 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
    //     col10_2.Attributes["class"] = "col-10";
    //     return col10_2;
    // }
    // private HtmlGenericControl Addlabel1(string columnname)
    // {
    //     LiteralControl label1 = new LiteralControl("<label>" + columnname + "</label>");
    //     System.Web.UI.HtmlControls.HtmlGenericControl col10_1 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
    //     col10_1.Attributes["class"] = "col-10";
    //     col10_1.Controls.Add(label1);
    //     return col10_1;
    // }
    //private HtmlGenericControl AddDropDown(ASPxDropDownEdit dropdown)
    // {
    //     System.Web.UI.HtmlControls.HtmlGenericControl col25_1 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
    //     col25_1.Attributes["class"] = "col-25";
    //     col25_1.ID = "col," + dropdown.ID;
    //     col25_1.Controls.Add(dropdown);
    //     return col25_1;
    // }
    // private HtmlGenericControl AddColumnName(ASPxTextBox columnname)
    // {
    //     System.Web.UI.HtmlControls.HtmlGenericControl col25_2 = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
    //     col25_2.Attributes["class"] = "col-25";
    //     col25_2.ID = "colColumnName" + columnname.ID;
    //     col25_2.Controls.Add(columnname);
    //     return col25_2;
    // }
    #endregion

}