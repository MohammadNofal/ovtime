﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Nonav.master" CodeFile="Reports2.aspx.cs" Inherits="Report2" %>

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxPanel ID="ASPxPanel1" runat="server" CssClass="detailPanelSmallHeader"></dx:ASPxPanel>
    <dx:ASPxCallbackPanel ID="DetailPanel" runat="server" ClientInstanceName="detailPanelSmall" Width="100%" CssClass="detailPanelLarge" Collapsible="false"  SettingsLoadingPanel-Enabled ="false" >
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <Styles>
            <ExpandBar Width="100%" CssClass="bar">
            </ExpandBar>
            <ExpandedExpandBar CssClass="expanded">
            </ExpandedExpandBar>
        </Styles>
        <BorderTop BorderWidth="0px">
        </BorderTop>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                Time Sheet Details:
                <br />
                <br />
                <dx:ASPxDateEdit runat="server" ID="ASPXDateEdit" Caption="Period End Date" AutoPostBack="true"  CssClass="editor" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption">
                </dx:ASPxDateEdit>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>

    

</asp:Content>
