﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Nonav.master" CodeFile="Reports.aspx.cs" Inherits="Reports" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    
    <script>
        function onSubmit(s, e) {
            ASPxCallbackPanelReport.PerformCallback();
        }

    </script>

    <dx:ASPxCallbackPanel ID="LeftPane2" runat="server" FixedPosition="WindowLeft" ClientInstanceName="leftPane" Width="300px" CssClass="leftPane2" Collapsible="true" OnCallback="LeftPane_Callback" ScrollBars="Auto" SettingsLoadingPanel-Enabled="false">
        <SettingsAdaptivity CollapseAtWindowInnerWidth="1023" />
        <ClientSideEvents/>
        <SettingsLoadingPanel Enabled="False">
        </SettingsLoadingPanel>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
                <dx:ASPxPanel ID="ASPxPanel2" runat="server" CssClass="detailPanelSmallHeaderBlue">
                </dx:ASPxPanel>
                <dx:ASPxPanel ID="ASPxPanel3" runat="server" CssClass="detailPanelSmallBlue">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent3" runat="server" SupportsDisabledAttribute="True">
                          Report List:
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
                <dx:ASPxNavBar Width="100%" EnableViewState="False" CssClass="LeftNavBar"
                            ID="navbar" runat="server" AutoCollapse="False" EncodeHtml="False" AllowSelectItem="False" ItemStyle-HoverStyle-BackColor ="LightGray" >
                    <GroupHeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" >
                    <HoverStyle BackColor="LightGray">
                    </HoverStyle>
                    </ItemStyle>
                    <ItemTextTemplate>
                        <span style="vertical-align: top; display: block; margin: 1px 0 0 1px"><%# Eval("Text") %></span>
                    </ItemTextTemplate>
                </dx:ASPxNavBar>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
    <dx:ASPxPanel ID="ASPxPanel1" runat="server" CssClass="detailPanelSmallHeader">
    </dx:ASPxPanel>
    
    <dx:ASPxCallbackPanel ID="DetailPanel" runat="server" ClientInstanceName="detailPanelSmall" Width="100%" CssClass="detailPanelLargeNoMargin" Collapsible="false"  SettingsLoadingPanel-Enabled ="false" >
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <Styles>
            <ExpandBar Width="100%" CssClass="bar">
            </ExpandBar>
            <ExpandedExpandBar CssClass="expanded">
            </ExpandedExpandBar>
        </Styles>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                Report Runner:
                <br />
                <br />
                <asp:Literal ID="Literal" runat="server"></asp:Literal>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>


    <dx:ASPxCallbackPanel ID="ASPxCallbackPanelReport" runat="server" ClientInstanceName="ASPxCallbackPanelReport" Width="100%" Collapsible="false"  SettingsLoadingPanel-Enabled ="false">
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <Styles>
            <ExpandBar Width="100%" CssClass="bar">
            </ExpandBar>
            <ExpandedExpandBar CssClass="expanded">
            </ExpandedExpandBar>
        </Styles>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                     <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="True" Width="100%" Visible="false" CssFilename="custom.css" />       
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>


    
    
</asp:Content>
