﻿<%@ Page Title="Project Analysis" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="Analysis.aspx.cs" Inherits="Reports_Analysis" %>

<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v15.2, Version=15.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <style>
          .detailPanelLarge {
            height:36px;
            margin-bottom:0px;
       }
         .editorText {
            margin-left: 27px;
            margin-right: 35px;
        }
        .editorCaption {
            padding-left:13px;
        }
        .header_fixed {
            padding: 10px;
            margin-bottom: 10px;   
            background-color: #fafafa;
            height: 70px;
            border-bottom: 1px solid #eee;
        }
    </style>  
        <script>

            function analysisBtn_click(s, e) {
                ASPxCallbackPanel2.PerformCallback();
            }
    </script>

    
    <dx:ASPxCallbackPanel ID="DetailPanel" runat="server" ClientInstanceName="detailPanelSmall" Width="100%" CssClass="detailPanelLarge" Collapsible="false"  SettingsLoadingPanel-Enabled ="false" >
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
<SettingsLoadingPanel Enabled="False"></SettingsLoadingPanel>

        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                 <label style="float:left;width:30%;"> PROJECT ANALYSIS TIME AND EXPENSE  </label> 
                     

            </dx:PanelContent>
        </PanelCollection>
        <Paddings Padding="8px" PaddingBottom="2px" />
    </dx:ASPxCallbackPanel>
    
   
    <table>
        <tr>
            <td style="padding-right: 10px"><dx:ASPxComboBox  ID="DropDownEdit" runat="server" ClientInstanceName="ProjectEdit"
          DropDownStyle="DropDownList" Caption="Main Project" OnInit="TreeList_Init" Width="300px" TextField="ProjectName" ValueField="ProjectID"
        ValueType="System.String" TextFormatString="{1}" DropDownWidth="400">
      <Columns>
            <dx:ListBoxColumn FieldName="ProjectID" Width="50px" />
            <dx:ListBoxColumn FieldName="ProjectName" Width="100" />
            <dx:ListBoxColumn FieldName="ProjectCode" Width="70px" />
        </Columns>
         
    </dx:ASPxComboBox></td>
            <td style="padding-right: 10px"><dx:ASPxButton ID="ASPxButton1" Text="Analysis" runat="server" AutoPostBack="false" ClientInstanceName="analysisBtn">
       <ClientSideEvents Click="analysisBtn_click" />
   </dx:ASPxButton></td>

        </tr>
        <tr>
             
            <td style="padding-right: 10px">
                <dx:ASPxComboBox ID="listExportFormat" runat="server"  SelectedIndex="6" 
                    ValueType="System.String" Width="300px" Height="23px" Caption="Export to" >
                                <Items>
                                    <dx:ListEditItem Text="Pdf" Value="0" />
                                    <dx:ListEditItem Text="Excel" Value="1" />
                                    <dx:ListEditItem Text="Mht" Value="2" />
                                    <dx:ListEditItem Text="Rtf" Value="3" />
                                    <dx:ListEditItem Text="Text" Value="4" />
                                    <dx:ListEditItem Text="Html" Value="5" />
                                    <dx:ListEditItem Text="Excel (Data Aware)" Value="6" />
                                </Items>
                               <%-- <ClientSideEvents
                                    Init="function(s, e) {
                                        fieldHeaderOptionsPanel.SetVisible(false);
                                        fieldValuesOptionsPanel.SetVisible(false);
                                        dataAwareOptionsPanel.SetVisible(true);
                                        checkCustomFormattedValuesAsText.SetEnabled(false);
                                    }"
                                    SelectedIndexChanged="function(s, e) {
                                        var selectedIndex = s.GetSelectedIndex(), 
                                            isExportToExcel = selectedIndex == 1,
                                            isDataAwareExport = selectedIndex == 6;
                                        fieldHeaderOptionsPanel.SetVisible(!isDataAwareExport);
                                        fieldValuesOptionsPanel.SetVisible(!isDataAwareExport);
                                        dataAwareOptionsPanel.SetVisible(isDataAwareExport);
                                        checkCustomFormattedValuesAsText.SetEnabled(isExportToExcel);
                                    }" 
                                 />--%>
                            </dx:ASPxComboBox>
            </td>
            <td style="padding-right: 10px" >
                <dx:ASPxButton ID="ASPxButton3" ClientInstanceName="buttonSaveAs" runat="server" ToolTip="Export and save"
                    OnClick="buttonSaveAs_Click" Text="Export"/>
            </td>
           
        </tr>
        </table>
    <dx:ASPxCallbackPanel runat="server" ID="ASPxCallbackPanel2" ClientInstanceName="ASPxCallbackPanel2" RenderMode="Div" OnCallback="ASPxCallbackPanel2_Callback">
        <PanelCollection>
            <dx:PanelContent ID="PanelContent1" runat="server">
                 
                <dx:ASPxPivotGrid ID="pivotGrid" runat="server"
                    Width="800px">
                    <OptionsView ShowFilterHeaders="False" ShowDataHeaders="False" ShowFilterSeparatorBar="False" ShowRowGrandTotalHeader="False"/>
                     <Fields>
                        <dx:PivotGridField Area="RowArea"
                            FieldName="DesignStage" Caption="Stage" />
                        <dx:PivotGridField Area="RowArea"
                            FieldName="DetailsStage" Caption="Details" />
                        <dx:PivotGridField Area="RowArea"
                            FieldName="DetailsStage2" Caption="Details Stage"  /> 
                         <dx:PivotGridField Area="RowArea"
                            FieldName="EmpName"  Caption="Employee" /> 
                          <dx:PivotGridField Area="DataArea"
                            FieldName="aHours"  Caption="Hours" SummaryType="Sum" /> 
                          <dx:PivotGridField Area="DataArea"
                            FieldName="EmpCost"  Caption="Cost (AED)" SummaryType="Sum" /> 
                    </Fields>
                    <OptionsPager RowsPerPage="100" />
                    <OptionsView HorizontalScrollBarMode="Auto" />
                    <OptionsFilter NativeCheckBoxes="False" />
                </dx:ASPxPivotGrid>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>


    <dx:ASPxPivotGridExporter ID="ASPxPivotGridExporter1" runat="server" ASPxPivotGridID="pivotGrid" Visible="False" />
</asp:Content>
   