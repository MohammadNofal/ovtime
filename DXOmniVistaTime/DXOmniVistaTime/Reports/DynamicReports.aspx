﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/OVMaster.master" CodeFile="DynamicReports.aspx.cs" Inherits="DynamicReports" %>
<asp:Content ID="header_" ContentPlaceHolderID="Header" runat="server">
    <style>
        .page-header-default {
            background-color: #fff;
            /* margin-bottom: 20px; */
            margin-bottom:0px;
            -webkit-box-shadow: 0 1px 0 0 #ddd;
            box-shadow: 0 1px 0 0 #ddd;
        }
    </style>
    <dx:ASPxCallbackPanel ID="ASPxCallbackPanel6" runat="server" OnCallback="DisplayReportExportOptions_CallBack" ClientInstanceName="ReportOptions" Width="100%" Collapsible="false" SettingsLoadingPanel-Enabled="false">
        <PanelCollection>
            <dx:PanelContent ID="PanelContent8" runat="server" SupportsDisabledAttribute="True">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 50%;">
                            <h4><span class="text-semibold" id="reportNameTitle" runat="server">Reports</span></h4>
                        </td>
                        <td style="width: 50%;text-align:right;">
                            <%--<asp:LinkButton ID="ExporttoPDFBtn" runat="server" OnClick="ExportToPDFClicked" ToolTip="Export to PDF" Visible="false" CssClass="pdf_icon"></asp:LinkButton>--%>
                            <dx:ASPxButton ID="ExporttoExecBtn" ClientInstanceName="ExporttoExecBtn" runat="server" OnClick="ExportToPDFClicked" ToolTip="Export to Excel" Visible="false" CssClass="excel_icon" RenderMode="Link" CssPostfix="none"></dx:ASPxButton> 
                            
                            <dx:ASPxButton ID="ExporttoPDFBtn" ClientInstanceName="ExporttoPDFBtn" runat="server" OnClick="ExportToPDFClicked" ToolTip="Export to PDF" Visible="false" CssClass="pdf_icon" RenderMode="Link" CssPostfix="none"></dx:ASPxButton>

                            <dx:ASPxButton ID="CustomizePDFBtn" ClientInstanceName="CustomizePDFBtn" runat="server" RenderMode="Link" AutoPostBack="false" CssPostfix="none" ClientSideEvents-Click="CustomizePDFClicked" ToolTip="Customize Export" Visible="false" CssClass="txt_icon"></dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPage" runat="server">
    
    <style>
         .dxWeb_pPrevDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNext {
             background: url('../content/images/iconmonstr-arrow-63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pPrev {
             background: url('../content/images/iconmonstr-arrow-64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNextDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }
        .newFont * 
        {
            font-family: Calibri;
            font-size: 16px;
          
        }
        .popupanimation {
            width: 100%;
            height: 100%;
            position: fixed;
            overflow-y: auto;
            overflow-x: hidden;
            top: 0;
            left: 0;
            animation-duration: 0.5s;
            -webkit-animation-duration: 0.5s;
            animation-name: slide-down;
            -webkit-animation-name: slide-down;
            animation-fill-mode: both;
            -webkit-animation-fill-mode: both;
            animation-delay: 0;
            -webkit-animation-delay: 0;
        }

        @Keyframes slide-down {
            0% {
                transform: translate3d(0,-200%,0);
            }

            100% {
                transform: translateZ(0);
            }
        }
        .container {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;
            width: 100%;
            height: 100%;
        }

        .col-25 {
            float: left;
            width: 25%;
            margin-top: 6px;
        }
        .col-33point33 {
            float: left;
            width: 33.33%;
            margin-top: 6px;
        }
        .col-10 {
            float: left;
            width: 10%;
            margin-top: 6px;
        }
        .col-37point5{
            float: left;
            width: 37.5%;
            margin-top: 6px;
        }
        .col-20 {
            float: left;
            width: 20%;
            margin-top: 6px;
        }
        .TextBox {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }
        .TextBoxPDF {
            width: 100%;
            padding: 12px;
            border-width: 0px;
            border-bottom-width: 4px;
            border-color: #ccc;
            resize: vertical;
        }

            .TextBoxPDF input {
                text-align: center;
            }
        .TextBoxPDFFooter{
            width:80%;
            padding:12px;
            border-width: 0px;
            border-bottom-width: 4px;
            border-color: #ccc;
            resize:vertical;
            /*float:left;*/
        }
         .SubmitButton {
             background-image: none !important;
             background-color: #4CAF50;
             color: white;
             padding: 6px 10px;
             border: none;
             border-radius: 4px;
             cursor: pointer;
             float: right;
             margin-top: 10px;
             margin-right: 10px;
         }
            .SubmitButton:hover {
                background-color: #419544;
            }

        .RestoreDefaultButton {
            background-image: none !important;
             background-color: #bfbfbf;
             color: white;
             padding: 6px 10px;
             border: none;
             border-radius: 4px;
             cursor: pointer;
             float: right;
             margin-top: 10px;
             margin-right: 10px;
        }

            .RestoreDefaultButton:hover {
                 background-color: #a6a6a6;
            }
       
        .ColumnChangeButton {
            background-image: none !important;
            background-color: #4CAF50;
            color: white;
            padding: 6px 10px;
            border: none;
            border-radius: 4px;
            text-align: center;
            cursor: pointer;
            margin-top: 10px;
            margin-right: 10px;
        }
            .ColumnChangeButton:hover{
            background-color: #419544;

        }
            .ColumnChangeButton:disabled{
                background-color: #A2A2A2;
            }
        .CancelButton {
             background-image: none !important;
             background-color: #ccc;
             color: black;
             padding: 6px 10px;
             border: none;
             border-radius: 4px;
             cursor: pointer;
             float: right;
             margin-top: 10px;
        }
            .CancelButton:hover {
                background-color: #adadad;
            }

          .TextBoxEdit {
            width: 100%;
            padding: 7px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
            margin-bottom:0px;
        }

       

        .close_icon {
            position: relative;
            font-family: FontAwesome !important;
            color: #FFFFFF !important;
            font-size: 200% !important;
            padding: 1px !important;
            text-decoration: none !important;
        }

            .close_icon:hover {
                color: #d9d9d9 !important;
            }

            .close_icon::before {
                content: "\f00d" !important;
            }

        .scroll {
            width: 100%;
            height: 100%;
            position: fixed;
            overflow-y: auto;
            overflow-x: hidden;
            top: 0;
            left: 0;
        }
        label {
            padding: 12px 12px 12px 0;
            display: inline-block;
        }
        .col-10 {
            float: left;
            width: 10%;
            margin-top: 6px;
        }
        .col-50{
            float: left;
            width: 50%;
            margin-top: 6px;
        }

        .col-25 {
            float: left;
            width: 25%;
            margin-top: 6px;
        }
        .col-55{
               float: left;
            width: 55%;
            margin-top: 6px;
        }

        .col-2point5 {
            float: left;
            width: 2.5%;
            margin-top: 6px;
        }
        .DateEdit {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }
         .ResetPasswordButton {
             background-image: none !important;
             background-color: #ff7979;
             color: white;
             padding: 7px 10px;
             border: none;
             border-radius: 4px;
             cursor: pointer;
             float: right;
             margin-left:6px;
             margin-right:6px;
         }
         .ResetPasswordButton:hover{
             background-color: #ff4c4c;
         }
         .ExportButton{
            background-image: none !important;
            background-color: #4CAF50;
            color: white;
            padding: 7px 10px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
         }
         .ExportButton:hover{
              background-color: #419544;
         }
          .ModifyButton {
            background-image: none !important;
            background-color: #4CAF50;
            color: white;
            padding: 7px 10px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            float: right;
        }

            .ModifyButton:hover {
                background-color: #419544;
            }
            .GroupRow{
                text-align:left;
                font-weight:bold;
                
            }
        /*.row:hover{
            background-color: #e6e6e6;
        }*/

        /*.lbRow
        {
            width: 200px;
        }*/
        
        /* like SelectedItem style */
        /*.ui-draggable-dragging
        {
            background-color: #A0A0A0;
            color: White;
        }*/
        
        /* small glowing effect */
        /*.hover
        {
            -webkit-box-shadow: 0 0 15px #ff0000;
            -moz-box-shadow: 0 0 15px #ff0000;
            box-shadow: 0 0 15px #ff0000;
        }*/

        /*.original-placeholde{

        }*/
       .pdf_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #4CAF50 !important;
            font-size: 170% !important;
            padding: 1px 1px 1px 1px;
            padding-left: 5px;
            text-decoration: none !important;
        }
        .pdf_icon:hover 
        {
            color: #419544  !important;
        }
        .pdf_icon:before 
        {
           content: "\f1c1" !important;
        }
        .excel_icon{
            position: relative;
            font-family: FontAwesome !important;
            color: #4CAF50 !important;
            font-size: 170% !important;
            padding: 1px 1px 1px 1px;
            padding-left: 5px;
            text-decoration: none !important;
        }
         .excel_icon:hover 
        {
            color: #419544  !important;
        }
        .excel_icon:before 
        {
           content: "\f1c3" !important;
        }
        .delete_icon{
             position: relative;
            font-family: FontAwesome !important;
            color: #ff7979 !important;
            font-size: 200% !important;
            padding: 1px 1px 1px 1px;
            padding-left: 5px;
            float:right;
            text-decoration: none !important;
        }
         .delete_icon:hover 
        {
            color: #ff4c4c !important;
        }
        .delete_icon:before 
        {
           content: "\f057" !important;
        }
        
        .ModifyReport_icon{
             position: relative;
            font-family: FontAwesome !important;
            color: #4CAF50 !important;
            font-size: 200% !important;
            padding: 1px 1px 1px 1px;
            padding-left: 5px;
            float:right;
            text-decoration: none !important;
        }
          .ModifyReport_icon:hover 
        {
            color: #419544 !important;
        }
        .ModifyReport_icon:before 
        {
           content: "\f044" !important;
        }
        .txt_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #4CAF50 !important;
            font-size: 170% !important;
            padding: 1px 1px 1px 1px;
            padding-left: 5px;
            text-decoration: none !important;
        }
        .txt_icon:hover 
        {
            color: #419544  !important;
        }
        .txt_icon:before 
        {
           content: "\f0f6" !important;
        }
        .ReportTitlePanel {
            height: 100%;
            width: 45px;
            padding: 0px;
            overflow-x: hidden;
        }
        .ReportPanel{
             height: 100%;
            width: 100%;
            overflow-x: hidden;
        }
        .flex {
            display: flex;
        }


        /*.navigationreports {
            margin: 0;
            padding: 10px 0;
            list-style: none;
            position: relative;
        }

            .navigationreports li {
                position: relative;
                padding-bottom: 5px;

            }

                .navigationreports li:first-child {
                    padding-top: 5px;
                }

            .navigationreports > li > ul {
                display: none;
            }

            .navigationreports > li.active > a, .navigationreports > li.active > a:hover, .navigationreports > li.active > a:focus {
                background-color: #4CAF50;
                color: #fff;
            }

            .navigationreports li a {
                color: #333333;
                display: block;
                -webkit-transition: background 0.15s linear, color 0.15s linear;
                -o-transition: background 0.15s linear, color 0.15s linear;
                transition: background 0.15s linear, color 0.15s linear;
            }

            .navigationreports li ul li:hover {
                background-color: #cfcfcf;
            }

            .navigationreports .hidden-ul {
                display: none;
            }

            .navigationreports > li ul {
                list-style: none;
                margin: 0;
                padding: 0;
                background-color: rgba(0, 0, 0, 0.15);
                -webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.05);
                box-shadow: 0 1px 0 rgba(255, 255, 255, 0.05);
            }

                .navigationreports > li ul li a {
                    padding-left: 7px;
                }

            .navigationreports li a {
                padding-left: 3px;
                font-size: 14px;
            }


            .navigationreports li > .has-ul:after {
               
                font-family: FontAwesome !important;
                font-size: 14px;
                display: block;
                position: absolute;
                
            }
            .navigationreports li > .has-ul:before{
                 content: "\f105" !important;
            }*/
            /*.navigation li.active > .has-ul:after {
    -webkit-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    -o-transform: rotate(90deg);
    transform: rotate(90deg);
}*/



           .timesheet_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            font-style: normal;
            text-decoration: none !important;
            font-size: 175% !important;

        }
        .timesheet_icon:before 
        {
           content: "\f133" !important;
        }
        .creditcard_icon{
             position: relative;
            font-family: FontAwesome !important;
            font-style: normal;
            text-decoration: none !important;
            font-size: 175% !important;
        }
        .creditcard_icon:before{
             content: "\f155" !important;
        }
        .users_icon{
             position: relative;
            font-family: FontAwesome !important;
            font-style: normal;
            text-decoration: none !important;
            font-size: 175% !important;
        }
        .users_icon:before{
            content: "\f0c0" !important;
        }
        .new_icon{
             position: relative;
            font-family: FontAwesome !important;
            font-style: normal;
            text-decoration: none !important;
            font-size: 175% !important;
            color: #6cb5c9 !important;
        }
        .new_icon:before{
            content: "\f067" !important;
        }
        .search_icon{
            position: relative;
            font-family: FontAwesome !important;
            font-style: normal;
            text-decoration: none !important;
            font-size: 150% !important;
           background-color: #FFFFFF;
        }
        .search_icon:before{
            content: "\f002" !important;
                padding-right: 3px;
        }
        .searchbox {
            width: 123px;
            font-size: 16px;
            color: #333333;
            border: 0px;
        }
        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f1f1f1;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
            list-style:none;
            padding: 0px;
            text-align:initial;
            /*left:50px;*/
            margin-left:44.3px;
        }
            .dropdown-content a:first-child {
                color: #FFFFFF !important;
                font-size: 155%;
                padding-bottom: 7px;
                padding-top: 7px;
                    height: 35px;
            }
             @-moz-document url-prefix(){
            .dropdown-content a:first-child{
                color: #FFFFFF !important;
                font-size: 155%;
                padding-bottom: 30px;
                padding-top: 10px;
                    height: 35px;
            }
        }
        .dropdown-content a:nth-child(2){
            color: #bfbfbf !important;
        }
        .dropdown-content  a{
            color:#333333 !important;
            display:block;
            font-size: 15px;
            padding-left:5px;
            padding-bottom:3px;
            padding-top:3px;
        }
        /*.dropdown-content a:hover {
                background-color: #ddd;
            }*/
        .dropdown-content2 {
            max-height: 400px;
            overflow-y: auto;
            text-align: left;
        }
        .dropdown-content2 a{
            
            border-top:  1.5px solid #ddd;
        }
            .dropdown-content2 a:first-child {
                color: #333333 !important;
                display: block;
                font-size: 15px;
                padding-left: 5px;
                padding-bottom: 3px;
                padding-top: 3px;
                border-top: 0px;
                height: auto;
            }
         .dropdown-content2 a:nth-child(2){
             color:#333333 !important;
            display:block;
            font-size: 15px;
            padding-left:5px;
            padding-bottom:3px;
            padding-top:3px;
        }
        .dropdown-content2 a:hover {
                background-color: #ddd;
            }
        .dropdownreportDescritpion{
            display:none;
        }
        .dropdown-content2 a:hover .dropdownreportDescritpion{
            display:block;
        }

        .dropbtn:hover .dropdown-content {
            display: block;
            
        }

        .dropbtn{
            padding-top:7px;
            padding-bottom:7px;
            text-align:center;
        }
        .dropbtn:hover{
            background-color:#4CAF50;
            border-right-color:#4CAF50;
        }
        .dropbtn:hover a{
            color:#FFFFFF;
        }
         
        .dropbtn a{
            color:#333333;
        }
         .dropbtn a:hover{
             color:#FFFFFF;
         }
 
        .Entirelist{
            list-style:none;
            padding: 0 0 0 0 ;
            /*position:relative;*/
        }
        .reportheadingtext {
            background-color: #4CAF50;
           text-align:left;
        }
        .reportheadingtext:hover{
            color: #FFFFFF !important;
             background-color: #4CAF50 !important;
        }
       .newreport{
             text-align:center;
             padding-bottom:7px;
             padding-top:7px;
       }
       .newreport:hover{
           background-color:#cfcfcf;
       }
        .newreport:hover a {
            color: #61a3b5 !important;
        }

        .newreport .tooltiptext {
            visibility: hidden;
            width: 120px;
            background-color: #4CAF50;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            position: absolute;
            z-index: 1;
            /*top: 113px;*/
            margin-left:35.5px;
        }
            .newreport .tooltiptext::after {
                content: "";
                position: absolute;
                top: 50%;
                right: 100%;
                margin-top: -5px;
                border-width: 5px;
                border-style: solid;
                border-color: transparent #4CAF50 transparent transparent;
            }
            .newreport:hover .tooltiptext{
                visibility: visible;
            }
        .dropdown-content2 a .tooltiptext {
            visibility: hidden;
            /*width: 120px;*/
            background-color: #4CAF50;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            position: absolute;
            z-index: 1;
            /*top: 50px;*/
            margin-left: 156px;
        }
             .dropdown-content2 a .tooltiptext::after {
                content: "";
                position: absolute;
                top: 50%;
                right: 100%;
                margin-top: -5px;
                border-width: 5px;
                border-style: solid;
                /*border-color: transparent #4CAF50 transparent transparent;*/
                 border-color: transparent transparent transparent transparent;
            }


              .dropdown-content2 a:hover .tooltiptext{
                visibility: visible;
            }
            .treelisttext{
                font-size:16px;
            }

        .listboxstyle {
            width: 100% !important;
        }

        .dxlbd {
            webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            width: 100% !important;
        }
        .reportDescriptionStyle{
            width:100%;
        
        }
        .dxpc-headerText{
            padding-top:8px !important;
        }
        .headerstyle{
            padding-bottom: 14.5px !important;
            padding-left: 15px !important;
            padding-right: 15px !important;
            padding-top: 14.5px !important;
        }
        .pagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #333333 !important;
            text-decoration: none !important;
            padding-left: 7px !important;
            padding-right: 7px !important;
        }
        .pagenumber:hover{
            background-color : #333333;
            color: #FFFFFF !important;
        }
        .currentpagenumber {
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style: solid;
            color: #FFFFFF !important;
            background-color: #333333;
            text-decoration: none !important;
        }

        .dxtvControl .dxtv-btn {
            margin-left: 10px;
            margin-top: 7px;
        }
        .dxeButtonEditButton{
            background: none !important;
            border: none !important;
        }
        .firsttopvalue{
            top: 127.8px;
        }
        @-moz-document url-prefix(){
            .firsttopvalue{
                top:131.5px;
            }
        }
          .secondtopvalue{
            top: 162.6px;
        }
        @-moz-document url-prefix(){
            .secondtopvalue{
                top: 171.5px;
            }
        }
        .thirdtopvalue{
            top: 197.8px;
        }
         @-moz-document url-prefix(){
            .thirdtopvalue{
                top:210.8px;
            }
        }
    </style>

    <script>
        var ModifyButtonPressed; //1 (Modify) | 0 (New)

        function donothing(s, e) {
            e.stopPropagation();
        }
        function changereport(e) {
           // var e = window.event || e;
            e.stopPropagation();
            var fullid = e.target.id;
            var parts = fullid.split("|");
            var group = parts[1];
            var reportid = parts[2];
            var reportName = parts[3];
            console.log(reportid);
            console.log(reportName);
            ReportPanel.PerformCallback("DisplayReport," + reportid + "," + reportName);
            ReportOptions.PerformCallback();
            CustomizePDFPanel.PerformCallback("SetInitialPDFValues");
        }
        function NewReportClicked(s, e) {
            ModifyButtonPressed = 0;
            ReportPanel.PerformCallback("PopUpNewReport");
            ReportOptions.PerformCallback();
        }
        function ModifyreportClicked(s, e) {
            ModifyButtonPressed = 1;
            ReportPanel.PerformCallback("ModifyReport");
            ReportOptions.PerformCallback();
        }
        function ExportPDFBtnClicked(s, e) {
            ReportPanel.PerformCallback("ExportToPDF");
            ReportOptions.PerformCallback();
        }
        function DeletereportClicked(s, e) {
            if (confirm("Are you sure you want to delete the report?") == true) {
                ReportPanel.PerformCallback("DeleteReport");
                ReportOptions.PerformCallback('Hide');
            }
        }
  
        function submitnewreportpressed(s, e) {
            
            //ColumnPopUp.Hide();
            if (ASPxClientEdit.ValidateGroup('popupValidationGroup')) {
                NewReportPopUp.Hide();
            FilterPopup.Hide();
                ReportPanel.PerformCallback("SaveNewReport" + ModifyButtonPressed + "," + rowPositions.join("~"));
                ReportOptions.PerformCallback();
            }

        }
        function NextPressed(s, e) {
            //ReportPanel.PerformCallback("ShowFilters");
            if (ASPxClientEdit.ValidateGroup('popupValidationGroup')) {
                FilterPanel.PerformCallback("ShowFilters" + ModifyButtonPressed);
            }
        }
        
        function Next2Pressed(s, e) {
            ColumnPanel.PerformCallback("ShowColumnsOrder");
        }

        function updateText(s, e) {

            var listid = s.GetMainElement().id;
            //split
            var es = listid.split("|");
            var ids = es[1].split(",");
            var id = ids[0] + "," + ids[1] + "," + ids[2] + "," + ids[3] + "," + ids[4];
            var checkComboBox = ASPxClientControl.GetControlCollection().GetByName(id);
            var selectedValues = s.GetSelectedValues();
            if (e.index == 0) {
                if (selectedValues[0] == "SelectAll") {
                    s.SelectAll();
                    checkComboBox.SetText("All Selected");
                }
                else {
                    s.UnselectAll();
                    checkComboBox.SetText("");
                }
            }
            else {
                var x = new Array(); x[0] = 0;
                s.UnselectIndices(x);
                selectedValues = s.GetSelectedValues();
                checkComboBox.SetText(selectedValues);
            }
            selectedValues = s.GetSelectedValues();
            var idcall = s.GetMainElement().id;
            ValuePanel.PerformCallback(idcall + "," + selectedValues);

        }
        function IntBoxValueChanged(s, e) {
            var id = s.GetMainElement().id;
            ValuePanel.PerformCallback(id + "," + s.GetText().toString());
        }
        function DateEditDateChanged(s, e) {
            var id = s.GetMainElement().id;
            var date = s.GetDate();
            console.log(date);
            if (date == null) {
                ValuePanel.PerformCallback(id + "," + "NoDate");
            }
            else {
                var year = date.getFullYear();
                var month = date.getMonth() + 1;

                if (month.toString().length == 1)
                    month = "0".concat(month);

                var day = date.getDate();

                if (day.toString().length == 1) { day = "0".concat(day); }
                ValuePanel.PerformCallback(id + "," + day + "/" + month + "/" + year);
            }
        }
        function TextBoxValueChanged(s, e) {
            var id = s.GetMainElement().id;
            ValuePanel.PerformCallback(id + "," + s.GetText().toString());
        }
        function CheckBoxValueChanged(s, e) {
            var id = s.GetMainElement().id;
            ValuePanel.PerformCallback(id + "," + s.GetValue());
        }
        function AddClicked(s, e) {
            MoveSelectedItems(lbAvailable, lbChoosen);
            UpdateButtonState();
            GridPanel.PerformCallback();
            //ColumnPanel.PerformCallback("AddClicked");
        }
        function AddAllClicked(s, e) {
            MoveAllItems(lbAvailable, lbChoosen);
            UpdateButtonState();
            GridPanel.PerformCallback();
            //ColumnPanel.PerformCallback("UpdateGrid");
        }
        function RemoveClicked(s, e) {
            MoveSelectedItems(lbChoosen, lbAvailable);
            UpdateButtonState();
            GridPanel.PerformCallback();
            //ColumnPanel.PerformCallback("UpdateGridandUpdateListsLbChoosen");
        }
        function RemoveAllClicked(s, e) {
            MoveAllItems(lbChoosen, lbAvailable);
            UpdateButtonState();
            GridPanel.PerformCallback();
            //ColumnPanel.PerformCallback("UpdateGrid");
        }
        function MoveSelectedItems(srcListBox, dstListBox) {
            srcListBox.BeginUpdate();
            dstListBox.BeginUpdate();
            var items = srcListBox.GetSelectedItems();
            for (var i = items.length - 1; i >= 0; i = i - 1) {
                dstListBox.AddItem(items[i].text, items[i].value);
                srcListBox.RemoveItem(items[i].index);
            }
            srcListBox.EndUpdate();
            dstListBox.EndUpdate();
        }
        function MoveAllItems(srcListBox, dstListBox) {
            srcListBox.BeginUpdate();
            var count = srcListBox.GetItemCount();
            for (var i = 0; i < count; i++) {
                var item = srcListBox.GetItem(i);
                dstListBox.AddItem(item.text, item.value);
            }
            srcListBox.EndUpdate();
            srcListBox.ClearItems();
        }
        function UpdateButtonState() {
            btnMoveAllItemsToRight.SetEnabled(lbAvailable.GetItemCount() > 0);
            btnMoveAllItemsToLeft.SetEnabled(lbChoosen.GetItemCount() > 0);
            btnMoveSelectedItemsToRight.SetEnabled(lbAvailable.GetSelectedItems().length > 0);
            btnMoveSelectedItemsToLeft.SetEnabled(lbChoosen.GetSelectedItems().length > 0);
        }

        //function InitalizejQuery() {
        //    $('.lbItem').parent().addClass('lbRow');
        //    $('.lbRow').draggable({
        //        helper: 'clone'
        //    });
        //    $('.lbRow').droppable({
        //        activate: function (ev, ui) {
        //            $(lbChoosen.GetMainElement()).addClass("hover");
        //        },
        //        deactivate: function (ev, ui) {
        //            $(lbChoosen.GetMainElement()).removeClass("hover");
        //        },
        //        drop: function (ev, ui) {
        //            var sourceIndex = $(ui.draggable).index();
        //            var targetIndex = $(this).index();
        //            var text = lbChoosen.GetItem(sourceIndex).text;
        //            var value = lbChoosen.GetItem(sourceIndex).value;
        //            lbChoosen.RemoveItem(sourceIndex);
        //            lbChoosen.InsertItem(targetIndex, text, value);
        //            InitalizejQuery();
        //        }
        //    });
        //}
        var rowPositions;
        function InitalizejQuery() {
            
            $(".all-slides").sortable({
                tolerance: 'touch',
                axis: "y",
                scroll: true,
                scrollSensitivity: 30,
                scrollSpeed: 22,
                stop: function (e, ui) {
                    rowPositions = $('.all-slides').sortable('toArray');
                   // console.log($('.all-slides').sortable('toArray'));
                },
                create: function (e, ui) {
                    rowPositions = $('.all-slides').sortable('toArray');
                     //console.log($('.all-slides').sortable('toArray'));
                }
                //start: function (e, ui) {
                //    ui.item.show().addClass('original-placeholder');
                //}
                //helper: function () {return $('<label>Report Name</label>') },
                
                

            });
        }
        var footerposition;
        function InitalizejQueryDragXY() {
            $(".footer-slides").sortable({
                tolerance: 'touch',
                axis: "x",
                scroll: true,
                scrollSensitivity: 30,
                scrollSpeed: 22,
                stop: function (e, ui) {
                    footerposition = $('.footer-slides').sortable('toArray');
                    FooterOrder.Set("FooterOrder", footerposition.join("~"));
                    // console.log($('.all-slides').sortable('toArray'));
                },
                create: function (e, ui) {
                    footerposition = $('.footer-slides').sortable('toArray');
                     FooterOrder.Set("FooterOrder", footerposition.join("~"));
                    //console.log($('.all-slides').sortable('toArray'));
                }
       
            });
        }
        function ComboBoxGroupByIndexChanged(s, e) {
            var value = s.GetText().toString();
            ReportPanel.PerformCallback("GroupbyChanged," + value);
            ReportOptions.PerformCallback();
        }
        function DateFromChanged(s, e) {
            var id = s.GetMainElement().id;
            var date = s.GetDate();
            if (date == null) {
                ReportPanel.PerformCallback("DateFromChanged" + "," + "NoDate");
                ReportOptions.PerformCallback();
            }
            else {
                var year = date.getFullYear();
                var month = date.getMonth() + 1;
                if (month.toString().length == 1) month = "0".concat(month);
                var day = date.getDate();
                if (day.toString().length == 1) { day = "0".concat(day); }
                ReportPanel.PerformCallback("DateFromChanged" + "," + month + "/" + day + "/" + year);
                ReportOptions.PerformCallback();
            }
        }
        function DateToChanged(s, e) {
            var id = s.GetMainElement().id;
            var date = s.GetDate();
            if (date == null) {
                ReportPanel.PerformCallback("DateToChanged" + "," + "NoDate");
                ReportOptions.PerformCallback();
            }
            else {
                var year = date.getFullYear();
                var month = date.getMonth() + 1;
                if (month.toString().length == 1) month = "0".concat(month);
                var day = date.getDate();
                if (day.toString().length == 1) { day = "0".concat(day); }
                ReportPanel.PerformCallback("DateToChanged" + "," + month + "/" + day + "/" + year);
                ReportOptions.PerformCallback();
            }
        }
        function CustomizePDFClicked(s, e) {
            if (CompanyNamePDFTextBox.GetText().toString() == "" && ReportTitlePDFTextBox.GetText().toString() == "" && DatePDFTextBox.GetText().toString() == "") CustomizePDFPanel.PerformCallback("CustomizePDF");
            else {
                var cName = CompanyNamePDFTextBox.GetText().toString();
                //var cCheck = CheckBoxCompanyNamePDF.GetValue().toString();
                var cCheck = "true";
                var rName = ReportTitlePDFTextBox.GetText().toString();
                //var rCheck = CheckBoxReportTitlePDF.GetValue().toString();
                var rCheck = "true";
                var dName = DatePDFTextBox.GetText().toString();
                //var dCheck = CheckBoxDatePDF.GetValue().toString();
                var dCheck = "true";
                var eName = EmployeeNamePDFTextBox.GetText().toString();
                var fromtoName = FromToDatePDFTextBox.GetText().toString();

                CustomizePDFPanel.PerformCallback("CustomizePDF|" + cName + "|" + cCheck + "|" + rName + "|" + rCheck + "|" + dName + "|" + dCheck + "|" + eName + "|" + fromtoName);
            }
        }
        function SubmitCustomPDFButtonPressed(s, e) {
            CustomizePDFPanel.PerformCallback("PDFHeaderFooterChanged");
        }
        function RestoreDefaultPDFButtonPressed(s, e) {
            CustomizePDFPanel.PerformCallback("SetInitialPDFValues");
        }
        function myFunction(n) {
            var input, filter, ul, li, a, i;
            input = document.getElementsByClassName("myInput")[n];
            filter = input.value.toUpperCase();
            ul = document.getElementsByClassName("dropdown-content2")[n];
            a = ul.getElementsByTagName("a");
            for (i = 0; i < a.length; i++) {
                //a = li[i].getElementsByTagName("a")[0];
                if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
                    a[i].style.display = "";
                } else {
                    a[i].style.display = "none";
                }
            }
        } 
        function GroupBySelectionChanged(s, e) {
            var selectedValues = s.GetSelectedValues();
           
            checkComboBox.SetText(getSelectedItemsText(s.GetSelectedItems()));
            //perform call back
            // ReportPanel.PerformCallback("GroupbyChanged," + selectedValues);
        }
        function getSelectedItemsText(items) {
            var texts = [];
            for (var i = 0; i < items.length; i++) 
                    texts.push(items[i].text);
            return texts.join(";");
        }
        
    </script>
   <%-- <div class="container">--%>
  
        <dx:ASPxCallbackPanel ID="ReportPanel" runat="server" OnCallback="ReportPanel_CallBack" ClientInstanceName="ReportPanel" Width="100%" Collapsible="false" SettingsLoadingPanel-Enabled="false">
            <PanelCollection>
                <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                     <div class="flex" style="height:calc(100vh - 185px);">
                    <div class="ReportTitlePanel" style="background-color: #e6e6e6;">

                        <ul class="Entirelist">
                            <li class="newreport" onclick="NewReportClicked()">
                                <span class="tooltiptext">New Report</span>
                                <a class="new_icon"></a>
                            </li>
                            <li class="dropbtn">
                                <a class="timesheet_icon"></a>
                                <div class="dropdown-content firsttopvalue" >
                                    <a class="reportheadingtext">Time Sheet</a>
                                    <a class="search_icon">
                                        <input type="text" class="searchbox myInput" onkeyup="myFunction(0)" placeholder="Search.." /></a>
                                    <div id="TimeSheetUl" class="dropdown-content2" runat="server">
                                        <%-- <a>Approve Time Sheets</a>--%>
                                    </div>
                                </div>
                            </li>
                            <li class="dropbtn">
                                <a class="creditcard_icon"></a>
                                <div class="dropdown-content secondtopvalue">
                                    <a class="reportheadingtext">Financial</a>
                                    <a class="search_icon">
                                        <input type="text" class="searchbox myInput" onkeyup="myFunction(1)" placeholder="Search.." /></a>
                                    <div id="FinancialUl" class="dropdown-content2" runat="server"></div>
                                </div>
                            </li>
                            <li class="dropbtn">
                                <a class="users_icon"></a>
                                <div class="dropdown-content thirdtopvalue">
                                    <a class="reportheadingtext">Management</a>
                                    <a class="search_icon">
                                        <input type="text" class="searchbox myInput" onkeyup="myFunction(2)" placeholder="Search.." /></a>
                                    <div id="ManagementUl" class="dropdown-content2" runat="server"></div>
                                </div>
                            </li>
                        </ul>

                    </div>
                    <div class="ReportPanel">

                        <table style="width: 100%;">
                           <%-- <tr>
                                
                                
                                <td style="text-align: center;">
                                    <h3><span id="reportNameTitle" runat="server"></span></h3>
                                </td>
                            </tr>--%>
                            <tr style="width:100%;">
                                <td style="padding-left:10px;">
                                    <h5><span id="reportDescription" runat="server"></span></h5>
                                </td>
                            </tr>
                        </table>
                        <table style="width: 100%; padding-top: 7px; padding-bottom: 7px;">
                            <tr>
                                <td style="width: 20%">
                                    <dx:ASPxComboBox runat="server" Visible="false" ID="ComboBoxGroupBy" ClientInstanceName="ComboBoxGroupBy" DropDownStyle="DropDownList" Width="100%"
                                        CaptionCellStyle-Paddings-PaddingLeft="4px"
                                        CaptionCellStyle-Paddings-PaddingTop="10px" Caption="Group By"
                                        CssClass="TextBoxEdit"
                                        TextField="GroupName" ValueField="GroupValue" ItemStyle-Wrap="True">
                                        <DropDownButton>
                                            <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                                        </DropDownButton>
                                        <ClientSideEvents SelectedIndexChanged="ComboBoxGroupByIndexChanged" />
                                    </dx:ASPxComboBox>

                                    <dx:ASPxDropDownEdit ClientInstanceName="checkComboBox" Visible="false" CssClass="TextBoxEdit" Caption="Group By" ID="checkComboBox" Width="100%" runat="server" AnimationType="Fade"
                                        CaptionCellStyle-Paddings-PaddingTop="10px" CaptionCellStyle-Paddings-PaddingLeft="7px">
                                        <DropDownButton>
                                            <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                                        </DropDownButton>
                                        <DropDownWindowStyle BackColor="#EDEDED" />
                                        <DropDownWindowTemplate>
                                            <dx:ASPxListBox Width="100%" ID="listBoxGroupBy" ClientInstanceName="listBoxGroupBy" SelectionMode="CheckColumn"
                                                runat="server">

                                                <Border BorderStyle="None" />
                                                <BorderBottom BorderStyle="Solid" BorderWidth="1px" BorderColor="#DCDCDC" />
                                                <ClientSideEvents SelectedIndexChanged="GroupBySelectionChanged" />
                                            </dx:ASPxListBox>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="padding: 4px">
                                                        <dx:ASPxButton ID="ASPxButton1" AutoPostBack="False" runat="server" Text="Close" Style="float: right">
                                                            <ClientSideEvents Click="function(s, e){ checkComboBox.HideDropDown(); }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </DropDownWindowTemplate>
                                    </dx:ASPxDropDownEdit>
                                </td>
                                <td style="width: 20%">
                                    <dx:ASPxDateEdit ID="ASPxDateFrom" runat="server" OnCalendarDayCellPrepared="ASPXDateEdit_CustomCell" Width="100%" Visible="false"
                                        Caption="From Date" CssClass="TextBoxEdit" CaptionCellStyle-Paddings-PaddingTop="10px" PropertiesDateEdit-DisplayFormatString="dd/MM/yyyy"
                                        CaptionCellStyle-Paddings-PaddingLeft="4px">
                                        <DropDownButton>
                                            <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                                        </DropDownButton>
                                        <ClientSideEvents DateChanged="DateFromChanged" />
                                    </dx:ASPxDateEdit>
                                </td>
                                <td style="width: 20%">
                                    <dx:ASPxDateEdit ID="ASPxDateTo" runat="server" OnCalendarDayCellPrepared="ASPXDateEdit_CustomCell" Width="100%" Visible="false"
                                        Caption="To Date" CssClass="TextBoxEdit" CaptionCellStyle-Paddings-PaddingTop="10px" PropertiesDateEdit-DisplayFormatString="dd/MM/yyyy"
                                        CaptionCellStyle-Paddings-PaddingLeft="4px">
                                        <DropDownButton>
                                            <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                                        </DropDownButton>
                                        <ClientSideEvents DateChanged="DateToChanged" />
                                    </dx:ASPxDateEdit>

                                </td>
                                <td style="width: 20%">
                                    <dx:ASPxButton ID="DeleteBtn2" RenderMode="Link" CssPostfix="none" CssClass="delete_icon" runat="server" AutoPostBack="false" ToolTip="Delete Report" Visible="false">
                                        <ClientSideEvents Click="DeletereportClicked" />
                                    </dx:ASPxButton>
                                    <dx:ASPxButton ID="ModifyBtn2" RenderMode="Link" CssPostfix="none" CssClass="ModifyReport_icon" runat="server" AutoPostBack="false" ToolTip="Modify Report" Visible="false">
                                        <ClientSideEvents Click="ModifyreportClicked" />
                                    </dx:ASPxButton>
                                </td>

                            </tr>
                        </table>

                        <%--Report chosen will be placed inside this div--%>
                        <div id="ReportDiv" runat="server"  style="overflow-y: auto;height:calc(100vh - 252px);">

                            <dx:ASPxGridView ID="ASPxGridViewReport" runat="server" Visible="false" SettingsBehavior-AllowDragDrop="false" ClientInstanceName="ASPxGridViewReport" Width="100%" AutoPostBack="true" CssClass="newFont"
                                Border-BorderColor="#dbdbdb" OnCustomGroupDisplayText="ASPxGridViewReport_CustomGroupDisplayText">
                                
                                <Images>
                                    <CollapsedButton  Url="../Content/Images/iconmonstr-plus-2-12.png" Height="12px" Width="12px"></CollapsedButton>
                                    <ExpandedButton Url="../Content/Images/iconmonstr-minus-2-12.png" Height="12px" Width="12px"></ExpandedButton>
                                </Images>
                                <SettingsPager CurrentPageNumberFormat="{0}">
                    </SettingsPager>
                    <StylesPager>
                        <PageNumber CssClass="pagenumber"></PageNumber>
                        <CurrentPageNumber CssClass="currentpagenumber"></CurrentPageNumber>
                    </StylesPager>
                                <SettingsDetail ExportMode="All" />

                                <Settings ShowTitlePanel="true" />
                               
                                <SettingsEditing EditFormColumnCount="4" Mode="Inline" />
                                <Styles>
                                    <GroupRow HorizontalAlign="Left" CssClass="GroupRow"></GroupRow>
                                    <AlternatingRow Enabled="true" />
                                    <Header HorizontalAlign="Center"></Header>
                                </Styles>

                                <SettingsPopup>
                                    <EditForm Width="100%" Modal="false" />
                                </SettingsPopup>

                                <SettingsPager PageSize="50" />
                                <Paddings Padding="0px" />
                                <Border BorderWidth="0px" />
                                <BorderBottom BorderWidth="1px" />
                                <Settings ShowFooter="True" />
                                <Styles Header-Wrap="True" />
                                <Columns>
                                    <%--Insert Columns based on report Columns--%>
                                </Columns>
                            </dx:ASPxGridView>
                            <dx:ASPxGridViewExporter ID="GridExport" runat="server" GridViewID="ASPxGridViewReport" ExportSelectedRowsOnly="false" OnRenderBrick="GridExport_RenderBrick">
                            </dx:ASPxGridViewExporter>
                        </div>
                       

                        <dx:ASPxPopupControl ID="NewReportPopUpControl"
                            HeaderText="New Report" HeaderStyle-BackColor="#37474F" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-ForeColor="White"
                             HeaderStyle-CssClass="headerstyle"
                            HeaderStyle-Font-Size="Large" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" runat="server"
                            ClientInstanceName="NewReportPopUp"
                            CssClass="popupanimation"
                            EnableCallbackAnimation="true"
                            PopupAnimationType="Auto"
                            ShowCloseButton="true"
                            CloseButtonImage-Width="0px"
                            CloseButtonImage-Height="0px"
                            CloseButtonStyle-CssClass="close_icon">
                            <ContentCollection>

                                <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" Width="100%">
                                    <div class="container">
                                        <div class="row" style="padding-left: 15%;">
                                            <div class="col-10">
                                                <label>Report Name</label>
                                            </div>
                                            <div class="col-25">
                                                <dx:ASPxTextBox Width="100%" CssClass="TextBox" MaxLength="255" ID="ReportNameTextBox" ClientInstanceName="ReportNameTextBox" runat="server" NullText="Report Name">
                                                    <%-- <ClientSideEvents KeyUp="checktextbox" />--%>
                                                    <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                                        <RequiredField IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                            <div class="col-10"></div>
                                            <div class="col-10">
                                                <label>Report Tag</label>
                                            </div>
                                            <div class="col-25">
                                                <dx:ASPxComboBox runat="server" ID="ASPxComboBoxReportTag" ClientInstanceName="ASPxComboBoxReportTag" DropDownStyle="DropDownList" Width="100%"
                                                    CssClass="TextBox"
                                                    TextField="Tag" ValueField="Id" ItemStyle-Wrap="True">
                                                    <DropDownButton>
                                                        <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                                                    </DropDownButton>
                                                    <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                                        <RequiredField IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row" style="padding-left: 15%;">
                                            <%--Multiple Tree List--%>
                                            <div class="col-25">
                                                <label for="company">Please select the data needed to create the report</label>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left: 15%;">
                                            <div class="col-10">
                                                <%--Employee Tree list--%>
                                                <dx:ASPxTreeView ID="ASPxtreeViewEmployee" runat="server" AllowCheckNodes="true" AllowSelectNode="true" ClientInstanceName="ASPxtreeViewEmployee" CheckNodesRecursive="true">
                                                    <%--<ClientSideEvents CheckedChanged="treeViewCheckedChanged" />--%>
                                                    <Images>
                                                        <CheckBoxChecked Url="../Content/Images/iconmonstr-checkbox-4-16.png" Height="16px" Width="16px"></CheckBoxChecked>
                                                        <CheckBoxUnchecked Url ="../Content/Images/iconmonstr-square-4-16.png" Height="16px" Width="16px"></CheckBoxUnchecked>
                                                        <CollapseButton  Url="../Content/Images/iconmonstr-minus-4-16.png" Height="16px" Width="16px"></CollapseButton>
                                                        <ExpandButton  Url="../Content/Images/iconmonstr-plus-5-16.png"  Height="16px" Width="16px"></ExpandButton>
                                                        <CheckBoxGrayed  Url="../Content/Images/iconmonstr-checkbox-17-16.png"  Height="16px" Width="16px"></CheckBoxGrayed>
                                                    </Images>
                                                    <Styles>
                                                        <NodeText CssClass="treelisttext"></NodeText>
                                                    </Styles>
                                                    <Nodes>
                                                        <dx:TreeViewNode Name="Employee,null,null,null,null" Text="Employee">
                                                            <%--Employee ID--%>
                                                            <Nodes>
                                                                <dx:TreeViewNode Name="Employee,EmployeeID,DropDownList,null,null" Text="Employee Name"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="Employee,EmpTitle,DropDownList,null,null" Text="Title"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="Employee,EmpDepartment,DropDownList,null,null" Text="Discipline"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="Employee,EmpCostRate,IntBox,null,null" Text="Hourly Cost Rate"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="Employee,EmpEmail,DropDownList,null,null" Text="Email"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="Employee,EmpManager,DropDownList,null,null" Text="Employee Manager"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="UDF,UDF1,Calendar,null,null" Text="Passport Expiry"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="UDF,UDF2,Calendar,null,null" Text="Emirates ID"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="UDF,UDF3,Calendar,null,null" Text="Labor Card"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="UDF,UDF4,Calendar,null,null" Text="AC. Card"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="Employee,EmpNextImpDate,Calendar,null,null" Text="Visa Expiry"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="OMV_EmployeeSalary,TotalSalary,IntBox,null,null" Text="Total Salary"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="OMV_EmployeeSalary,BasicSalary,IntBox,null,null" Text="Basic Salary"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="OMV_EmployeeSalary,Transportation,IntBox,null,null" Text="Transportation"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="OMV_EmployeeSalary,Housing,IntBox,null,null" Text="Housing"></dx:TreeViewNode>
                                                            </Nodes>
                                                        </dx:TreeViewNode>
                                                    </Nodes>
                                                </dx:ASPxTreeView>
                                            </div>
                                            <div class="col-10"></div>
                                            <div class="col-10">
                                                <%--Project Tree List--%>
                                                <dx:ASPxTreeView ID="ASPxtreeProject" runat="server" AllowCheckNodes="true" AllowSelectNode="true" ClientInstanceName="ASPxtreeViewEmployee" CheckNodesRecursive="true">
                                                    <%--<ClientSideEvents CheckedChanged="treeViewCheckedChanged" />--%>
                                                     <Images>
                                                        <CheckBoxChecked Url="../Content/Images/iconmonstr-checkbox-4-16.png" Height="16px" Width="16px"></CheckBoxChecked>
                                                        <CheckBoxUnchecked Url ="../Content/Images/iconmonstr-square-4-16.png" Height="16px" Width="16px"></CheckBoxUnchecked>
                                                        <CollapseButton  Url="../Content/Images/iconmonstr-minus-4-16.png" Height="16px" Width="16px"></CollapseButton>
                                                        <ExpandButton  Url="../Content/Images/iconmonstr-plus-5-16.png"  Height="16px" Width="16px"></ExpandButton>
                                                        <CheckBoxGrayed  Url="../Content/Images/iconmonstr-checkbox-17-16.png"  Height="16px" Width="16px"></CheckBoxGrayed>
                                                    </Images>
                                                    <Styles>
                                                        <NodeText CssClass="treelisttext"></NodeText>
                                                    </Styles>
                                                    <Nodes>
                                                        <dx:TreeViewNode Name="Project,null,null,null,null" Text="Project">
                                                            <Nodes>
                                                                <dx:TreeViewNode Name="Project,ProjectID,DropDownList,null,null" Text="Project ID"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="Project,ProjectName,DropDownList,null,null" Text="Project Name"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="Project,ProjectStartDate,Calendar,null,null" Text="Project Start Date"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="Project,ProjectPONum,DropDownList,null,null" Text="Project PO Number"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="ProjectDetails,Manager,DropDownList,null,null" Text="Project Manager"></dx:TreeViewNode>
                                                            </Nodes>
                                                        </dx:TreeViewNode>
                                                    </Nodes>
                                                </dx:ASPxTreeView>
                                            </div>
                                            <div class="col-10"></div>
                                            <div class="col-10">
                                                <%--Client Tree List--%>
                                                <dx:ASPxTreeView ID="ASPxTreeClient" runat="server" AllowCheckNodes="true" AllowSelectNode="true" ClientInstanceName="ASPxtreeViewEmployee" CheckNodesRecursive="true">
                                                    <%--<ClientSideEvents CheckedChanged="treeViewCheckedChanged" />--%>
                                                     <Images>
                                                        <CheckBoxChecked Url="../Content/Images/iconmonstr-checkbox-4-16.png" Height="16px" Width="16px"></CheckBoxChecked>
                                                        <CheckBoxUnchecked Url ="../Content/Images/iconmonstr-square-4-16.png" Height="16px" Width="16px"></CheckBoxUnchecked>
                                                        <CollapseButton  Url="../Content/Images/iconmonstr-minus-4-16.png" Height="16px" Width="16px"></CollapseButton>
                                                        <ExpandButton  Url="../Content/Images/iconmonstr-plus-5-16.png"  Height="16px" Width="16px"></ExpandButton>
                                                        <CheckBoxGrayed  Url="../Content/Images/iconmonstr-checkbox-17-16.png"  Height="16px" Width="16px"></CheckBoxGrayed>
                                                    </Images>
                                                    <Styles>
                                                        <NodeText CssClass="treelisttext"></NodeText>
                                                    </Styles>
                                                    <Nodes>
                                                        <dx:TreeViewNode Name="Client,null,null,null,null" Text="Client">
                                                            <Nodes>
                                                                <dx:TreeViewNode Name="Client,ClientID,DropDownList,null,null" Text="Client Company Name"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="Client,ClientStreet,DropDownList,null,null" Text="Street" />
                                                                <dx:TreeViewNode Name="Client,ClientCity,DropDownList,null,null" Text="City" />
                                                                <dx:TreeViewNode Name="Client,ClientZip,DropDownList,null,null" Text="Zip/Postal Code" />
                                                                <dx:TreeViewNode Name="Client,ClientPhone,DropDownList,null,null" Text="Phone Number" />
                                                                <dx:TreeViewNode Name="Client,ClientFName,DropDownList,null,null" Text="Representative First Name" />
                                                                <dx:TreeViewNode Name="Client,ClientLName,DropDownList,null,null" Text="Representative Last Name" />
                                                                <dx:TreeViewNode Name="Client,ClientMainSalt,DropDownList,null,null" Text="Representative Main Salutation" />
                                                                <dx:TreeViewNode Name="Client,ClientOther3,DropDownList,null,null" Text="Representative Title" />
                                                            </Nodes>
                                                        </dx:TreeViewNode>
                                                    </Nodes>
                                                </dx:ASPxTreeView>
                                            </div>
                                            <div class="col-10"></div>
                                            <div class="col-10">
                                                <%--TimeSheet Tree List--%>
                                                <dx:ASPxTreeView ID="ASPxTreeTimeSheet" runat="server" AllowCheckNodes="true" AllowSelectNode="true" ClientInstanceName="ASPxtreeViewEmployee" CheckNodesRecursive="true">
                                                    <%--<ClientSideEvents CheckedChanged="treeViewCheckedChanged" />--%>
                                                     <Images>
                                                        <CheckBoxChecked Url="../Content/Images/iconmonstr-checkbox-4-16.png" Height="16px" Width="16px"></CheckBoxChecked>
                                                        <CheckBoxUnchecked Url ="../Content/Images/iconmonstr-square-4-16.png" Height="16px" Width="16px"></CheckBoxUnchecked>
                                                        <CollapseButton  Url="../Content/Images/iconmonstr-minus-4-16.png" Height="16px" Width="16px"></CollapseButton>
                                                        <ExpandButton  Url="../Content/Images/iconmonstr-plus-5-16.png"  Height="16px" Width="16px"></ExpandButton>
                                                        <CheckBoxGrayed  Url="../Content/Images/iconmonstr-checkbox-17-16.png"  Height="16px" Width="16px"></CheckBoxGrayed>
                                                    </Images>
                                                    <Styles>
                                                        <NodeText CssClass="treelisttext"></NodeText>
                                                    </Styles>
                                                    <Nodes>
                                                        <dx:TreeViewNode Name="TimeEntry,null,null,null,null" Text="Time Sheet">
                                                            <Nodes>
                                                                <%--  <dx:TreeViewNode Name="TimeEntry,ApprovalStatus,DropDownList" Text="Approval Status"></dx:TreeViewNode>--%>
                                                                <dx:TreeViewNode Name="TimeEntry,ApprovedBy,DropDownList,null,null" Text="Approved By"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="TimeEntry,Cost,IntBox,SUM,TEHours*TECostRate" Text="Cost"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="TimeEntry,TEHours,IntBox,SUM,TEHours" Text="Hours"></dx:TreeViewNode>
                                                                <dx:TreeViewNode Name="TimeEntry,TEDate,Date,null,null" Text="Date"></dx:TreeViewNode>
                                                            </Nodes>
                                                        </dx:TreeViewNode>
                                                    </Nodes>
                                                </dx:ASPxTreeView>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row" style="margin-left: 15%; margin-right: 15%;">
                                            <div class="reportDescriptionStyle">
                                                <dx:ASPxTextBox ID="ReportDescriptionTextBox" CssClass="TextBox" Width="100%" Height="100%" runat="server" MaxLength="255" NullText="Report Description..."></dx:ASPxTextBox>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <dx:ASPxButton CssClass="CancelButton" ID="CancelButton" Text="Cancel" AutoPostBack="false" UseSubmitBehavior="false" ClientSideEvents-Click="function(s, e) {
                                        NewReportPopUp.Hide();}"
                                                runat="server">
                                            </dx:ASPxButton>
                                            <dx:ASPxButton CssClass="SubmitButton" ID="SubmitButton" Text="Next" AutoPostBack="false" UseSubmitBehavior="true" ClientSideEvents-Click="NextPressed" runat="server"></dx:ASPxButton>

                                        </div>
                                    </div>
                                </dx:PopupControlContentControl>
                            </ContentCollection>
                        </dx:ASPxPopupControl>

                    </div>
                           </div>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
  
         <%--</div>--%>





    <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" OnCallback="FilterPanel_CallBack" ClientInstanceName="FilterPanel" Width="100%" Collapsible="false" SettingsLoadingPanel-Enabled="false">
        <PanelCollection>
            <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                    <dx:ASPxPopupControl ID="ASPxPopupControlFilters"
                    HeaderText="New Report" HeaderStyle-BackColor="#37474F" HeaderStyle-HorizontalAlign="Center"
                    HeaderStyle-ForeColor="White" HeaderStyle-Paddings-Padding="15px"
                    HeaderStyle-Font-Size="Large" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" runat="server"
                    ClientInstanceName="FilterPopup"
                    EnableCallbackAnimation="true"
                    CssClass="scroll"
                    PopupAnimationType="Auto"
                    ShowCloseButton="true"
                    CloseButtonImage-Width="0px"
                    CloseButtonImage-Height="0px"
                    CloseButtonStyle-CssClass="close_icon">
                    <ContentCollection>

                        <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server" Width="100%">
                            <div class="container" id ="popupcontainer">
                                <div class="row" style="padding-left: 15%;">
                                    <div class="col-25">
                                        <label>Please choose report filters and column display names</label>
                                    </div>
                                    <div class="col-55"></div>
                                    <div class="col-10">
                                          <label>Display Column</label>
                                    </div>
                                </div>
                                <%--Add the filters here in C# based on the selected columns--%>
                                 <dx:ASPxGlobalEvents ID="ge" runat="server">
            <ClientSideEvents ControlsInitialized="function (s, e) { InitalizejQuery(); }" />
        </dx:ASPxGlobalEvents>
                                <div id="divFilters" class="all-slides" runat="server"></div>
                                <%--<div id="cloned-slides" class="cloned-slides"></div>--%>
                                <div class="row" style="padding-left: 15%;">
                                    <dx:ASPxButton CssClass="CancelButton" ID="ASPxButton1" Text="Back" AutoPostBack="false" UseSubmitBehavior="false" ClientSideEvents-Click="function(s, e) {
                                        FilterPopup.Hide();}"
                                        runat="server">
                                    </dx:ASPxButton>
                                    <dx:ASPxButton CssClass="SubmitButton" ID="SubmitBttn" ClientInstanceName="SubmitBttn" Text="Create Report" AutoPostBack="false" UseSubmitBehavior="true" ClientSideEvents-Click="submitnewreportpressed" runat="server"></dx:ASPxButton>
                                </div>
                            </div>
                        </dx:PopupControlContentControl>
                    </ContentCollection>
                </dx:ASPxPopupControl>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
 
    <dx:ASPxCallbackPanel ID="ASPxCallbackPanel2" runat="server" OnCallback="ValueChanged_CallBack" ClientInstanceName="ValuePanel" Collapsible="false" SettingsLoadingPanel-Enabled="false">
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">

                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>


    <dx:ASPxCallbackPanel ID="ASPxCallbackPanel5" runat="server" OnCallback="CustomizePDF_CallBack" ClientInstanceName="CustomizePDFPanel" Width="100%" Collapsible="false" SettingsLoadingPanel-Enabled="false">
        <PanelCollection>
            <dx:PanelContent ID="PanelContent7" runat="server" SupportsDisabledAttribute="True">
                <dx:ASPxPopupControl ID="ASPxPopUpCostumizePDF"
                    HeaderText="Customize Export" HeaderStyle-BackColor="#37474F" HeaderStyle-HorizontalAlign="Center"
                    HeaderStyle-ForeColor="White" HeaderStyle-Paddings-Padding="15px"
                    HeaderStyle-Font-Size="Large" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" runat="server"
                    ClientInstanceName="CustomizePDFPopup"
                    EnableCallbackAnimation="true"
                    CssClass="popupanimation"
                    PopupAnimationType="Auto"
                    ShowCloseButton="true"
                    CloseButtonImage-Width="0px"
                    CloseButtonImage-Height="0px"
                    CloseButtonStyle-CssClass="close_icon">
                    <ContentCollection>

                        <dx:PopupControlContentControl ID="PopupControlContentControl4" runat="server" Width="100%">
                            <div class="container">
                                <div class="row">
                                    <label>You can customize footer order by dragging and dropping the field names</label>
                                    <dx:ASPxButton CssClass="RestoreDefaultButton" ID="RestoreDefaultButton" Text="Restore Default" AutoPostBack="false" UseSubmitBehavior="true" ClientSideEvents-Click="RestoreDefaultPDFButtonPressed" runat="server"></dx:ASPxButton>
                                </div>
                                <br />
                                <div style="background-color: white; width: 100%; padding-left: 7%; padding-right: 7%;">
                                    <div class="row">
                                        <div class="col-37point5"></div>
                                       <div class="col-25">
                                            <dx:ASPxTextBox Width="100%" CssClass="TextBoxPDF" MaxLength="50" ID="ASPxTextBoxCompanyNamePDF" ClientInstanceName="CompanyNamePDFTextBox" runat="server" NullText="Empty Heading">
                                            </dx:ASPxTextBox>
                                       </div>
                                        <div class="col-10">
                                           <%-- <dx:ASPxCheckBox ID="CheckBoxCompanyNamePDF" ClientInstanceName="CheckBoxCompanyNamePDF" runat="server" ClientEnabled="true" Visible="false" Checked="true"></dx:ASPxCheckBox>--%>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-37point5"></div>
                                        <div class="col-25">
                                            <dx:ASPxTextBox Width="100%" CssClass="TextBoxPDF" MaxLength="50" ID="ASPxTextBoxReportTitlePDF" ClientInstanceName="ReportTitlePDFTextBox" runat="server" NullText="Empty Heading">
                                            </dx:ASPxTextBox>
                                       </div>
                                        <div class="col-10">
                                           <%-- <dx:ASPxCheckBox ID="CheckBoxReportTitlePDF" ClientInstanceName="CheckBoxReportTitlePDF" runat="server" Visible="false" Checked="true"></dx:ASPxCheckBox>--%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-37point5"></div>
                                      <div class="col-25">
                                            <dx:ASPxTextBox Width="100%" CssClass="TextBoxPDF" MaxLength="50" ID="ASPxTextBoxDatePdf" ClientInstanceName="DatePDFTextBox" runat="server" NullText="Empty Heading">
                                            </dx:ASPxTextBox>
                                       </div>
                                        <div class="col-10">
                                          <%--  <dx:ASPxCheckBox ID="CheckBoxDatePDF" ClientInstanceName="CheckBoxDatePDF" runat="server" Visible="false" Checked="true"></dx:ASPxCheckBox>--%>
                                        </div>
                                    </div>
                                    <br /> <br />
                                    <div class="row">
                                        <%--  Grid HERE of 5 ROWS--%>
                                        <dx:ASPxGridView ID="ASPxGridViewPDF" runat="server" SettingsBehavior-AllowDragDrop="false" Visible="true" ClientInstanceName="ASPxGridViewPDF" Width="100%" AutoPostBack="true" CssClass="newFont"
                                            Border-BorderColor="#dbdbdb">
                                            <SettingsDetail ExportMode="All" />
                                            <SettingsPager CurrentPageNumberFormat="{0}">
                                            </SettingsPager>
                                            <StylesPager>
                                                <PageNumber CssClass="pagenumber"></PageNumber>
                                                <CurrentPageNumber CssClass="currentpagenumber"></CurrentPageNumber>
                                            </StylesPager>
                                            <Settings ShowTitlePanel="true" />

                                            <SettingsEditing EditFormColumnCount="4" Mode="Inline" />
                                            <Styles>
                                                <GroupRow HorizontalAlign="Left" CssClass="GroupRow"></GroupRow>
                                                <AlternatingRow Enabled="true" />
                                                <Header HorizontalAlign="Center"></Header>
                                            </Styles>

                                            <SettingsPopup>
                                                <EditForm Width="100%" Modal="false" />
                                            </SettingsPopup>

                                            <SettingsPager PageSize="50" />
                                            <Paddings Padding="0px" />
                                            <Border BorderWidth="0px" />
                                            <BorderBottom BorderWidth="1px" />
                                            <Settings ShowFooter="True" />
                                            <Styles Header-Wrap="True" />
                                            <Columns>
                                                <%--Insert Columns based on report Columns--%>
                                            </Columns>
                                        </dx:ASPxGridView>
                                    </div>

                                    <div class="row">
                                        <dx:ASPxHiddenField ID="FooterOrder" ClientInstanceName="FooterOrder" runat="server"></dx:ASPxHiddenField>
                                        <dx:ASPxGlobalEvents ID="ASPxGlobalEvents1" runat="server">
                                            <ClientSideEvents ControlsInitialized="function (s, e) { InitalizejQueryDragXY(); }" />
                                        </dx:ASPxGlobalEvents>
                                        <div class="footer-slides">

                                            <div class="col-33point33 slide" id="divfooter1">
                                                <dx:ASPxTextBox Width="80%" CssClass="TextBoxPDFFooter" MaxLength="50" ID="ASPxTextBoxEmployeeNamePDF" ClientInstanceName="EmployeeNamePDFTextBox" runat="server" NullText="Empty Footer">
                                                </dx:ASPxTextBox>
                                            </div>
                                           <%-- <div class="col-10"></div>
                                            <div class="col-2point5"></div>--%>
                                            <div class="col-33point33 slide"  id="divfooter2">
                                                <dx:ASPxTextBox Width="80%" CssClass="TextBoxPDFFooter" MaxLength="50" ID="ASPxTextBoxFromToDatePDF" ClientInstanceName="FromToDatePDFTextBox" runat="server" NullText="Empty Footer">
                                                </dx:ASPxTextBox>
                                            </div>
                                            <%--<div class="col-10"></div>
                                            <div class="col-2point5"></div>--%>
                                            <div class="col-33point33 slide"  id="divfooter3">
                                                <dx:ASPxTextBox Width="80%" CssClass="TextBoxPDFFooter" MaxLength="50" ID="ASPxTextBoxPageNumberPDF" ClientInstanceName="PageNumberPDFTextBox" Enabled="false" runat="server" NullText="Page Number">
                                                </dx:ASPxTextBox>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                 <div class="row">
                                        <dx:ASPxButton CssClass="CancelButton" ID="ASPxButton2" Text="Cancel" AutoPostBack="false" UseSubmitBehavior="false" ClientSideEvents-Click="function(s, e) {
                                        CustomizePDFPopup.Hide();}" runat="server">
                                        </dx:ASPxButton>
                                        <dx:ASPxButton CssClass="SubmitButton" ID="SubmitCustomPDFButton" Text="Submit" AutoPostBack="false" UseSubmitBehavior="true" ClientSideEvents-Click="SubmitCustomPDFButtonPressed" runat="server"></dx:ASPxButton>

                                    </div>
                            </div>
                        </dx:PopupControlContentControl>
                    </ContentCollection>
                </dx:ASPxPopupControl>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>




















    <dx:ASPxCallbackPanel ID="ASPxCallbackPanel3" runat="server" OnCallback="ColumnPanel_CallBack" ClientInstanceName="ColumnPanel" Width="100%" Collapsible="false" SettingsLoadingPanel-Enabled="false">
        <PanelCollection>
            <dx:PanelContent ID="PanelContent3" runat="server" SupportsDisabledAttribute="True">
                 <dx:PanelContent ID="PanelContent5" runat="server" SupportsDisabledAttribute="True">
                    <dx:ASPxPopupControl ID="ASPxPopUpControlColumns"
                    HeaderText="New Report" HeaderStyle-BackColor="#4CAF50" HeaderStyle-HorizontalAlign="Center"
                    HeaderStyle-ForeColor="White" HeaderStyle-Paddings-Padding="15px"
                    HeaderStyle-Font-Size="Large" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" runat="server"
                    ClientInstanceName="ColumnPopUp"
                    EnableCallbackAnimation="true"
                    CssClass="scroll"
                    PopupAnimationType="Auto"
                    ShowCloseButton="true"
                    CloseButtonImage-Width="0px"
                    CloseButtonImage-Height="0px"
                    CloseButtonStyle-CssClass="close_icon">
                    <ContentCollection>
                        <dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server" Width="100%">
                            <div class="container">
                                <div class="row" style="padding-left: 15%;">
                                    <div class="col-25">
                                        <label>Please choose the columns to be displayed</label>
                                    </div>
                                </div>
                                
                                   
                                <div class="row" style="padding-left: 15%;">
                                    <div class="col-25">
                                        <label>Available Columns</label>
                                    </div>
                                    <div class="col-25"></div>
                                     <div class="col-25">
                                        <label>Choosen Columns</label>
                                    </div>
                                </div>
                                <div class="row" style="padding-left: 15%;">
                                    <dx:ASPxGlobalEvents ID="GlobalEvents" runat="server">
                                        <ClientSideEvents ControlsInitialized="function(s, e) { UpdateButtonState(); }" />
                                    </dx:ASPxGlobalEvents>
                                    <div class="col-25">
                                        <dx:ASPxListBox ID="lbAvailable" runat="server" ClientInstanceName="lbAvailable" Width="100%" Height="240px"
                                            SelectionMode="CheckColumn">
                                            <Items>
                                            </Items>
                                             <ClientSideEvents SelectedIndexChanged="function(s, e) { UpdateButtonState(); }" />
                                        </dx:ASPxListBox>
                                    </div>
                                    <div class="col-2point5"></div>
                                    <div class="col-20">
                                        <table style="width:100%;">
                                            <tr>
                                                <td style="text-align:center">
                                                   <dx:ASPxButton Width="60%" CssClass="ColumnChangeButton"  ClientInstanceName="btnMoveSelectedItemsToRight" ID="ASPxButton5" Text="Add >" AutoPostBack="false" UseSubmitBehavior="true" ClientSideEvents-Click="AddClicked" runat="server"></dx:ASPxButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                 <td style="text-align:center">
                                                   <dx:ASPxButton Width="60%" CssClass="ColumnChangeButton" ClientInstanceName="btnMoveAllItemsToRight" ID="ASPxButton6" Text="Add All >>" AutoPostBack="false" UseSubmitBehavior="true" ClientSideEvents-Click="AddAllClicked" runat="server"></dx:ASPxButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:center">
                                                   <dx:ASPxButton Width="60%" CssClass="ColumnChangeButton" ID="ASPxButton7"   ClientInstanceName="btnMoveSelectedItemsToLeft" Text="< Remove" AutoPostBack="false" UseSubmitBehavior="true" ClientSideEvents-Click="RemoveClicked" runat="server"></dx:ASPxButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                 <td style="text-align:center">
                                                   <dx:ASPxButton Width="60%" CssClass="ColumnChangeButton" ID="ASPxButton8"    ClientInstanceName="btnMoveAllItemsToLeft" Text="<< Remove All" AutoPostBack="false" UseSubmitBehavior="true" ClientSideEvents-Click="RemoveAllClicked" runat="server"></dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-2point5"></div>
                                    <div class="col-25">
                                 

                                        <dx:ASPxListBox ID="lbChoosen" runat="server" ClientInstanceName="lbChoosen" Width="100%" Height="240px"
                                            SelectionMode="CheckColumn">
                                            <Items>
                                            </Items>
                                            <ClientSideEvents SelectedIndexChanged="function(s, e) { UpdateButtonState(); }"></ClientSideEvents>
                                        </dx:ASPxListBox>

                                    </div>
                                </div>


                                <%-- Grid that shows how the report will look like + Allows the user to drag and drop columns to change order--%>
                                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel4" runat="server" OnCallback="GridPrev_CallBack" ClientInstanceName="GridPanel" Width="100%" Collapsible="false" SettingsLoadingPanel-Enabled="false">
                                    <PanelCollection>
                                        <dx:PanelContent ID="PanelContent6" runat="server" SupportsDisabledAttribute="True">
                                            <div class="row" style="padding-left: 15%;">
                                                <div class="col-25">
                                                    <label id="Gridlabel1" runat="server" visible="false">Report will look like as follows:</label>
                                                    <label id="Gridlabel2" runat="server" visible="false">You can reorder the columns by dragging them</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <dx:ASPxGridView ID="ASPxGridView1" runat="server" ClientInstanceName="ASPxGridView1" Visible="false"
                                                    Width="100%" AutoPostBack="true" CssClass="newFont" Border-BorderColor="#dbdbdb" OnCustomJSProperties="Grid_CustomJSProperties">
                                                    <SettingsText EmptyDataRow="This is a demo" />
                                                    <Settings ShowTitlePanel="true" />
                                                    <SettingsPager CurrentPageNumberFormat="{0}">
                                                    </SettingsPager>
                                                    <StylesPager>
                                                        <PageNumber CssClass="pagenumber"></PageNumber>
                                                        <CurrentPageNumber CssClass="currentpagenumber"></CurrentPageNumber>
                                                    </StylesPager>
                                                    <SettingsEditing EditFormColumnCount="4" Mode="Inline" />
                                                    <Styles>
                                                        <AlternatingRow Enabled="true" />
                                                        <Header HorizontalAlign="Center"></Header>
                                                    </Styles>

                                                    <SettingsPopup>
                                                        <EditForm Width="100%" Modal="false" />
                                                    </SettingsPopup>

                                                    <SettingsPager PageSize="50" />
                                                    <Paddings Padding="0px" />
                                                    <Border BorderWidth="0px" />
                                                    <BorderBottom BorderWidth="1px" />
                                                    <Settings ShowFooter="True" />
                                                    <Styles Header-Wrap="True" />
                                                    <Columns>
                                                        <%--Add columns based on the selected items--%>
                                                    </Columns>
                                                </dx:ASPxGridView>
                                            </div>
                                            <%--<dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="ASPxGridView1" ExportSelectedRowsOnly="false" />--%>

                                        </dx:PanelContent>
                                    </PanelCollection>
                                </dx:ASPxCallbackPanel>

                                <div class="row" style="padding-left: 15%;">
                                    <dx:ASPxButton CssClass="CancelButton" ID="ASPxButton3" Text="Back" AutoPostBack="false" UseSubmitBehavior="false" ClientSideEvents-Click="function(s, e) {
                                        ColumnPopUp.Hide();}"
                                        runat="server">
                                    </dx:ASPxButton>
                                    <dx:ASPxButton CssClass="SubmitButton" ID="ASPxButton4" Text="Create Report" AutoPostBack="false" UseSubmitBehavior="true" ClientSideEvents-Click="submitnewreportpressed" runat="server"></dx:ASPxButton>
                                </div>
                            </div>
                        </dx:PopupControlContentControl>
                    </ContentCollection>
                    </dx:ASPxPopupControl>
                 </dx:PanelContent>

                </dx:PanelContent>
            </PanelCollection>
         </dx:ASPxCallbackPanel>
 

</asp:Content>
