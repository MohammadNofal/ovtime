﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/OVMaster.master" CodeFile="NewMyReports.aspx.cs" Inherits="NewMyReports" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>


<asp:Content ID="header_" ContentPlaceHolderID="Header" runat="server">
    
<h4><span class="text-semibold" id="Report_title">
 </span></h4>
    <style>

        .TextBox {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }

        .ComboBox {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }

        .DateEdit {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }

        label {
          padding: 12px 12px 12px 0;
          display: inline-block;
        }

        /* Style the container */
        .container {
          border-radius: 5px;
          background-color: #f2f2f2;
          padding: 20px;
        }

        /* Floating column for labels: 25% width */
        .col-25 {
          float: left;
          width: 25%;
          margin-top: 6px;
        }

        /* Floating column for inputs: 75% width */
        .col-75 {
          float: left;
          width: 75%;
          margin-top: 6px;
        }

        .SubmitButton {
              background-image: none !important;
              background-color: #4CAF50;
              color: white;
              padding: 6px 10px;
              border: none;
              border-radius: 4px;
              cursor: pointer;
              margin-top: 10px;
         }
    </style>
     <script>
         var reportName = window.location.href;
         reportName = reportName.split("id=");
         reportName = reportName[1];
         reportName = reportName.replace(/%20/g, " ");
         reportName = reportName.replace(/\+/g, " ");
         reportName = reportName.replace(".rpt", "");
         $("#Report_title").text("Reports / " + reportName);
    </script>
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPage" runat="server">

    <dx:ASPxCallbackPanel ID="DetailPanel" runat="server" ClientInstanceName="detailPanelSmall" Width="100%" CssClass="detailPanelLargeNoMargin" Collapsible="false"  SettingsLoadingPanel-Enabled ="false" >
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <Styles>
            <ExpandBar Width="100%" CssClass="bar">
            </ExpandBar>
            <ExpandedExpandBar CssClass="expanded">
            </ExpandedExpandBar>
        </Styles>
        <BorderTop BorderWidth="0px"></BorderTop>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                
                <asp:Literal ID="Literal" runat="server"></asp:Literal>
            </dx:PanelContent>
        </PanelCollection>

        <Paddings Padding="8px" />
    </dx:ASPxCallbackPanel>


     <dx:ASPxCallbackPanel ID="ASPxCallbackPanelReport" runat="server" ClientInstanceName="ASPxCallbackPanelReport" Width="100%" Collapsible="false"  SettingsLoadingPanel-Enabled ="false">
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <Styles>
            <ExpandBar Width="100%" CssClass="bar">
            </ExpandBar>
            <ExpandedExpandBar CssClass="expanded">
            </ExpandedExpandBar>
        </Styles>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                     <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="True" Width="100%" Visible="false" CssFilename="custom.css" />       
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>


</asp:Content>