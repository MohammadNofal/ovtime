﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DXOmniVistaTimeEngine;
using System.Web.Security;
using DevExpress.XtraCharts;

public partial class Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
        DateTime _dt = new DateTime();
        _dt = getToDate(DateTime.Now); //SAT
        _dt = _dt.AddDays(-7); //SAT of Prev Week
        String sql = "SELECT CASE WHEN SUM(TEHOURS) IS NULL THEN '0' ELSE SUM(TEHOURS) END  AS 'Hours' FROM TimeEntry WHERE EmployeeID='" + Membership.GetUser(User.Identity.Name) + "' AND( TEDate='" + _dt.Date + "' OR TEDate='" + _dt.AddDays(-1).Date + "' OR TEDate='" + _dt.AddDays(-2).Date + "' OR TEDate='" + _dt.AddDays(-3).Date + "' OR TEDate='" + _dt.AddDays(-4).Date + "' OR TEDate='" + _dt.AddDays(-5).Date + "' OR TEDate='" + _dt.AddDays(-6).Date + "')";
        DataTable timeSheetHoursCount = DataAccess.GetDataTableBySqlSyntax(sql, "");
        // DataTable timeSheetHoursCount = DataAccess.GetDataTableBySqlSyntax("EXEC GetLastWeekHoursCount '" + DateTime.Today.AddDays(-7) + "' , '" + Membership.GetUser(User.Identity.Name) + "'", "");

        string hours = timeSheetHoursCount.Rows[0]["Hours"].ToString();

        if (hours == "")
        {
            hours = "0";
        }
        
        //EmployeeID.Text = Membership.GetUser(User.Identity.Name).ToString();
        EmployeeID2.Set("EmployeeName", Membership.GetUser(User.Identity.Name).ToString());
        TimeSheetStatusContent.InnerText =  hours + " hours were submitted for last week";

        //Setting the status of the timesheet
        DataTable timeSheet = DataAccess.GetDataTableBySqlSyntax("EXEC OVS_GetWeeklyTimeSheet '" + _dt.Date + "' , '" + Membership.GetUser(User.Identity.Name) + "'", "");
        if (timeSheet.Rows.Count > 0)
        {
            IconStatus.Src = "assets/images/icon-check.png";
        }
        else
        {
            IconStatus.Src = "assets/images/icon-cross.png";
        }

        //Loading the notifications gridview
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ToString());
        this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax("EXEC Notifications '" + Membership.GetUser(User.Identity.Name) + "', NULL", "");
        this.ASPxGridView1.DataBind();
    }

    //Mohammad added this
    private DateTime getToDate(DateTime passedDate) //sets Date to SAT
    {
        int days = 0;

        string ToDate = ConfigurationManager.AppSettings["TimeSheetToDate"];

        switch (ToDate)
        {
            case "Sunday":
                days = 0;
                break;
            case "Monday":
                days = 1;
                break;
            case "Tuesday":
                days = 2;
                break;
            case "Wednesday":
                days = 3;
                break;
            case "Thursday":
                days = 4;
                break;
            case "Friday":
                days = 5;
                break;
            case "Saturday":
                days = 6;
                break;
        }

        //If the configuration is Saturday
        switch (passedDate.DayOfWeek)
        {
            case DayOfWeek.Sunday:
                return passedDate.AddDays(days);
            case DayOfWeek.Monday:
                return passedDate.AddDays((days - 1) < 0 ? days + 6 : days - 1);
            case DayOfWeek.Tuesday:
                return passedDate.AddDays((days - 2) < 0 ? days + 5 : days - 2);
            case DayOfWeek.Wednesday:
                return passedDate.AddDays((days - 3) < 0 ? days + 4 : days - 3);
            case DayOfWeek.Thursday:
                return passedDate.AddDays((days - 4) < 0 ? days + 3 : days - 4);
            case DayOfWeek.Friday:
                return passedDate.AddDays((days - 5) < 0 ? days + 2 : days - 5);
            case DayOfWeek.Saturday:
                return passedDate.AddDays((days - 6) < 0 ? days + 1 : days - 6);
        }

        return passedDate;

    }
    ///.............................
}