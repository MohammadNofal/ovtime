﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/OVMaster.master" CodeFile="EmployeeAssignment.aspx.cs" Inherits="EmployeeAssignment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Header" runat="server">
<h4><span class="text-semibold">Setup / Employee Assignment</span></h4>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">

    <style>
         .dxWeb_pPrevDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNext {
             background: url('../content/images/iconmonstr-arrow-63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pPrev {
             background: url('../content/images/iconmonstr-arrow-64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNextDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }
        .month1_border 
        {
            border-left-width: 2px !important;
        }
        .head_container 
        {
            background-color: #EEE;
            float: right;
            margin-right: 7.8%;
        }

        .Month1_css 
        {
            border-style: solid;
            border-width: medium;
            float: right;
            background-color: red;
        }
        .Month2_css 
        {
            border-style: solid;
            border-width: medium;
            float: right;
        }
        .delete_icon 
        {
            position: relative;
            font-family: "Courier New" !important;
            color: #ee784a !important;
            font-size: 200% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .delete_icon:hover 
        {
            color: #FF0000 !important;
        }
        .delete_icon::before 
        {
            content: "\000D7" !important;
        }
        .new_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 20px !important;
            padding: 1px 1px;
            text-decoration: none !important;
        }

        .new_icon:hover 
        {
            color: #7373FF !important;
        }
        .new_icon:before 
        {
           content: "\f067" !important;
        }
        .edit_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #f3cb76 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .edit_icon:hover 
        {
            color: #FF8000 !important;
        }
        .edit_icon:before 
        {
            content: "\f044" !important;
        }
        .save_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
            text-align: center !important;
        }

        .save_icon:before 
        {
            content: "\f0c7" !important;
        }
        .location_icon
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
        }
        .location_icon:before 
        {
            content: "\f041" !important;
        }
        .book_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
        }
        .book_icon:before 
        {
            content: "\f02d" !important;
        }
        .cancel_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #ee784a !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .cancel_icon:hover 
        {
            color: #FF8080 !important;
        }
        .cancel_icon:before
        {
            content: "\f0e2" !important;
        }
        .update_icon
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .update_icon:hover 
        {
            color: #7373FF !important;
        }
        .update_icon:before 
        {
            content: "\f05d " !important;
        }
        .header 
        {
            font-size: 1.25em !important;
        }
        .ProjectDescription 
        {
            font-size: 14px !important;
        }

        .newFont * 
        {
            font-family: Calibri;
            font-size: 16px;
        }
        
.pagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #333333 !important;
            text-decoration: none !important;
            padding-left: 7px !important;
            padding-right: 7px !important;
        }
        .pagenumber:hover{
            background-color : #333333;
            color: #FFFFFF !important;
        }
        .currentpagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #FFFFFF !important;
            background-color: #333333;
            text-decoration: none !important;
        }
    </style>

    <script type="text/javascript">
    </script>
     <div style="overflow-y: auto;height: calc(100vh - 200px);">
    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { LoadingPanel.Hide(); }" />
    </dx:ASPxCallback>
    <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel"
        Modal="True">
    </dx:ASPxLoadingPanel>
    <dx:ASPxPanel ID="ASPxPanel1" runat="server" CssClass="detailPanelSmallHeader"></dx:ASPxPanel>
    <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" ClientInstanceName="GridViewPannel" Collapsible="false" SettingsLoadingPanel-Enabled ="true" OnCallback="ASPxCallbackPanel1_Callback" >
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
                <dx:ASPxGridView ID="EmployeeAssignmentGridView"
                                runat="server" AutoGenerateColumns="False" 
                                Width="100%"
                                AutoPostBack="false"
                                KeyFieldName="EmployeeAssignmentID"
                                CssClass="newFont"
                                OnRowInserting="EmployeeAssignmentGridView_RowInserting"
                                OnRowUpdating="EmployeeAssignmentGridView_RowUpdating"
                                OnRowDeleting="EmployeeAssignmentGridView_RowDeleting"
                                OnRowValidating="EmployeeAssignmentGridView_RowValidating">
                                <SettingsPager CurrentPageNumberFormat="{0}">
                    </SettingsPager>
                    <StylesPager>
                        <PageNumber CssClass="pagenumber"></PageNumber>
                        <CurrentPageNumber CssClass="currentpagenumber"></CurrentPageNumber>
                    </StylesPager>
                               <Settings ShowTitlePanel ="true"/>
                    
                               <SettingsEditing EditFormColumnCount="4" Mode="Inline"/>

                                <Styles>
                                    <AlternatingRow Enabled="true" />
                                    <Header HorizontalAlign="Center"></Header>
                                </Styles>

                                <SettingsPopup>
                                    <EditForm Width="100%" Modal="false"/>
                                </SettingsPopup>

                                <SettingsPager PageSize="50" />
                                <Paddings Padding="0px" />
                                <Border BorderWidth="0px" />
                                <BorderBottom BorderWidth="1px" />
                                <Settings ShowFooter="True" />
                                <Styles Header-Wrap="True" />
                                 <SettingsBehavior ConfirmDelete="true" />
                                <Columns>
                                    <dx:GridViewCommandColumn ShowNewButtonInHeader="true" ShowEditButton="True" ShowDeleteButton="true"></dx:GridViewCommandColumn>
                                    <dx:GridViewDataComboBoxColumn Runat="Server" FieldName="Project" Name="Project" Caption="Project" HeaderStyle-Font-Size="Medium" >
                                        <EditFormSettings VisibleIndex="4" ColumnSpan="1" />
                                        <PropertiesComboBox TextField="Project" ValueField="ProjectID"  EnableSynchronization="true">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>                                    
                                    <dx:GridViewDataComboBoxColumn Runat="Server" FieldName="Employee" Name="Employee" Caption="Employee" HeaderStyle-Font-Size="Medium" >
                                        <EditFormSettings VisibleIndex="4" ColumnSpan="1" />
                                        <PropertiesComboBox TextField="EmployeeName" ValueField="EmployeeID"  EnableSynchronization="true">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn> 
                                    <dx:GridViewDataComboBoxColumn Runat="Server" FieldName="Role" Name="Role" Caption="Role" HeaderStyle-Font-Size="Medium" >
                                        <EditFormSettings VisibleIndex="4" ColumnSpan="1" />
                                        <PropertiesComboBox TextField="Role" ValueField="RoleID"  EnableSynchronization="true">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataSpinEditColumn FieldName="Cost Rate" HeaderStyle-Font-Size="Medium">
                                        <PropertiesSpinEdit MaxLength="11">
                                        <SpinButtons ShowIncrementButtons="False" ShowLargeIncrementButtons="False" />
                                        </PropertiesSpinEdit>
                                    </dx:GridViewDataSpinEditColumn>
                                    <dx:GridViewDataSpinEditColumn FieldName="OT Cost Rate" HeaderStyle-Font-Size="Medium">
                                         <PropertiesSpinEdit MaxLength="11">
                                        <SpinButtons ShowIncrementButtons="False" ShowLargeIncrementButtons="False" />
                                        </PropertiesSpinEdit>
                                    </dx:GridViewDataSpinEditColumn>
                                    <dx:GridViewDataColumn FieldName="DefaultRole" HeaderStyle-Font-Size="Medium"/>
                                    
                                </Columns>
                   
                                <SettingsCommandButton>
                                    <NewButton ButtonType="Image" Text="Add Role">
                                        <Styles Style-CssClass="new_icon"></Styles>
                                    </NewButton>
                                    <DeleteButton Text=" ">
                                        <Styles Style-CssClass="delete_icon"></Styles>
                                    </DeleteButton>
                                    <EditButton Text=" ">
                                        <Styles Style-CssClass="edit_icon"></Styles>
                                    </EditButton>
                                    <UpdateButton Text=" ">
                                        <Styles Style-CssClass="update_icon"></Styles>
                                    </UpdateButton>
                                    <CancelButton Text=" ">
                                        <Styles Style-CssClass="cancel_icon"></Styles>
                                    </CancelButton>
                                </SettingsCommandButton>
                            </dx:ASPxGridView>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
    <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="ASPxGridView1"/>
         </div>
</asp:Content>