﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DXOmniVistaTimeEngine;
using System.Web.Security;
using DevExpress.Web;
using System.Text;
using DevExpress.Export;
using DevExpress.XtraPrinting;
using System.Xml;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Net.Mail;
using System.Net;
using System.IO;
using Newtonsoft.Json;

public partial class SetupEmployee : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        FirstNameTextBox.CssClass = "TextBox";

        //Updating the filter based on the passed parameter
        if(Request.QueryString["Name"] != null) {
            var EmployeeName = Request.QueryString["Name"];

            string[] employeeNames = EmployeeName.Split(' ');

            (EmployeeGridView.Columns["First Name"] as GridViewDataColumn).AutoFilterBy(employeeNames[0]);
            (EmployeeGridView.Columns["Last Name"] as GridViewDataColumn).AutoFilterBy(employeeNames[1]);
        }

        //Connecting to database
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);

        //Getting employees data from database and binding it to gridview
        String sql = ConfigurationManager.AppSettings["EmployeesDetails"];
        DataTable employees = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["EmployeesDetails"], "");
        EmployeeGridView.DataSource = employees;
        employees.PrimaryKey = new DataColumn[] { employees.Columns["EmployeeID"] };
        EmployeeGridView.DataBind();

        DataTable employeesWithEmailsAndKeys = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["EmloyeeDetailsSetupEmployee"], "");

        //Checking how many users logged in after march 3, 2018

        //MembershipUserCollection users = Membership.GetAllUsers();
        //DateTime t1 = DateTime.Parse("2018/03/03 15:00:00.000");
        //int count = 0;

        //for (int i = 0; i < employees.Rows.Count ; i++)
        //{
        //    string userID = employees.Rows[i]["EmployeeID"].ToString();

        //    MembershipUser u = users[userID];
        //    if (u != null && u.LastLoginDate != null && u.LastLoginDate > t1)
        //    {
        //        count = count + 1;
        //    }
        //}

        //int count2 = 0;
    }

    protected void ASPxCallbackPanel1_Callback(object sender, CallbackEventArgsBase e)
    {
        string[] parameters = e.Parameter.Split(',').ToArray();

        // Adding a new employee
        if (e.Parameter == "")
        {
            //Mohammad added the following
            ASPxEdit.ClearEditorsInContainer(this, "popupValidationGroup");
            EmployeeIDTextBox.Visible = false;
            EmployeeIDLabel.Visible = false;
            ResetPasswordButton.Visible = false;
            EmployeeTabPage.TabPages[1].Visible = false;
            
            ReEmailTextBox.Text = "";
            //ContentControlSalary.Visible = false;
            //...............................
            FirstNameTextBox.Text = "";
            LastNameTextBox.Text = "";
            DisciplineComboBox.Text = "";
            ManagerComboBox.Text = "";
            PassportExpiryASPxDateEdit.Text = "";
            EmiratesIDDateEdit.Text = "";
            LaborCardDateEdit.Text = "";
            ACCardDateEdit.Text = "";
            VisaExpiryDateEdit.Text = "";
            AccessLevelComboBox.Text = "";
            HourlyRateTextBox.Text = "";
            TitleTextBox.Text = "";
            EmailTextBox.Text = "";
            
            EmployeeIDHiddenField.Set("EmployeeID", "");
            // Mohammad added the following
            this.EmployeePopUpControl.ShowOnPageLoad = true;
            ///..............
        }
        else if (e.Parameter.ToString() == "Save")
        {
            SaveEmployeeData();
        }
        //Mohammad Added the following for Reseting password
        else if (e.Parameter.ToString() == "ResetPassword")
        {
            if (!EmployeeIDHiddenField.Get("EmployeeID").ToString().Equals(""))
            {
                string employeeID = EmployeeIDHiddenField.Get("EmployeeID").ToString();
                DataTable employee_key = DataAccess.GetDataTableBySqlSyntax("SELECT [Key] from EmployeeDetails WHERE EmployeeID = '"+ employeeID+"'", "");
                string key = employee_key.Rows[0][0].ToString();

                SendResetPasswordEmail(EmailTextBox.Text, FirstNameTextBox.Text.Replace(" ", ""), key);
            }
        }
        //..............................................
        else if (parameters.Count() == 1)
        {
            //SQL delete where employeID = parameter
            string employeeId = e.Parameter;

            string deleteEmployeeSql = "DELETE FROM dbo.Employee WHERE EmployeeID = '" + employeeId + "'";
            DataAccess.ExecuteSqlStatement(deleteEmployeeSql, "");
            //Remove row in datatable of employee grid and reload it and bind it

            string deleteEmployeeDetailsSql = "DELETE FROM EmployeeDetails WHERE EmployeeID = '" + employeeId +"'";
            DataAccess.ExecuteSqlStatement(deleteEmployeeDetailsSql, "");

            string deleteUDFOfEmployeeSql = "DELETE FROM UDF WHERE EmployeeID = '" + employeeId + "'";
            DataAccess.ExecuteSqlStatement(deleteUDFOfEmployeeSql, "");

            Membership.DeleteUser(employeeId) ;

            //string deleteEmployeeRolesSql = "DELETE FROM dbo.EmployeeRoles WHERE EmployeeID = '" + employeeId + "'";
            //DataAccess.ExecuteSqlStatement(deleteEmployeeRolesSql, "");

            DataTable employees = (DataTable)EmployeeGridView.DataSource;
            employees.Rows.Remove(employees.Rows.Find(employeeId));

            EmployeeGridView.DataSource = employees;
            EmployeeGridView.DataBind();

        }
        // Updating an employee
        
        else
        {
            ASPxEdit.ClearEditorsInContainer(this, "popupValidationGroup");//Mohammad added the following

            //Filling the employee fields
            EmployeeIDHiddenField.Set("EmployeeID", parameters[0]);

            DataTable employees = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["EmployeesDetails"], "");
            //Mohammad added the following
            EmployeeIDTextBox.Text = parameters[0];
            EmployeeIDTextBox.Enabled = false;
           
            ReEmailTextBox.Text = parameters[13];
            //.........................................
            FirstNameTextBox.Text = parameters[1];
            LastNameTextBox.Text = parameters[2];
            DisciplineComboBox.Value = parameters[3];
            ManagerComboBox.Value = parameters[4];
            AccessLevelComboBox.Value = parameters[5];
            HourlyRateTextBox.Text = parameters[6];
            //SalaryTextBox.Text = parameters[7];
            PassportExpiryASPxDateEdit.Value = parameters[7];
            EmiratesIDDateEdit.Value = parameters[8];
            LaborCardDateEdit.Value = parameters[9];
            ACCardDateEdit.Value = parameters[10];
            VisaExpiryDateEdit.Value = parameters[11];
            TitleTextBox.Text = parameters[12];
            EmailTextBox.Text = parameters[13];

            //Mohammad added the following

            //get salary details 
            OBjRoot RootJSON = GetSalaryDetailsFromServer(parameters[0]);
            //DataTable salary_table = DataAccess.GetDataTableBySqlSyntax("SELECT BasicSalary, Transportation, Housing, TotalSalary FROM OMV_EmployeeSalary WHERE UserId='" + parameters[0]+"'", "");
            EmployeeIDSalaryTextBox.Value = parameters[0];
            if (RootJSON != null)//assign to popup
            {
                BasicSalaryTextBox.Value = RootJSON.BasicSalary;
                TransportationTextBox.Value = RootJSON.Transportation;
                HousingTextBox.Value = RootJSON.Housing;
                TotalSalaryTextBox.Value = RootJSON.TotalSalary;
            }
            this.EmployeePopUpControl.ShowOnPageLoad = true;
            //........................
        }
        
        DisciplineComboBox.Items.Add("");

        DataTable roles = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["RoleNames"], "");
        DisciplineComboBox.DataSource = roles;
        DisciplineComboBox.DataBind();
        //DisciplineComboBox.Items.Add("Management");
        //DisciplineComboBox.Items.Add("Architecture");
        //DisciplineComboBox.Items.Add("Structure");
        //DisciplineComboBox.Items.Add("Mechanical");
        //DisciplineComboBox.Items.Add("Electrical");
        //DisciplineComboBox.Items.Add("QS");
        //DisciplineComboBox.Items.Add("Document Controlling");
        //DisciplineComboBox.Items.Add("Accounting");

        //Bind Manager combobox data
        //Mohammad eddited the following 
        DataTable managersBrief = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["ManagersBrief"], "");
        //  DataTable managersBrief = DataAccess.GetDataTableBySqlSyntax("","");
         //...................................
         ManagerComboBox.DataSource = managersBrief;
        ManagerComboBox.DataBind();
        ManagerComboBox.Items.Insert(0, new ListEditItem(""));

        DataTable accessLevels = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["AccessLevels"], "");
        AccessLevelComboBox.DataSource = accessLevels;
        AccessLevelComboBox.DataBind();
        AccessLevelComboBox.Items.Insert(0, new ListEditItem(""));
       
    }

    protected void CancelEditing(CancelEventArgs e, ASPxGridView gridView)
    {
        e.Cancel = true;
        gridView.CancelEdit();
    }

    protected void SaveEmployeeData()
    {
            //If it is a new employee
            if (EmployeeIDHiddenField.Get("EmployeeID").ToString() == "")
            {
                FirstNameTextBox.ErrorText = "Test";

                string key = Guid.NewGuid().ToString();

                var visaExpiryDate = VisaExpiryDateEdit.Value == null ? "NULL" : "'" + VisaExpiryDateEdit.Value.ToString() + "'";

                //Creating new employee
                string createEmployeeSql = "INSERT INTO dbo.Employee (EmployeeID,EmpFName,EmpLName,EmpDepartment,EmpManager,ManagerID,EmpCostRate,EmpNextImpDate,EmpTitle, EmpEmail) Values ('" + FirstNameTextBox.Text.Replace(" ", "").ToUpper() + "." + LastNameTextBox.Text.Replace(" ", "").ToUpper() + "', '" + FirstNameTextBox.Text + "', '" + LastNameTextBox.Text + "', '" + DisciplineComboBox.Value + "', '" + ManagerComboBox.Text + "', '" + ManagerComboBox.Value + "', '" + HourlyRateTextBox.Text + "', " + visaExpiryDate + " , '" + TitleTextBox.Text + "', '" + EmailTextBox.Text + "')";
                DataAccess.ExecuteSqlStatement(createEmployeeSql, "");

                string addEmployeeDetailsSql = "INSERT INTO EmployeeDetails (EmployeeID, AccessLevelID, [Key]) VALUES ('" + FirstNameTextBox.Text.Replace(" ", "").ToUpper() + "." + LastNameTextBox.Text.Replace(" ", "").ToUpper() + "','" + AccessLevelComboBox.Value + "','" + key + "')";
                DataAccess.ExecuteSqlStatement(addEmployeeDetailsSql, "");

                var passportExpiryDate = PassportExpiryASPxDateEdit.Value == null ? "NULL" : "'" + VisaExpiryDateEdit.Value.ToString() + "'";
                var emiratesIDExpiryDate = EmiratesIDDateEdit.Value == null ? "NULL" : "'" + EmiratesIDDateEdit.Value.ToString() + "'";
                var laborCardExpiryDate = LaborCardDateEdit.Value == null ? "NULL" : "'" + LaborCardDateEdit.Value.ToString() + "'";
                var acCardExpiryDate = ACCardDateEdit.Value == null ? "NULL" : "'" + ACCardDateEdit.Value.ToString() + "'";

                string addUDFToEmployeeSql = "INSERT INTO UDF (EmployeeID, UDF1, UDF2, UDF3, UDF4) VALUES ('" + FirstNameTextBox.Text.Replace(" ", "").ToUpper() + "." + LastNameTextBox.Text.Replace(" ", "").ToUpper() + "'," + passportExpiryDate + "," + emiratesIDExpiryDate + "," + laborCardExpiryDate + "," + acCardExpiryDate + ")";
                DataAccess.ExecuteSqlStatement(addUDFToEmployeeSql, "");

                Membership.DeleteUser(FirstNameTextBox.Text.Replace(" ", "").ToUpper() + "." + LastNameTextBox.Text.Replace(" ", "").ToUpper());

                //Send email to user
                SendActivationEmail(EmailTextBox.Text, FirstNameTextBox.Text.Replace(" ", ""), LastNameTextBox.Text, key);
            }
            //If it is an old employee
            else
            {
                //Changing Id,First Name, Last Name, Discipline
                string updateEmployeeSql = "Update dbo.Employee SET EmployeeID = '" + FirstNameTextBox.Text.Replace(" ", "").ToUpper() + "." + LastNameTextBox.Text.Replace(" ", "").ToUpper() + "', EmpFName = '" + FirstNameTextBox.Text + "', EmpLName = '" + LastNameTextBox.Text + "', EmpDepartment = '" + DisciplineComboBox.Value + "', EmpManager = '" + ManagerComboBox.Text + "', ManagerID = '" + ManagerComboBox.Value + "', EmpCostRate = '" + HourlyRateTextBox.Text + "', EmpNextImpDate = '" + VisaExpiryDateEdit.Value + "', EmpTitle = '" + TitleTextBox.Text + "', EmpEmail = '" + EmailTextBox.Text + "' WHERE EmployeeID = '" + EmployeeIDHiddenField.Get("EmployeeID") + "'";
                DataAccess.ExecuteSqlStatement(updateEmployeeSql, "");

                string updateAccessLevelOfEmployeeSql = "UPDATE EmployeeDetails SET EmployeeID = '" + FirstNameTextBox.Text.Replace(" ", "").ToUpper() + "." + LastNameTextBox.Text.Replace(" ", "").ToUpper() + "', AccessLevelID = '" + AccessLevelComboBox.Value + "' WHERE EmployeeID = '" + EmployeeIDHiddenField.Get("EmployeeID") + "'";
                DataAccess.ExecuteSqlStatement(updateAccessLevelOfEmployeeSql, "");

                string updateUDFOfEmployeeSql = "UPDATE UDF SET EmployeeID = '" + FirstNameTextBox.Text.Replace(" ", "").ToUpper() + "." + LastNameTextBox.Text.Replace(" ", "").ToUpper() + "', UDF1 = '" + PassportExpiryASPxDateEdit.Value + "', UDF2 = '" + EmiratesIDDateEdit.Value + "', UDF3 = '" + LaborCardDateEdit.Value + "', UDF4 = '" + ACCardDateEdit.Value + "'" + " WHERE EmployeeID = '" + EmployeeIDHiddenField.Get("EmployeeID") + "'";
                DataAccess.ExecuteSqlStatement(updateUDFOfEmployeeSql, "");
            }

            DataTable employees = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["EmployeesDetails"], "");
            EmployeeGridView.DataSource = employees;
            EmployeeGridView.DataBind();
    }

    public void SendActivationEmail(string Email, string FirstName, string LastName, string key)
    {
        try
        {

            SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["EmailServer"], 25);

            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["EmailPassword"]);
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.EnableSsl = true;
            smtpClient.Port = 465;

            MailMessage mail = new MailMessage();
            mail.Subject = "You are invited to OVTime!";
            String host = HttpContext.Current.Request.Url.Host;
            mail.Body = "Dear " + FirstName + ", \n\n A user account was created for you in the new OVTime application and you are invited to start using this application by following the activation link below: \n\nYour username: " + FirstName.Replace(" ", "").ToUpper() + "." + LastName.Replace(" ", "").ToUpper() + ", \n Your password: will be set up during your first login" + "\n http://" + host + "/time/Account/NewRegister.aspx?key=" + key;

            //Setting From , To and CC
            mail.From = new MailAddress(ConfigurationManager.AppSettings["Email"], "OVTime");
            mail.To.Add(new MailAddress(Email));

            smtpClient.Send(mail);
        }
        catch (Exception ex)
        {

        }
    }
    //Mohammad added the following
    private void SendResetPasswordEmail(string Email, string FirstName, string key)
    {
        try
        {

            SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["EmailServer"], 25);

            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["EmailPassword"]);
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.EnableSsl = true;
            smtpClient.Port = 465;

            MailMessage mail = new MailMessage();
            mail.Subject = "OVTime Account Reset Password";
            String host = HttpContext.Current.Request.Url.Host;
            mail.Body = "Dear " + FirstName + ", \n\n Your manager issued to reset your password, please follow the provided link to reset your password. \n http://" + host + "/time/Account/ResetPassword.aspx?key=" + key;

            //Setting From , To and CC
            mail.From = new MailAddress(ConfigurationManager.AppSettings["Email"], "OVTime");
            mail.To.Add(new MailAddress(Email));

            smtpClient.Send(mail);
        }
        catch (Exception ex)
        {

        }
    }
    //...........................................
    protected void ASPXDateEdit_CustomCell(object sender, CalendarDayCellPreparedEventArgs e)
    {
        // #c00000 (red active)
        // #333333 (black active)
        // #ececec (grey not active)
        if (!e.IsOtherMonthDay)
        {
            if (e.Date.DayOfWeek == DayOfWeek.Sunday) e.Cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#333333");
            else if (e.Date.DayOfWeek == DayOfWeek.Friday) e.Cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#c00000"); //#c00000
        }
    }

    private OBjRoot GetSalaryDetailsFromServer(String employeeID)
    {
        if (!employeeID.Equals("") && !employeeID.Equals("SelectAll"))
        {
            WebRequest req = WebRequest.Create(@ConfigurationManager.AppSettings["Salary"] + employeeID);
            req.Method = "GET";
            try
            {
                HttpWebResponse resp = req.GetResponse() as HttpWebResponse;
                Object result;
                if (resp.StatusCode == HttpStatusCode.OK)
                {
                    using (Stream respStream = resp.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(respStream, Encoding.UTF8);
                        result = reader.ReadToEnd();
                        String resultS = (String)result;
                        OBjRoot JSONobj = JsonConvert.DeserializeObject<OBjRoot>(resultS); //Create JSONobject for employee
                        //Session["SetupEmployeeJSONobj"] = JSONobj;
                        return JSONobj;
                    }
                }
                else
                {
                    //Error connecting to database
                    //Console.WriteLine(string.Format("Status Code: {0}, Status Description: {1}", resp.StatusCode, resp.StatusDescription));
                    return null;
                }
            }
            catch (WebException ex)
            {
                //Error connecting to database
                return null;
            }
            // Console.Read();
           
        }
        return null;
    }

    private class OBjRoot
    {
        public String Id { get; set; }
        public String UserId { get; set; }
        public String BasicSalary { get; set; }
        public String Transportation { get; set; }
        public String Housing { get; set; }
        public String TotalSalary { get; set; }
        public String VacationRuleId { get; set; }
        public String EndOfServiceRuleId { get; set; }
    }
}