﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DXOmniVistaTimeEngine;
using System.Web.Security;
using DevExpress.Web;
using System.Text;
using DevExpress.Export;
using DevExpress.XtraPrinting;
using System.Xml;
using System.Collections;
using System.Globalization;

public partial class SetupProject : System.Web.UI.Page
{
    public DataTable _dt = new DataTable();
    private DateTime _friday = new DateTime();
    private int originalWidth;
    private GridViewBandColumn _month1 = new GridViewBandColumn();
    private GridViewBandColumn _month2 = new GridViewBandColumn();
    private GridViewDataTextColumn SAT_HRS = new GridViewDataTextColumn();
    private GridViewDataTextColumn SUN_HRS = new GridViewDataTextColumn();
    private GridViewDataTextColumn MON_HRS = new GridViewDataTextColumn();
    private GridViewDataTextColumn TUE_HRS = new GridViewDataTextColumn();
    private GridViewDataTextColumn WED_HRS = new GridViewDataTextColumn();
    private GridViewDataTextColumn THU_HRS = new GridViewDataTextColumn();
    private GridViewDataTextColumn FRI_HRS = new GridViewDataTextColumn();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Connecting to database
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);

        //Getting projects data from database and binding it to gridview
        DataTable projects = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["Projects"], "");
        //DataTable projects = DataAccess.GetDataTableBySqlSyntax("SELECT P.ProjectID, P.ProjectCode, P.ProjectName, P.ProjectID + ' - ' + P.ProjectName as 'Project', (SELECT ClientCompany FROM Client C WHERE P.ClientID = C.ClientID) AS 'Client', PD.Manager, P.ProjectStartDate AS 'Project Start Date', P.ProjectPONum AS 'Project PO Number',  P.MessageOnInvoice AS 'Message On Invoice' FROM Project P LEFT JOIN ProjectDetails PD ON P.ProjectID = PD.ProjectID WHERE ProjectCode !='' AND ProjectCode IS NOT NULL AND ProjectName !='' AND ProjectName IS NOT NULL AND (SELECT ClientCompany FROM Client C WHERE P.ClientID = C.ClientID) !='' AND (SELECT ClientCompany FROM Client C WHERE P.ClientID = C.ClientID) IS NOT NULL AND Manager !='' AND Manager IS NOT NULL AND ProjectStartDate !='' AND ProjectStartDate IS NOT NULL AND ProjectPONum !='' AND ProjectPONum IS NOT NULL AND MessageOnInvoice !='' AND MessageOnInvoice IS NOT NULL", "");
        ProjectGridView.DataSource = projects;
        projects.PrimaryKey = new DataColumn[] { projects.Columns["ProjectID"] };
        ProjectGridView.DataBind();

    }

    protected void ASPxCallbackPanel1_Callback(object sender, CallbackEventArgsBase e)
    {
        String[] parameters = e.Parameter.Split(',').ToArray();

        // Adding a new project
        if (e.Parameter == "")
        {
            ASPxEdit.ClearEditorsInContainer(this, "popupValidationGroup");//Mohammad added the following
            ProjectIDTextBox.Text = "";
            ProjectNameTextBox.Text = "";
            ClientComboBox.Text = "";
            ManagerComboBox.Text = "";
            ProjectStartDateEdit.Text = "";
            ProjectPONumberTextBox.Text = "";
            MessageOnInvoiceTextBox.Text = "";

            DataTable dt = new DataTable();
            dt.Clear();
            dt.Columns.Add("EmployeeName");

            EmployeeGridView.DataSource = dt;
            EmployeeGridView.DataBind();

            EmployeeGridView.Visible = false;

            ProjectIDHiddenField.Set("ProjectID", "");

            // Mohammad added the following
            this.ProjectPopUpControl.ShowOnPageLoad = true;
            ///..............
        }
        else if (e.Parameter.ToString() == "Save")
        {
            SaveProjectData();
        }
        else if (parameters.Count() == 1)
        {
            //SQL delete where projectID = parameter
            string projectId = e.Parameter;

            string deleteProjectSql = "DELETE FROM dbo.Project WHERE ProjectID = '" + projectId + "'";
            DataAccess.ExecuteSqlStatement(deleteProjectSql, "");
            //Remove row in datatable of project grid and reload it and bind it

            string deleteManagerOfProjectSql = "DELETE FROM dbo.ProjectDetails WHERE ProjectID = '" + projectId + "'";
            DataAccess.ExecuteSqlStatement(deleteManagerOfProjectSql, "");

            DataTable projects = (DataTable)ProjectGridView.DataSource;
            projects.Rows.Remove(projects.Rows.Find(projectId));

            ProjectGridView.DataSource = projects;
            ProjectGridView.DataBind();
        }
        // Updating an project
        else
        {
            ASPxEdit.ClearEditorsInContainer(this, "popupValidationGroup");//Mohammad added the following
            //Filling the project fields
            ProjectIDHiddenField.Set("ProjectID", parameters[0]);
            ProjectIDTextBox.Text = parameters[0];
            ProjectNameTextBox.Text = parameters[1];
            ClientComboBox.Value = parameters[2];
            ManagerComboBox.Value = parameters[3];
            ProjectStartDateEdit.Value = parameters[4];
            ProjectPONumberTextBox.Text = parameters[5];
            MessageOnInvoiceTextBox.Text = parameters[6];

            //ProjectStartDateTextBox.Text = DateTime.Parse(parameters[2].ToString()).ToString("MM/dd/yyyy");

            DataTable projectEmployees = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["ProjectEmployees"], "WHERE ProjectID = '" + ProjectIDHiddenField.Get("ProjectID").ToString() + "'");
            EmployeeGridView.DataSource = projectEmployees;
            EmployeeGridView.DataBind();

            // Mohammad added the following
            this.ProjectPopUpControl.ShowOnPageLoad = true;
            ///..............
        }
        //Mohamamd eddited the following
         DataTable clients = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["Clients"], "");
        //DataTable clients = DataAccess.GetDataTableBySqlSyntax("SELECT C.ClientID, C.ClientCompany as Company, C.ClientStreet as Street, C.ClientCity as City, C.ClientZip AS 'Zip', C.ClientPhone as 'Phone Number', C.ClientFName as 'Representative First Name', C.ClientLName as 'Representative Last Name', C.ClientMainSalt as 'Representative Main Salutation',  C.ClientOther3 AS 'Representative Title' FROM Client C WHERE C.ClientCompany !='' AND C.ClientCompany IS NOT NULL", "");
        
        ClientComboBox.DataSource = clients;
        ClientComboBox.DataBind();
        ClientComboBox.Items.Insert(0, new ListEditItem(""));


        DataTable managers = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["ManagersBrief"], "");
        ManagerComboBox.DataSource = managers;
        ManagerComboBox.DataBind();
        ManagerComboBox.Items.Insert(0, new ListEditItem(""));

    }

    protected void SaveProjectData()
    {
        //If it is a new projecthttp://localhost:57860/Account/Web.config
        if (ProjectIDHiddenField.Get("ProjectID").ToString() == "")
        {
            //Creating new project
            //Mohammad edditted the following 
            //string createProjectSql = "INSERT INTO dbo.Project (ProjectID,ProjectCode,ProjectName,ProjectStartDate,ProjectPONum,MessageOnInvoice,ClientID) Values ('" + ProjectIDTextBox.Text + "', '" + ProjectIDTextBox.Text + "', '" + ProjectNameTextBox.Text + "', '" + ProjectStartDateEdit.Value + "', '" + ProjectPONumberTextBox.Text + "', '" + MessageOnInvoiceTextBox.Text + "', " + "(SELECT ClientID FROM Client WHERE ClientCompany = '" + ClientComboBox.Value + "')" + ")";
            string createProjectSql = "INSERT INTO dbo.Project (ProjectID,ProjectCode,ProjectName,ProjectStartDate,ProjectPONum,MessageOnInvoice,ClientID) Values ('" + ProjectIDTextBox.Text + "', '" + ProjectIDTextBox.Text + "', '" + ProjectNameTextBox.Text + "', '" + ProjectStartDateEdit.Value + "', '" + ProjectPONumberTextBox.Text + "', '" + MessageOnInvoiceTextBox.Text + "', " + "'" + ClientComboBox.Value + "'" + ")";
            //..............
            string addManagerToProjectSql = "INSERT INTO ProjectDetails (ProjectID, ManagerID,Manager) VALUES ('" + ProjectIDTextBox.Text + "','" + ManagerComboBox.Value + "','" + ManagerComboBox.Text + "')";
            
            DataAccess.ExecuteSqlStatement(createProjectSql, "");
            DataAccess.ExecuteSqlStatement(addManagerToProjectSql, "");
        }
        //If it is an old project
        else
        {
            //Changing Id, Code, Name
            string updateProjectSql = "Update dbo.Project SET ProjectID = '" + ProjectIDHiddenField.Get("ProjectID").ToString() + "', ProjectCode = '" + ProjectIDHiddenField.Get("ProjectID").ToString() + "', ProjectName = '" + ProjectNameTextBox.Text + "', ProjectStartDate = '" + ProjectStartDateEdit.Value + "', ProjectPONum = '" + ProjectPONumberTextBox.Text + "', MessageOnInvoice = '" + MessageOnInvoiceTextBox.Text + "', ClientID = " + "(SELECT ClientID FROM Client WHERE ClientID = '" + ClientComboBox.Value + "')" + " WHERE ProjectID = '" + ProjectIDHiddenField.Get("ProjectID") + "'";

            string updateManagerOfProjectSql = "UPDATE ProjectDetails SET Manager = '" + ManagerComboBox.Text + "', ManagerID = '" + ManagerComboBox.Value + "' WHERE ProjectID = '" + ProjectIDHiddenField.Get("ProjectID").ToString() + "'";

            DataAccess.ExecuteSqlStatement(updateProjectSql, "");
            DataAccess.ExecuteSqlStatement(updateManagerOfProjectSql, "");
        }

        DataTable projects = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["Projects"], "");
        //DataTable projects = DataAccess.GetDataTableBySqlSyntax("SELECT P.ProjectID, P.ProjectCode, P.ProjectName, P.ProjectID + ' - ' + P.ProjectName as 'Project', (SELECT ClientCompany FROM Client C WHERE P.ClientID = C.ClientID) AS 'Client', PD.Manager, P.ProjectStartDate AS 'Project Start Date', P.ProjectPONum AS 'Project PO Number',  P.MessageOnInvoice AS 'Message On Invoice' FROM Project P LEFT JOIN ProjectDetails PD ON P.ProjectID = PD.ProjectID WHERE ProjectCode !='' AND ProjectCode IS NOT NULL AND ProjectName !='' AND ProjectName IS NOT NULL AND (SELECT ClientCompany FROM Client C WHERE P.ClientID = C.ClientID) !='' AND (SELECT ClientCompany FROM Client C WHERE P.ClientID = C.ClientID) IS NOT NULL AND Manager !='' AND Manager IS NOT NULL AND ProjectStartDate !='' AND ProjectStartDate IS NOT NULL AND ProjectPONum !='' AND ProjectPONum IS NOT NULL AND MessageOnInvoice !='' AND MessageOnInvoice IS NOT NULL", "");
        ProjectGridView.DataSource = projects;
        ProjectGridView.DataBind();
    }

    protected void ASPXDateEdit_CustomCell(object sender, CalendarDayCellPreparedEventArgs e)
    {
        // #c00000 (red active)
        // #333333 (black active)
        // #ececec (grey not active)
        if (!e.IsOtherMonthDay)
        {
            if (e.Date.DayOfWeek == DayOfWeek.Sunday) e.Cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#333333");
            else if (e.Date.DayOfWeek == DayOfWeek.Friday) e.Cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#c00000"); //#c00000
        }
    }
}