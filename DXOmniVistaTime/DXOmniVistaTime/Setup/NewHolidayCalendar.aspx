﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/OVMaster.master" CodeFile="NewHolidayCalendar.aspx.cs" Inherits="NewHolidayCalendar" %>

<asp:Content ID="header_" ContentPlaceHolderID="Header" runat="server">
<h4><span class="text-semibold">Holiday Calendar</span></h4>
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPage" runat="server">
     <style>
          .dxWeb_pPrevDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNext {
             background: url('../content/images/iconmonstr-arrow-63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pPrev {
             background: url('../content/images/iconmonstr-arrow-64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNextDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }
         .delete_icon 
        {
            position: relative;
            font-family: "Courier New" !important;
            color: #ee784a !important;
            font-size: 200% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .delete_icon:hover 
        {
            color: #FF0000 !important;
        }
        .delete_icon::before 
        {
            content: "\000D7" !important;
        }
        .new_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 20px !important;
            padding: 1px 1px;
            text-decoration: none !important;
        }

        .new_icon:hover 
        {
            color: #7373FF !important;
        }
        .new_icon:before 
        {
           content: "\f067" !important;
        }
        .update_icon
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        
        .update_icon:hover
        {
            color: #7373FF !important;
             visibility: visible;
        }
        .update_icon:before 
        {
            content: "\f05d " !important;
        }
         .cancel_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #ee784a !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .cancel_icon:hover 
        {
            color: #FF8080 !important;
        }
        .cancel_icon:before
        {
            content: "\f0e2" !important;
        }
             .newFont * 
        {
            font-family: Calibri;
            font-size: 16px;
        }
             
.pagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #333333 !important;
            text-decoration: none !important;
            padding-left: 7px !important;
            padding-right: 7px !important;
        }
        .pagenumber:hover{
            background-color : #333333;
            color: #FFFFFF !important;
        }
        .currentpagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #FFFFFF !important;
            background-color: #333333;
            text-decoration: none !important;
        }
     </style>
     <div style="overflow-y: auto;height: calc(100vh - 200px);">
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" ClientInstanceName="ASPxGridView1" Width="65%" AutoPostBack="true" KeyFieldName="HolidayName;HolidayDate"
        OnRowInserting = "ASPxGridView1_RowInserting"
        OnRowDeleting = "ASPxGridView1_RowDeleting"
        OnRowUpdating = "ASPxGridView1_RowUpdating"
        CssClass="newFont"
        OnBeforeColumnSortingGrouping="ASPxGridView1_BeforeColumnSortingGrouping" Border-BorderColor="#dbdbdb" >
        <SettingsPager CurrentPageNumberFormat="{0}">
                    </SettingsPager>
                    <StylesPager>
                        <PageNumber CssClass="pagenumber"></PageNumber>
                        <CurrentPageNumber CssClass="currentpagenumber"></CurrentPageNumber>
                    </StylesPager>
        <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
        <%--<Paddings Padding="0px" />
        <Styles Row-BackColor="White" >
            <Header Wrap="True" BackColor="#f3f3f2" Border-BorderColor="#dbdbdb" >

            </Header>
            
            <AlternatingRow BackColor="#ededeb" Enabled="true" />
            <Cell Border-BorderColor="#CFCFCF"/>
            
        </Styles>--%>
        <%-- Mohammad commented what is up and added the following (7/5/2018)  --%>
         <Settings ShowTitlePanel ="true"  />
                    
                    <SettingsEditing EditFormColumnCount="4" Mode="Inline"/>
                    <Styles>
                        <AlternatingRow Enabled="true" />
                        <Header HorizontalAlign="Center"></Header>
                    </Styles>

                    <SettingsPopup>
                        <EditForm Width="100%" Modal="false"/>
                    </SettingsPopup>

                    <SettingsPager PageSize="50" />
                    <Paddings Padding="0px" />
                    <Border BorderWidth="0px" />
                    <BorderBottom BorderWidth="1px" />
                    <Settings ShowFooter="True" />
                    <Styles Header-Wrap="True" />
         <%-- ......................................................  --%>
        <Paddings PaddingLeft="10px" />
        <SettingsPager PageSize="50" />
        <Settings ShowFooter="True" />
        <SettingsBehavior ConfirmDelete="true" />
        <SettingsText ConfirmDelete="Do you want delete this row?" />
        <SettingsEditing Mode="Batch" />
        <Columns>
            <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="False" ShowNewButtonInHeader="True" VisibleIndex="0" Width="1%">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="HolidayName" HeaderStyle-Font-Size="Medium" CellStyle-Font-Size="Medium" Width="16%" CellStyle-HorizontalAlign="Left" VisibleIndex="1">
                <HeaderStyle Font-Size="Medium"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" Font-Size="Medium">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="HolidayDate" HeaderStyle-Font-Size="Medium" CellStyle-Font-Size="Medium" Width="16%" CellStyle-HorizontalAlign="Left" VisibleIndex="2">
                <HeaderStyle Font-Size="Medium"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" Font-Size="Medium">
                </CellStyle>
            </dx:GridViewDataDateColumn>
        </Columns>

        <Paddings PaddingTop="10px" />
        <Border BorderWidth="0px" />
        <BorderBottom BorderWidth="1px" />

         <SettingsCommandButton>
             <DeleteButton Text=" ">
                            <Styles Style-CssClass="delete_icon"></Styles>
                        </DeleteButton>
                        <NewButton Text=" ">
                            <Styles Style-CssClass="new_icon"></Styles>
                        </NewButton>
             <CancelButton Text=" ">
                  <Styles Style-CssClass="cancel_icon"></Styles>
             </CancelButton>
             <UpdateButton Text=" ">
                 <Styles Style-CssClass="update_icon">
                     
                 </Styles>
                 
             </UpdateButton>
             </SettingsCommandButton>
        
    </dx:ASPxGridView>


         </div>
 </asp:content>

