﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/OVMaster.master" CodeFile="SetupEmployee.aspx.cs" Inherits="SetupEmployee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Header" runat="server">
<h4><span class="text-semibold">Setup / Employees</span></h4>
<%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script> --%> <%--Mohammad commented this out becuase it was causing issue with Master page list on the left side of web app--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">

     <style>
        .month1_border 
        {
            border-left-width: 2px !important;
        }
        .head_container 
        {
            background-color: #EEE;
            float: right;
            margin-right: 7.8%;
        }

        .Month1_css 
        {
            border-style: solid;
            border-width: medium;
            float: right;
            background-color: red;
        }
        .Month2_css 
        {
            border-style: solid;
            border-width: medium;
            float: right;
        }
        .delete_icon 
        {
            position: relative;
            font-family: "Courier New" !important;
            color: #ee784a !important;
            font-size: 200% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .delete_icon:hover 
        {
            color: #FF0000 !important;
        }
        .delete_icon::before 
        {
            content: "\000D7" !important;
        }
        .new_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 20px !important;
            padding: 1px 1px;
            text-decoration: none !important;
        }

        .new_icon:hover 
        {
            color: #7373FF !important;
        }
        .new_icon:before 
        {
           content: "\f067" !important;
        }
        .edit_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #f3cb76 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .edit_icon:hover 
        {
            color: #FF8000 !important;
        }
        .edit_icon:before 
        {
            content: "\f044" !important;
        }
        .save_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
            text-align: center !important;
        }

        .save_icon:before 
        {
            content: "\f0c7" !important;
        }
        .location_icon
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
        }
        .location_icon:before 
        {
            content: "\f041" !important;
        }
        .book_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
        }
        .book_icon:before 
        {
            content: "\f02d" !important;
        }
        .cancel_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #ee784a !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .cancel_icon:hover 
        {
            color: #FF8080 !important;
        }
        .cancel_icon:before
        {
            content: "\f0e2" !important;
        }
        .update_icon
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .update_icon:hover 
        {
            color: #7373FF !important;
        }
        .update_icon:before 
        {
            content: "\f05d " !important;
        }
        .header 
        {
            font-size: 1.25em !important;
        }
        .ProjectDescription 
        {
            font-size: 14px !important;
        }

        .newFont * 
        {
            font-family: Calibri;
            font-size: 16px;
        }

        .newFontLeft * 
        {
            font-family: Calibri;
            font-size: 16px;
            text-align: right;
        }

        .TextBox {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }

        .ComboBox {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }

        .DateEdit {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }

        /* Style inputs, select elements and textareas */
        input[type=text], select, textarea{
          width: 100%;
          padding: 12px;
          border: 1px solid #ccc;
          border-radius: 4px;
          box-sizing: border-box;
          resize: vertical;
        }

        /* Style the label to display next to the inputs */
        label {
          padding: 12px 12px 12px 0;
          display: inline-block;
        }

        /* Style the submit button */
        input[type=submit] {
          background-color: #4CAF50;
          color: white;
          padding: 12px 20px;
          border: none;
          border-radius: 4px;
          cursor: pointer;
          float: right;
        }

        .SubmitButton {
             background-image: none !important;
             background-color: #4CAF50;
             color: white;
             padding: 6px 10px;
             border: none;
             border-radius: 4px;
             cursor: pointer;
             float: right;
             margin-top: 10px;
             margin-right: 10px;
         }
        .SubmitButton:hover{
            background-color: #419544;

        }
         .SubmitButtonResetPass {
             background-image: none !important;
             background-color: #4CAF50;
             color: white;
             padding: 6px 10px;
             border: none;
             border-radius: 4px;
             cursor: pointer;
             float: left;
             margin-top: 10px;
             margin-right: 10px;
         }
        .ResetPasswordButton {
             background-image: none !important;
             background-color: #ff7979;
             color: white;
             padding: 7px 10px;
             border: none;
             border-radius: 4px;
             cursor: pointer;
             float: left;
             margin-top: 2px;
              margin-left: 6px;
             
         }
         .ResetPasswordButton:hover{
             background-color: #ff4c4c;
         }

        .CancelButton {
             background-image: none !important;
             background-color: #ccc;
             color: black;
             padding: 6px 10px;
             border: none;
             border-radius: 4px;
             cursor: pointer;
             float: right;
             margin-top: 10px;
        }
        .CancelButton:hover{
             background-color: #adadad;

        }
         .CancelButtonPassReset {
             background-image: none !important;
             background-color: #ccc;
             color: black;
             padding: 6px 10px;
             border: none;
             border-radius: 4px;
             cursor: pointer;
             float: left;
             margin-top: 10px;
        }
        /* Style the container */
        .container {
          border-radius: 5px;
          background-color: #f2f2f2;
          padding: 20px;
          width: 100%; /*Mohammad added this*/
         height: 100%;
        
        }

        /* Floating column for labels: 25% width */
        .col-25 {
          float: left;
          width: 25%;
          margin-top: 6px;
        }

        /* Floating column for inputs: 75% width */
        .col-75 {
          float: left;
          width: 75%;
          margin-top: 6px;
        }
        /* Mohammad added the following*/
        .col-55 {
          float: left;
          width: 55%;
          margin-top: 6px;
        }

           .col-20 {
          float: left;
          width: 20%;
          margin-top: 6px;
        }
           .col-10{
                 float: left;
          width: 10%;
          margin-top: 6px;
           }
           .ActiveTab{
               width: 70px;
               height: 30px; 
               background-color: #4CAF50 !important;
                font-size: 17px;
               text-align:center;
               margin-left: 0px !important;
             text-align:center;
           }

         .Tab {
             width: 70px;
             height: 30px;
             text-align: center;
             font-size: 17px;
             margin-left: 0px !important;
             text-align: center;
             background-color: #FFFFFF;
         }

         .dxtcLite > .dxtc-stripContainer .dxtc-tab {
             background-color: #FFFFFF;
             float: left;
             overflow: hidden;
             text-align: center;
             white-space: nowrap;
             color: #000000;
         }
           .dxtcLite > .dxtc-stripContainer .dxtc-tab:hover{
               background-color: #d9d9d9;
           }
           .dxtcLite > .dxtc-stripContainer .dxtc-link{
               color: #000000;
           }
           .row{
               padding-left:15%;
           }
    
         .close_icon {
             position: relative;
             font-family: FontAwesome !important;
             color: #FFFFFF !important;
             font-size: 200% !important;
                padding: 1px !important;
         
             text-decoration: none !important;
         }


             .close_icon:hover {
                 color: #d9d9d9 !important;
                
             }

             .close_icon::before {
                 content: "\f00d" !important;
             }
        
         /* Clear floats after the columns */
         .row:after {
             content: "";
             display: table;
             clear: both;
         }

         .popupanimation {
             width: 100%;
             height: 100%;
             position: fixed;
             overflow-y: auto;
             overflow-x: hidden;
             top: 0;
             left: 0;
             animation-duration: 0.5s;
             -webkit-animation-duration: 0.5s;
             animation-name: slide-down;
             -webkit-animation-name: slide-down;
             animation-fill-mode: both;
             -webkit-animation-fill-mode: both;
             animation-delay: 0;
             -webkit-animation-delay: 0;
         }
          /*...........................*/
         @Keyframes slide-down {
             0% {
                 transform: translate3d(0,-200%,0);
             }
             100% {
                 transform: translateZ(0);
             }
         }
        
         /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
        @media (max-width: 600px) {
          .col-25, .col-75, input[type=submit] {
            width: 100%;
            margin-top: 0;
          }
        }

        .dxpc-headerText{
            padding-top:8px !important;
        }
        .headerstyle{
            padding-bottom: 14.5px !important;
            padding-left: 15px !important;
            padding-right: 15px !important;
            padding-top: 14.5px !important;
        }
        .pagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #333333 !important;
            text-decoration: none !important;
            padding-left: 7px !important;
            padding-right: 7px !important;
        }
        .pagenumber:hover{
            background-color : #333333;
            color: #FFFFFF !important;
        }
        .currentpagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #FFFFFF !important;
            background-color: #333333;
            text-decoration: none !important;
        }
         .dxeButtonEditButton{
            background: none !important;
            border: none !important;
        }
         .dxWeb_pPrevDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNext {
             background: url('../content/images/iconmonstr-arrow-63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pPrev {
             background: url('../content/images/iconmonstr-arrow-64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNextDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }
     </style>
    <script type="text/javascript">
        function OnGetRowValues(Value) {
            if (Value[7]) {
                Value[7] = convertDate(Value[7] + '');
            }

            if (Value[8]) {
                Value[8] = convertDate(Value[8] + '');
            }

            if (Value[9]) {
                Value[9] = convertDate(Value[9] + '');
            }

            if (Value[10]) {
                Value[10] = convertDate(Value[10] + '');
            }

            if (Value[11]) {
                Value[11] = convertDate(Value[11] + '');
            }

            //Mohammad commented this because when updating an employee the title was being converted to date
            //if(Value[12]) {
            //    Value[12] = convertDate(Value[12] + '');
            //}
            GridViewPannel.PerformCallback(Value);
        }
        function RoleError() {
            alert("you can't add the same user twice");
        }
        function myFunc() {
            alert("Hello Mz");
        }

        function convertDate(d) {
            var parts = d.split(" ");
            var months = { Jan: "01", Feb: "02", Mar: "03", Apr: "04", May: "05", Jun: "06", Jul: "07", Aug: "08", Sep: "09", Oct: "10", Nov: "11", Dec: "12" };
            return months[parts[1]] + "/" + parts[2] + "/" + parts[3];
        }
        function submitemployeedetailspressed(s, e) {
            if (ASPxClientEdit.ValidateGroup('popupValidationGroup')) {
                if (firstnametextbox.GetText().indexOf(',') > -1 || lastnametextbox.GetText().indexOf(',') > -1 || titletextbox.GetText().indexOf(',') > -1 || emailtextbox.GetText().indexOf(',') > -1 || reemailtextbox.GetText().indexOf(',') > -1) {
                    window.alert("Text can not contain commas");

                }
                else {
                    if (emailtextbox.GetText() == reemailtextbox.GetText()) {
                        if (validateEmail(emailtextbox.GetText())) {//check email format here
                            
                            employeePopUp.Hide();
                            GridViewPannel.PerformCallback('Save');
                            //ASPxClientEdit.ClearEditorsInContainer(null, 'popupValidationGroup');
                           
                             
                           
                        }
                        else { window.alert('Invalid Email'); }
                    }
                    else { window.alert('Emails do not match'); }
                }

            }
            else {
                
            }
        }
        function checktextbox(s, e) {
            if (s.GetText().indexOf(',') > -1) {
                s.GetInputElement().style.color = "red";
            }
            else if (s.GetText() == "") {
                s.GetInputElement().style.color = "#818181"; //#818181
            }
            else {
                 s.GetInputElement().style.color = "black";
            }
        }
        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
        
    </script>
    <div style="overflow-y: auto;height: calc(100vh - 200px);">
    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { LoadingPanel.Hide(); }" />
    </dx:ASPxCallback>
    <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel"
        Modal="True">
    </dx:ASPxLoadingPanel>
    <dx:ASPxPanel ID="ASPxPanel1" runat="server" CssClass="detailPanelSmallHeader"></dx:ASPxPanel>
    <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" ClientInstanceName="GridViewPannel" Collapsible="false" SettingsLoadingPanel-Enabled ="true" OnCallback="ASPxCallbackPanel1_Callback" >
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">

                <dx:ASPxGridView ID="EmployeeGridView"
                    runat="server" AutoGenerateColumns="False"
                    ClientInstanceName="EmployeeGridView"
                    Width="100%"
                    AutoPostBack="true"
                    KeyFieldName="EmployeeID"
                    CssClass="newFont" 
                    >
                    <SettingsPager CurrentPageNumberFormat="{0}">
                    </SettingsPager>
                    <StylesPager>
                        <PageNumber CssClass="pagenumber"></PageNumber>
                        <CurrentPageNumber CssClass="currentpagenumber"></CurrentPageNumber>
                        <%--<DisabledButton></DisabledButton>
                        <Button> 
                            <backgroundimage imageurl="../content/images/iconmonstr-arrow-64-16.png" />
                        </Button>--%>
                        
                    </StylesPager>
                   
                    
                    <ClientSideEvents CustomButtonClick="function(s, e) {
                        if(e.buttonID == 'delete') {
                            if (confirm('Are you sure you want to delete this employee?')) {
                                s.GetRowValues(e.visibleIndex, 'EmployeeID', OnGetRowValues);                
                            }
                        }
                        else if (e.buttonID == 'update') {
                         
                           
                          
                            ASPxClientEdit.ClearEditorsInContainer(null, 'popupValidationGroup'); 
                            s.GetRowValues(e.visibleIndex, 'EmployeeID;First Name;Last Name;Discipline;Manager;AccessLevel;Hourly Rate;Passport Expiry;Emirates ID;Labor Card;AC. Card;Visa Expiry;Title;Email', OnGetRowValues);                     
                             
                        }
                        else {
                         
                            
                            ASPxClientEdit.ClearEditorsInContainer(null, 'popupValidationGroup');
                            s.GetRowValues(e.visibleIndex, 'EmployeeID;First Name', OnGetRowValues);    
                            employeePopUp.Show();
                        }
                        }
                        " />
                    
                    <Settings ShowTitlePanel ="true"  ShowFilterRow="true" ShowFilterRowMenu="true"/>
                    
                    <SettingsEditing EditFormColumnCount="4" Mode="Inline"/>
                    <Styles>
                        <AlternatingRow Enabled="true" />
                        <Header HorizontalAlign="Center"></Header>
                    </Styles>

                    <SettingsPopup>
                        <EditForm Width="100%" Modal="false"/>
                    </SettingsPopup>

                    <SettingsPager PageSize="50" />
                    <Paddings Padding="0px" />
                    <Border BorderWidth="0px" />
                    <BorderBottom BorderWidth="1px" />
                    <Settings ShowFooter="True" />
                    <Styles Header-Wrap="True" />
                    
                    <Columns>
                         <dx:GridViewCommandColumn ButtonType="Image">
                            <CustomButtons>
                                <dx:GridViewCommandColumnCustomButton ID="update">
                                    <Styles Style-CssClass="edit_icon">
<Style CssClass="edit_icon"></Style>
                                    </Styles>
                                </dx:GridViewCommandColumnCustomButton>
                                <dx:GridViewCommandColumnCustomButton runat="server" ID="delete" Text ="">
                                    <Styles Style-CssClass="delete_icon">
<Style CssClass="delete_icon"></Style>
                                    </Styles>
                                </dx:GridViewCommandColumnCustomButton>
<%--                                <dx:GridViewCommandColumnCustomButton Image-Height="30px" Image-Width="30px" ID="password">
                                    <Image Url="~/Content/Images/Icons/password_user.png"></Image>
                                </dx:GridViewCommandColumnCustomButton>--%>
                            </CustomButtons>
                            <HeaderTemplate>
                                <dx:ASPxButton ID="ASPxButton1" Image-Width="30px" Image-Height="30px" CausesValidation="false" RenderMode="Link" CssClass="new_icon" runat="server" AutoPostBack="false">
                                    <ClientSideEvents  Click="function(s, e) {
                                                               
                                                               
                                                                ASPxClientEdit.ClearEditorsInContainer(null, 'popupValidationGroup');
                                                                GridViewPannel.PerformCallback();
                                                            }" />
                                </dx:ASPxButton>
                            </HeaderTemplate>
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataColumn FieldName="First Name" HeaderStyle-Font-Size="Medium" CellStyle-HorizontalAlign="Left">
<HeaderStyle Font-Size="Medium"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                         </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn FieldName="Last Name" HeaderStyle-Font-Size="Medium" CellStyle-HorizontalAlign="Left">
<HeaderStyle Font-Size="Medium"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                         </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Visible="false" FieldName="Discipline" HeaderStyle-Font-Size="Medium" CellStyle-HorizontalAlign="Left">
<HeaderStyle Font-Size="Medium"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                         </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Visible="false" FieldName="Email" HeaderStyle-Font-Size="Medium" CellStyle-HorizontalAlign="Left">
<HeaderStyle Font-Size="Medium"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                         </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn FieldName="Manager" HeaderStyle-Font-Size="Medium" CellStyle-HorizontalAlign="Left">
<HeaderStyle Font-Size="Medium"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                         </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn FieldName="Title" HeaderStyle-Font-Size="Medium" >
<HeaderStyle Font-Size="Medium"></HeaderStyle>
                         </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Visible="false" FieldName="AccessLevel" HeaderStyle-Font-Size="Medium" >
<HeaderStyle Font-Size="Medium"></HeaderStyle>
                         </dx:GridViewDataColumn>
<%--                        <dx:GridViewDataColumn FieldName="Default Role" HeaderStyle-Font-Size="Medium" />--%>
                        <dx:GridViewDataColumn Visible="false" FieldName="Hourly Rate" Caption="Hourly Cost Rate" HeaderStyle-Font-Size="Medium" >
<HeaderStyle Font-Size="Medium"></HeaderStyle>
                         </dx:GridViewDataColumn>
                        <%--<dx:GridViewDataColumn Visible="false" FieldName="Salary" HeaderStyle-Font-Size="Medium" />--%>
                        <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd MMM yyyy" FieldName="Passport Expiry" CellStyle-HorizontalAlign="Center" UnboundType="DateTime"  HeaderStyle-Font-Size="Medium" >
<PropertiesDateEdit DisplayFormatString="dd MMM yyyy"></PropertiesDateEdit>

<HeaderStyle Font-Size="Medium"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                         </dx:GridViewDataDateColumn>
                        <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd MMM yyyy" FieldName="Emirates ID" CellStyle-HorizontalAlign="Center" HeaderStyle-Font-Size="Medium" >
<PropertiesDateEdit DisplayFormatString="dd MMM yyyy"></PropertiesDateEdit>

<HeaderStyle Font-Size="Medium"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                         </dx:GridViewDataDateColumn>
                        <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd MMM yyyy" FieldName="Labor Card" CellStyle-HorizontalAlign="Center" HeaderStyle-Font-Size="Medium" >
<PropertiesDateEdit DisplayFormatString="dd MMM yyyy"></PropertiesDateEdit>

<HeaderStyle Font-Size="Medium"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                         </dx:GridViewDataDateColumn>
                        <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd MMM yyyy" FieldName="AC. Card" CellStyle-HorizontalAlign="Center" HeaderStyle-Font-Size="Medium" >
<PropertiesDateEdit DisplayFormatString="dd MMM yyyy"></PropertiesDateEdit>

<HeaderStyle Font-Size="Medium"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                         </dx:GridViewDataDateColumn>
                        <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd MMM yyyy" FieldName="Visa Expiry" CellStyle-HorizontalAlign="Center" HeaderStyle-Font-Size="Medium" >
<PropertiesDateEdit DisplayFormatString="dd MMM yyyy"></PropertiesDateEdit>

<HeaderStyle Font-Size="Medium"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                         </dx:GridViewDataDateColumn>
                    </Columns>
                  
                </dx:ASPxGridView>
           
                <!-- Employees popup control-->
                <dx:ASPxPopupControl sID="EmployeePopUpControl" ID="EmployeePopUpControl"
                    HeaderText="Employee Details" HeaderStyle-BackColor="#37474F" HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White"
            
                    HeaderStyle-CssClass="headerstyle"
                    HeaderStyle-Font-Size="Large"  PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" runat="server" 
                    ClientInstanceName="employeePopUp"  
                    CssClass="popupanimation" 
                    EnableCallbackAnimation="true" 
                    PopupAnimationType="Auto" 
                    ShowCloseButton="true"
                     CloseButtonImage-Width="0px"
                     CloseButtonImage-Height="0px"
                     CloseButtonStyle-CssClass="close_icon"
                  
                     >
                    
                    <HeaderStyle HorizontalAlign="Center" BackColor="#37474F" Font-Size="Large" ForeColor="White">
                        <Paddings PaddingBottom="0px"></Paddings>
                        
                    </HeaderStyle>

                    <ContentCollection>
                        <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                           
                           <dx:ASPxPageControl ID="EmployeeTabPage"   Width="100%"  runat="server" EnableHierarchyRecreation="false">
                           <TabPages>
                               
                               <dx:TabPage Text="Basic" TabStyle-CssClass="Tab" ActiveTabStyle-CssClass="ActiveTab"   ActiveTabStyle-ForeColor="White" >
<ActiveTabStyle CssClass="ActiveTab" ForeColor="White"></ActiveTabStyle>
                                   
<TabStyle CssClass="Tab"></TabStyle>
                                   <ContentCollection>
                                       <dx:ContentControl ID="ContentControl1" runat="server"  Width="100%" Height="100%">
                           <div class="container">
                                 <dx:ASPxHiddenField ID="EmployeeIDHiddenField" Text="" runat="server"></dx:ASPxHiddenField>
                                <div class="row">
                                  <div class="col-10">
                                    <label runat="server" id="EmployeeIDLabel" for="fname">Employee ID</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxTextBox Width="100%"  CssClass="TextBox" ID="EmployeeIDTextBox" runat="server" NullText="Employee ID">
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="true" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                       </div>
                                      <div class="col-20">
                                <dx:ASPxButton runat="server"  CssClass="ResetPasswordButton" ID="ResetPasswordButton" Text="Reset Password" AutoPostBack="false" UseSubmitBehavior="false"
                                 ClientSideEvents-Click="function(s, e){
                                                ResetPassPopUp.Show();
                                }">
<ClientSideEvents Click="function(s, e){
                                                ResetPassPopUp.Show();
                                }"></ClientSideEvents>
                                          </dx:ASPxButton>
                                      </div>
                                
                                </div>
                               <br /> 
                                <div class="row">
                                  <div class="col-10">
                                    <label for="fname">First Name</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxTextBox Width="100%" MaxLength="32" ID="FirstNameTextBox" ClientInstanceName="firstnametextbox" runat="server" NullText="First Name">
                                        <ClientSideEvents   KeyUp ="checktextbox" />
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="true" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                  </div>
                                <div class="col-10"></div>
                                <%--</div>
                                <div class="row">--%>
                                  <div class="col-10">
                                    <label for="lname">Last Name</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxTextBox Width="100%" MaxLength="32" CssClass="TextBox" ID="LastNameTextBox" ClientInstanceName="lastnametextbox" runat="server" NullText="Last Name">
                                          <ClientSideEvents   KeyUp ="checktextbox" />
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic" >
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                  </div>
                                  </div>
                                <div class="row">
                                  <div class="col-10">
                                    <label for="Title">Title</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxTextBox Width="100%" MaxLength="30" CssClass="TextBox" NullText="Title" ClientInstanceName="titletextbox" ID="TitleTextBox" runat="server">
                                          <ClientSideEvents   KeyUp ="checktextbox" />
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                  </div>
                                     <div class="col-10"></div>
                                <%--</div>
                                <div class="row">--%>
                                  <div class="col-10">
                                    <label for="discipline">Discipline</label>
                                  </div>
                                  <div class="col-25">
                                      <dx:ASPxComboBox Width="100%" CssClass="ComboBox" ID="DisciplineComboBox" ValueField="RoleName" TextField="RoleName" runat="server">
                                          <DropDownButton>
                                              <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                                          </DropDownButton>
                                          <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                              <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxComboBox>
                                  </div>
                                </div>
                               <br /> 
                               <div class="row">
                                  <div class="col-10">
                                    <label for="hourlycostrate">Hourly Cost Rate</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxSpinEdit  Width="100%" MaxLength="11" CssClass="TextBox" ID="HourlyRateTextBox" runat="server" NullText="Hourly Cost Rate">
                                         <SpinButtons ShowIncrementButtons="False" ShowLargeIncrementButtons="False" />
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxSpinEdit>
                                  </div>
                                </div>
                               <br />
                                <div class="row">
                                  <div class="col-10">
                                    <label for="Email">Email</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxTextBox Width="100%" MaxLength="255" CssClass="TextBox" ID="EmailTextBox" ClientInstanceName="emailtextbox" runat="server">
                                          <ClientSideEvents   KeyUp ="checktextbox" />
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                  </div>
                                    <div class="col-10"></div>
                                  <div class="col-10">
                                    <label for="Email">Confirm Email</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxTextBox Width="100%" MaxLength="255" CssClass="TextBox" ID="ReEmailTextBox"  ClientInstanceName="reemailtextbox" runat="server">
                                          <ClientSideEvents   KeyUp ="checktextbox" />
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                  </div>
                               
                                </div>
                               <br />
                                <div class="row">
                                  <div class="col-10">
                                    <label for="Manager">Manager</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxComboBox Width="100%" CssClass="ComboBox" ID="ManagerComboBox" ValueField="EmployeeID" TextField="Manager" runat="server">
                                        <DropDownButton>
                                              <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                                          </DropDownButton>
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxComboBox>
                                  </div>
                                     <div class="col-10"></div>
                               <%-- </div>
                                <div class="row">--%>
                                  <div class="col-10">
                                    <label for="AccessLevel">Access Level</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxComboBox Width="100%" CssClass="ComboBox" ID="AccessLevelComboBox" ValueField="AccessLevelID" TextField="AccessLevelName" runat="server">
                                        <DropDownButton>
                                              <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                                          </DropDownButton>
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxComboBox>
                                  </div>
                                </div>
                               <br />
                                <div class="row">
                                  <div class="col-10">
                                    <label for="PassportExpiry">Passport Expiry</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxDateEdit Width="100%" CssClass="DateEdit" PopupHorizontalAlign="RightSides" OnCalendarDayCellPrepared="ASPXDateEdit_CustomCell" EditFormatString="dd/MM/yyyy" DisplayFormatString="dd/MM/yyyy" ID="PassportExpiryASPxDateEdit" runat="server">
                                        <DropDownButton>
                                              <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                                          </DropDownButton>
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxDateEdit>
                                  </div>
                                     <div class="col-10"></div>
                               <%-- </div>
                                <div class="row">--%>
                                  <div class="col-10">
                                    <label for="EmiratesID">Emirates ID</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxDateEdit Width="100%" CssClass="DateEdit" PopupHorizontalAlign="RightSides" OnCalendarDayCellPrepared="ASPXDateEdit_CustomCell" EditFormatString="dd/MM/yyyy" DisplayFormatString="dd/MM/yyyy" ID="EmiratesIDDateEdit" runat="server">
                                        <DropDownButton>
                                              <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                                          </DropDownButton>
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxDateEdit>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-10">
                                    <label for="LaborCard">Labor Card</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxDateEdit Width="100%" CssClass="DateEdit" PopupHorizontalAlign="RightSides" OnCalendarDayCellPrepared="ASPXDateEdit_CustomCell" EditFormatString="dd/MM/yyyy" DisplayFormatString="dd/MM/yyyy" ID="LaborCardDateEdit" runat="server">
                                        <DropDownButton>
                                              <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                                          </DropDownButton>
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxDateEdit>
                                  </div>
                                     <div class="col-10"></div>
                               <%-- </div>
                                <div class="row">--%>
                                    <div class="col-10">
                                        <label for="ACCard">AC. Card</label>
                                    </div>
                                    <div class="col-25">
                                        <dx:ASPxDateEdit Width="100%" CssClass="DateEdit" PopupHorizontalAlign="RightSides" OnCalendarDayCellPrepared="ASPXDateEdit_CustomCell" EditFormatString="dd/MM/yyyy" DisplayFormatString="dd/MM/yyyy" ID="ACCardDateEdit" runat="server">
                                            <DropDownButton>
                                              <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                                          </DropDownButton>
                                            <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                                <RequiredField IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxDateEdit>
                                    </div>
                                </div>
                               <div class="row">
                                   <div class="col-10">
                                       <label for="VisaExpiry">Visa Expiry</label>
                                   </div>
                                   <div class="col-25">
                                       <dx:ASPxDateEdit Width="100%" CssClass="DateEdit" PopupHorizontalAlign="RightSides" OnCalendarDayCellPrepared="ASPXDateEdit_CustomCell" EditFormatString="dd/MM/yyyy" DisplayFormatString="dd/MM/yyyy" ID="VisaExpiryDateEdit" runat="server">
                                           <DropDownButton>
                                              <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                                          </DropDownButton>
                                           <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                               <RequiredField IsRequired="True" />
                                           </ValidationSettings>
                                       </dx:ASPxDateEdit>
                                   </div>
                               </div>
                               <br />
                               <div class="row">
                                   <dx:ASPxButton CssClass="CancelButton" ID="CancelButton" Text="Cancel" AutoPostBack="false" UseSubmitBehavior="false" runat="server">
                                       <ClientSideEvents Click="function(s, e) {
                                                            
                                                            employeePopUp.Hide();
                                                            
           
             
                                                        }"></ClientSideEvents>
                                   </dx:ASPxButton>
                                   <dx:ASPxButton CssClass="SubmitButton" ID="SubmitButton" ClientInstanceName="submitbutton" Text="Submit" AutoPostBack="false" UseSubmitBehavior="true"
                                       ClientSideEvents-Click="submitemployeedetailspressed" runat="server">
                                       <ClientSideEvents Click="submitemployeedetailspressed"></ClientSideEvents>
                                   </dx:ASPxButton>

                               </div>
                           </div>


                                       </dx:ContentControl>
                                   </ContentCollection>
                               </dx:TabPage>
                               <dx:TabPage Text="Salary" TabStyle-CssClass="Tab" ActiveTabStyle-CssClass="ActiveTab" ActiveTabStyle-ForeColor="White">
                                   <ActiveTabStyle CssClass="ActiveTab" ForeColor="White"></ActiveTabStyle>

                                   <TabStyle CssClass="Tab"></TabStyle>
                                   <ContentCollection>
                                       <dx:ContentControl ID="ContentControlSalary" runat="server">
                                           <div class="container">
                                               <div class="row">
                                                   <div class="col-10">
                                                       <label runat="server" id="Label5">Employee ID</label>
                                                   </div>
                                                   <div class="col-25">
                                                       <dx:ASPxTextBox Width="100%" Enabled="false" CssClass="TextBox" ID="EmployeeIDSalaryTextBox" runat="server" NullText="Employee ID">
                                                           <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                                               <RequiredField IsRequired="true" />
                                                           </ValidationSettings>
                                                       </dx:ASPxTextBox>
                                                   </div>
                                               </div>
                                               <br />
                                               <div class="row">
                                                   <div class="col-10">
                                                       <label runat="server" id="Label1">Basic Salary</label>
                                                   </div>
                                                   <div class="col-25">
                                                       <dx:ASPxTextBox Width="100%" MaxLength="15" Enabled="false" CssClass="TextBox" ID="BasicSalaryTextBox" runat="server" NullText="Basic Salary">
                                                           <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                                               <RequiredField IsRequired="true" />
                                                           </ValidationSettings>
                                                       </dx:ASPxTextBox>
                                                   </div>
                                               </div>
                                               <br />
                                               <div class="row">
                                                   <div class="col-10">
                                                       <label runat="server" id="Label2">Transportation</label>
                                                   </div>
                                                   <div class="col-25">
                                                       <dx:ASPxTextBox Width="100%" MaxLength="15" Enabled="false" CssClass="TextBox" ID="TransportationTextBox" runat="server" NullText="Transportation">
                                                           <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                                               <RequiredField IsRequired="true" />
                                                           </ValidationSettings>
                                                       </dx:ASPxTextBox>
                                                   </div>
                                               </div>
                                               <br />
                                               <div class="row">
                                                   <div class="col-10">
                                                       <label runat="server" id="Label3">Housing</label>
                                                   </div>
                                                   <div class="col-25">
                                                       <dx:ASPxTextBox Width="100%" MaxLength="15" Enabled="false" CssClass="TextBox" ID="HousingTextBox" runat="server" NullText="Housing">
                                                           <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                                               <RequiredField IsRequired="true" />
                                                           </ValidationSettings>
                                                       </dx:ASPxTextBox>
                                                   </div>
                                               </div>
                                               <br />
                                               <div class="row">
                                                   <div class="col-10">
                                                       <label runat="server" id="Label4">Total Salary</label>
                                                   </div>
                                                   <div class="col-25">
                                                       <dx:ASPxTextBox Width="100%" Enabled="false" MaxLength="15" CssClass="TextBox" ID="TotalSalaryTextBox" runat="server" NullText="Total Salary">
                                                           <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                                               <RequiredField IsRequired="true" />
                                                           </ValidationSettings>
                                                       </dx:ASPxTextBox>
                                                   </div>
                                               </div>


                                           </div>


                                       </dx:ContentControl>
                                   </ContentCollection>
                               </dx:TabPage>
                                   </TabPages>
                               </dx:ASPxPageControl>
                              
                               </dx:PopupControlContentControl>

                   </ContentCollection>
                
                </dx:ASPxPopupControl>
                  

                <dx:ASPxPopupControl runat="server" sID="ResetPasswordPopUpControl"  HeaderText="Are you sure you want to reset the employee's password?" ClientInstanceName="ResetPassPopUp" 
                    HeaderStyle-BackColor="#4CAF50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White" HeaderStyle-Paddings-Padding="15px"
                    HeaderStyle-Font-Size="Large" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" 
                      Width="100px" Height="100px"
                    >
<HeaderStyle HorizontalAlign="Center" BackColor="#4CAF50" Font-Size="Large" ForeColor="White">
<Paddings Padding="15px"></Paddings>
</HeaderStyle>
                    <ContentCollection>
                        <dx:PopupControlContentControl ID="PopupControlContentControlResetPassword" runat="server">
                            <div >
                                <div>
                                    <table style="width: 100%;">
                                      <tr>  
                                   <td style="width: 50%">
                                    
                                       
                                    <dx:ASPxButton CssClass="SubmitButton" ID="ASPxButtonSubmitResetPass" Text="Submit" AutoPostBack="false" 
                                                    ClientSideEvents-Click="function(s, e) {
                                                            ResetPassPopUp.Hide();
                                                            
                                                           
                                                            employeePopUp.Hide();
                                                             GridViewPannel.PerformCallback('ResetPassword');
                                                           
                                                            window.alert('Email sent successfully to employee to reset password');
                                                        }" runat="server">
<ClientSideEvents Click="function(s, e) {
                                                            ResetPassPopUp.Hide();
                                                            
                                                           
                                                            employeePopUp.Hide();
                                                             GridViewPannel.PerformCallback(&#39;ResetPassword&#39;);
                                                           
                                                          
                                                        }"></ClientSideEvents>
                                       </dx:ASPxButton>
                                   </td>
                                            <td style="width: 50%">
                                    <dx:ASPxButton CssClass="CancelButtonPassReset" ID="ASPxButtonCancelResetPass" Text="Cancel" AutoPostBack="false" 
                                                    ClientSideEvents-Click="function(s, e) {
                                                            ResetPassPopUp.Hide();
                                                        }" runat="server">
<ClientSideEvents Click="function(s, e) {
                                                            ResetPassPopUp.Hide();
                                                        }"></ClientSideEvents>
                                                </dx:ASPxButton>
                                                </td>
                                        </tr>
                                         </table>
                                </div>
                            </div>
                        </dx:PopupControlContentControl>

                    </ContentCollection>
                </dx:ASPxPopupControl>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
    <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="ASPxGridView1"/>
        </div>
</asp:Content>