﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DXOmniVistaTimeEngine;
using System.Web.Security;
using DevExpress.Web;
using System.Text;
using DevExpress.Export;
using DevExpress.XtraPrinting;
using System.Xml;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Text.RegularExpressions;

public partial class EmployeeAssignment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);

        //Filling the RoleGridView
        //Mohammad changed the following 
        DataTable employeeAssignment = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["EmployeeAssignment"], "");
        //DataTable employeeAssignment = DataAccess.GetDataTableBySqlSyntax("SELECT EA.EmployeeAssignmentID, P.ProjectID + ' - ' + P.ProjectName as 'Project', E.EmpFName + ' ' + E.EmpLName AS 'Employee',R.RoleName as 'Role', EA.Rate AS 'Cost Rate', EA.OVRate AS 'OT Cost Rate', EA.DefaultRole FROM dbo.EmployeeAssignment EA INNER JOIN Project P ON P.ProjectID = EA.ProjectID  INNER JOIN Employee E ON E.EmployeeID = EA.EmployeeID INNER JOIN Role R ON R.RoleID = EA.RoleID", "");
        //...
        employeeAssignment.PrimaryKey = new DataColumn[] { employeeAssignment.Columns["EmployeeAssignmentID"] };
        Session["GridDataEmployeeAssignment"] = employeeAssignment;

        EmployeeAssignmentGridView.DataSource = employeeAssignment;
        EmployeeAssignmentGridView.DataBind();

        //Filling the projects combobox
        //Mohammad eddited the following
        DataTable projects = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["Projects"], "");
        //DataTable projects = DataAccess.GetDataTableBySqlSyntax("SELECT P.ProjectID, P.ProjectCode, P.ProjectName, P.ProjectID + ' - ' + P.ProjectName as 'Project', (SELECT ClientCompany FROM Client C WHERE P.ClientID = C.ClientID) AS 'Client', PD.Manager, P.ProjectStartDate AS 'Project Start Date', P.ProjectPONum AS 'Project PO Number', P.MessageOnInvoice AS 'Message On Invoice' FROM Project P LEFT JOIN ProjectDetails PD ON P.ProjectID = PD.ProjectID WHERE ProjectName !='' AND ProjectName IS NOT NULL", "");
        //.....
        GridViewDataComboBoxColumn projectsColumn = (GridViewDataComboBoxColumn)this.EmployeeAssignmentGridView.DataColumns["Project"];

        projectsColumn.PropertiesComboBox.DataSource = projects;
        projectsColumn.PropertiesComboBox.ValueField = "ProjectID";
        projectsColumn.PropertiesComboBox.TextField = "Project";

        //Filling the employees combobox
        //Mohammad eddited the following
        DataTable employees = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["EmployeesBrief"], "");
        //DataTable employees = DataAccess.GetDataTableBySqlSyntax("SELECT E.EmployeeID, E.EmpFName + ' '+ E.EmpLName as 'EmployeeName' FROM dbo.Employee E WHERE E.EmpFName !='' AND E.EmpFName IS NOT NULL AND E.EmpLName !='' AND E.EmpLName IS NOT NULL", "");
        GridViewDataComboBoxColumn employeesColumn = (GridViewDataComboBoxColumn)this.EmployeeAssignmentGridView.DataColumns["Employee"];

        employeesColumn.PropertiesComboBox.DataSource = employees;
        employeesColumn.PropertiesComboBox.ValueField = "EmployeeID";
        employeesColumn.PropertiesComboBox.TextField = "EmployeeName";

        //Filling the roles combobox
        
        DataTable roles = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["Roles"], "");

        GridViewDataComboBoxColumn rolesColumn = (GridViewDataComboBoxColumn)this.EmployeeAssignmentGridView.DataColumns["Role"];

        rolesColumn.PropertiesComboBox.DataSource = roles;
        rolesColumn.PropertiesComboBox.ValueField = "RoleID";
        rolesColumn.PropertiesComboBox.TextField = "RoleName";

    }
    protected void ASPxCallbackPanel1_Callback(object sender, CallbackEventArgsBase e)
    {

    }

    protected void EmployeeAssignmentGridView_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int DefaultRole = 0;

        if (e.NewValues["DefaultRole"] != null)
        {
            if ((bool)e.NewValues["DefaultRole"] == true)
            {
                DefaultRole = 1;
            }
            else
            {
                DefaultRole = 0;
            }
        }

        //Updating the grid view
        DataTable oldEmployeeAssignments = (DataTable)Session["GridDataEmployeeAssignment"];
        DataRow found = oldEmployeeAssignments.Rows.Find(e.Keys[0]);
       
        
        GridViewDataComboBoxColumn rolesColumn = (GridViewDataComboBoxColumn)this.EmployeeAssignmentGridView.DataColumns["Role"];
        GridViewDataComboBoxColumn projectsColumn = (GridViewDataComboBoxColumn)this.EmployeeAssignmentGridView.DataColumns["Project"];
        GridViewDataComboBoxColumn employeesColumn = (GridViewDataComboBoxColumn)this.EmployeeAssignmentGridView.DataColumns["Employee"];


        //Mohammad added the following
        String newkey ="";
        if (projectsColumn.PropertiesComboBox.Items.FindByValue(e.NewValues["Project"]) == null && employeesColumn.PropertiesComboBox.Items.FindByValue(e.NewValues["Employee"]) == null && rolesColumn.PropertiesComboBox.Items.FindByValue(e.NewValues["Role"]) == null) { }
        else {
            if (projectsColumn.PropertiesComboBox.Items.FindByValue(e.NewValues["Project"]) != null)
            {
                newkey = e.NewValues["Project"].ToString();
            }
            else
            {
                DataTable projectid = DataAccess.GetDataTableBySqlSyntax("SELECT P.ProjectID FROM Project P WHERE P.ProjectID + ' - ' + P.ProjectName = '" + found[1] + "'", "");
                newkey = projectid.Rows[0][0].ToString();
            }
            if (employeesColumn.PropertiesComboBox.Items.FindByValue(e.NewValues["Employee"]) != null)
            {
                newkey = newkey + e.NewValues["Employee"].ToString();
            }
            else
            {
                DataTable employeeid = DataAccess.GetDataTableBySqlSyntax("SELECT EmployeeID FRom Employee WHERE EmpFName + ' ' + EmpLName = '"+found[2] +"'", "");
                newkey = newkey + employeeid.Rows[0][0].ToString();
            }
            if (rolesColumn.PropertiesComboBox.Items.FindByValue(e.NewValues["Role"]) != null)
            {
                newkey = newkey + e.NewValues["Role"].ToString();
            }
            else
            {
                DataTable roleid = DataAccess.GetDataTableBySqlSyntax("SELECT RoleID from dbo.Role Where RoleName='"+found[3] +"'", "");
                newkey = newkey + roleid.Rows[0][0].ToString();
            }
            // initialkey = e.Keys["EmployeeAssignmentID"].ToString();

            for (int i = 0; i < oldEmployeeAssignments.Rows.Count; i++)
            {
                var test = oldEmployeeAssignments.Rows[i][0].ToString();

                //Check if there is a row with the same project, employee and role
                if (oldEmployeeAssignments.Rows[i][0].ToString() == newkey )
                {
                    EmployeeAssignmentGridView.CancelEdit();

                    Exception ex = new Exception("You cannot insert a row with the same project, employee and role. Please correct this record to save.");
                    throw (ex);
                }
            }
        }
        //.....................................




        //Checking if the user selected something from combobox or not
        if (projectsColumn.PropertiesComboBox.Items.FindByValue(e.NewValues["Project"]) != null)
        {
            found[1] = projectsColumn.PropertiesComboBox.Items.FindByValue(e.NewValues["Project"]).Text;

            string updateEmployeeAssignmentProjectSql = "UPDATE EmployeeAssignment SET ProjectID ='" + e.NewValues["Project"] + "'" + " WHERE EmployeeAssignmentID='" + e.Keys["EmployeeAssignmentID"] + "'";
            DataAccess.ExecuteSqlStatement(updateEmployeeAssignmentProjectSql, "");
        }

        if (employeesColumn.PropertiesComboBox.Items.FindByValue(e.NewValues["Employee"]) != null)
        {
            found[2] = employeesColumn.PropertiesComboBox.Items.FindByValue(e.NewValues["Employee"]).Text;

            string updateEmployeeAssignmentEmployeeSql = "UPDATE EmployeeAssignment SET EmployeeID ='" + e.NewValues["Employee"] + "' WHERE EmployeeAssignmentID='" + e.Keys["EmployeeAssignmentID"] + "'";
            DataAccess.ExecuteSqlStatement(updateEmployeeAssignmentEmployeeSql, "");
        }

        if (rolesColumn.PropertiesComboBox.Items.FindByValue(e.NewValues["Role"]) != null)
        {
            found[3] = rolesColumn.PropertiesComboBox.Items.FindByValue(e.NewValues["Role"]).Text;

            string updateEmployeeAssignmentRoleSql = "UPDATE EmployeeAssignment SET RoleID ='" + e.NewValues["Role"] + "' WHERE EmployeeAssignmentID='" + e.Keys["EmployeeAssignmentID"] + "'";
            DataAccess.ExecuteSqlStatement(updateEmployeeAssignmentRoleSql, "");
        }

        if (DefaultRole == 1)
        {
            for (int i = 0; i < oldEmployeeAssignments.Rows.Count; i++)
            {
               // if (newkey != oldEmployeeAssignments.Rows[i][0].ToString()) {//Mohammad added this so that the changed one will not update itself
                    if (oldEmployeeAssignments.Rows[i][2].ToString() == found[2].ToString() && oldEmployeeAssignments.Rows[i][1].ToString() == found[1].ToString() && (bool)oldEmployeeAssignments.Rows[i][6] == true)
                    {
                        oldEmployeeAssignments.Rows[i][6] = false;

                        //Modify this role in the database
                        //Mohammad changed and added the following
                        DataTable employeeid = DataAccess.GetDataTableBySqlSyntax("SELECT EmployeeID FRom Employee WHERE EmpFName + ' ' + EmpLName = '" + found[2] + "'", "");
                        DataTable projectid = DataAccess.GetDataTableBySqlSyntax("SELECT P.ProjectID FROM Project P WHERE P.ProjectID + ' - ' + P.ProjectName = '" + found[1] + "'", "");
                        string updateEmployeeAssignmentSql = "UPDATE EmployeeAssignment SET DefaultRole = '0' WHERE EmployeeID = '" + employeeid.Rows[0][0] + "' AND ProjectID = '" + projectid.Rows[0][0] + "'";
                        //........................................
                        DataAccess.ExecuteSqlStatement(updateEmployeeAssignmentSql, "");
                    }
               // }
            }
        }

        found[4] = e.NewValues["Cost Rate"].ToString();
        found[5] = e.NewValues["OT Cost Rate"];
        found[6] = DefaultRole;

        string updatecheckboxsql = "UPDATE EmployeeAssignment SET DefaultRole='" + DefaultRole + "'WHERE EmployeeAssignmentID = '" + e.Keys["EmployeeAssignmentID"] + "'";
        DataAccess.ExecuteSqlStatement(updatecheckboxsql, "");
        Session["GridDataEmployeeAssignment"] = oldEmployeeAssignments;
        EmployeeAssignmentGridView.DataSource = oldEmployeeAssignments;

        //Mohammad commented this out because it is not needed
        //string newEmployeeAssignmentID = found[1].ToString() + found[2] + found[3];
        //string updateEmployeeAssignmentsSql = "UPDATE EmployeeAssignment SET Rate ='" + e.NewValues["Cost Rate"] + "'" + ", EmployeeAssignmentID='" + newEmployeeAssignmentID + "'" + ", OVRate='" + e.NewValues["OT Cost Rate"] + "'" + ", EmployeeAssignmentID='" + newEmployeeAssignmentID + "'" + ", DefaultRole='" + DefaultRole + "'" + " WHERE EmployeeAssignmentID='" + e.Keys["EmployeeAssignmentID"] + "'";
        //DataAccess.ExecuteSqlStatement(updateEmployeeAssignmentsSql, "");
        //.....................
        CancelEditing(e, this.EmployeeAssignmentGridView);
    }

    protected void EmployeeAssignmentGridView_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        DataTable oldEmployeeAssignments = (DataTable)Session["GridDataEmployeeAssignment"];
        oldEmployeeAssignments.Rows.Remove(oldEmployeeAssignments.Rows.Find(e.Keys[EmployeeAssignmentGridView.KeyFieldName]));

        Session["GridDataEmployeeAssignment"] = oldEmployeeAssignments;
        EmployeeAssignmentGridView.DataSource = oldEmployeeAssignments;

        string deleteEmployeeAssignmentSql = "DELETE From EmployeeAssignment WHERE EmployeeAssignmentID ='" + e.Keys["EmployeeAssignmentID"] + "'";
        DataAccess.ExecuteSqlStatement(deleteEmployeeAssignmentSql, "");

        CancelEditing(e, this.EmployeeAssignmentGridView);
    }

    protected void EmployeeAssignmentGridView_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        //Converting DefaultRole column from null or boolean to number
        int DefaultRole = 0;

        if (e.NewValues["DefaultRole"] != null)
        {
            if ((bool)e.NewValues["DefaultRole"] == true)
            {
                DefaultRole = 1;
            }
            else
            {
                DefaultRole = 0;
            }
        }

        //Checking if one of the required fields is empty
        if (e.NewValues["Project"] == null || e.NewValues["Employee"] == null || e.NewValues["Role"] == null)
        {
            EmployeeAssignmentGridView.CancelEdit();

            Exception ex = new Exception("Project, Employee and Role are needed to save record. Please correct this record to save.");
            throw (ex);
        }

        // Filling the added data in grid view
        DataTable oldEmployeeAssignment = (DataTable)Session["GridDataEmployeeAssignment"];
        DataRow newEmployeeAssignmentRow = oldEmployeeAssignment.NewRow();

        GridViewDataComboBoxColumn rolesColumn = (GridViewDataComboBoxColumn)this.EmployeeAssignmentGridView.DataColumns["Role"];
        GridViewDataComboBoxColumn projectsColumn = (GridViewDataComboBoxColumn)this.EmployeeAssignmentGridView.DataColumns["Project"];
        GridViewDataComboBoxColumn employeesColumn = (GridViewDataComboBoxColumn)this.EmployeeAssignmentGridView.DataColumns["Employee"];
        
        DataTable EmployeeAssignments = (DataTable) Session["GridDataEmployeeAssignment"];

        newEmployeeAssignmentRow[0] = projectsColumn.PropertiesComboBox.Items.FindByValue(e.NewValues["Project"]).Value.ToString() + employeesColumn.PropertiesComboBox.Items.FindByValue(e.NewValues["Employee"]).Value + rolesColumn.PropertiesComboBox.Items.FindByValue(e.NewValues["Role"]).Value;
        newEmployeeAssignmentRow[1] = projectsColumn.PropertiesComboBox.Items.FindByValue(e.NewValues["Project"]).Text;
        newEmployeeAssignmentRow[2] = employeesColumn.PropertiesComboBox.Items.FindByValue(e.NewValues["Employee"]).Text;
        newEmployeeAssignmentRow[3] = rolesColumn.PropertiesComboBox.Items.FindByValue(e.NewValues["Role"]).Text;
        
        if(e.NewValues["Cost Rate"] != null) {
            newEmployeeAssignmentRow[4] = e.NewValues["Cost Rate"].ToString();
        }
        else {
            newEmployeeAssignmentRow[4] = 0;
        }
        if(e.NewValues["OT Cost Rate"] != null) {
            newEmployeeAssignmentRow[5] = e.NewValues["OT Cost Rate"];
        }
        else {
            newEmployeeAssignmentRow[5] = 0;
        }
        newEmployeeAssignmentRow[6] = DefaultRole;

        //Validation

        for (int i = 0; i < oldEmployeeAssignment.Rows.Count; i++)
        {
            var test1 = oldEmployeeAssignment.Rows[i][0].ToString();
            var test2 = newEmployeeAssignmentRow[0].ToString();

            //Check if there is a row with the same project, employee and role
            String old = oldEmployeeAssignment.Rows[i][0].ToString();
            String neww = newEmployeeAssignmentRow[0].ToString();
            if (oldEmployeeAssignment.Rows[i][0].ToString() == newEmployeeAssignmentRow[0].ToString())
            {
                EmployeeAssignmentGridView.CancelEdit();

                Exception ex = new Exception("You cannot insert a row with the same project, employee and role. Please correct this record to save.");
                throw (ex);
            }
        }

        //Resetting the other roles
        if (DefaultRole == 1)
        {
            for (int i = 0; i < oldEmployeeAssignment.Rows.Count; i++)
            {
                //Check if the row is for the same employee and for the same project
                if (oldEmployeeAssignment.Rows[i][2].ToString() == newEmployeeAssignmentRow[2].ToString() && oldEmployeeAssignment.Rows[i][1].ToString() == newEmployeeAssignmentRow[1].ToString() && (bool)oldEmployeeAssignment.Rows[i][6] == true)
                {
                    oldEmployeeAssignment.Rows[i][6] = false;

                    //Modify this role in the database
                    string updateEmployeeAssignmentSql = "UPDATE EmployeeAssignment SET DefaultRole = '0' WHERE EmployeeID = '" + e.NewValues["Employee"] + "' AND ProjectID = '" + e.NewValues["Project"] + "'";
                    DataAccess.ExecuteSqlStatement(updateEmployeeAssignmentSql, "");
                }
            }
        }

        oldEmployeeAssignment.Rows.InsertAt(newEmployeeAssignmentRow, 0);

        Session["GridDataEmployeeAssignment"] = oldEmployeeAssignment;
        EmployeeAssignmentGridView.DataSource = oldEmployeeAssignment;
        EmployeeAssignmentGridView.DataBind();

        //Inserting the employeeassignment in the database
        string createEmployeeAssignmentSql = "INSERT INTO EmployeeAssignment (EmployeeAssignmentID,ProjectID,EmployeeID,RoleID,Rate,OVRate,DefaultRole) VALUES ('" + e.NewValues["Project"] + e.NewValues["Employee"] + e.NewValues["Role"] + "','" + e.NewValues["Project"] + "','" + e.NewValues["Employee"] + "','" + e.NewValues["Role"] + "','" + newEmployeeAssignmentRow[4] + "','" + newEmployeeAssignmentRow[5] + "','" + Convert.ToByte(newEmployeeAssignmentRow[6]) + "')";
        DataAccess.ExecuteSqlStatement(createEmployeeAssignmentSql, "");

        CancelEditing(e, this.EmployeeAssignmentGridView);
    }

    protected void EmployeeAssignmentGridView_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    {

    }

    protected void CancelEditing(CancelEventArgs e, ASPxGridView gridView)
    {
        e.Cancel = true;
        gridView.CancelEdit();
    }
}