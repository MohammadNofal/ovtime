﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/OVMaster.master" CodeFile="SetupClient.aspx.cs" Inherits="SetupClient" %>

<asp:Content ID="header_" ContentPlaceHolderID="Header" runat="server">
<h4><span class="text-semibold">Setup / Clients</span></h4>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="ContentPage" runat="server">

    <style>
         .dxWeb_pPrevDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNext {
             background: url('../content/images/iconmonstr-arrow-63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pPrev {
             background: url('../content/images/iconmonstr-arrow-64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNextDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }
        .month1_border 
        {
            border-left-width: 2px !important;
        }
        .head_container 
        {
            background-color: #EEE;
            float: right;
            margin-right: 7.8%;
        }

        .Month1_css 
        {
            border-style: solid;
            border-width: medium;
            float: right;
            background-color: red;
        }
        .Month2_css 
        {
            border-style: solid;
            border-width: medium;
            float: right;
        }
        .delete_icon 
        {
            position: relative;
            font-family: "Courier New" !important;
            color: #ee784a !important;
            font-size: 200% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .delete_icon:hover 
        {
            color: #FF0000 !important;
        }
        .delete_icon::before 
        {
            content: "\000D7" !important;
        }
        .new_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 20px !important;
            padding: 1px 1px;
            text-decoration: none !important;
        }

        .new_icon:hover 
        {
            color: #7373FF !important;
        }
        .new_icon:before 
        {
           content: "\f067" !important;
        }
        .edit_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #f3cb76 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .edit_icon:hover 
        {
            color: #FF8000 !important;
        }
        .edit_icon:before 
        {
            content: "\f044" !important;
        }
        .save_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
            text-align: center !important;
        }

        .save_icon:before 
        {
            content: "\f0c7" !important;
        }
        .location_icon
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
        }
        .location_icon:before 
        {
            content: "\f041" !important;
        }
        .book_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
        }
        .book_icon:before 
        {
            content: "\f02d" !important;
        }
        .cancel_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #ee784a !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .cancel_icon:hover 
        {
            color: #FF8080 !important;
        }
        .cancel_icon:before
        {
            content: "\f0e2" !important;
        }
        .update_icon
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .update_icon:hover 
        {
            color: #7373FF !important;
        }
        .update_icon:before 
        {
            content: "\f05d " !important;
        }
        .header 
        {
            font-size: 1.25em !important;
        }
        .ProjectDescription 
        {
            font-size: 14px !important;
        }

        .newFont * 
        {
            font-family: Calibri;
            font-size: 16px;
        }

        .TextBox {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }

        .ComboBox {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }

        .DateEdit {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }

        label {
          padding: 12px 12px 12px 0;
          display: inline-block;
        }

        /* Style the container */
        .container {
          border-radius: 5px;
          background-color: #f2f2f2;
          padding: 20px;
          width: 100%; /*Mohammad added this*/
         height: 100%;

        }

        /* Floating column for labels: 25% width */
        .col-25 {
          float: left;
          width: 25%;
          margin-top: 6px;
        }
        .col-10{
                 float: left;
          width: 10%;
          margin-top: 6px;
           }
          .row{
               padding-left:15%;
           }
        /* Floating column for inputs: 75% width */
        .col-75 {
          float: left;
          width: 75%;
          margin-top: 6px;
        }
        

         .SubmitButton {
             background-image: none !important;
             background-color: #4CAF50;
             color: white;
             padding: 6px 10px;
             border: none;
             border-radius: 4px;
             cursor: pointer;
             float: right;
             margin-top: 10px;
             margin-right: 10px;
         }
          .SubmitButton:hover{
            background-color: #419544;

        }
        .CancelButton {
             background-image: none !important;
             background-color: #ccc;
             color: black;
             padding: 6px 10px;
             border: none;
             border-radius: 4px;
             cursor: pointer;
             float: right;
             margin-top: 10px;
        }
         .CancelButton:hover{
             background-color: #adadad;

        }
         .close_icon {
             position: relative;
             font-family: FontAwesome !important;
             color: #FFFFFF !important;
             font-size: 200% !important;
                padding: 1px !important;
         
             text-decoration: none !important;
         }
             .close_icon:hover {
                 color: #d9d9d9 !important;
             }

             .close_icon::before {
                 content: "\f00d" !important;
             }
        .popupanimation {
             width: 100%;
             height: 100%;
             position: fixed;
             overflow-y: auto;
             overflow-x: hidden;
             top: 0;
             left: 0;
             animation-duration: 0.5s;
             -webkit-animation-duration: 0.5s;
             animation-name: slide-down;
             -webkit-animation-name: slide-down;
             animation-fill-mode: both;
             -webkit-animation-fill-mode: both;
             animation-delay: 0;
             -webkit-animation-delay: 0;
         }
          /*...........................*/
         @Keyframes slide-down {
             0% {
                 transform: translate3d(0,-200%,0);
             }
             100% {
                 transform: translateZ(0);
             }
         }
          .dxpc-headerText{
            padding-top:8px !important;
        }
        .headerstyle {
            padding-bottom: 14.5px !important;
            padding-left: 15px !important;
            padding-right: 15px !important;
            padding-top: 14.5px !important;
        }

        .pagenumber {
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style: solid;
            color: #333333 !important;
            text-decoration: none !important;
            padding-left: 7px !important;
            padding-right: 7px !important;
        }

            .pagenumber:hover {
                background-color: #333333;
                color: #FFFFFF !important;
            }

        .currentpagenumber {
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style: solid;
            color: #FFFFFF !important;
            background-color: #333333;
            text-decoration: none !important;
        }
    </style>

    <script>
        function OnGetRowValues(Value) {
            if (Value.length > 2 && Value[2]!=null) {

                Value[2] = Value[2].replace(/,/g, "?");
            }
            GridViewPannel.PerformCallback(Value);
        }

         function checktextbox(s, e) {
            if (s.GetText().indexOf(',') > -1) {
                s.GetInputElement().style.color = "red";
            }
             else if (s.GetText() == "") {
                s.GetInputElement().style.color = "#818181"; //#818181
            }
            else {
                 s.GetInputElement().style.color = "black";
            }
        }
        function submitclientdetailspressed(s, e) {

            if (ASPxClientEdit.ValidateGroup('popupValidationGroup')) {
                if (companytextbox.GetText().indexOf(',') >-1 || citytextbox.GetText().indexOf(',') >-1 || representativefirstnametextbox.GetText().indexOf(',') >-1 || representativelastnametextbox.GetText().indexOf(',') >-1 || representativemainsalutationtextbox.GetText().indexOf(',') >-1 || representativetitletextbox.GetText().indexOf(',') >-1 ||ziptextbox.GetText().indexOf(',') >-1 ||phonenumbertextbox.GetText().indexOf(',') >-1) {
                      window.alert("Text can not contain commas (except street address)");
                }
                else {
                    
                    clientPopUp.Hide();
                    GridViewPannel.PerformCallback('Save');
                }
            }

        }
    </script>
     <div style="overflow-y: auto;height: calc(100vh - 200px);">
    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { LoadingPanel.Hide(); }" />
    </dx:ASPxCallback>
    <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel"
        Modal="True">
    </dx:ASPxLoadingPanel>
    <dx:ASPxPanel ID="ASPxPanel1" runat="server" CssClass="detailPanelSmallHeader"></dx:ASPxPanel>
    <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" ClientInstanceName="GridViewPannel" Collapsible="false" SettingsLoadingPanel-Enabled ="true" OnCallback="ASPxCallbackPanel1_Callback" >
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">

                <dx:ASPxGridView ID="ClientGridView"
                    runat="server" AutoGenerateColumns="False"
                    ClientInstanceName="ClientGridView"
                    Width="100%"
                    AutoPostBack="true"
                    KeyFieldName="ClientID"
                    CssClass="newFont"
                    >
                     <SettingsPager CurrentPageNumberFormat="{0}">
                    </SettingsPager>
                    <StylesPager>
                        <PageNumber CssClass="pagenumber"></PageNumber>
                        <CurrentPageNumber CssClass="currentpagenumber"></CurrentPageNumber>
                    </StylesPager>
                    <ClientSideEvents CustomButtonClick="function(s, e) {
                            if(e.buttonID == 'delete') {
                                if (confirm('Are you sure you want to delete this client?')) {
                                    s.GetRowValues(e.visibleIndex, 'ClientID', OnGetRowValues);                
                                }
                            }
                            else if (e.buttonID == 'update') {
                                
                                ASPxClientEdit.ClearEditorsInContainer(null, 'popupValidationGroup');
                                s.GetRowValues(e.visibleIndex, 'ClientID;Company;Street;City;Zip;Phone Number;Representative First Name;Representative Last Name;Representative Main Salutation;Representative Title', OnGetRowValues);                   
                            }


                    }" />
                    
                   <Settings ShowTitlePanel ="true" ShowFilterRow="true" ShowFilterRowMenu="true" />
                    
                   <SettingsEditing EditFormColumnCount="4" Mode="Inline"/>
                
                    <Styles>
                        <AlternatingRow Enabled="true" />
                        <Header HorizontalAlign="Center"></Header>
                    </Styles>

                    <SettingsPopup>
                        <EditForm Width="100%" Modal="false"/>
                    </SettingsPopup>

                    <SettingsPager PageSize="50" />
                    <Paddings Padding="0px" />
                    <Border BorderWidth="0px" />
                    <BorderBottom BorderWidth="1px" />
                    <Settings ShowFooter="True" />
                    <Styles Header-Wrap="True" />

                    <Columns>
                         <dx:GridViewCommandColumn ButtonType="Image">
                            <CustomButtons>
                            <dx:GridViewCommandColumnCustomButton ID="update">
                                <Styles Style-CssClass="edit_icon"></Styles>
                            </dx:GridViewCommandColumnCustomButton>
                            <dx:GridViewCommandColumnCustomButton ID="delete" Text ="">
                                <Styles Style-CssClass="delete_icon"></Styles>
                            </dx:GridViewCommandColumnCustomButton>
                            </CustomButtons>
                            <HeaderTemplate>
                                <dx:ASPxButton ID="ASPxButton1" Image-Width="30px" Image-Height="30px" CausesValidation="false" RenderMode="Link" CssClass="new_icon" runat="server" AutoPostBack="false">
                                    <ClientSideEvents  Click="function(s, e) {
                                                               
                                                                ASPxClientEdit.ClearEditorsInContainer(null, 'popupValidationGroup');
                                                                GridViewPannel.PerformCallback();
                                                            }" />
                                </dx:ASPxButton>
                            </HeaderTemplate>
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataColumn FieldName="Company" HeaderStyle-Font-Size="Medium" CellStyle-HorizontalAlign="Left">
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn FieldName="Street" HeaderStyle-Font-Size="Medium" CellStyle-HorizontalAlign="Left"/>
                        <dx:GridViewDataColumn FieldName="City" HeaderStyle-Font-Size="Medium" CellStyle-HorizontalAlign="Left"/>
                        <dx:GridViewDataColumn FieldName="Zip" HeaderStyle-Font-Size="Medium" CellStyle-HorizontalAlign="Left"/>
                        <dx:GridViewDataColumn FieldName="Phone Number" HeaderStyle-Font-Size="Medium" CellStyle-HorizontalAlign="Left"/>
                        <%--Mohammad added the following--%> 
                        <dx:GridViewDataColumn FieldName="Representative First Name"  Visible="false" />
                         <dx:GridViewDataColumn FieldName="Representative Last Name"  Visible="false" />
                        <dx:GridViewDataColumn FieldName="Representative Main Salutation"  Visible="false" />
                        <dx:GridViewDataColumn FieldName="Representative Title"  Visible="false" />
                        <%--.........................................--%>
                    </Columns>
                  
                </dx:ASPxGridView>


                <!-- Clients popup control-->
                <dx:ASPxPopupControl ID="ClientPopUpControl" 
                    HeaderText="Client Details" HeaderStyle-BackColor="#37474F" HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White" 
                    HeaderStyle-CssClass="headerstyle"
                    HeaderStyle-Font-Size="Large" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" runat="server" 
                    ClientInstanceName="clientPopUp"
                     CssClass="popupanimation" 
                    EnableCallbackAnimation="true" 
                    PopupAnimationType="Auto" 
                    ShowCloseButton="true"
                     CloseButtonImage-Width="0px"
                     CloseButtonImage-Height="0px"
                     CloseButtonStyle-CssClass="close_icon"
                    >
                    <ContentCollection>
                       <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" Width="100%">
                            <dx:ASPxHiddenField ID="ClientIDHiddenField" Text="" runat="server"></dx:ASPxHiddenField>
                            <div class="container">
                                <div class="row">
                                  <div class="col-10">
                                    <label for="company">Company</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxTextBox Width="100%" CssClass="TextBox" MaxLength="55" ID="CompanyTextBox" ClientInstanceName="companytextbox" runat="server" NullText="Company">
                                         <ClientSideEvents   KeyUp ="checktextbox" />
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                  </div>
                                </div>
                                <br />
                                <div class="row">
                                  <div class="col-10">
                                    <label for="street">Street</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxTextBox Width="100%" MaxLength="55" CssClass="TextBox" ID="StreetTextBox" runat="server" NullText="Street">
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                  </div>
                                    <div class="col-10"></div>
                               <%-- </div>
                                <div class="row">--%>
                                  <div class="col-10">
                                    <label for="city">City</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxTextBox Width="100%" MaxLength="45" CssClass="TextBox" ID="CityTextBox" ClientInstanceName="citytextbox" runat="server" NullText="City">
                                         <ClientSideEvents   KeyUp ="checktextbox" />
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-10">
                                    <label for="zip">Zip/Postal Code</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxTextBox Width="100%" MaxLength="10" CssClass="TextBox" ID="ZipTextBox" ClientInstanceName="ziptextbox" runat="server" NullText="Zip">
                                         <ClientSideEvents   KeyUp ="checktextbox" />
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                  </div>
                                </div>
                                <br />
                                <div class="row">
                                  <div class="col-10">
                                    <label for="phonenumber">Phone Number</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxTextBox Width="100%" MaxLength="25" CssClass="TextBox" ID="PhoneNumberTextBox" ClientInstanceName="phonenumbertextbox" runat="server" NullText="Phone Number">
                                         <ClientSideEvents   KeyUp ="checktextbox" />
                                       <%-- <MaskSettings Mask="+999 (000) 000-0000" IncludeLiterals="None" />--%>
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                  </div>
                                </div>
                                <br />
                                <div class="row">
                                  <div class="col-10">
                                    <label for="representativefirstname">Representative First Name</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxTextBox Width="100%" MaxLength="45" CssClass="TextBox" ID="RepresentativeFirstNameTextBox" ClientInstanceName="representativefirstnametextbox" runat="server" NullText="Representative First Name">
                                        <ClientSideEvents   KeyUp ="checktextbox" />
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                  </div>
                                    <div class="col-10"></div>
                               <%-- </div>
                                <div class="row">--%>
                                  <div class="col-10">
                                    <label for="representativelastname">Representative Last Name</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxTextBox Width="100%" MaxLength="45" CssClass="TextBox" ID="RepresentativeLastNameTextBox" ClientInstanceName="representativelastnametextbox" runat="server" NullText="Representative Last Name">
                                        <ClientSideEvents   KeyUp ="checktextbox" />
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                  </div>
                                </div>
                                <br />
                                <div class="row">
                                  <div class="col-10">
                                    <label for="representativemainsalutation">Representative Main Salutation</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxTextBox Width="100%" MaxLength="11" CssClass="TextBox" ID="RepresentativeMainSalutationTextBox" ClientInstanceName="representativemainsalutationtextbox" runat="server" NullText="Representative Main Salutation">
                                        <ClientSideEvents   KeyUp ="checktextbox" />
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                  </div>
                                    <div class="col-10"></div>
                               <%-- </div>
                                <div class="row">--%>
                                  <div class="col-10">
                                    <label for="representativetitle">Representative Title</label>
                                  </div>
                                  <div class="col-25">
                                    <dx:ASPxTextBox Width="100%" MaxLength="50" CssClass="TextBox" ID="RepresentativeTitleTextBox" ClientInstanceName="representativetitletextbox" runat="server" NullText="Representative Title">
                                         <ClientSideEvents   KeyUp ="checktextbox" />
                                        <ValidationSettings ValidationGroup="popupValidationGroup" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                  </div>
                                </div>
                                <br />
                                <div class="row">
                                    <dx:ASPxButton CssClass="CancelButton" ID="CancelButton" Text="Cancel" AutoPostBack="false" UseSubmitBehavior="false" ClientSideEvents-Click="function(s, e) {
                                          
                                        clientPopUp.Hide();
                                        
                                            }" runat="server"></dx:ASPxButton>
                                    <dx:ASPxButton CssClass="SubmitButton" ID="SubmitButton" Text="Submit" AutoPostBack="false" UseSubmitBehavior="true" ClientSideEvents-Click="submitclientdetailspressed" runat="server"></dx:ASPxButton>
                                </div>
                            </div>
                       </dx:PopupControlContentControl>
                   </ContentCollection>
                </dx:ASPxPopupControl>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
    <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="ASPxGridView1"/>
         </div>
</asp:Content>