﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DXOmniVistaTimeEngine;
using System.Web.Security;
using DevExpress.Web;
using System.Text;
using DevExpress.Export;
using DevExpress.XtraPrinting;
using System.Xml;
using System.Collections;

public partial class SetupClient : System.Web.UI.Page
{
    public DataTable _dt = new DataTable();
    private DateTime _friday = new DateTime();
    private int originalWidth;
    private GridViewBandColumn _month1 = new GridViewBandColumn();
    private GridViewBandColumn _month2 = new GridViewBandColumn();
    private GridViewDataTextColumn SAT_HRS = new GridViewDataTextColumn();
    private GridViewDataTextColumn SUN_HRS = new GridViewDataTextColumn();
    private GridViewDataTextColumn MON_HRS = new GridViewDataTextColumn();
    private GridViewDataTextColumn TUE_HRS = new GridViewDataTextColumn();
    private GridViewDataTextColumn WED_HRS = new GridViewDataTextColumn();
    private GridViewDataTextColumn THU_HRS = new GridViewDataTextColumn();
    private GridViewDataTextColumn FRI_HRS = new GridViewDataTextColumn();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Connecting to database
        DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);

        //Getting clients data from database and binding it to gridview
        //Mohammad eddited the following 
         DataTable clients = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["Clients"], "");
        //DataTable clients = DataAccess.GetDataTableBySqlSyntax("SELECT C.ClientID, C.ClientID as Company, C.ClientStreet as Street, C.ClientCity as City, C.ClientZip AS 'Zip', C.ClientPhone as 'Phone Number', C.ClientFName as 'Representative First Name', C.ClientLName as 'Representative Last Name', C.ClientMainSalt as 'Representative Main Salutation',  C.ClientOther3 AS 'Representative Title' FROM Client C WHERE C.ClientID !='' AND C.ClientID IS NOT NULL", "");
        ClientGridView.DataSource = clients;
        clients.PrimaryKey = new DataColumn[] { clients.Columns["ClientID"] };
        ClientGridView.DataBind();
    }

    protected void ASPxCallbackPanel1_Callback(object sender, CallbackEventArgsBase e)
    {
        String[] parameters = e.Parameter.Split(',').ToArray();

        // Adding a new client
        if (e.Parameter == "")
        {
            ASPxEdit.ClearEditorsInContainer(this, "popupValidationGroup");//Mohammad added the following

            CompanyTextBox.Text = "";
            StreetTextBox.Text = "";
            CityTextBox.Text = "";
            ZipTextBox.Text = "";
            PhoneNumberTextBox.Text = "";
            RepresentativeFirstNameTextBox.Text = "";
            RepresentativeLastNameTextBox.Text = "";
            RepresentativeMainSalutationTextBox.Text = "";
            RepresentativeTitleTextBox.Text = "";

            DataTable dt = new DataTable();                                   
            dt.Clear();                                     
            dt.Columns.Add("ClientId");
            dt.Columns.Add("Name");
            dt.Columns.Add("Representative");
            dt.PrimaryKey = new DataColumn[] { dt.Columns["ClientId"] };

            ClientIDHiddenField.Set("ClientID", "");
            // Mohammad added the following
           
            this.ClientPopUpControl.ShowOnPageLoad = true;
            
            ///..............
        }
        else if (e.Parameter.ToString() == "Save")
        {
            SaveClientData();
        }
        // Updating an client
        else if (parameters.Count() == 1)
        {
            //SQL delete where clientID = parameter
            string clientId = e.Parameter;

            string deleteClientSql = "DELETE FROM dbo.Client WHERE ClientID = '" + clientId + "'";
            DataAccess.ExecuteSqlStatement(deleteClientSql, "");
            //Remove row in datatable of client grid and reload it and bind it

            DataTable clients = (DataTable)ClientGridView.DataSource;
            clients.Rows.Remove(clients.Rows.Find(clientId));

            ClientGridView.DataSource = clients;
            ClientGridView.DataBind();
        }
        else
        {
            ASPxEdit.ClearEditorsInContainer(this, "popupValidationGroup");//Mohammad added the following

            //Filling the client fields
            ClientIDHiddenField.Set("ClientID", parameters[0]);
            CompanyTextBox.Text = parameters[1];
            StreetTextBox.Text = parameters[2].Replace("?",",");
            CityTextBox.Text = parameters[3];
            ZipTextBox.Text = parameters[4];
            PhoneNumberTextBox.Text = parameters[5];
            RepresentativeFirstNameTextBox.Text = parameters[6];
            RepresentativeLastNameTextBox.Text = parameters[7];
            RepresentativeMainSalutationTextBox.Text = parameters[8];
            RepresentativeTitleTextBox.Text = parameters[9];

            // Mohammad added the following
            this.ClientPopUpControl.ShowOnPageLoad = true;
            ///..............
        }
    }

    protected void SaveClientData()
    {
        //If it is a new client
        if (ClientIDHiddenField.Get("ClientID").ToString() == "")
        {
            //Creating new client
            string createClientSql = "INSERT INTO dbo.Client (ClientID,ClientCompany,ClientStreet,ClientCity,ClientZip,ClientPhone,ClientFName,ClientLName,ClientMainSalt,ClientOther3) Values ('" + CompanyTextBox.Text + "', '" + CompanyTextBox.Text + "', '" + StreetTextBox.Text + "', '" + CityTextBox.Text + "', '" + ZipTextBox.Text + "', '" + PhoneNumberTextBox.Text + "', '" + RepresentativeFirstNameTextBox.Text + "', '" + RepresentativeLastNameTextBox.Text + "', '" + RepresentativeMainSalutationTextBox.Text + "', '" + RepresentativeTitleTextBox.Text + "')";
            DataAccess.ExecuteSqlStatement(createClientSql, "");
        }
        //If it is an old client
        else
        {
            //Changing Id, Name, Representative
            string updateClientSql = "Update dbo.Client SET ClientID = '" + CompanyTextBox.Text.Replace(" ", "").ToUpper() + "', ClientCompany = '" + CompanyTextBox.Text + "', ClientStreet = '" + StreetTextBox.Text + "', ClientPhone = '" + PhoneNumberTextBox.Text + "', ClientCity = '" + CityTextBox.Text + "', ClientZip = '" + ZipTextBox.Text + "', ClientFName = '" + RepresentativeFirstNameTextBox.Text + "', ClientLName = '" + RepresentativeLastNameTextBox.Text + "', ClientMainSalt = '" + RepresentativeMainSalutationTextBox.Text + "', ClientOther3 = '" + RepresentativeTitleTextBox.Text + "' WHERE ClientID = '" + ClientIDHiddenField.Get("ClientID") + "'";
            DataAccess.ExecuteSqlStatement(updateClientSql, "");
        }
        //Mohammad eddited the following
        DataTable clients = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["Clients"], "");
        //DataTable clients = DataAccess.GetDataTableBySqlSyntax("SELECT C.ClientID, C.ClientID as Company, C.ClientStreet as Street, C.ClientCity as City, C.ClientZip AS 'Zip', C.ClientPhone as 'Phone Number', C.ClientFName as 'Representative First Name', C.ClientLName as 'Representative Last Name', C.ClientMainSalt as 'Representative Main Salutation',  C.ClientOther3 AS 'Representative Title' FROM Client C WHERE C.ClientID !='' AND C.ClientID IS NOT NULL", "");
        
        ClientGridView.DataSource = clients;
        ClientGridView.DataBind();
    }
}