﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/OVMaster.master" CodeFile="Default.aspx.cs" Inherits="Default" %>

<%@ Register Assembly="DevExpress.XtraCharts.v15.2.Web, Version=15.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v15.2, Version=15.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="dx" %>

<asp:Content ID="header_" ContentPlaceHolderID="Header" runat="server">
<!-- DevExtreme dependencies -->
<script type="text/javascript" src="/time/OVTime/Script/dx/js/jquery-2.1.4.min"></script>
<script type="text/javascript" src="/time/OVTime/Script/DX/js/globalize.js"></script>
<script type="text/javascript" src="/time/OVTime/Script/DX/js/dx.chartjs.js"></script>
<!-- DevExtreme themes -->
<link rel="stylesheet" type="text/css" href="/time/OVTime/Script/dx/css/dx.common.css" />
<link rel="stylesheet" type="text/css" href="/time/OVTime/Script/dx/css/dx.light.css" />
<!-- A DevExtreme library -->
<script type="text/javascript" src="/time/OVTime/Script/dx/js/dx.all.js"></script>
<h4><span class="text-semibold">Home</span></h4>
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPage" runat="server">
    
     <style>
          .dxWeb_pPrevDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNext {
             background: url('../content/images/iconmonstr-arrow-63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pPrev {
             background: url('../content/images/iconmonstr-arrow-64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNextDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }
         .delete_icon 
        {
            position: relative;
            font-family: "Courier New" !important;
            color: #ee784a !important;
            font-size: 200% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .delete_icon:hover 
        {
            color: #FF0000 !important;
        }
        .delete_icon::before 
        {
            content: "\000D7" !important;
        }
        .new_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 20px !important;
            padding: 1px 1px;
            text-decoration: none !important;
        }

        .new_icon:hover 
        {
            color: #7373FF !important;
        }
        .new_icon:before 
        {
           content: "\f067" !important;
        }
        .update_icon
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        
        .GridView * 
        {
            font-family: Calibri;
            font-size: 16px;
        }

        .update_icon:hover
        {
            color: #7373FF !important;
             visibility: visible;
        }
        .update_icon:before 
        {
            content: "\f05d " !important;
        }
         .cancel_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #ee784a !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .cancel_icon:hover 
        {
            color: #FF8080 !important;
        }
        .cancel_icon:before
        {
            content: "\f0e2" !important;
        }
        /*.center {
    margin: auto;
    width: 80%;
    border: 2px solid #ACACAC;
    padding: 2px;
}*/
        #TimeSheetStatusTitle {
            margin-left: 20px; 
        }
        #EmployeesUpdates {
            margin-left: 20px; 
        }
        #TimeSheetStatusContent {
            margin-left: 10px;
            display: inline-block;
        }

        #pieChartContainer {
            display: inline-block;
        }

         #barChartContainer {
             display: inline-block;
         }

         @media  (max-width: 1550px) {
             .barchartfloat {
                 float: left !important;
             }
         }

         /*@media  (min-width: 620px) {
             .barchartfloat {
                 float: right;
             }
         }*/
         
.pagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #333333 !important;
            text-decoration: none !important;
            padding-left: 7px !important;
            padding-right: 7px !important;
        }
        .pagenumber:hover{
            background-color : #333333;
            color: #FFFFFF !important;
        }
        .currentpagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #FFFFFF !important;
            background-color: #333333;
            text-decoration: none !important;
        }
     </style>
  
    
    <div style="overflow-y: auto;height: calc(100vh - 200px);">
     <h5 id="TimeSheetStatusTitle">Last Week's Time Sheet Status: </h5><img runat="server" style="margin-left: 10px; height: 45px;width: 45px;display: inline-block;" id="IconStatus" src="assets/images/icon-check.png" /><h6 runat="server" id="TimeSheetStatusContent" style="margin-left: 5px; display: inline-block;">40 minutes were submitted for last week</h6>

     <%--<dx:WebChartControl ID="TimeSheetWebChartControl" runat="server" Height="400px" Visible="false"
                    Width="700px"
                    ClientInstanceName="chart"
                    ToolTipEnabled="False" CrosshairEnabled="True" RenderFormat="Png">
                </dx:WebChartControl>--%>

                 <h5 id="EmployeesUpdates" >Notifications: </h5>
               <%-- <div class="center">--%>
                    <dx:ASPxGridView 
                    ID="ASPxGridView1"
                    runat="server"
                    AutoGenerateColumns="False"
                    ClientInstanceName="ASPxGridView1"
                    Width="100%"
                    AutoPostBack="true" 
                    CssClass="GridView"                    
                    >
                        <SettingsPager CurrentPageNumberFormat="{0}">
                    </SettingsPager>
                    <StylesPager>
                        <PageNumber CssClass="pagenumber"></PageNumber>
                        <CurrentPageNumber CssClass="currentpagenumber"></CurrentPageNumber>
                    </StylesPager>
                    <Settings ShowTitlePanel ="true"  />
                    
                    <SettingsEditing EditFormColumnCount="4" Mode="Inline"/>
                    <Styles>
                        <AlternatingRow Enabled="true" />
                        <Header HorizontalAlign="Center"></Header>
                        
                    </Styles>

                    <SettingsPopup>
                        <EditForm Width="100%" Modal="false"/>
                    </SettingsPopup>

                    <SettingsPager PageSize="50" />
                    <Paddings Padding="0px" />
                    <Border BorderWidth="0px" />
                    <BorderBottom BorderWidth="1px" />
                    <Settings ShowFooter="True" />
                    <Styles Header-Wrap="True" />

                    <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
                    <Columns>
                        <dx:GridViewDataColumn FieldName="Employee" HeaderStyle-Font-Size="Medium" CellStyle-HorizontalAlign="left"  Width="20%">
                            <HeaderCaptionTemplate>
                                  Employee
                            </HeaderCaptionTemplate>
                        </dx:GridViewDataColumn>

                        <dx:GridViewDataColumn FieldName="Subject" HeaderStyle-Font-Size="Medium" CellStyle-HorizontalAlign="left" Width="20%"/>

                        <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd MMM yyyy" FieldName="date" Caption="Date" Width="20%"
                            CellStyle-HorizontalAlign="Center" UnboundType="DateTime"  HeaderStyle-Font-Size="Medium" />
                    
                        <dx:GridViewDataColumn FieldName="Status" HeaderStyle-Font-Size="Medium" CellStyle-HorizontalAlign="left" Width="20%"/>

                    </Columns>
                    
                </dx:ASPxGridView>
             <%--   </div>--%>
                <div id="pieChartContainer" style="width: 640px; height: 400px; padding-left: 10px; "></div>
                <dx:ASPxHiddenField ClientInstanceName="EmployeeID2" ID ="EmployeeID2" runat="server"></dx:ASPxHiddenField>
                <%--<asp:TextBox  id="EmployeeID" Text="" runat="server"></asp:TextBox>--%>
                <div id="barChartContainer" style="width: 640px; height: 400px; padding-left: 10px; float:right;" class="barchartfloat"></div>
                <script type="text/javascript" src="https://cdn3.devexpress.com/jslib/17.1.3/js/dx.all.debug.js"></script>
                <script type="text/javascript" >

                    var pieChartDataSource = new DevExpress.data.DataSource({
                        load: function (loadOptions) {
                            var d = $.Deferred();
                            var EmployeeName = EmployeeID2.Get("EmployeeName");
                            var location1 = '<%=ConfigurationManager.AppSettings["GetProjectsWithHours"].ToString() %>' + EmployeeName;
                               
                            
                            //console.log(location1);
                      

                            $.getJSON(location1).done(function (data) {
                                d.resolve(data);
                            });

                            return d.promise();
                        }
                    });

                    var barChartDataSource = new DevExpress.data.DataSource({
                        load: function (loadOptions) {
                            var d = $.Deferred();
                            var EmployeeName = EmployeeID2.Get("EmployeeName");
                            var location1 = '<%=ConfigurationManager.AppSettings["GetDaysOffWithHours"].ToString() %>' + EmployeeName;
                            //console.log(location1);
                            $.getJSON(location1).done(function (data) {
                                d.resolve(data);
                            });

                            return d.promise();
                        }
                    });

                    $("#pieChartContainer").dxPieChart({
                        type: "doughnut",
                        title: "Last week's project hours",
                        dataSource: pieChartDataSource,
                        series: [
                            {
                                argumentField: "Project",
                                valueField: "ProjectHours",
                                label: {
                                    visible: true,
                                    connector: {
                                        visible: true,
                                        width: 1
                                    },
                                    customizeText: function (pointInfo) {
                                        return pointInfo.value + " hr(s)";
                                    }
                                }
                            }
                        ]
                    });

                    $("#barChartContainer").dxChart({
                            dataSource: barChartDataSource,
                            title: "Last week's days off",
                            commonSeriesSettings: {
                                type: 'bar',
                                argumentField: 'Project'
                            },
                            series: [
                                { valueField: 'ProjectHours', name: 'Days Off' }
                            ],
                            commonAxisSettings: {
                                label: {
                                    overlappingBehavior: { mode: 'enlargeTickInterval' }
                                },
                                grid: { visible: true }
                            },
                            rotated: true
                   });
                </script>
       </div>
 </asp:content>