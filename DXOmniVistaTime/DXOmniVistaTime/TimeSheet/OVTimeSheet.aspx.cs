﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DXOmniVistaTimeEngine;
using System.Web.Security;
using DevExpress.Web;
using System.Text;
using DevExpress.Export;
using DevExpress.XtraPrinting;
using System.Xml;
using System.Collections;

public partial class OVTimeSheet : System.Web.UI.Page
{
    public DataTable _dt = new DataTable();
    private DateTime _toDate = new DateTime();
    private int originalWidth;
    private GridViewBandColumn _month1 = new GridViewBandColumn();
    private GridViewBandColumn _month2 = new GridViewBandColumn();
    private GridViewDataTextColumn SUN_HRS = new GridViewDataTextColumn();
    private GridViewDataTextColumn MON_HRS = new GridViewDataTextColumn();
    private GridViewDataTextColumn TUE_HRS = new GridViewDataTextColumn();
    private GridViewDataTextColumn WED_HRS = new GridViewDataTextColumn();
    private GridViewDataTextColumn THU_HRS = new GridViewDataTextColumn();
    private GridViewDataTextColumn FRI_HRS = new GridViewDataTextColumn();
    private GridViewDataTextColumn SAT_HRS = new GridViewDataTextColumn();
    private string selectedProjectID = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ASPxGridView1.EnableRowsCache = false;

            this.LoadingPanel.ContainerElementID = "GridViewPannel";
            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);
            
            _toDate = getToDate(DateTime.Now);
            this.ASPXDateEdit.Date = _toDate;
            getManagerEmployees(); //Mohammad added this
            EmployeelistConfigure(); //Mohammad added this
            GridViewDataComboBoxColumn cboColumnClient = (GridViewDataComboBoxColumn)this.ASPxGridView1.DataColumns["ClientID"];

            cboColumnClient.PropertiesComboBox.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["Clients"], "");//Mohammad changed this
             cboColumnClient.PropertiesComboBox.ValueField = "ClientID";
            cboColumnClient.PropertiesComboBox.TextField = "Company";

            GridViewDataComboBoxColumn cboColumnProject = (GridViewDataComboBoxColumn)this.ASPxGridView1.DataColumns["ProjectID"];
            cboColumnProject.PropertiesComboBox.DataSource = DataAccess.GetDataTableBySqlSyntax("EXEC ClientProjects null, null", "");

            cboColumnProject.PropertiesComboBox.ValueField = "ProjectID";
            cboColumnProject.PropertiesComboBox.TextField = "Project";

            //GridViewDataComboBoxColumn cboColumnRole = (GridViewDataComboBoxColumn)this.ASPxGridView1.DataColumns["RoleName"];
            //cboColumnRole.PropertiesComboBox.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["Roles"], "");
            //cboColumnRole.PropertiesComboBox.ValueField = "RoleName";
            //cboColumnRole.PropertiesComboBox.TextField = "RoleName";

            GridViewDataTextColumn cboColumnDiscipline = (GridViewDataTextColumn)this.ASPxGridView1.DataColumns["Discipline"];
            //cboColumnActivity.PropertiesComboBox.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["EmployeeRoles"], "WHERE dbo.Employee.EmployeeID = '" + Membership.GetUser(User.Identity.Name).ToString().ToUpper() + "'");

            createHeader();
            formatGridHeaderSheet();
            Session["EmployeeID"] = Membership.GetUser(User.Identity.Name).ToString();//Mohammad added this
            ComboBoxEmployees.Text = (String)Session["EmployeeID"];//Mohammad added this
            refreshDataGrid((String)Session["EmployeeID"]);
            calcSheetTotals();
            formatSheetTotals();
        }
        else
        {
            //refresh project list based on used projects in time period; dont allow duplicates
            if (!this.ASPXDateEdit.Date.Year.ToString().Equals("1")){ //if Date was empty/clear
               
                _toDate = getToDate(this.ASPXDateEdit.Date);
                this.ASPXDateEdit.Date = _toDate;

                //cboColumnProject.PropertiesComboBox.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["Projects"], "");
                //cboColumnProject.PropertiesComboBox.ValueField = "ProjectID";
                //cboColumnProject.PropertiesComboBox.TextField = "Project";

                //clearHeader();
                //createHeader();
                EditformatGridHeaderSheet();
                calcSheetTotals();
            }
           
        }
    }

    private void EditformatGridHeaderSheet()
        {
        _toDate = getToDate(this.ASPXDateEdit.Date);
        this.ASPxGridView1.Columns["SUN_HRS"].Caption = "Sun " + _toDate.AddDays(-6).Day.ToString();
        this.ASPxGridView1.Columns["MON_HRS"].Caption = "Mon " + _toDate.AddDays(-5).Day.ToString();
        this.ASPxGridView1.Columns["TUE_HRS"].Caption = "Tue " + _toDate.AddDays(-4).Day.ToString();
        this.ASPxGridView1.Columns["WED_HRS"].Caption = "Wed " + _toDate.AddDays(-3).Day.ToString();
        this.ASPxGridView1.Columns["THU_HRS"].Caption = "Thu " + _toDate.AddDays(-2).Day.ToString();
        this.ASPxGridView1.Columns["FRI_HRS"].Caption = "Fri " + _toDate.AddDays(-1).Day.ToString();
        this.ASPxGridView1.Columns["SAT_HRS"].Caption = "Sat " + _toDate.AddDays(0).Day.ToString();

        clearHeader();
        createHeader();
        formatGridHeaderSheet();
    }

    private void clearHeader()
    {
        if (this.ASPxGridView1.Columns["Month1"] != null)
        {
             this.ASPxGridView1.Columns.RemoveAt(this.ASPxGridView1.Columns["Month1"].Index);  
            if (this.ASPxGridView1.Columns["Month2"] != null)
            {
                this.ASPxGridView1.Columns.RemoveAt(this.ASPxGridView1.Columns["Month2"].Index);
            }  
        }
    }

    private void createHeader()
    {
        /*
         * 
         * <dx:GridViewDataTextColumn FieldName="TOTAL_HRS" Caption="Total Hrs" VisibleIndex="18" UnboundType="Decimal" ReadOnly="true">
                            <DataItemTemplate>
                                <dx:ASPxTextBox ID="ASPxTextBox1" runat="server" Text='test1' Font-Bold="True"></dx:ASPxTextBox>
                                </br>
                                <dx:ASPxTextBox ID="ASPxLabel1" runat="server" Text='test1' Font-Bold="True"></dx:ASPxTextBox>
                                </br>
                                <dx:ASPxTextBox ID="ASPxLabel2" runat="server" Text='test2' Font-Size="Smaller" ForeColor="Gray"></dx:ASPxTextBox>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
         * 
         * */

        _month1.Name = "Month1";
        _month2.Name = "Month2";
        //_month1.HeaderStyle.Border.BorderColor = System.Drawing.Color.FromArgb(64, 64, 64) ;
        //_month2.HeaderStyle.Border.BorderColor = System.Drawing.Color.FromArgb(64, 64, 64) ;
        _month1.HeaderStyle.CssClass = "month1_border";
        
        _month1.HeaderStyle.Border.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(1);
        _month2.HeaderStyle.Border.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(1);
       
        _month1.AllowDragDrop = DevExpress.Utils.DefaultBoolean.False;
        _month2.AllowDragDrop = DevExpress.Utils.DefaultBoolean.False;

        SUN_HRS.FieldName = "SUN_HRS";
        MON_HRS.FieldName = "MON_HRS";
        TUE_HRS.FieldName = "TUE_HRS";
        WED_HRS.FieldName = "WED_HRS";
        THU_HRS.FieldName = "THU_HRS";
        FRI_HRS.FieldName = "FRI_HRS";
        SAT_HRS.FieldName = "SAT_HRS";

        SUN_HRS.VisibleIndex = 4;
        MON_HRS.VisibleIndex = 6;
        TUE_HRS.VisibleIndex = 8;
        WED_HRS.VisibleIndex = 10;
        THU_HRS.VisibleIndex = 12;
        FRI_HRS.VisibleIndex = 14;
        SAT_HRS.VisibleIndex = 16;

        SUN_HRS.Visible= true;
        MON_HRS.Visible= true;
        TUE_HRS.Visible= true;
        WED_HRS.Visible= true;
        THU_HRS.Visible= true;
        FRI_HRS.Visible= true;
        SAT_HRS.Visible= true;

        SUN_HRS.EditFormSettings.ColumnSpan = 1;
        MON_HRS.EditFormSettings.ColumnSpan = 1;
        TUE_HRS.EditFormSettings.ColumnSpan = 1;
        WED_HRS.EditFormSettings.ColumnSpan = 1;
        THU_HRS.EditFormSettings.ColumnSpan = 1;
        FRI_HRS.EditFormSettings.ColumnSpan = 1;
        SAT_HRS.EditFormSettings.ColumnSpan = 1;

        SUN_HRS.PropertiesTextEdit.DisplayFormatString = "N2";
        MON_HRS.PropertiesTextEdit.DisplayFormatString = "N2";
        TUE_HRS.PropertiesTextEdit.DisplayFormatString = "N2";
        WED_HRS.PropertiesTextEdit.DisplayFormatString = "N2";
        THU_HRS.PropertiesTextEdit.DisplayFormatString = "N2";
        FRI_HRS.PropertiesTextEdit.DisplayFormatString = "N2";
        SAT_HRS.PropertiesTextEdit.DisplayFormatString = "N2";
           
        SUN_HRS.PropertiesTextEdit.MaskSettings.IncludeLiterals = DevExpress.Web.MaskIncludeLiteralsMode.DecimalSymbol;
        MON_HRS.PropertiesTextEdit.MaskSettings.IncludeLiterals = DevExpress.Web.MaskIncludeLiteralsMode.DecimalSymbol;
        TUE_HRS.PropertiesTextEdit.MaskSettings.IncludeLiterals = DevExpress.Web.MaskIncludeLiteralsMode.DecimalSymbol;
        WED_HRS.PropertiesTextEdit.MaskSettings.IncludeLiterals = DevExpress.Web.MaskIncludeLiteralsMode.DecimalSymbol;
        THU_HRS.PropertiesTextEdit.MaskSettings.IncludeLiterals = DevExpress.Web.MaskIncludeLiteralsMode.DecimalSymbol;
        FRI_HRS.PropertiesTextEdit.MaskSettings.IncludeLiterals = DevExpress.Web.MaskIncludeLiteralsMode.DecimalSymbol;
        SAT_HRS.PropertiesTextEdit.MaskSettings.IncludeLiterals = DevExpress.Web.MaskIncludeLiteralsMode.DecimalSymbol;

        // FRI_HRS.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(18, 121, 192); (Blue)
        FRI_HRS.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#4CAF50");
        SAT_HRS.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#4CAF50");

        //SAT_HRS.DataItemTemplate = new MemoTemplate();

        //Mohammad added the following 
        SUN_HRS.PropertiesTextEdit.MaxLength = 5;
        MON_HRS.PropertiesTextEdit.MaxLength = 5;
        TUE_HRS.PropertiesTextEdit.MaxLength = 5;
        WED_HRS.PropertiesTextEdit.MaxLength = 5;
        THU_HRS.PropertiesTextEdit.MaxLength = 5;
        FRI_HRS.PropertiesTextEdit.MaxLength = 5;
        SAT_HRS.PropertiesTextEdit.MaxLength = 5;
    }

    class MemoTemplate : ITemplate
    {
        public void InstantiateIn(Control container)
        {
            ASPxTextBox textBox = new ASPxTextBox();
            textBox.Text = "<%# Eval('SAT_MEMO') %>";

            container.Controls.Add(textBox);
        }
    }

    /*class MyHyperlinkTemplate : ITemplate
    {
        public void InstantiateIn(Control container)
        {
            ASPxTextBox fromTextBox = new ASPxTextBox();
            fromTextBox.MaskSettings.Mask = "00:00";

            ASPxTextBox toTextBox = new ASPxTextBox();
            toTextBox.MaskSettings.Mask = "00:00";

            ASPxTextBox totalTextBox = new ASPxTextBox();
            totalTextBox.MaskSettings.Mask = "00:00";
            totalTextBox.ReadOnly = true;

            GridViewDataItemTemplateContainer gridContainer = (GridViewDataItemTemplateContainer)container;
            container.Controls.Add(fromTextBox);
            container.Controls.Add(new LiteralControl("<br />"));
            container.Controls.Add(toTextBox);
            container.Controls.Add(new LiteralControl("<br />"));
            container.Controls.Add(totalTextBox);
        }
    }*/

    class MyHyperlinkTemplate : ITemplate
    {
        public void InstantiateIn(Control container)
        {
            ASPxButton memoButton = new ASPxButton();
            memoButton.Text = "Memo";

            GridViewDataItemTemplateContainer gridContainer = (GridViewDataItemTemplateContainer)container;
            container.Controls.Add(new LiteralControl("<table>"));
            container.Controls.Add(new LiteralControl("<tr>"));
            container.Controls.Add(new LiteralControl("<td style='width: 90px'>"));
            container.Controls.Add(new LiteralControl("<%#Eval('SUN_MEMO') %>"));
            container.Controls.Add(new LiteralControl("</td>"));
            container.Controls.Add(new LiteralControl("<td>"));
            container.Controls.Add(memoButton);
            container.Controls.Add(new LiteralControl("</td>"));
            container.Controls.Add(new LiteralControl("</tr>"));
            container.Controls.Add(new LiteralControl("</table>"));
        }
    }

    /*
     * <DataItemTemplate>
                                <table>
                                    <tr>
                                        <td style="width: 90px">
                                            <%#Eval("SUN_MEMO") %>
                                        </td>
                                        <td>
                                            <dx:ASPxButton runat="server" ID="Btn" Text="Search"></dx:ASPxButton>
                                        </td>
                                    </tr>
                                </table>
                            </DataItemTemplate>   
     * 
     * */

    /*
         * 
         * <dx:GridViewDataTextColumn FieldName="TOTAL_HRS" Caption="Total Hrs" VisibleIndex="18" UnboundType="Decimal" ReadOnly="true">
                            <DataItemTemplate>
                                <dx:ASPxTextBox ID="ASPxTextBox1" runat="server" Text='test1' Font-Bold="True"></dx:ASPxTextBox>
                                </br>
                                <dx:ASPxTextBox ID="ASPxLabel1" runat="server" Text='test1' Font-Bold="True"></dx:ASPxTextBox>
                                </br>
                                <dx:ASPxTextBox ID="ASPxLabel2" runat="server" Text='test2' Font-Size="Smaller" ForeColor="Gray"></dx:ASPxTextBox>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
         * 
         * */

    protected void ASPXDateEdit_CustomDisabledDate(object sender, CalendarCustomDisabledDateEventArgs e)
    {
        //Choosing the date that the timesheet ends with
        e.IsDisabled = true;  
        
        string ToDate = ConfigurationManager.AppSettings["TimeSheetToDate"];

        switch (ToDate)
        {
            case "Sunday":
                if (e.Date.DayOfWeek == DayOfWeek.Sunday)
                {
                    e.IsDisabled = false;
                }
                break;
            case "Monday":
                if (e.Date.DayOfWeek == DayOfWeek.Monday)
                {
                    e.IsDisabled = false;
                }                
                break;
            case "Tuesday":
                if (e.Date.DayOfWeek == DayOfWeek.Tuesday)
                {
                    e.IsDisabled = false;
                }                
                break;
            case "Wednesday":
                if (e.Date.DayOfWeek == DayOfWeek.Wednesday)
                {
                    e.IsDisabled = false;
                }                
                break;
            case "Thursday":
                if (e.Date.DayOfWeek == DayOfWeek.Thursday)
                {
                    e.IsDisabled = false;
                }                
                break;
            case "Friday":
                if (e.Date.DayOfWeek == DayOfWeek.Friday)
                {
                    e.IsDisabled = false;
                }               
                break;
            case "Saturday":
                if (e.Date.DayOfWeek == DayOfWeek.Saturday)
                {
                    e.IsDisabled = false;
                }                
                break;
        }
    }

    protected void ASPXDateEdit_CalendarDayCellPrepared(object sender, CalendarDayCellPreparedEventArgs e)
    {
        string ToDate = ConfigurationManager.AppSettings["TimeSheetToDate"];

        switch (ToDate)
        {
            case "Sunday":
                if (e.Date.DayOfWeek != DayOfWeek.Sunday)
                {
                    e.Cell.Attributes["disabled"] = "disabled";
                    e.Cell.Attributes["style"]= "pointer-events:none";
                }
                break;
            case "Monday":
                if (e.Date.DayOfWeek != DayOfWeek.Monday)
                {
                    e.Cell.Attributes["disabled"] = "disabled";
                    e.Cell.Attributes["style"]= "pointer-events:none";
                }
                break;
            case "Tuesday":
                if (e.Date.DayOfWeek != DayOfWeek.Tuesday)
                {
                    e.Cell.Attributes["disabled"] = "disabled";
                    e.Cell.Attributes["style"]= "pointer-events:none";
                }
                break;
            case "Wednesday":
                if (e.Date.DayOfWeek != DayOfWeek.Wednesday)
                {
                    e.Cell.Attributes["disabled"] = "disabled";
                    e.Cell.Attributes["style"]= "pointer-events:none";
                }
                break;
            case "Thursday":
                if (e.Date.DayOfWeek != DayOfWeek.Thursday)
                {
                    e.Cell.Attributes["disabled"] = "disabled";
                    e.Cell.Attributes["style"]= "pointer-events:none";
                }
                break;
            case "Friday":
                if (e.Date.DayOfWeek != DayOfWeek.Friday)
                {
                    e.Cell.Attributes["disabled"] = "disabled";
                    e.Cell.Attributes["style"]= "pointer-events:none";
                }
                break;
            case "Saturday":
                if (e.Date.DayOfWeek != DayOfWeek.Saturday)
                {
                    e.Cell.Attributes["disabled"] = "disabled";
                    e.Cell.Attributes["style"]= "pointer-events:none";
                }
                break;
        }
    }

    private DateTime getToDate(DateTime passedDate)
    {
        int days = 0;

        string ToDate = ConfigurationManager.AppSettings["TimeSheetToDate"];

        switch (ToDate)
        {
            case "Sunday":
                days = 0;
                break;
            case "Monday":
                days = 1;
                break;
            case "Tuesday":
                days = 2;
                break;
            case "Wednesday":
                days = 3;
                break;
            case "Thursday":
                days = 4;
                break;
            case "Friday":
                days = 5;
                break;
            case "Saturday":
                days = 6;
                break;
        }

        //If the configuration is Saturday
        switch (passedDate.DayOfWeek)
        {
            case DayOfWeek.Sunday:
                return passedDate.AddDays(days);
            case DayOfWeek.Monday:
                return passedDate.AddDays((days - 1 ) <0 ? days + 6 : days - 1);
            case DayOfWeek.Tuesday:
                return passedDate.AddDays((days - 2) < 0 ? days + 5 : days - 2);
            case DayOfWeek.Wednesday:
                return passedDate.AddDays((days - 3) < 0 ? days + 4 : days - 3);
            case DayOfWeek.Thursday:
                return passedDate.AddDays((days - 4) < 0 ? days + 3 : days - 4);
            case DayOfWeek.Friday:
                return passedDate.AddDays((days - 5) < 0 ? days + 2 : days - 5);
            case DayOfWeek.Saturday:
                return passedDate.AddDays((days - 6) < 0 ? days + 1 : days - 6);
        }

        return passedDate;

    }

    protected void ASPXDateEdit_DateChanged(object sender, EventArgs e)
    {
        //_toDate = getToDate(this.ASPXDateEdit.Date);
        
        formatGridHeaderSheet();

        this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax("EXEC OVS_GetWeeklyTimeSheet '" + this.ASPXDateEdit.Date + "' , '" + (String)Session["EmployeeID"]+ "'", "");//Mohammad editted this
        this.ASPxGridView1.DataBind();
        
        calcSheetTotals();
    }

    protected void ASPxGridView1_CustomColumnSort(object sender, DevExpress.Web.CustomColumnSortEventArgs e)
    {

    }

    protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
    {
        
        
    }

    protected void ASPxGridView1_DataBound(object sender, EventArgs e)
    {
        
        
    }

    protected void ASPxGridView1_BeforeColumnSortingGrouping(object sender, DevExpress.Web.ASPxGridViewBeforeColumnGroupingSortingEventArgs e)
    {
    }

    private void formatGridHeaderSheet()
    {
        _toDate = getToDate(this.ASPXDateEdit.Date);

        SUN_HRS.Caption = "Sun " + _toDate.AddDays(-6).Day.ToString();
        MON_HRS.Caption = "Mon " + _toDate.AddDays(-5).Day.ToString();
        TUE_HRS.Caption = "Tue " + _toDate.AddDays(-4).Day.ToString();
        WED_HRS.Caption = "Wed " + _toDate.AddDays(-3).Day.ToString();
        THU_HRS.Caption = "Thu " + _toDate.AddDays(-2).Day.ToString();
        FRI_HRS.Caption = "Fri " + _toDate.AddDays(-1).Day.ToString();
        SAT_HRS.Caption = "Sat " + _toDate.AddDays(0).Day.ToString();

        if (_toDate.AddDays(-6).Month.ToString() + " " + _toDate.AddDays(-6).Year.ToString() == _toDate.AddDays(-0).Month.ToString() + " " + _toDate.AddDays(-0).Year.ToString())//same month and year
        {
            _month1.Caption = getMonth(_toDate.AddDays(-6).Month) + " " + _toDate.AddDays(-6).Year.ToString();
            _month1.Columns.Add(SUN_HRS);
            _month1.Columns.Add(MON_HRS);
            _month1.Columns.Add(TUE_HRS);
            _month1.Columns.Add(WED_HRS);
            _month1.Columns.Add(THU_HRS);
            _month1.Columns.Add(FRI_HRS);
            _month1.Columns.Add(SAT_HRS);
            this.ASPxGridView1.Columns.Add(_month1);
        }
        else
        {
            GridViewDataTextColumn[] date = { SAT_HRS, FRI_HRS, THU_HRS, WED_HRS, TUE_HRS, MON_HRS, SUN_HRS };
            for (int i = 6; i > 0; i--)
            {
                 _month1.Columns.Add(date[i]);
                 if (_toDate.AddDays(-i).Month.ToString() != _toDate.AddDays(-(i - 1)).Month.ToString())
                {
                    _month1.Caption = getMonth(_toDate.AddDays(-i).Month) + " " + _toDate.AddDays(-i).Year.ToString();
                    _month2.Caption = getMonth(_toDate.AddDays(-(i - 1)).Month) + " " + _toDate.AddDays(-(i - 1)).Year.ToString();
                    int j = i - 1; 
                    while (j >=0)
                    {
                     
                        _month2.Columns.Add(date[j]);
                        j--;
                    }
                    break;
                }
            }
            this.ASPxGridView1.Columns.Add(_month1);
            this.ASPxGridView1.Columns.Add(_month2);      
        }

    }

    private String getMonth(int x)
    {
        if (x == 1) return "Jan";
        else if (x == 2) return "Feb";
        else if (x == 3) return "March";
        else if (x == 4) return "April";
        else if (x == 5) return "May";
        else if (x == 6) return "Jun";
        else if (x == 7) return "July";
        else if (x == 8) return "Aug";
        else if (x == 9) return "Sept";
        else if (x == 10) return "Oct";
        else if (x == 11) return "Nov";
        else if (x == 12) return "Dec";
        else return "";
}

    private void formatSheetTotals()
    {
        this.ASPxGridView1.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "SUN_HRS");
        this.ASPxGridView1.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "MON_HRS");
        this.ASPxGridView1.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "TUE_HRS");
        this.ASPxGridView1.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "WED_HRS");
        this.ASPxGridView1.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "THU_HRS");
        this.ASPxGridView1.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "FRI_HRS");
        this.ASPxGridView1.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "SAT_HRS");

        ASPxGridView1.SettingsBehavior.ConfirmDelete = true;
    }
    String text;
    private void calcSheetTotals()
    {
        if (this.ASPxGridView1.DataSource == null)
        {
            refreshDataGrid((String)Session["EmployeeID"]);
        }
        if (!this.ASPXDateEdit.Date.Year.ToString().Equals("1"))
        { //if Date was empty/clear
            _dt = (DataTable)this.ASPxGridView1.DataSource;
            _dt.PrimaryKey = new DataColumn[] { _dt.Columns["TEID"] };

            var sumSun = _dt.AsEnumerable().Sum(x => x.Field<double>("SUN_HRS"));
            var sumMon = _dt.AsEnumerable().Sum(x => x.Field<double>("MON_HRS"));
            var sumTue = _dt.AsEnumerable().Sum(x => x.Field<double>("TUE_HRS"));
            var sumWed = _dt.AsEnumerable().Sum(x => x.Field<double>("WED_HRS"));
            var sumThu = _dt.AsEnumerable().Sum(x => x.Field<double>("THU_HRS"));
            var sumFri = _dt.AsEnumerable().Sum(x => x.Field<double>("FRI_HRS"));
            var sumSat = _dt.AsEnumerable().Sum(x => x.Field<double>("SAT_HRS"));

            this.ASPxTextBoxHoursWorkedText.Text = (sumSun + sumMon + sumTue + sumWed + sumThu + sumFri + sumSat).ToString();
            this.ASPxTextBoxHoursWorkedText.Value = (sumSun + sumMon + sumTue + sumWed + sumThu + sumFri + sumSat).ToString();
            text = this.ASPxTextBoxHoursWorkedText.Text;

            Session["GridTimeEnterData"] = _dt;
        }
    }

    protected void ASPxGridView1_CustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "TOTAL_HRS")
        {
            decimal sunHrs = Convert.ToDecimal(e.GetListSourceFieldValue("SUN_HRS"));
            decimal monHrs = Convert.ToDecimal(e.GetListSourceFieldValue("MON_HRS"));
            decimal tueHrs = Convert.ToDecimal(e.GetListSourceFieldValue("TUE_HRS"));
            decimal wedHrs = Convert.ToDecimal(e.GetListSourceFieldValue("WED_HRS"));
            decimal thuHrs = Convert.ToDecimal(e.GetListSourceFieldValue("THU_HRS"));
            decimal friHrs = Convert.ToDecimal(e.GetListSourceFieldValue("FRI_HRS"));
            decimal satHrs = Convert.ToDecimal(e.GetListSourceFieldValue("SAT_HRS")); 

            e.Value = sunHrs + monHrs + tueHrs + wedHrs + thuHrs + friHrs + satHrs;
        }
    }

    private void refreshDataGrid(String EmployeeID)
    { 

        if (this.ASPXDateEdit == null || this.ASPXDateEdit.Text == string.Empty)
        {
            //this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["TimeEntryPeriod"], " and e.EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' ");
        }
        else
        {
            DateTime date = _toDate.AddDays(-6);

            _toDate = getToDate(this.ASPXDateEdit.Date);
            this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax("EXEC OVS_GetWeeklyTimeSheet '" + _toDate + "' , '" + EmployeeID+ "'","");
           
        }
        this.ASPxGridView1.DataBind();

    }

    protected void ASPxGridView1_SummaryDisplayText(object sender, ASPxGridViewSummaryDisplayTextEventArgs e)
    {
        if (e.Item.FieldName == "SUN_HRS" || e.Item.FieldName == "MON_HRS" || e.Item.FieldName == "TUE_HRS" ||
            e.Item.FieldName == "WED_HRS" || e.Item.FieldName == "THU_HRS" || e.Item.FieldName == "FRI_HRS" || e.Item.FieldName == "SAT_HRS")
            e.Text = string.Format("{0:N2}", Convert.ToDouble(e.Value));
    }

    protected void ASPxGridView1_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {

        _toDate = getToDate(this.ASPXDateEdit.Date);
        
        string weekDay = string.Empty;
        string dayHRS = string.Empty;

        bool TEOT = false;

        if (e.NewValues["TEOT"] != null)
        {
            TEOT = (bool)e.NewValues["TEOT"];
        }

        DataRow row = ((DataTable)Session["GridTimeEnterData"]).NewRow();
        row["TEID"] = ((DataTable)Session["GridTimeEnterData"]).Rows.Count + 1;
        //row["EmployeeID"] = User.Identity.Name;
        row["EmployeeID"] = (String)Session["EmployeeID"]; //Mohamamd Editted this
        row["ProjectID"] = e.NewValues["ProjectID"];
        row["RoleName"] = e.NewValues["RoleName"];
        row["ClientID"] = e.NewValues["ClientID"];
        row["TEOT"] = TEOT;
        row["SUN_HRS"] = e.NewValues["SUN_HRS"] ?? 0;
        row["MON_HRS"] = e.NewValues["MON_HRS"] ?? 0;
        row["TUE_HRS"] = e.NewValues["TUE_HRS"] ?? 0;
        row["WED_HRS"] = e.NewValues["WED_HRS"] ?? 0;
        row["THU_HRS"] = e.NewValues["THU_HRS"] ?? 0;
        row["FRI_HRS"] = e.NewValues["FRI_HRS"] ?? 0;
        row["SAT_HRS"] = e.NewValues["SAT_HRS"] ?? 0;

        //memo fields
        row["SUN_MEMO"] = e.NewValues["SUN_MEMO"];
        row["MON_MEMO"] = e.NewValues["MON_MEMO"];
        row["TUE_MEMO"] = e.NewValues["TUE_MEMO"];
        row["WED_MEMO"] = e.NewValues["WED_MEMO"];
        row["THU_MEMO"] = e.NewValues["THU_MEMO"];
        row["FRI_MEMO"] = e.NewValues["FRI_MEMO"];
        row["SAT_MEMO"] = e.NewValues["SAT_MEMO"];

        DataTable dt = (DataTable)Session["GridTimeEnterData"];
        dt.PrimaryKey = new DataColumn[] { _dt.Columns["TEID"] };

        /*var results = from dataRow in dt.AsEnumerable()
                      where dataRow.Field<string>("ProjectId") + "_" + dataRow.Field<string>("ActivityId") == row["ProjectID"].ToString() + "_" + row["ActivityID"].ToString()
                      select dataRow;
         */
        int count = 0;
        count = dt.AsEnumerable()
              .Count(dataRow => dataRow.Field<string>("ProjectId") + "_" + dataRow.Field<string>("RoleName") == row["ProjectID"].ToString() + "_" + row["RoleName"].ToString());

        if (row["ProjectID"].ToString().ToUpper() == string.Empty || row["RoleName"].ToString().ToUpper() == string.Empty || row["ClientID"].ToString().ToUpper() == string.Empty)
        {
            ASPxGridView1.CancelEdit();

            Exception ex = new Exception("Project, Role and Client are needed to save record. Please correct this record to save.");
            throw (ex);
        }
        else if (
                double.Parse(row["SUN_HRS"].ToString()) == 0 &&
                double.Parse(row["MON_HRS"].ToString()) == 0 &&
                double.Parse(row["TUE_HRS"].ToString()) == 0 &&
                double.Parse(row["WED_HRS"].ToString()) == 0 &&
                double.Parse(row["THU_HRS"].ToString()) == 0 &&
                double.Parse(row["FRI_HRS"].ToString())== 0 && 
                double.Parse(row["SAT_HRS"].ToString())== 0
                )
        {
            ASPxGridView1.CancelEdit();

            Exception ex = new Exception("At least one date must have a time value. Please correct this record to save.");
            throw (ex);
        }
        else
        {

            DataAccess.InsertTime(row, _toDate);
        }
        e.Cancel = true;
        ASPxGridView1.CancelEdit();
        
        refreshDataGrid((String)Session["EmployeeID"]);
        calcSheetTotals();
    }

    protected void ASPxGridView1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        try
        {
            DataRow row = ((DataTable)Session["GridTimeEnterData"]).Rows.Find(e.Keys["TEID"]);
            row["SUN_HRS"] = e.NewValues["SUN_HRS"];
            row["MON_HRS"] = e.NewValues["MON_HRS"];
            row["TUE_HRS"] = e.NewValues["TUE_HRS"];
            row["WED_HRS"] = e.NewValues["WED_HRS"];
            row["THU_HRS"] = e.NewValues["THU_HRS"];
            row["FRI_HRS"] = e.NewValues["FRI_HRS"];
            row["SAT_HRS"] = e.NewValues["SAT_HRS"];

            //memo fields
            row["SUN_MEMO"] = e.NewValues["SUN_MEMO"];
            row["MON_MEMO"] = e.NewValues["MON_MEMO"];
            row["TUE_MEMO"] = e.NewValues["TUE_MEMO"];
            row["WED_MEMO"] = e.NewValues["WED_MEMO"];
            row["THU_MEMO"] = e.NewValues["THU_MEMO"];
            row["FRI_MEMO"] = e.NewValues["FRI_MEMO"];
            row["SAT_MEMO"] = e.NewValues["SAT_MEMO"];

            if (row["Status"].ToString().ToUpper() == "LOCKED")
            {
                ASPxGridView1.CancelEdit();

                Exception ex = new Exception("Record locked, you cannot update a locked record. Please unlock this record first");
                throw (ex);
                
            }
            else if (
                    double.Parse(row["SUN_HRS"].ToString()) == 0 &&
                    double.Parse(row["MON_HRS"].ToString()) == 0 &&
                    double.Parse(row["TUE_HRS"].ToString()) == 0 &&
                    double.Parse(row["WED_HRS"].ToString()) == 0 &&
                    double.Parse(row["THU_HRS"].ToString()) == 0 &&
                    double.Parse(row["FRI_HRS"].ToString()) == 0 &&
                    double.Parse(row["SAT_HRS"].ToString()) == 0
                    )
            {
                ASPxGridView1.CancelEdit();

                Exception ex = new Exception("Atleast one date must have a time value. Please correct this record to save.");
                throw (ex);
            }
            else
            {
                //string projectID = row["ProjectID"].ToString();

                //char[] separators = { '-' };
                //projectID = projectID.Split(separators, 2)[0].Trim();

                //row["ProjectID"] = projectID;
                GridViewDataComboBoxColumn cboColumnProject = (GridViewDataComboBoxColumn)this.ASPxGridView1.DataColumns["ProjectID"];
                DataTable test = (DataTable) cboColumnProject.PropertiesComboBox.DataSource;


                DataAccess.UpdateTime(row, _toDate);

                e.Cancel = true;
                ASPxGridView1.CancelEdit();

                refreshDataGrid((String)Session["EmployeeID"]);
                calcSheetTotals();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        try
        {
            DataRow row = ((DataTable)Session["GridTimeEnterData"]).Rows.Find(e.Keys["TEID"]);

            if (row["Status"].ToString().ToUpper() == "LOCKED")
            {
                ASPxGridView1.CancelEdit();

                Exception ex = new Exception("Record locked, you cannot delete a locked record. Please unlock this record first");
                throw (ex);
            }
            else
            {
                DataAccess.DeleteTime(row);

                e.Cancel = true;
                ASPxGridView1.CancelEdit();
                
                refreshDataGrid((String)Session["EmployeeID"]);
                calcSheetTotals();
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    protected void ASPxGridView1_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        
        if (e.Column.FieldName == "Status")
        {
            e.Editor.ReadOnly = true;
            e.Editor.Enabled = false;
           // e.Editor.BackColor = System.Drawing.Color.Red;
        }
        if (((ASPxGridView)sender).IsNewRowEditing == false)
        {
            if (e.Column.FieldName == "ProjectID" || e.Column.FieldName == "RoleName" || e.Column.FieldName == "ClientID" || e.Column.FieldName == "TEOT")
            {
                e.Editor.ReadOnly = true;
                e.Editor.Enabled = false;
                // e.Editor.BackColor = System.Drawing.Color.Red;
            }
        }

        if (e.Column.FieldName == "ProjectID")
        {
            var combo = (ASPxComboBox)e.Editor;
            combo.Callback += new CallbackEventHandlerBase(Callback_Project_from_Client_Changes);
        }

        if (e.Column.FieldName == "RoleName")
        {
            var combo = (ASPxComboBox)e.Editor;
            combo.Callback += new CallbackEventHandlerBase(Callback_Role_from_Project_Changes);
        }

        if (e.Column.FieldName == "ClientID")
        {
            var combo = (ASPxComboBox)e.Editor;
            combo.Callback += new CallbackEventHandlerBase(Callback_Client_from_Project_Changes);
        }
    }

    protected void ASPxTextBoxDiscipline_Init(object sender, EventArgs e)
    {
        ASPxTextBox disciplineTextBox = (ASPxTextBox)sender;

        string discipline = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["Discipline"], "where e.EmployeeId = '" + Membership.GetUser(User.Identity.Name) + "'").Rows[0][0].ToString();

        disciplineTextBox.Text = discipline;
    }

    protected void ASPxTextBoxHoursWorkedText_TextChanged(object sender, EventArgs e)
    {

    }

    protected void DetailPanel_Callback(object sender, CallbackEventArgsBase e)
    {


        calcSheetTotals();

        //formatSheetTotals();
        if (Session["OriginalWidth"] != null)
        {
            originalWidth = int.Parse(Session["OriginalWidth"].ToString());
        }
        else
        {
            originalWidth = 0;
        }

        ASPxGridView1_CustomCallback(null, new ASPxGridViewCustomCallbackEventArgs("0;" + originalWidth.ToString()));

        ASPxCallbackPanel panel = (ASPxCallbackPanel)Master.FindControl("leftPane");
        //ASPxNavBar navbar = (ASPxNavBar)panel.FindControl("navbar");
    }

    protected void Callback_Project_from_Client_Changes(object sender, CallbackEventArgsBase e)
    {
        DataTable filteredTable = new DataTable();
        var clientID = e.Parameter;

        filteredTable = DataAccess.GetDataTableBySqlSyntax("EXEC ClientProjects '" + Membership.GetUser().UserName + "','" + clientID + "'", "");

        ASPxComboBox c = sender as ASPxComboBox;
        c.DataSource = filteredTable;
        c.DataBind();
    }

    protected void Callback_Role_from_Project_Changes(object sender, CallbackEventArgsBase e)
    {
        DataTable filteredTable = new DataTable();
        var projectID = e.Parameter;

        filteredTable = DataAccess.GetDataTableBySqlSyntax("EXEC ProjectRoles '" + Membership.GetUser().UserName + "','" + projectID + "'", "");

        ASPxComboBox c = sender as ASPxComboBox;
        c.DataSource = filteredTable;
        c.DataBind();
        //Mohammad added the following
        if (filteredTable.Rows.Count == 1) c.SelectedItem = c.Items[0];
    }

    protected void Callback_Client_from_Project_Changes(object sender, CallbackEventArgsBase e)
    {
        var projectID = e.Parameter;
        DataTable selectedClient = DataAccess.GetDataTableBySqlSyntax("SELECT ClientID FROM Project WHERE ProjectID = '" + projectID + "'", "");

        selectedProjectID = projectID;

        GridViewDataComboBoxColumn cboColumnClient = (GridViewDataComboBoxColumn)this.ASPxGridView1.DataColumns["ClientID"];

        ASPxComboBox c = sender as ASPxComboBox;
        
        c.Value = selectedClient.Rows[0]["ClientID"].ToString();
    }

    protected void ASPxButton2_Click(object sender, EventArgs e)
    {
        DataAccess.SubmitTime(_dt);
        refreshDataGrid((String)Session["EmployeeID"]);
        calcSheetTotals();

    }

    protected void btnPdfExport_Click(object sender, EventArgs e)
    {
        this.gridExport.Landscape  = true;
        this.Header.Title = "OVTime for: " + Membership.GetUser(User.Identity.Name) + System.Environment.NewLine + " for period ending: " + _toDate.ToString("dd-MMM-yyyy");

        this.gridExport.WritePdfToResponse("ovtime-" + Membership.GetUser(User.Identity.Name) + "-" + _toDate.ToString("ddMMMyyyy") + ".pdf");

        
    }

    protected void btnXlsExport_Click(object sender, EventArgs e)
    {
        
    }

    protected void ASPxGridView1_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        
        string[] parameterValues = e.Parameters.Split(';');

        int height = Convert.ToInt32(parameterValues[0]);
        int width = Convert.ToInt32(parameterValues[1]);
        //this.ASPxGridView1.SettingsPager.PageSize = (height / 100) * 2;

        if (Session["OriginalWidth"] != null)
        {
            originalWidth = int.Parse(Session["OriginalWidth"].ToString());
        }
        

        //if (originalWidth != width)
        //{

        GridViewDataColumn col = new GridViewDataColumn();

        if (width <= 1023) // 736 1023 iphone6 layout
            {
                this.ASPxGridView1.Columns["Status"].Visible = false;
                this.ASPxGridView1.Columns["RoleName"].Visible = false;

                this.ASPxGridView1.Columns["SUN_HRS"].Visible = false;
                this.ASPxGridView1.Columns["MON_HRS"].Visible = false;
                this.ASPxGridView1.Columns["TUE_HRS"].Visible = false;
                this.ASPxGridView1.Columns["WED_HRS"].Visible = false;
                this.ASPxGridView1.Columns["THU_HRS"].Visible = false;
                this.ASPxGridView1.Columns["FRI_HRS"].Visible = false;
                this.ASPxGridView1.Columns["SAT_HRS"].Visible = false;

                //create new total amount column
                this.ASPxGridView1.Columns["TOTAL_HRS"].Visible = true;

                //editor adjustments as well
                col = this.ASPxGridView1.Columns["ProjectID"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;

                col = this.ASPxGridView1.Columns["RoleName"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.True;

                col = this.ASPxGridView1.Columns["SUN_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["MON_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["TUE_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["WED_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["THU_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["FRI_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["SAT_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;

                col = this.ASPxGridView1.Columns["SUN_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["MON_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["TUE_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["WED_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["THU_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["FRI_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["SAT_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;

                col = this.ASPxGridView1.Columns["TOTAL_HRS"] as GridViewDataColumn;
                col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
   
                //this.btnPdfExport.Visible = false;
                //this.btnXlsExport.Visible = false;
            }
            else
            {
                this.ASPxGridView1.Columns["Status"].Visible = true;
                this.ASPxGridView1.Columns["RoleName"].Visible = true;

                this.ASPxGridView1.Columns["SUN_HRS"].Visible = true;
                this.ASPxGridView1.Columns["MON_HRS"].Visible = true;
                this.ASPxGridView1.Columns["TUE_HRS"].Visible = true;
                this.ASPxGridView1.Columns["WED_HRS"].Visible = true;
                this.ASPxGridView1.Columns["THU_HRS"].Visible = true;
                this.ASPxGridView1.Columns["FRI_HRS"].Visible = true;
                this.ASPxGridView1.Columns["SAT_HRS"].Visible = true;

                this.ASPxGridView1.Columns["TOTAL_HRS"].Visible = false;

                //editor adjustments as well
                col = this.ASPxGridView1.Columns["ProjectID"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 2;

                col = this.ASPxGridView1.Columns["RoleName"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 1;
                col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.True;

                col = this.ASPxGridView1.Columns["SUN_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 1;
                col = this.ASPxGridView1.Columns["MON_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 1;
                col = this.ASPxGridView1.Columns["TUE_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 1;
                col = this.ASPxGridView1.Columns["WED_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 1;
                col = this.ASPxGridView1.Columns["THU_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 1;
                col = this.ASPxGridView1.Columns["FRI_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 1;
                col = this.ASPxGridView1.Columns["SAT_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 1;

                col = this.ASPxGridView1.Columns["SAT_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 3;
                col = this.ASPxGridView1.Columns["SUN_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 3;
                col = this.ASPxGridView1.Columns["MON_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 3;
                col = this.ASPxGridView1.Columns["TUE_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 3;
                col = this.ASPxGridView1.Columns["WED_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 3;
                col = this.ASPxGridView1.Columns["THU_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 3;
                col = this.ASPxGridView1.Columns["FRI_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 3;

                col = this.ASPxGridView1.Columns["TOTAL_HRS"] as GridViewDataColumn;
                col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

                //this.btnPdfExport.Visible = true;
                //this.btnXlsExport.Visible = true;
            }

            Session["OriginalWidth"] = width;
            
            this.ASPxGridView1.DataBind();
            refreshDataGrid((String)Session["EmployeeID"]);
            calcSheetTotals();
        //}
        
        
    }

    public DataTable RemoveDuplicateRows(DataTable dTable, string colName)
    {
        Hashtable hTable = new Hashtable();
        ArrayList duplicateList = new ArrayList();

        //Add list of all the unique item value to hashtable, which stores combination of key, value pair.
        //And add duplicate item value in arraylist.
        foreach (DataRow drow in dTable.Rows)
        {
            if (hTable.Contains(drow[colName]))
                duplicateList.Add(drow);
            else
                hTable.Add(drow[colName], string.Empty);
        }

        //Removing a list of duplicate items from datatable.
        foreach (DataRow dRow in duplicateList)
            dTable.Rows.Remove(dRow);

        //Datatable which contains unique records will be return as output.
        return dTable;
    }

    private void fomatGridColumnsBasedOnSize()
    {
        GridViewColumn column = this.ASPxGridView1.Columns["ProjectID"];
        column.Visible = true;
        column.VisibleIndex = 2;
        

        

    }

    protected void ASPxGridView1_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
    {
        //e.NewValues["RoleName"] = 1;
    }


    //added by Mahmoud

    protected void FillProjectCombo(ASPxComboBox cmb, string client)
    {
        if (string.IsNullOrEmpty(client)) return;

        DataTable projects = GetProjects(client);
        cmb.Items.Clear();
        foreach (DataRow project in projects.Rows)
            cmb.Items.Add(project["Project"].ToString());
    }

    DataTable GetProjects(string ClientId)
    {
        string sql = "SELECT * FROM PROJECT p WHERE p.ClientID = '"+ClientId+"'";
        DataTable dt =  DataAccess.GetDataTableBySqlSyntax(sql, "");
        return dt;
    }

    void cmbProject_OnCallback(object source, CallbackEventArgsBase e)
    {
        FillProjectCombo(source as ASPxComboBox, e.Parameter);
    }

    //Mohammad added the following code 27/6/2018
    private bool isManager;
    private void getManagerEmployees()
    {
        DataTable dt = DataAccess.GetDataTableBySqlSyntax("SELECT EmpFName + ' ' + EmpLName as 'EmployeeName', EmployeeID FROM Employee WHERE ManagerID='" + Membership.GetUser(User.Identity.Name) + "'  OR EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "'", "");
        if (dt != null)
        {
            if (dt.Rows.Count == 1) //dt contains only logged in employee = not a manager
            {
                isManager = false;

            }
            else //User Logged in is manager
            {
                isManager = true;
                foreach (DataRow row in dt.Rows)
                {

                    ComboBoxEmployees.Items.Add(row.Field<String>("EmployeeName"), row.Field<String>("EmployeeID"));//.Add("Text", "Value");

                }
            }
        }
        else //dt is null = error connection to database
        {
            isManager = false;
        }
        Session["isManager"] = isManager;
        ComboBoxEmployees.DataBind();
    }

    private void EmployeelistConfigure()
    {
        bool isManager = (bool)Session["isManager"];
        if (!isManager) //Disable change for ComboBox
        {
            ComboBoxEmployees.Enabled = false;
        }
    }

    protected void GridViewPannel_Callback(object sender, CallbackEventArgsBase e)
    {
        String SelectedEmployee = e.Parameter; //name,value
        if (e.Parameter != null && !e.Parameter.Equals(""))
        {
            
                                                   //Refresh the timesheet with the new employee name
            String[] parts = SelectedEmployee.Split(',');
            //selectedEmployeeTimeSheet(parts[1], parts[0]);
            Session["EmployeeID"] = parts[1];
            ComboBoxEmployees.Text = parts[0]; //set the combobox text to selected employees name
            refreshDataGrid((String)Session["EmployeeID"]);
        }

    }

    private void selectedEmployeeTimeSheet(String emplyeeID, String employeeName)
    {
        //set the combobox text to selected employees name

        //get the dataTable
        //assign datatable to grid
        //refreshgrid()

        
    }
    //..............
}