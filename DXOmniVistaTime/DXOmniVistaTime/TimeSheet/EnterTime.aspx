﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Main.master" CodeFile="EnterTime.aspx.cs" Inherits="EnterTime" %>


<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
<style>
@font-face {
  font-family: 'FontAwesome';
  src: url('font/fontawesome-webfont.eot');
  src: url('font/fontawesome-webfont.eot?#iefix') format('embedded-opentype'),
    url('font/fontawesome-webfont.woff') format('woff'),
    url('font/fontawesome-webfont.ttf') format('truetype'),
      url('fonts/fontawesome-webfont.svg#svgFontName') format('svg');
}
.delete_icon
{
position: relative;
font-family: "Courier New" !important;
color: #ee784a !important;
font-size: 200% !important;
padding: 5px 7px;
background: Transparent;
text-decoration: none !important;
}
.delete_icon:hover
{
	color: #FF0000 !important;
}
.delete_icon:before
{
content: "\000D7" !important;
}

.new_icon
{
position: relative;
font-family: FontAwesome !important;
color: #6cb5c9 !important;
font-size: 20px !important;
padding: 1px 1px;
background: Transparent;
text-decoration: none !important;
}
.new_icon:hover
{
	color: #0000FF !important;
}
.new_icon:before
{
content: "\f067" !important;
}

.edit_icon
{
position: relative;
font-family: FontAwesome !important;
color: #f3cb76 !important;
font-size: 20px !important;
padding: 5px 7px;
background: Transparent;
text-decoration: none !important;
}
.edit_icon:hover
{
	color: #FF8000 !important;
}
.edit_icon:before
{
content: "\f044" !important;
}

.save_icon
{
position: relative;
font-family: FontAwesome !important;
color: #404040 !important;
font-size: 20px !important;
padding: 5px 7px;
background: Transparent;
text-decoration: none !important;
font-style: normal !important;
}
.save_icon:before
{
content: "\f0c7" !important;
}

.location_icon
{
position: relative;
font-family: FontAwesome !important;
color: #404040 !important;
font-size: 20px !important;
padding: 5px 7px;
background: Transparent;
text-decoration: none !important;
font-style: normal !important;
}
.location_icon:before
{
content: "\f041" !important;
}

.book_icon
{
position: relative;
font-family: FontAwesome !important;
color: #404040 !important;
font-size: 20px !important;
padding: 5px 7px;
background: Transparent;
text-decoration: none !important;
font-style: normal !important;
}
.book_icon:before
{
content: "\f02d" !important;
}
.newFont *
{
    font-family: Calibri ;
    font-size: 20px;
}


    </style>
    <script>
        var usrName = '<%=HttpUtility.JavaScriptStringEncode(HttpContext.Current.User.Identity.Name)%>';

        function onEdit(s, e) {
            //detailPanelSmall.PerformCallback();
            leftPane.PerformCallback();
        }
        function onDateChange(s, e) {
           
            alert("You changed the date");
            //refresh the grid only
            GridViewPannel.PerformCallback();
        }
        function onSorting(s, e) {
            //alert("Hi There!");
            //detailPanelSmall.PerformCallback();
            //leftPane.PerformCallback();
        }

        function onGridChange(s, e) {
           detailPanelSmall.PerformCallback();
           leftPane.PerformCallback();
           
        }

        /*
        //onPage Load Function --setup to adjust grid width
        $(document).ready(function () {
            var height = Math.max(0, document.documentElement.clientHeight);
            var width = Math.max(0, document.documentElement.clientWidth);
            //alert(height + ";" + width);
            ASPxGridView1.PerformCallback(height + ";" + width);
        });

        //onPage Resize Function --setup to adjust grid width
        $(window).resize(function () {
            //resize just happened, pixels changed
            var height = Math.max(0, document.documentElement.clientHeight);
            var width = Math.max(0, document.documentElement.clientWidth);
            ASPxGridView1.PerformCallback(height + ";" + width);
        });*/

        var oldwidth = 0;
        var showExport = true;
        $(document).ready(function () {

            var height = Math.max(0, document.documentElement.clientHeight);
            var width = Math.max(0, document.documentElement.clientWidth);

            if (width <= 1023) {
                showExport = false;
            }
            else {
                showExport = true;
            }
            //document.getElementById("btnPdfExport").attributes["Visible"] = showExport;
            //document.getElementById("btnXlsExport").attributes["Visible"] = showExport;

            //run on first instance
            if (oldwidth == 0) {
                var height = Math.max(0, document.documentElement.clientHeight);
                var width = Math.max(0, document.documentElement.clientWidth);
                if (width <= 1023) {
                    showExport = false;
                }
                else {
                    showExport = true;
                }
                
                //show/hide export button first
                detailPanelSmall.PerformCallback();

                //resize grid
                ASPxGridView1.PerformCallback(height + ";" + width);
            }
            //update width
            oldwidth = $(window).width();

            //fire on resize
            $(window).resize(function () {
                var nw = $(window).width();
                //compare new and old width      
                if (oldwidth != nw) {
                    var height = Math.max(0, document.documentElement.clientHeight);
                    var width = Math.max(0, document.documentElement.clientWidth);
                    if (width <= 1023) {
                        showExport = false;
                    }
                    else {
                        showExport = true;
                    }
                    //document.getElementById("btnPdfExport").Visible = showExport;
                    //document.getElementById("btnXlsExport").Visible = showExport;

                    //show/hide export button first
                    detailPanelSmall.PerformCallback();

                    //resize grid
                    
                    ASPxGridView1.PerformCallback(height + ";" + width);
                }

                oldwidth = nw;
            });
        });


        /*
        //new code for resizing grid
        var isPageSizeCallbackRequired = false;
        function OnInit(s, e) {
            OnResized();
        }
        function OnBeginCallback(s, e) {
            if (e.command != "CUSTOMCALLBACK")
                isPageSizeCallbackRequired = true;
        }
        function OnEndCallback(s, e) {
            if (isPageSizeCallbackRequired === true) {
                OnResized();
            }
            else {
                //onGridChange();
            }
        }
        function OnControlsInitialized(s, e) {
            ASPxClientUtils.AttachEventToElement(window, "resize", function (evt) {
                OnResized();
            });
        }
        function OnResized() {
            var height = Math.max(0, document.documentElement.clientHeight);
            var width = Math.max(0, document.documentElement.clientWidth);
            ASPxGridView1.PerformCallback(height + ";" + width);

            isPageSizeCallbackRequired = false;
        }
        */
    </script>

    <%--<dx:ASPxGlobalEvents ID="ge" runat="server">
        <ClientSideEvents ControlsInitialized="OnControlsInitialized" />
    </dx:ASPxGlobalEvents>--%>  

    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { LoadingPanel.Hide(); }" />
    </dx:ASPxCallback>
    <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel"
        Modal="True">
    </dx:ASPxLoadingPanel>
    <dx:ASPxPanel runat="server" CssClass="detailPanelSmallHeader"></dx:ASPxPanel>
    <dx:ASPxCallbackPanel ID="DetailPanel" runat="server" ClientInstanceName="detailPanelSmall" Width="100%" CssClass="detailPanelLarge" Collapsible="false" OnCallback="DetailPanel_Callback"  SettingsLoadingPanel-Enabled ="false" >
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <Styles>
            <ExpandBar Width="100%" CssClass="bar">
            </ExpandBar>
            <ExpandedExpandBar CssClass="expanded">
            </ExpandedExpandBar>
        </Styles>
        <BorderTop BorderWidth="0px">
        </BorderTop>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                Time Sheet Details:
                <br />
                <br />
                <dx:ASPxDateEdit runat="server" ID="ASPXDateEdit" Caption="Period End Date" AutoPostBack="false"   
                    OnCustomDisabledDate="ASPXDateEdit_CustomDisabledDate" 
                    OnCalendarCustomDisabledDate="ASPXDateEdit_CustomDisabledDate" 
                    CssClass="editor" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption">
                <ClientSideEvents ValueChanged="onDateChange" />
                </dx:ASPxDateEdit>
                <%--<dx:ASPxComboBox runat="server" ID="ASPxComboBox2"
                                        TextField="Date" ValueField="Date"
                                        SelectedIndex="0" Height="25px" 
                                        Caption="  Period End Date " 
                                        AutoPostBack="true" 
                                        OnSelectedIndexChanged="ASPxComboBox2_SelectedIndexChanged"
                                        CssClass="editor" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption"
                                        >--%><%--to show the loading panel--%><%--<ClientSideEvents SelectedIndexChanged="function(s, e) {
                                            LoadingPanel.Show();
                                        }" />--%>
               <%-- <CaptionCellStyle CssClass="editorCaption">
                </CaptionCellStyle>
                <RootStyle CssClass="editorContainer">
                </RootStyle>
                </dx:ASPxComboBox>--%>
               <dx:ASPxTextBox ID="ASPxTextBoxHoursWorkedText" Caption="Total Hours Worked" runat="server" Enabled="false" AutoPostBack="false"
                    CssClass="editor" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption">
                    <ClientSideEvents ValueChanged="onDateChange" /> <%--Refresh grid only(callBack) --%>
                    <CaptionCellStyle CssClass="editorCaption">
                    </CaptionCellStyle>
                    <RootStyle CssClass="editorContainer">
                    </RootStyle>
                </dx:ASPxTextBox>
                <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Submit "
                    ImagePosition = "Right" RootStyle-CssClass="editorContainer" 
                    CaptionCellStyle-CssClass="editorCaption"  AutoPostBack="false" 
                    Image-Url="~/Content/Images/Icons/check-64-icon.png" Image-Height="20px" 
                    Image-Width="20px" Height="36px">
              <%--  <ClientSideEvents  Click="onDateChange" />--%>
                </dx:ASPxButton>
                
                <%--<dx:ASPxLabel ID="ExportLabel" runat="server" Text="" Width="30px" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption"></dx:ASPxLabel>--%><%--<dx:ASPxButton ID="btnPdfExport" runat="server" Text="Pdf Export"  Visible="false" ImagePosition = "Right" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption" OnClick="btnPdfExport_Click"  RenderMode="Link" AutoPostBack="false" Image-Url="../Content/Images/Pdf-icon.png" Image-Height="40px" Image-Width="40px" >
                    </dx:ASPxButton>
                    
                    <dx:ASPxButton ID="btnXlsExport" runat="server" Text="Xls Export"  Visible="false" ImagePosition = "Right" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption" OnClick="btnXlsExport_Click"  RenderMode="Link" AutoPostBack="false" Image-Url="../Content/Images/excel-xls-icon.png" Image-Height="40px" Image-Width="40px">
                    </dx:ASPxButton>--%>
            </dx:PanelContent>
        </PanelCollection>
        <Paddings Padding="8px" />
    </dx:ASPxCallbackPanel>

    <dx:ASPxCallbackPanel ID="GridViewPannel" runat="server" ClientInstanceName="GridViewPannel"    Collapsible="false"   SettingsLoadingPanel-Enabled ="false" >
        <PanelCollection>
            <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                <dx:ASPxGridView ID="ASPxGridView1"   
                    runat="server" AutoGenerateColumns="False" 
                    ClientInstanceName="ASPxGridView1" 
                    SettingsDataSecurity-AllowEdit ="true" 
                    SettingsDataSecurity-AllowInsert ="true" 
                    SettingsDataSecurity-AllowDelete ="true"
                    Width="100%" 
                    AutoPostBack="true"  
                    OnDataBinding ="ASPxGridView1_DataBinding" 
                    OnDataBound = "ASPxGridView1_DataBound" 
                    OnBeforeColumnSortingGrouping="ASPxGridView1_BeforeColumnSortingGrouping"
                    Onsummarydisplaytext="ASPxGridView1_SummaryDisplayText"
                    OnRowUpdating="ASPxGridView1_RowUpdating"
                    OnRowDeleting ="ASPxGridView1_RowDeleting" 
                    OnRowInserting="ASPxGridView1_RowInserting"
                   
                    OnCellEditorInitialize="ASPxGridView1_CellEditorInitialize"
                    OnCustomCallback ="ASPxGridView1_CustomCallback"
                    OnCustomUnboundColumnData ="ASPxGridView1_CustomUnboundColumnData"
                    KeyFieldName="TEID"
                    CssClass="newFont"
                    >
                    <ClientSideEvents ColumnSorting="onSorting" EndCallback="onGridChange"    />
                    <%-- <ClientSideEvents ColumnSorting="onSorting" EndCallback="OnEndCallback" Init="OnInit" BeginCallback="OnBeginCallback" />--%>
                    <SettingsPager PageSize="50" />
                    <Paddings Padding="0px" />
                    <Border BorderWidth="0px" />
                    <BorderBottom BorderWidth="1px" />
                    <Settings ShowFooter="True" />
                    <Styles Header-Wrap="True" />
                    <SettingsCommandButton>
                        <DeleteButton Text=" ">
                           <Styles Style-CssClass="delete_icon"></Styles>
                        </DeleteButton>
                    </SettingsCommandButton>
                    <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
                    <Columns>
                        <dx:GridViewCommandColumn VisibleIndex="0"  ShowNewButtonInHeader="true" ShowDeleteButton="true" ShowEditButton="true">
                            <%--<NewButton Visible="False">
                                <Image AlternateText="New" IconID="data_addnewdatasource_16x16" ToolTip="New">
                                </Image>
                            </NewButton>
                            <EditButton Visible="True" Text="Edit" >
                                <Image AlternateText="Edit" IconID="data_editdatasource_16x16" ToolTip="Edit">
                                </Image>
                            </EditButton>
                            
                            <UpdateButton Visible="True" >
                                <Image AlternateText="Update" IconID="data_editdatasource_16x16" ToolTip="Update">
                                </Image>
                            </UpdateButton>
                            <CancelButton Visible="True">
                                <Image AlternateText="Cancel" IconID="data_deletedatasource_16x16" ToolTip="Cancel">
                                </Image>
                            </CancelButton>--%>
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="Status" VisibleIndex="1">
                            <HeaderCaptionTemplate>
                                <table>
                                  <tr>
                                    <th><i class="save_icon"></i> </th>
                                    <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Status" ForeColor="White"/></th>
                                  </tr>
                                </table>
                            </HeaderCaptionTemplate>
                            <EditFormSettings VisibleIndex="1" ColumnSpan="1" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataComboBoxColumn FieldName="ProjectID" VisibleIndex="2" Width="150">
                            <HeaderCaptionTemplate>
                                <table>
                                  <tr>
                                    <th><i class="location_icon"></i></th>
                                    <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Project" ForeColor="White"/></th>
                                  </tr>
                                </table>
                            </HeaderCaptionTemplate>
                            <EditFormSettings VisibleIndex="2" ColumnSpan="2" />
                            <PropertiesComboBox TextField="ProjectID" ValueField="ProjectID" EnableSynchronization="False"
                                IncrementalFilteringMode="StartsWith">
                                <Columns>
                                    <dx:ListBoxColumn FieldName="ProjectID" />
                                    <dx:ListBoxColumn FieldName="ProjectName" />
                                </Columns>
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>
                        <dx:GridViewDataComboBoxColumn FieldName="ActivityID" VisibleIndex="3">
                            <HeaderCaptionTemplate>
                                <table>
                                  <tr>
                                    <th><i class="book_icon"></i></th>
                                    <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Activity" ForeColor="White"/></th>
                                  </tr>
                                </table>
                            </HeaderCaptionTemplate>
                            <EditFormSettings VisibleIndex="3" ColumnSpan="1" />
                            <PropertiesComboBox TextField="ActivityDescription"  ValueField="ActivityID" EnableSynchronization="False"
                                IncrementalFilteringMode="StartsWith">
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>
                        <dx:GridViewDataTextColumn FieldName="SUN_HRS" VisibleIndex="6">
                            <%--<HeaderCaptionTemplate>
                                <table>
                                  <tr>
                                    <th><dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="../Content/Images/Icons/Calendar-Empty-icon.png" /></th>
                                    <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Activity" ForeColor="White"/></th>
                                  </tr>
                                </table>
                            </HeaderCaptionTemplate>--%>
                            <PropertiesTextEdit DisplayFormatString="N2" MaskSettings-Mask="<0..99g>.<00..99>" MaskSettings-IncludeLiterals="DecimalSymbol" />
                            <EditFormSettings ColumnSpan="1" Visible="True" VisibleIndex="6"/>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="SUN_MEMO" Visible="False">
                            <EditFormSettings Caption="Memo" ColumnSpan="3" Visible="True" VisibleIndex="7"/>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="MON_HRS" VisibleIndex="8">
                            <PropertiesTextEdit DisplayFormatString="N2" MaskSettings-Mask="<0..99g>.<00..99>" MaskSettings-IncludeLiterals="DecimalSymbol" />
                            <EditFormSettings ColumnSpan="1" Visible="True" VisibleIndex="8"/>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="MON_MEMO" Visible="False">
                            <EditFormSettings Caption="Memo" ColumnSpan="3" Visible="True" VisibleIndex="9"/>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="TUE_HRS" VisibleIndex="10">
                            <PropertiesTextEdit DisplayFormatString="N2" MaskSettings-Mask="<0..99g>.<00..99>" MaskSettings-IncludeLiterals="DecimalSymbol" />
                            <EditFormSettings ColumnSpan="1" Visible="True" VisibleIndex="10"/>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="TUE_MEMO" Visible="False">
                            <EditFormSettings Caption="Memo" ColumnSpan="3" Visible="True" VisibleIndex="11"/>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="WED_HRS" VisibleIndex="12">
                            <PropertiesTextEdit DisplayFormatString="N2" MaskSettings-Mask="<0..99g>.<00..99>" MaskSettings-IncludeLiterals="DecimalSymbol" />
                            <EditFormSettings ColumnSpan="1" Visible="True" VisibleIndex="12"/>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="WED_MEMO" Visible="False">
                            <EditFormSettings Caption="Memo" ColumnSpan="3" Visible="True" VisibleIndex="13"/>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="THU_HRS" VisibleIndex="14">
                            <PropertiesTextEdit DisplayFormatString="N2" MaskSettings-Mask="<0..99g>.<00..99>" MaskSettings-IncludeLiterals="DecimalSymbol" />
                            <EditFormSettings ColumnSpan="1" Visible="True" VisibleIndex="14"/>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="THU_MEMO" Visible="False">
                            <EditFormSettings Caption="Memo" ColumnSpan="3" Visible="True" VisibleIndex="15"/>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="FRI_HRS" VisibleIndex="16" HeaderStyle-BackColor ="#1279C0" EditFormCaptionStyle-BackColor="#1279C0">
                            <PropertiesTextEdit DisplayFormatString="N2" MaskSettings-Mask="<0..99g>.<00..99>" MaskSettings-IncludeLiterals="DecimalSymbol" />
                            <EditFormSettings ColumnSpan="1" Visible="True" VisibleIndex="16"/>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="FRI_MEMO" Visible="False">
                            <EditFormSettings Caption="Memo" ColumnSpan="3" Visible="True" VisibleIndex="17"/>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="SAT_HRS" VisibleIndex="4" HeaderStyle-BackColor ="#1279C0" EditFormCaptionStyle-BackColor ="#1279C0">
                            <PropertiesTextEdit DisplayFormatString="N2" MaskSettings-Mask="<0..99g>.<00..99>" MaskSettings-IncludeLiterals="DecimalSymbol" />
                            <EditFormSettings ColumnSpan="1" Visible="True" VisibleIndex="4"/>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="SAT_MEMO" Visible="False">
                            <EditFormSettings Caption="Memo" ColumnSpan="3" Visible="True" VisibleIndex="5"/>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="TOTAL_HRS" Caption="Total Hrs" VisibleIndex="18" UnboundType="Decimal" >
                            
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <SettingsCommandButton>
                        <NewButton Text=" ">
                           <Styles Style-CssClass="new_icon"></Styles>
                        </NewButton>
                        <EditButton Text=" " >
                            <Styles Style-CssClass="edit_icon"></Styles>
                        </EditButton>
                        <UpdateButton Text=" ">
                            <Image AlternateText="Update" Url="~/Content/Images/Icons/comment-user-page-icon.png" ToolTip="Update">
                            </Image>
                        </UpdateButton>
                        <CancelButton>
                            <Image AlternateText="Cancel" Url="~/Content/Images/Icons/comment-user-close-icon.png" ToolTip="Cancel">
                            </Image>
                        </CancelButton>
                    </SettingsCommandButton>
                    <SettingsEditing EditFormColumnCount="4"/>
                    <Styles>
                        <AlternatingRow Enabled="true" />
                    </Styles>
                    <SettingsPopup>
                        <EditForm Width="100%" Modal="false" />
                    </SettingsPopup>
                    <Templates>
                        <EditForm>
                            <%--<dx:ASPxPanel ID="ASPxPanel2" runat="server" ClientInstanceName="detailPanel" Width="100%" CssClass="detailPanel" Collapsible="False">
            <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
            <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="380" />
            <Styles>
                <ExpandBar Width="100%" CssClass="bar"></ExpandBar>
                <ExpandedExpandBar CssClass="expanded"></ExpandedExpandBar>
            </Styles>
            <BorderTop BorderWidth="0px"></BorderTop>
            <PanelCollection>
                <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">--%>
                            <div style="padding: 4px 4px 3px 4px">
                                <dx:ASPxPageControl runat="server" ID="pageControl" Width="100%" ActiveTabIndex="0" Enabled="true">
                                    <TabPages>
                                        <dx:TabPage Text="Details" Visible="true">
                                            <ContentCollection>
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </ContentCollection>
                                        </dx:TabPage>
                                        <%--<dx:TabPage Text="Notes" Visible="true">
                                            <ContentCollection>
                                                <dx:ContentControl ID="ContentControl2" runat="server">
                                                    <dx:ASPxMemo runat="server" ID="notesEditor" Text='' Width="100%"
                                                        Height="93px">
                                                    </dx:ASPxMemo>
                                                </dx:ContentControl>
                                            </ContentCollection>
                                        </dx:TabPage>--%>
                                    </TabPages>
                                </dx:ASPxPageControl>
                            </div>
                            <div style="text-align: right; padding: 2px 2px 2px 2px">
                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                    runat="server">
                                </dx:ASPxGridViewTemplateReplacement>
                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                    runat="server">
                                </dx:ASPxGridViewTemplateReplacement>
                            </div>
                            <%--</dx:PanelContent>
            </PanelCollection>
        </dx:ASPxPanel>--%>
                        </EditForm>
                    </Templates>
                </dx:ASPxGridView>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>


    <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="ASPxGridView1">
    </dx:ASPxGridViewExporter>
</asp:Content>
