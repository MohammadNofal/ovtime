﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DXOmniVistaTimeEngine;
using System.Web.Security;


public partial class ViewTime : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);

            this.ASPxComboBox2.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["getFridaysToDate"], string.Empty);
            this.ASPxComboBox2.DataBind();

            if (this.ASPxComboBox2 == null || this.ASPxComboBox2.Text == string.Empty)
            {
                this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["getTimeSheetPerUser"], " and e.EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' ");

            }
            else
            {
                this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["getTimeSheetPerUser"], " and e.EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' " + "and t.TEDate <= '" + this.ASPxComboBox2.Text + "' and t.TEDate >= dateadd(dd,-7,'" + this.ASPxComboBox2.Text + "')");
            }
            this.ASPxGridView1.DataBind();
            
        }
    }

    protected void ASPxComboBox2_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["getTimeSheetPerUser"]," and e.EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' " + "and t.TEDate <= '" + this.ASPxComboBox2.Text + "' and t.TEDate >= dateadd(dd,-7,'" + this.ASPxComboBox2.Text + "')");
        this.ASPxGridView1.DataBind();

    }

    protected void ASPxGridView1_CustomColumnSort(object sender, DevExpress.Web.CustomColumnSortEventArgs e)
    {

    }
    protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
    {
        
        
    }
    protected void ASPxGridView1_DataBound(object sender, EventArgs e)
    {
        
        
    }
    protected void ASPxGridView1_BeforeColumnSortingGrouping(object sender, DevExpress.Web.ASPxGridViewBeforeColumnGroupingSortingEventArgs e)
    {
        if (this.ASPxComboBox2 == null || this.ASPxComboBox2.Text == string.Empty)
        {
            this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["getTimeSheetPerUser"], string.Empty);

        }
        else
        {
            this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["getTimeSheetPerUser"], " and e.EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' " + "and t.TEDate <= '" + this.ASPxComboBox2.Text + "' and t.TEDate >= dateadd(dd,-7,'" + this.ASPxComboBox2.Text + "')");
        }
        this.ASPxGridView1.DataBind();
    }
}