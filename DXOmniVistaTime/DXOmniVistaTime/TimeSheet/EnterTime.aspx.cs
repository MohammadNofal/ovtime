﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DXOmniVistaTimeEngine;
using System.Web.Security;
using DevExpress.Web;
using System.Text;
using DevExpress.Export;
using DevExpress.XtraPrinting;


public partial class EnterTime : System.Web.UI.Page
{
    public DataTable _dt = new DataTable();
    private DateTime _friday = new DateTime();
    private int originalWidth;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.LoadingPanel.ContainerElementID = "GridViewPannel";
            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);

            _friday = getFridayDate(DateTime.Now);
            this.ASPXDateEdit.Date = _friday;
            DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);


            GridViewDataComboBoxColumn cboColumnProject = (GridViewDataComboBoxColumn)this.ASPxGridView1.DataColumns["ProjectID"];
            string projectIdsUsedFilter = ConfigurationManager.AppSettings["ProjectIdsUsedFilter"].Replace("[EMPLOYEE]", " and t.EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' ").Replace("[TIME]", " and TEDATE <= '" + _friday.ToShortDateString() + "' and TEDATE >= dateadd(dd,-6,'" + _friday.ToShortDateString() + "')");

            
            cboColumnProject.PropertiesComboBox.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["ProjectIds"], " and ec.EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' ");

            cboColumnProject.PropertiesComboBox.ValueField = "ProjectID";
            cboColumnProject.PropertiesComboBox.TextField = "ProjectName";

            GridViewDataComboBoxColumn cboColumnActivity = (GridViewDataComboBoxColumn)this.ASPxGridView1.DataColumns["ActivityID"];
            cboColumnActivity.PropertiesComboBox.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["ActivityIds"], " where ec.EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' "); ;
            cboColumnActivity.PropertiesComboBox.ValueField = "ActivityID";
            cboColumnActivity.PropertiesComboBox.TextField = "ActivityDescription";


            formatGridHeaderSheet();
            refreshDataGrid();
            calcSheetTotals();
            formatSheetTotals();

            

            
        }
        else
        {
            //refresh project list based on used projects in time period; dont allow duplicates
            GridViewDataComboBoxColumn cboColumnProject = (GridViewDataComboBoxColumn)this.ASPxGridView1.DataColumns["ProjectID"];

            _friday = getFridayDate(this.ASPXDateEdit.Date);
            this.ASPXDateEdit.Date = _friday;
            
            string projectIdsUsedFilter = ConfigurationManager.AppSettings["ProjectIdsUsedFilter"].Replace("[EMPLOYEE]", " and t.EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' ").Replace("[TIME]", " and TEDATE <= '" + _friday.ToShortDateString() + "' and TEDATE >= dateadd(dd,-6,'" + _friday.ToShortDateString() + "')");

            cboColumnProject.PropertiesComboBox.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["ProjectIds"], " and ec.EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' ");
            cboColumnProject.PropertiesComboBox.ValueField = "ProjectID";
            cboColumnProject.PropertiesComboBox.TextField = "ProjectID";
            formatGridHeaderSheet();
            calcSheetTotals();
        }
    }


    protected void ASPXDateEdit_CustomDisabledDate(object sender, CalendarCustomDisabledDateEventArgs e)
    {
        if (e.Date.DayOfWeek == DayOfWeek.Saturday ||
            e.Date.DayOfWeek == DayOfWeek.Sunday ||
            e.Date.DayOfWeek == DayOfWeek.Monday ||
            e.Date.DayOfWeek == DayOfWeek.Tuesday ||
            e.Date.DayOfWeek == DayOfWeek.Wednesday ||
            e.Date.DayOfWeek == DayOfWeek.Thursday)
            e.IsDisabled = true;
    }

    protected void ASPXDateEdit_CalendarDayCellPrepared(object sender, CalendarDayCellPreparedEventArgs e)
    {
        if (e.Date.DayOfWeek == DayOfWeek.Saturday ||
            e.Date.DayOfWeek == DayOfWeek.Sunday ||
            e.Date.DayOfWeek == DayOfWeek.Monday ||
            e.Date.DayOfWeek == DayOfWeek.Tuesday ||
            e.Date.DayOfWeek == DayOfWeek.Wednesday ||
            e.Date.DayOfWeek == DayOfWeek.Thursday)
        {
           e.Cell.Attributes["disabled"] = "disabled";
           e.Cell.Attributes["style"]= "pointer-events:none";
        }
    }

    private DateTime getFridayDate(DateTime passedDate)
    {
        switch (passedDate.DayOfWeek)
        {
            case DayOfWeek.Saturday:
                return passedDate.AddDays(6);
            case DayOfWeek.Sunday:
                return passedDate.AddDays(5);
            case DayOfWeek.Monday:
                return passedDate.AddDays(4);
            case DayOfWeek.Tuesday:
                return passedDate.AddDays(3);
            case DayOfWeek.Wednesday:
                 return passedDate.AddDays(2);
            case DayOfWeek.Thursday:
                return passedDate.AddDays(1);
            case DayOfWeek.Friday:
                return passedDate.AddDays(0);
        }

        return passedDate;

    }

    protected void ASPXDateEdit_DateChanged(object sender, EventArgs e)
    {
        _friday = getFridayDate(this.ASPXDateEdit.Date);
        
        formatGridHeaderSheet();


        this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["TimeEntryPeriod"].Replace("[SAT]", _friday.AddDays(-6).ToShortDateString()).Replace("[SUN]", _friday.AddDays(-5).ToShortDateString()).Replace("[MON]", _friday.AddDays(-4).ToShortDateString()).Replace("[TUE]", _friday.AddDays(-3).ToShortDateString()).Replace("[WED]", _friday.AddDays(-2).ToShortDateString()).Replace("[THU]", _friday.AddDays(-1).ToShortDateString()).Replace("[FRI]", _friday.AddDays(-0).ToShortDateString()).Replace("[FILTER]", " and e.EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' " + "and TEDATE <= '" + _friday.ToShortDateString() + "' and TEDATE >= dateadd(dd,-6,'" + _friday.ToShortDateString() + "')"), " and EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' " + "and TEDATE <= '" + _friday.ToShortDateString()+ "' and TEDATE >= dateadd(dd,-6,'" + _friday.ToShortDateString() + "')");
        this.ASPxGridView1.DataBind();
        
        calcSheetTotals();

    }


    protected void ASPxGridView1_CustomColumnSort(object sender, DevExpress.Web.CustomColumnSortEventArgs e)
    {

    }
    protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
    {
        
        
    }
    protected void ASPxGridView1_DataBound(object sender, EventArgs e)
    {
        
        
    }
    protected void ASPxGridView1_BeforeColumnSortingGrouping(object sender, DevExpress.Web.ASPxGridViewBeforeColumnGroupingSortingEventArgs e)
    {
        if (this.ASPXDateEdit == null || this.ASPXDateEdit.Text == string.Empty)
        {
            //this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["getTimeSheetPerUser"], string.Empty);

        }
        else
        {
            _friday = getFridayDate(this.ASPXDateEdit.Date);
            
            this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["TimeEntryPeriod"].Replace("[SAT]", _friday.AddDays(-6).ToShortDateString()).Replace("[SUN]", _friday.AddDays(-5).ToShortDateString()).Replace("[MON]", _friday.AddDays(-4).ToShortDateString()).Replace("[TUE]", _friday.AddDays(-3).ToShortDateString()).Replace("[WED]", _friday.AddDays(-2).ToShortDateString()).Replace("[THU]", _friday.AddDays(-1).ToShortDateString()).Replace("[FRI]", _friday.AddDays(-0).ToShortDateString()).Replace("[FILTER]", " and e.EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' " + "and TEDATE <= '" + _friday.ToShortDateString() + "' and TEDATE >= dateadd(dd,-6,'" + _friday.ToShortDateString() + "')"), " and EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' " + "and TEDATE <= '" + _friday.ToShortDateString() + "' and TEDATE >= dateadd(dd,-6,'" + _friday.ToShortDateString() + "')");
        
            
        }
        this.ASPxGridView1.DataBind();
        //ASPxGridView1.Columns["SAT_MEMO"].Visible = false;
    }

    private void formatGridHeaderSheet()
    {
        _friday = getFridayDate(this.ASPXDateEdit.Date);
        
        this.ASPxGridView1.DataColumns["SAT_HRS"].Caption = "Sat" + System.Environment.NewLine + _friday.AddDays(-6).ToShortDateString();
        this.ASPxGridView1.DataColumns["SUN_HRS"].Caption = "Sun" + System.Environment.NewLine + _friday.AddDays(-5).ToShortDateString();
        this.ASPxGridView1.DataColumns["MON_HRS"].Caption = "Mon" + System.Environment.NewLine + _friday.AddDays(-4).ToShortDateString();
        this.ASPxGridView1.DataColumns["TUE_HRS"].Caption = "Tue" + System.Environment.NewLine + _friday.AddDays(-3).ToShortDateString();
        this.ASPxGridView1.DataColumns["WED_HRS"].Caption = "Wed" + System.Environment.NewLine + _friday.AddDays(-2).ToShortDateString();
        this.ASPxGridView1.DataColumns["THU_HRS"].Caption = "Thu" + System.Environment.NewLine + _friday.AddDays(-1).ToShortDateString();
        this.ASPxGridView1.DataColumns["FRI_HRS"].Caption = "Fri" + System.Environment.NewLine + _friday.AddDays(-0).ToShortDateString();
    }

    private void formatSheetTotals()
    {
        this.ASPxGridView1.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "SAT_HRS");
        this.ASPxGridView1.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "SUN_HRS");
        this.ASPxGridView1.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "MON_HRS");
        this.ASPxGridView1.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "TUE_HRS");
        this.ASPxGridView1.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "WED_HRS");
        this.ASPxGridView1.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "THU_HRS");
        this.ASPxGridView1.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "FRI_HRS");

        ASPxGridView1.SettingsBehavior.ConfirmDelete = true;
    }

    private void  calcSheetTotals()
    {
        if (this.ASPxGridView1.DataSource == null)
        {
            refreshDataGrid();
        }
        
        _dt = (DataTable)this.ASPxGridView1.DataSource;
        _dt.PrimaryKey = new DataColumn[] { _dt.Columns["TEID"] };
        

        var sumSat = _dt.AsEnumerable().Sum(x => x.Field<double>("SAT_HRS"));
        var sumSun = _dt.AsEnumerable().Sum(x => x.Field<double>("SUN_HRS"));
        var sumMon = _dt.AsEnumerable().Sum(x => x.Field<double>("MON_HRS"));
        var sumTue = _dt.AsEnumerable().Sum(x => x.Field<double>("TUE_HRS"));
        var sumWed = _dt.AsEnumerable().Sum(x => x.Field<double>("WED_HRS"));
        var sumThu = _dt.AsEnumerable().Sum(x => x.Field<double>("THU_HRS"));
        var sumFri = _dt.AsEnumerable().Sum(x => x.Field<double>("FRI_HRS"));

        this.ASPxTextBoxHoursWorkedText.Text = (sumSat + sumSun + sumMon + sumTue + sumWed + sumThu + sumFri).ToString();
        this.ASPxTextBoxHoursWorkedText.Value = (sumSat + sumSun + sumMon + sumTue + sumWed + sumThu + sumFri).ToString();

        
        Session["GridTimeEnterData"] = _dt;
    }

    protected void ASPxGridView1_CustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "TOTAL_HRS")
        {
            decimal satHrs = Convert.ToDecimal(e.GetListSourceFieldValue("SAT_HRS")); 
            decimal sunHrs = Convert.ToDecimal(e.GetListSourceFieldValue("SUN_HRS"));
            decimal monHrs = Convert.ToDecimal(e.GetListSourceFieldValue("MON_HRS"));
            decimal tueHrs = Convert.ToDecimal(e.GetListSourceFieldValue("TUE_HRS"));
            decimal wedHrs = Convert.ToDecimal(e.GetListSourceFieldValue("WED_HRS"));
            decimal thuHrs = Convert.ToDecimal(e.GetListSourceFieldValue("THU_HRS"));
            decimal friHrs = Convert.ToDecimal(e.GetListSourceFieldValue("FRI_HRS"));


            e.Value = satHrs + sunHrs + monHrs + tueHrs + wedHrs + thuHrs + friHrs;
        }
    }


    private void refreshDataGrid()
    {
        if (this.ASPXDateEdit == null || this.ASPXDateEdit.Text == string.Empty)
        {
            //this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["TimeEntryPeriod"], " and e.EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' ");
        }
        else
        {
            _friday = getFridayDate(this.ASPXDateEdit.Date);
            this.ASPxGridView1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["TimeEntryPeriod"].Replace("[SAT]", _friday.AddDays(-6).ToShortDateString()).Replace("[SUN]", _friday.AddDays(-5).ToShortDateString()).Replace("[MON]", _friday.AddDays(-4).ToShortDateString()).Replace("[TUE]", _friday.AddDays(-3).ToShortDateString()).Replace("[WED]", _friday.AddDays(-2).ToShortDateString()).Replace("[THU]", _friday.AddDays(-1).ToShortDateString()).Replace("[FRI]", _friday.AddDays(-0).ToShortDateString()).Replace("[FILTER]", " and e.EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' " + "and TEDATE <= '" + _friday.ToShortDateString() + "' and TEDATE >= dateadd(dd,-7,'" + _friday.ToShortDateString() + "')"), " and EmployeeID = '" + Membership.GetUser(User.Identity.Name) + "' " + "and TEDATE <= '" + _friday.ToShortDateString() + "' and TEDATE >= dateadd(dd,-7,'" + _friday.ToShortDateString() + "')");

        }
        this.ASPxGridView1.DataBind();
    }

    protected void ASPxGridView1_SummaryDisplayText(object sender, ASPxGridViewSummaryDisplayTextEventArgs e)
    {
        if (e.Item.FieldName == "SAT_HRS" || e.Item.FieldName == "SUN_HRS" || e.Item.FieldName == "MON_HRS" || e.Item.FieldName == "TUE_HRS" ||
            e.Item.FieldName == "WED_HRS" || e.Item.FieldName == "THU_HRS" || e.Item.FieldName == "FRI_HRS")
            e.Text = string.Format("{0:N2}", Convert.ToDouble(e.Value));
    }

    protected void ASPxGridView1_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        _friday = getFridayDate(this.ASPXDateEdit.Date);
        
        string weekDay = string.Empty;
        string dayHRS = string.Empty;

        
        DataRow row = ((DataTable)Session["GridTimeEnterData"]).NewRow();
        row["TEID"] = ((DataTable)Session["GridTimeEnterData"]).Rows.Count + 1;
        row["EmployeeID"] = User.Identity.Name;
        row["ProjectID"] = e.NewValues["ProjectID"];
        row["ActivityID"] = e.NewValues["ActivityID"];
        row["SAT_HRS"] = e.NewValues["SAT_HRS"];
        row["SUN_HRS"] = e.NewValues["SUN_HRS"];
        row["MON_HRS"] = e.NewValues["MON_HRS"];
        row["TUE_HRS"] = e.NewValues["TUE_HRS"];
        row["WED_HRS"] = e.NewValues["WED_HRS"];
        row["THU_HRS"] = e.NewValues["THU_HRS"];
        row["FRI_HRS"] = e.NewValues["FRI_HRS"];

        //memo fields
        row["SAT_MEMO"] = e.NewValues["SAT_MEMO"];
        row["SUN_MEMO"] = e.NewValues["SUN_MEMO"];
        row["MON_MEMO"] = e.NewValues["MON_MEMO"];
        row["TUE_MEMO"] = e.NewValues["TUE_MEMO"];
        row["WED_MEMO"] = e.NewValues["WED_MEMO"];
        row["THU_MEMO"] = e.NewValues["THU_MEMO"];
        row["FRI_MEMO"] = e.NewValues["FRI_MEMO"];

        DataTable dt = (DataTable)Session["GridTimeEnterData"];
        dt.PrimaryKey = new DataColumn[] { _dt.Columns["TEID"] };

        /*var results = from dataRow in dt.AsEnumerable()
                      where dataRow.Field<string>("ProjectId") + "_" + dataRow.Field<string>("ActivityId") == row["ProjectID"].ToString() + "_" + row["ActivityID"].ToString()
                      select dataRow;
         */
        int count = 0;
        count = dt.AsEnumerable()
              .Count(dataRow => dataRow.Field<string>("ProjectId") + "_" + dataRow.Field<string>("ActivityId") == row["ProjectID"].ToString() + "_" + row["ActivityID"].ToString());



        if (count != 0)
        {
            ASPxGridView1.CancelEdit();

            Exception ex = new Exception("Cannot use the same Project ID and Activity ID combination. Please correct this record to save.");
            throw (ex);
        } 
        else if (row["ProjectID"].ToString().ToUpper() == string.Empty && row["ActivityID"].ToString().ToUpper() == string.Empty)
        {
            ASPxGridView1.CancelEdit();

            Exception ex = new Exception("Project ID and Activity ID are needed to save record. Please correct this record to save.");
            throw (ex);
        }
        else if (row["ProjectID"].ToString().ToUpper() == string.Empty )
        {
            ASPxGridView1.CancelEdit();

            Exception ex = new Exception("Project ID is needed to save record. Please correct this record to save.");
            throw (ex);
        }
        else if (row["ActivityID"].ToString().ToUpper() == string.Empty)
        {
            ASPxGridView1.CancelEdit();

            Exception ex = new Exception("Activity ID is needed to save record. Please correct this record to save.");
            throw (ex);
        }
        else if (double.Parse(row["SAT_HRS"].ToString())== 0 &&
                double.Parse(row["SUN_HRS"].ToString()) == 0 &&
                double.Parse(row["MON_HRS"].ToString()) == 0 &&
                double.Parse(row["TUE_HRS"].ToString()) == 0 &&
                double.Parse(row["WED_HRS"].ToString()) == 0 &&
                double.Parse(row["THU_HRS"].ToString()) == 0 &&
                double.Parse(row["FRI_HRS"].ToString())== 0 
                )
        {
            ASPxGridView1.CancelEdit();

            Exception ex = new Exception("Atleast one date must have a time value. Please correct this record to save.");
            throw (ex);
        }
        else
        {

            DataAccess.InsertTime(row, _friday);
        }
        e.Cancel = true;
        ASPxGridView1.CancelEdit();
        
        refreshDataGrid();
        calcSheetTotals();

        

    }

    protected void ASPxGridView1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        try
        {
            DataRow row = ((DataTable)Session["GridTimeEnterData"]).Rows.Find(e.Keys["TEID"]);
            row["SAT_HRS"] = e.NewValues["SAT_HRS"];
            row["SUN_HRS"] = e.NewValues["SUN_HRS"];
            row["MON_HRS"] = e.NewValues["MON_HRS"];
            row["TUE_HRS"] = e.NewValues["TUE_HRS"];
            row["WED_HRS"] = e.NewValues["WED_HRS"];
            row["THU_HRS"] = e.NewValues["THU_HRS"];
            row["FRI_HRS"] = e.NewValues["FRI_HRS"];

            //memo fields
            row["SAT_MEMO"] = e.NewValues["SAT_MEMO"];
            row["SUN_MEMO"] = e.NewValues["SUN_MEMO"];
            row["MON_MEMO"] = e.NewValues["MON_MEMO"];
            row["TUE_MEMO"] = e.NewValues["TUE_MEMO"];
            row["WED_MEMO"] = e.NewValues["WED_MEMO"];
            row["THU_MEMO"] = e.NewValues["THU_MEMO"];
            row["FRI_MEMO"] = e.NewValues["FRI_MEMO"];

            if (row["Status"].ToString().ToUpper() == "LOCKED")
            {
                ASPxGridView1.CancelEdit();

                Exception ex = new Exception("Record locked, you cannot update a locked record. Please unlock this record first");
                throw (ex);
                
            }
            else if (double.Parse(row["SAT_HRS"].ToString()) == 0 &&
                    double.Parse(row["SUN_HRS"].ToString()) == 0 &&
                    double.Parse(row["MON_HRS"].ToString()) == 0 &&
                    double.Parse(row["TUE_HRS"].ToString()) == 0 &&
                    double.Parse(row["WED_HRS"].ToString()) == 0 &&
                    double.Parse(row["THU_HRS"].ToString()) == 0 &&
                    double.Parse(row["FRI_HRS"].ToString()) == 0
                    )
            {
                ASPxGridView1.CancelEdit();

                Exception ex = new Exception("Atleast one date must have a time value. Please correct this record to save.");
                throw (ex);
            }
            else
            {
                DataAccess.UpdateTime(row, _friday);

                e.Cancel = true;
                ASPxGridView1.CancelEdit();

                refreshDataGrid();
                calcSheetTotals();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    

    protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        try
        {
            DataRow row = ((DataTable)Session["GridTimeEnterData"]).Rows.Find(e.Keys["TEID"]);

            if (row["Status"].ToString().ToUpper() == "LOCKED")
            {
                ASPxGridView1.CancelEdit();

                Exception ex = new Exception("Record locked, you cannot delete a locked record. Please unlock this record first");
                throw (ex);
            }
            else
            {
                DataAccess.DeleteTime(row);

                e.Cancel = true;
                ASPxGridView1.CancelEdit();
                
                refreshDataGrid();
                calcSheetTotals();
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    
    //protected void btnShowPopup_Click(object sender, EventArgs e) {
    //        txtPopup.Text = txtMain.Text;
    //        ASPxPopupControl2.ShowOnPageLoad = true;

         
    //    }


    /*
        protected void btnOK_Click(object sender, EventArgs e) {
            // TODO: your code is here to process the popup window's data at the server
            txtMain.Text = txtPopup.Text;
            ASPxPopupControl1.ShowOnPageLoad = false;
        }
    */
    protected void ASPxGridView1_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        
        if (e.Column.FieldName == "Status")
        {
            e.Editor.ReadOnly = true;
            e.Editor.Enabled = false;
           // e.Editor.BackColor = System.Drawing.Color.Red;
        }
        if (((ASPxGridView)sender).IsNewRowEditing == false)
        {
            if (e.Column.FieldName == "ProjectID" || e.Column.FieldName == "ActivityID")
            {
                e.Editor.ReadOnly = true;
                e.Editor.Enabled = false;
                // e.Editor.BackColor = System.Drawing.Color.Red;
            }
        }

        
    }
    protected void ASPxTextBoxHoursWorkedText_TextChanged(object sender, EventArgs e)
    {

    }
    protected void DetailPanel_Callback(object sender, CallbackEventArgsBase e)
    {
        calcSheetTotals();

        //formatSheetTotals();
        if (Session["OriginalWidth"] != null)
        {
            originalWidth = int.Parse(Session["OriginalWidth"].ToString());
        }
        else
        {
            originalWidth = 0;
        }

        ASPxGridView1_CustomCallback(null, new ASPxGridViewCustomCallbackEventArgs("0;" + originalWidth.ToString()));

        ASPxCallbackPanel panel = (ASPxCallbackPanel)Master.FindControl("leftPane");
        //ASPxNavBar navbar = (ASPxNavBar)panel.FindControl("navbar");
    }
   // protected void GridViewPannel_Callback(object sender, CallbackEventArgsBase e)
   // {
       // refreshDataGrid();
   // }
    protected void ASPxButton2_Click(object sender, EventArgs e)
    {
        //DataAccess.SubmitTime(_dt);

       // refreshDataGrid();
        //calcSheetTotals();

    }
    protected void btnPdfExport_Click(object sender, EventArgs e)
    {
        this.gridExport.Landscape  = true;
        this.Header.Title = "OVTime for: " + Membership.GetUser(User.Identity.Name) + System.Environment.NewLine + " for period ending: " + _friday.ToString("dd-MMM-yyyy");

        this.gridExport.WritePdfToResponse("ovtime-" + Membership.GetUser(User.Identity.Name) + "-" + _friday.ToString("ddMMMyyyy") + ".pdf");

        
    }
    protected void btnXlsExport_Click(object sender, EventArgs e)
    {

    }
    protected void ASPxGridView1_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        
        string[] parameterValues = e.Parameters.Split(';');

        int height = Convert.ToInt32(parameterValues[0]);
        int width = Convert.ToInt32(parameterValues[1]);
        //this.ASPxGridView1.SettingsPager.PageSize = (height / 100) * 2;

        if (Session["OriginalWidth"] != null)
        {
            originalWidth = int.Parse(Session["OriginalWidth"].ToString());
        }
        

        //if (originalWidth != width)
        //{

        GridViewDataColumn col = new GridViewDataColumn();

        if (width <= 1023) // 736 1023 iphone6 layout
            {
                this.ASPxGridView1.Columns["Status"].Visible = false;
                this.ASPxGridView1.Columns["ActivityID"].Visible = false;

                this.ASPxGridView1.Columns["SAT_HRS"].Visible = false;
                this.ASPxGridView1.Columns["SUN_HRS"].Visible = false;
                this.ASPxGridView1.Columns["MON_HRS"].Visible = false;
                this.ASPxGridView1.Columns["TUE_HRS"].Visible = false;
                this.ASPxGridView1.Columns["WED_HRS"].Visible = false;
                this.ASPxGridView1.Columns["THU_HRS"].Visible = false;
                this.ASPxGridView1.Columns["FRI_HRS"].Visible = false;

                //create new total amount column
                this.ASPxGridView1.Columns["TOTAL_HRS"].Visible = true;

                //editor adjustments as well
                col = this.ASPxGridView1.Columns["ProjectID"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;

                col = this.ASPxGridView1.Columns["ActivityID"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.True;

                col = this.ASPxGridView1.Columns["SAT_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["SUN_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["MON_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["TUE_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["WED_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["THU_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["FRI_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;

                col = this.ASPxGridView1.Columns["SAT_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["SUN_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["MON_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["TUE_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["WED_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["THU_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;
                col = this.ASPxGridView1.Columns["FRI_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 4;

                col = this.ASPxGridView1.Columns["TOTAL_HRS"] as GridViewDataColumn;
                col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
   
                //this.btnPdfExport.Visible = false;
                //this.btnXlsExport.Visible = false;
            }
            else
            {
                this.ASPxGridView1.Columns["Status"].Visible = true;
                this.ASPxGridView1.Columns["ActivityID"].Visible = true;

                this.ASPxGridView1.Columns["SAT_HRS"].Visible = true;
                this.ASPxGridView1.Columns["SUN_HRS"].Visible = true;
                this.ASPxGridView1.Columns["MON_HRS"].Visible = true;
                this.ASPxGridView1.Columns["TUE_HRS"].Visible = true;
                this.ASPxGridView1.Columns["WED_HRS"].Visible = true;
                this.ASPxGridView1.Columns["THU_HRS"].Visible = true;
                this.ASPxGridView1.Columns["FRI_HRS"].Visible = true;

                this.ASPxGridView1.Columns["TOTAL_HRS"].Visible = false;

                //editor adjustments as well
                col = this.ASPxGridView1.Columns["ProjectID"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 2;

                col = this.ASPxGridView1.Columns["ActivityID"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 1;
                col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.True;

                col = this.ASPxGridView1.Columns["SAT_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 1;
                col = this.ASPxGridView1.Columns["SUN_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 1;
                col = this.ASPxGridView1.Columns["MON_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 1;
                col = this.ASPxGridView1.Columns["TUE_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 1;
                col = this.ASPxGridView1.Columns["WED_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 1;
                col = this.ASPxGridView1.Columns["THU_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 1;
                col = this.ASPxGridView1.Columns["FRI_HRS"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 1;

                col = this.ASPxGridView1.Columns["SAT_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 3;
                col = this.ASPxGridView1.Columns["SUN_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 3;
                col = this.ASPxGridView1.Columns["MON_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 3;
                col = this.ASPxGridView1.Columns["TUE_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 3;
                col = this.ASPxGridView1.Columns["WED_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 3;
                col = this.ASPxGridView1.Columns["THU_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 3;
                col = this.ASPxGridView1.Columns["FRI_MEMO"] as GridViewDataColumn;
                col.EditFormSettings.ColumnSpan = 3;

                col = this.ASPxGridView1.Columns["TOTAL_HRS"] as GridViewDataColumn;
                col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

                //this.btnPdfExport.Visible = true;
                //this.btnXlsExport.Visible = true;
            }

            Session["OriginalWidth"] = width;
            
            this.ASPxGridView1.DataBind();
            refreshDataGrid();
            calcSheetTotals();
        //}
        
        
    }

    private void fomatGridColumnsBasedOnSize()
    {
        GridViewColumn column = this.ASPxGridView1.Columns["ProjectID"];
        column.Visible = true;
        column.VisibleIndex = 2;
        

        

    }
    
}