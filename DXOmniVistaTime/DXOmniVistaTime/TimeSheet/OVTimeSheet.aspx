﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/OVMaster.master" CodeFile="OVTimeSheet.aspx.cs" Inherits="OVTimeSheet" %>

<asp:Content ID="header_" ContentPlaceHolderID="Header" runat="server">
<h4><span class="text-semibold">Time Sheet</span></h4>
</asp:Content>


<asp:Content ID="Content" ContentPlaceHolderID="ContentPage" runat="server">

    <style>
         .dxWeb_pPrevDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNext {
             background: url('../content/images/iconmonstr-arrow-63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pPrev {
             background: url('../content/images/iconmonstr-arrow-64-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }

         .dxWeb_pNextDisabled {
             background: url('../content/images/iconmonstr-arrow-disabled63-16.png');
             width: 16px;
             height: 16px;
             margin-top: 3px;
         }
        .month1_border 
        {
            /*border-left-width: 1px !important;*/
        }

        .head_container 
        {
            background-color: #EEE;
            float: right;
            margin-right: 7.8%;
        }

        .Month1_css 
        {
            border-style: solid;
            border-width: medium;
            float: right;
            background-color: red;
        }
        .Month2_css 
        {
            border-style: solid;
            border-width: medium;
            float: right;
        }
        .delete_icon 
        {
            position: relative;
            font-family: "Courier New" !important;
            color: #ee784a !important;
            font-size: 200% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .delete_icon:hover 
        {
            color: #FF0000 !important;
        }
        .delete_icon::before 
        {
            content: "\000D7" !important;
        }
        .new_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 20px !important;
            padding: 1px 1px;
            text-decoration: none !important;
        }

        .new_icon:hover 
        {
            color: #7373FF !important;
        }
        .new_icon:before 
        {
           content: "\f067" !important;
        }
        .edit_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #f3cb76 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .edit_icon:hover 
        {
            color: #FF8000 !important;
        }
        .edit_icon:before 
        {
            content: "\f044" !important;
        }
        .save_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
            text-align: center !important;
        }

        .save_icon:before 
        {
            content: "\f0c7" !important;
        }
        .location_icon
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
        }
        .location_icon:before 
        {
            content: "\f041" !important;
        }
        .book_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #404040 !important;
            font-size: 20px !important;
            padding: 5px 7px;
            text-decoration: none !important;
            font-style: normal !important;
            text-align: center !important;
        }
        .book_icon:before 
        {
            content: "\f02d" !important;
        }
        .cancel_icon 
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #ee784a !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .cancel_icon:hover 
        {
            color: #FF8080 !important;
        }
        .cancel_icon:before
        {
            content: "\f0e2" !important;
        }
        .update_icon
        {
            position: relative;
            font-family: FontAwesome !important;
            color: #6cb5c9 !important;
            font-size: 140% !important;
            padding: 5px 7px;
            text-decoration: none !important;
        }
        .update_icon:hover 
        {
            color: #7373FF !important;
        }
        .update_icon:before 
        {
            content: "\f05d " !important;
        }
        .header 
        {
            font-size: 1.25em !important;
        }
        .ProjectDescription 
        {
            font-size: 14px !important;
        }

        .newFont * 
        {
            font-family: Calibri;
            font-size: 16px;
        }

        .newFontLeft * 
        {
            font-family: Calibri;
            font-size: 16px;
            text-align: right;
        }

        .TextBox {
            width: 100%;
            padding: 7px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }

        .ComboBox {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }

        .DateEdit {
            width: 100%;
            padding: 7px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }

        label {
          padding: 12px 12px 12px 0;
          display: inline-block;
        }

        /* Style the container */
        .container {
          border-radius: 5px;
          background-color: #f2f2f2;
          padding: 20px;
        }

        /* Floating column for labels: 25% width */
        .col-25 {
          float: left;
          width: 25%;
          margin-top: 6px;
        }

        /* Floating column for inputs: 75% width */
        .col-75 {
          float: left;
          width: 75%;
          margin-top: 6px;
        }
        .pagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #333333 !important;
            text-decoration: none !important;
            padding-left: 7px !important;
            padding-right: 7px !important;
        }
        .pagenumber:hover{
            background-color : #333333;
            color: #FFFFFF !important;
        }
        .currentpagenumber{
            border-color: #333333;
            border-radius: 20px;
            border-width: 1.5px;
            border-style:solid;
            color: #FFFFFF !important;
            background-color: #333333;
            text-decoration: none !important;
        }
        .dxeButtonEditButton{
            background: none !important;
            border: none !important;
        }
    </style>
    <script>
        var usrName = '<%=HttpUtility.JavaScriptStringEncode(HttpContext.Current.User.Identity.Name)%>';
        var lastClient = null;

        function OnClientChanged(cmbClient) {
            if (ASPxGridView1.GetEditor("ProjectID").InCallback())
                lastClient = cmbClient.GetValue().toString();
            else
                ASPxGridView1.GetEditor("ProjectID").PerformCallback(cmbClient.GetValue().toString());
        }

        function onDateChange(s, e) {
            GridViewPannel.PerformCallback("");//Mohammad eddited this
            detailPanelSmall.PerformCallback();
        }
        function onSubmition(s, e)
        {  
           ASPxButton2_Click();
           GridViewPannel.PerformCallback("");//Mohammad eddited this
        }
        function onGridChange(s, e)
        {
           detailPanelSmall.PerformCallback();
        }

        var oldwidth = 0;
        var showExport = true;
        $(document).ready(function () {

            var height = Math.max(0, document.documentElement.clientHeight);
            var width = Math.max(0, document.documentElement.clientWidth);

            if (width <= 1023) {
                showExport = false;
            }
            else {
                showExport = true;
            }
           
            //run on first instance
            if (oldwidth == 0) {
                var height = Math.max(0, document.documentElement.clientHeight);
                var width = Math.max(0, document.documentElement.clientWidth);
                if (width <= 1023) {
                    showExport = false;
                }
                else {
                    showExport = true;
                }

                //show/hide export button first
                detailPanelSmall.PerformCallback();

                //resize grid
                ASPxGridView1.PerformCallback(height + ";" + width);
            }
            //update width
            oldwidth = $(window).width();

            //fire on resize
            $(window).resize(function () {
                var nw = $(window).width();
                //compare new and old width      
                if (oldwidth != nw) {
                    var height = Math.max(0, document.documentElement.clientHeight);
                    var width = Math.max(0, document.documentElement.clientWidth);
                    if (width <= 1023) {
                        showExport = false;
                    }
                    else {
                        showExport = true;
                    }
                    

                    //show/hide export button first
                    detailPanelSmall.PerformCallback();

                    //resize grid

                    ASPxGridView1.PerformCallback(height + ";" + width);
                }

                oldwidth = nw;
            });
        });

        var firedFromProject = false;
        var clientSelected = false;

        function Client_IndexChanged(s, e) {
            var currentValue_client = s.GetValue();
            if (!firedFromProject) {
                Project.PerformCallback(currentValue_client);
            }
            firedFromProject = false;
        }

        function Project_IndexChanged(s, e) {
            var currentValue_project = s.GetValue();
            firedFromProject = true;
            Role.PerformCallback(currentValue_project);
            Client.PerformCallback(currentValue_project);
        }
          //Mohammad added the following 27/6/2018
        function comboboxIndexChanged(s, e) {
            var value = s.GetValue().toString();
            var name = s.GetText().toString();
            GridViewPannel.PerformCallback(name + "," + value);
            detailPanelSmall.PerformCallback();
        }
        //......................................
    </script>
     
    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { LoadingPanel.Hide(); }" />
    </dx:ASPxCallback>
    <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel"
        Modal="True">
    </dx:ASPxLoadingPanel>
    
    <dx:ASPxCallbackPanel ID="DetailPanel" runat="server" ClientInstanceName="detailPanelSmall" Width="100%" CssClass="detailPanelLarge" Collapsible="false" OnCallback="DetailPanel_Callback" SettingsLoadingPanel-Enabled="false">
        <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
        <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
        <Styles>
            <ExpandBar Width="100%" CssClass="bar">
            </ExpandBar>
            <ExpandedExpandBar CssClass="expanded">
            </ExpandedExpandBar>
        </Styles>
        <BorderTop BorderWidth="0px"></BorderTop>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                <table style="width:100%;">
                    <tr>
                        <td style="width:33.33%;">
                <dx:ASPxDateEdit runat="server" ID="ASPXDateEdit" Caption="Period End Date" AutoPostBack="false" CalendarProperties-ShowClearButton="false" Width="100%"
                    OnCustomDisabledDate="ASPXDateEdit_CustomDisabledDate" OnCalendarCustomDisabledDate="ASPXDateEdit_CustomDisabledDate"
                    CssClass="DateEdit" >
                   <%-- <BackgroundImage ImageUrl="none" />--%>
                    <DropDownButton >
                        <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                    </DropDownButton>
                    
                    <ValidationSettings ValidationGroup="DateValidationGroup" Display="Static" ErrorDisplayMode="None"> 
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                    <ClientSideEvents ValueChanged="onDateChange" />
                    <%--Refresh grid only(callBack) --%>
                </dx:ASPxDateEdit>
                </td>
                        <td style="width:33.33%;">
                <dx:ASPxTextBox ID="ASPxTextBoxHoursWorkedText" Caption="Total Hours Worked" runat="server" Enabled="false" AutoPostBack="false" Width="100%"
                    CssClass="TextBox" >
                  
                </dx:ASPxTextBox>
                            </td>
                 <%--Mohammad Added the following (27/6/2018) to let the manager choose the a specific employees timesheet + added onCallBack for GridViewPannel --%>
                        <td style="width:33.33%;">
                <dx:ASPxComboBox runat="server" Width="100%" CssClass="TextBox"  Caption="Employee" ID="ComboBoxEmployees" ClientInstanceName="comboboxemployees"  CaptionCellStyle-Paddings-PaddingLeft="3px"
                    DropDownStyle="DropDownList" TextField="EmployeeName" ValueField="EmployeeID">
                     <DropDownButton >
                        <Image Url="../Content/Images/iconmonstr-arrow-65-32.png" Width="16px" Height="16px"></Image>
                    </DropDownButton>
                    <Items>
                       
                    </Items>
                    <ClientSideEvents SelectedIndexChanged="comboboxIndexChanged" />
                </dx:ASPxComboBox>
                            </td>
                        <td style="width:0%;">
                <%-- Mohmmad's added code ends here  --%>
                <dx:ASPxButton ID="ASPxButton1" Visible="false" runat="server" Text="Submit " ImagePosition = "Right" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption"  AutoPostBack="false" Image-Url="~/Content/Images/Icons/check-64-icon.png" Image-Height="20px" Image-Width="20px" Height="36px">
                 <ClientSideEvents Click="onSubmition" />
                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
            </dx:PanelContent>
        </PanelCollection>
      
        <Paddings Padding="8px" />
    </dx:ASPxCallbackPanel>
   <div style="overflow-y: auto;height: calc(100vh - 267px);">
    <dx:ASPxCallbackPanel ID="GridViewPannel" runat="server" ClientInstanceName="GridViewPannel" Collapsible="false"  SettingsLoadingPanel-Enabled ="true" OnCallback="GridViewPannel_Callback" >
        <PanelCollection>
            <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
               
                <dx:ASPxGridView ID="ASPxGridView1"
                    runat="server" AutoGenerateColumns="False"
                    ClientInstanceName="ASPxGridView1"
                    SettingsDataSecurity-AllowEdit="true"
                    SettingsDataSecurity-AllowInsert="true"
                    SettingsDataSecurity-AllowDelete="true"
                    Width="100%"
                    AutoPostBack="true"
                    OnDataBinding="ASPxGridView1_DataBinding"
                    OnDataBound="ASPxGridView1_DataBound"
                    OnBeforeColumnSortingGrouping="ASPxGridView1_BeforeColumnSortingGrouping"
                    OnSummaryDisplayText="ASPxGridView1_SummaryDisplayText"     
                    OnRowUpdating="ASPxGridView1_RowUpdating"
                    OnRowDeleting="ASPxGridView1_RowDeleting"
                    OnRowInserting="ASPxGridView1_RowInserting"
                    OnCellEditorInitialize="ASPxGridView1_CellEditorInitialize"
                    OnCustomCallback="ASPxGridView1_CustomCallback"
                    OnCustomUnboundColumnData="ASPxGridView1_CustomUnboundColumnData"
                    OnInitNewRow="ASPxGridView1_InitNewRow"
                    KeyFieldName="TEID"
                    SettingsBehavior-AllowSort="false"
                    CssClass="newFont"
                    >
                    <SettingsPager CurrentPageNumberFormat="{0}">
                    </SettingsPager>
                    <StylesPager>
                        <PageNumber CssClass="pagenumber"></PageNumber>
                        <CurrentPageNumber CssClass="currentpagenumber"></CurrentPageNumber>
                    </StylesPager>
                    <Settings ShowTitlePanel ="true"  />
                    
                    <SettingsEditing EditFormColumnCount="4" Mode="Inline"/>
                    <ClientSideEvents EndCallback="onGridChange" />                  
                    <Styles>
                        <AlternatingRow Enabled="true" />
                        <Header HorizontalAlign="Center"></Header>
                    </Styles>

                    <SettingsPopup>
                        <EditForm Width="100%" Modal="false"/>
                    </SettingsPopup>

                    <SettingsPager PageSize="50" />
                    <Paddings Padding="0px" />
                    <Border BorderWidth="0px" />
                    <BorderBottom BorderWidth="1px" />
                    <Settings ShowFooter="True" />
                    <Styles Header-Wrap="True" />

                    <SettingsCommandButton>
                        <DeleteButton Text=" ">
                            <Styles Style-CssClass="delete_icon"></Styles>
                        </DeleteButton>
                        <NewButton Text=" ">
                            <Styles Style-CssClass="new_icon"></Styles>
                        </NewButton>
                        <EditButton Text=" ">
                            <Styles Style-CssClass="edit_icon"></Styles>
                        </EditButton>
                        <UpdateButton Text=" ">
                            <Styles Style-CssClass="update_icon"></Styles>
                        </UpdateButton>
                        <CancelButton Text=" ">
                            <Styles Style-CssClass="cancel_icon"></Styles>
                        </CancelButton>
                    </SettingsCommandButton>
                    <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
                    <Columns>
                        <dx:GridViewCommandColumn VisibleIndex="0" ShowNewButtonInHeader="true" ShowDeleteButton="true" ShowEditButton="true" />

                        <dx:GridViewDataTextColumn FieldName="Status" VisibleIndex="1" CellStyle-HorizontalAlign="Left">
                            <HeaderCaptionTemplate>
                               <i class="save_icon"></i>
                            </HeaderCaptionTemplate>
                            <EditFormSettings VisibleIndex="1" ColumnSpan="1" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataComboBoxColumn FieldName="ClientID" CellStyle-HorizontalAlign="Left">
                            <HeaderCaptionTemplate>
                               <h1 class="">Client</h1> 
                            </HeaderCaptionTemplate>
                            <EditFormSettings VisibleIndex="2" ColumnSpan="1" />
                            <PropertiesComboBox TextField="Company" ValueField="ClientID" EnableSynchronization="False" ClientInstanceName = "Client"
                                IncrementalFilteringMode="StartsWith">
                                <ClientSideEvents SelectedIndexChanged="Client_IndexChanged" />
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>

                        <dx:GridViewDataComboBoxColumn FieldName="ProjectID">
                            <HeaderCaptionTemplate>
                               <h1 class="">Project</h1> 
                            </HeaderCaptionTemplate>
                            <EditFormSettings VisibleIndex="3" ColumnSpan="1" />
                            <PropertiesComboBox TextField="Project" ValueField="ProjectID" EnableSynchronization="False" ClientInstanceName="Project"
                                IncrementalFilteringMode="StartsWith">
                                <ClientSideEvents SelectedIndexChanged="Project_IndexChanged" />
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>

                        <dx:GridViewDataComboBoxColumn FieldName="RoleName">
                            <HeaderCaptionTemplate>
                            <h1 class="">Role</h1>
                            </HeaderCaptionTemplate>
                            <EditFormSettings VisibleIndex="4" ColumnSpan="1" />
                            <PropertiesComboBox TextField="RoleName" ValueField="RoleID" EnableSynchronization="False" ClientInstanceName="Role"
                                IncrementalFilteringMode="StartsWith">
                                <ClientSideEvents Init="function(s, e) {
                                                 s.SetSelectedIndex(0);
                                            }" />
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>

                        <%--<dx:GridViewDataTextColumn  FieldName="Discipline" ReadOnly="true">
                            <HeaderCaptionTemplate>
                               <h1 class="">Discipline</h1> 
                            </HeaderCaptionTemplate>
                            <EditItemTemplate>
                               <dx:ASPxTextBox ID="ASPxTextBoxDiscipline" Text='Test' ReadOnly="true" ForeColor="Gray" runat="server" OnInit="ASPxTextBoxDiscipline_Init"></dx:ASPxTextBox>
                            </EditItemTemplate>
                        </dx:GridViewDataTextColumn>--%>

                        <dx:GridViewDataTextColumn  FieldName="SUN_MEMO" Visible="False">
                            <EditFormSettings Caption="Memo" ColumnSpan="3" Visible="True" VisibleIndex="7"/>
                             <DataItemTemplate>
                                <table>
                                    <tr>
                                        <td style="width: 90px">
                                            <%#Eval("SUN_MEMO") %>
                                        </td>
                                        <td>
                                            <dx:ASPxButton runat="server" ID="Btn" Text="Search"></dx:ASPxButton>
                                        </td>
                                    </tr>
                                </table>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                       
                        <dx:GridViewDataTextColumn FieldName="MON_MEMO" Visible="False">
                            <EditFormSettings Caption="Memo" ColumnSpan="3" Visible="True" VisibleIndex="9"/>
                        </dx:GridViewDataTextColumn>
                        
                        <dx:GridViewDataTextColumn FieldName="TUE_MEMO" Visible="False">
                           <EditFormSettings Caption="Memo" ColumnSpan="3" Visible="True" VisibleIndex="11"/>
                        </dx:GridViewDataTextColumn>
                        
                        <dx:GridViewDataTextColumn FieldName="WED_MEMO" Visible="False">
                            <EditFormSettings Caption="Memo" ColumnSpan="3" Visible="True" VisibleIndex="13"/>
                        </dx:GridViewDataTextColumn>
                        
                        <dx:GridViewDataTextColumn FieldName="THU_MEMO" Visible="False">
                            <EditFormSettings Caption="Memo" ColumnSpan="3" Visible="True" VisibleIndex="15"/>
                        </dx:GridViewDataTextColumn>
                        
                        <dx:GridViewDataTextColumn FieldName="FRI_MEMO" Visible="False">
                            <EditFormSettings Caption="Memo" ColumnSpan="3" Visible="True" VisibleIndex="17"/>
                        </dx:GridViewDataTextColumn>
                       
                        <dx:GridViewDataTextColumn FieldName="SAT_MEMO" Visible="False">
                            <EditFormSettings Caption="Memo" ColumnSpan="3" Visible="True" VisibleIndex="5"/>
                        </dx:GridViewDataTextColumn>
                                
                        <dx:GridViewDataTextColumn FieldName="TOTAL_HRS" Caption="Total Hrs" VisibleIndex="18" UnboundType="Decimal" ReadOnly="true">
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataColumn FieldName="TEOT" Caption="Overtime" VisibleIndex="19" UnboundType="Boolean" HeaderStyle-Font-Size="Medium"/>

                    </Columns>
                </dx:ASPxGridView>
                 
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
        
    <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="ASPxGridView1"/>
      </div>
</asp:Content>
                   