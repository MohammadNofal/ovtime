﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Main.master" CodeFile="ViewTime.aspx.cs" Inherits="ViewTime" %>

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        var usrName = '<%=HttpUtility.JavaScriptStringEncode(HttpContext.Current.User.Identity.Name)%>';
    </script>

    <%--<dx:ASPxPanel ID="ASPxPanel1" runat="server" ClientInstanceName="employeeSelectorPanel">
    <PanelCollection>
        <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
            <dx:ASPxComboBox runat="server" ID="ASPxComboBox2"
                        TextField="Date" ValueField="Date" TextFormatString="d"
                        SelectedIndex="0" Height="100%" Caption=" End Period Date " AutoPostBack="true" OnSelectedIndexChanged="ASPxComboBox2_SelectedIndexChanged">
            </dx:ASPxComboBox>
        </dx:PanelContent>
    </PanelCollection>
    <Paddings Padding="8px" />
</dx:ASPxPanel>--%>

    <dx:ASPxPanel ID="DetailPanel" runat="server" ClientInstanceName="detailPanelSmall" Width="100%" CssClass="detailPanelSmall" Collapsible="true">
            <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
            <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="380" />
            <Styles>
                <ExpandBar Width="100%" CssClass="bar"></ExpandBar>
                <ExpandedExpandBar CssClass="expanded"></ExpandedExpandBar>
            </Styles>
            <BorderTop BorderWidth="0px"></BorderTop>
            <PanelCollection>
                <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                    <dx:ASPxComboBox runat="server" ID="ASPxComboBox2"
                                        TextField="Date" ValueField="Date"
                                        SelectedIndex="0" Height="100%" 
                                        Caption="  Period End Date " 
                                        AutoPostBack="true" 
                                        OnSelectedIndexChanged="ASPxComboBox2_SelectedIndexChanged"
                                        CssClass="editor" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption"
                                        >
                            </dx:ASPxComboBox>

                    

                </dx:PanelContent>
            </PanelCollection>
        <Paddings Padding="8px" />
        </dx:ASPxPanel>

<dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="True" ClientInstanceName="ASPxGridView1"
    Width="100%" AutoPostBack="true"  OnDataBinding ="ASPxGridView1_DataBinding" OnDataBound = "ASPxGridView1_DataBound" OnBeforeColumnSortingGrouping="ASPxGridView1_BeforeColumnSortingGrouping">
    <SettingsPager PageSize="50" />
    <Paddings Padding="0px" />
    <Border BorderWidth="0px" />
    <BorderBottom BorderWidth="1px" />
    <Styles Header-Wrap="True" />
    <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
    <Columns>
        <dx:GridViewDataTextColumn FieldName="EmployeeName" VisibleIndex="0">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="ProjectID" VisibleIndex="1">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="ActivityID" VisibleIndex="2">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="TEDate" VisibleIndex="3">
            <PropertiesTextEdit DisplayFormatString="d" />
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="TEHours" VisibleIndex="4">
        </dx:GridViewDataTextColumn>
    </Columns>
    <Styles>
        <AlternatingRow Enabled="true" />
    </Styles>
</dx:ASPxGridView>

    


</asp:Content>
