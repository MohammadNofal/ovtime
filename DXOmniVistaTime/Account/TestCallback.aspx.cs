﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_TestCallback : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void CallbackPane_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
        PanelText.Text = this.ASPxTextBox1.Text + " - " +  DateTime.Now.ToLongTimeString();
    }
}