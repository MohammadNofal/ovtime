using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DXOmniVistaTimeEngine;
using System.Web.Security;

using System.Net;
using System.Runtime.Serialization.Json;


using System.IO;
using System.Text;


    public partial class MainMaster : System.Web.UI.MasterPage {

        DataTable dt = new DataTable();

        protected void Page_Load(object sender, EventArgs e) {

            
            //DataAccess.SetSqlConnection(ConfigurationManager.ConnectionStrings["BQEDB"].ConnectionString);

            
            //this.ASPxNavBar1.DataSource = DataAccess.GetDataTableBySqlSyntax(ConfigurationManager.AppSettings["SummaryByProject"], " and EmployeeID = 'baher.yousef' ");
           // this.ASPxNavBar1.DataBind();

            WebRequest request = WebRequest.Create("http://52.3.32.111/WCFOVTimeJSon/RestServiceImpl.svc/ptosummary/" + HttpContext.Current.User.Identity.Name + "") as HttpWebRequest;
            WebResponse response = request.GetResponse();
          
            using (Stream stream = response.GetResponseStream())
            {
                Type serializationTargetType = typeof(List<Pto>);
                DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(serializationTargetType);

                List<Pto> jsonDeserialized = (List<Pto>)jsonSerializer.ReadObject(stream);

                dt = new DataTable();
                dt.Columns.Add("Group", typeof(string));
                dt.Columns.Add("ColumnText", typeof(string));
                dt.Columns.Add("ColumnValue", typeof(string));

                dt.PrimaryKey = new DataColumn[] { dt.Columns["ColumnText"] };

                foreach (Pto p in jsonDeserialized)
                {
                   DataRow dr = dt.NewRow();
                   dr[0] = "PTO Summary";
                   dr[1] = p.ColumnText;
                   dr[2] = p.ColumnValue;
                   dt.Rows.Add(dr);
                }

                if (dt.Rows.Count == 0)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = "PTO Summary";
                    dr[1] = "No PTO Data";
                    dr[2] = "0";
                    dt.Rows.Add(dr);
                }

            }

            NavBarDataBind();

        }

        protected void LeftPane_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            //txtBox.Value = "records may need refresh";
            //NavBarDataBind();

        }

        private void NavBarDataBind()
        {
            //create groups:

            string currentGroup = dt.Rows[0].ItemArray[0].ToString();
            DevExpress.Web.NavBarGroup  gr = new DevExpress.Web.NavBarGroup();
            gr.Text = currentGroup;
            gr.Name = currentGroup;
            navbar.Groups.Add(gr);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string rowGroup = dt.Rows[i].ItemArray[0].ToString();
                if (rowGroup != currentGroup)
                {
                    DevExpress.Web.NavBarGroup rowGr = new DevExpress.Web.NavBarGroup();
                    rowGr.Text = rowGroup;
                    rowGr.Name = rowGroup;
                    navbar.Groups.Add(rowGr);
                    currentGroup = rowGroup;
                }
            }

            //fill items:

            for (int j = 0; j < dt.Rows.Count; j++)
            {
                string gr1 = dt.Rows[j].ItemArray[0].ToString();
                DevExpress.Web.NavBarGroup navBarGr = navbar.Groups.FindByName(gr1);
                if (navBarGr != null)
                {
                    DevExpress.Web.NavBarItem it = new DevExpress.Web.NavBarItem();

                    string result = "<div class='Content'><div class='LeftPanel'><div class='Title'>" + dt.Rows[j].ItemArray[1].ToString() + "</div>";
                    result += "<div id='CallbackTime' class='Value'><span>" + dt.Rows[j].ItemArray[2].ToString() + "</span><span>hrs</span></div></div></div>";

                    it.Text = result;
                                        
                    navbar.Groups[navBarGr.Index].Items.Add(it);
                }
            }
        } 
}

public class Pto
{
    public string ColumnText { get; set; }
    public double ColumnValue { get; set; }
}
