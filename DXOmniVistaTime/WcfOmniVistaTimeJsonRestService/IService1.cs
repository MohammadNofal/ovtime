﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfOmniVistaTimeJsonRestService
{
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        string GetData(string id);
        //Person GetData(string id);

        [OperationContract]
        TimeEntry[] GetTimeEntry(string id);

        [OperationContract]
        TimeEntry[] GetTimeEntry1();
    }
}
