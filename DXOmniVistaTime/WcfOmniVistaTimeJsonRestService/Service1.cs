﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;



namespace WcfOmniVistaTimeJsonRestService
{
    public class Service1 : IService1
    {
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    UriTemplate = "data/{id}")]
        public string GetData(string id)
        {
            return JsonDataAccess.ConvertDataTabletoString(id);
        }

        [WebInvoke(Method = "GET", 
                    ResponseFormat = WebMessageFormat.Json, 
                    UriTemplate = "timeentry/{id}")]
        public TimeEntry[] GetTimeEntry(string id)
        {
            return JsonDataAccess.GetTimeEntries(id).ToArray();
        }

        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    UriTemplate = "timeentry1.Json")]
        public TimeEntry[] GetTimeEntry1()
        {
            return JsonDataAccess.GetTimeEntries("baher.yousef").ToArray();
        }

        //public Person GetData(string id)
        //{
        //    // lookup person with the requested id 
        //    return new Person()
        //    {
        //        Id = Convert.ToInt32(id),
        //        Name = "Leo Messi"
        //    };
        //}
    }

    //public class Person
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}
