﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace WcfOmniVistaTimeJsonRestService
{
    [DataContract(Namespace = "http://52.3.32.111")]
    public class TimeEntry
    {
        [DataMember]
        public string EmployeeID { get; set; }
        [DataMember]
        public string ProjectID { get; set; }
        //[DataMember]
        //public string ProjectName { get; set; }
        //[DataMember]
        //public string TotalHours { get; set; }
    }
}
