﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Main.master" CodeFile="Default.aspx.cs" Inherits="Default" %>



<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    
   
    <script>
        var usrName = '<%=HttpUtility.JavaScriptStringEncode(HttpContext.Current.User.Identity.Name)%>';
    </script>

    <div id="populateLable"></div>

    <%--<dx:ASPxPanel ID="ASPxPanel1" runat="server" ClientInstanceName="employeeSelectorPanel">
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
                <dx:ASPxLabel runat="server">
                </dx:ASPxLabel>
            </dx:PanelContent>
        </PanelCollection>
        <Paddings Padding="8px" />
    </dx:ASPxPanel>--%>

    
    <%--<dxchartsui:WebChartControl ID="WebChartControl1" runat="server" CrosshairEnabled="True" Height="300px" Width="600">
                            
    </dxchartsui:WebChartControl>--%>
    
    

    <%--<div id="chartContainer" style="height:500px; max-width:700px; margin: 0 auto"></div>--%>
    <%--<table style="width:85%">
        <tr>
            <td style="width:50%"></td>
            <td style="width:50%"><div id="chart"></div></td>
        </tr>
    </table>--%>
    
    <%--<div id="populateLable"></div>--%>

    <dx:ASPxPanel ID="ASPxPanel3" runat="server" CssClass="detailPanelSmallHeader"></dx:ASPxPanel>
    <dx:ASPxPanel ID="ASPxPanel5" runat="server" CssClass="detailPanelSmall">
        <PanelCollection>
                <dx:PanelContent ID="PanelContent3" runat="server" SupportsDisabledAttribute="True">
                  Top 4 Projects:
                </dx:PanelContent>
            </PanelCollection>
    </dx:ASPxPanel>
    <dx:ASPxPanel ID="ASPxPanel2" runat="server" ClientInstanceName="detailPanel" Width="100%" CssClass="detailPanel" Collapsible="False">
            <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
            <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="180" />
            <Styles>
                <ExpandBar Width="100%" CssClass="bar"></ExpandBar>
                <ExpandedExpandBar CssClass="expanded"></ExpandedExpandBar>
            </Styles>
            <BorderTop BorderWidth="0px"></BorderTop>
            <PanelCollection>
                <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                   
                    <div id="pieChartContainer" class="graphContainer" style="float:left;width:45%;" ></div>
                    
                    <div id="lineChartContainer" class="graphContainer" style="float:left;width:45%;"></div>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxPanel>
   <%-- <br />
    <dx:ASPxPanel ID="DetailPanel" runat="server" ClientInstanceName="detailPanel" Width="100%" CssClass="detailPanel" Collapsible="true">
            <SettingsCollapsing ExpandEffect="PopupToTop" AnimationType="Slide" />
            <SettingsAdaptivity CollapseAtWindowInnerHeight="680" HideAtWindowInnerHeight="380" />
            <Styles>
                <ExpandBar Width="100%" CssClass="bar"></ExpandBar>
                <ExpandedExpandBar CssClass="expanded"></ExpandedExpandBar>
            </Styles>
            <BorderTop BorderWidth="1px"></BorderTop>
            <PanelCollection>
                <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                    <dx:ASPxTextBox ID="tbCompanyName" ClientInstanceName="tbCompanyName" runat="server" Caption="Company Name" CssClass="editor" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption" ReadOnly="true" />
                    <dx:ASPxTextBox ID="tbContactName" ClientInstanceName="tbContactName" runat="server" Caption="Contact Name" CssClass="editor" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption" ReadOnly="true" />
                    <dx:ASPxTextBox ID="tbContactTitle" ClientInstanceName="tbContactTitle" runat="server" Caption="Contact Title" CssClass="editor" RootStyle-CssClass="editorContainer" CaptionCellStyle-CssClass="editorCaption" ReadOnly="true" />
                    
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxPanel>--%>
        

    <%--<table id="table" class="table"></table>--%>

    <dx:ASPxPanel ID="ASPxPanel1" runat="server" CssClass="detailPanelSmallHeader"></dx:ASPxPanel>
    <dx:ASPxPanel ID="ASPxPanel4" runat="server" CssClass="detailPanelSmall">
        <PanelCollection>
                <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
                  Year to Date Summary:
                </dx:PanelContent>
            </PanelCollection>
    </dx:ASPxPanel>
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="True" ClientInstanceName="ASPxGridView1"
    Width="100%" AutoPostBack="true"  
        OnDataBinding ="ASPxGridView1_DataBinding" 
        OnDataBound = "ASPxGridView1_DataBound" 
        OnBeforeColumnSortingGrouping="ASPxGridView1_BeforeColumnSortingGrouping"
        Onsummarydisplaytext="ASPxGridView1_SummaryDisplayText"> 
        <SettingsPager PageSize="50" />
        <Paddings Padding="0px" />
        <Border BorderWidth="0px" />
        <BorderBottom BorderWidth="1px" />
        <Settings ShowFooter="True" />
        <Styles Header-Wrap="True" />
        <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
        <Columns>
            <dx:GridViewDataTextColumn FieldName="ProjectID" VisibleIndex="0">
                <HeaderCaptionTemplate>
                    <table>
                        <tr>
                        <th><dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="~/Content/Images/Icons/Write-Document-icon.png" /></th>
                        <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Project Id" ForeColor="White"/></th>
                        </tr>
                    </table>
                </HeaderCaptionTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ProjectName" VisibleIndex="1">
                <HeaderCaptionTemplate>
                    <table>
                        <tr>
                        <th><dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="~/Content/Images/Icons/Remove-Document-icon.png" /></th>
                        <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Project Name" ForeColor="White"/></th>
                        </tr>
                    </table>
                </HeaderCaptionTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="TotalHours" VisibleIndex="4">
                <HeaderCaptionTemplate>
                    <table>
                        <tr>
                        <th><dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="~/Content/Images/Icons/Internet-History-icon.png" /></th>
                        <th><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Total Hours" ForeColor="White"/></th>
                        </tr>
                    </table>
                </HeaderCaptionTemplate>
            </dx:GridViewDataTextColumn>
        </Columns>
        <Styles>
            <AlternatingRow Enabled="true" />
        </Styles>
    </dx:ASPxGridView>

    


</asp:Content>
